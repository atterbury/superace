using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Collections;
using System.Net;
using System.Linq;

using Project.DAL;

namespace SuperACECalcs
{
	/// <summary>
	/// Summary description for Utils.
	/// </summary>
	static class Utils
	{
        //public Utils()
        //{
        //}

        //public static ProjectTblsModel.Species GetSpecies(ref ProjectTbModelEntities pContext, Project.DAL.Project pProject, string pSpecies)
        //{
        //    Species aRec = pContext.Species.FirstOrDefault(s => s.TableName == pProject.SpeciesTableName && s.Abbreviation == pSpecies);

        //    return aRec;
        //}

        public static byte ConvertToTiny(string sTmp)
        {
            byte f;

            try
            {
                f = Convert.ToByte(sTmp);
            }
            catch
            {
                f = 0;
            }
            return (f);
        }

        public static bool ConvertToBool(string sTmp)
        {
            bool f;

            try
            {
                f = Convert.ToBoolean(sTmp);
            }
            catch
            {
                f = false;
            }
            return (f);
        }

        public static float ConvertToFloat(string sTmp)
		{
			float f;

			try
			{
				f = Convert.ToSingle(sTmp);
			}
			catch
			{
				f = 0.0f;
			}
			return(f);
		}

        public static int ConvertToInt(string sTmp)
		{
			int i;

			try
			{
				i = Convert.ToInt16(sTmp);
			}
			catch
			{
				i = 0;
			}
			return(i);
		}

        public static double ConvertToDouble(string sTmp)
		{
			double f;

			try
			{
				f = Convert.ToDouble(sTmp);
			}
			catch
			{
				f = 0.0f;
			}
			return(f);
		}

        public static long ConvertToLong(string sTmp)
		{
			long l;

			try
			{
				l = Convert.ToInt32(sTmp);
			}
			catch
			{
				l = 0;
			}
			return(l);
		}

        public static bool IsEven(string sTmp)
		{
			int iNum = ConvertToInt(sTmp);

			if(iNum % 2 == 0)
			{
				return(true); // It's even
			}
			else
			{
				return(false);  // It's odd
			}
		}

        public static bool IsOdd(string sTmp)
		{
			int iNum = ConvertToInt(sTmp);

			if(iNum % 2 == 0)
			{
				return(false); // It's even
			}
			else
			{
				return(true);  // It's odd
			}
		}

        public static bool IsDigit(char c)
		{
			bool b;

			try
			{
				b = Char.IsDigit(c);
			}
			catch
			{
				b = false;
			}
			return(b);
		}

        public static bool IsAlpha(char c)
		{
			bool b;

			try
			{
				b = Char.IsLetter(c);
			}
			catch
			{
				b = false;
			}
			return(b);
		}

        public static bool IsValidInt(string sTmp)
		{
			try
			{
				int iTmp = Int16.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}

        public static bool IsValidLong(string sTmp)
		{
			try
			{
				long lTmp = Int32.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}

        public static bool IsValidFloat(string sTmp)
		{
			try
			{
				float fTmp = Single.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}
	}
}
