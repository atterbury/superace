using System;
using System.Data;
using System.Windows.Forms;
//using System.Data.SqlServerCe;

using System.Diagnostics;
using System.Linq;

using Project.DAL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SuperACECalcs
{
    /// <summary>
    /// Summary description for Calc.
    /// </summary>
    public static class Calc
    {
        //public Calc(ref ProjectDbContext pContext, Project.DAL.Project pProject, ref Stand pStand)
        //{
        //    Init();
        //    mCurrentProject = pProject;
        //    mCurrentStand = pStand;
        //    projectContext = pContext;
        //}

        ////public Calc(ref ProjectDbContext pContext)
        ////{
        ////    Init();
        ////    projectContext = pContext;
        ////}

        ////public void SetProjectStand(Project pProject, ref Stand pStand)
        ////{
        ////    Init();
        ////    mCurrentProject = pProject;
        ////    mCurrentStand = pStand;
        ////}

        public static void SetCalcContextProjectStand(ref ProjectDbContext pContext, Project.DAL.Project pProject, ref Stand pStand)
        {
            Init();
            mCurrentProject = pProject;
            mCurrentStand = pStand;
            projectContext = pContext;
        }

        public static void Init()
        {
            bark = 0.0f;
            ao = 0.0f;
            basal_area = 0.0f;
            trees_pa = 0.0f;
            logs_pa = 0.0f;
            log_top = 0.0f;
            log_butt = 0.0f;
            logs_mbf = 0.0f;
            logs_ccf = 0.0f;
            tree_mbf = 0.0f;
            tree_ccf = 0.0f;
            tree_scribner_gross = 0.0f;
            tree_scribner_net = 0.0f;
            tree_cubic_gross = 0.0f;
            tree_cubic_net = 0.0f;
            tree_cubic_merch = 0.0f;
            tree_cubic_gross_pa = 0.0f;
            tree_cubic_net_pa = 0.0f;
            tree_scribner_gross_pa = 0.0f;
            tree_scribner_net_pa = 0.0f;
            number_segments = 0;
            teList.Clear();
            for (int i = 0; i < 12; i++)
                seg[i] = new SgCalc();
        }

        public static string projectDataSource = string.Empty;
        private static ProjectDbContext projectContext = null;
        private static Species spcRec = null;

        public static Tree tree;
        public static float bark;
        public static float ao;
        public static float basal_area;
        public static float trees_pa;
        public static float tree_scribner_gross;
        public static float tree_scribner_net;
        public static float tree_cubic_gross;
        public static float tree_cubic_net;
        public static float tree_cubic_merch;
        public static float logs_pa;
        public static float log_top;
        public static float log_butt;
        public static float tree_cubic_gross_pa;
        public static float tree_cubic_net_pa;
        public static float tree_scribner_gross_pa;
        public static float tree_scribner_net_pa;
        public static float tree_mbf;
        public static float tree_ccf;
        public static float logs_mbf;
        public static float logs_ccf;
        public static int number_segments;
        public static SgCalc[] seg = new SgCalc[12];
        public static List<TreeErrors> teList = new List<TreeErrors>();

        // Calc Values
        static bool cull_segment;
        static bool fiber_segment;
        static bool west_routine;
        static int y;
        static int dash_or_plus, plus_status, plus_seg;
        static public int calc_error;
        static public int calc_segment;
        static float c_baf;
        static int c_bole_length;
        static int c_form_point;
        static float c_d4h;
        static float c_form_factor;
        static float bark_thickness;
        static float asubo, lsubo;
        public static float trees_per_acre;
        static float basal_area_per_acre;
        static float diameter_top;
        public static float total_tree_height;
        static float dib_at_d4h, dib_at_form_point;
        static float slope_calc, c_a, c_b, c_c;
        static float mtd_height, merchantable_height;
        static float merchantable_diameter;
        static int log_index, log_max;
        static int seg_index, cvol_index;
        static string[] segment_string = new string[12];
        static string segment_sort, segment_grade;
        static string segment_length;
        static float seg_length;
        static float bf_ded_feet, bf_ded_inches, cf_ded_feet, cf_ded_inches;
        static string deduction_code;
        static float c_percent;
        static float bf_ded_percent, cf_ded_percent;
        static float east_side_butt_length, east_side_next_butt;
        static float[] segment_whole_half = new float[12];
        static float trim_allowed;
        static float[] log_log_length = new float[20];
        static float[] seg_log_length = new float[20];
        static float[] seg_log_top = new float[20];
        static float[] seg_log_butt = new float[20];
        static float taper_top, taper_butt;
        static float taper_difference;

        static float national_diameter;
        static float national_diameter_rounded;
        static float sweyco_diameter;
        static float sweyco_diameter_rounded;
        static float seg_top_height, seg_butt_height;
        static float seg_top_diameter, seg_butt_diameter;
        static float real_value;
        static float original_length;
        static float segment_actual_length;
        static int seg_log_index, seg_log_max;
        static float graded_logs;
        static float graded_butt_dia;
        static float graded_top_dia;
        static float log_length, log_butt_diameter, log_top_diameter;
        static float log_scribner;
        static float log_net_scribner;
        static float log_cubic;
        static float log_net_cubic;
        static float log_merch_cubic;
        static string temp_species;

        static string aoSpecies = string.Empty;
        static double ao_a = 0;
        static double ao_b = 0;
        static double ao_c = 0;

        // Species Values
        private static string spcBarkThickness;
        private static string spcAo;
        //private string spcAlphaAo = null;
        private static string spcFF16 = null;
        private static string spcScribnerScale;
        private static string spcCubicScale;
        private static string spcMinTopDia;
        private static string spcMinLogLen;
        private static string spcMaxLogLen;
        private static string spcTrim;
        private static string spcWeight;

        // Sort Values
        private static string srtFiber;
        private static string srtMinDia;
        private static string srtMinLen;
        private static string srtMinVol;
        private static string srtMaxButt;

        // Grade Values
        private static string grdFiber;
        private static string grdMinDia;
        private static string grdMinLen;
        private static string grdMinVol;
        private static string grdMaxButt;

        // Master Values
        private static Stand mCurrentStand = null;
        private static Project.DAL.Project mCurrentProject = null;
        //private BusinessObjects.Plots mCurrentPlot = new Plots();
        private static string masterAcres = null;
        private static string masterStripTree = null;
        private static string masterFixedRadius = null;
        private static string masterBaf = null;
        private static string masterAreaPlotAcres = null;
        private static int masterAge = 0;
        private static string masterDbhOpt = null;

        public static bool CalcStandFields(ref ProjectDbContext ctx, Stand pStand)
        {
            double dBAPerAcre = 0;
            double dLogsPA = 0;
            double dTmpBA = 0;
            double dTmp2 = 0;
            double dTmpTpa = 0;
            double dTotalBdFtGross = 0;
            double dTotalBdFtNet = 0;
            double dTotalCuFtGross = 0;
            double dTotalCuFtNet = 0;

            double dTotalBdFtGrossPerAcre = 0;
            double dTotalBdFtNetPerAcre = 0;
            double dTotalCuFtGrossPerAcre = 0;
            double dTotalCuFtNetPerAcre = 0;

            double dTotalCcfPerAcre = 0;
            double dTotalMbfPerAcre = 0;

            double dTotalCuFtMerch = 0;
            double dTons = 0;
            double dTonPerAcre = 0;

            double dTotalCcf = 0;
            double dTotalMbf = 0;

            int iAvgTotalHt = 0;
            double dTotalHt = 0;
            double dTrees = 0;

            Stand sRec = ctx.Stands.FirstOrDefault(s => s.StandsId == pStand.StandsId);
            sRec.Plots = GetNumberOfPlots();

            List<Tree> coll = ctx.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == sRec.StandsId).ToList();
            foreach (Tree item in coll)
            {
                if (item.BasalArea > 0)
                    dTmpBA += (double)item.BasalArea;
                if (item.TreesPerAcre > 0)
                    dTmpTpa += (double)item.TreesPerAcre;
                if (item.LogsPerAcre > 0)
                    dLogsPA += (double)item.LogsPerAcre;
                if (item.TotalHeight > 0)
                {
                    dTrees++;
                    dTotalHt += (double)item.TotalHeight;
                }

                if (item.TotalBdFtGrossVolume != null && item.TreesPerAcre != null)
                    dTotalBdFtGross += (double)item.TotalBdFtGrossVolume * (double)item.TreesPerAcre;
                if (item.TotalBdFtNetVolume != null && item.TreesPerAcre != null)
                    dTotalBdFtNet += (double)item.TotalBdFtNetVolume * (double)item.TreesPerAcre;
                if (item.TotalCuFtGrossVolume != null && item.TreesPerAcre != null)
                    dTotalCuFtGross += (double)item.TotalCuFtGrossVolume * (double)item.TreesPerAcre;
                if (item.TotalCuFtNetVolume != null && item.TreesPerAcre != null)
                    dTotalCuFtNet += (double)item.TotalCuFtNetVolume * (double)item.TreesPerAcre;

                if (item.TotalGrossCubicPerAcre != null)
                    dTotalCuFtGrossPerAcre += (double)item.TotalGrossCubicPerAcre;
                if (item.TotalNetCubicPerAcre != null)
                    dTotalCuFtNetPerAcre += (double)item.TotalNetCubicPerAcre;
                if (item.TotalGrossScribnerPerAcre != null)
                    dTotalBdFtGrossPerAcre += (double)item.TotalGrossScribnerPerAcre;
                if (item.TotalNetScribnerPerAcre != null)
                    dTotalBdFtNetPerAcre += (double)item.TotalNetScribnerPerAcre;

                if (item.TotalCcf != null)
                    dTotalCcf += (double)item.TotalCcf;
                if (item.TotalMbf != null)
                    dTotalMbf += (double)item.TotalMbf;
            }
            //coll.Save();

            if (dTotalHt > 0 && dTrees > 0)
                sRec.AvgTotalHeight = (short)(dTotalHt / dTrees);
            if (dLogsPA > 0 && mCurrentStand.Plots > 0)
                sRec.LogsPerAcre = dLogsPA / (double)sRec.Plots;
            else
                sRec.LogsPerAcre = 0;
            if (dTmpTpa > 0 && mCurrentStand.Plots > 0)
                sRec.TreesPerAcre = dTmpTpa / (double)sRec.Plots;
            else
                sRec.TreesPerAcre = 0;
            if (dTmpBA > 0 && sRec.Plots > 0)
                sRec.BasalAreaPerAcre = dTmpBA / (double)sRec.Plots;
            else
                sRec.BasalAreaPerAcre = 0;
            if (sRec.BasalAreaPerAcre != null && sRec.TreesPerAcre != null)
            {
                if (sRec.BasalAreaPerAcre > 0 && sRec.TreesPerAcre > 0)
                    sRec.QmDbh = Math.Sqrt((double)sRec.BasalAreaPerAcre / ((double)sRec.TreesPerAcre * 0.005454154));
            }
            if (dTotalBdFtGross > 0)
                sRec.TotalBdFtGross = dTotalBdFtGross / (double)sRec.Plots;
            else
                sRec.TotalBdFtGross = 0;
            if (dTotalBdFtNet > 0)
                sRec.TotalBdFtNet = dTotalBdFtNet / (double)sRec.Plots;
            else
                sRec.TotalBdFtNet = 0;
            if (dTotalCuFtGross > 0)
                sRec.TotalCuFtGross = dTotalCuFtGross / (double)sRec.Plots;
            else
                sRec.TotalCuFtGross = 0;
            if (dTotalCuFtNet > 0)
                sRec.TotalCuFtNet = dTotalCuFtNet / (double)sRec.Plots;
            else
                sRec.TotalCuFtNet = 0;

            //if (dTonPerAcre > 0 && pMaster.Plots > 0)
            //{
            //    pMaster.TonsPerAcre = ((double)dTonPerAcre / 2000)/pMaster.Plots; // (double)pMaster.Plots;
            //    //pMaster.TotalTons = dTonPerAcre * pMaster.NetGeographicAcres;
            //}
            //else
            //{
            //    pMaster.TonsPerAcre = 0;
            //    //pMaster.TotalTons = 0;
            //}
            if (dTotalCuFtMerch > 0)
                sRec.TotalCuFtMerch = dTotalCuFtMerch;
            else
                sRec.TotalCuFtMerch = 0;

            //if (dTotalCcf > 0 && pMaster.Plots > 0)
            //    pMaster.TotalCcf = dTotalCcf / pMaster.Plots;
            //if (dTotalMbf > 0 && pMaster.Plots > 0)
            //    pMaster.TotalMbf = dTotalMbf / pMaster.Plots;

            if (sRec.TotalCuFtNet > 0 && sRec.NetGeographicAcres > 0)
                sRec.TotalCcf = ((sRec.TotalCuFtNet * (double)sRec.NetGeographicAcres)) / 100;
            else
                sRec.TotalCcf = 0;
            if (sRec.TotalBdFtNet > 0 && sRec.NetGeographicAcres > 0)
                sRec.TotalMbf = ((sRec.TotalBdFtNet * (double)sRec.NetGeographicAcres)) / 1000;
            else
                sRec.TotalMbf = 0;

            if (sRec.TotalCcf > 0)
                sRec.CcfPerAcre = sRec.TotalCcf / sRec.NetGeographicAcres;
            if (sRec.TotalMbf > 0)
                sRec.MbfPerAcre = sRec.TotalMbf / sRec.NetGeographicAcres;

            //if (pMaster.NetGeographicAcres > 0)
            //{
            //    if (pMaster.TotalMbf > 0)
            //        pMaster.MbfPerAcre = pMaster.TotalMbf / pMaster.NetGeographicAcres; // needs TotalMbf
            //    else
            //        pMaster.MbfPerAcre = 0; // needs TotalMbf
            //    if (pMaster.TotalCcf > 0)
            //        pMaster.CcfPerAcre = (double)pMaster.TotalCcf / (double)pMaster.NetGeographicAcres;
            //    else
            //        pMaster.CcfPerAcre = 0;
            //}
            // Major Species
            var q1 = (from s in ctx.Treesegments
                      where s.StandsId == sRec.StandsId
                         group s by s.Species into g
                         select new
                         {
                             tSpecies = g.Key,
                             TotalGross = g.Sum(s => s.ScribnerGrossVolume)
                         }).OrderByDescending(x => x.TotalGross);
            int f = 0;
            foreach (var item in q1)
            {
                if (f == 0)
                {
                    sRec.MajorSpecies = item.tSpecies;
                    f = 1;
                }
            }
            //string sql = string.Format("SELECT Species, SUM(ScribnerGrossVolume) AS TotalGross FROM Treesegment WHERE StandsId = {0} GROUP BY Species ORDER BY TotalGross Desc", mCurrentStand.StandsId);
            //TreesegmentsCollection sc = new TreesegmentsCollection(ProjectDataSource);
            //DataTable tblSpc = sc.CustomFilLdataTableText(sql);
            //if (tblSpc.Rows.Count > 0)
            //    mCurrentStand.MajorSpecies = tblSpc.Rows[0][0].ToString();
            //tblSpc.Dispose();

            if (sRec.MajorSpecies == null || string.IsNullOrEmpty(sRec.MajorSpecies.Trim()))
            {
                var q2 = (from t in ctx.Trees
                          where t.StandsId == sRec.StandsId
                           group t by t.SpeciesAbbreviation into g
                           select new
                           {
                               tSpecies = g.Key,
                               TreesPerAcre = g.Sum(s => s.TreesPerAcre)
                           }).OrderByDescending(x => x.TreesPerAcre);
                int f2 = 0;
                foreach (var item in q2)
                {
                    if (f2 == 0)
                    {
                        sRec.MajorSpecies = item.tSpecies;
                        f2 = 1;
                    }
                }
                //sql = string.Format("SELECT SpeciesAbbreviation, SUM(TreesPerAcre) AS TotalTPA FROM Tree WHERE StandsId = {0} GROUP BY SpeciesAbbreviation ORDER BY TotalTPA Desc", mCurrentStand.StandsId);

                //TreesCollection tc = new TreesCollection(ProjectDataSource);
                //tblSpc = tc.CustomFilLdataTableText(sql);
                //if (tblSpc.Rows.Count > 0)
                //    mCurrentStand.MajorSpecies = tblSpc.Rows[0][0].ToString();
                //tblSpc.Dispose();
            }

            List<DefReportDomPri> dpColl = ctx.DefReportDomPris.OrderBy(t => t.Priority).ToList();
            IList<Tree> cc = null;
            if (dpColl.Count > 0)
            {
                switch (dpColl[0].Item)
                {
                    case "Trees per Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    //case "Normality":
                    //    cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                    //    break;
                    case "Net Cubic Feet / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetCubicPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    case "Net Board Feet / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    case "Basal Area / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.BasalArea).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    default:
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                }
            }
            else
            {
                cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
            }
            if (cc.Count > 0)
                sRec.MajorAge = (short)GetAge(cc[0].AgeCode.ToString());

            //IList<Tree> cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
            //if (cc.Count > 0)
            //    sRec.MajorAge = (short)GetAge(cc[0].AgeCode.ToString());
            ////tblAge.Dispose();

            if (sRec.MajorSpecies != null && !string.IsNullOrEmpty(sRec.MajorSpecies.Trim()))
            {
                Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, mCurrentProject.SpeciesTableName, sRec.MajorSpecies);
                sRec.TonsPerAcre = sRec.CcfPerAcre * spc.Lbs / 2000;
            }

            // SiteIndex
            if (sRec.Species11.Trim() == sRec.MajorSpecies || sRec.Species12.Trim() == sRec.MajorSpecies || sRec.Species13.Trim() == sRec.MajorSpecies)
                sRec.SiteIndex = sRec.SiteIndex1;
            else
                if (sRec.Species21.Trim() == sRec.MajorSpecies || sRec.Species22.Trim() == sRec.MajorSpecies || sRec.Species23.Trim() == sRec.MajorSpecies)
                    sRec.SiteIndex = sRec.SiteIndex2;
                else
                    if (sRec.Species31.Trim() == sRec.MajorSpecies || sRec.Species32.Trim() == sRec.MajorSpecies || sRec.Species33.Trim() == sRec.MajorSpecies)
                        sRec.SiteIndex = sRec.SiteIndex3;
                    else
                        if (sRec.Species41.Trim() == sRec.MajorSpecies || sRec.Species42.Trim() == sRec.MajorSpecies || sRec.Species43.Trim() == sRec.MajorSpecies)
                            sRec.SiteIndex = sRec.SiteIndex4;
            ctx.SaveChanges();
            //mCurrentStand.Save();
            //Debug.WriteLine("Finished Stand");
            return true;
        }

        private static int? GetNumberOfPlots()
        {
            int total = 0;

//#if DEBUG
//            if (pMaster.TractName == "UNIT 3")
//                System.Diagnostics.Debugger.Break();
//#endif
            var q = (from p in projectContext.Plots
                    where p.StandsId == mCurrentStand.StandsId
                    group p by p.UserPlotNumber into g
                    select g);
            //tColl.Query.Select(tColl.Query.PlotNumber).Where(tColl.Query.StandsId == pMaster.StandsId).GroupBy(tColl.Query.PlotNumber).OrderBy(tColl.Query.PlotNumber.Ascending);
            //tColl.Query.Load();
            return q.Count();
        }
        /*
        public bool CalcStandFieLds(ref Stands pStand)
        {
            double dBAPerAcre = 0;
            double dLogsPA = 0;
            double dTmp1 = 0;
            double dTmp2 = 0;
            double dTmpTpa = 0;
            double dTotalBdFtGross = 0;
            double dTotalBdFtNet = 0;
            double dTotalCuFtGross = 0;
            double dTotalCuFtNet = 0;
            double dTotalCuFtMerch = 0;
            double dTons = 0;
            double dTonPerAcre = 0;

            int iAvgTotalHt = 0;
            double dTotalHt = 0;
            double dTrees = 0;

		     TreesCollection tColl = new TreesCollection();
             tColl.Query.Where(tColl.Query.StandsId == pStand.StandsId);
             tColl.Query.Load();
             foreach (Trees tItem in tColl)
             {
                 // BAPerAcre
                 if (tItem.ReportedFormFactor > 0)
                 {
                     double ff2 = ((double)tItem.ReportedFormFactor / 100) * ((double)tItem.ReportedFormFactor / 100);
                     dTmp1 = Utils.ConvertToDouble(GetPlotFactor(pStand, tItem)) / ff2;
                     if (dTmp1 > 0)
                         dTmp2 += dTmp1;
                 }
                 if (tItem.BasalArea > 0)
                     dTmp2 += (double)tItem.BasalArea;
                 if (tItem.TreesPerAcre > 0)
                     dTmpTpa += (double)tItem.TreesPerAcre;
                 if (tItem.LogsPerAcre > 0)
                     dLogsPA += (double)tItem.LogsPerAcre;
                 if (tItem.TotalHeight > 0)
                 {
                     dTrees++;
                     dTotalHt += (double)tItem.TotalHeight;
                 }

                 TreesegmentsCollection sColl = new TreesegmentsCollection();
                 sColl.Query.Where(sColl.Query.StandsId == tItem.StandsId);
                 sColl.Query.Load();
                 foreach (Treesegments segItem in sColl)
                 {
                     //if (segItem.ScribnerGrossVolume != null)
                     //    dTotalBdFtGross += (double)segItem.ScribnerGrossVolume;
                     //if (segItem.ScribnerNetVolume != null)
                     //    dTotalBdFtNet += (double)segItem.ScribnerNetVolume;
                     //if (segItem.CubicGrossVolume != null)
                     //    dTotalCuFtGross += (double)segItem.CubicGrossVolume;
                     //if (segItem.CubicNetVolume != null)
                     //    dTotalCuFtNet += (double)segItem.CubicNetVolume;

                     if (segItem.ScribnerGrossVolume != null && tItem.TreesPerAcre != null)
                         dTotalBdFtGross += (double)segItem.ScribnerGrossVolume * (double)tItem.TreesPerAcre;
                     if (segItem.ScribnerNetVolume != null && tItem.TreesPerAcre != null)
                         dTotalBdFtNet += (double)segItem.ScribnerNetVolume * (double)tItem.TreesPerAcre;
                     if (segItem.CubicGrossVolume != null && tItem.TreesPerAcre != null)
                         dTotalCuFtGross += (double)segItem.CubicGrossVolume * (double)tItem.TreesPerAcre;
                     if (segItem.CubicNetVolume != null && tItem.TreesPerAcre != null)
                         dTotalCuFtNet += (double)segItem.CubicNetVolume * (double)tItem.TreesPerAcre;
                 }
                 if (dTotalBdFtGross > 0)
                     dTotalBdFtGross = dTotalBdFtGross / (float)pStand.PlotsCollectionByStandsId.Count;
                 if (dTotalBdFtNet > 0)
                     dTotalBdFtNet = dTotalBdFtNet / (float)pStand.PlotsCollectionByStandsId.Count;
                 if (dTotalCuFtGross > 0)
                     dTotalCuFtGross = dTotalCuFtGross / (float)pStand.PlotsCollectionByStandsId.Count;
                 if (dTotalCuFtNet > 0)
                     dTotalCuFtNet = dTotalCuFtNet / (float)pStand.PlotsCollectionByStandsId.Count;

                 if (dTotalCuFtNet > 0)
                     dTonPerAcre += (double)(tItem.TotalNetCubicPerAcre * (double)GetWeightFromSpecies(tItem)) / 2000;
             }
            
            if (dTotalHt > 0 && dTrees > 0)
                pStand.AvgTotalHeight = (short)(dTotalHt / dTrees);
            if (dLogsPA > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
                pStand.LogsPerAcre = dLogsPA / (double)pStand.PlotsCollectionByStandsId.Count;
            else
                pStand.LogsPerAcre = 0;
            if (dTmpTpa > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
                pStand.TreesPerAcre = dTmpTpa / (double)pStand.PlotsCollectionByStandsId.Count;
            else
                pStand.TreesPerAcre = 0;
            if (dTmp2 > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
                pStand.BasalAreaPerAcre = dTmp2 / (double)pStand.PlotsCollectionByStandsId.Count;
            else
                pStand.BasalAreaPerAcre = 0;
            if (pStand.BasalAreaPerAcre != null && pStand.TreesPerAcre != null)
            {
                if (pStand.BasalAreaPerAcre > 0 && pStand.TreesPerAcre > 0)
                    pStand.QmDbh = Math.Sqrt((double)pStand.BasalAreaPerAcre / ((double)pStand.TreesPerAcre * 0.005454154));
            }

            if (dTotalBdFtGross > 0)
                pStand.TotalBdFtGross = dTotalBdFtGross;
            else
                pStand.TotalBdFtGross = 0;
            if (dTotalBdFtNet > 0)
                pStand.TotalBdFtNet = dTotalBdFtNet;
            else
                pStand.TotalBdFtNet = 0;
            if (dTotalCuFtGross > 0)
                pStand.TotalCuFtGross = dTotalCuFtGross;
            else
                pStand.TotalCuFtGross = 0;
            if (dTotalCuFtNet > 0)
                pStand.TotalCuFtNet = dTotalCuFtNet;
            else
                pStand.TotalCuFtNet = 0;

            if (dTonPerAcre > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
            {
                pStand.TonsPerAcre = (double)dTonPerAcre;
                pStand.TotalTons = dTonPerAcre * pStand.NetGeographicAcres;
            }
            else
            {
                pStand.TonsPerAcre = 0;
                pStand.TotalTons = 0;
            }

            //if (dTotalCuFtMerch > 0)
            //    pStand.TotalCuFtMerch = dTotalCuFtMerch;
            //else
            //    pStand.TotalCuFtMerch = 0;

            if (pStand.TotalCuFtNet > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
                pStand.TotalCcf = ((pStand.TotalCuFtNet / (double)pStand.PlotsCollectionByStandsId.Count) * (double)pStand.NetGeographicAcres) / 100;
            else
                pStand.TotalCcf = 0;

            if (pStand.TotalBdFtNet > 0 && pStand.PlotsCollectionByStandsId.Count > 0)
                pStand.TotalMbf = ((pStand.TotalBdFtNet / (double)pStand.PlotsCollectionByStandsId.Count) * (double)pStand.NetGeographicAcres) / 1000;
            else
                pStand.TotalMbf = 0;

            if (pStand.NetGeographicAcres > 0)
            {
                if (pStand.TotalMbf > 0)
                    pStand.MbfPerAcre = pStand.TotalMbf / pStand.NetGeographicAcres; // needs TotalMbf
                else
                    pStand.MbfPerAcre = 0; // needs TotalMbf
                if (pStand.TotalCcf > 0)
                    pStand.CcfPerAcre = (double)pStand.TotalCcf / (double)pStand.NetGeographicAcres;
                else
                    pStand.CcfPerAcre = 0;
            }

            // Major Species
            string sql = string.Format("SELECT Species, SUM(ScribnerGrossVolume) AS TotalGross FROM Treesegment WHERE StandsId = {0} GROUP BY Species ORDER BY TotalGross Desc", pStand.StandsId);

            TreesegmentsCollection sc = new TreesegmentsCollection();
            DataTable tblSpc = sc.CustomFilLdataTableText(sql);
            if (tblSpc.Rows.Count > 0)
                pStand.MajorSpecies = tblSpc.Rows[0][0].ToString();
            tblSpc.Dispose();

            if (pStand.MajorSpecies == null || string.IsNullOrEmpty(pStand.MajorSpecies.Trim()))
            {
                sql = string.Format("SELECT SpeciesAbbreviation, SUM(TreesPerAcre) AS TotalTPA FROM Tree WHERE StandsId = {0} GROUP BY SpeciesAbbreviation ORDER BY TotalTPA Desc", pStand.StandsId);

                TreesCollection tc = new TreesCollection();
                tblSpc = tc.CustomFilLdataTableText(sql);
                if (tblSpc.Rows.Count > 0)
                    pStand.MajorSpecies = tblSpc.Rows[0][0].ToString();
                tblSpc.Dispose();
            }

            TreesCollection cc = new TreesCollection();
            sql = string.Format("SELECT AgeCode, TreesPerAcre FROM Tree WHERE StandsId = {0} ORDER BY TreesPerAcre Desc", pStand.StandsId);
            DataTable tblAge = cc.CustomFilLdataTableText(sql);
            //cc.Query.Select(cc.Query.AgeCode, cc.Query.TreesPerAcre).Where(cc.Query.StandsId == pStand.StandsId).OrderBy(cc.Query.TreesPerAcre.Descending);
            //if (cc.Query.Load())
            //    pStand.MajorAge = (short)GetAge(pStand, (Trees)cc[0]);
            if (tblAge.Rows.Count > 0)
                pStand.MajorAge = (short)GetAge(pStand, tblAge.Rows[0][0].ToString());
            tblAge.Dispose();

            // SiteIndex
            if (pStand.Species11.Trim() == pStand.MajorSpecies || pStand.Species12.Trim() == pStand.MajorSpecies || pStand.Species13.Trim() == pStand.MajorSpecies)
                pStand.SiteIndex = pStand.SiteIndex1;
            else
                if (pStand.Species21.Trim() == pStand.MajorSpecies || pStand.Species22.Trim() == pStand.MajorSpecies || pStand.Species23.Trim() == pStand.MajorSpecies)
                    pStand.SiteIndex = pStand.SiteIndex2;
                else
                    if (pStand.Species31.Trim() == pStand.MajorSpecies || pStand.Species32.Trim() == pStand.MajorSpecies || pStand.Species33.Trim() == pStand.MajorSpecies)
                        pStand.SiteIndex = pStand.SiteIndex3;
                    else
                        if (pStand.Species41.Trim() == pStand.MajorSpecies || pStand.Species42.Trim() == pStand.MajorSpecies || pStand.Species43.Trim() == pStand.MajorSpecies)
                            pStand.SiteIndex = pStand.SiteIndex4;
            return true;
        }
        */
        public static short GetAge(string p)
        {
            byte age = Utils.ConvertToTiny(p);
            StandAge rec = projectContext.StandAges.FirstOrDefault(s => s.StandsId == mCurrentStand.StandsId && s.AgeCode == age);
            if (rec != null)
                return rec.Age;
            else
                return 50;
            //switch (p)
            //{
            //    case "1":
            //        return (short)mCurrentStand.Age1;
            //    case "2":
            //        return (short)mCurrentStand.Age2;
            //    case "3":
            //        return (short)mCurrentStand.Age3;
            //    case "4":
            //        return (short)mCurrentStand.Age4;
            //    case "5":
            //        return (short)mCurrentStand.Age5;
            //    case "6":
            //        return (short)mCurrentStand.Age6;
            //    case "7":
            //        return (short)mCurrentStand.Age7;
            //    case "8":
            //        return (short)mCurrentStand.Age8;
            //    case "9":
            //        return (short)mCurrentStand.Age9;
            //    default:
            //        return 50;
            //}
        }
        /*
        private string GetPlotFactor(Stands pStand, TreeObject pItem)
        {
            switch (pItem.PlotFactorInputCode)
            {
                case "B1":
                    return pStand.Baf1.ToString();
                case "B2":
                    return pStand.Baf2.ToString();
                case "B3":
                    return pStand.Baf3.ToString();
                case "B4":
                    return pStand.Baf4.ToString();
                case "B5":
                    return pStand.Baf5.ToString();

                case "S1":
                    return pStand.Strip1.ToString();
                case "S2":
                    return pStand.Strip2.ToString();
                case "S3":
                    return pStand.Strip3.ToString();
                case "S4":
                    return pStand.Strip4.ToString();
                case "S5":
                    return pStand.Strip5.ToString();

                case "R1":
                case "R2":
                case "R3":
                case "R4":
                case "R5":
                case "F1":
                case "F2":
                case "F3":
                case "F4":
                case "F5":
                case "A1":
                case "A2":
                case "A3":
                case "A4":
                case "A5":
                    return GetAreaValue(pStand, pItem.PlotFactorInputCode);
            }
            return string.Empty;
        }
        */

        //private string GetPlotFactor(Stand pStand, Tree pItem)
        //{
        //    switch (pItem.PlotFactorInput)
        //    {
        //        case "B1":
        //            return pStand.Baf1.ToString();
        //        case "B2":
        //            return pStand.Baf2.ToString();
        //        case "B3":
        //            return pStand.Baf3.ToString();
        //        case "B4":
        //            return pStand.Baf4.ToString();
        //        case "B5":
        //            return pStand.Baf5.ToString();

        //        case "S1":
        //            return pStand.StripBlowup1.ToString(); // was pStand.Strip1
        //        case "S2":
        //            return pStand.StripBlowup2.ToString();
        //        case "S3":
        //            return pStand.StripBlowup3.ToString();
        //        case "S4":
        //            return pStand.StripBlowup4.ToString();
        //        case "S5":
        //            return pStand.StripBlowup5.ToString();

        //        case "R1":
        //        case "R2":
        //        case "R3":
        //        case "R4":
        //        case "R5":
        //        case "F1":
        //        case "F2":
        //        case "F3":
        //        case "F4":
        //        case "F5":
        //        case "A1":
        //        case "A2":
        //        case "A3":
        //        case "A4":
        //        case "A5":
        //            return GetAreaValue(pStand, pItem.PlotFactorInput);
        //    }
        //    return string.Empty;
        //}

        private static string GetAreaValue(Stand pStand, string p)
        {
            if (pStand.AreaCode1 == "R1")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "R1")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "R1")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "R1")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "R1")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "R2")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "R2")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "R2")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "R2")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "R2")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "R3")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "R3")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "R3")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "R3")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "R3")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "R4")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "R4")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "R4")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "R4")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "R4")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "R5")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "R5")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "R5")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "R5")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "R5")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "F1")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "F1")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "F1")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "F1")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "F1")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "F2")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "F2")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "F2")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "F2")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "F2")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "F3")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "F3")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "F3")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "F3")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "F3")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "F4")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "F4")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "F4")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "F4")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "F4")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "F5")
                return pStand.AreaPlotRadius1.ToString();
            if (pStand.AreaCode2 == "F5")
                return pStand.AreaPlotRadius2.ToString();
            if (pStand.AreaCode3 == "F5")
                return pStand.AreaPlotRadius3.ToString();
            if (pStand.AreaCode4 == "F5")
                return pStand.AreaPlotRadius4.ToString();
            if (pStand.AreaCode5 == "F5")
                return pStand.AreaPlotRadius5.ToString();

            if (pStand.AreaCode1 == "A1")
                return pStand.AreaBlowup1.ToString();
            if (pStand.AreaCode2 == "A1")
                return pStand.AreaBlowup2.ToString();
            if (pStand.AreaCode3 == "A1")
                return pStand.AreaBlowup3.ToString();
            if (pStand.AreaCode4 == "A1")
                return pStand.AreaBlowup4.ToString();
            if (pStand.AreaCode5 == "A1")
                return pStand.AreaBlowup5.ToString();

            if (pStand.AreaCode1 == "A2")
                return pStand.AreaBlowup1.ToString();
            if (pStand.AreaCode2 == "A2")
                return pStand.AreaBlowup2.ToString();
            if (pStand.AreaCode3 == "A2")
                return pStand.AreaBlowup3.ToString();
            if (pStand.AreaCode4 == "A2")
                return pStand.AreaBlowup4.ToString();
            if (pStand.AreaCode5 == "A2")
                return pStand.AreaBlowup5.ToString();

            if (pStand.AreaCode1 == "A3")
                return pStand.AreaBlowup1.ToString();
            if (pStand.AreaCode2 == "A3")
                return pStand.AreaBlowup2.ToString();
            if (pStand.AreaCode3 == "A3")
                return pStand.AreaBlowup3.ToString();
            if (pStand.AreaCode4 == "A3")
                return pStand.AreaBlowup4.ToString();
            if (pStand.AreaCode5 == "A3")
                return pStand.AreaBlowup5.ToString();

            if (pStand.AreaCode1 == "A4")
                return pStand.AreaBlowup1.ToString();
            if (pStand.AreaCode2 == "A4")
                return pStand.AreaBlowup2.ToString();
            if (pStand.AreaCode3 == "A4")
                return pStand.AreaBlowup3.ToString();
            if (pStand.AreaCode4 == "A4")
                return pStand.AreaBlowup4.ToString();
            if (pStand.AreaCode5 == "A4")
                return pStand.AreaBlowup5.ToString();

            if (pStand.AreaCode1 == "A5")
                return pStand.AreaBlowup1.ToString();
            if (pStand.AreaCode2 == "A5")
                return pStand.AreaBlowup2.ToString();
            if (pStand.AreaCode3 == "A5")
                return pStand.AreaBlowup3.ToString();
            if (pStand.AreaCode4 == "A5")
                return pStand.AreaBlowup4.ToString();
            if (pStand.AreaCode5 == "A5")
                return pStand.AreaBlowup5.ToString();

            return "";
        }

        //private double GetWeightFromSpecies(TREE pTree)
        //{
        //    Species aRec = new Species(ProjectDataSource);
        //    aRec.Query.Where(aRec.Query.TableName == mCurrentProject.SpeciesTableName && aRec.Query.Abbreviation == pTree.SpeciesAbbreviation);
        //    if (aRec.Query.Load())
        //    {
        //        if (aRec.Lbs == null)
        //            return 0;
        //        else
        //            return (double)aRec.Lbs;
        //    }
        //    return 0;
        //}

        public static int CalcVolume(bool bErrors)
        {
            Init();

            //mCurrentStand = pStand;
            //mCurrentPlot = GetPlot(tree.PlotsId);
            LookupMasterValues();
            // Question
            //if (tree.RecordCode == "R")
            //{
            //    //trees_per_acre = Utils.ConvertToFloat(tree.TreeCount) * Utils.ConvertToFloat(masterRef);
            //    trees_per_acre = (Utils.ConvertToFloat(tree.TreeCount) * (float)13865.77) / (Utils.ConvertToFloat(masterRef) * Utils.ConvertToFloat(masterRef));
            //    basal_area_per_acre = (float)0.005454 * (float)Square(Utils.ConvertToFloat(tree.D4h)) * trees_per_acre;
            //    basal_area = basal_area_per_acre;
            //    trees_pa = trees_per_acre;
            //}
            LookupSpeciesValues();
            ComputeTreeAndBasalArea();
            if (masterAge > 0)
                ComputeASubo(masterAge);
            else
                ComputeASubo(40);
            ao = asubo;
            bark = bark_thickness;
            compute_stem();
            compute_log_adjustment_factor();
            process_each_segment();
            number_segments = seg_index;

            if (bErrors)
                DisplayCalcError(calc_error);
            return (calc_error);
        }

        public static void DisplayCalcError(int err)
        {
            int iSegOffSet = (calc_segment * 8) - 8;
            //switch (err)
            //{
            //    case 1:
            //        this.seg[calc_segment - 1].comments = "Under Min Dia On Seg";
            //        MessageBox.Show("Under Min Dia On Seg = " + calc_segment);
            //        break;
            //    case 2:
            //        this.seg[calc_segment - 1].comments = "Under Min Len On Seg";
            //        MessageBox.Show("Under Min Len On Seg = " + calc_segment);
            //        break;
            //    case 3:
            //        this.seg[calc_segment - 1].comments = "Under Min Len For Grade On Seg";
            //        MessageBox.Show("Under Min Len For Grade On Seg = " + calc_segment);
            //        break;
            //    case 4:
            //        this.seg[calc_segment - 1].comments = "Under Min Dia For Grade On Seg";
            //        MessageBox.Show("Under Min Dia For Grade On Seg = " + calc_segment);
            //        break;
            //    case 5:
            //        this.seg[calc_segment - 1].comments = "Under Min Len For Sort On Seg";
            //        MessageBox.Show("Under Min Len For Sort On Seg = " + calc_segment);
            //        break;
            //    case 6:
            //        this.seg[calc_segment - 1].comments = "Under Min Dia For Sort On Seg";
            //        MessageBox.Show("Under Min Dia For Sort On Seg = " + calc_segment);
            //        break;
            //    case 7:
            //        this.seg[calc_segment - 1].comments = "Over Max Len On Seg";
            //        MessageBox.Show("Over Max Len On Seg =" + calc_segment);
            //        break;
            //    case 8:
            //        this.seg[calc_segment - 1].comments = "Over Max Butt Dia For Grade On Seg";
            //        MessageBox.Show("Over Max Butt Dia For Grade On Seg = " + calc_segment);
            //        break;
            //    case 9:
            //        this.seg[calc_segment - 1].comments = "Over Max Butt Dia For Sort On Seg";
            //        MessageBox.Show("Over Max Butt Dia For Sort On Seg =" + calc_segment);
            //        break;
            //    case 10:
            //        this.seg[calc_segment - 1].comments = "Sort/Grade is Invalid For Seg";
            //        MessageBox.Show("Sort/Grade is Invalid For Seg = " + calc_segment);
            //        break;
            //    case 11:
            //        this.seg[calc_segment - 1].comments = "Under Min Dia For Seg";
            //        MessageBox.Show("Under Min Dia For Seg = " + calc_segment);
            //        break;
            //    case 12:
            //        this.seg[calc_segment - 1].comments = "Under Min Len For Seg";
            //        MessageBox.Show("Under Min Len For Seg = " + calc_segment);
            //        break;
            //}
        }

        private static void LookupMasterValues()
        {
            //Stands aRec = tree.UpToPlotsByPlotId.UpToStandsByStandsId;

            masterAge = GetAge(tree.AgeCode.ToString());

            if (!string.IsNullOrEmpty(tree.PlotFactorInput))
            {
                if (tree.PlotFactorInput[0] == 'S')
                    masterStripTree = ProjectBLL.GetPlotFactor(mCurrentStand, tree);
                if (tree.PlotFactorInput[0] == 'R')
                    masterFixedRadius = ProjectBLL.GetPlotFactor(mCurrentStand, tree);
                if (tree.PlotFactorInput[0] == 'F')
                    masterFixedRadius = ProjectBLL.GetPlotFactor(mCurrentStand, tree);
                if (tree.PlotFactorInput[0] == 'B')
                    masterBaf = ProjectBLL.GetPlotFactor(mCurrentStand, tree);
            }
            //masterDbhOpt = dr["D4hOpt"].ToString();
            masterAcres = mCurrentStand.NetGeographicAcres.ToString();
        }

        private static void LookupSpeciesValues()
        {
            spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, mCurrentProject.SpeciesTableName, tree.SpeciesAbbreviation);
            if (spcRec != null)
            {
                spcTrim = spcRec.Trim.ToString();
                spcMinTopDia = spcRec.MinDia.ToString();
                spcMinLogLen = spcRec.MinLen.ToString();
                spcMaxLogLen = spcRec.MaxLen.ToString();
                spcFF16 = spcRec.FormFactorsDisplayCode;
                spcBarkThickness = spcRec.BarkRatioValue.ToString();
                spcWeight = spcRec.Lbs.ToString();
                spcAo = spcRec.AsubODisplayCode;
                bark_thickness = (float)spcRec.BarkRatioValue; // Utils.ConvertToFloat(spcBarkThickness);

                Asubo oRec = ProjectBLL.GetASubo(ref projectContext, spcRec.AsubOTableName, spcRec.AsubODisplayCode);
                //oRec.Query.Where(oRec.Query.TableName == aRec[0].AsubOTableName && oRec.Query.DisplayCode == aRec[0].AsubODisplayCode);
                if (oRec != null)
                {
                    aoSpecies = oRec.DisplayCode;
                    ao_a = (double)oRec.Avalue;
                    ao_b = (double)oRec.Bvalue;
                    ao_c = (double)oRec.Cvalue;
                }

                //Boardfootrule bRec = ProjectBLL.GetBdFtRule(projectDataSource, aRec.BoardFootRuleTableName, aRec.BdFtDisplayCode);
                //if (bRec != null)
                //{
                //    switch (bRec.InputCode)
                //    {
                //        case "W":
                //            spcScribnerScale = "W";
                //            break;
                //        case "E":
                //            spcScribnerScale = "E";
                //            break;
                //        case "I":
                //            spcScribnerScale = "I";
                //            break;
                //    }
                //}
                spcScribnerScale = spcRec.BdFtDisplayCode;
                //Cubicfootrule cRec = ProjectBLL.GetCuFtRule(projectDataSource, aRec.CubicFootRuleTableName, aRec.CubicFootRuleTableName);
                //if (cRec != null)
                //{
                //    switch (cRec.CubicVolumeFormula)
                //    {
                //        case "NorthWest":
                //            spcCubicScale = "1";
                //            break;
                //        case "Smalian":
                //            spcCubicScale = "S";
                //            break;
                //    }
                //}
                spcCubicScale = spcRec.CuFtDisplayCode;
            }
        }

        private static void LookupSortValues(string sTmp)
        {
            string table = mCurrentProject.SortsTableName;
            Sort aRec = ProjectBLL.GetSort(ref projectContext, table, sTmp, spcRec.SpeciesGroup);
            if (aRec != null)
            {
                srtFiber = string.Empty; // aRec.Fiber;
                srtMinDia = aRec.MinimumDiameter.ToString();
                srtMinLen = aRec.MinimumLength.ToString();
                srtMinVol = aRec.MinimumBdftPerLog.ToString();
                srtMaxButt = aRec.MaximumDiameter.ToString();
            }
        }

        private static void LookupGradeValues(string sTmp)
        {
            string table = mCurrentProject.GradesTableName;
            Grade aRec = ProjectBLL.GetGrade(ref projectContext, table, sTmp, spcRec.SpeciesGroup);
            if (aRec != null)
            {
                grdFiber = string.Empty; // aRec.Fiber;
                grdMinDia = aRec.MinimumDiameter.ToString();
                grdMinLen = aRec.MinimumLength.ToString();
                grdMinVol = aRec.MinimumBdftPerLog.ToString();
                grdMaxButt = aRec.MaximumDiameter.ToString();
            }
        }

        private static void process_log()
        {
            float bump_log_length;
            seg_length = seg_log_length[seg_log_index];
            seg_top_height = seg_butt_height + seg_log_length[seg_log_index] +
                trim_allowed;
            seg_top_diameter = computed_diameter(seg_top_height);

            graded_logs = 0;
            graded_butt_dia = seg_butt_diameter;
            graded_top_dia = seg_top_diameter;

            if (west_routine == true)
            {
                taper_top = seg_top_diameter;
                taper_butt = seg_butt_diameter;
                compute_west_side_log_length();
            }
            else
            {
                //taper_top = (float)Math.Round(seg_top_diameter);
                //taper_butt = (float)Math.Round(seg_butt_diameter);
                taper_top = seg_top_diameter;
                taper_butt = seg_butt_diameter;
                compute_east_side_log_length();
            }

            seg_log_top[0] = taper_top;
            seg_log_butt[0] = taper_butt;
            if (log_max > 0)
            {
                switch (spcScribnerScale)
                {
                    case "W":
                    case "1":
                    case "D":
                        figure_west_taper();
                        break;
                    case "E":
                    case "O":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_east_taper_butt((int)taper_butt, (int)taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_east_taper_2nd();
                        break;
                    case "I":
                    case "T":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_idaho_taper_butt(taper_butt, taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_east_taper_2nd();
                        break;
                    case "B":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_bia_taper_butt(taper_butt, taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_bia_taper_2nd();
                        break;
                }
            }
            graded_butt_dia = seg_log_butt[0];
            graded_top_dia = seg_log_top[log_max - 1];

            //*    process   log     data     
            log_index = 0;
            bump_log_length = seg_butt_height;

            if (calc_error == 0)
            {
                if (dash_or_plus != 2)
                {
                    if (segment_sort == "0" || segment_grade == "0")
                        teList.Add(new TreeErrors((short)seg_index + 1, "", "Cull Segment"));
                    check_grade_len_top_butt(segment_grade, (int)Math.Round(seg_length), (int)Math.Round(seg_top_diameter), (int)Math.Round(seg_butt_diameter));
                    check_sort_len_top_butt(segment_sort, (int)Math.Round(seg_length), (int)Math.Round(seg_top_diameter), (int)Math.Round(seg_butt_diameter));
                    if (calc_error > 0)
                        calc_segment = cvol_index + 1;
                }
            }

            while (log_index < log_max)
            {
                CalcDiameters();
                log_length = log_log_length[log_index];
                log_top_diameter = seg_log_top[log_index];
                log_butt_diameter = seg_log_butt[log_index];

                if (log_index < log_max && spcCubicScale == "2")  // sweyco
                {
                    bump_log_length += log_length;
                    log_top_diameter = (float)Math.Round(computed_diameter(bump_log_length));
                }

                log_scribner = 0;
                log_net_scribner = 0;
                log_cubic = 0;
                log_net_cubic = 0;
                log_merch_cubic = 0;

                figure_scribner();

                if ((log_index + 1) == log_max)
                    figure_cubic();

                //*  create graded record   
                if (log_max > 0)
                    graded_logs =
                        graded_logs + segment_whole_half[seg_index] / log_max;
                log_index++;
            }
        }

        private static int IdahoTaper(int dLen)
        {
            int tFeet = 0, xq = 0, xr = 0;


            if (tree.SpeciesAbbreviation == "L" ||
                tree.SpeciesAbbreviation == "LP")
            {
                if (dLen > 20 && dLen < 49)
                    tFeet = log_max;
                if (dLen > 48 && dLen < 61)
                    tFeet = (log_max - 1) + 2;
            }
            else
                if (tree.SpeciesAbbreviation == "RC")
                {
                    if (dLen > 20 && dLen < 41)
                        tFeet = log_max * 2;
                }
                else
                {
                    if (dLen > 20 && dLen < 41)
                    {
                        xq = DivRem((int)seg_top_diameter, 2, out xr);
                        if (xr == 0)
                            tFeet = log_max * 2;
                        else
                            tFeet = log_max;
                    }
                }
            return tFeet;
        }

        public static int DefaultTaper(int dLen)
        {
            int tFeet = 0;

            string strSpecies = "**";
            if (tree.SpeciesAbbreviation == "L")
                strSpecies = tree.SpeciesAbbreviation;
            tFeet = GetTaper(strSpecies, dLen);
            return tFeet;
        }

        private static int GetTaper(string strSpecies, int iLen)
        {
            if (strSpecies == "**")
            {
                if (iLen >= 21 && iLen <= 31)
                    return (2);
                if (iLen >= 32 && iLen <= 46)
                    return (4);
                if (iLen >= 47 && iLen <= 54)
                    return (6);
                if (iLen >= 55 && iLen <= 64)
                    return (7);
                if (iLen >= 65 && iLen <= 72)
                    return (8);
                if (iLen >= 73 && iLen <= 80)
                    return (9);
                if (iLen >= 81 && iLen <= 88)
                    return (10);
                if (iLen >= 89 && iLen <= 99)
                    return (11);
            }
            if (strSpecies == "L")
            {
                if (iLen >= 21 && iLen <= 41)
                    return (2);
                if (iLen >= 42 && iLen <= 50)
                    return (3);
                if (iLen >= 51 && iLen <= 60)
                    return (4);
                if (iLen >= 61 && iLen <= 80)
                    return (5);
            }
            if (strSpecies == "LP")
            {
                if (iLen >= 21 && iLen <= 41)
                    return (2);
                if (iLen >= 42 && iLen <= 50)
                    return (3);
                if (iLen >= 51 && iLen <= 60)
                    return (4);
            }
            return (0);
        }

        private static void CalcDiameters()
        {
            int TaperRemain = 0, TaperLen = 0, ActualTaper = 0;
            int Dia2Actual = 0, Dia3Actual = 0;
            int iSegs = 0;
            int wGrDia1 = 0, wGrDia2 = 0;
            int wNtDia1 = 0, wNtDia2 = 0;
            int tGrDia1 = 0, tGrDia2 = 0, tGrDia3 = 0;
            int tGrLen1 = 0, tGrLen2 = 0, tGrLen3 = 0;
            int tDia, tLen, tButt;
            int xq, xr, yq, yr;

            //tButt = (int)seg_butt_diameter;
            tButt = 0;
            tLen = (int)seg_length;
            iSegs = log_max;

            if (spcScribnerScale == "W")
            {
                tDia = (int)seg_top_diameter;
                tGrLen1 = (int)log_log_length[0];
                tGrLen2 = (int)log_log_length[1];
                tGrLen3 = (int)log_log_length[2];
            }
            else
            {
                tDia = (int)Math.Round(seg_top_diameter);
                switch (log_max)
                {
                    case 1:
                        tGrLen1 = (int)log_log_length[0];
                        break;
                    case 2:
                        tGrLen1 = (int)log_log_length[1];
                        tGrLen2 = (int)log_log_length[0];
                        break;
                    case 3:
                        tGrLen1 = (int)log_log_length[2];
                        tGrLen2 = (int)log_log_length[1];
                        tGrLen3 = (int)log_log_length[0];
                        break;

                }
            }

            if (tDia < 1)
                return;

            // if no butt diameter use default taper
            if (tButt == 0)
            {
                if (spcScribnerScale == "W")
                    TaperLen = 10;
                if (spcScribnerScale == "E")
                {
                    if (tLen > 20)
                    {
                        TaperLen = DefaultTaper(tLen);
                    }
                }
                if (spcScribnerScale == "I")
                {
                    if (tLen > 20)
                    {
                        TaperLen = IdahoTaper(tLen);
                    }
                }
            }

            //* ----- calculate actual taper for segments ----- 
            if (tButt > 0 || spcScribnerScale == "E")
            {
                if (spcScribnerScale == "E" && tButt == 0)
                    ActualTaper = TaperLen;
                if (spcScribnerScale == "E" && tButt > 0)
                    ActualTaper = tButt - tDia;
                if (spcScribnerScale == "W")
                    ActualTaper = tButt - tDia;
                if (spcScribnerScale == "D")
                    ActualTaper = tButt - tDia;
                TaperRemain = ActualTaper;
                xq = DivRem(ActualTaper, iSegs, out xr);
                //* ----- distribution of even taper -----
                if (xr == 0)
                {
                    if (iSegs == 2)
                    {
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia2Actual = xq;
                    }
                    if (iSegs == 3)
                    {
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia2Actual = xq;
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia3Actual = xq;
                    }
                }
                /* ----- distribution of uneven taper ----- */
                if (xr > 0)
                {
                    ActualTaper++;
                    TaperRemain = ActualTaper;
                    if (iSegs == 2)
                    {
                        yq = DivRem(ActualTaper, iSegs, out yr);
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia2Actual = yq;
                    }
                    if (iSegs == 3)
                    {
                        yq = DivRem(ActualTaper, iSegs, out yr);
                        if (yr > 0)
                            yq++;
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia2Actual = yq;
                        yq = DivRem(TaperRemain, 2, out yr);
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia3Actual = yq;
                    }
                }
            }
            /* ----- segment 1 ----- */
            tGrDia1 = tDia;
            /* ----- segment 2 ----- */
            if (iSegs > 1)
            {
                tGrDia2 = tDia;
                if (tButt == 0 && spcScribnerScale == "W")
                {
                    xq = DivRem(tGrLen1, TaperLen, out xr);
                    tGrDia2 = tGrDia2 + xq;
                }
                if (tButt == 0 && spcScribnerScale == "D")
                {
                    xq = DivRem(tGrLen1, TaperLen, out xr);
                    tGrDia2 = tGrDia2 + xq;
                }
                if (tButt > 0 || spcScribnerScale == "E")
                {
                    tGrDia2 = tGrDia2 + Dia2Actual;
                }
            }
            /* ----- segment 3 ----- */
            if (iSegs > 2)
            {
                tGrDia3 = tDia;
                if (tButt == 0 && spcScribnerScale == "W")
                {
                    xq = DivRem(tGrLen1 + tGrLen2, TaperLen, out xr);
                    tGrDia3 = tGrDia3 + xq;
                }
                if (tButt == 0 && spcScribnerScale == "D")
                {
                    xq = DivRem(tGrLen1 + tGrLen2, TaperLen, out xr);
                    tGrDia3 = tGrDia3 + xq;
                }
                if (tButt > 0 || spcScribnerScale == "E")
                {
                    tGrDia3 = tGrDia3 + Dia2Actual;
                    tGrDia3 = tGrDia3 + Dia3Actual;
                }
            }
            if (spcScribnerScale == "W")
            {
                /* ----- segment 1 net ----- */
                seg_log_top[0] = tGrDia1;
                /* ----- segment 2 net ----- */
                if (iSegs > 1)
                {
                    seg_log_top[1] = tGrDia2;
                }
                /* ----- segment 3 net ----- */
                if (iSegs > 2)
                {
                    seg_log_top[2] = tGrDia3;
                }
            }
            else
            {
                switch (log_max)
                {
                    case 1:
                        seg_log_top[0] = tGrDia1;
                        break;
                    case 2:
                        seg_log_top[1] = tGrDia1;
                        seg_log_top[0] = tGrDia2;
                        break;
                    case 3:
                        seg_log_top[2] = tGrDia1;
                        seg_log_top[1] = tGrDia2;
                        seg_log_top[0] = tGrDia3;
                        break;
                }
            }
        }

        private static void ComputeASubo(int tage)
        {
            double ttage;
            string[] ao_species = new string[9];
            double[] ao_a = new double[9];
            double[] ao_b = new double[9];
            double[] ao_c = new double[9];
            ao_species[0] = "DF";
            ao_a[0] = 0.64620566960;
            ao_b[0] = -0.0003666245662;
            ao_c[0] = -6.15389545900;
            ao_species[1] = "LP";
            ao_a[1] = 0.64620566960;
            ao_b[1] = -0.0003666245662;
            ao_c[1] = -6.15389545900;
            ao_species[2] = "WH";
            ao_a[2] = 0.69429326230;
            ao_b[2] = -0.0005374096360;
            ao_c[2] = -6.75006589600;
            ao_species[3] = "RA";
            ao_a[3] = 0.68043852280;
            ao_b[3] = -0.0007233655182;
            ao_c[3] = -4.95080564100;
            ao_species[4] = "PP";
            ao_a[4] = 0.58134710670;
            ao_b[4] = 0.0003879441574;
            ao_c[4] = -11.14531440000;
            ao_species[5] = "SS";
            ao_a[5] = 0.59176427320;
            ao_b[5] = -0.0002486198821;
            ao_c[5] = -4.43475477700;
            ao_species[6] = "SF";
            ao_a[6] = 0.59176427320;
            ao_b[6] = -0.0002486198821;
            ao_c[6] = -4.43475477700;
            ao_species[7] = "RC";
            ao_a[7] = 0.59176427320;
            ao_b[7] = -0.0002486198821;
            ao_c[7] = -4.43475477700;
            ao_species[8] = "NF";
            ao_a[8] = 0.89086009290;
            ao_b[8] = -0.0004617957011;
            ao_c[8] = -18.89577825000;

            asubo = Utils.ConvertToFloat(spcAo);
            if (asubo == 0)
            {
                ttage = (double)masterAge;
                if (ttage < 30)
                    ttage = 30;
                for (int i = 0; i < 9; i++)
                {
                    if (spcAo == ao_species[i])
                    {
                        double dTmp = ao_a[i] + (ao_b[i] * ttage) + (ao_c[i] * (1.00 / ttage));
                        spcAo = dTmp.ToString();
                        asubo = (float)dTmp;
                        return;
                    }
                }
                //double dTmp = ao_a + (ao_b * ttage) + (ao_c * (1.00 / ttage));
                //spcAo = dTmp.ToString(); // Math.Round((decimal)dTmp, 3).ToString();
                //ao = (float)dTmp; // Math.Round((decimal)dTmp, 3);
                //asubo = (float)dTmp;
            }
        }

        //public float ComputeASubo(int tage, string pSpeciesTable, string pSpecies)
        //{
        //    double ttage;
        //    float f;
        //    Species sRec;

        //    try
        //    {
        //        // Get species record
        //        sRec = projectContext.Species.First(s => s.TableName == pSpeciesTable && s.Abbreviation == pSpecies);
        //        // if Asubo is entered - return value
        //        if (float.TryParse(sRec.AsubODisplayCode, out f))
        //        {
        //            return f;
        //        }
        //        else
        //        {
        //            // get the values from Asubo table and calc
        //            Asubo oRec = projectContext.Asubos.First(a => a.TableName == sRec.AsubOTableName && a.DisplayCode == sRec.AsubODisplayCode);
        //            if (oRec != null)
        //            {
        //                aoSpecies = oRec.DisplayCode;
        //                ao_a = (double)oRec.Avalue;
        //                ao_b = (double)oRec.Bvalue;
        //                ao_c = (double)oRec.Cvalue;
        //            }

        //            ttage = (double)tage;
        //            if (ttage < 30)
        //                ttage = 30;
        //            double dTmp = ao_a + (ao_b * ttage) + (ao_c * (1.00 / ttage));
        //            spcAo = Math.Round((decimal)dTmp, 3).ToString();
        //            ao = (float)Math.Round((decimal)dTmp, 3);
        //            return (float)dTmp;
        //        }
        //    }
        //    catch (InvalidOperationException ex)
        //    {
        //        MessageBox.Show("Species not found.", "Error");
        //        return 0;
        //    }
        //    return 0;
        //}

        public static double ComputeBarkFactor(double pHeight)
        {
            return ((0.950414 - 0.000113) * pHeight);
        }

        //private double ComputeASubo(int tage, string pSpecies)
        //{
        //    double retAO = 0;
        //    double ttage;
        //    string[] ao_species = new string[9];
        //    double[] ao_a = new double[9];
        //    double[] ao_b = new double[9];
        //    double[] ao_c = new double[9];
        //    ao_species[0] = "DF";
        //    ao_a[0] = 0.64620566960;
        //    ao_b[0] = -0.0003666245662;
        //    ao_c[0] = -6.15389545900;
        //    ao_species[1] = "LP";
        //    ao_a[1] = 0.64620566960;
        //    ao_b[1] = -0.0003666245662;
        //    ao_c[1] = -6.15389545900;
        //    ao_species[2] = "WH";
        //    ao_a[2] = 0.69429326230;
        //    ao_b[2] = -0.0005374096360;
        //    ao_c[2] = -6.75006589600;
        //    ao_species[3] = "RA";
        //    ao_a[3] = 0.68043852280;
        //    ao_b[3] = -0.0007233655182;
        //    ao_c[3] = -4.95080564100;
        //    ao_species[4] = "PP";
        //    ao_a[4] = 0.58134710670;
        //    ao_b[4] = 0.0003879441574;
        //    ao_c[4] = -11.14531440000;
        //    ao_species[5] = "SS";
        //    ao_a[5] = 0.59176427320;
        //    ao_b[5] = -0.0002486198821;
        //    ao_c[5] = -4.43475477700;
        //    ao_species[6] = "SF";
        //    ao_a[6] = 0.59176427320;
        //    ao_b[6] = -0.0002486198821;
        //    ao_c[6] = -4.43475477700;
        //    ao_species[7] = "RC";
        //    ao_a[7] = 0.59176427320;
        //    ao_b[7] = -0.0002486198821;
        //    ao_c[7] = -4.43475477700;
        //    ao_species[8] = "NF";
        //    ao_a[8] = 0.89086009290;
        //    ao_b[8] = -0.0004617957011;
        //    ao_c[8] = -18.89577825000;

        //    ttage = (double)masterAge;
        //    if (ttage < 30)
        //        ttage = 30;
        //    for (int i = 0; i < 9; i++)
        //    {
        //        if (pSpecies == ao_species[i])
        //        {
        //            double dTmp = ao_a[i] + (ao_b[i] * ttage) + (ao_c[i] * (1.00 / ttage));
        //            retAO = (float)dTmp;
        //        }
        //    }
        //    return retAO;
        //}

        private static void ComputeTreeAndBasalArea()
        {
            float save_form_factor;
            int treeCount = (int)tree.TreeCount;

            //if (tree.BoleHeight == null || tree.Dbh == null || tree.ReportedFormFactor == null || tree.FormPoint == null)
            //    return;
            if (tree.BoleHeight == null || tree.Dbh == null)
                return;
            if (tree.ReportedFormFactor == null)
                tree.ReportedFormFactor = 0;
            if (tree.FormPoint == null)
                tree.FormPoint = 0;

            save_form_factor = 0.0f;
            if (spcScribnerScale == "W" || spcScribnerScale == "1")
                west_routine = true;

            c_bole_length = (int)tree.BoleHeight;
            c_d4h = (float)tree.Dbh;
            c_form_factor = (float)tree.ReportedFormFactor / 100; // *(float)0.01
            c_form_point = (int)tree.FormPoint;

            if (masterDbhOpt == "Y")
                c_d4h = c_d4h / c_form_factor;

            if (c_form_point == 4 && c_form_factor == 0)
                c_form_factor = Utils.ConvertToFloat(spcFF16);
            if (c_form_point == 4)
            {
                save_form_factor = c_form_factor;
                c_form_factor = 1.00f;
                c_form_point = 16;
            }

            //sub = (sg.Cells[sg.ActiveRowIndex, 1].Text[1] - 49);

            ////if (tree.UpToPlotsByPlotId.UserPlotNumber[0] == '-')
            //if (tree.UpToPlotsByPlotId.UserPlotNumber[0] == '-')
            //{
            //    basal_area_per_acre = (float)0.005454 * (float)Square(c_d4h) * trees_per_acre;
            //    basal_area = basal_area_per_acre;
            //    if (Utils.ConvertToFloat(masterAcres) > 0)
            //        trees_per_acre = (float)1.0 / Utils.ConvertToFloat(masterAcres);
            //    else
            //        trees_per_acre = 0;
            //    trees_pa = trees_per_acre;
            //    if (save_form_factor > 0)
            //        c_form_factor = save_form_factor;
            //    return;
            //}

            switch (tree.PlotFactorInput[0])
            {
                case '0':
                    if (Utils.ConvertToFloat(masterAcres) > 0)
                        trees_per_acre = ((float)1 / Utils.ConvertToFloat(masterAcres)) * treeCount;
                    else
                        trees_per_acre = 0;
                    basal_area_per_acre = ((float)0.005454 * (float)Square(c_d4h) * trees_per_acre) * treeCount;
                    break;
                case 'S':
                    if (Utils.ConvertToFloat(masterStripTree) > 0)
                    {
                        if (Utils.ConvertToFloat(masterAcres) > 0)
                            trees_per_acre = Utils.ConvertToFloat(masterStripTree) * treeCount;
                        else
                            trees_per_acre = 0;
                        basal_area_per_acre = ((float) 0.005454 * (float) Square(c_d4h) * trees_per_acre) * treeCount;
                    }
                    else
                    {
                        trees_per_acre = 0;
                        basal_area_per_acre = 0;
                    }
                    break;
                case 'F':
                    if (Utils.ConvertToFloat(masterFixedRadius) > 0)
                        trees_per_acre = Utils.ConvertToFloat(masterFixedRadius) * treeCount;
                    else
                        trees_per_acre = 0;
                    basal_area_per_acre = (Utils.ConvertToFloat(masterFixedRadius) * (float)Square(c_d4h) * ((float)Math.PI / 576)) * treeCount; //(float)0.005454 * (float)Square(c_d4h) * trees_per_acre;
                    break;
                case 'R':
                    if (Utils.ConvertToFloat(masterFixedRadius) > 0)
                    {
                        float paa = (float)Square(Utils.ConvertToFloat(masterFixedRadius))/13865;
                        float ste = 1 / paa;
                        trees_per_acre = ste * treeCount; // (float) 99.9188 * treeCount;
                    }
                    else
                        trees_per_acre = 0;
                    if (c_d4h > 0)
                        basal_area_per_acre = ((float)Square(c_d4h) * trees_per_acre * (float)0.005454154) * treeCount;
                    else
                    {
                        basal_area_per_acre = 0;
                    }
                    break;
                default:
                    if (tree.PlotFactorInput[0] == 'B')
                        c_baf = Utils.ConvertToFloat(masterBaf);
                    else
                        c_baf = Utils.ConvertToFloat(tree.PlotFactorInput);
                    basal_area_per_acre = 0;
                    if (c_d4h == 4.0)
                    {
                        basal_area_per_acre = c_baf * (float)treeCount;
                    }
                    else
                    {
                        if (c_form_factor > 0)
                        {
                            float sq = c_form_factor * c_form_factor;
                            float t = (c_baf * (float)treeCount) / (float)Math.Round((decimal)sq, 4);
                            basal_area_per_acre = (float)Math.Round((decimal)t, 7);
                        }
                    }
                    trees_per_acre = 0;
                    if (c_d4h > 0 && c_form_factor > 0)
                    {
                        float x = (float)(basal_area_per_acre / (0.005454154 * (Square(c_d4h))));
                        trees_per_acre = (float)Math.Round((decimal)x, 7);
                    }
                    break;
            }
            basal_area = basal_area_per_acre;
            trees_pa = trees_per_acre;
            if (save_form_factor > 0)
                c_form_factor = save_form_factor;
        }

        private static Plot GetPlot(int? p)
        {
            Plot aRec = projectContext.Plots.FirstOrDefault(plot => plot.PlotsId == p);
            //BusinessObjects.Plots aRec = new Plots(ProjectDataSource);
            //aRec.Query.Where(aRec.Query.PlotsId == p);
            //aRec.Query.Load();
            return (aRec);
        }

        private static void compute_stem()
        {
            int modified_bole_length = c_bole_length;
            int modified_form_point = c_form_point;
            int detail_bole_len = c_bole_length;

            dib_at_d4h = c_d4h * bark_thickness;
            dib_at_form_point = dib_at_d4h * c_form_factor;

            if (!string.IsNullOrEmpty(tree.TopDiameterFractionCode))
            {
                if (tree.TopDiameterFractionCode[0] >= 'A' && tree.TopDiameterFractionCode[0] <= 'Z')
                {
                    diameter_top = tree.TopDiameterFractionCode[0] - 64;
                    diameter_top *= bark_thickness;
                }
                if (Utils.IsDigit(tree.TopDiameterFractionCode[0]))
                    diameter_top = dib_at_form_point * (float)(tree.TopDiameterFractionCode[0] - 48) * (float)0.1;
                if (tree.TopDiameterFractionCode[0] == '-')
                {
                    if (c_d4h < 15)
                        diameter_top = 5.0f;
                    else
                    {
                        diameter_top = (float)2.24 + (float)0.184 * c_d4h;
                        diameter_top *= bark_thickness;
                    }
                }

                if (tree.TopDiameterFractionCode[0] == '+')
                {
                    if (detail_bole_len <= 14)
                    {
                        detail_bole_len += 15;
                        c_bole_length = detail_bole_len;
                    }
                    diameter_top = predict_top_diam(c_form_factor * 100.0f * bark_thickness,
                        c_d4h, c_bole_length / 10.0f);
                    detail_bole_len = detail_bole_len * 16 / 10;
                    modified_bole_length = detail_bole_len;
                    c_bole_length = detail_bole_len;
                }
            }

            //* lsubo is length from form point to tip of tree 
            if (modified_bole_length == modified_form_point)
                modified_bole_length++;

            lsubo = 0;
            if (dib_at_form_point == diameter_top)
                lsubo = 1.0f;
            else
                lsubo = (modified_bole_length - modified_form_point) *
                    (dib_at_form_point - diameter_top * asubo) /
                    (dib_at_form_point - diameter_top);

            total_tree_height = lsubo + modified_form_point;
            if (total_tree_height > 365)
                total_tree_height = c_bole_length;
            if (total_tree_height > (4 * c_bole_length))
                total_tree_height = c_bole_length;
            slope_calc = dib_at_form_point * (asubo - 1.0f) / lsubo;

            if (modified_form_point == 4)
                modified_form_point++;

            c_a = 0;
            c_a = ((modified_form_point - 4) * slope_calc +
                dib_at_d4h - dib_at_form_point) / ((float)Square((modified_form_point - 4f)));

            if (c_a > 0.0)
            {
                c_b = slope_calc - 2.0f * modified_form_point * c_a;
                c_c = dib_at_d4h - 4.0f * c_b - 16.0f * c_a;
            }
            else
            {
                c_a = 0;
                c_b = (dib_at_form_point - dib_at_d4h) / (modified_form_point - 4.0f);
                c_c = dib_at_d4h - 4.0f * c_b;
            }

            //* make diameter limit for logs  
            merchantable_diameter = Utils.ConvertToFloat(spcMinTopDia);

            if (diameter_top > Utils.ConvertToFloat(spcMinTopDia))
                merchantable_diameter = diameter_top;

            mtd_height = computed_height(Utils.ConvertToFloat(spcMinTopDia));
            merchantable_height = computed_height(merchantable_diameter);
            national_diameter = computed_diameter(4.0f);
            sweyco_diameter = computed_diameter(1.5f);
            sweyco_diameter_rounded = (float)Math.Round(sweyco_diameter);
            if (west_routine == true)
                national_diameter_rounded = national_diameter;
            else
                national_diameter_rounded = (float)Math.Round(national_diameter);
        }

        private static float predict_top_diam(float d3_fc, float d4_d4h, float d5_ls)
        {
            float slope_const = 0.00001244f;
            float slope_intercept = 0.0010754f;
            float c7_slope, c8_intercept, c11_slope, c12_intercept, c15, td;

            c7_slope = 0.001045f - (d3_fc - 65) * slope_const;
            c8_intercept = 0.094013f - (d3_fc - 65) * slope_intercept;
            c11_slope = 2.0f * (d5_ls - 1.0f) * c7_slope;
            c12_intercept = 1.0f - 2.0f * (d5_ls - 1.0f) * c8_intercept;
            c15 = c11_slope * d4_d4h + c12_intercept;
            td = c15 * d3_fc * d4_d4h * 0.01f;
            return (td);
        }

        private static void process_each_segment()
        {
            int x;
            int iCol = 22;

            seg_index = 0;
            seg_top_height = 0;
            seg_top_diameter = 0;
            seg_butt_diameter = 0;
            cvol_index = 0;

            while (seg_index < 12)
            {
                switch (seg_index)
                {
                    case 0:
                        if (string.IsNullOrEmpty(tree.Sort1))
                            return;
                        segment_sort = tree.Sort1;
                        segment_grade = tree.Grade1;
                        segment_length = tree.Length1;

                        bf_ded_feet = (float)tree.BdFtLd1;
                        bf_ded_inches = (float)tree.BdFtDd1;
                        cf_ded_feet = (float)tree.CuFtLd1;
                        cf_ded_inches = (float)tree.CuFtDd1;
                        deduction_code = tree.BdFtPd1;
                        break;
                    case 1:
                        if (string.IsNullOrEmpty(tree.Sort2))
                            return;
                        segment_sort = tree.Sort2;
                        segment_grade = tree.Grade2;
                        segment_length = tree.Length2;

                        bf_ded_feet = (float)tree.BdFtLd2;
                        bf_ded_inches = (float)tree.BdFtDd2;
                        cf_ded_feet = (float)tree.CuFtLd2;
                        cf_ded_inches = (float)tree.CuFtDd2;
                        deduction_code = tree.BdFtPd2;
                        break;
                    case 2:
                        if (string.IsNullOrEmpty(tree.Sort3))
                            return;
                        segment_sort = tree.Sort3;
                        segment_grade = tree.Grade3;
                        segment_length = tree.Length3;

                        bf_ded_feet = (float)tree.BdFtLd3;
                        bf_ded_inches = (float)tree.BdFtDd3;
                        cf_ded_feet = (float)tree.CuFtLd3;
                        cf_ded_inches = (float)tree.CuFtDd3;
                        deduction_code = tree.BdFtPd3;
                        break;
                    case 3:
                        if (string.IsNullOrEmpty(tree.Sort4))
                            return;
                        segment_sort = tree.Sort4;
                        segment_grade = tree.Grade4;
                        segment_length = tree.Length4;

                        bf_ded_feet = (float)tree.BdFtLd4;
                        bf_ded_inches = (float)tree.BdFtDd4;
                        cf_ded_feet = (float)tree.CuFtLd4;
                        cf_ded_inches = (float)tree.CuFtDd4;
                        deduction_code = tree.BdFtPd4;
                        break;
                    case 4:
                        if (string.IsNullOrEmpty(tree.Sort5))
                            return;
                        segment_sort = tree.Sort5;
                        segment_grade = tree.Grade5;
                        segment_length = tree.Length5;

                        bf_ded_feet = (float)tree.BdFtLd5;
                        bf_ded_inches = (float)tree.BdFtDd5;
                        cf_ded_feet = (float)tree.CuFtLd5;
                        cf_ded_inches = (float)tree.CuFtDd5;
                        deduction_code = tree.BdFtPd5;
                        break;
                    case 5:
                        if (string.IsNullOrEmpty(tree.Sort6))
                            return;
                        segment_sort = tree.Sort6;
                        segment_grade = tree.Grade6;
                        segment_length = tree.Length6;

                        bf_ded_feet = (float)tree.BdFtLd6;
                        bf_ded_inches = (float)tree.BdFtDd6;
                        cf_ded_feet = (float)tree.CuFtLd6;
                        cf_ded_inches = (float)tree.CuFtDd6;
                        deduction_code = tree.BdFtPd6;
                        break;
                    case 6:
                        if (string.IsNullOrEmpty(tree.Sort7))
                            return;
                        segment_sort = tree.Sort7;
                        segment_grade = tree.Grade7;
                        segment_length = tree.Length7;

                        bf_ded_feet = (float)tree.BdFtLd7;
                        bf_ded_inches = (float)tree.BdFtDd7;
                        cf_ded_feet = (float)tree.CuFtLd7;
                        cf_ded_inches = (float)tree.CuFtDd7;
                        deduction_code = tree.BdFtPd7;
                        break;
                    case 7:
                        if (string.IsNullOrEmpty(tree.Sort8))
                            return;
                        segment_sort = tree.Sort8;
                        segment_grade = tree.Grade8;
                        segment_length = tree.Length8;

                        bf_ded_feet = (float)tree.BdFtLd8;
                        bf_ded_inches = (float)tree.BdFtDd8;
                        cf_ded_feet = (float)tree.CuFtLd8;
                        cf_ded_inches = (float)tree.CuFtDd8;
                        deduction_code = tree.BdFtPd8;
                        break;
                    case 8:
                        if (string.IsNullOrEmpty(tree.Sort9))
                            return;
                        segment_sort = tree.Sort9;
                        segment_grade = tree.Grade9;
                        segment_length = tree.Length9;

                        bf_ded_feet = (float)tree.BdFtLd9;
                        bf_ded_inches = (float)tree.BdFtDd9;
                        cf_ded_feet = (float)tree.CuFtLd9;
                        cf_ded_inches = (float)tree.CuFtDd9;
                        deduction_code = tree.BdFtPd9;
                        break;
                    case 9:
                        if (string.IsNullOrEmpty(tree.Sort10))
                            return;
                        segment_sort = tree.Sort10;
                        segment_grade = tree.Grade10;
                        segment_length = tree.Length10;

                        bf_ded_feet = (float)tree.BdFtLd10;
                        bf_ded_inches = (float)tree.BdFtDd10;
                        cf_ded_feet = (float)tree.CuFtLd10;
                        cf_ded_inches = (float)tree.CuFtDd10;
                        deduction_code = tree.BdFtPd10;
                        break;
                    case 10:
                        if (string.IsNullOrEmpty(tree.Sort11))
                            return;
                        segment_sort = tree.Sort11;
                        segment_grade = tree.Grade11;
                        segment_length = tree.Length11;

                        bf_ded_feet = (float)tree.BdFtLd11;
                        bf_ded_inches = (float)tree.BdFtDd11;
                        cf_ded_feet = (float)tree.CuFtLd11;
                        cf_ded_inches = (float)tree.CuFtDd11;
                        deduction_code = tree.BdFtPd11;
                        break;
                    case 11:
                        if (string.IsNullOrEmpty(tree.Sort12))
                            return;
                        segment_sort = tree.Sort12;
                        segment_grade = tree.Grade12;
                        segment_length = tree.Length12;

                        bf_ded_feet = (float)tree.BdFtLd12;
                        bf_ded_inches = (float)tree.BdFtDd12;
                        cf_ded_feet = (float)tree.CuFtLd12;
                        cf_ded_inches = (float)tree.CuFtDd12;
                        deduction_code = tree.BdFtPd12;
                        break;
                }
                bf_ded_percent = bf_ded_feet * 10.0f + bf_ded_inches;
                cf_ded_percent = cf_ded_feet * 10.0f + cf_ded_inches;
                dash_or_plus = 0;
                if (segment_length.Contains("-"))
                {
                    original_length = 0.0f;
                    dash_or_plus = 1;
                }
                else
                    if (segment_length.Contains("+"))
                    {
                        original_length = 0.0f;
                        dash_or_plus = 2;
                    }
                    else
                        original_length = Utils.ConvertToInt(segment_length);
                seg_length = original_length;

                for (x = 0; x < 5; x++)
                {
                    seg_log_top[x] = 0.0f;
                    seg_log_butt[x] = 0.0f;
                }

                c_percent = 0.0f;
                if (deduction_code == "1")
                    c_percent = 1.0f;

                cull_segment = false;
                fiber_segment = false;
                trim_allowed = Utils.ConvertToFloat(spcTrim);

                LookupSortValues(segment_sort);
                LookupGradeValues(segment_grade);

                if (grdFiber == "F" || srtFiber == "F")
                    fiber_segment = true;

                if (segment_sort == "0" || segment_grade == "0")
                {
                    cull_segment = true;
                    trim_allowed = 0.0f;
                }

                if (segment_sort == "0")
                {
                    if (segment_grade != "0")
                    {
                        cull_segment = false;
                        trim_allowed = Utils.ConvertToFloat(spcTrim);
                    }
                }

                if (deduction_code == "-")
                    trim_allowed = 0;

                seg_butt_height = seg_top_height;
                seg_butt_diameter = computed_diameter(seg_butt_height);

                if (seg_index == 0)
                    east_side_next_butt = 0;

                if (west_routine == false)
                {
                    if (east_side_next_butt > 0 && seg_index == 1)
                    {
                        seg_butt_diameter = east_side_next_butt;
                        east_side_next_butt = 0;
                    }
                }

                if (original_length > 0)
                {
                    log_max = 1;
                    seg_top_height = seg_length + seg_butt_height;
                    if ((seg_top_height + log_max * trim_allowed) > mtd_height)
                    {
                        real_value = mtd_height - seg_butt_height;
                        recompute_segment_length(real_value);
                        if (seg_length < 0)
                            seg_length = 0;
                        seg_top_height = seg_length + seg_butt_height;
                    }
                    seg_top_height = seg_top_height + log_max * trim_allowed;
                }

                // calc dash dash
                if (original_length == 0 && dash_or_plus == 1)
                {
                    compute_segment_length(merchantable_height - seg_butt_height);
                    if (seg_length < 0)
                        seg_length = 0;
                    original_length = seg_length;
                    seg_top_height = seg_butt_height + seg_length + log_max * trim_allowed;
                }
                // calc plus plus
                if (original_length == 0 && dash_or_plus == 2)
                {
                    seg_length = compute_plus_length(seg_butt_diameter, seg_butt_height,
                        merchantable_height - seg_butt_height,
                        segment_sort, segment_grade);
                    plus_seg = seg_index;
                    log_max = 1;
                    if (seg_length < 0)
                        seg_length = 0;
                    original_length = seg_length;
                    seg_top_height = seg_butt_height + seg_length + log_max * trim_allowed;
                }

                if (segment_sort == "P" || segment_sort == "G")
                    seg_length = original_length;

                if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                {
                    cull_segment = true;
                    trim_allowed = 0;
                }

                if (seg_length < 0)
                    seg_length = 0;

                segment_actual_length = seg_length;
                if (west_routine == true)
                    compute_west_true_logs();
                else
                    compute_east_true_logs();

                seg_log_max = log_max;
                //number_segments = seg_log_max;
                seg_log_index = 0;

                if (plus_status > 0)
                {
                    if (calc_error > 0)
                        calc_segment = plus_seg + 1;
                    switch (plus_status)
                    {
                        case 1:
                            calc_error = 10;
                            teList.Add(new TreeErrors(seg_index + 1, string.Format("Sort{0}", seg_index + 1), "Sort/Grade is Invalid"));
                            break;
                        case 2:
                            calc_error = 11;
                            teList.Add(new TreeErrors(seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Dia"));
                            break;
                        case 3:
                            calc_error = 12;
                            teList.Add(new TreeErrors(seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Len"));
                            break;
                    }
                }

                while (seg_log_index < seg_log_max)
                {
                    process_log();
                    seg[cvol_index].tdia = seg_top_diameter;
                    seg[cvol_index].bdia = seg_butt_diameter;
                    seg[cvol_index].len = seg_length;
                    seg[cvol_index].cum_len = seg_top_height;
                    seg_log_index++;
                    cvol_index++;
                    seg_butt_height = seg_top_height;
                    seg_butt_diameter = seg_top_diameter;
                    national_diameter_rounded = 0;
                    sweyco_diameter_rounded = 0;
                }
                seg_index++;
            }
        }

        private static float computed_height(float diameter)
        {
            float height_at_given_diameter, calc_value;

            if (diameter < dib_at_form_point)
            {       //* above form point 
                height_at_given_diameter = c_form_point +
                    (lsubo * (dib_at_form_point - diameter)) /
                    (dib_at_form_point - diameter * asubo);
                return (height_at_given_diameter);
            }

            if (c_a == 0.0)
            {                            //*  below form point  
                height_at_given_diameter = (c_c - diameter) * c_form_point /
                    (c_c - dib_at_form_point);
                return (height_at_given_diameter);
            }

            calc_value = (float)Square(c_b) - 4.0f * c_a * (c_c - diameter);
            height_at_given_diameter = (0.0f - c_b - (float)Math.Sqrt(calc_value)) / (2.0f * c_a);

            return (height_at_given_diameter);
        }

        private static float computed_diameter(float height)
        {
            float diameter_at_given_height;


            if (height > c_form_point)
            {
                diameter_at_given_height = (dib_at_form_point *
                    (lsubo + c_form_point - height)) / (lsubo + asubo * (c_form_point - height));
                return (diameter_at_given_height);
            }
            diameter_at_given_height = c_a * (float)Square(height) + c_b * height + c_c;
            return (diameter_at_given_height);
        }

        /*
        private int remainder(int x, int y)
        {
            int z = x / y;
            return(x - z * y);
        }

        private float round(float x)
        {
            x += 0.5f;
            return((int)x);
        }
        */

        private static void compute_log_adjustment_factor()
        {
            int iCol = 17;
            int x, y;
            bool done;

            east_side_butt_length = 0;
            for (x = 0; x < 12; x++)
                segment_whole_half[x] = 1.0f;

            if (spcScribnerScale == "W")
                return;
            if (spcScribnerScale == "1")
                return;

            x = 0;
            done = false;
            while (done == false)
            {
                string sSort = string.Empty;
                string sGrade = string.Empty;
                string sPercent = string.Empty;
                switch (x)
                {
                    case 0:
                        sSort = tree.Sort1;
                        sGrade = tree.Grade1;
                        sPercent = tree.BdFtPd1;
                        break;
                    case 1:
                        sSort = tree.Sort2;
                        sGrade = tree.Grade2;
                        sPercent = tree.BdFtPd2;
                        break;
                    case 2:
                        sSort = tree.Sort3;
                        sGrade = tree.Grade3;
                        sPercent = tree.BdFtPd3;
                        break;
                    case 3:
                        sSort = tree.Sort4;
                        sGrade = tree.Grade4;
                        sPercent = tree.BdFtPd4;
                        break;
                    case 4:
                        sSort = tree.Sort5;
                        sGrade = tree.Grade5;
                        sPercent = tree.BdFtPd5;
                        break;
                    case 5:
                        sSort = tree.Sort6;
                        sGrade = tree.Grade6;
                        sPercent = tree.BdFtPd6;
                        break;
                    case 6:
                        sSort = tree.Sort7;
                        sGrade = tree.Grade7;
                        sPercent = tree.BdFtPd7;
                        break;
                    case 7:
                        sSort = tree.Sort8;
                        sGrade = tree.Grade8;
                        sPercent = tree.BdFtPd8;
                        break;
                    case 8:
                        sSort = tree.Sort9;
                        sGrade = tree.Grade9;
                        sPercent = tree.BdFtPd9;
                        break;
                    case 9:
                        sSort = tree.Sort10;
                        sGrade = tree.Grade10;
                        sPercent = tree.BdFtPd10;
                        break;
                    case 10:
                        sSort = tree.Sort11;
                        sGrade = tree.Grade11;
                        sPercent = tree.BdFtPd11;
                        break;
                    case 11:
                        sSort = tree.Sort12;
                        sGrade = tree.Grade12;
                        sPercent = tree.BdFtPd12;
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                {
                    compute_east_butt_length();
                    return;
                }
                if (sSort == " " && sGrade == " ")
                {
                    compute_east_butt_length();
                    return;
                }
                if (sPercent == "-")
                {
                    y = find_second_dash(x);
                    insert_log_adjustment(x, y);
                    x = y;
                }
                x++;
                if (x == 12)
                    done = true;
            }
        }

        private static int find_second_dash(int x)
        {
            int y = x + 1;
            int iCol = 17;

            while (y < 12)
            {
                string sSort = string.Empty;
                string sPercent = string.Empty;
                switch (y)
                {
                    case 0:
                        sSort = tree.Sort1;
                        sPercent = tree.BdFtPd1;
                        break;
                    case 1:
                        sSort = tree.Sort2;
                        sPercent = tree.BdFtPd2;
                        break;
                    case 2:
                        sSort = tree.Sort3;
                        sPercent = tree.BdFtPd3;
                        break;
                    case 3:
                        sSort = tree.Sort4;
                        sPercent = tree.BdFtPd4;
                        break;
                    case 4:
                        sSort = tree.Sort5;
                        sPercent = tree.BdFtPd5;
                        break;
                    case 5:
                        sSort = tree.Sort6;
                        sPercent = tree.BdFtPd6;
                        break;
                    case 6:
                        sSort = tree.Sort7;
                        sPercent = tree.BdFtPd7;
                        break;
                    case 7:
                        sSort = tree.Sort8;
                        sPercent = tree.BdFtPd8;
                        break;
                    case 8:
                        sSort = tree.Sort9;
                        sPercent = tree.BdFtPd9;
                        break;
                    case 9:
                        sSort = tree.Sort10;
                        sPercent = tree.BdFtPd10;
                        break;
                    case 10:
                        sSort = tree.Sort11;
                        sPercent = tree.BdFtPd11;
                        break;
                    case 11:
                        sSort = tree.Sort12;
                        sPercent = tree.BdFtPd12;
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                {
                    y = y - 1;
                    return (y);
                }
                if (sPercent != "-")
                    return (y);
                y++;
            }
            return (y - 1);
        }

        private static void insert_log_adjustment(int x, int y)
        {
            int j;

            j = y - x;
            if (j == 0)
                return;
            if (j == 1)
            {
                segment_whole_half[x] = 0.5f;
                segment_whole_half[x + 1] = 0.5f;
                return;
            }
            if (j == 2)
            {
                segment_whole_half[x] = 0.34f;
                segment_whole_half[x + 1] = 0.33f;
                segment_whole_half[x + 2] = 0.33f;
                return;
            }

            segment_whole_half[x] = 0.25f;
            segment_whole_half[x + 1] = 0.25f;
            segment_whole_half[x + 2] = 0.25f;
            segment_whole_half[x + 3] = 0.25f;
        }

        private static void compute_east_butt_length()
        {
            int iCol = 17;
            int x = 0;
            string fieLd;

            while (x < 12)
            {
                string sSort = string.Empty;
                string sPercent = string.Empty;
                string sLenDed = string.Empty;
                string sDiaDed = string.Empty;
                switch (y)
                {
                    case 0:
                        sSort = tree.Sort1;
                        sLenDed = tree.BdFtLd1.ToString();
                        sDiaDed = tree.BdFtDd1.ToString();
                        sPercent = tree.BdFtPd1;
                        break;
                    case 1:
                        sSort = tree.Sort2;
                        sLenDed = tree.BdFtLd2.ToString();
                        sDiaDed = tree.BdFtDd2.ToString();
                        sPercent = tree.BdFtPd2;
                        break;
                    case 2:
                        sSort = tree.Sort3;
                        sLenDed = tree.BdFtLd3.ToString();
                        sDiaDed = tree.BdFtDd3.ToString();
                        sPercent = tree.BdFtPd3;
                        break;
                    case 3:
                        sSort = tree.Sort4;
                        sLenDed = tree.BdFtLd4.ToString();
                        sDiaDed = tree.BdFtDd4.ToString();
                        sPercent = tree.BdFtPd4;
                        break;
                    case 4:
                        sSort = tree.Sort5;
                        sLenDed = tree.BdFtLd5.ToString();
                        sDiaDed = tree.BdFtDd5.ToString();
                        sPercent = tree.BdFtPd5;
                        break;
                    case 5:
                        sSort = tree.Sort6;
                        sLenDed = tree.BdFtLd6.ToString();
                        sDiaDed = tree.BdFtDd6.ToString();
                        sPercent = tree.BdFtPd6;
                        break;
                    case 6:
                        sSort = tree.Sort7;
                        sLenDed = tree.BdFtLd7.ToString();
                        sDiaDed = tree.BdFtDd7.ToString();
                        sPercent = tree.BdFtPd7;
                        break;
                    case 7:
                        sSort = tree.Sort8;
                        sLenDed = tree.BdFtLd8.ToString();
                        sDiaDed = tree.BdFtDd8.ToString();
                        sPercent = tree.BdFtPd8;
                        break;
                    case 8:
                        sSort = tree.Sort9;
                        sLenDed = tree.BdFtLd9.ToString();
                        sDiaDed = tree.BdFtDd9.ToString();
                        sPercent = tree.BdFtPd9;
                        break;
                    case 9:
                        sSort = tree.Sort10;
                        sLenDed = tree.BdFtLd10.ToString();
                        sDiaDed = tree.BdFtDd10.ToString();
                        sPercent = tree.BdFtPd10;
                        break;
                    case 10:
                        sSort = tree.Sort11;
                        sLenDed = tree.BdFtLd11.ToString();
                        sDiaDed = tree.BdFtDd11.ToString();
                        sPercent = tree.BdFtPd11;
                        break;
                    case 11:
                        sSort = tree.Sort12;
                        sLenDed = tree.BdFtLd12.ToString();
                        sDiaDed = tree.BdFtDd12.ToString();
                        sPercent = tree.BdFtPd12;
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                    return;
                fieLd = sLenDed + sDiaDed;
                y = Utils.ConvertToInt(fieLd);
                if (x == 0)
                    east_side_butt_length = y;
                else
                    east_side_butt_length = east_side_butt_length + y;
                if (sPercent != "-")
                    x = 11;
                x++;
            }
        }

        private static void compute_segment_length(float given_length)
        {
            float tl, c_c;   //* total segment length 
            int l;

            log_max = 1;
            seg_length = (int)given_length;
            if (fiber_segment)
                return;
            if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                return;

            tl = seg_length;
            c_c = (int)Utils.ConvertToInt(spcMaxLogLen) + 1.0f + 2.0f * trim_allowed - 0.1f;

            while (tl > c_c)
            {
                log_max = log_max + 1;
                c_c = c_c + Utils.ConvertToInt(spcMaxLogLen) + trim_allowed;
            }

            l = (int)tl - (int)trim_allowed * log_max;
            seg_length = l;
        }

        private static float compute_plus_length(float buttdib, float butthgt, float len, string s, string g)
        {
            int[] tmindia = new int[] { 7, 4, 2, 6, 2, 2, 2, 7, 6, 2, 10, 2, 2 };
            int[] tminlen = new int[] { 18, 6, 10, 12, 0, 10, 10, 16, 0, 10, 16, 10, 10 };
            string[] tsorgrd = new string[] { "11", "12", "13", "21", "22", "23", "33", "41", "42", "43", "51", "52", "53" };
            float tophgt;
            int mindia, minlen, sub, maxlen;
            string sorgrd;

            sorgrd = String.Empty;
            sorgrd = s + g;
            sub = 0;
            mindia = 0;
            minlen = 0;
            plus_status = 0;
            if (s == "3")
                sorgrd = "33";
            while (sub < 13)
            {
                if (sorgrd == tsorgrd[sub])
                {
                    mindia = tmindia[sub];
                    minlen = tminlen[sub];
                    break;
                }
                sub++;
            }
            if (mindia == 0)
            {
                plus_status = 1;
                return 0.0f;
            }
            if (buttdib < mindia)
            {
                plus_status = 2;
                return 0.0f;
            }
            if (len < minlen)
            {
                plus_status = 3;
                return 0.0f;
            }
            tophgt = computed_height(mindia);
            maxlen = (int)tophgt - (int)butthgt;
            if (maxlen >= minlen)
            {
                plus_status = 0;
                return maxlen;
            }
            plus_status = 1;
            return 0.0f;
        }

        private static void recompute_segment_length(float given_length)
        {
            float tl;     //* total segment length 
            float c;
            int l;

            log_max = 1;
            seg_length = (int)given_length;
            if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                return;
            tl = seg_length;
            c = Utils.ConvertToInt(spcMaxLogLen) + 1.0f + 2.0f * trim_allowed - 0.1f;

            while (tl > c)
            {
                log_max = log_max + 1;
                c = c + Utils.ConvertToInt(spcMaxLogLen) + trim_allowed;
            }
            l = (int)tl - (int)trim_allowed * (int)log_max;
            seg_length = l;
        }

        private static void compute_west_true_logs()
        {
            int x, y, log_length, increment;

            for (x = 0; x < 20; x++)
                seg_log_length[x] = 0;

            if (log_max == 1)
            {
                seg_log_length[0] = seg_length;
                return;
            }

            if (log_max == 0)
                return;

            x = (int)seg_length / ((int)log_max * 2);
            log_length = x * 2;
            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                seg_log_length[x] = log_length;

            for (x = (log_max - 1); x >= 0; x--)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                seg_log_length[x] = seg_log_length[x] + increment;
            }
        }

        private static void compute_east_true_logs()
        {
            int x, y, log_length, increment;

            for (x = 0; x < 20; x++)
                seg_log_length[x] = 0;

            if (log_max == 1)
            {
                seg_log_length[0] = seg_length;
                return;
            }

            if (log_max == 0)
                return;

            x = (int)seg_length / (log_max * 2);
            log_length = x * 2;
            y = (int)seg_length - (log_length * log_max);

            for (x = 0; x < log_max; x++)
                seg_log_length[x] = log_length;

            for (x = 0; x < log_max; x++)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                seg_log_length[x] = seg_log_length[x] + increment;
            }
        }

        private static void compute_west_side_log_length()
        {
            int x, y, log_length, increment;
            int xq, xr;
            decimal z;

            for (x = 0; x < 20; x++)
                log_log_length[x] = 0;

            log_max = 1;

            if (cull_segment == true || seg_length < 41)
            {
                log_log_length[0] = seg_length;
                return;
            }

            //z = Decimal.Divide((int)seg_length, (int)40);
            xq = DivRem((int)seg_length, 40, out xr);
            log_max = xq;
            if (xr > 0)
                log_max++;
            log_length = (int)seg_length / (log_max * 2);
            log_length *= 2;

            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                log_log_length[x] = log_length;

            x = log_max;
            for (x = log_max - 1; x >= 0; x--)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                log_log_length[x] += increment;
            }
        }

        private static void compute_east_side_log_length()
        {
            int x, y, log_length, increment;
            int xr, xq;
            decimal z;

            for (x = 0; x < 20; x++)
                log_log_length[x] = 0;

            log_max = 1;

            if (cull_segment == true || seg_length < 21)
            {
                log_log_length[0] = seg_length;
                return;
            }

            //z = Decimal.Divide((int)seg_length, (int)20);
            xq = DivRem((int)seg_length, 20, out xr);
            log_max = xq;
            if (xr > 0)
                log_max++;
            log_length = (int)seg_length / (log_max * 2);
            log_length *= 2;

            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                log_log_length[x] = log_length;

            for (x = 0; x < log_max; x++)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                log_log_length[x] += increment;
            }
        }

        private static void figure_west_taper()
        {
            int x;
            float taper_difference, y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;

            if (log_max == 1)
                return;

        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];

        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;

            if (log_max == 1)
                return;
            if (spcScribnerScale == "W")
                return;
            //*  one in ten scale   
            x = (int)log_log_length[(int)log_max - 1] / 10;
            seg_log_top[log_max - 2] = seg_log_top[log_max - 1] + x;
            seg_log_butt[log_max - 1] = seg_log_top[log_max - 2];
            if (log_max == 2)
                return;
            x = (int)log_log_length[(int)log_max - 2] / 10;
            seg_log_top[log_max - 3] = seg_log_top[log_max - 2] + x;
            seg_log_butt[log_max - 2] = seg_log_top[log_max - 3];
        }

        private static void figure_bia_taper_2nd()
        {
            int x;
            float y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;
            if (log_max == 1)
                return;
        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private static void figure_bia_taper_butt(float butt_diam, float top_diam)
        {
            int x, y, increment;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[01] = butt_diam;

            if (east_side_butt_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Convert.ToSingle(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (seg_length > 40)
                goto loop1;

            increment = 1;
            if (east_side_butt_length > 22)
                increment = 2;

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[0];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = 2;
        loop2:
            y = (int)taper_difference;
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private static void figure_east_taper_2nd()
        {
            int x;
            float y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;
            if (log_max == 1)
                return;
        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private static void figure_east_taper_butt(int butt_diam, int top_diam)
        {
            int x, increment;
            float y, z;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[0] = butt_diam;

            if (east_side_butt_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Utils.ConvertToFloat(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (seg_length > 40)
                goto loop1;

            temp_species = tree.SpeciesAbbreviation.Trim();
            if (temp_species == "WL")
                increment = 1;
            else
            {
                increment = 1;
                if (east_side_butt_length > 31)
                    increment = 2;
            }
            if (spcScribnerScale == "O")
            {
                if (temp_species == "WL" || temp_species == "LP")
                    increment = 1;
            }

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[1];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = butt_diam - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private static void figure_idaho_taper_butt(float butt_diam, float top_diam)
        {
            int x, increment;
            float y, z;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[0] = butt_diam;
            if (seg_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Utils.ConvertToFloat(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (spcScribnerScale == "T")
            {
                increment = 2;
                if (tree.SpeciesAbbreviation == "DF" || tree.SpeciesAbbreviation == "AF" || tree.SpeciesAbbreviation == "ES")
                    goto loop0;

                if (tree.SpeciesAbbreviation == "LP" || tree.SpeciesAbbreviation == "DL")
                {
                    if (seg_length < 32)
                        increment = 1;
                    else
                        increment = 2;
                    goto loop0;
                }
            }

            if (seg_length > 40)
                goto loop1;

            increment = 1;

            temp_species = tree.SpeciesAbbreviation.Trim();
            if (tree.SpeciesAbbreviation == "WL")
                goto loop0;
            if (tree.SpeciesAbbreviation == "LP")
                goto loop0;
            if (tree.SpeciesAbbreviation == "DL")
                goto loop0;

            increment = 2;
            if (tree.SpeciesAbbreviation == "RC")
                goto loop0;
            if (tree.SpeciesAbbreviation == "PO")
                goto loop0;
            if (tree.SpeciesAbbreviation == "IC")
                goto loop0;

            //* taper = 2 if top diameter is even 
            //* taper = 1 if top diameter is oDd  

            x = (int)top_diam / 2;
            x = x * 2;

            if (x == top_diam)
                increment = 2;
            else
                increment = 1;

        loop0:

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[0];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = butt_diam - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1) goto loop1;
        }

        private static void figure_scribner()
        {
            float rounded_scribner;

            //log_length = (int)Math.Round(log_length); // was (float)
            //log_top_diameter = (int)Math.Round(log_top_diameter); // was (float)
            log_length = (int)log_length;
            log_top_diameter = (int)log_top_diameter;
            if (log_length > 0 && log_top_diameter > 0)
                log_scribner = scribner((int)log_length, (int)log_top_diameter, spcScribnerScale);

            if (c_percent > 0)
            {
                rounded_scribner = (int)Math.Round(log_scribner * (100.0f - bf_ded_percent) * 0.001f); // was (float)
                log_net_scribner = rounded_scribner * 10.0f;
            }
            else
            {
                if ((log_length - bf_ded_feet < 1) || (log_top_diameter - bf_ded_inches < 1))
                    log_net_scribner = 0;
                else
                {
                    if (log_index > 0)
                        bf_ded_feet = 0;
                    log_net_scribner = scribner((int)Math.Round(log_length - bf_ded_feet),
                        (int)Math.Round(log_top_diameter - bf_ded_inches), spcScribnerScale); // was (float)
                }
            }
            if (log_net_scribner > log_scribner)
                log_net_scribner = log_scribner;

            if (cull_segment)
            {
                log_scribner = 0;
                log_net_scribner = 0;
            }
            if (fiber_segment)
            {
                log_scribner = 0;
                log_net_scribner = 0;
            }

            seg[cvol_index].scribner_gross += log_scribner;
            seg[cvol_index].scribner_net += log_net_scribner;

            seg[cvol_index].scribner_gross_pa += log_scribner * trees_pa;
            seg[cvol_index].scribner_net_pa += log_net_scribner * trees_pa;

            if (!cull_segment)
            {
                //if (tree.PlotFactorInput[0] == 'S')
                //    logs_pa += Utils.ConvertToFloat(masterStripTree) * trees_pa;
                //else
                    logs_pa += (1 * trees_pa);
            }
            logs_mbf = ((log_net_scribner * trees_pa) * Utils.ConvertToFloat(masterAcres)) / 1000;
            tree_scribner_gross_pa += log_scribner * trees_pa;
            tree_scribner_net_pa += log_net_scribner * trees_pa;

            tree_mbf += logs_mbf;

            tree_scribner_gross += log_scribner;
            tree_scribner_net += log_net_scribner;

            seg[cvol_index].mbf += logs_mbf;
        }

        private static void figure_cubic()
        {
            string temp;
            float diam;

            if (sweyco_diameter_rounded > 0)
                diam = sweyco_diameter_rounded;
            else
                diam = seg_log_butt[0];

            if (log_length > 0 && log_top_diameter > 0)
            {
                if (spcCubicScale == "1")
                    log_cubic = northwest_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                if (spcCubicScale == "2")
                    log_cubic = sweyco_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)diam);
                if (spcCubicScale == "S")
                    log_cubic = smalian_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                if (spcCubicScale == "W")
                    log_cubic = weyco_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)dib_at_d4h);
                if (spcCubicScale == "N")
                {
                    if (national_diameter_rounded == 0)
                        log_cubic = national_cubic((int)seg_log_length[seg_log_index],
                            (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                    else
                        log_cubic = national_cubic((int)seg_log_length[seg_log_index],
                            (int)seg_log_top[log_max - 1], (int)national_diameter_rounded);
                }
            }

            if (c_percent > 0)
                log_net_cubic = (int)Math.Round(log_cubic * (100 - cf_ded_percent) * 0.01);
            else
            {
                if (spcCubicScale == "1")
                    log_net_cubic =
                        northwest_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)seg_log_butt[0] - (int)cf_ded_inches);
                if (spcCubicScale == "2")
                    log_net_cubic =
                        sweyco_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)diam - (int)cf_ded_inches);
                if (spcCubicScale == "S")
                    log_net_cubic =
                        smalian_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        seg_log_top[log_max - 1] - cf_ded_inches,
                        seg_log_butt[0] - cf_ded_inches);
                if (spcCubicScale == "W")
                    log_net_cubic =
                        weyco_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)dib_at_d4h - (int)cf_ded_inches);
                if (spcCubicScale == "N")
                {
                    if (national_diameter_rounded == 0)
                        log_net_cubic =
                            national_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                            (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                            (int)seg_log_butt[0] - (int)cf_ded_inches);
                    else
                        log_net_cubic =
                            national_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                            (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                            (int)national_diameter_rounded - (int)cf_ded_inches);
                }
            }

            if (spcCubicScale == "1")
                log_merch_cubic =
                    northwest_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                    (int)seg_log_top[log_max - 1] - (int)bf_ded_inches,
                    (int)seg_log_butt[0] - (int)bf_ded_inches);
            if (spcCubicScale == "2")
                log_merch_cubic =
                    sweyco_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                    (int)seg_log_top[log_max - 1] - (int)bf_ded_inches,
                    (int)diam - (int)bf_ded_inches);
            if (spcCubicScale == "S")
                log_merch_cubic =
                    smalian_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                    seg_log_top[log_max - 1] - bf_ded_inches,
                    seg_log_butt[0] - bf_ded_inches);
            if (spcCubicScale == "W")
                log_merch_cubic =
                    weyco_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                    (int)seg_log_top[log_max - 1] - (int)bf_ded_inches,
                    (int)dib_at_d4h - (int)bf_ded_inches);
            if (spcCubicScale == "N")
            {
                if (national_diameter_rounded == 0)
                    log_merch_cubic =
                        national_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)bf_ded_inches,
                        (int)seg_log_butt[0] - (int)bf_ded_inches);
                else
                    log_merch_cubic =
                        national_cubic((int)seg_log_length[seg_log_index] - (int)bf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)bf_ded_inches,
                        (int)national_diameter_rounded - (int)bf_ded_inches);
            }

            if (log_net_cubic > log_cubic)
                log_net_cubic = log_cubic;

            if (cull_segment)
            {
                log_cubic = 0;
                log_net_cubic = 0;
                log_merch_cubic = 0;
            }

            seg[cvol_index].cubic_gross += log_cubic;
            seg[cvol_index].cubic_net += log_net_cubic;
            seg[cvol_index].cubic_merch += log_merch_cubic;
            seg[cvol_index].cubic_gross_pa += log_cubic * trees_pa;
            seg[cvol_index].cubic_net_pa += log_net_cubic * trees_pa;

            tree_cubic_gross_pa += log_cubic * trees_pa;
            tree_cubic_net_pa += log_net_cubic * trees_pa;
            logs_ccf = ((log_net_cubic * trees_pa) * Utils.ConvertToFloat(masterAcres)) / 100;
            tree_ccf += logs_ccf;

            temp = String.Format("{0:0}", log_cubic);
            tree_cubic_gross += Utils.ConvertToFloat(temp);
            temp = String.Format("{0:0}", log_net_cubic);
            tree_cubic_net += Utils.ConvertToFloat(temp);
            temp = String.Format("{0:0}", log_merch_cubic);
            tree_cubic_merch += Utils.ConvertToFloat(temp);

            seg[cvol_index].ccf += logs_ccf;
        }

        private static float scribner(int l, int t, string scale)
        {
            //decimal x;
            int xq = 0, xr = 0, x = 0;
            float volume, len, top;

            len = (int)l;
            top = (int)t;

            if (scale == "D")
            {
                if (top < 5)
                {
                    volume = 0;
                    return (volume);
                }
                volume = ((float)top - 4.0f) / 4.0f;
                volume = (int)volume * (int)volume * (int)len;
                return (volume);
            }
            double fac = GetVolumeFactor((int)top, (int)len);
            volume = len * (float)fac;
            //x = Decimal.Divide((decimal)volume, (decimal)10);
            //x = Decimal.Divide((int)volume, (int)10);
            xq = DivRem((int)volume, 10, out xr);
            if (xq > 0 && xr > 4)
                xq++;
            else
                if (xr > 4)
                    xq++;
            volume = (xq * 10);
            if (scale == "I" || scale == "T")
            {
                if (top == 6 && len == 14)
                    volume -= 10;
                if (top == 8 && len == 20)
                    volume -= 10;
                if (top == 9 && len == 20)
                    volume -= 10;
                if (top == 9 && len == 10)
                    volume += 10;
                if (top == 10 && len == 12)
                    volume -= 10;
                if (top == 23 && len == 10)
                    volume -= 10;
                if (top == 5 && len == 16)
                    volume -= 10;
                if (top == 5 && len == 15)
                    volume -= 10;
            }
            long tVol = (int)volume;
            return ((float)tVol);
        }

        private static float smalian_cubic(int len, float top, float butt)
        {
            float volume, log_l;
            float tdia, bdia;

            log_l = len;
            tdia = top; // (float)Math.Round(top);
            bdia = butt; // (float)Math.Round(butt);
            volume = 0.00272708f * (tdia * tdia + bdia * bdia) * log_l;
            return (volume);
            //long lTmp = (long)volume;
            //return ((float)lTmp);
        }

        private static float national_cubic(int len, int top, int butt)
        {
            float volume, log_l;
            float tdia, bdia;

            log_l = len;
            tdia = top;
            bdia = butt;
            volume = 0.002727f * (tdia * tdia + bdia * bdia) * log_l;
            return (volume);
            //long lTmp = (long)volume;
            //return ((float)lTmp);
        }

        private static float northwest_cubic(int len, int top, int butt)
        {
            float volume, len_fac, log_l, length;
            float tdia, bdia, top_dia, bot_dia, d1, d2;

            log_l = len;
            tdia = top;
            bdia = butt;

            if (log_l >= 17)
                len_fac = 1.00f;
            else
                len_fac = 0.67f;
            length = log_l + len_fac;
            if (west_routine == true)
            {
                top_dia = tdia + 0.7f;
                bot_dia = bdia + 0.7f;
            }
            else
            {
                top_dia = tdia;
                bot_dia = bdia;
            }
            d1 = top_dia * top_dia;
            d2 = bot_dia * bot_dia;
            volume = 0.001818f * length * (d1 + d2 + (top_dia * bot_dia));
            return (volume);
            //long lTmp = (long)volume;
            //return ((float)lTmp);
        }

        private static float sweyco_cubic(int len, int top, int butt)
        {
            float volume, log_l, length;
            float tdia, bdia, top_dia, bot_dia, d1, d2;

            log_l = len;
            tdia = top;
            bdia = butt;

            length = log_l;
            top_dia = tdia;
            bot_dia = bdia;
            d1 = top_dia * top_dia;
            d2 = bot_dia * bot_dia;
            volume = 0.001818f * length * (d1 + d2 + (top_dia * bot_dia));
            long lTmp = (long)volume;
            return ((float)lTmp);
        }

        private static float weyco_cubic(int len, int top, int butt)
        {
            float volume, len_fac, log_l, length, top_dia, bot_dia;
            float tdia, bdia;

            log_l = len;
            tdia = top;
            bdia = butt;

            if (log_l < 17)
                len_fac = 0.75f;
            else
                len_fac = 0.67f;
            length = (len_fac + log_l) * 0.005454f;
            top_dia = tdia + 0.5f;
            bot_dia = bdia + 0.5f;
            volume = ((top_dia * top_dia) + (bot_dia * bot_dia) + (top_dia * bot_dia)) / 3.0f;
            return ((volume * length) + 0.5f);
            //long lTmp = (long)volume;
            //return ((float)lTmp);
        }

        private static float merch_cubic(int i, int len, int top, int butt, int Lded, int Dded)
        {
            double volume, lenFac, length, toPdia, buttDia;
            int log_l;
            double d1, d2, a, b, c, d, e, f, g, fs, gs;
            //char* ptr;

            volume = lenFac = length = toPdia = buttDia = 0;
            d1 = d2 = a = b = c = d = e = f = g = fs = gs = 0;
            log_l = 0;
            switch (i)
            {
                case 0:
                    c = (double)Lded / 2;
                    a = (double)top;
                    b = (double)butt;
                    d = (b - a) / len;
                    e = (len - Lded);
                    if (log_max == 2)
                        lenFac = .5;
                    if (log_max == 4)
                        lenFac = .25;
                    if (log_max == 3)
                    {
                        switch (i)
                        {
                            case 0:
                            case 1:
                                lenFac = .33;
                                break;
                            case 2:
                                lenFac = .34;
                                break;
                        }
                        lenFac = .25;
                    }
                    if (log_max == 1)
                    {
                        if (len < 17)
                            lenFac = .67;
                        else
                            lenFac = 1.00;
                    }
                    e += lenFac;
                    f = (a + (c * d)) - (double)Dded;
                    f += .7;
                    g = (b - (c * d)) - (double)Dded;
                    g += .7;
                    break;
                case 1:
                    c = Lded / 2;
                    a = (float)top;
                    b = butt;
                    d = (b - a) / len;
                    e = (len - Lded);
                    if (log_max == 2)
                        lenFac = .5;
                    if (log_max == 4)
                        lenFac = .25;
                    if (log_max == 3)
                    {
                        switch (i)
                        {
                            case 0:
                            case 1:
                                lenFac = .33;
                                break;
                            case 2:
                                lenFac = .34;
                                break;
                        }
                        lenFac = .25;
                    }
                    if (log_max == 1)
                    {
                        if (len < 17)
                            lenFac = .67;
                        else
                            lenFac = 1.00;
                    }
                    e += lenFac;
                    f = a + (c * d) - Dded;
                    f += .7;
                    g = b - (c * d) - Dded;
                    g += .7;
                    break;
                case 2:
                    c = Lded / 2;
                    log_l = len;
                    a = (float)top;
                    b = butt;
                    d = (b - a) / len;
                    e = (len - Lded);
                    if (log_max == 2)
                        lenFac = .5;
                    if (log_max == 4)
                        lenFac = .25;
                    if (log_max == 3)
                    {
                        switch (i)
                        {
                            case 0:
                            case 1:
                                lenFac = .33;
                                break;
                            case 2:
                                lenFac = .34;
                                break;
                        }
                        lenFac = .25;
                    }
                    if (log_max == 1)
                    {
                        if (len < 17)
                            lenFac = .67;
                        else
                            lenFac = 1.00;
                    }
                    e += lenFac;
                    f = a + (c * d) - Dded;
                    g = b - (c * d) - Dded;
                    f += .7;
                    g += .7;
                    break;
            }
            fs = f * f;
            gs = g * g;
            volume = .001818 * (e * (fs + gs + (f * g)));
            return (Utils.ConvertToFloat(string.Format("{0}", volume)));
        }

        private static double Square(float flt)
        {
            double t = (double)flt * (double)flt;
            return Math.Round(t,4);
        }

        private static int check_sort_grade_combo(string sort, string grade, int len, int top)
        {
            int[] tmindia = new int[] { 6, 2, 2, 6, 2, 2, 2, 6, 2, 2, 10, 2, 2 };
            int[] tminlen = new int[] { 14, 10, 10, 14, 10, 10, 10, 14, 10, 10, 12, 10, 10 };
            string[] tsorgrd = new string[] { "11", "12", "13", "21", "22", "23", "33", "41", "42", "43", "51", "52", "53" };
            int mindia, minlen, sub;
            string sorgrd = null;
            int found;

            found = 0;
            sorgrd = String.Format("{0}{1}", sort, grade);
            sub = 0;
            mindia = 0;
            minlen = 0;
            if (sort == "3")
                sorgrd = "33";
            while (sub < 13)
            {
                if (sorgrd == tsorgrd[sub])
                {
                    mindia = tmindia[sub];
                    minlen = tminlen[sub];
                    found = 1;
                    break;
                }
                sub++;
            }
            if (mindia == 0)
            {
                calc_error = 10;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Sort{0}", seg_index + 1), "Sort/Grade is Invalid"));
            }
            if (top < mindia)
            {
                calc_error = 11;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Dia"));
            }
            if (len < minlen)
            {
                calc_error = 12;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Len"));
            }
            return found;
        }

        private static void check_grade_len_top_butt(string grade, int len, int top, int butt)
        {
            LookupGradeValues(grade);
            if (len < Utils.ConvertToInt(grdMinLen))
            {
                calc_error = 3;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Grade{0}", seg_index + 1), string.Format("Under Min Len for Grade ({0})", grdMinLen)));
            }
            if (top < Utils.ConvertToInt(grdMinDia))
            {
                calc_error = 4;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Grade{0}", seg_index + 1), string.Format("Under Min Dia for Grade ({0})", grdMinDia)));
            }
            if (Utils.ConvertToInt(grdMaxButt) > 0)
            {
                if (butt > Utils.ConvertToInt(grdMaxButt))
                {
                    calc_error = 8;
                    teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Grade{0}", seg_index + 1), string.Format("Over Max Butt Dia for Grade ({0})", grdMaxButt)));
                }
            }
        }

        private static void check_sort_len_top_butt(string sort, int len, int top, int butt)
        {
            LookupSortValues(sort);
            if (len < Utils.ConvertToInt(srtMinLen))
            {
                calc_error = 5;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Sort{0}", seg_index + 1), string.Format("Under Min Len for Sort ({0})", srtMinLen)));
            }
            if (top < Utils.ConvertToInt(srtMinDia))
            {
                calc_error = 6;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Sort{0}", seg_index + 1), string.Format("Under Min Dia for Sort ({0})", srtMinDia)));
            }
            if (Utils.ConvertToInt(srtMaxButt) > 0)
            {
                if (butt > Utils.ConvertToInt(srtMaxButt))
                {
                    calc_error = 9;
                    teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Sort{0}", seg_index + 1), string.Format("Over Max Butt Dia for Sort ({0})", srtMaxButt)));
                }
            }
        }

        private static void check_master_len_dia(int len, int dia)
        {
            if (len < Utils.ConvertToInt(spcMinLogLen))
            {
                calc_error = 2;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Len"));
            }
            if (dia < Utils.ConvertToInt(spcMinTopDia))
            {
                calc_error = 1;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Length{0}", seg_index + 1), "Under Min Dia"));
            }
            if (len > Utils.ConvertToInt(spcMaxLogLen))
            {
                calc_error = 7;
                teList.Add(new TreeErrors((short)seg_index + 1, string.Format("Length{0}", seg_index + 1), "Over Max Len"));
            }
        }

        private static double GetVolumeFactor(int iDia, int iLen)
        {
            double VolFac = 0;

            if ((iLen > 0 && iLen < 16) && (iDia > 5 && iDia < 12))
            {
                switch (iDia)
                {
                    case 6:
                        VolFac = 1.160;
                        break;
                    case 7:
                        VolFac = 1.400;
                        break;
                    case 8:
                        VolFac = 1.501;
                        break;
                    case 9:
                        VolFac = 2.084;
                        break;
                    case 10:
                        VolFac = 3.126;
                        break;
                    case 11:
                        VolFac = 3.749;
                        break;
                }
            }
            else
                if ((iLen > 15 && iLen < 32) && (iDia > 5 && iDia < 12))
                {
                    switch (iDia)
                    {
                        case 6:
                            VolFac = 1.249;
                            break;
                        case 7:
                            VolFac = 1.608;
                            break;
                        case 8:
                            VolFac = 1.854;
                            break;
                        case 9:
                            VolFac = 2.410;
                            break;
                        case 10:
                            VolFac = 3.542;
                            break;
                        case 11:
                            VolFac = 4.167;
                            break;
                    }
                }
                else
                    if ((iLen > 31 && iLen < 41) && (iDia > 5 && iDia < 12))
                    {
                        switch (iDia)
                        {
                            case 6:
                                VolFac = 1.570;
                                break;
                            case 7:
                                VolFac = 1.800;
                                break;
                            case 8:
                                VolFac = 2.200;
                                break;
                            case 9:
                                VolFac = 2.900;
                                break;
                            case 10:
                                VolFac = 3.815;
                                break;
                            case 11:
                                VolFac = 4.499;
                                break;
                        }
                    }
                    else
                        if (iDia < 6)
                        {
                            switch (iDia)
                            {
                                case 1:
                                    VolFac = 0.000;
                                    break;
                                case 2:
                                    VolFac = 0.143;
                                    break;
                                case 3:
                                    VolFac = 0.390;
                                    break;
                                case 4:
                                    VolFac = 0.676;
                                    break;
                                case 5:
                                    VolFac = 1.070;
                                    break;
                            }
                        }
                        else
                            if (iDia > 11)
                            {
                                switch (iDia)
                                {
                                    case 12:
                                        VolFac = 4.900;
                                        break;
                                    case 13:
                                        VolFac = 6.043;
                                        break;
                                    case 14:
                                        VolFac = 7.140;
                                        break;
                                    case 15:
                                        VolFac = 8.880;
                                        break;
                                    case 16:
                                        VolFac = 10.000;
                                        break;
                                    case 17:
                                        VolFac = 11.528;
                                        break;
                                    case 18:
                                        VolFac = 13.290;
                                        break;
                                    case 19:
                                        VolFac = 14.990;
                                        break;
                                    case 20:
                                        VolFac = 17.499;
                                        break;
                                    case 21:
                                        VolFac = 18.990;
                                        break;
                                    case 22:
                                        VolFac = 20.880;
                                        break;
                                    case 23:
                                        VolFac = 23.510;
                                        break;
                                    case 24:
                                        VolFac = 25.218;
                                        break;
                                    case 25:
                                        VolFac = 28.677;
                                        break;
                                    case 26:
                                        VolFac = 31.249;
                                        break;
                                    case 27:
                                        VolFac = 34.220;
                                        break;
                                    case 28:
                                        VolFac = 36.376;
                                        break;
                                    case 29:
                                        VolFac = 38.040;
                                        break;
                                    case 30:
                                        VolFac = 41.060;
                                        break;
                                    case 31:
                                        VolFac = 44.376;
                                        break;
                                    case 32:
                                        VolFac = 45.975;
                                        break;
                                    case 33:
                                        VolFac = 48.990;
                                        break;
                                    case 34:
                                        VolFac = 50.000;
                                        break;
                                    case 35:
                                        VolFac = 54.688;
                                        break;
                                    case 36:
                                        VolFac = 57.660;
                                        break;
                                    case 37:
                                        VolFac = 64.319;
                                        break;
                                    case 38:
                                        VolFac = 66.730;
                                        break;
                                    case 39:
                                        VolFac = 70.000;
                                        break;
                                    case 40:
                                        VolFac = 75.240;
                                        break;
                                    case 41:
                                        VolFac = 79.480;
                                        break;
                                    case 42:
                                        VolFac = 83.910;
                                        break;
                                    case 43:
                                        VolFac = 87.190;
                                        break;
                                    case 44:
                                        VolFac = 92.501;
                                        break;
                                    case 45:
                                        VolFac = 94.990;
                                        break;
                                    case 46:
                                        VolFac = 99.075;
                                        break;
                                    case 47:
                                        VolFac = 103.501;
                                        break;
                                    case 48:
                                        VolFac = 107.970;
                                        break;
                                    case 49:
                                        VolFac = 112.292;
                                        break;
                                    case 50:
                                        VolFac = 116.990;
                                        break;
                                    case 51:
                                        VolFac = 121.650;
                                        break;
                                    case 52:
                                        VolFac = 126.525;
                                        break;
                                    case 53:
                                        VolFac = 131.510;
                                        break;
                                    case 54:
                                        VolFac = 136.510;
                                        break;
                                    case 55:
                                        VolFac = 141.610;
                                        break;
                                    case 56:
                                        VolFac = 146.912;
                                        break;
                                    case 57:
                                        VolFac = 152.210;
                                        break;
                                    case 58:
                                        VolFac = 157.710;
                                        break;
                                    case 59:
                                        VolFac = 163.288;
                                        break;
                                    case 60:
                                        VolFac = 168.990;
                                        break;
                                    case 61:
                                        VolFac = 174.850;
                                        break;
                                    case 62:
                                        VolFac = 180.749;
                                        break;
                                    case 63:
                                        VolFac = 186.623;
                                        break;
                                    case 64:
                                        VolFac = 193.170;
                                        break;
                                    case 65:
                                        VolFac = 199.120;
                                        break;
                                    case 66:
                                        VolFac = 205.685;
                                        break;
                                    case 67:
                                        VolFac = 211.810;
                                        break;
                                    case 68:
                                        VolFac = 218.501;
                                        break;
                                    case 69:
                                        VolFac = 225.685;
                                        break;
                                    case 70:
                                        VolFac = 232.499;
                                        break;
                                    case 71:
                                        VolFac = 239.317;
                                        break;
                                    case 72:
                                        VolFac = 246.615;
                                        break;
                                    case 73:
                                        VolFac = 254.040;
                                        break;
                                    case 74:
                                        VolFac = 261.525;
                                        break;
                                    case 75:
                                        VolFac = 269.040;
                                        break;
                                    case 76:
                                        VolFac = 276.630;
                                        break;
                                    case 77:
                                        VolFac = 284.260;
                                        break;
                                    case 78:
                                        VolFac = 292.501;
                                        break;
                                    case 79:
                                        VolFac = 300.655;
                                        break;
                                    case 80:
                                        VolFac = 308.970;
                                        break;
                                    case 81:
                                        VolFac = 317.360;
                                        break;
                                    case 82:
                                        VolFac = 325.790;
                                        break;
                                    case 83:
                                        VolFac = 334.217;
                                        break;
                                    case 84:
                                        VolFac = 343.290;
                                        break;
                                    case 85:
                                        VolFac = 350.785;
                                        break;
                                    case 86:
                                        VolFac = 359.120;
                                        break;
                                    case 87:
                                        VolFac = 368.380;
                                        break;
                                    case 88:
                                        VolFac = 376.610;
                                        break;
                                    case 89:
                                        VolFac = 385.135;
                                        break;
                                    case 90:
                                        VolFac = 393.380;
                                        break;
                                    case 91:
                                        VolFac = 402.499;
                                        break;
                                    case 92:
                                        VolFac = 410.834;
                                        break;
                                    case 93:
                                        VolFac = 419.166;
                                        break;
                                    case 94:
                                        VolFac = 428.380;
                                        break;
                                    case 95:
                                        VolFac = 437.499;
                                        break;
                                    case 96:
                                        VolFac = 446.565;
                                        break;
                                    case 97:
                                        VolFac = 455.010;
                                        break;
                                    case 98:
                                        VolFac = 464.150;
                                        break;
                                    case 99:
                                        VolFac = 473.430;
                                        break;
                                    case 100:
                                        VolFac = 482.490;
                                        break;
                                    case 101:
                                        VolFac = 491.700;
                                        break;
                                    case 102:
                                        VolFac = 501.700;
                                        break;
                                    case 103:
                                        VolFac = 511.700;
                                        break;
                                    case 104:
                                        VolFac = 521.700;
                                        break;
                                    case 105:
                                        VolFac = 531.700;
                                        break;
                                    case 106:
                                        VolFac = 541.700;
                                        break;
                                    case 107:
                                        VolFac = 552.499;
                                        break;
                                    case 108:
                                        VolFac = 562.501;
                                        break;
                                    case 109:
                                        VolFac = 573.350;
                                        break;
                                    case 110:
                                        VolFac = 583.350;
                                        break;
                                    case 111:
                                        VolFac = 594.150;
                                        break;
                                    case 112:
                                        VolFac = 604.170;
                                        break;
                                    case 113:
                                        VolFac = 615.010;
                                        break;
                                    case 114:
                                        VolFac = 625.890;
                                        break;
                                    case 115:
                                        VolFac = 636.660;
                                        break;
                                    case 116:
                                        VolFac = 648.380;
                                        break;
                                    case 117:
                                        VolFac = 660.000;
                                        break;
                                    case 118:
                                        VolFac = 671.700;
                                        break;
                                    case 119:
                                        VolFac = 683.330;
                                        break;
                                    case 120:
                                        VolFac = 695.011;
                                        break;
                                }
                            }
            return VolFac;
        }

        private static int DivRem(int Num, int DivBy, out int rem)
        {
            int iAns;
            float fDivBy = (float)DivBy;
            float fNum = (float)Num;
            float a = fNum / fDivBy;

            iAns = (int)a;
            rem = Num - (iAns * DivBy);
            return (int)a;
        }
    }

    public class SgCalc
    {
        public string sort;
        public string grade;
        public string scribner_lended;
        public string scribner_diaded;
        public string cubic_lended;
        public string cubic_diaded;
        public string percent;
        public float tdia;
        public float bdia;
        public float len;
        public float cum_len;
        public float scribner_gross;
        public float scribner_net;
        public float cubic_gross;
        public float cubic_net;
        public float scribner_gross_pa;
        public float scribner_net_pa;
        public float cubic_gross_pa;
        public float cubic_net_pa;
        public float cubic_merch;
        public float ccf;
        public float mbf;
        public string comments;

        public SgCalc()
        {
            scribner_lended = string.Empty;
            scribner_diaded = string.Empty;
            cubic_lended = string.Empty;
            cubic_diaded = string.Empty;
            percent = string.Empty;
            tdia = 0.0f;
            bdia = 0.0f;
            len = 0.0f;
            cum_len = 0.0f;
            scribner_gross = 0.0f;
            scribner_net = 0.0f;
            cubic_gross = 0.0f;
            cubic_net = 0.0f;
            scribner_gross_pa = 0.0f;
            scribner_net_pa = 0.0f;
            cubic_gross_pa = 0.0f;
            cubic_net_pa = 0.0f;
            cubic_merch = 0.0f;
            ccf = 0;
            mbf = 0;
            comments = string.Empty;
        }
    }

    public class TreeErrors
    {
        public int segment;
        public string fieLd;
        public string ErrorMsg;

        public TreeErrors(int pSegment, string pFieLd, string pErrorMsg)
        {
            segment = pSegment;
            fieLd = pFieLd;
            ErrorMsg = pErrorMsg;
        }
    }
}
