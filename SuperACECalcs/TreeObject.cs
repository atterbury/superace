﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperACECalcs
{
    public class TreeObject
    {
        public int? PlotsID;
        public int? StandsID;
        public string CrownPositionDisplayCode;
        public string CrownRatioDisplayCode;
        public string DamageDisplayCode;
        public string TreeStatusDisplayCode;
        public string UserDefinedDisplayCode;
        public string VigorDisplayCode;

        public string PlotNumber;
        public string PlotFactorInputCode;
        public byte AgeCode;
        public string SpeciesAbbreviation;
        public string TopDiameterFractionCode;
        public string TreeNumber;
        public float Dbh;
        public byte TreeCount;
        public byte FormPoint;
        public byte ReportedFormFactor;
        public short BoleHeight;
        public short TotalHeight;

        public string Sort1;
        public string Grade1;
        public string Length1;
        public byte BdFtLD1;
        public byte BdFtDD1;
        public byte BdFtPD1;
        public byte CuFtLD1;
        public byte CuFtDD1;
        public byte CuFtPD1;

        public string Sort2;
        public string Grade2;
        public string Length2;
        public byte BdFtLD2;
        public byte BdFtDD2;
        public byte BdFtPD2;
        public byte CuFtLD2;
        public byte CuFtDD2;
        public byte CuFtPD2;

        public string Sort3;
        public string Grade3;
        public string Length3;
        public byte BdFtLD3;
        public byte BdFtDD3;
        public byte BdFtPD3;
        public byte CuFtLD3;
        public byte CuFtDD3;
        public byte CuFtPD3;

        public string Sort4;
        public string Grade4;
        public string Length4;
        public byte BdFtLD4;
        public byte BdFtDD4;
        public byte BdFtPD4;
        public byte CuFtLD4;
        public byte CuFtDD4;
        public byte CuFtPD4;

        public string Sort5;
        public string Grade5;
        public string Length5;
        public byte BdFtLD5;
        public byte BdFtDD5;
        public byte BdFtPD5;
        public byte CuFtLD5;
        public byte CuFtDD5;
        public byte CuFtPD5;

        public string Sort6;
        public string Grade6;
        public string Length6;
        public byte BdFtLD6;
        public byte BdFtDD6;
        public byte BdFtPD6;
        public byte CuFtLD6;
        public byte CuFtDD6;
        public byte CuFtPD6;

        public string Sort7;
        public string Grade7;
        public string Length7;
        public byte BdFtLD7;
        public byte BdFtDD7;
        public byte BdFtPD7;
        public byte CuFtLD7;
        public byte CuFtDD7;
        public byte CuFtPD7;

        public string Sort8;
        public string Grade8;
        public string Length8;
        public byte BdFtLD8;
        public byte BdFtDD8;
        public byte BdFtPD8;
        public byte CuFtLD8;
        public byte CuFtDD8;
        public byte CuFtPD8;

        public string Sort9;
        public string Grade9;
        public string Length9;
        public byte BdFtLD9;
        public byte BdFtDD9;
        public byte BdFtPD9;
        public byte CuFtLD9;
        public byte CuFtDD9;
        public byte CuFtPD9;

        public string Sort10;
        public string Grade10;
        public string Length10;
        public byte BdFtLD10;
        public byte BdFtDD10;
        public byte BdFtPD10;
        public byte CuFtLD10;
        public byte CuFtDD10;
        public byte CuFtPD10;

        public string Sort11;
        public string Grade11;
        public string Length11;
        public byte BdFtLD11;
        public byte BdFtDD11;
        public byte BdFtPD11;
        public byte CuFtLD11;
        public byte CuFtDD11;
        public byte CuFtPD11;

        public string Sort12;
        public string Grade12;
        public string Length12;
        public byte BdFtLD12;
        public byte BdFtDD12;
        public byte BdFtPD12;
        public byte CuFtLD12;
        public byte CuFtDD12;
        public byte CuFtPD12;

        public TreeObject()
        {
            Init();
        }

        private void Init()
        {
            this.PlotsID = -1;
            this.StandsID = -1;
            this.CrownPositionDisplayCode = string.Empty;
            this.CrownRatioDisplayCode = string.Empty;
            this.DamageDisplayCode = string.Empty;
            this.TreeStatusDisplayCode = string.Empty;
            this.UserDefinedDisplayCode = string.Empty;
            this.VigorDisplayCode = string.Empty;

            this.PlotNumber = string.Empty;
            this.PlotFactorInputCode = string.Empty;
            this.AgeCode = 0;
            this.SpeciesAbbreviation = string.Empty;
            this.TopDiameterFractionCode = string.Empty;
            this.TreeNumber = string.Empty;
            this.Dbh = 0;
            this.TreeCount = 0;
            this.FormPoint = 0;
            this.ReportedFormFactor = 0;
            this.BoleHeight = 0;
            this.TotalHeight = 0;

            this.Sort1 = string.Empty;
            this.Grade1 = string.Empty;
            this.Length1 = string.Empty;
            this.BdFtLD1 = 0;
            this.BdFtDD1 = 0;
            this.BdFtPD1 = 0;
            this.CuFtLD1 = 0;
            this.CuFtDD1 = 0;
            this.CuFtPD1 = 0;

            this.Sort2 = string.Empty;
            this.Grade2 = string.Empty;
            this.Length2 = string.Empty;
            this.BdFtLD2 = 0;
            this.BdFtDD2 = 0;
            this.BdFtPD2 = 0;
            this.CuFtLD2 = 0;
            this.CuFtDD2 = 0;
            this.CuFtPD2 = 0;

            this.Sort3 = string.Empty;
            this.Grade3 = string.Empty;
            this.Length3 = string.Empty;
            this.BdFtLD3 = 0;
            this.BdFtDD3 = 0;
            this.BdFtPD3 = 0;
            this.CuFtLD3 = 0;
            this.CuFtDD3 = 0;
            this.CuFtPD3 = 0;

            this.Sort4 = string.Empty;
            this.Grade4 = string.Empty;
            this.Length4 = string.Empty;
            this.BdFtLD4 = 0;
            this.BdFtDD4 = 0;
            this.BdFtPD4 = 0;
            this.CuFtLD4 = 0;
            this.CuFtDD4 = 0;
            this.CuFtPD4 = 0;

            this.Sort5 = string.Empty;
            this.Grade5 = string.Empty;
            this.Length5 = string.Empty;
            this.BdFtLD5 = 0;
            this.BdFtDD5 = 0;
            this.BdFtPD5 = 0;
            this.CuFtLD5 = 0;
            this.CuFtDD5 = 0;
            this.CuFtPD5 = 0;

            this.Sort6 = string.Empty;
            this.Grade6 = string.Empty;
            this.Length6 = string.Empty;
            this.BdFtLD6 = 0;
            this.BdFtDD6 = 0;
            this.BdFtPD6 = 0;
            this.CuFtLD6 = 0;
            this.CuFtDD6 = 0;
            this.CuFtPD6 = 0;

            this.Grade7 = string.Empty;
            this.Length7 = string.Empty;
            this.BdFtLD7 = 0;
            this.BdFtDD7 = 0;
            this.BdFtPD7 = 0;
            this.CuFtLD7 = 0;
            this.CuFtDD7 = 0;
            this.CuFtPD7 = 0;

            this.Sort8 = string.Empty;
            this.Grade8 = string.Empty;
            this.Length8 = string.Empty;
            this.BdFtLD8 = 0;
            this.BdFtDD8 = 0;
            this.BdFtPD8 = 0;
            this.CuFtLD8 = 0;
            this.CuFtDD8 = 0;
            this.CuFtPD8 = 0;

            this.Sort9 = string.Empty;
            this.Grade9 = string.Empty;
            this.Length9 = string.Empty;
            this.BdFtLD9 = 0;
            this.BdFtDD9 = 0;
            this.BdFtPD9 = 0;
            this.CuFtLD9 = 0;
            this.CuFtDD9 = 0;
            this.CuFtPD9 = 0;

            this.Sort10 = string.Empty;
            this.Grade10 = string.Empty;
            this.Length10 = string.Empty;
            this.BdFtLD10 = 0;
            this.BdFtDD10 = 0;
            this.BdFtPD10 = 0;
            this.CuFtLD10 = 0;
            this.CuFtDD10 = 0;
            this.CuFtPD10 = 0;

            this.Sort11 = string.Empty;
            this.Grade11 = string.Empty;
            this.Length11 = string.Empty;
            this.BdFtLD11 = 0;
            this.BdFtDD11 = 0;
            this.BdFtPD11 = 0;
            this.CuFtLD11 = 0;
            this.CuFtDD11 = 0;
            this.CuFtPD11 = 0;

            this.Sort12 = string.Empty;
            this.Grade12 = string.Empty;
            this.Length12 = string.Empty;
            this.BdFtLD12 = 0;
            this.BdFtDD12 = 0;
            this.BdFtPD12 = 0;
            this.CuFtLD12 = 0;
            this.CuFtDD12 = 0;
            this.CuFtPD12 = 0;
        }
    }
}
