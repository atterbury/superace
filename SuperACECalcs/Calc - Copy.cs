using System;
using System.Data;
using System.Windows.Forms;
//using System.Data.SqlServerCe;
//using DevExpress.XtraGrid.Views.Grid;
//using DevExpress.XtraGrid.Views.Base;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using BusinessObjects;

namespace SuperACECalcs
{
    /// <summary>
    /// Summary description for Calc.
    /// </summary>
    public class Calc
    {
        public Calc()
        {
            ////sg = pGrd;
            ////iMasterID = iID;
            //sTownship = Twn;
            //sRange = Rge;
            //sSection = Sec;
            //sTract = Tr;
            //sType = Typ;
            ////sPlot = Plt;

            basal_area = 0.0f;
            trees_pa = 0.0f;
            tree_scribner_gross = 0.0f;
            tree_scribner_net = 0.0f;
            tree_cubic_gross = 0.0f;
            tree_cubic_net = 0.0f;
            for (int i = 0; i < 12; i++)
                seg[i] = new SgCalc();
            //utl = new Utils(cn);
        }

        public Trees tree;
        public float basal_area;
        public float trees_pa;
        public float tree_scribner_gross;
        public float tree_scribner_net;
        public float tree_cubic_gross;
        public float tree_cubic_net;
        public SgCalc[] seg = new SgCalc[12];
        //private SqlCeConnection cn;
        //private GridView sg;
        //private int iMasterID;
        //private string sProject;
        //private string sTownship;
        //private string sRange;
        //private string sSection;
        //private string sTract;
        //private string sType;
        //private string sPlot;
        //private Utils utl = null;

        // Calc Values
        bool cull_segment;
        bool fiber_segment;
        bool west_routine;
        int y;
        int dash_or_plus, plus_status, plus_seg;
        public int calc_error;
        public int calc_segment;
        float c_baf;
        int c_bole_length;
        int c_form_point;
        float c_d4h;
        float c_form_factor;
        float bark_thickness;
        float asubo, lsubo;
        public float trees_per_acre;
        float basal_area_per_acre;
        float diameter_top;
        float total_tree_height;
        float dib_at_d4h, dib_at_form_point;
        float slope_calc, c_a, c_b, c_c;
        float mtd_height, merchantable_height;
        float merchantable_diameter;
        int log_index, log_max;
        int seg_index, cvol_index;
        string[] segment_string = new string[12];
        string segment_sort, segment_grade;
        string segment_length;
        float seg_length;
        float bf_ded_feet, bf_ded_inches, cf_ded_feet, cf_ded_inches;
        string deduction_code;
        float c_percent;
        float bf_ded_percent, cf_ded_percent;
        float east_side_butt_length, east_side_next_butt;
        float[] segment_whole_half = new float[12];
        float trim_allowed;
        float[] log_log_length = new float[20];
        float[] seg_log_length = new float[20];
        float[] seg_log_top = new float[20];
        float[] seg_log_butt = new float[20];
        float taper_top, taper_butt;
        float taper_difference;

        float national_diameter;
        float national_diameter_rounded;
        float sweyco_diameter;
        float sweyco_diameter_rounded;
        float seg_top_height, seg_butt_height;
        float seg_top_diameter, seg_butt_diameter;
        float real_value;
        float original_length;
        float segment_actual_length;
        int seg_log_index, seg_log_max;
        float graded_logs;
        float graded_butt_dia;
        float graded_top_dia;
        float log_length, log_butt_diameter, log_top_diameter;
        float log_scribner;
        float log_net_scribner;
        float log_cubic;
        float log_net_cubic;
        string temp_species;

        // Species Values
        private string spcBarkThickness;
        private string spcAo;
        //private string spcAlphaAo = null;
        private string spcFF16 = null;
        private string spcScribnerScale;
        private string spcCubicScale;
        private string spcMinTopDia;
        private string spcMinLogLen;
        private string spcMaxLogLen;
        private string spcTrim;

        // Sort Values
        private string srtFiber;
        private string srtMinDia;
        private string srtMinLen;
        private string srtMinVol;
        private string srtMaxButt;

        // Grade Values
        private string grdFiber;
        private string grdMinDia;
        private string grdMinLen;
        private string grdMinVol;
        private string grdMaxButt;

        // Master Values
        private string masterAcres = null;
        private string masterStripTreeise = null;
        private string masterFixedRadius = null;
        private string masterBaf = null;
        private int masterAge = 0;
        private string masterDbhOpt = null;

        public int CalcVolume(bool bErrors)
        {
            LookupMasterValues();
            LookupSpeciesValues();
            ComputeTreeAndBasalArea();
            if (masterAge > 0)
                ComputeASubo(masterAge);
            else
                ComputeASubo(40);

            compute_stem();
            compute_log_adjustment_factor();
            process_each_segment();

            tree.BdFtGrossVolume1 = (double)seg[0].scribner_gross;
            tree.BdFtNetVolume1 = seg[0].scribner_net;
            tree.CuFtGrossVolume1 = seg[0].cubic_gross;
            tree.CuFtNetVolume1 = seg[0].cubic_net;

            tree.BdFtGrossVolume2 = seg[1].scribner_gross;
            tree.BdFtNetVolume2 = seg[1].scribner_net;
            tree.CuFtGrossVolume2 = seg[1].cubic_gross;
            tree.CuFtNetVolume2 = seg[1].cubic_net;

            tree.BdFtGrossVolume3 = seg[2].scribner_gross;
            tree.BdFtNetVolume3 = seg[2].scribner_net;
            tree.CuFtGrossVolume3 = seg[2].cubic_gross;
            tree.CuFtNetVolume3 = seg[2].cubic_net;

            tree.BdFtGrossVolume4 = seg[3].scribner_gross;
            tree.BdFtNetVolume4 = seg[3].scribner_net;
            tree.CuFtGrossVolume4 = seg[3].cubic_gross;
            tree.CuFtNetVolume4 = seg[3].cubic_net;

            tree.BdFtGrossVolume5 = seg[4].scribner_gross;
            tree.BdFtNetVolume5 = seg[4].scribner_net;
            tree.CuFtGrossVolume5 = seg[4].cubic_gross;
            tree.CuFtNetVolume5 = seg[4].cubic_net;

            tree.BdFtGrossVolume6 = seg[5].scribner_gross;
            tree.BdFtNetVolume6 = seg[5].scribner_net;
            tree.CuFtGrossVolume6 = seg[5].cubic_gross;
            tree.CuFtNetVolume6 = seg[5].cubic_net;

            tree.BdFtGrossVolume7 = seg[6].scribner_gross;
            tree.BdFtNetVolume7 = seg[6].scribner_net;
            tree.CuFtGrossVolume7 = seg[6].cubic_gross;
            tree.CuFtNetVolume7 = seg[6].cubic_net;

            tree.BdFtGrossVolume8 = seg[7].scribner_gross;
            tree.BdFtNetVolume8 = seg[7].scribner_net;
            tree.CuFtGrossVolume8 = seg[7].cubic_gross;
            tree.CuFtNetVolume8 = seg[7].cubic_net;

            tree.BdFtGrossVloume9 = seg[8].scribner_gross;
            tree.BdFtNetVolume9 = seg[8].scribner_net;
            tree.CuFtGrossVolume9 = seg[8].cubic_gross;
            tree.CuFtNetVolume9 = seg[8].cubic_net;

            tree.BdFtGrossVolume10 = seg[9].scribner_gross;
            tree.BdFtNetVolume10 = seg[9].scribner_net;
            tree.CuFtGrossVolume10 = seg[9].cubic_gross;
            tree.CuFtNetVolume10 = seg[9].cubic_net;

            tree.BdFtGrossVolume11 = seg[10].scribner_gross;
            tree.BdFtNetVolume = seg[10].scribner_net;
            tree.CuFtGrossVolume11 = seg[10].cubic_gross;
            tree.CuFtNetVolume11 = seg[10].cubic_net;

            tree.BdFtGrossVolume12 = seg[11].scribner_gross;
            tree.BdFtNetVolume12 = seg[11].scribner_net;
            tree.CuFtGrossVolume12 = seg[11].cubic_gross;
            tree.CuFtNetVolume12 = seg[11].cubic_net;

            //tree.TotalBdFtGross = tree_scribner_gross;
            //tree.TotalBdFtNet = tree_scribner_net;

            //tree.TotalCuFtGross = tree_cubic_gross;
            //tree.TotalCuFtNet = tree_cubic_net;

            //tree.TreesPerAcre = trees_per_acre;
            //tree.BasalArea = basal_area_per_acre;

            //if (bErrors) 
            //	DisplayCalcError(calc_error);
            return (calc_error);
        }

        public void DisplayCalcError(int err)
        {
            int iSegOffSet = (calc_segment * 8) - 8;
            switch (err)
            {
                case 1:
                    MessageBox.Show("Under Min Dia On Seg = " + calc_segment);
                    break;
                case 2:
                    MessageBox.Show("Under Min Len On Seg = " + calc_segment);
                    break;
                case 3:
                    MessageBox.Show("Under Min Len For Grade On Seg = " + calc_segment);
                    break;
                case 4:
                    MessageBox.Show("Under Min Dia For Grade On Seg = " + calc_segment);
                    break;
                case 5:
                    MessageBox.Show("Under Min Len For Sort On Seg = " + calc_segment);
                    break;
                case 6:
                    MessageBox.Show("Under Min Dia For Sort On Seg = " + calc_segment);
                    break;
                case 7:
                    MessageBox.Show("Over Max Len On Seg =" + calc_segment);
                    break;
                case 8:
                    MessageBox.Show("Over Max Butt Dia For Grade On Seg = " + calc_segment);
                    break;
                case 9:
                    MessageBox.Show("Over Max Butt Dia For Sort On Seg =" + calc_segment);
                    break;
                case 10:
                    MessageBox.Show("Sort/Grade is Invalid For Seg = " + calc_segment);
                    break;
                case 11:
                    MessageBox.Show("Under Min Dia For Seg = " + calc_segment);
                    break;
                case 12:
                    MessageBox.Show("Under Min Len For Seg = " + calc_segment);
                    break;
            }
        }

        private void LookupMasterValues()
        {
            Stands aRec = tree.UpToPlotsByPlotID.UpToStandsByStandsID;
            AgecodesCollection aColl = new AgecodesCollection();
            aColl.Query.Where(aColl.Query.StandsID == aRec.StandsID).OrderBy(aColl.Query.AgeCodeOrder.Ascending);
            aColl.Query.Load();
            masterAge = GetAge(aColl, (int)tree.AgeCode);

            PlotfactorCollection pColl = new PlotfactorCollection();
            pColl.Query.Where(pColl.Query.StandsID == aRec.StandsID).OrderBy(pColl.Query.PlotFactorCode.Ascending);
            pColl.Query.Load();
            if (!string.IsNullOrEmpty(tree.PlotFactorInput))
            {
                if (tree.PlotFactorInput[0] == 'S')
                    masterStripTreeise = GetPlotFactor(pColl, tree.PlotFactorInput);
                if (tree.PlotFactorInput[0] == 'F')
                    masterFixedRadius = GetPlotFactor(pColl, tree.PlotFactorInput);
                if (tree.PlotFactorInput[0] == 'B')
                    masterBaf = GetPlotFactor(pColl, tree.PlotFactorInput);
            }
            //masterDbhOpt = dr["D4hOpt"].ToString();
            masterAcres = aRec.NetGeographicAcres.ToString();
        }

        private string GetPlotFactor(PlotfactorCollection pColl, string pCode)
        {
            foreach (Plotfactor item in pColl)
            {
                if (item.PlotFactorCode == pCode)
                    return item.Input1.ToString();
            }
            return string.Empty;
        }

        private int GetAge(AgecodesCollection aColl, int pAge)
        {
            foreach (Agecodes item in aColl)
            {
                if (item.AgeCodeOrder == pAge)
                    return (int)item.AgeCodeValue;
            }
            return 0;
        }

        private void LookupSpeciesValues()
        {
            Species aRec = new Species();
            aRec.Query.Where(aRec.Query.TableName == tree.UpToPlotsByPlotID.UpToStandsByStandsID.UpToProjectsByProjectsID.SpeciesTableName && aRec.Query.Abbreviation == tree.SpeciesAbbreviation);
            if (aRec.Query.Load())
            {
                spcTrim = aRec.Abbreviation;
                spcMinTopDia = aRec.MinDia.ToString();
                spcMinLogLen = aRec.MinLen.ToString();
                spcMaxLogLen = aRec.MaxLen.ToString();
                spcFF16 = aRec.FormFactorsDisplayCode;
                //spcBarkThickness = aRec.BarkFactorsDisplayCode;
                this.bark_thickness = (float)aRec.BarkRatio; // Utils.ConvertToFloat(spcBarkThickness);
                //spcAo = aRec.A.ASubo.ToString().Trim();
                //this.asubo = Utils.ConvertToFloat(spcAo);

                BoardfootrulesCollection bColl = new BoardfootrulesCollection();
                bColl.Query.Where(bColl.Query.TableName == aRec.BoardFootRuleTableName);
                if (bColl.Query.Load())
                {
                    switch (bColl[0].BoardFootVolumeMethod)
                    {
                        case "West":
                            spcScribnerScale = "W";
                            break;
                        case "East":
                            spcScribnerScale = "E";
                            break;
                        case "Idaho":
                            spcScribnerScale = "I";
                            break;
                    }
                }
                CubicfootrulesCollection cColl = new CubicfootrulesCollection();
                cColl.Query.Where(cColl.Query.TableName == aRec.CubicFootRuleTableName);
                if (cColl.Query.Load())
                {
                    switch (cColl[0].CubicVolumeFormula)
                    {
                        case "NorthWest":
                            spcCubicScale = "1";
                            break;
                        case "Smalian":
                            spcCubicScale = "S";
                            break;
                    }
                }
            }
        }

        private void LookupSortValues(string sTmp)
        {
            Sortsandgrades aRec = new Sortsandgrades();
            string table = tree.UpToPlotsByPlotID.UpToStandsByStandsID.UpToProjectsByProjectsID.SortsAndGradesTableName;
            aRec.Query.Where(aRec.Query.TableName == table && aRec.Query.InputCode == sTmp && aRec.Query.SortOrGrade == "S");
            if (aRec.Query.Load())
            {
                //srtFiber = aRec.Fiber;
                srtMinDia = aRec.MinimumDiameter.ToString();
                srtMinLen = aRec.MinimumLength.ToString();
                srtMinVol = aRec.MinimumBDFTPerLog.ToString();
                srtMaxButt = aRec.MaximumDiameter.ToString();
            }
        }

        private void LookupGradeValues(string sTmp)
        {
            Sortsandgrades aRec = new Sortsandgrades();
            string table = tree.UpToPlotsByPlotID.UpToStandsByStandsID.UpToProjectsByProjectsID.SortsAndGradesTableName;
            aRec.Query.Where(aRec.Query.TableName == table && aRec.Query.InputCode == sTmp && aRec.Query.SortOrGrade == "G");
            if (aRec.Query.Load())
            {
                //grdFiber = aRec.Fiber;
                grdMinDia = aRec.MaximumDiameter.ToString();
                grdMinLen = aRec.MinimumLength.ToString();
                grdMinVol = aRec.MinimumBDFTPerLog.ToString();
                grdMaxButt = aRec.MaximumDiameter.ToString();
            }
        }

        private void process_log()
        {
            float bump_log_length;
            seg_length = seg_log_length[seg_log_index];
            seg_top_height = seg_butt_height + seg_log_length[seg_log_index] +
                trim_allowed;
            seg_top_diameter = computed_diameter(seg_top_height);

            graded_logs = 0;
            graded_butt_dia = seg_butt_diameter;
            graded_top_dia = seg_top_diameter;

            if (west_routine == true)
            {
                taper_top = seg_top_diameter;
                taper_butt = seg_butt_diameter;
                compute_west_side_log_length();
            }
            else
            {
                //taper_top = (float)Math.Round(seg_top_diameter);
                //taper_butt = (float)Math.Round(seg_butt_diameter);
                taper_top = seg_top_diameter;
                taper_butt = seg_butt_diameter;
                compute_east_side_log_length();
            }

            seg_log_top[0] = taper_top;
            seg_log_butt[0] = taper_butt;
            if (log_max > 0)
            {
                switch (spcScribnerScale)
                {
                    case "W":
                    case "1":
                    case "D":
                        figure_west_taper();
                        break;
                    case "E":
                    case "O":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_east_taper_butt((int)taper_butt, (int)taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_east_taper_2nd();
                        break;
                    case "I":
                    case "T":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_idaho_taper_butt(taper_butt, taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_east_taper_2nd();
                        break;
                    case "B":
                        if (seg_index == 0 && seg_log_index == 0)
                        {
                            figure_bia_taper_butt(taper_butt, taper_top);
                            if (east_side_butt_length > seg_length)
                            {
                                seg_top_diameter = seg_log_top[log_max - 1];
                                east_side_next_butt = seg_top_diameter;
                            }
                            if (deduction_code != "-")
                                east_side_next_butt = 0;
                        }
                        else
                            figure_bia_taper_2nd();
                        break;
                }
            }
            graded_butt_dia = seg_log_butt[0];
            graded_top_dia = seg_log_top[log_max - 1];

            //*    process   log     data     
            log_index = 0;
            bump_log_length = seg_butt_height;

            if (calc_error == 0)
            {
                if (dash_or_plus != 2)
                {
                    /*
                    if (mb_opt) 
                    {
                        if (!check_sort_grade_combo(segment_sort,segment_grade,(int)Math.Round(seg_length),(int)Math.Round(seg_top_diameter))) 
                        {
                            check_grade_len_top_butt(segment_grade, Math.Round(seg_length), Math.Round(seg_top_diameter), Math.Round(seg_butt_diameter));
                            check_sort_len_top_butt(segment_sort, Math.Round(seg_length), Math.Round(seg_top_diameter), Math.Round(seg_butt_diameter));
                        }
                    }
                    else 
                    {
                        check_grade_len_top_butt(segment_grade, Math.Round(seg_length), Math.Round(seg_top_diameter), Math.Round(seg_butt_diameter));
                        check_sort_len_top_butt(segment_sort, Math.Round(seg_length), Math.Round(seg_top_diameter), Math.Round(seg_butt_diameter));
                    }
                    */
                    check_grade_len_top_butt(segment_grade, (int)Math.Round(seg_length), (int)Math.Round(seg_top_diameter), (int)Math.Round(seg_butt_diameter));
                    check_sort_len_top_butt(segment_sort, (int)Math.Round(seg_length), (int)Math.Round(seg_top_diameter), (int)Math.Round(seg_butt_diameter));
                    if (calc_error > 0)
                        calc_segment = cvol_index + 1;
                }
            }

            while (log_index < log_max)
            {
                CalcDiameters();
                log_length = log_log_length[log_index];
                log_top_diameter = seg_log_top[log_index];
                log_butt_diameter = seg_log_butt[log_index];

                if (log_index < log_max && spcCubicScale == "2")  // sweyco
                {
                    bump_log_length += log_length;
                    log_top_diameter = (float)Math.Round(computed_diameter(bump_log_length));
                }

                log_scribner = 0;
                log_net_scribner = 0;
                log_cubic = 0;
                log_net_cubic = 0;

                figure_scribner();

                if ((log_index + 1) == log_max)
                    figure_cubic();

                //*  create graded record   
                if (log_max > 0)
                    graded_logs =
                        graded_logs + segment_whole_half[seg_index] / log_max;
                log_index++;
            }
        }

        private int IdahoTaper(int dLen)
        {
            int tFeet = 0, xq = 0, xr = 0;


            if (tree.SpeciesAbbreviation == "L" ||
                tree.SpeciesAbbreviation == "LP")
            {
                if (dLen > 20 && dLen < 49)
                    tFeet = log_max;
                if (dLen > 48 && dLen < 61)
                    tFeet = (log_max - 1) + 2;
            }
            else
                if (tree.SpeciesAbbreviation == "RC")
                {
                    if (dLen > 20 && dLen < 41)
                        tFeet = log_max * 2;
                }
                else
                {
                    if (dLen > 20 && dLen < 41)
                    {
                        xq = DivRem((int)seg_top_diameter, 2, out xr);
                        if (xr == 0)
                            tFeet = log_max * 2;
                        else
                            tFeet = log_max;
                    }
                }
            return tFeet;
        }

        public int DefaultTaper(int dLen)
        {
            int tFeet = 0;

            string strSpecies = "**";
            if (tree.SpeciesAbbreviation == "L")
                strSpecies = tree.SpeciesAbbreviation;
            tFeet = GetTaper(strSpecies, dLen);
            return tFeet;
        }

        private int GetTaper(string strSpecies, int iLen)
        {
            if (strSpecies == "**")
            {
                if (iLen >= 21 && iLen <= 31)
                    return (2);
                if (iLen >= 32 && iLen <= 46)
                    return (4);
                if (iLen >= 47 && iLen <= 54)
                    return (6);
                if (iLen >= 55 && iLen <= 64)
                    return (7);
                if (iLen >= 65 && iLen <= 72)
                    return (8);
                if (iLen >= 73 && iLen <= 80)
                    return (9);
                if (iLen >= 81 && iLen <= 88)
                    return (10);
                if (iLen >= 89 && iLen <= 99)
                    return (11);
            }
            if (strSpecies == "L")
            {
                if (iLen >= 21 && iLen <= 41)
                    return (2);
                if (iLen >= 42 && iLen <= 50)
                    return (3);
                if (iLen >= 51 && iLen <= 60)
                    return (4);
                if (iLen >= 61 && iLen <= 80)
                    return (5);
            }
            if (strSpecies == "LP")
            {
                if (iLen >= 21 && iLen <= 41)
                    return (2);
                if (iLen >= 42 && iLen <= 50)
                    return (3);
                if (iLen >= 51 && iLen <= 60)
                    return (4);
            }
            return (0);
        }

        private void CalcDiameters()
        {
            int TaperRemain = 0, TaperLen = 0, ActualTaper = 0;
            int Dia2Actual = 0, Dia3Actual = 0;
            int iSegs = 0;
            int wGrDia1 = 0, wGrDia2 = 0;
            int wNtDia1 = 0, wNtDia2 = 0;
            int tGrDia1 = 0, tGrDia2 = 0, tGrDia3 = 0;
            int tGrLen1 = 0, tGrLen2 = 0, tGrLen3 = 0;
            int tDia, tLen, tButt;
            int xq, xr, yq, yr;

            //tButt = (int)seg_butt_diameter;
            tButt = 0;
            tLen = (int)seg_length;
            iSegs = log_max;

            if (spcScribnerScale == "W")
            {
                tDia = (int)seg_top_diameter;
                tGrLen1 = (int)log_log_length[0];
                tGrLen2 = (int)log_log_length[1];
                tGrLen3 = (int)log_log_length[2];
            }
            else
            {
                tDia = (int)Math.Round(seg_top_diameter);
                switch (log_max)
                {
                    case 1:
                        tGrLen1 = (int)log_log_length[0];
                        break;
                    case 2:
                        tGrLen1 = (int)log_log_length[1];
                        tGrLen2 = (int)log_log_length[0];
                        break;
                    case 3:
                        tGrLen1 = (int)log_log_length[2];
                        tGrLen2 = (int)log_log_length[1];
                        tGrLen3 = (int)log_log_length[0];
                        break;

                }
            }

            if (tDia < 1)
                return;

            // if no butt diameter use default taper
            if (tButt == 0)
            {
                if (spcScribnerScale == "W")
                    TaperLen = 10;
                if (spcScribnerScale == "E")
                {
                    if (tLen > 20)
                    {
                        TaperLen = DefaultTaper(tLen);
                    }
                }
                if (spcScribnerScale == "I")
                {
                    if (tLen > 20)
                    {
                        TaperLen = IdahoTaper(tLen);
                    }
                }
            }

            //* ----- calculate actual taper for segments ----- 
            if (tButt > 0 || spcScribnerScale == "E")
            {
                if (spcScribnerScale == "E" && tButt == 0)
                    ActualTaper = TaperLen;
                if (spcScribnerScale == "E" && tButt > 0)
                    ActualTaper = tButt - tDia;
                if (spcScribnerScale == "W")
                    ActualTaper = tButt - tDia;
                if (spcScribnerScale == "D")
                    ActualTaper = tButt - tDia;
                TaperRemain = ActualTaper;
                xq = DivRem(ActualTaper, iSegs, out xr);
                //* ----- distribution of even taper -----
                if (xr == 0)
                {
                    if (iSegs == 2)
                    {
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia2Actual = xq;
                    }
                    if (iSegs == 3)
                    {
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia2Actual = xq;
                        TaperRemain = TaperRemain - xq;
                        if (TaperRemain > 0)
                            Dia3Actual = xq;
                    }
                }
                /* ----- distribution of uneven taper ----- */
                if (xr > 0)
                {
                    ActualTaper++;
                    TaperRemain = ActualTaper;
                    if (iSegs == 2)
                    {
                        yq = DivRem(ActualTaper, iSegs, out yr);
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia2Actual = yq;
                    }
                    if (iSegs == 3)
                    {
                        yq = DivRem(ActualTaper, iSegs, out yr);
                        if (yr > 0)
                            yq++;
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia2Actual = yq;
                        yq = DivRem(TaperRemain, 2, out yr);
                        TaperRemain = TaperRemain - yq;
                        if (TaperRemain > 0)
                            Dia3Actual = yq;
                    }
                }
            }
            /* ----- segment 1 ----- */
            tGrDia1 = tDia;
            /* ----- segment 2 ----- */
            if (iSegs > 1)
            {
                tGrDia2 = tDia;
                if (tButt == 0 && spcScribnerScale == "W")
                {
                    xq = DivRem(tGrLen1, TaperLen, out xr);
                    tGrDia2 = tGrDia2 + xq;
                }
                if (tButt == 0 && spcScribnerScale == "D")
                {
                    xq = DivRem(tGrLen1, TaperLen, out xr);
                    tGrDia2 = tGrDia2 + xq;
                }
                if (tButt > 0 || spcScribnerScale == "E")
                {
                    tGrDia2 = tGrDia2 + Dia2Actual;
                }
            }
            /* ----- segment 3 ----- */
            if (iSegs > 2)
            {
                tGrDia3 = tDia;
                if (tButt == 0 && spcScribnerScale == "W")
                {
                    xq = DivRem(tGrLen1 + tGrLen2, TaperLen, out xr);
                    tGrDia3 = tGrDia3 + xq;
                }
                if (tButt == 0 && spcScribnerScale == "D")
                {
                    xq = DivRem(tGrLen1 + tGrLen2, TaperLen, out xr);
                    tGrDia3 = tGrDia3 + xq;
                }
                if (tButt > 0 || spcScribnerScale == "E")
                {
                    tGrDia3 = tGrDia3 + Dia2Actual;
                    tGrDia3 = tGrDia3 + Dia3Actual;
                }
            }
            if (spcScribnerScale == "W")
            {
                /* ----- segment 1 net ----- */
                seg_log_top[0] = tGrDia1;
                /* ----- segment 2 net ----- */
                if (iSegs > 1)
                {
                    seg_log_top[1] = tGrDia2;
                }
                /* ----- segment 3 net ----- */
                if (iSegs > 2)
                {
                    seg_log_top[2] = tGrDia3;
                }
            }
            else
            {
                switch (log_max)
                {
                    case 1:
                        seg_log_top[0] = tGrDia1;
                        break;
                    case 2:
                        seg_log_top[1] = tGrDia1;
                        seg_log_top[0] = tGrDia2;
                        break;
                    case 3:
                        seg_log_top[2] = tGrDia1;
                        seg_log_top[1] = tGrDia2;
                        seg_log_top[0] = tGrDia3;
                        break;
                }
            }
        }

        private void ComputeASubo(int tage)
        {
            double ttage;
            string[] ao_species = new string[9];
            double[] ao_a = new double[9];
            double[] ao_b = new double[9];
            double[] ao_c = new double[9];
            ao_species[0] = "DF";
            ao_a[0] = 0.64620566960;
            ao_b[0] = -0.0003666245662;
            ao_c[0] = -6.15389545900;
            ao_species[1] = "LP";
            ao_a[1] = 0.64620566960;
            ao_b[1] = -0.0003666245662;
            ao_c[1] = -6.15389545900;
            ao_species[2] = "WH";
            ao_a[2] = 0.69429326230;
            ao_b[2] = -0.0005374096360;
            ao_c[2] = -6.75006589600;
            ao_species[3] = "RA";
            ao_a[3] = 0.68043852280;
            ao_b[3] = -0.0007233655182;
            ao_c[3] = -4.95080564100;
            ao_species[4] = "PP";
            ao_a[4] = 0.58134710670;
            ao_b[4] = 0.0003879441574;
            ao_c[4] = -11.14531440000;
            ao_species[5] = "SS";
            ao_a[5] = 0.59176427320;
            ao_b[5] = -0.0002486198821;
            ao_c[5] = -4.43475477700;
            ao_species[6] = "SF";
            ao_a[6] = 0.59176427320;
            ao_b[6] = -0.0002486198821;
            ao_c[6] = -4.43475477700;
            ao_species[7] = "RC";
            ao_a[7] = 0.59176427320;
            ao_b[7] = -0.0002486198821;
            ao_c[7] = -4.43475477700;
            ao_species[8] = "NF";
            ao_a[8] = 0.89086009290;
            ao_b[8] = -0.0004617957011;
            ao_c[8] = -18.89577825000;

            asubo = Utils.ConvertToFloat(spcAo);
            if (asubo == 0)
            {
                ttage = (double)masterAge;
                if (ttage < 30)
                    ttage = 30;
                for (int i = 0; i < 9; i++)
                {
                    if (spcAo == ao_species[i])
                    {
                        double dTmp = ao_a[i] + (ao_b[i] * ttage) + (ao_c[i] * (1.00 / ttage));
                        spcAo = dTmp.ToString();
                        asubo = (float)dTmp;
                        return;
                    }
                }
            }
        }

        private void ComputeTreeAndBasalArea()
        {
            float save_form_factor;

            save_form_factor = 0.0f;
            if (spcScribnerScale == "W" || spcScribnerScale == "1")
                west_routine = true;

            c_bole_length = (int)tree.BoleHeight;
            c_d4h = (float)tree.Dbh;
            c_form_factor = (float)tree.ReportedFormFactor * (float)0.01;
            c_form_point = (int)tree.FormPoint;

            if (masterDbhOpt == "Y")
                c_d4h = c_d4h / c_form_factor;

            if (c_form_point == 4 && c_form_factor == 0)
                c_form_factor = Utils.ConvertToFloat(spcFF16);
            if (c_form_point == 4)
            {
                save_form_factor = c_form_factor;
                c_form_factor = 1.00f;
                c_form_point = 16;
            }

            //sub = (sg.Cells[sg.ActiveRowIndex, 1].Text[1] - 49);

            if (tree.UpToPlotsByPlotID.UserPlotNumber[0] == '-')
            {
                if (Utils.ConvertToFloat(masterAcres) > 0)
                    trees_per_acre = (float)1.0 / Utils.ConvertToFloat(masterAcres);
                else
                    trees_per_acre = 0;
                basal_area_per_acre = (float)0.005454 * (float)Square(c_d4h) * trees_per_acre;
                basal_area = basal_area_per_acre;
                trees_pa = trees_per_acre;
                if (save_form_factor > 0)
                    c_form_factor = save_form_factor;
                return;
            }

            switch (tree.PlotFactorInput[0])
            {
                case 'S':
                    if (Utils.ConvertToFloat(masterAcres) > 0)
                        trees_per_acre = Utils.ConvertToFloat(masterStripTreeise) / Utils.ConvertToFloat(masterAcres);
                    else
                        trees_per_acre = 0;
                    basal_area_per_acre = (float)0.005454 * (float)Square(c_d4h) * trees_per_acre;
                    break;
                case 'F':
                    if (Utils.ConvertToFloat(masterFixedRadius) > 0)
                        trees_per_acre = (float)1.0 / Utils.ConvertToFloat(masterFixedRadius);
                    else
                        trees_per_acre = 0;
                    basal_area_per_acre = (float)0.005454 * (float)Square(c_d4h) * trees_per_acre;
                    break;
                default:
                    if (tree.PlotFactorInput[0] == 'B')
                        c_baf = Utils.ConvertToFloat(masterBaf);
                    else
                        c_baf = Utils.ConvertToFloat(tree.PlotFactorInput);
                    trees_per_acre = 0;
                    if (c_d4h > 0 && c_form_factor > 0)
                        trees_per_acre =
                            c_baf / ((float)Square(c_d4h) * (float)Square(c_form_factor) * (float)0.005454);
                    basal_area_per_acre = 0;
                    if (c_form_factor > 0)
                        basal_area_per_acre = c_baf / ((float)Square(c_form_factor));
                    break;
            }
            basal_area = basal_area_per_acre;
            trees_pa = trees_per_acre;
            if (save_form_factor > 0)
                c_form_factor = save_form_factor;
        }

        private void compute_stem()
        {
            int modified_bole_length = c_bole_length;
            int modified_form_point = c_form_point;
            int detail_bole_len = c_bole_length;

            dib_at_d4h = c_d4h * bark_thickness;
            dib_at_form_point = dib_at_d4h * c_form_factor;

            if (!string.IsNullOrEmpty(tree.TopDiameterFractionCode))
            {
                if (tree.TopDiameterFractionCode[0] >= 'A' && tree.TopDiameterFractionCode[0] <= 'Z')
                {
                    diameter_top = tree.TopDiameterFractionCode[0] - 64;
                    diameter_top *= bark_thickness;
                }
                if (Utils.IsDigit(tree.TopDiameterFractionCode[0]))
                    diameter_top = dib_at_form_point * (float)(tree.TopDiameterFractionCode[0] - 48) * (float)0.1;
                if (tree.TopDiameterFractionCode[0] == '-')
                {
                    if (c_d4h < 15)
                        diameter_top = 5.0f;
                    else
                    {
                        diameter_top = (float)2.24 + (float)0.184 * c_d4h;
                        diameter_top *= bark_thickness;
                    }
                }

                if (tree.TopDiameterFractionCode[0] == '+')
                {
                    if (detail_bole_len <= 14)
                    {
                        detail_bole_len += 15;
                        c_bole_length = detail_bole_len;
                    }
                    diameter_top = predict_top_diam(c_form_factor * 100.0f * bark_thickness,
                        c_d4h, c_bole_length / 10.0f);
                    detail_bole_len = detail_bole_len * 16 / 10;
                    modified_bole_length = detail_bole_len;
                    c_bole_length = detail_bole_len;
                }
            }

            //* lsubo is length from form point to tip of tree 
            if (modified_bole_length == modified_form_point)
                modified_bole_length++;

            lsubo = 0;
            if (dib_at_form_point == diameter_top)
                lsubo = 1.0f;
            else
                lsubo = (modified_bole_length - modified_form_point) *
                    (dib_at_form_point - diameter_top * asubo) /
                    (dib_at_form_point - diameter_top);

            total_tree_height = lsubo + modified_form_point;
            if (total_tree_height > 365)
                total_tree_height = c_bole_length;
            if (total_tree_height > (4 * c_bole_length))
                total_tree_height = c_bole_length;
            slope_calc = dib_at_form_point * (asubo - 1.0f) / lsubo;

            if (modified_form_point == 4)
                modified_form_point++;

            c_a = 0;
            c_a = ((modified_form_point - 4) * slope_calc +
                dib_at_d4h - dib_at_form_point) / ((float)Square((modified_form_point - 4f)));

            if (c_a > 0.0)
            {
                c_b = slope_calc - 2.0f * modified_form_point * c_a;
                c_c = dib_at_d4h - 4.0f * c_b - 16.0f * c_a;
            }
            else
            {
                c_a = 0;
                c_b = (dib_at_form_point - dib_at_d4h) / (modified_form_point - 4.0f);
                c_c = dib_at_d4h - 4.0f * c_b;
            }

            //* make diameter limit for logs  
            merchantable_diameter = Utils.ConvertToFloat(spcMinTopDia);

            if (diameter_top > Utils.ConvertToFloat(spcMinTopDia))
                merchantable_diameter = diameter_top;

            mtd_height = computed_height(Utils.ConvertToFloat(spcMinTopDia));
            merchantable_height = computed_height(merchantable_diameter);
            national_diameter = computed_diameter(4.0f);
            sweyco_diameter = computed_diameter(1.5f);
            sweyco_diameter_rounded = (float)Math.Round(sweyco_diameter);
            if (west_routine == true)
                national_diameter_rounded = national_diameter;
            else
                national_diameter_rounded = (float)Math.Round(national_diameter);
        }
       
        private float predict_top_diam(float d3_fc, float d4_d4h, float d5_ls)
        {
            float slope_const = 0.00001244f;
            float slope_intercept = 0.0010754f;
            float c7_slope, c8_intercept, c11_slope, c12_intercept, c15, td;

            c7_slope = 0.001045f - (d3_fc - 65) * slope_const;
            c8_intercept = 0.094013f - (d3_fc - 65) * slope_intercept;
            c11_slope = 2.0f * (d5_ls - 1.0f) * c7_slope;
            c12_intercept = 1.0f - 2.0f * (d5_ls - 1.0f) * c8_intercept;
            c15 = c11_slope * d4_d4h + c12_intercept;
            td = c15 * d3_fc * d4_d4h * 0.01f;
            return (td);
        }

        private void process_each_segment()
        {
            int x;
            int iCol = 22;

            seg_index = 0;
            seg_top_height = 0;
            seg_top_diameter = 0;
            seg_butt_diameter = 0;
            cvol_index = 0;

            while (seg_index < 12)
            {
                switch(seg_index)
                {
                    case 0:
                        if (string.IsNullOrEmpty(tree.SortCode1))
                            return;
                        segment_sort = tree.SortCode1;
                        segment_grade = tree.GradeCode1;
                        segment_length = tree.Length1.ToString();

                        bf_ded_feet = (float)tree.BdFtLD1;
                        bf_ded_inches = (float)tree.BdFtDD1;
                        cf_ded_feet = (float)tree.CuFtLD1;
                        cf_ded_inches = (float)tree.CuFtDD1;
                        deduction_code = tree.BdFtPD1.ToString();
                        break;
                    case 1:
                        if (string.IsNullOrEmpty(tree.SortCode2))
                            return;
                        segment_sort = tree.SortCode2;
                        segment_grade = tree.GradeCode2;
                        segment_length = tree.Length2.ToString();

                        bf_ded_feet = (float)tree.BdFtLD2;
                        bf_ded_inches = (float)tree.BdFtDD2;
                        cf_ded_feet = (float)tree.CuFtLD2;
                        cf_ded_inches = (float)tree.CuFtDD2;
                        deduction_code = tree.BdFtPD2.ToString();
                        break;
                    case 2:
                        if (string.IsNullOrEmpty(tree.SortCode3))
                            return;
                        segment_sort = tree.SortCode3;
                        segment_grade = tree.GradeCode3;
                        segment_length = tree.Length3.ToString();

                        bf_ded_feet = (float)tree.BdFtLD3;
                        bf_ded_inches = (float)tree.BdFtDD3;
                        cf_ded_feet = (float)tree.CuFtLD3;
                        cf_ded_inches = (float)tree.CuFtDD3;
                        deduction_code = tree.BdFtPD3.ToString();
                        break;
                    case 3:
                        if (string.IsNullOrEmpty(tree.SortCode4))
                            return;
                        segment_sort = tree.SortCode4;
                        segment_grade = tree.GradeCode4;
                        segment_length = tree.Length4.ToString();

                        bf_ded_feet = (float)tree.BdFtLD4;
                        bf_ded_inches = (float)tree.BdFtDD4;
                        cf_ded_feet = (float)tree.CuFtLD4;
                        cf_ded_inches = (float)tree.CuFtDD4;
                        deduction_code = tree.BdFtPD4.ToString();
                        break;
                    case 4:
                        if (string.IsNullOrEmpty(tree.SortCode5))
                            return;
                        segment_sort = tree.SortCode5;
                        segment_grade = tree.GradeCode5;
                        segment_length = tree.Length5.ToString();

                        bf_ded_feet = (float)tree.BdFtLD5;
                        bf_ded_inches = (float)tree.BdFtDD5;
                        cf_ded_feet = (float)tree.CuFtLD5;
                        cf_ded_inches = (float)tree.CuFtDD5;
                        deduction_code = tree.BdFtPD5.ToString();
                        break;
                    case 5:
                        if (string.IsNullOrEmpty(tree.SortCode6))
                            return;
                        segment_sort = tree.SortCode6;
                        segment_grade = tree.GradeCode6;
                        segment_length = tree.Length6.ToString();

                        bf_ded_feet = (float)tree.BdFtLD6;
                        bf_ded_inches = (float)tree.BdFtDD6;
                        cf_ded_feet = (float)tree.CuFtLD6;
                        cf_ded_inches = (float)tree.CuFtDD6;
                        deduction_code = tree.BdFtPD6.ToString();
                        break;
                    case 6:
                        if (string.IsNullOrEmpty(tree.SortCode7))
                            return;
                        segment_sort = tree.SortCode7;
                        segment_grade = tree.GradeCode7;
                        segment_length = tree.Length7.ToString();

                        bf_ded_feet = (float)tree.BdFtLD7;
                        bf_ded_inches = (float)tree.BdFtDD7;
                        cf_ded_feet = (float)tree.CuFtLD7;
                        cf_ded_inches = (float)tree.CuFtDD7;
                        deduction_code = tree.BdFtPD7.ToString();
                        break;
                    case 7:
                        if (string.IsNullOrEmpty(tree.SortCode8))
                            return;
                        segment_sort = tree.SortCode8;
                        segment_grade = tree.GradeCode8;
                        segment_length = tree.Length8.ToString();

                        bf_ded_feet = (float)tree.BdFtLD8;
                        bf_ded_inches = (float)tree.BdFtDD8;
                        cf_ded_feet = (float)tree.CuFtLD8;
                        cf_ded_inches = (float)tree.CuFtDD8;
                        deduction_code = tree.BdFtPD8.ToString();
                        break;
                    case 8:
                        if (string.IsNullOrEmpty(tree.SortCode9))
                            return;
                        segment_sort = tree.SortCode9;
                        segment_grade = tree.GradeCode9;
                        segment_length = tree.Length9.ToString();

                        bf_ded_feet = (float)tree.BdFtLD9;
                        bf_ded_inches = (float)tree.BdFtDD9;
                        cf_ded_feet = (float)tree.CuFtLD9;
                        cf_ded_inches = (float)tree.CuFtDD9;
                        deduction_code = tree.BdFtPD9.ToString();
                        break;
                    case 9:
                        if (string.IsNullOrEmpty(tree.SortCode10))
                            return;
                        segment_sort = tree.SortCode10;
                        segment_grade = tree.GradeCode10;
                        segment_length = tree.Length10.ToString();

                        bf_ded_feet = (float)tree.BdFtLD10;
                        bf_ded_inches = (float)tree.BdFtDD10;
                        cf_ded_feet = (float)tree.CuFtLD10;
                        cf_ded_inches = (float)tree.CuFtDD10;
                        deduction_code = tree.BdFtPD10.ToString();
                        break;
                    case 10:
                        if (string.IsNullOrEmpty(tree.SortCode11))
                            return;
                        segment_sort = tree.SortCode11;
                        segment_grade = tree.GradeCode11;
                        segment_length = tree.Length11.ToString();

                        bf_ded_feet = (float)tree.BdFtLD11;
                        bf_ded_inches = (float)tree.BdFtDD11;
                        cf_ded_feet = (float)tree.CuFtLD11;
                        cf_ded_inches = (float)tree.CuFtDD11;
                        deduction_code = tree.BdFtPD11.ToString();
                        break;
                    case 11:
                        if (string.IsNullOrEmpty(tree.SortCode12))
                            return;
                        segment_sort = tree.SortCode12;
                        segment_grade = tree.GradeCode12;
                        segment_length = tree.Length12.ToString();

                        bf_ded_feet = (float)tree.BdFtLD12;
                        bf_ded_inches = (float)tree.BdFtDD12;
                        cf_ded_feet = (float)tree.CuFtLD12;
                        cf_ded_inches = (float)tree.CuFtDD12;
                        deduction_code = tree.BdFtPD12.ToString();
                        break;
                }
                bf_ded_percent = bf_ded_feet * 10.0f + bf_ded_inches;
                cf_ded_percent = cf_ded_feet * 10.0f + cf_ded_inches;
                dash_or_plus = 0;
                if (segment_length.Contains("-"))
                {
                    original_length = 0.0f;
                    dash_or_plus = 1;
                }
                else
                    if (segment_length.Contains("+"))
                    {
                        original_length = 0.0f;
                        dash_or_plus = 2;
                    }
                    else
                        original_length = Utils.ConvertToInt(segment_length);
                seg_length = original_length;

                for (x = 0; x < 5; x++)
                {
                    seg_log_top[x] = 0.0f;
                    seg_log_butt[x] = 0.0f;
                }

                c_percent = 0.0f;
                if (deduction_code == "1")
                    c_percent = 1.0f;

                cull_segment = false;
                fiber_segment = false;
                trim_allowed = Utils.ConvertToFloat(spcTrim);

                LookupSortValues(segment_sort);
                LookupGradeValues(segment_grade);

                if (grdFiber == "F" || srtFiber == "F")
                    fiber_segment = true;

                if (segment_sort == "0" || segment_grade == "0")
                {
                    cull_segment = true;
                    trim_allowed = 0.0f;
                }

                if (segment_sort == "0")
                {
                    if (segment_grade != "0")
                    {
                        cull_segment = false;
                        trim_allowed = Utils.ConvertToFloat(spcTrim);
                    }
                }

                if (deduction_code == "-")
                    trim_allowed = 0;

                seg_butt_height = seg_top_height;
                seg_butt_diameter = computed_diameter(seg_butt_height);

                if (seg_index == 0)
                    east_side_next_butt = 0;

                if (west_routine == false)
                {
                    if (east_side_next_butt > 0 && seg_index == 1)
                    {
                        seg_butt_diameter = east_side_next_butt;
                        east_side_next_butt = 0;
                    }
                }

                if (original_length > 0)
                {
                    log_max = 1;
                    seg_top_height = seg_length + seg_butt_height;
                    if ((seg_top_height + log_max * trim_allowed) > mtd_height)
                    {
                        real_value = mtd_height - seg_butt_height;
                        recompute_segment_length(real_value);
                        if (seg_length < 0)
                            seg_length = 0;
                        seg_top_height = seg_length + seg_butt_height;
                    }
                    seg_top_height = seg_top_height + log_max * trim_allowed;
                }

                // calc dash dash
                if (original_length == 0 && dash_or_plus == 1)
                {
                    compute_segment_length(merchantable_height - seg_butt_height);
                    if (seg_length < 0)
                        seg_length = 0;
                    original_length = seg_length;
                    seg_top_height = seg_butt_height + seg_length + log_max * trim_allowed;
                }
                // calc plus plus
                if (original_length == 0 && dash_or_plus == 2)
                {
                    seg_length = compute_plus_length(seg_butt_diameter, seg_butt_height,
                        merchantable_height - seg_butt_height,
                        segment_sort, segment_grade);
                    plus_seg = seg_index;
                    log_max = 1;
                    if (seg_length < 0)
                        seg_length = 0;
                    original_length = seg_length;
                    seg_top_height = seg_butt_height + seg_length + log_max * trim_allowed;
                }

                if (segment_sort == "P" || segment_sort == "G")
                    seg_length = original_length;

                if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                {
                    cull_segment = true;
                    trim_allowed = 0;
                }

                if (seg_length < 0)
                    seg_length = 0;

                segment_actual_length = seg_length;
                if (west_routine == true)
                    compute_west_true_logs();
                else
                    compute_east_true_logs();

                seg_log_max = log_max;
                seg_log_index = 0;

                if (plus_status > 0)
                {
                    switch (plus_status)
                    {
                        case 1:
                            calc_error = 10;
                            break;
                        case 2:
                            calc_error = 11;
                            break;
                        case 3:
                            calc_error = 12;
                            break;
                    }
                    if (calc_error > 0)
                        calc_segment = plus_seg + 1;
                }

                while (seg_log_index < seg_log_max)
                {
                    process_log();
                    seg[cvol_index].tdia = seg_top_diameter;
                    seg[cvol_index].bdia = seg_butt_diameter;
                    seg[cvol_index].len = seg_length;
                    seg_log_index++;
                    cvol_index++;
                    seg_butt_height = seg_top_height;
                    seg_butt_diameter = seg_top_diameter;
                    national_diameter_rounded = 0;
                    sweyco_diameter_rounded = 0;
                }
                seg_index++;
            }
        }

        private float computed_height(float diameter)
        {
            float height_at_given_diameter, calc_value;

            if (diameter < dib_at_form_point)
            {       //* above form point 
                height_at_given_diameter = c_form_point +
                    (lsubo * (dib_at_form_point - diameter)) /
                    (dib_at_form_point - diameter * asubo);
                return (height_at_given_diameter);
            }

            if (c_a == 0.0)
            {                            //*  below form point  
                height_at_given_diameter = (c_c - diameter) * c_form_point /
                    (c_c - dib_at_form_point);
                return (height_at_given_diameter);
            }

            calc_value = (float)Square(c_b) - 4.0f * c_a * (c_c - diameter);
            height_at_given_diameter = (0.0f - c_b - (float)Math.Sqrt(calc_value)) / (2.0f * c_a);

            return (height_at_given_diameter);
        }

        private float computed_diameter(float height)
        {
            float diameter_at_given_height;


            if (height > c_form_point)
            {
                diameter_at_given_height = (dib_at_form_point *
                    (lsubo + c_form_point - height)) / (lsubo + asubo * (c_form_point - height));
                return (diameter_at_given_height);
            }
            diameter_at_given_height = c_a * (float)Square(height) + c_b * height + c_c;
            return (diameter_at_given_height);
        }

        /*
        private int remainder(int x, int y)
        {
            int z = x / y;
            return(x - z * y);
        }

        private float round(float x)
        {
            x += 0.5f;
            return((int)x);
        }
        */

        private void compute_log_adjustment_factor()
        {
            int iCol = 17;
            int x, y;
            bool done;

            east_side_butt_length = 0;
            for (x = 0; x < 12; x++)
                segment_whole_half[x] = 1.0f;

            if (spcScribnerScale == "W")
                return;
            if (spcScribnerScale == "1")
                return;

            x = 0;
            done = false;
            while (done == false)
            {
                string sSort = string.Empty;
                string sGrade = string.Empty;
                string sPercent = string.Empty;
                switch (x)
                {
                    case 0:
                        sSort = tree.SortCode1;
                        sGrade = tree.GradeCode1;
                        sPercent = tree.BdFtPD1.ToString();
                        break;
                    case 1:
                        sSort = tree.SortCode2;
                        sGrade = tree.GradeCode2;
                        sPercent = tree.BdFtPD2.ToString();
                        break;
                    case 2:
                        sSort = tree.SortCode3;
                        sGrade = tree.GradeCode3;
                        sPercent = tree.BdFtPD3.ToString();
                        break;
                    case 3:
                        sSort = tree.SortCode4;
                        sGrade = tree.GradeCode4;
                        sPercent = tree.BdFtPD4.ToString();
                        break;
                    case 4:
                        sSort = tree.SortCode5;
                        sGrade = tree.GradeCode5;
                        sPercent = tree.BdFtPD5.ToString();
                        break;
                    case 5:
                        sSort = tree.SortCode6;
                        sGrade = tree.GradeCode6;
                        sPercent = tree.BdFtPD6.ToString();
                        break;
                    case 6:
                        sSort = tree.SortCode7;
                        sGrade = tree.GradeCode7;
                        sPercent = tree.BdFtPD7.ToString();
                        break;
                    case 7:
                        sSort = tree.SortCode8;
                        sGrade = tree.GradeCode8;
                        sPercent = tree.BdFtPD8.ToString();
                        break;
                    case 8:
                        sSort = tree.SortCode9;
                        sGrade = tree.GradeCode9;
                        sPercent = tree.BdFtPD9.ToString();
                        break;
                    case 9:
                        sSort = tree.SortCode10;
                        sGrade = tree.GradeCode10;
                        sPercent = tree.BdFtPD10.ToString();
                        break;
                    case 10:
                        sSort = tree.SortCode11;
                        sGrade = tree.GradeCode11;
                        sPercent = tree.BdFtPD11.ToString();
                        break;
                    case 11:
                        sSort = tree.SortCode12;
                        sGrade = tree.GradeCode12;
                        sPercent = tree.BdFtPD12.ToString();
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                {
                    compute_east_butt_length();
                    return;
                }
                if (sSort == " " && sGrade == " ")
                {
                    compute_east_butt_length();
                    return;
                }
                if (sPercent == "-")
                {
                    y = find_second_dash(x);
                    insert_log_adjustment(x, y);
                    x = y;
                }
                x++;
                if (x == 12)
                    done = true;
            }
        }

        private int find_second_dash(int x)
        {
            int y = x + 1;
            int iCol = 17;

            while (y < 12)
            {
                string sSort = string.Empty;
                string sPercent = string.Empty;
                switch (y)
                {
                    case 0:
                        sSort = tree.SortCode1;
                        sPercent = tree.BdFtPD1.ToString();
                        break;
                    case 1:
                        sSort = tree.SortCode2;
                        sPercent = tree.BdFtPD2.ToString();
                        break;
                    case 2:
                        sSort = tree.SortCode3;
                        sPercent = tree.BdFtPD3.ToString();
                        break;
                    case 3:
                        sSort = tree.SortCode4;
                        sPercent = tree.BdFtPD4.ToString();
                        break;
                    case 4:
                        sSort = tree.SortCode5;
                        sPercent = tree.BdFtPD5.ToString();
                        break;
                    case 5:
                        sSort = tree.SortCode6;
                        sPercent = tree.BdFtPD6.ToString();
                        break;
                    case 6:
                        sSort = tree.SortCode7;
                        sPercent = tree.BdFtPD7.ToString();
                        break;
                    case 7:
                        sSort = tree.SortCode8;
                        sPercent = tree.BdFtPD8.ToString();
                        break;
                    case 8:
                        sSort = tree.SortCode9;
                        sPercent = tree.BdFtPD9.ToString();
                        break;
                    case 9:
                        sSort = tree.SortCode10;
                        sPercent = tree.BdFtPD10.ToString();
                        break;
                    case 10:
                        sSort = tree.SortCode11;
                        sPercent = tree.BdFtPD11.ToString();
                        break;
                    case 11:
                        sSort = tree.SortCode12;
                        sPercent = tree.BdFtPD12.ToString();
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                {
                    y = y - 1;
                    return (y);
                }
                if (sPercent != "-")
                    return (y);
                y++;
            }
            return (y - 1);
        }

        private void insert_log_adjustment(int x, int y)
        {
            int j;

            j = y - x;
            if (j == 0)
                return;
            if (j == 1)
            {
                segment_whole_half[x] = 0.5f;
                segment_whole_half[x + 1] = 0.5f;
                return;
            }
            if (j == 2)
            {
                segment_whole_half[x] = 0.34f;
                segment_whole_half[x + 1] = 0.33f;
                segment_whole_half[x + 2] = 0.33f;
                return;
            }

            segment_whole_half[x] = 0.25f;
            segment_whole_half[x + 1] = 0.25f;
            segment_whole_half[x + 2] = 0.25f;
            segment_whole_half[x + 3] = 0.25f;
        }

        private void compute_east_butt_length()
        {
            int iCol = 17;
            int x = 0;
            string field;

            while (x < 12)
            {
                string sSort = string.Empty;
                string sPercent = string.Empty;
                string sLenDed = string.Empty;
                string sDiaDed = string.Empty;
                switch (y)
                {
                    case 0:
                        sSort = tree.SortCode1;
                        sLenDed = tree.BdFtLD1.ToString();
                        sDiaDed = tree.BdFtDD1.ToString();
                        sPercent = tree.BdFtPD1.ToString();
                        break;
                    case 1:
                        sSort = tree.SortCode2;
                        sLenDed = tree.BdFtLD2.ToString();
                        sDiaDed = tree.BdFtDD2.ToString();
                        sPercent = tree.BdFtPD2.ToString();
                        break;
                    case 2:
                        sSort = tree.SortCode3;
                        sLenDed = tree.BdFtLD3.ToString();
                        sDiaDed = tree.BdFtDD3.ToString();
                        sPercent = tree.BdFtPD3.ToString();
                        break;
                    case 3:
                        sSort = tree.SortCode4;
                        sLenDed = tree.BdFtLD4.ToString();
                        sDiaDed = tree.BdFtDD4.ToString();
                        sPercent = tree.BdFtPD4.ToString();
                        break;
                    case 4:
                        sSort = tree.SortCode5;
                        sLenDed = tree.BdFtLD5.ToString();
                        sDiaDed = tree.BdFtDD5.ToString();
                        sPercent = tree.BdFtPD5.ToString();
                        break;
                    case 5:
                        sSort = tree.SortCode6;
                        sLenDed = tree.BdFtLD6.ToString();
                        sDiaDed = tree.BdFtDD6.ToString();
                        sPercent = tree.BdFtPD6.ToString();
                        break;
                    case 6:
                        sSort = tree.SortCode7;
                        sLenDed = tree.BdFtLD7.ToString();
                        sDiaDed = tree.BdFtDD7.ToString();
                        sPercent = tree.BdFtPD7.ToString();
                        break;
                    case 7:
                        sSort = tree.SortCode8;
                        sLenDed = tree.BdFtLD8.ToString();
                        sDiaDed = tree.BdFtDD8.ToString();
                        sPercent = tree.BdFtPD8.ToString();
                        break;
                    case 8:
                        sSort = tree.SortCode9;
                        sLenDed = tree.BdFtLD9.ToString();
                        sDiaDed = tree.BdFtDD9.ToString();
                        sPercent = tree.BdFtPD9.ToString();
                        break;
                    case 9:
                        sSort = tree.SortCode10;
                        sLenDed = tree.BdFtLD10.ToString();
                        sDiaDed = tree.BdFtDD10.ToString();
                        sPercent = tree.BdFtPD10.ToString();
                        break;
                    case 10:
                        sSort = tree.SortCode11;
                        sLenDed = tree.BdFtLD11.ToString();
                        sDiaDed = tree.BdFtDD11.ToString();
                        sPercent = tree.BdFtPD11.ToString();
                        break;
                    case 11:
                        sSort = tree.SortCode12;
                        sLenDed = tree.BdFtLD12.ToString();
                        sDiaDed = tree.BdFtDD12.ToString();
                        sPercent = tree.BdFtPD12.ToString();
                        break;
                }
                if (string.IsNullOrEmpty(sSort))
                    return;
                field = sLenDed + sDiaDed;
                y = Utils.ConvertToInt(field);
                if (x == 0)
                    east_side_butt_length = y;
                else
                    east_side_butt_length = east_side_butt_length + y;
                if (sPercent != "-")
                    x = 11;
                x++;
            }
        }

        private void compute_segment_length(float given_length)
        {
            float tl, c_c;   //* total segment length 
            int l;

            log_max = 1;
            seg_length = (int)given_length;
            if (fiber_segment)
                return;
            if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                return;

            tl = seg_length;
            c_c = (int)Utils.ConvertToInt(spcMaxLogLen) + 1.0f + 2.0f * trim_allowed - 0.1f;

            while (tl > c_c)
            {
                log_max = log_max + 1;
                c_c = c_c + Utils.ConvertToInt(spcMaxLogLen) + trim_allowed;
            }

            l = (int)tl - (int)trim_allowed * log_max;
            seg_length = l;
        }

        private float compute_plus_length(float buttdib, float butthgt, float len, string s, string g)
        {
            int[] tmindia = new int[] { 7, 4, 2, 6, 2, 2, 2, 7, 6, 2, 10, 2, 2 };
            int[] tminlen = new int[] { 18, 6, 10, 12, 0, 10, 10, 16, 0, 10, 16, 10, 10 };
            string[] tsorgrd = new string[] { "11", "12", "13", "21", "22", "23", "33", "41", "42", "43", "51", "52", "53" };
            float tophgt;
            int mindia, minlen, sub, maxlen;
            string sorgrd;

            sorgrd = String.Empty;
            sorgrd = s + g;
            sub = 0;
            mindia = 0;
            minlen = 0;
            plus_status = 0;
            if (s == "3")
                sorgrd = "33";
            while (sub < 13)
            {
                if (sorgrd == tsorgrd[sub])
                {
                    mindia = tmindia[sub];
                    minlen = tminlen[sub];
                    break;
                }
                sub++;
            }
            if (mindia == 0)
            {
                plus_status = 1;
                return 0.0f;
            }
            if (buttdib < mindia)
            {
                plus_status = 2;
                return 0.0f;
            }
            if (len < minlen)
            {
                plus_status = 3;
                return 0.0f;
            }
            tophgt = computed_height(mindia);
            maxlen = (int)tophgt - (int)butthgt;
            if (maxlen >= minlen)
            {
                plus_status = 0;
                return maxlen;
            }
            plus_status = 1;
            return 0.0f;
        }

        private void recompute_segment_length(float given_length)
        {
            float tl;     //* total segment length 
            float c;
            int l;

            log_max = 1;
            seg_length = (int)given_length;
            if (seg_length < Utils.ConvertToInt(spcMinLogLen))
                return;
            tl = seg_length;
            c = Utils.ConvertToInt(spcMaxLogLen) + 1.0f + 2.0f * trim_allowed - 0.1f;

            while (tl > c)
            {
                log_max = log_max + 1;
                c = c + Utils.ConvertToInt(spcMaxLogLen) + trim_allowed;
            }
            l = (int)tl - (int)trim_allowed * (int)log_max;
            seg_length = l;
        }

        private void compute_west_true_logs()
        {
            int x, y, log_length, increment;

            for (x = 0; x < 20; x++)
                seg_log_length[x] = 0;

            if (log_max == 1)
            {
                seg_log_length[0] = seg_length;
                return;
            }

            if (log_max == 0)
                return;

            x = (int)seg_length / ((int)log_max * 2);
            log_length = x * 2;
            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                seg_log_length[x] = log_length;

            for (x = (log_max - 1); x >= 0; x--)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                seg_log_length[x] = seg_log_length[x] + increment;
            }
        }

        private void compute_east_true_logs()
        {
            int x, y, log_length, increment;

            for (x = 0; x < 20; x++)
                seg_log_length[x] = 0;

            if (log_max == 1)
            {
                seg_log_length[0] = seg_length;
                return;
            }

            if (log_max == 0)
                return;

            x = (int)seg_length / (log_max * 2);
            log_length = x * 2;
            y = (int)seg_length - (log_length * log_max);

            for (x = 0; x < log_max; x++)
                seg_log_length[x] = log_length;

            for (x = 0; x < log_max; x++)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                seg_log_length[x] = seg_log_length[x] + increment;
            }
        }

        private void compute_west_side_log_length()
        {
            int x, y, log_length, increment;
            int xq, xr;
            decimal z;

            for (x = 0; x < 20; x++)
                log_log_length[x] = 0;

            log_max = 1;

            if (cull_segment == true || seg_length < 41)
            {
                log_log_length[0] = seg_length;
                return;
            }

            //z = Decimal.Divide((int)seg_length, (int)40);
            xq = DivRem((int)seg_length, 40, out xr);
            log_max = xq;
            if (xr > 0)
                log_max++;
            log_length = (int)seg_length / (log_max * 2);
            log_length *= 2;

            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                log_log_length[x] = log_length;

            x = log_max;
            for (x = log_max - 1; x >= 0; x--)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                log_log_length[x] += increment;
            }
        }

        private void compute_east_side_log_length()
        {
            int x, y, log_length, increment;
            int xr, xq;
            decimal z;

            for (x = 0; x < 20; x++)
                log_log_length[x] = 0;

            log_max = 1;

            if (cull_segment == true || seg_length < 21)
            {
                log_log_length[0] = seg_length;
                return;
            }

            //z = Decimal.Divide((int)seg_length, (int)20);
            xq = DivRem((int)seg_length, 20, out xr);
            log_max = xq;
            if (xr > 0)
                log_max++;
            log_length = (int)seg_length / (log_max * 2);
            log_length *= 2;

            y = (int)seg_length - ((int)log_length * (int)log_max);

            for (x = 0; x < log_max; x++)
                log_log_length[x] = log_length;

            for (x = 0; x < log_max; x++)
            {
                increment = 2;
                if ((y - increment) < 0)
                    increment = 1;
                if ((y - increment) < 0)
                    increment = 0;
                y -= increment;
                log_log_length[x] += increment;
            }
        }

        private void figure_west_taper()
        {
            int x;
            float taper_difference, y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;

            if (log_max == 1)
                return;

        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];

        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;

            if (log_max == 1)
                return;
            if (spcScribnerScale == "W")
                return;
            //*  one in ten scale   
            x = (int)log_log_length[(int)log_max - 1] / 10;
            seg_log_top[log_max - 2] = seg_log_top[log_max - 1] + x;
            seg_log_butt[log_max - 1] = seg_log_top[log_max - 2];
            if (log_max == 2)
                return;
            x = (int)log_log_length[(int)log_max - 2] / 10;
            seg_log_top[log_max - 3] = seg_log_top[log_max - 2] + x;
            seg_log_butt[log_max - 2] = seg_log_top[log_max - 3];
        }

        private void figure_bia_taper_2nd()
        {
            int x;
            float y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;
            if (log_max == 1)
                return;
        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private void figure_bia_taper_butt(float butt_diam, float top_diam)
        {
            int x, y, increment;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[01] = butt_diam;

            if (east_side_butt_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Convert.ToSingle(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (seg_length > 40)
                goto loop1;

            increment = 1;
            if (east_side_butt_length > 22)
                increment = 2;

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[0];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = 2;
        loop2:
            y = (int)taper_difference;
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private void figure_east_taper_2nd()
        {
            int x;
            float y, z;

            seg_log_top[log_max - 1] = taper_top;
            seg_log_butt[0] = taper_butt;
            x = log_max;
            if (log_max == 1)
                return;
        loop1:
            taper_difference = taper_butt - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private void figure_east_taper_butt(int butt_diam, int top_diam)
        {
            int x, increment;
            float y, z;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[0] = butt_diam;

            if (east_side_butt_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Utils.ConvertToFloat(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (seg_length > 40)
                goto loop1;

            temp_species = tree.SpeciesAbbreviation.Trim();
            if (temp_species == "WL")
                increment = 1;
            else
            {
                increment = 1;
                if (east_side_butt_length > 31)
                    increment = 2;
            }
            if (spcScribnerScale == "O")
            {
                if (temp_species == "WL" || temp_species == "LP")
                    increment = 1;
            }

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[1];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = butt_diam - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1)
                goto loop1;
        }

        private void figure_idaho_taper_butt(float butt_diam, float top_diam)
        {
            int x, increment;
            float y, z;
            float new_height, new_dia;

            east_side_next_butt = 0;
            seg_log_top[log_max - 1] = top_diam;
            seg_log_butt[0] = butt_diam;
            if (seg_length < 21)
                return;

            if (east_side_butt_length > seg_length)
            {
                new_height = east_side_butt_length + Utils.ConvertToFloat(spcTrim);
                new_dia = computed_diameter(new_height);
                seg_log_top[log_max - 1] = (float)Math.Round(new_dia);
            }

            x = log_max;

            if (spcScribnerScale == "T")
            {
                increment = 2;
                if (tree.SpeciesAbbreviation == "DF" || tree.SpeciesAbbreviation == "AF" || tree.SpeciesAbbreviation == "ES")
                    goto loop0;

                if (tree.SpeciesAbbreviation == "LP" || tree.SpeciesAbbreviation == "DL")
                {
                    if (seg_length < 32)
                        increment = 1;
                    else
                        increment = 2;
                    goto loop0;
                }
            }

            if (seg_length > 40)
                goto loop1;

            increment = 1;

            temp_species = tree.SpeciesAbbreviation.Trim();
            if (tree.SpeciesAbbreviation == "WL")
                goto loop0;
            if (tree.SpeciesAbbreviation == "LP")
                goto loop0;
            if (tree.SpeciesAbbreviation == "DL")
                goto loop0;

            increment = 2;
            if (tree.SpeciesAbbreviation == "RC")
                goto loop0;
            if (tree.SpeciesAbbreviation == "PO")
                goto loop0;
            if (tree.SpeciesAbbreviation == "IC")
                goto loop0;

            //* taper = 2 if top diameter is even 
            //* taper = 1 if top diameter is odd  

            x = (int)top_diam / 2;
            x = x * 2;

            if (x == top_diam)
                increment = 2;
            else
                increment = 1;

        loop0:

            if (log_max == 1)
            {
                seg_log_top[0] = seg_log_top[0] + increment;
                east_side_next_butt = seg_log_top[0];
                return;
            }

            seg_log_butt[log_max - 1] = seg_log_top[log_max - 1] + increment;
            seg_log_top[0] = seg_log_butt[log_max - 1];
            east_side_next_butt = seg_log_top[log_max - 1];
            return;

        loop1:
            taper_difference = butt_diam - seg_log_top[x - 1];
        loop2:
            y = taper_difference / x;
            z = y * x;
            if (z != taper_difference)
            {
                taper_difference = taper_difference + 1;
                goto loop2;
            }
            x = x - 1;
            seg_log_top[x - 1] = seg_log_top[x] + y;
            seg_log_butt[x] = seg_log_top[x - 1];
            if (x > 1) goto loop1;
        }

        private void figure_scribner()
        {
            float rounded_scribner;

            //log_length = (int)Math.Round(log_length); // was (float)
            //log_top_diameter = (int)Math.Round(log_top_diameter); // was (float)
            log_length = (int)log_length;
            log_top_diameter = (int)log_top_diameter;
            if (log_length > 0 && log_top_diameter > 0)
                log_scribner = scribner(log_length, log_top_diameter, spcScribnerScale);

            if (c_percent > 0)
            {
                rounded_scribner = (int)Math.Round(log_scribner * (100.0f - bf_ded_percent) * 0.001f); // was (float)
                log_net_scribner = rounded_scribner * 10.0f;
            }
            else
            {
                if ((log_length - bf_ded_feet < 1) || (log_top_diameter - bf_ded_inches < 1))
                    log_net_scribner = 0;
                else
                {
                    if (log_index > 0)
                        bf_ded_feet = 0;
                    log_net_scribner = scribner((int)Math.Round(log_length - bf_ded_feet),
                        (int)Math.Round(log_top_diameter - bf_ded_inches), spcScribnerScale); // was (float)
                }
            }
            if (log_net_scribner > log_scribner)
                log_net_scribner = log_scribner;

            if (cull_segment)
                log_net_scribner = 0;
            if (fiber_segment)
            {
                log_scribner = 0;
                log_net_scribner = 0;
            }

            seg[cvol_index].scribner_gross += log_scribner;
            seg[cvol_index].scribner_net += log_net_scribner;
            tree_scribner_gross += log_scribner;
            tree_scribner_net += log_net_scribner;
        }

        private void figure_cubic()
        {
            string temp;
            float diam;

            if (sweyco_diameter_rounded > 0)
                diam = sweyco_diameter_rounded;
            else
                diam = seg_log_butt[0];

            if (log_length > 0 && log_top_diameter > 0)
            {
                if (spcCubicScale == "1")
                    log_cubic = northwest_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                if (spcCubicScale == "2")
                    log_cubic = sweyco_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)diam);
                if (spcCubicScale == "S")
                    log_cubic = smalian_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                if (spcCubicScale == "W")
                    log_cubic = weyco_cubic((int)seg_log_length[seg_log_index],
                        (int)seg_log_top[log_max - 1], (int)dib_at_d4h);
                if (spcCubicScale == "N")
                {
                    if (national_diameter_rounded == 0)
                        log_cubic = national_cubic((int)seg_log_length[seg_log_index],
                            (int)seg_log_top[log_max - 1], (int)seg_log_butt[0]);
                    else
                        log_cubic = national_cubic((int)seg_log_length[seg_log_index],
                            (int)seg_log_top[log_max - 1], (int)national_diameter_rounded);
                }
            }

            if (c_percent > 0)
                log_net_cubic = (int)Math.Round(log_cubic * (100 - cf_ded_percent) * 0.01);
            else
            {
                if (spcCubicScale == "1")
                    log_net_cubic =
                        northwest_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)seg_log_butt[0] - (int)cf_ded_inches);
                if (spcCubicScale == "2")
                    log_net_cubic =
                        sweyco_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)diam - (int)cf_ded_inches);
                if (spcCubicScale == "S")
                    log_net_cubic =
                        smalian_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)seg_log_butt[0] - (int)cf_ded_inches);
                if (spcCubicScale == "W")
                    log_net_cubic =
                        weyco_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                        (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                        (int)dib_at_d4h - (int)cf_ded_inches);
                if (spcCubicScale == "N")
                {
                    if (national_diameter_rounded == 0)
                        log_net_cubic =
                            national_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                            (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                            (int)seg_log_butt[0] - (int)cf_ded_inches);
                    else
                        log_net_cubic =
                            national_cubic((int)seg_log_length[seg_log_index] - (int)cf_ded_feet,
                            (int)seg_log_top[log_max - 1] - (int)cf_ded_inches,
                            (int)national_diameter_rounded - (int)cf_ded_inches);
                }
            }

            if (log_net_cubic > log_cubic)
                log_net_cubic = log_cubic;

            if (cull_segment)
                log_net_cubic = 0;

            seg[cvol_index].cubic_gross += log_cubic;
            seg[cvol_index].cubic_net += log_net_cubic;
            temp = String.Format("{0:0}", log_cubic);
            tree_cubic_gross += Utils.ConvertToFloat(temp);
            temp = String.Format("{0:0}", log_net_cubic);
            tree_cubic_net += Utils.ConvertToFloat(temp);
        }

        private float scribner(float l, float t, string scale)
        {
            //decimal x;
            int xq = 0, xr = 0, x = 0;
            float volume, len, top;

            len = l;
            top = t;

            if (scale == "D")
            {
                if (top < 5)
                {
                    volume = 0;
                    return (volume);
                }
                volume = ((float)top - 4.0f) / 4.0f;
                volume = (int)volume * (int)volume * (int)len;
                return (volume);
            }
            double fac = this.GetVolumeFactor((int)top, (int)len);
            volume = len * (float)fac;
            //x = Decimal.Divide((decimal)volume, (decimal)10);
            //x = Decimal.Divide((int)volume, (int)10);
            xq = DivRem((int)volume, 10, out xr);
            if (xq > 0 && xr > 4)
                xq++;
            else
            if (xr > 4)
                xq++;
            volume = (xq * 10);
            if (scale == "I" || scale == "T")
            {
                if (top == 6 && len == 14)
                    volume -= 10;
                if (top == 8 && len == 20)
                    volume -= 10;
                if (top == 9 && len == 20)
                    volume -= 10;
                if (top == 9 && len == 10)
                    volume += 10;
                if (top == 10 && len == 12)
                    volume -= 10;
                if (top == 23 && len == 10)
                    volume -= 10;
                if (top == 5 && len == 16)
                    volume -= 10;
                if (top == 5 && len == 15)
                    volume -= 10;
            }
            return (volume);
        }

        private float smalian_cubic(int len, int top, int butt)
        {
            float volume, log_l;
            float tdia, bdia;

            log_l = len;
            tdia = top;
            bdia = butt;
            volume = 0.00272708f * (tdia * tdia + bdia * bdia) * log_l;
            return (volume);
        }

        private float national_cubic(int len, int top, int butt)
        {
            float volume, log_l;
            float tdia, bdia;

            log_l = len;
            tdia = top;
            bdia = butt;
            volume = 0.002727f * (tdia * tdia + bdia * bdia) * log_l;
            return (volume);
        }

        private float northwest_cubic(int len, int top, int butt)
        {
            float volume, len_fac, log_l, length;
            float tdia, bdia, top_dia, bot_dia, d1, d2;

            log_l = len;
            tdia = top;
            bdia = butt;

            if (log_l >= 17)
                len_fac = 1.00f;
            else
                len_fac = 0.67f;
            length = log_l + len_fac;
            if (west_routine == true)
            {
                top_dia = tdia + 0.7f;
                bot_dia = bdia + 0.7f;
            }
            else
            {
                top_dia = tdia;
                bot_dia = bdia;
            }
            d1 = top_dia * top_dia;
            d2 = bot_dia * bot_dia;
            volume = 0.001818f * length * (d1 + d2 + (top_dia * bot_dia));
            return (volume);
        }

        private float sweyco_cubic(int len, int top, int butt)
        {
            float volume, log_l, length;
            float tdia, bdia, top_dia, bot_dia, d1, d2;

            log_l = len;
            tdia = top;
            bdia = butt;

            length = log_l;
            top_dia = tdia;
            bot_dia = bdia;
            d1 = top_dia * top_dia;
            d2 = bot_dia * bot_dia;
            volume = 0.001818f * length * (d1 + d2 + (top_dia * bot_dia));
            return (volume);
        }

        private float weyco_cubic(int len, int top, int butt)
        {
            float volume, len_fac, log_l, length, top_dia, bot_dia;
            float tdia, bdia;

            log_l = len;
            tdia = top;
            bdia = butt;

            if (log_l < 17)
                len_fac = 0.75f;
            else
                len_fac = 0.67f;
            length = (len_fac + log_l) * 0.005454f;
            top_dia = tdia + 0.5f;
            bot_dia = bdia + 0.5f;
            volume = ((top_dia * top_dia) + (bot_dia * bot_dia) + (top_dia * bot_dia)) / 3.0f;
            return ((volume * length) + 0.5f);
        }

        private double Square(float flt)
        {
            return ((double)flt * (double)flt);
        }

        private int check_sort_grade_combo(string sort, string grade, int len, int top)
        {
            int[] tmindia = new int[] { 6, 2, 2, 6, 2, 2, 2, 6, 2, 2, 10, 2, 2 };
            int[] tminlen = new int[] { 14, 10, 10, 14, 10, 10, 10, 14, 10, 10, 12, 10, 10 };
            string[] tsorgrd = new string[] { "11", "12", "13", "21", "22", "23", "33", "41", "42", "43", "51", "52", "53" };
            int mindia, minlen, sub;
            string sorgrd = null;
            int found;

            found = 0;
            sorgrd = String.Format("{0}{1}", sort, grade);
            sub = 0;
            mindia = 0;
            minlen = 0;
            if (sort == "3")
                sorgrd = "33";
            while (sub < 13)
            {
                if (sorgrd == tsorgrd[sub])
                {
                    mindia = tmindia[sub];
                    minlen = tminlen[sub];
                    found = 1;
                    break;
                }
                sub++;
            }
            if (mindia == 0)
                calc_error = 10;
            if (top < mindia)
                calc_error = 11;
            if (len < minlen)
                calc_error = 12;
            return found;
        }

        private void check_grade_len_top_butt(string grade, int len, int top, int butt)
        {
            LookupGradeValues(grade);
            if (len < Utils.ConvertToInt(grdMinLen))
                calc_error = 3;
            if (top < Utils.ConvertToInt(grdMinDia))
                calc_error = 4;
            if (Utils.ConvertToInt(grdMaxButt) > 0)
            {
                if (butt > Utils.ConvertToInt(grdMaxButt))
                    calc_error = 8;
            }
        }

        private void check_sort_len_top_butt(string sort, int len, int top, int butt)
        {
            LookupSortValues(sort);
            if (len < Utils.ConvertToInt(srtMinLen))
                calc_error = 5;
            if (top < Utils.ConvertToInt(srtMinDia))
                calc_error = 6;
            if (Utils.ConvertToInt(srtMaxButt) > 0)
            {
                if (butt > Utils.ConvertToInt(srtMaxButt))
                    calc_error = 9;
            }
        }

        private void check_master_len_dia(int len, int dia)
        {
            if (len < Utils.ConvertToInt(spcMinLogLen))
                calc_error = 2;
            if (dia < Utils.ConvertToInt(spcMinTopDia))
                calc_error = 1;
            if (len > Utils.ConvertToInt(spcMaxLogLen))
                calc_error = 7;
        }

        private double GetVolumeFactor(int iDia, int iLen)
        {
            double VolFac = 0;

            if ((iLen > 0 && iLen < 16) && (iDia > 5 && iDia < 12))
            {
                switch (iDia)
                {
                    case 6:
                        VolFac = 1.160;
                        break;
                    case 7:
                        VolFac = 1.400;
                        break;
                    case 8:
                        VolFac = 1.501;
                        break;
                    case 9:
                        VolFac = 2.084;
                        break;
                    case 10:
                        VolFac = 3.126;
                        break;
                    case 11:
                        VolFac = 3.749;
                        break;
                }
            }
            else
                if ((iLen > 15 && iLen < 32) && (iDia > 5 && iDia < 12))
                {
                    switch (iDia)
                    {
                        case 6:
                            VolFac = 1.249;
                            break;
                        case 7:
                            VolFac = 1.608;
                            break;
                        case 8:
                            VolFac = 1.854;
                            break;
                        case 9:
                            VolFac = 2.410;
                            break;
                        case 10:
                            VolFac = 3.542;
                            break;
                        case 11:
                            VolFac = 4.167;
                            break;
                    }
                }
                else
                    if ((iLen > 31 && iLen < 41) && (iDia > 5 && iDia < 12))
                    {
                        switch (iDia)
                        {
                            case 6:
                                VolFac = 1.570;
                                break;
                            case 7:
                                VolFac = 1.800;
                                break;
                            case 8:
                                VolFac = 2.200;
                                break;
                            case 9:
                                VolFac = 2.900;
                                break;
                            case 10:
                                VolFac = 3.815;
                                break;
                            case 11:
                                VolFac = 4.499;
                                break;
                        }
                    }
                    else
                        if (iDia < 6)
                        {
                            switch (iDia)
                            {
                                case 1:
                                    VolFac = 0.000;
                                    break;
                                case 2:
                                    VolFac = 0.143;
                                    break;
                                case 3:
                                    VolFac = 0.390;
                                    break;
                                case 4:
                                    VolFac = 0.676;
                                    break;
                                case 5:
                                    VolFac = 1.070;
                                    break;
                            }
                        }
                        else
                            if (iDia > 11)
                            {
                                switch (iDia)
                                {
                                    case 12:
                                        VolFac = 4.900;
                                        break;
                                    case 13:
                                        VolFac = 6.043;
                                        break;
                                    case 14:
                                        VolFac = 7.140;
                                        break;
                                    case 15:
                                        VolFac = 8.880;
                                        break;
                                    case 16:
                                        VolFac = 10.000;
                                        break;
                                    case 17:
                                        VolFac = 11.528;
                                        break;
                                    case 18:
                                        VolFac = 13.290;
                                        break;
                                    case 19:
                                        VolFac = 14.990;
                                        break;
                                    case 20:
                                        VolFac = 17.499;
                                        break;
                                    case 21:
                                        VolFac = 18.990;
                                        break;
                                    case 22:
                                        VolFac = 20.880;
                                        break;
                                    case 23:
                                        VolFac = 23.510;
                                        break;
                                    case 24:
                                        VolFac = 25.218;
                                        break;
                                    case 25:
                                        VolFac = 28.677;
                                        break;
                                    case 26:
                                        VolFac = 31.249;
                                        break;
                                    case 27:
                                        VolFac = 34.220;
                                        break;
                                    case 28:
                                        VolFac = 36.376;
                                        break;
                                    case 29:
                                        VolFac = 38.040;
                                        break;
                                    case 30:
                                        VolFac = 41.060;
                                        break;
                                    case 31:
                                        VolFac = 44.376;
                                        break;
                                    case 32:
                                        VolFac = 45.975;
                                        break;
                                    case 33:
                                        VolFac = 48.990;
                                        break;
                                    case 34:
                                        VolFac = 50.000;
                                        break;
                                    case 35:
                                        VolFac = 54.688;
                                        break;
                                    case 36:
                                        VolFac = 57.660;
                                        break;
                                    case 37:
                                        VolFac = 64.319;
                                        break;
                                    case 38:
                                        VolFac = 66.730;
                                        break;
                                    case 39:
                                        VolFac = 70.000;
                                        break;
                                    case 40:
                                        VolFac = 75.240;
                                        break;
                                    case 41:
                                        VolFac = 79.480;
                                        break;
                                    case 42:
                                        VolFac = 83.910;
                                        break;
                                    case 43:
                                        VolFac = 87.190;
                                        break;
                                    case 44:
                                        VolFac = 92.501;
                                        break;
                                    case 45:
                                        VolFac = 94.990;
                                        break;
                                    case 46:
                                        VolFac = 99.075;
                                        break;
                                    case 47:
                                        VolFac = 103.501;
                                        break;
                                    case 48:
                                        VolFac = 107.970;
                                        break;
                                    case 49:
                                        VolFac = 112.292;
                                        break;
                                    case 50:
                                        VolFac = 116.990;
                                        break;
                                    case 51:
                                        VolFac = 121.650;
                                        break;
                                    case 52:
                                        VolFac = 126.525;
                                        break;
                                    case 53:
                                        VolFac = 131.510;
                                        break;
                                    case 54:
                                        VolFac = 136.510;
                                        break;
                                    case 55:
                                        VolFac = 141.610;
                                        break;
                                    case 56:
                                        VolFac = 146.912;
                                        break;
                                    case 57:
                                        VolFac = 152.210;
                                        break;
                                    case 58:
                                        VolFac = 157.710;
                                        break;
                                    case 59:
                                        VolFac = 163.288;
                                        break;
                                    case 60:
                                        VolFac = 168.990;
                                        break;
                                    case 61:
                                        VolFac = 174.850;
                                        break;
                                    case 62:
                                        VolFac = 180.749;
                                        break;
                                    case 63:
                                        VolFac = 186.623;
                                        break;
                                    case 64:
                                        VolFac = 193.170;
                                        break;
                                    case 65:
                                        VolFac = 199.120;
                                        break;
                                    case 66:
                                        VolFac = 205.685;
                                        break;
                                    case 67:
                                        VolFac = 211.810;
                                        break;
                                    case 68:
                                        VolFac = 218.501;
                                        break;
                                    case 69:
                                        VolFac = 225.685;
                                        break;
                                    case 70:
                                        VolFac = 232.499;
                                        break;
                                    case 71:
                                        VolFac = 239.317;
                                        break;
                                    case 72:
                                        VolFac = 246.615;
                                        break;
                                    case 73:
                                        VolFac = 254.040;
                                        break;
                                    case 74:
                                        VolFac = 261.525;
                                        break;
                                    case 75:
                                        VolFac = 269.040;
                                        break;
                                    case 76:
                                        VolFac = 276.630;
                                        break;
                                    case 77:
                                        VolFac = 284.260;
                                        break;
                                    case 78:
                                        VolFac = 292.501;
                                        break;
                                    case 79:
                                        VolFac = 300.655;
                                        break;
                                    case 80:
                                        VolFac = 308.970;
                                        break;
                                    case 81:
                                        VolFac = 317.360;
                                        break;
                                    case 82:
                                        VolFac = 325.790;
                                        break;
                                    case 83:
                                        VolFac = 334.217;
                                        break;
                                    case 84:
                                        VolFac = 343.290;
                                        break;
                                    case 85:
                                        VolFac = 350.785;
                                        break;
                                    case 86:
                                        VolFac = 359.120;
                                        break;
                                    case 87:
                                        VolFac = 368.380;
                                        break;
                                    case 88:
                                        VolFac = 376.610;
                                        break;
                                    case 89:
                                        VolFac = 385.135;
                                        break;
                                    case 90:
                                        VolFac = 393.380;
                                        break;
                                    case 91:
                                        VolFac = 402.499;
                                        break;
                                    case 92:
                                        VolFac = 410.834;
                                        break;
                                    case 93:
                                        VolFac = 419.166;
                                        break;
                                    case 94:
                                        VolFac = 428.380;
                                        break;
                                    case 95:
                                        VolFac = 437.499;
                                        break;
                                    case 96:
                                        VolFac = 446.565;
                                        break;
                                    case 97:
                                        VolFac = 455.010;
                                        break;
                                    case 98:
                                        VolFac = 464.150;
                                        break;
                                    case 99:
                                        VolFac = 473.430;
                                        break;
                                    case 100:
                                        VolFac = 482.490;
                                        break;
                                    case 101:
                                        VolFac = 491.700;
                                        break;
                                    case 102:
                                        VolFac = 501.700;
                                        break;
                                    case 103:
                                        VolFac = 511.700;
                                        break;
                                    case 104:
                                        VolFac = 521.700;
                                        break;
                                    case 105:
                                        VolFac = 531.700;
                                        break;
                                    case 106:
                                        VolFac = 541.700;
                                        break;
                                    case 107:
                                        VolFac = 552.499;
                                        break;
                                    case 108:
                                        VolFac = 562.501;
                                        break;
                                    case 109:
                                        VolFac = 573.350;
                                        break;
                                    case 110:
                                        VolFac = 583.350;
                                        break;
                                    case 111:
                                        VolFac = 594.150;
                                        break;
                                    case 112:
                                        VolFac = 604.170;
                                        break;
                                    case 113:
                                        VolFac = 615.010;
                                        break;
                                    case 114:
                                        VolFac = 625.890;
                                        break;
                                    case 115:
                                        VolFac = 636.660;
                                        break;
                                    case 116:
                                        VolFac = 648.380;
                                        break;
                                    case 117:
                                        VolFac = 660.000;
                                        break;
                                    case 118:
                                        VolFac = 671.700;
                                        break;
                                    case 119:
                                        VolFac = 683.330;
                                        break;
                                    case 120:
                                        VolFac = 695.011;
                                        break;
                                }
                            }
            return VolFac;
        }

        private int DivRem(int Num, int DivBy, out int rem)
        {
            int iAns;
            float fDivBy = (float)DivBy;
            float fNum = (float)Num;
            float a = fNum / fDivBy;

            iAns = (int)a;
            rem = Num - (iAns * DivBy);
            return (int)a;
        }
    }

    public class SgCalc
    {
        public float tdia;
        public float bdia;
        public float len;
        public float scribner_gross;
        public float scribner_net;
        public float cubic_gross;
        public float cubic_net;

        public SgCalc()
        {
            tdia = 0.0f;
            bdia = 0.0f;
            len = 0.0f;
            scribner_gross = 0.0f;
            scribner_net = 0.0f;
            cubic_gross = 0.0f;
            cubic_net = 0.0f;
        }
    }
}
