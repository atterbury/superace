﻿namespace Import
{
    partial class GetPocketEASYData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbMessage = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMessage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(72, 186);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(139, 23);
            this.btnCopy.TabIndex = 0;
            this.btnCopy.Text = "Copy Data From Device";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Location = new System.Drawing.Point(5, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(267, 57);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Make sure your device is connected to the PC via Windows Mobile Device Center.";
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(12, 131);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tbMessage.Properties.Appearance.Options.UseFont = true;
            this.tbMessage.Properties.ReadOnly = true;
            this.tbMessage.Size = new System.Drawing.Size(260, 20);
            this.tbMessage.TabIndex = 2;
            // 
            // GetPocketEASYData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 221);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnCopy);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GetPocketEASYData";
            this.Text = "Get PocketEASY Data";
            this.Load += new System.EventHandler(this.GetPocketEASYData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbMessage.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCopy;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbMessage;
    }
}