﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using System.IO;
using System.Data.OleDb;

namespace Import
{
    public partial class ImportSuperACE2008Form : DevExpress.XtraEditors.XtraForm
    {
        public bool bProcess = false;
        public string newProjectName = string.Empty;
        public string newProjectLocation = "C:\\ACI\\SuperACE\\Data\\";
        public string oldFlipsFileName = string.Empty;
        public string oldDefaultsFileName = string.Empty;
        public string oldProjectFileName = string.Empty;
        public string oldProjectName = string.Empty;

        private INIFile iniFile = null;

        private ProjectDbContext projectContext = null;
        private ProjectsDbContext locContext = null;

        private OleDbConnection mdbConn = null;
        private OleDbCommand mdbCmd = null;
        private OleDbDataReader mdbReader = null;
        private DataTable mdbTbl = null;

        public ImportSuperACE2008Form(ref ProjectDbContext pProjectContext, ref ProjectsDbContext pLocContext)
        {
            InitializeComponent();

            projectContext = pProjectContext;
            locContext = pLocContext;
        }

        private void ImportSuperACE2008Form_Load(object sender, EventArgs e)
        {
            iniFile = new INIFile(System.Environment.GetEnvironmentVariable("windir") + "\\flips.aci");
            newProjectLocation = Properties.Settings.Default["DbLocation"].ToString();
            //tbProjectName.Properties.Items.Clear();
            oldFlipsFileName = GetFLIPSIni() + "FLIPS.mdb"; // @"C:\FLIPS08\Flips.mdb";
            oldProjectFileName = GetLastProjectIni(); // @"C:\FLIPS08\DEMO\DEMO.mdb";
            oldDefaultsFileName = GetCurrentDataIni() + "\\Defaults.mdb"; // @"C:\FLIPS08\DEMO\Defaults.mdb";
            tbProjectDb.Text = oldProjectFileName;
            tbNewProjectLocation.Text = newProjectLocation;

            tbProjectDb.Focus();
            //OpenFile(tbFLIPSDb.Text, "TProject", "", "", "");
            //while (mdbReader.Read())
            //    cbProjectName.Properties.Items.Add(mdbReader["Project"].ToString().Trim());
            //CloseFile();
        }

        private string GetCurrentDataIni()
        {
            string result = iniFile.GetValue("Directories", "CurrDataDir", "");
            return result;
        }

        private string GetLastProjectIni()
        {
            string result = iniFile.GetValue("Directories", "CurrDB", "");
            return result;
        }

        private string GetFLIPSIni()
        {
            string result = iniFile.GetValue("Directories", "FlipsDir", "");
            return result;
        }

        //private void btnFlipsDb_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog frm = new OpenFileDialog();
        //    frm.Filter = "(*.mdb)|*.mdb";
        //    frm.InitialDirectory = @"C:\FLIPS08";
        //    frm.ShowDialog();
        //    tbFLIPSDb.Text = frm.FileName;
        //    frm.Dispose();
        //}

        private void btnProjectDb_Click(object sender, EventArgs e)
        {
            string flipsDb = string.Empty;
            OpenFileDialog frm = new OpenFileDialog();
            frm.Filter = "(*.mdb)|*.mdb";
            frm.InitialDirectory = GetLastProjectIni(); // @"C:\FLIPS08";
            frm.ShowDialog();
            tbProjectDb.Text = frm.FileName;

            if (!string.IsNullOrEmpty(frm.FileName))
            {
                FileInfo fi = new FileInfo(frm.FileName);
                oldDefaultsFileName = string.Format("{0}\\Defaults.mdb", fi.DirectoryName);
            }
            oldProjectFileName = frm.FileName;
            oldProjectName = Path.GetFileName(frm.FileName).Replace(".mdb", "");
            tbNewProjectName.Text = Path.GetFileName(frm.FileName).Replace(".mdb", "");
            frm.Dispose();
        }

        //private void btnDefaultsDb_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog frm = new OpenFileDialog();
        //    frm.Filter = "(*.mdb)|*.mdb";
        //    frm.InitialDirectory = @"C:\FLIPS08";
        //    frm.ShowDialog();
        //    tbDefaultsDb.Text = frm.FileName;
        //}

        private void cbProjectName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //tbNewProjectName.Text = tbProjectName.Text;
        }

        private void btnNewProjectLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = @"C:\ACI\SuperACE\Data";
            frm.ShowDialog();

            if (!string.IsNullOrEmpty(frm.SelectedPath))
            {
               tbNewProjectLocation.Text = string.Format("{0}", frm.SelectedPath);
            }
            frm.Dispose();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(oldFlipsFileName))
            {
                XtraMessageBox.Show("Flips.mdb is required.", "Error", MessageBoxButtons.OK);
                //tbFLIPSDb.Focus();
                return;
            }
            else
            {
                // Display Error if FLIPS.mdb not found
                if (!File.Exists(oldFlipsFileName))
                {
                    XtraMessageBox.Show("Flips.mdb file is NOT found.", "Error", MessageBoxButtons.OK);
                    //tbFLIPSDb.Focus();
                    return;
                }
            }

            if (string.IsNullOrEmpty(tbProjectDb.Text))
            {
                XtraMessageBox.Show("SuperACE08 Project is required.", "Error", MessageBoxButtons.OK);
                tbProjectDb.Focus();
                return;
            }
            else
            {
                // Display Error if Project.DAL.Project is not found
                if (!File.Exists(tbProjectDb.Text))
                {
                    XtraMessageBox.Show("SuperACE08 Project is NOT found.", "Error", MessageBoxButtons.OK);
                    tbProjectDb.Focus();
                    return;
                }
            }

            if (string.IsNullOrEmpty(oldDefaultsFileName))
            {
                XtraMessageBox.Show("Defaults.mdb file is required.", "Error", MessageBoxButtons.OK);
                //tbDefaultsDb.Focus();
                return;
            }
            else
            {
                // Display Error if Defaults.mdb is not found
                if (!File.Exists(oldDefaultsFileName))
                {
                    XtraMessageBox.Show("Defaults.mdb file is NOT found.", "Error", MessageBoxButtons.OK);
                    //tbDefaultsDb.Focus();
                    return;
                }
            }

            //if (string.IsNullOrEmpty(tbProjectName.Text))
            //{
            //    XtraMessageBox.Show("Project Name is required.", "Error", MessageBoxButtons.OK);
            //    tbProjectName.Focus();
            //    return;
            //}
            //else
            //{
            //    oldProjectFileName = tbProjectName.Text;
            //}

            if (string.IsNullOrEmpty(tbNewProjectName.Text))
            {
                XtraMessageBox.Show("New Project is required.", "Error", MessageBoxButtons.OK);
                tbNewProjectName.Focus();
                return;
            }
            newProjectName = tbNewProjectName.Text;

            if (string.IsNullOrEmpty(tbNewProjectLocation.Text))
            {
                XtraMessageBox.Show("New Project Location is required.", "Error", MessageBoxButtons.OK);
                tbNewProjectLocation.Focus();
                return;
            }
            newProjectLocation = tbNewProjectLocation.Text;

            //flipsFileName = tbFLIPSDb.Text;
            //defaultsFileName = tbDefaultsDb.Text;
            //projectFileName = tbProjectDb.Text;

            bProcess = true;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bProcess = false;

            this.Close();
        }

        private void OpenFile(string pDb, string pTable, string pOrderBy, string pFieldName, string pFieldValue)
        {
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                mdbConn = new OleDbConnection(connectionString);

                mdbConn.Open();

                string sql = string.Empty;
                if (!string.IsNullOrEmpty(pFieldName) && !string.IsNullOrEmpty(pFieldValue))
                    sql = string.Format("SELECT * FROM {0} WHERE [{1}] = '{2}'", pTable, pFieldName, pFieldValue);
                else
                    if (!string.IsNullOrEmpty(pOrderBy))
                        sql = string.Format("SELECT * FROM {0} ORDER BY {1}", pTable, pOrderBy);
                    else
                        sql = string.Format("SELECT * FROM {0}", pTable);
                mdbCmd = new OleDbCommand(sql, mdbConn);
                mdbReader = mdbCmd.ExecuteReader();
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch
            {
                MessageBox.Show("Error Opening database");
            }
        }

        private void CloseFile()
        {
            mdbReader.Close();
            mdbCmd.Dispose();
            mdbConn.Close();
        }

        private void ImportSuperACE2008Form_Shown(object sender, EventArgs e)
        {
            tbProjectDb.Focus();
        }

        private void ImportSuperACE2008Form_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}