﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using OpenNETCF.Desktop.Communication;
using System.IO;
using System.Threading;

namespace Import
{
    public partial class GetPocketEASYData : DevExpress.XtraEditors.XtraForm
    {
        private RAPI mRapi = new RAPI();
        private bool bConnected = false;

        private string sDevFileName = "\\Program Files\\ACI\\PocketEasy\\engmet.dat";
        
        public GetPocketEASYData()
        {
            InitializeComponent();
        }

        private void GetPocketEASYData_Load(object sender, EventArgs e)
        {
            XtraMessageBox.Show(string.Format("Make sure your device is connected to the PC via {0}", SyncMethod()), "Reminder");

            bConnected = ConnectDevice();
            if (bConnected)
            {
                btnCopy.Enabled = true;
                tbMessage.Text = " Connected ";
                tbMessage.Update();
            }
            else
                btnCopy.Enabled = false;
        }

        private string SyncMethod()
        {
            // Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            string prg = string.Empty;
            if (os.Platform == PlatformID.Win32NT)
            {
                switch (os.Version.Major)
                {
                    case 5:
                        prg = "Active Sync";
                        break;
                    case 6:
                        prg = "Windows Mobile Device Center";
                        break;
                    default:
                        prg = string.Empty;
                        break;
                }
            }
            else
                prg = string.Empty;
            return prg;
        }

        public bool ConnectDevice()
        {
            if (mRapi.DevicePresent)
            {
                mRapi.Connect();
                tbMessage.Text = " Connected ";
                tbMessage.Update();
                return true;
            }
            else
            {
                XtraMessageBox.Show("Please connect your device to your PC using " + SyncMethod() + " before clicking the OK button.", 
                    "Device was not detected");
                if (mRapi.DevicePresent)
                {
                    mRapi.Connect();
                    tbMessage.Text = " Connected ";
                    tbMessage.Update();
                    return true;
                }
                else
                {
                    XtraMessageBox.Show("Please connect your device to your PC using " + SyncMethod() + " before clicking the OK button. If you have just docked your unit, please wait for " + SyncMethod() + " to start up and connect to your device before pressing OK", 
                        "Device still has not been detected");
                    if (mRapi.DevicePresent)
                    {
                        mRapi.Connect();
                        tbMessage.Text = " Connected ";
                        tbMessage.Update();
                        return true;
                    }
                    else
                    {
                        XtraMessageBox.Show("Please verify " + SyncMethod() + " is running and displays 'Connected' before clicking the OK button. If the device cannot be detected at this point, this application will shut down.", 
                            "Device was not detected, last try.");
                        if (mRapi.DevicePresent)
                        {
                            mRapi.Connect();
                            tbMessage.Text = " Connected ";
                            tbMessage.Update();
                            return true;
                        }
                    }
                }
            }
            XtraMessageBox.Show("Device did not connect.", "Error connecting to Device");
            tbMessage.Text = "Device did not connect.";
            tbMessage.Update();
            return false;
        }

        private void DisconnectDevice()
        {
            if (mRapi.Connected)
                mRapi.Disconnect();
            mRapi.Dispose();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                // delete old engmet file before processing new one
                string sDeviceDbLocation = Properties.Settings.Default["DownloadLocation"].ToString();
                if (sDeviceDbLocation.EndsWith("\\") == true)
                    sDeviceDbLocation += "Download\\dbdir.aci";
                else
                    sDeviceDbLocation += "\\Download\\dbdir.aci";
                if (mRapi.DeviceFileExists("\\Program Files\\ACI\\PocketEASY\\dbdir.aci"))
                {
                    // copy file from the device
                    mRapi.CopyFileFromDevice(sDeviceDbLocation, "\\Program Files\\ACI\\PocketEASY\\dbdir.aci", true);
                    using (StreamReader sr = File.OpenText(sDeviceDbLocation))
                    {
                        sDevFileName = sr.ReadLine();
                        sDevFileName += "engmet.dat";
                        sr.Close();

                        CopyTheFile();
                    }
                }
                else
                {
                    XtraMessageBox.Show("The file does not exist on the mobile device.\nCreate the file on the Mobile Device.", "Error");
                    CloseApp(false);
                }
            }
            catch
            {
                CloseApp(false);
            }
        }

        private void CopyTheFile()
        {
            try
            {
                // see where the engmet.dat file is located
                string sPCFileName = Properties.Settings.Default["DownloadLocation"].ToString();
                if (sPCFileName.EndsWith("\\") == true)
                    sPCFileName += "Download\\engmet.dat";
                else
                    sPCFileName += "\\Download\\engmet.dat";
                if (sPCFileName == null || sPCFileName.Length == 0)
                {
                    XtraMessageBox.Show("ENGMET.DAT is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CloseApp(false);
                }

                try
                {
                    if (File.Exists(sPCFileName))
                        File.Delete(sPCFileName);

                    Thread.Sleep(4000);
                }
                catch
                {
                }

                try
                {
                    if (mRapi.DeviceFileExists(sDevFileName))
                    {
                        // copy file from the device
                        Thread.Sleep(3000);
                        mRapi.CopyFileFromDevice(sPCFileName, sDevFileName, true);
                        Thread.Sleep(3000);
                        mRapi.DeleteDeviceFile(sDevFileName);
                        CloseApp(true);
                    }
                    else
                    {
                        XtraMessageBox.Show("The file does not exist on the mobile device.\nCreate the file on the Mobile Device.", "Error");
                        CloseApp(false);
                    }
                }
                catch (RAPIException ex)
                {
                    XtraMessageBox.Show(ex.Message, "RAPIException");
                    CloseApp(false);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Exception");
                    CloseApp(false);
                }
                catch
                {
                    XtraMessageBox.Show(string.Format("Device={0}, PC={1}", sDevFileName, sPCFileName), "Exception on Copying file from Device");
                    CloseApp(false);
                }
            }
            catch
            {
                CloseApp(false);
            }
        }

        private void CloseApp(bool bComplete)
        {
            DisconnectDevice();
            if (bComplete)
                XtraMessageBox.Show("Process is complete.", "Status", MessageBoxButtons.OK);
            this.Close();
        }
    }
}