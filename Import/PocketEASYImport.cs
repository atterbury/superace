﻿using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using Project.DAL;
using SuperACECalcs;
using SuperACEUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Import
{
    public class PocketEASYImport
    {
        private Project.DAL.Project currentProject = null;
        private string sSourceFile = null;
        private string sDb = null;
        private long prjId = -1;
        private OleDbConnection mdbConn = null;
        private OleDbCommand mdbCmd = null;
        private OleDbDataReader mdbReader = null;
        private DataTable mdbTbl = null;
        private List<Project.DAL.Project> addProjects = new List<Project.DAL.Project>();
        private List<Stand> addStands = new List<Stand>();
        private List<Plot> addPlots = new List<Plot>();
        private List<Tree> addTrees = new List<Tree>();
        private List<Treesegment> addSegments = new List<Treesegment>();

        public bool ProcessProject(ref ProjectDbContext ctx, string pDb, string pTable, string pOldProject, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Project");
            OpenFile(pDb, pTable, "", "", "");
            ImportProject(ref ctx, pOldProject, pProject);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessProjectTables(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Tables");
            OpenFile(pDb, "TTables", "", "", "");
            ImportProjectTable(ref ctx, pProject);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessStand(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Stands");
            OpenFile(pDb, pTable, "[Township], [Range], [Section], [Tract], [Type]", "", "");
            ImportStand(ref ctx, pProject);
            SplashScreenManager.CloseForm();
            CloseFile();

            return true;
        }

        public bool ProcessStandInput(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Stand Input");
            OpenFile(pDb, pTable, "[Township], [Range], [Section], [Type]", "", "");
            ImportStandInput(ref ctx, pProject);
            SplashScreenManager.CloseForm();
            CloseFile();

            return true;
        }

        public bool ProcessPlots(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Plots");
            OpenFile(pDb, "TPLOTLOC", "[Township], [Range], [Section], [Tract], [Type], [PlotNo]", "", "");
            ImportPlot(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessTrees(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Trees");
            OpenFile(pDb, "TCRUISE", "[Township], [Range], [Section], [Tract], [Type], [PlotNo], [TreeNo], [Continuation]", "[Township], [Range], [Section], [Tract], [Type], [PlotNo], [TreeNo], [Continuation]", "");
            ImportTree(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();
            
            return true;
        }

        private void OpenFile(string pDb, string pTable, string pOrderBy, string pFieldName, string pFieldValue) //
        {
            //string sDir = sSourceFile.Substring(0, sSourceFile.LastIndexOf('\\'));
            //string sFile = sSourceFile.Substring(sSourceFile.LastIndexOf('\\') + 1, sSourceFile.LastIndexOf('.') - (sSourceFile.LastIndexOf('\\') + 1));
            //string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}", pDb);
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                mdbConn = new OleDbConnection(connectionString);

                mdbConn.Open();

                string sql = string.Empty;
                if (!string.IsNullOrEmpty(pFieldName) && !string.IsNullOrEmpty(pFieldValue))
                    sql = string.Format("SELECT * FROM {0} WHERE [{1}] = '{2}'", pTable, pFieldName, pFieldValue);
                else
                    if (!string.IsNullOrEmpty(pOrderBy))
                        sql = string.Format("SELECT * FROM {0} ORDER BY {1}", pTable, pOrderBy);
                    else
                        sql = string.Format("SELECT * FROM {0}", pTable);
                mdbCmd = new OleDbCommand(sql, mdbConn);
                mdbReader = mdbCmd.ExecuteReader();
            }
            catch (System.InvalidOperationException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            catch
            {
                XtraMessageBox.Show("Error Opening database");
            }
        }

        private void CloseFile()
        {
            //mdbTbl.Dispose();
            mdbReader.Close();
            mdbCmd.Dispose();
            mdbConn.Close();
        }

        private void ImportProject(ref ProjectDbContext ctx, string pOldProject, string pProject)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            while (mdbReader.Read())
            {
                if (mdbReader["Project"].ToString().Trim() == pOldProject)
                {
                    Project.DAL.Project prj = new Project.DAL.Project();
                    //prj.AspectsTableName = string.Empty;
                    //prj.ComponentsTableName = string.Empty;
                    //prj.CostTableName = string.Empty;
                    //prj.CrownPositionTableName = string.Empty;
                    //prj.CruisersTableName = string.Empty;
                    prj.CustomerAddress1 = mdbReader["Address 1"].ToString().Trim();
                    prj.CustomerAddress2 = mdbReader["Address 2"].ToString().Trim();
                    prj.CustomerCellPhone = string.Empty;
                    prj.CustomerCity = string.Empty;
                    prj.CustomerCompanyName = mdbReader["Customer Name"].ToString().Trim();
                    prj.CustomerContactName = mdbReader["Key Contact"].ToString().Trim();
                    prj.CustomerEmail = mdbReader["EMail"].ToString().Trim();
                    prj.CustomerFax = mdbReader["Fax"].ToString().Trim();
                    prj.CustomerNotes = string.Empty;
                    prj.CustomerOfficePhone = mdbReader["Phone"].ToString().Trim();
                    prj.CustomerPostalCode = string.Empty;
                    prj.CustomerState = string.Empty;
                    //prj.DamageTableName = string.Empty;
                    //prj.EnvironmentsTableName = string.Empty;
                    //prj.GISProjectName = string.Empty;
                    //prj.GradesTableName = string.Empty;
                    //prj.GrowthModelTableName = string.Empty;
                    //prj.HabitatTableName = string.Empty;
                    //prj.HarvestSystemsTableName = string.Empty;
                    //prj.LandFormsTableName = string.Empty;
                    //prj.NonStockedTableName = string.Empty;
                    //prj.NonTimberedTableName = string.Empty;
                    //prj.PriceTableName = string.Empty;
                    //prj.ProjectAdjustmentsTableName = string.Empty;
                    //prj.ProjectDate = string.Empty;
                    prj.ProjectLead = string.Empty;
                    prj.ProjectName = pProject;
                    prj.ProjectNumber = string.Empty;
                    //prj.RoadsTableName = string.Empty;
                    prj.SaleName = string.Empty;
                    //prj.SeedZonesTableName = string.Empty;
                    //prj.SlashDistributionsTableName = string.Empty;
                    //prj.SoilsTableName = string.Empty;
                    //prj.SortsTableName = string.Empty;
                    //prj.SpeciesTableName = string.Empty;
                    //prj.StandActivitiesTableName = string.Empty;
                    //prj.StandDataSourcesTableName = string.Empty;
                    //prj.StreamsTableName = string.Empty;
                    prj.SureToBeMeasured = string.Empty;
                    //prj.TreatmentsTableName = string.Empty;
                    //prj.TreeSourcesTableName = string.Empty;
                    //prj.TreeStatusTableName = string.Empty;
                    //prj.UserDefinedTableName = string.Empty;
                    //prj.VegetationsTableName = string.Empty;
                    //prj.VigorTableName = string.Empty;

                    ctx.Projects.Add(prj);
                }
            }
            ctx.SaveChanges();

            //ctx.Dispose();
        }

        private void ImportProjectTable(ref ProjectDbContext ctx, string pProjectName)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            if (mdbReader.Read())
            {
                Project.DAL.Project prj = ctx.Projects.FirstOrDefault(p => p.ProjectName == pProjectName);
                prj.AspectsTableName = mdbReader["Aspect"].ToString().Trim();
                prj.ASuboTableName = "GENERAL";
                prj.BarkTableName = "GENERAL";
                prj.BdFtRule = "GENERAL";
                prj.ComponentsTableName = "GENERAL";
                prj.CostTableName = mdbReader["Cost"].ToString().Trim();
                prj.CrownPositionTableName = mdbReader["Crown Position"].ToString().Trim();
                prj.CruisersTableName = string.Empty;
                prj.CuFtRule = "GENERAL";
                prj.DamageTableName = mdbReader["Damage"].ToString().Trim();
                prj.DestinationTableName = "GENERAL";
                prj.EnvironmentsTableName = mdbReader["Environment"].ToString().Trim();
                prj.FormTableName = "GENERAL";
                prj.GradesTableName = mdbReader["SortGrade"].ToString().Trim();
                prj.GrowthModelTableName = string.Empty;
                prj.HabitatTableName = string.Empty;
                prj.HarvestSystemsTableName = mdbReader["Harvest"].ToString().Trim();
                prj.LandFormsTableName = mdbReader["Landform"].ToString().Trim();
                prj.NonStockedTableName = "GENERAL";
                prj.NonTimberedTableName = "GENERAL";
                prj.PilingTableName = "GENERAL";
                prj.PolesTableName = "GENERAL";
                prj.PriceTableName = mdbReader["Price"].ToString().Trim();
                prj.ProjectAdjustmentsTableName = "GENERAL";
                prj.RoadsTableName = mdbReader["Roads"].ToString().Trim();
                prj.SeedZonesTableName = mdbReader["Seed Zone"].ToString().Trim();
                prj.SlashDistributionsTableName = mdbReader["Slash"].ToString().Trim();
                prj.SoilsTableName = mdbReader["Soils"].ToString().Trim();
                prj.SortsTableName = mdbReader["SortGrade"].ToString().Trim();
                prj.SpeciesTableName = mdbReader["Species"].ToString().Trim();
                prj.StandActivitiesTableName = "GENERAL";
                prj.StandDataSourcesTableName = "GENERAL";
                prj.StreamsTableName = mdbReader["Stream"].ToString().Trim();
                prj.TreatmentsTableName = mdbReader["Treatment"].ToString().Trim();
                prj.TreeSourcesTableName = mdbReader["Tree Source"].ToString().Trim();
                prj.TreeStatusTableName = mdbReader["Status"].ToString().Trim();
                prj.UserDefinedTableName = mdbReader["T5"].ToString().Trim();
                prj.VegetationsTableName = mdbReader["Vegetation"].ToString().Trim();
                prj.VigorTableName = mdbReader["Vigor"].ToString().Trim();
                prj.WoodTypeTableName = "GENERAL";
                currentProject = prj;
            }
            ctx.SaveChanges();
            //ctx.Dispose();
        }

        private void ImportStand(ref ProjectDbContext ctx, string pProject)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            Project.DAL.Project prj = ctx.Projects.FirstOrDefault(p => p.ProjectName == pProject);
            while (mdbReader.Read())
            {
                Stand stand = new Stand();
                prjId = (long)prj.ProjectsId;
                stand.ProjectsId = (int)prj.ProjectsId;
                int month = Utils.ConvertToInt(mdbReader["Month"].ToString());
                int day = Utils.ConvertToInt(mdbReader["Day"].ToString());
                int year = Utils.ConvertToInt(mdbReader["Year"].ToString());

                if (day == 0)
                    day = 1;
                stand.DateOfStandData = Convert.ToDateTime(string.Format("{0}-{1}-{2}", month, day, year));
                stand.GrownToDate = Convert.ToDateTime(string.Format("{0}-{1}-{2}", month, day, year));
                stand.GrownToMonth = Utils.ConvertToInt(mdbReader["Grown Month"].ToString());
                stand.GrownToYear = Utils.ConvertToInt(mdbReader["Grown Year"].ToString());
                stand.AvgTotalHeight = 0;
                stand.BasalAreaPerAcre = 0;
                //stand.BirthDate = null;
                stand.CcfPerAcre = 0;
                if (!string.IsNullOrEmpty(mdbReader["Cost"].ToString().Trim()))
                    stand.CostTableName = mdbReader["Cost"].ToString().Trim();
                else
                    stand.CostTableName = string.Empty;
                stand.DiameterInterval = string.Empty;
                stand.DiameterRound = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Dia Interval"].ToString()))
                //    stand.DiameterInterval = mdbReader["Dia Interval"].ToString();
                //else
                //stand.DiameterInterval = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Dia Round"].ToString()))
                ////    stand.DiameterRound = mdbReader["Dia Round"].ToString();
                ////else
                //stand.DiameterRound = string.Empty;
                //stand.EnvironmentsDisplayCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Interval Zero"].ToString()))
                ////    stand.IntervalZero = mdbReader["Interval Zero"].ToString();
                ////else
                //    stand.IntervalZero = string.Empty;
                stand.GrossGeographicAcres = (double)Utils.ConvertToDouble(mdbReader["Acres"].ToString());
                stand.NetGeographicAcres = (double)Utils.ConvertToDouble(mdbReader["Acres"].ToString());
                stand.LegalAcres = (double)Utils.ConvertToDouble(mdbReader["Acres"].ToString());
                stand.LogsPerAcre = 0;
                stand.MajorAge = 0;
                stand.MbfPerAcre = 0;
                stand.Meridian = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Price"].ToString().Trim()))
                    stand.PriceTableName = mdbReader["Price"].ToString().Trim();
                else
                    stand.PriceTableName = string.Empty;
                stand.QmDbh = 0;
                stand.Range = mdbReader["Range"].ToString().Trim();
                ////if (!string.IsNullOrEmpty(mdbReader["Samples Each Class"].ToString()))
                ////    stand.SamplesEachClass = mdbReader["Samples Each Class"].ToString();
                ////else
                //    stand.SamplesEachClass = string.Empty;
                stand.Section = mdbReader["Section"].ToString().Trim();
                //stand.SeedZonesDisplayCode = string.Empty;
                //stand.SlashDistributionsDisplayCode = string.Empty;
                //stand.SNCRetentionYears = 0;
                //stand.SoilsDisplayCode = string.Empty;
                //stand.StandAdjustmentsTableName = string.Empty;
                //stand.StandDataSourcesDisplayCode = string.Empty;
                //stand.StandExamDate = null;
                //stand.StandGrowthProjectionDate = null;
                stand.StandName = mdbReader["Type"].ToString().Trim();
                //stand.StockingStatusPrimaryDisplayCode = string.Empty;
                //stand.StockingStatusSecondaryDisplayCode = string.Empty;
                //stand.StreamsDisplayCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Sure To Be Measured"].ToString()))
                ////    stand.SureToBeMeasured = mdbReader["Sure To Be Measured"].ToString();
                ////else
                //    stand.SureToBeMeasured = string.Empty;
                stand.TonsPerAcre = 0;
                stand.TotalBdFtGross = 0;
                stand.TotalBdFtNet = 0;
                stand.TotalCcf = 0;
                stand.TotalCuFtGross = 0;
                stand.TotalCuFtMerch = 0;
                stand.TotalCuFtNet = 0;
                stand.TotalMbf = 0;
                stand.TotalTons = 0;
                stand.Township = mdbReader["Township"].ToString().Trim();
                stand.TractName = mdbReader["Tract"].ToString().Trim();
                stand.TractGroup = mdbReader["Tract"].ToString().Trim();
                stand.TransportationsTableName = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Source"].ToString().Trim()))
                    stand.Source = mdbReader["Source"].ToString().Trim();
                else
                    stand.Source = string.Empty;
                stand.TreesPerAcre = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["UseStndDbh"].ToString()))
                ////    stand.UseStandDbh = mdbReader["UseStndDbh"].ToString();
                ////else
                //    stand.UseStandDbh = string.Empty;
                //stand.VegetationsDisplayCode = string.Empty;

                //stand.Age1 = (short)Utils.ConvertToInt(mdbReader["Age1"].ToString());
                //stand.Age2 = (short)Utils.ConvertToInt(mdbReader["Age2"].ToString());
                //stand.Age3 = (short)Utils.ConvertToInt(mdbReader["Age3"].ToString());
                //stand.Age4 = (short)Utils.ConvertToInt(mdbReader["Age4"].ToString());
                //stand.Age5 = (short)Utils.ConvertToInt(mdbReader["Age5"].ToString());
                //stand.Age6 = (short)Utils.ConvertToInt(mdbReader["Age6"].ToString());
                //stand.Age7 = (short)Utils.ConvertToInt(mdbReader["Age7"].ToString());
                //stand.Age8 = (short)Utils.ConvertToInt(mdbReader["Age8"].ToString());
                //stand.Age9 = (short)Utils.ConvertToInt(mdbReader["Age9"].ToString());

                stand.Baf1 = Utils.ConvertToDouble(mdbReader["BAFactor1"].ToString());
                stand.BafPlotRadius1 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf1);
                stand.Baf2 = Utils.ConvertToDouble(mdbReader["BAFactor2"].ToString());
                stand.BafPlotRadius2 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf2);
                stand.Baf3 = Utils.ConvertToDouble(mdbReader["BAFactor3"].ToString());
                stand.BafPlotRadius3 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf3);
                stand.Baf4 = Utils.ConvertToDouble(mdbReader["BAFactor4"].ToString());
                stand.BafPlotRadius4 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf4);
                stand.Baf5 = Utils.ConvertToDouble(mdbReader["BAFactor5"].ToString());
                stand.BafPlotRadius5 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf5);

                PopulateArea(ref stand, mdbReader);
                // Not used anymore
                //stand.FixedArea1 = Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString());
                //stand.FixedArea2 = Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString());
                //stand.FixedArea3 = Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString());
                //stand.FixedArea4 = Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString());
                //stand.FixedArea5 = Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString());

                //stand.Ref1 = Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString());
                //stand.Ref2 = Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString());
                //stand.Ref3 = Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString());
                //stand.Ref4 = Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString());
                //stand.Ref5 = Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString());

                stand.StripBlowup1 = Utils.ConvertToDouble(mdbReader["StripFact1"].ToString());
                stand.StripBlowup2 = Utils.ConvertToDouble(mdbReader["StripFact2"].ToString());
                stand.StripBlowup3 = Utils.ConvertToDouble(mdbReader["StripFact3"].ToString());
                stand.StripBlowup4 = Utils.ConvertToDouble(mdbReader["StripFact4"].ToString());
                stand.StripBlowup5 = Utils.ConvertToDouble(mdbReader["StripFact5"].ToString());

                stand.Species11 = mdbReader["Species11"].ToString().Trim();
                stand.Species12 = mdbReader["Species12"].ToString().Trim();
                stand.Species13 = mdbReader["Species13"].ToString().Trim();
                stand.SiteIndex1 = (short)Utils.ConvertToInt(mdbReader["SiteIndex1"].ToString());

                stand.Species21 = mdbReader["Species21"].ToString().Trim();
                stand.Species22 = mdbReader["Species22"].ToString().Trim();
                stand.Species23 = mdbReader["Species23"].ToString().Trim();
                stand.SiteIndex2 = (short)Utils.ConvertToInt(mdbReader["SiteIndex2"].ToString());

                stand.Species31 = mdbReader["Species31"].ToString().Trim();
                stand.Species32 = mdbReader["Species32"].ToString().Trim();
                stand.Species33 = mdbReader["Species33"].ToString().Trim();
                stand.SiteIndex3 = (short)Utils.ConvertToInt(mdbReader["SiteIndex3"].ToString());

                stand.Species41 = mdbReader["Species41"].ToString().Trim();
                stand.Species42 = mdbReader["Species42"].ToString().Trim();
                stand.Species43 = mdbReader["Species43"].ToString().Trim();
                stand.SiteIndex4 = (short)Utils.ConvertToInt(mdbReader["SiteIndex4"].ToString());

                //// TODO Cruisers
                ////if (!string.IsNullOrEmpty(mdbReader["Cruiser"].ToString()))
                ////    cs.Cruiser = mdbReader["Cruiser"].ToString();
                ////else
                ////    cs.Cruiser = string.Empty;

                ////if (!string.IsNullOrEmpty(mdbReader["Destination"].ToString()))
                ////    cs.Destination = mdbReader["Destination"].ToString();
                ////else
                ////    cs.Destination = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Hours"].ToString()))
                ////    cs.Hours = float.Parse(mdbReader["Hours"].ToString());
                ////else
                ////    cs.Hours = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Miles"].ToString()))
                ////    cs.Miles = float.Parse(mdbReader["Miles"].ToString());
                ////else
                ////    cs.Miles = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Plots"].ToString()))
                ////    cs.Plots = Int16.Parse(mdbReader["Plots"].ToString());
                ////else
                ////    cs.Plots = 0;
                if (!string.IsNullOrEmpty(mdbReader["Grown Month"].ToString()))
                    stand.GrownToMonth = Int16.Parse(mdbReader["Grown Month"].ToString());
                if (!string.IsNullOrEmpty(mdbReader["Grown Year"].ToString()))
                    stand.GrownToYear = Int16.Parse(mdbReader["Grown Year"].ToString());
                if (!string.IsNullOrEmpty(mdbReader["Catagory"].ToString()))
                {
                    if (mdbReader["Catagory"].ToString().StartsWith("NS"))
                        stand.LandClass = "NS";
                    else
                        if (mdbReader["Catagory"].ToString().StartsWith("NT"))
                            stand.LandClass = "NT";
                        else
                            stand.LandClass = "TIM";
                }
                else
                {
                    stand.LandClass = "TIM";
                }
                ////if (!string.IsNullOrEmpty(mdbReader["Non Forest"].ToString()))
                ////    cs.NonForest = mdbReader["Non Forest"].ToString();
                ////else
                ////    cs.NonForest = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Site Index"].ToString()))
                    stand.SiteIndex = Int16.Parse(mdbReader["Site Index"].ToString());
                else
                    stand.SiteIndex = 0;

                ////if (!string.IsNullOrEmpty(mdbReader["Weyco D4h"].ToString()))
                ////    cs.WeycoD4h = mdbReader["Weyco D4h"].ToString();
                ////else
                ////    cs.WeycoD4h = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Inv Code"].ToString()))
                ////    cs.InvCode = mdbReader["Inv Code"].ToString();
                ////else
                ////    cs.InvCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["EngMet Code"].ToString()))
                ////    cs.EngMetCode = mdbReader["EngMet Code"].ToString();
                ////else
                ////    cs.EngMetCode = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Inventory Flag"].ToString().Trim()))
                    stand.InventoryFlag = mdbReader["Inventory Flag"].ToString().Trim();
                else
                    stand.InventoryFlag = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Cruise Flag"].ToString().Trim()))
                    stand.CruiseFlag = mdbReader["Cruise Flag"].ToString().Trim();
                else
                    stand.CruiseFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Reforestation Flag"].ToString()))
                ////    cs.ReforestationFlag = mdbReader["Reforestation Flag"].ToString();
                ////else
                ////    cs.ReforestationFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Site Flag"].ToString()))
                ////    cs.SiteFlag = mdbReader["Site Flag"].ToString();
                ////else
                ////    cs.SiteFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Class Flag"].ToString()))
                ////    cs.ClassFlag = mdbReader["Class Flag"].ToString();
                ////else
                ////    cs.ClassFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Scale Flag"].ToString()))
                ////    cs.ScaleFlag = mdbReader["Scale Flag"].ToString();
                ////else
                ////    cs.ScaleFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Cruise Trees"].ToString()))
                ////    cs.CruiseTrees = Int16.Parse(mdbReader["Cruise Trees"].ToString());
                ////else
                ////    cs.CruiseTrees = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Count Trees"].ToString()))
                ////    cs.CountTrees = Int16.Parse(mdbReader["Count Trees"].ToString());
                ////else
                ////    cs.CountTrees = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Percent Trees Cruised"].ToString()))
                ////    cs.PercentTreesCruised = Int16.Parse(mdbReader["Percent Trees Cruised"].ToString());
                ////else
                ////    cs.PercentTreesCruised = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Av Plots Cruised"].ToString()))
                ////    cs.AvPlotsCruised = Single.Parse(mdbReader["Av Plots Cruised"].ToString());
                ////else
                ////    cs.AvPlotsCruised = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Av Plots Counted"].ToString()))
                ////    cs.AvPlotsCounted = Single.Parse(mdbReader["Av Plots Counted"].ToString());
                ////else
                ////    cs.AvPlotsCounted = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["InvAdjustmentTbl"].ToString()))
                ////    cs.InvAdjustmentTbl = mdbReader["InvAdjustmentTbl"].ToString();
                ////else
                ////    cs.InvAdjustmentTbl = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["CruAdjustmentTbl"].ToString()))
                ////    cs.CruAdjustmentTbl = mdbReader["CruAdjustmentTbl"].ToString();
                ////else
                ////    cs.CruAdjustmentTbl = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["CalcBaTpa"].ToString()))
                ////    cs.CalcBaTpa = mdbReader["CalcBaTpa"].ToString();
                ////else
                ////    cs.CalcBaTpa = string.Empty;
                addStands.Add(stand);
                //ctx.Stands.Add(stand);
            }
            ctx.Stands.AddRange(addStands);
            ctx.SaveChanges();
            //ctx.Dispose();
        }

        private void CalcFixed(ref Stand stand, short i, string AreaCode, string AreaPlotAcres)
        {
            string plotRadius = Utils.CalculateAreaPlotRadius(AreaCode, AreaPlotAcres);
            string blowup = Utils.CalculateAreaBlowup(AreaCode, AreaPlotAcres);
            if (!string.IsNullOrEmpty(plotRadius))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaPlotRadius1 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 2:
                        stand.AreaPlotRadius2 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 3:
                        stand.AreaPlotRadius3 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 4:
                        stand.AreaPlotRadius4 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 5:
                        stand.AreaPlotRadius5 = Utils.ConvertToDouble(plotRadius);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(blowup))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaBlowup1 = Utils.ConvertToDouble(blowup);
                        break;
                    case 2:
                        stand.AreaBlowup2 = Utils.ConvertToDouble(blowup);
                        break;
                    case 3:
                        stand.AreaBlowup3 = Utils.ConvertToDouble(blowup);
                        break;
                    case 4:
                        stand.AreaBlowup4 = Utils.ConvertToDouble(blowup);
                        break;
                    case 5:
                        stand.AreaBlowup5 = Utils.ConvertToDouble(blowup);
                        break;
                }
            }
        }

        private void CalcRef(ref Stand stand, short i, string AreaCode, string AreaPlotRadius)
        {
            string plotAcres = Utils.CalculateAreaPlotAcres(AreaCode, AreaPlotRadius);
            string blowup = Utils.CalculateAreaBlowup(AreaCode, plotAcres);
            if (!string.IsNullOrEmpty(plotAcres))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaPlotAcres1 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 2:
                        stand.AreaPlotAcres2 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 3:
                        stand.AreaPlotAcres3 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 4:
                        stand.AreaPlotAcres4 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 5:
                        stand.AreaPlotAcres5 = Utils.ConvertToDouble(plotAcres);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(blowup))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaBlowup1 = Utils.ConvertToDouble(blowup);
                        break;
                    case 2:
                        stand.AreaBlowup2 = Utils.ConvertToDouble(blowup);
                        break;
                    case 3:
                        stand.AreaBlowup3 = Utils.ConvertToDouble(blowup);
                        break;
                    case 4:
                        stand.AreaBlowup4 = Utils.ConvertToDouble(blowup);
                        break;
                    case 5:
                        stand.AreaBlowup5 = Utils.ConvertToDouble(blowup);
                        break;
                }
            }
        }

        private void PopulateArea(ref Stand stand, OleDbDataReader mdbReader)
        {
            if (Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString()) > 0)
            {
                stand.AreaCode1 = "F1";
                stand.AreaPlotAcres1 = Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString());
                CalcFixed(ref stand, 1, stand.AreaCode1, stand.AreaPlotAcres1.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString()) > 0)
            {
                stand.AreaCode1 = "R1";
                stand.AreaPlotRadius1 = Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString());
                CalcRef(ref stand, 1, stand.AreaCode1, stand.AreaPlotRadius1.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString()) > 0)
            {
                stand.AreaCode2 = "F2";
                stand.AreaPlotAcres2 = Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString());
                CalcFixed(ref stand, 2, stand.AreaCode2, stand.AreaPlotAcres2.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString()) > 0)
            {
                stand.AreaCode2 = "R2";
                stand.AreaPlotRadius2 = Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString());
                CalcRef(ref stand, 2, stand.AreaCode2, stand.AreaPlotRadius2.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString()) > 0)
            {
                stand.AreaCode3 = "F3";
                stand.AreaPlotAcres3 = Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString());
                CalcFixed(ref stand, 3, stand.AreaCode3, stand.AreaPlotAcres3.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString()) > 0)
            {
                stand.AreaCode3 = "R3";
                stand.AreaPlotRadius3 = Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString());
                CalcRef(ref stand, 3, stand.AreaCode3, stand.AreaPlotRadius3.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString()) > 0)
            {
                stand.AreaCode4 = "F4";
                stand.AreaPlotAcres4 = Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString());
                CalcFixed(ref stand, 4, stand.AreaCode4, stand.AreaPlotAcres4.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString()) > 0)
            {
                stand.AreaCode4 = "R4";
                stand.AreaPlotRadius4 = Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString());
                CalcRef(ref stand, 4, stand.AreaCode4, stand.AreaPlotRadius4.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString()) > 0)
            {
                stand.AreaCode5 = "F5";
                stand.AreaPlotAcres5 = Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString());
                CalcFixed(ref stand, 5, stand.AreaCode5, stand.AreaPlotAcres5.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString()) > 0)
            {
                stand.AreaCode5 = "R5";
                stand.AreaPlotRadius5 = Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString());
                CalcRef(ref stand, 5, stand.AreaCode5, stand.AreaPlotRadius5.ToString());
            }
        }

        private void ImportStandInput(ref ProjectDbContext ctx, string pProject)
        {
            Stand currentStand = null;
            string currentKey = string.Empty;
            string saveKey = string.Empty;

            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            //Project prj = ctx.Projects.FirstOrDefault(p => p.ProjectName == pProject);
            while (mdbReader.Read())
            {
                //string sTract = mdbReader["Tract"].ToString();
                string sStand = mdbReader["Type"].ToString();
                string sTownship = mdbReader["Township"].ToString();
                string sRange = mdbReader["Range"].ToString();
                string sSection = mdbReader["Section"].ToString();
                currentKey = string.Format("{0}{1}{2}{3}", sStand, sTownship, sRange, sSection);
                if (saveKey != currentKey)
                {
                    //currentStand = ProjectBLL.GetStand(pDataSource, sTract, sStand, sTownship, sRange, sSection);
                    currentStand = addStands.FirstOrDefault(p => p.StandName == sStand && p.Township == sTownship && p.Range == sRange && p.Section == sSection);
                    saveKey = currentKey;
                }

                if (currentStand != null)
                {
                    StandInput standInput = new StandInput();
                    standInput.Age = (short)Utils.ConvertToInt(mdbReader["Age"].ToString());
                    standInput.BasalArea = Utils.ConvertToDouble(mdbReader["Basal Area"].ToString());
                    standInput.BirthYear = (short)Utils.ConvertToInt(mdbReader["Birth Year"].ToString());
                    standInput.Cost = Utils.ConvertToDouble(mdbReader["Cost"].ToString());
                    standInput.D4H = (short)Utils.ConvertToInt(mdbReader["D4h"].ToString());
                    standInput.DcHeight = 0;
                    standInput.GrossBdFt = Utils.ConvertToDouble(mdbReader["Gross Bdft"].ToString());
                    standInput.GrossCuFt = Utils.ConvertToDouble(mdbReader["Gross CuFt"].ToString());
                    standInput.LogsPerAcre = Utils.ConvertToDouble(mdbReader["Logs"].ToString());
                    standInput.NetAcres = currentStand.NetGeographicAcres;
                    standInput.NetBdFt = Utils.ConvertToDouble(mdbReader["Net BdFt"].ToString());
                    standInput.NetCuFt = Utils.ConvertToDouble(mdbReader["Net CuFt"].ToString());
                    standInput.OrigAge = (short)Utils.ConvertToInt(mdbReader["OrigAge"].ToString());
                    standInput.OrigBasalArea = Utils.ConvertToDouble(mdbReader["OrigBasalArea"].ToString());
                    standInput.OrigD4H = (short)Utils.ConvertToInt(mdbReader["OrigD4h"].ToString());
                    standInput.OrigDcHeight = 0;
                    standInput.OrigGrossBdFt = Utils.ConvertToDouble(mdbReader["OrigGrossBdFt"].ToString());
                    standInput.OrigGrossCuFt = Utils.ConvertToDouble(mdbReader["OrigGrossCuFt"].ToString());
                    standInput.OrigLogDib = (short)Utils.ConvertToInt(mdbReader["OrigLogDib"].ToString());
                    standInput.OrigLogLen = (short)Utils.ConvertToInt(mdbReader["OrigLogLen"].ToString());
                    standInput.OrigLogsPerAcre = Utils.ConvertToDouble(mdbReader["OrigLogs"].ToString());
                    standInput.OrigNetBdFt = Utils.ConvertToDouble(mdbReader["OrigNetBdFt"].ToString());
                    standInput.OrigNetCuFt = Utils.ConvertToDouble(mdbReader["OrigNetCuFt"].ToString());
                    standInput.OrigStocking = (short)Utils.ConvertToInt(mdbReader["OrigStocking"].ToString());
                    standInput.OrigTotalHeight = Utils.ConvertToDouble(mdbReader["OrigTotalHeight"].ToString());
                    standInput.OrigTreesPerAcre = Utils.ConvertToDouble(mdbReader["OrigTpa"].ToString());
                    standInput.PoleSeq = (short)Utils.ConvertToInt(mdbReader["Pole Seq"].ToString()); ;
                    standInput.Revenue = Utils.ConvertToDouble(mdbReader["Revenue"].ToString()); ;
                    standInput.SiteIndex = (short)Utils.ConvertToInt(mdbReader["Site Index"].ToString());
                    standInput.Species = mdbReader["Species"].ToString();
                    standInput.StandsId = currentStand.StandsId;
                    standInput.Status = mdbReader["Status"].ToString();
                    standInput.Stocking = (short)Utils.ConvertToInt(mdbReader["Stocking"].ToString());
                    standInput.TotalHeight = Utils.ConvertToDouble(mdbReader["Total Height"].ToString());
                    standInput.TreesPerAcre = Utils.ConvertToDouble(mdbReader["Tpa"].ToString());

                    ctx.StandInputs.Add(standInput);
                }
            }
            ctx.SaveChanges();
            //ctx.Dispose();
        }

        //private void ImportSection()
        //{
        //    /*
        //    ProjectCollection coll = new ProjectCollection();
        //    coll.LoadAll();

        //    while (mdbReader.Read())
        //    {
        //        Project.DAL.Project cs = coll.AddNew();

        //        cs.Project = mdbReader["Project"].ToString();
        //        if (!string.IsNullOrEmpty(mdbReader["Customer Name"].ToString()))
        //            cs.CustomerName = mdbReader["Customer Name"].ToString();
        //        else
        //            cs.CustomerName = string.Empty;
        //        if (!string.IsNullOrEmpty(mdbReader["Location"].ToString()))
        //            cs.Location = mdbReader["Location"].ToString();
        //        else
        //            cs.Location = string.Empty;
        //        cs.MapData = string.Empty;
        //        cs.MapProject = string.Empty;
        //        cs.Meridian = string.Empty;
        //    }
        //    coll.Save();
        //     */
        //}

        private void ImportPlot(ref ProjectDbContext ctx)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));

            Stand currentStand = null;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            //ctx.Configuration.AutoDetectChangesEnabled = false;

            while (mdbReader.Read())
            {
                try
                {
                    string sTract = mdbReader["Tract"].ToString();
                    string sStand = mdbReader["Type"].ToString();
                    string sTownship = mdbReader["Township"].ToString();
                    string sRange = mdbReader["Range"].ToString();
                    string sSection = mdbReader["Section"].ToString();
                    currentKey = string.Format("{0}{1}{2}{3}{4}", sTract, sStand, sTownship, sRange, sSection);
                    if (saveKey != currentKey)
                    {
                        //currentStand = ProjectBLL.GetStand(pDataSource, sTract, sStand, sTownship, sRange, sSection);
                        currentStand = addStands.FirstOrDefault(p => p.TractName == sTract && p.StandName == sStand && p.Township == sTownship && p.Range == sRange && p.Section == sSection);
                        saveKey = currentKey;
                    }

                    if (currentStand != null)
                    {
                        Plot cs = new Plot();
                        cs.StandsId = currentStand.StandsId;
                        //cs.Stand = currentStand;
                        cs.Notes = string.Empty;
                        //cs.PlotDate = Convert.ToDateTime(string.Format("{0}-{1}-{2}", mdbReader["Month"].ToString(), mdbReader["Day"].ToString(), mdbReader["Year"].ToString()));
                        cs.TreatmentDisplayCode = string.Empty;
                        cs.UserPlotNumber = mdbReader["PlotNo"].ToString().Trim();
                        if (!string.IsNullOrEmpty(mdbReader["Cruiser"].ToString()))
                            cs.Cruiser = mdbReader["Cruiser"].ToString().Trim();
                        else
                            cs.Cruiser = string.Empty;
                        if (!string.IsNullOrEmpty(mdbReader["CoordX"].ToString()))
                            cs.X = float.Parse(mdbReader["CoordX"].ToString());
                        else
                            cs.X = 0;
                        if (!string.IsNullOrEmpty(mdbReader["CoordY"].ToString()))
                            cs.Y = float.Parse(mdbReader["CoordY"].ToString());
                        else
                            cs.Y = 0;
                        if (!string.IsNullOrEmpty(mdbReader["CoordZ"].ToString()))
                            cs.Z = float.Parse(mdbReader["CoordZ"].ToString());
                        else
                            cs.Z = 0;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Bearing"].ToString()))
                        //    cs.Bearing = mdbReader["Bearing"].ToString();
                        //else
                        //    cs.Bearing = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Source"].ToString()))
                        //    cs.Source = mdbReader["Source"].ToString();
                        //else
                        //    cs.Source = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Comments"].ToString()))
                        //    cs.Comments = mdbReader["Comments"].ToString();
                        //else
                        //    cs.Comments = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Weyco D4h"].ToString()))
                        //    cs.WeycoD4h = mdbReader["Weyco D4h"].ToString();
                        //else
                        //    cs.WeycoD4h = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["EngMet Code"].ToString()))
                        //    cs.EngMetCode = mdbReader["EngMet Code"].ToString();
                        //else
                        //    cs.EngMetCode = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Distance"].ToString()))
                        //    cs.Distance = Int16.Parse(mdbReader["Distance"].ToString());
                        //else
                        //    cs.Distance = 0;
                        if (!string.IsNullOrEmpty(mdbReader["Slope"].ToString()))
                            cs.Slope = mdbReader["Slope"].ToString();
                        else
                            cs.Slope = string.Empty;
                        if (!string.IsNullOrEmpty(mdbReader["Aspect"].ToString()))
                            cs.Aspect = mdbReader["Aspect"].ToString();
                        else
                            cs.Aspect = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Desc"].ToString()))
                        //    cs.Description = mdbReader["Desc"].ToString();
                        //else
                        //    cs.Description = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Ind Species"].ToString()))
                        //    cs.IndSpecies = mdbReader["Ind Species"].ToString();
                        //else
                        //    cs.IndSpecies = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Cruise Flag"].ToString()))
                        //    cs.CruiseFlag = mdbReader["Cruise Flag"].ToString();
                        //else
                        //    cs.CruiseFlag = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //ctx.Plot.Add(cs);
                        addPlots.Add(cs);
                    }
                }
                catch (NotSupportedException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    XtraMessageBox.Show("Unhandled Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            ctx.Plots.AddRange(addPlots);
            ctx.SaveChanges();
            //ctx.Dispose();
        }

        private void ImportTree(ref ProjectDbContext ctx)
        {
            bool bCruise = false;
            Stand currentStand = null;
            Plot currentPlot = null;
            string currentStandKey = string.Empty;
            string saveStandKey = string.Empty;
            string currentPlotKey = string.Empty;
            string savePlotKey = string.Empty;
            int count = 0;

            while (mdbReader.Read())
            {
                string sTract = mdbReader["Tract"].ToString();
                string sStand = mdbReader["Type"].ToString();
                string sTownship = mdbReader["Township"].ToString();
                string sRange = mdbReader["Range"].ToString();
                string sSection = mdbReader["Section"].ToString();
                string sPlot = mdbReader["PlotNo"].ToString();
                currentStandKey = string.Format("{0}{1}{2}{3}{4}", sTract, sStand, sTownship, sRange, sSection);
                if (saveStandKey != currentStandKey)
                {
                    currentStand = addStands.FirstOrDefault(p => p.TractName == sTract && p.StandName == sStand && p.Township == sTownship && p.Range == sRange && p.Section == sSection);
                    //currentStand = ProjectBLL.GetStand(pDataSource, sTract, sStand, sTownship, sRange, sSection);
                    //currentStand = ctx.Stands.FirstOrDefault(s => s.TractName == sTract && s.StandName == sStand && s.Township == sTownship && s.Range == sRange && s.Section == sSection);
                    saveStandKey = currentStandKey;
                    SplashScreenManager.Default.SetWaitFormDescription(string.Format("Importing Trees for Tract: {0} and Stand: {1}", sTract, sStand));
                }
                if (currentStand != null)
                {
                    Project.DAL.Project projectRec = ProjectBLL.GetProject(ref ctx);
                    Calc.SetCalcContextProjectStand(ref ctx, projectRec, ref currentStand);
                    
                    currentPlotKey = sPlot;
                    if (savePlotKey != currentPlotKey)
                    {
                        currentPlot = addPlots.FirstOrDefault(p => p.StandsId == currentStand.StandsId && p.UserPlotNumber == sPlot);
                        //currentPlot = ProjectBLL.GetPlotByStandIDPlot(ref ctx, currentStand.StandsId, sPlot);
                        savePlotKey = currentPlotKey;
                    }
                    if (currentPlot != null)
                    {
                        ++count;
                        bCruise = true;
                        Tree cs = new Tree();
                        cs.PlotId = currentPlot.PlotsId;
                        cs.StandsId = currentStand.StandsId;
                        cs.PlotNumber = mdbReader["PlotNo"].ToString().Trim();
                        cs.GroupPlot = mdbReader["PlotNo"].ToString().Trim();
                        cs.AgeCode = (byte)Utils.ConvertToInt(mdbReader["AgeCode"].ToString());
                        cs.BoleHeight = (short)Utils.ConvertToInt(mdbReader["BoleLength"].ToString());
                        cs.Dbh = Utils.ConvertToDouble(mdbReader["D4h"].ToString());
                        cs.FormPoint = (byte)Utils.ConvertToInt(mdbReader["FormPoint"].ToString());
                        cs.PlotFactorInput = mdbReader["PrsmFactor"].ToString().Trim();
                        cs.ReportedFormFactor = (byte)Utils.ConvertToInt(mdbReader["FormFactor"].ToString());
                        cs.SpeciesAbbreviation = mdbReader["Species"].ToString().Trim();
                        if (string.IsNullOrEmpty(mdbReader["TDF"].ToString().Trim()))
                            cs.TopDiameterFractionCode = string.Empty;
                        else
                        {
                            if (Utils.ConvertToInt(mdbReader["TDF"].ToString().Trim()) > 0)
                            {
                                if (mdbReader["TDF"].ToString().Trim().Length == 1)
                                    cs.TopDiameterFractionCode = mdbReader["TDF"].ToString().Trim() + "0";
                                else
                                    cs.TopDiameterFractionCode = mdbReader["TDF"].ToString().Trim();
                            }
                            else
                                cs.TopDiameterFractionCode = mdbReader["TDF"].ToString().Trim();
                        }
                        cs.TotalHeight = (short)Utils.ConvertToInt(mdbReader["Tot Height"].ToString());
                        if (string.IsNullOrEmpty(mdbReader["TreeCount"].ToString()))
                            cs.TreeCount = 1;
                        else
                            cs.TreeCount = (byte)Utils.ConvertToInt(mdbReader["TreeCount"].ToString());
                        cs.TreeNumber = (int)Utils.ConvertToInt(mdbReader["TreeNo"].ToString().Trim());
                        cs.GroupTree = (int)Utils.ConvertToInt(mdbReader["TreeNo"].ToString().Trim());
                        cs.TreeStatusDisplayCode = mdbReader["Status"].ToString().Trim();
                        //cs.MasterId = item.MasterId;
                        //cs.Plot = item.PlotNo;
                        cs.TreeType = mdbReader["RecordCode"].ToString();
                        cs.CrownPositionDisplayCode = mdbReader["T1"].ToString().Trim();
                        cs.CrownRatioDisplayCode = mdbReader["T2"].ToString().Trim();
                        cs.VigorDisplayCode = mdbReader["T3"].ToString().Trim();
                        cs.DamageDisplayCode = mdbReader["T4"].ToString().Trim();
                        cs.UserDefinedDisplayCode = mdbReader["T5"].ToString().Trim();
                        //if (!string.IsNullOrEmpty(mdbReader["ThinMonth"].ToString()))
                        //    cs.ThinMonth = Int16.Parse(mdbReader["ThinMonth"].ToString());
                        //if (!string.IsNullOrEmpty(mdbReader["ThinYear"].ToString()))
                        //    cs.ThinYear = Int16.Parse(mdbReader["ThinYear"].ToString());
                        if (!string.IsNullOrEmpty(mdbReader["Dbh"].ToString()))
                            cs.Dbh = Utils.ConvertToDouble(mdbReader["Dbh"].ToString());

                        //cs.StandTbl = mdbReader["StandTbl"].ToString();

                        Utils.DoCalcForTree(ref ctx, ref cs, ref currentPlot, ref currentStand, projectRec);

                        cs.Age = Utils.GetAge(ref ctx, currentStand, cs.AgeCode.ToString());

                        ProcessSegments(ref cs);

                        ////Calc.tree = cs;

                        ////Calc.CalcVolume(false);

                        //cs.TreesPerAcre = Calc.trees_pa;
                        //cs.BasalArea = Calc.basal_area;
                        //cs.LogsPerAcre = Calc.logs_pa;

                        //cs.TotalCcf = Calc.tree_ccf;
                        //cs.TotalMbf = Calc.tree_mbf;
                        //cs.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        //cs.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        //cs.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        //cs.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                        //ctx.Trees.Add(cs);
                        addTrees.Add(cs);
                        //ctx = TreeAddToContext(pDataSource, ctx, cs, count, 100, true);
                        for (int i = 0; i < 12; i++)
                        {
                            if (Calc.seg[i].len > 0) // was scribner_gross
                            {
                                Treesegment s = new Treesegment();

                                s.StandsId = cs.StandsId;
                                s.PlotId = cs.PlotId;
                                s.TreesId = cs.TreesId;
                                //s.Tree = cs;
                                s.GroupPlotNumber = cs.PlotNumber;
                                s.GroupTreeNumber = cs.TreeNumber;
                                s.SegmentNumber = i + 1;
                                s.TreeType = cs.TreeType;

                                s.CalcAccumulatedLength = Calc.seg[i].cum_len;
                                s.Comments = Calc.seg[i].comments;
                                s.LogValue = 0;
                                s.Species = cs.SpeciesAbbreviation;
                                s.TreeBasalArea = Calc.basal_area;
                                s.TreeTreesPerAcre = Calc.trees_per_acre;

                                s.CalcLen = Calc.seg[i].len;
                                s.CalcTopDia = (double)Math.Round((decimal)Calc.seg[i].tdia, 2);
                                s.CalcButtDia = (double)Math.Round((decimal)Calc.seg[i].bdia, 2);
                                s.CubicGrossVolume = Calc.seg[i].cubic_gross;
                                s.CubicNetVolume = Calc.seg[i].cubic_net;
                                s.ScribnerGrossVolume = Calc.seg[i].scribner_gross;
                                s.ScribnerNetVolume = Calc.seg[i].scribner_net;
                                s.CubicGrossVolumePerAce = Calc.seg[i].cubic_gross_pa;
                                s.CubicNetVolumePerAce = Calc.seg[i].cubic_net_pa;
                                s.ScribnerGrossVolumePerAce = Calc.seg[i].scribner_gross_pa;
                                s.ScribnerNetVolumePerAce = Calc.seg[i].scribner_net_pa;
                                s.Ccf = Calc.seg[i].ccf;
                                s.Mbf = Calc.seg[i].mbf;
                                s.SegmentNumber = i + 1;
                                s.TreeType = cs.TreeType;
                                s.Species = cs.SpeciesAbbreviation.Trim();
                                s.Age = cs.Age;
                                s.Ao = cs.Ao;
                                s.Bark = cs.Bark;
                                s.BoleHeight = cs.BoleHeight;
                                s.CalcTotalHtUsedYn = cs.CalcTotalHtUsedYn;
                                s.CrownPosition = cs.CrownPositionDisplayCode;
                                s.CrownRatio = cs.CrownRatioDisplayCode;
                                s.Damage = cs.DamageDisplayCode;
                                s.Dbh = cs.Dbh;
                                s.FormFactor = cs.ReportedFormFactor;
                                s.FormPoint = cs.FormPoint;
                                s.PlotFactorInput = cs.PlotFactorInput;
                                s.PlotNumber = cs.PlotNumber;
                                s.Tdf = cs.TopDiameterFractionCode;
                                s.TotalHeight = cs.TotalHeight;
                                s.TreeCount = cs.TreeCount;
                                s.TreeNumber = (short)cs.TreeNumber;
                                s.TreeStatusDisplayCode = cs.TreeStatusDisplayCode;
                                s.UserDefined = cs.UserDefinedDisplayCode;
                                s.Vigor = cs.VigorDisplayCode;
                                switch (i)
                                {
                                    case 0:
                                        if (cs.BdFtLd1 != null)
                                            s.BdFtLd = cs.BdFtLd1;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd1 != null)
                                            s.BdFtDd = cs.BdFtDd1;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd1 != null)
                                            s.BdFtPd = cs.BdFtPd1;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd1 != null)
                                            s.CuFtDd = cs.CuFtDd1;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd1 != null)
                                            s.CuFtLd = cs.CuFtLd1;
                                        else
                                            s.CuFtLd = cs.CuFtLd1;
                                        if (cs.Length1 != null)
                                            s.Length = cs.Length1.Trim();
                                        else
                                            s.Length = cs.Length1.Trim();
                                        if (cs.Sort1 != null)
                                            s.SortCode = cs.Sort1.Trim();
                                        else
                                            s.SortCode = cs.Sort1.Trim();
                                        if (cs.Grade1 != null)
                                            s.GradeCode = cs.Grade1.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 1:
                                        if (cs.BdFtLd2 != null)
                                            s.BdFtLd = cs.BdFtLd2;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd2 != null)
                                            s.BdFtDd = cs.BdFtDd2;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd2 != null)
                                            s.BdFtPd = cs.BdFtPd2;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd2 != null)
                                            s.CuFtDd = cs.CuFtDd2;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd2 != null)
                                            s.CuFtLd = cs.CuFtLd2;
                                        else
                                            s.CuFtLd = 01;
                                        if (cs.Length2 != null)
                                            s.Length = cs.Length2.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort2 != null)
                                            s.SortCode = cs.Sort2.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade2 != null)
                                            s.GradeCode = cs.Grade2.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 2:
                                        if (cs.BdFtLd3 != null)
                                            s.BdFtLd = cs.BdFtLd3;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd3 != null)
                                            s.BdFtDd = cs.BdFtDd3;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd3 != null)
                                            s.BdFtPd = cs.BdFtPd3;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd3 != null)
                                            s.CuFtDd = cs.CuFtDd3;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd3 != null)
                                            s.CuFtLd = cs.CuFtLd3;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length3 != null)
                                            s.Length = cs.Length3.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort3 != null)
                                            s.SortCode = cs.Sort3.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade3 != null)
                                            s.GradeCode = cs.Grade3.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 3:
                                        if (cs.BdFtLd4 != null)
                                            s.BdFtLd = cs.BdFtLd4;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd4 != null)
                                            s.BdFtDd = cs.BdFtDd4;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd4 != null)
                                            s.BdFtPd = cs.BdFtPd4;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd4 != null)
                                            s.CuFtDd = cs.CuFtDd4;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd4 != null)
                                            s.CuFtLd = cs.CuFtLd4;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length4 != null)
                                            s.Length = cs.Length4.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort4 != null)
                                            s.SortCode = cs.Sort4.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade4 != null)
                                            s.GradeCode = cs.Grade4.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 4:
                                        if (cs.BdFtLd5 != null)
                                            s.BdFtLd = cs.BdFtLd5;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd5 != null)
                                            s.BdFtDd = cs.BdFtDd5;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd5 != null)
                                            s.BdFtPd = cs.BdFtPd5;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd5 != null)
                                            s.CuFtDd = cs.CuFtDd5;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd5 != null)
                                            s.CuFtLd = cs.CuFtLd5;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length5 != null)
                                            s.Length = cs.Length5.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort5 != null)
                                            s.SortCode = cs.Sort5.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade5 != null)
                                            s.GradeCode = cs.Grade5.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 5:
                                        if (cs.BdFtLd6 != null)
                                            s.BdFtLd = cs.BdFtLd6;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd6 != null)
                                            s.BdFtDd = cs.BdFtDd6;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd6 != null)
                                            s.BdFtPd = cs.BdFtPd6;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd6 != null)
                                            s.CuFtDd = cs.CuFtDd6;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd6 != null)
                                            s.CuFtLd = cs.CuFtLd6;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length6 != null)
                                            s.Length = cs.Length6.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort6 != null)
                                            s.SortCode = cs.Sort6.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade6 != null)
                                            s.GradeCode = cs.Grade6.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 6:
                                        if (cs.BdFtLd7 != null)
                                            s.BdFtLd = cs.BdFtLd7;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd7 != null)
                                            s.BdFtDd = cs.BdFtDd7;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd7 != null)
                                            s.BdFtPd = cs.BdFtPd7;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd7 != null)
                                            s.CuFtDd = cs.CuFtDd7;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd7 != null)
                                            s.CuFtLd = cs.CuFtLd7;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length7 != null)
                                            s.Length = cs.Length7.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort7 != null)
                                            s.SortCode = cs.Sort7.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade7 != null)
                                            s.GradeCode = cs.Grade7.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 7:
                                        if (cs.BdFtLd8 != null)
                                            s.BdFtLd = cs.BdFtLd8;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd8 != null)
                                            s.BdFtDd = cs.BdFtDd8;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd8 != null)
                                            s.BdFtPd = cs.BdFtPd8;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd8 != null)
                                            s.CuFtDd = cs.CuFtDd8;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd8 != null)
                                            s.CuFtLd = cs.CuFtLd8;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length8 != null)
                                            s.Length = cs.Length8.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort8 != null)
                                            s.SortCode = cs.Sort8.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade8 != null)
                                            s.GradeCode = cs.Grade8.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 8:
                                        if (cs.BdFtLd9 != null)
                                            s.BdFtLd = cs.BdFtLd9;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd9 != null)
                                            s.BdFtDd = cs.BdFtDd9;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd9 != null)
                                            s.BdFtPd = cs.BdFtPd9;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd9 != null)
                                            s.CuFtDd = cs.CuFtDd9;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd9 != null)
                                            s.CuFtLd = cs.CuFtLd9;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length9 != null)
                                            s.Length = cs.Length9.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort9 != null)
                                            s.SortCode = cs.Sort9.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade9 != null)
                                            s.GradeCode = cs.Grade9.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 9:
                                        if (cs.BdFtLd10 != null)
                                            s.BdFtLd = cs.BdFtLd10;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd10 != null)
                                            s.BdFtDd = cs.BdFtDd10;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd10 != null)
                                            s.BdFtPd = cs.BdFtPd10;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd10 != null)
                                            s.CuFtDd = cs.CuFtDd10;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd10 != null)
                                            s.CuFtLd = cs.CuFtLd10;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length10 != null)
                                            s.Length = cs.Length10.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort10 != null)
                                            s.SortCode = cs.Sort10.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade10 != null)
                                            s.GradeCode = cs.Grade10.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 10:
                                        if (cs.BdFtLd11 != null)
                                            s.BdFtLd = cs.BdFtLd11;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd11 != null)
                                            s.BdFtDd = cs.BdFtDd11;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd11 != null)
                                            s.BdFtPd = cs.BdFtPd11;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd11 != null)
                                            s.CuFtDd = cs.CuFtDd11;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd11 != null)
                                            s.CuFtLd = cs.CuFtLd11;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length11 != null)
                                            s.Length = cs.Length11.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort11 != null)
                                            s.SortCode = cs.Sort11.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade11 != null)
                                            s.GradeCode = cs.Grade11.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 11:
                                        if (cs.BdFtLd12 != null)
                                            s.BdFtLd = cs.BdFtLd12;
                                        else
                                            s.BdFtLd = 0;
                                        if (cs.BdFtDd12 != null)
                                            s.BdFtDd = cs.BdFtDd12;
                                        else
                                            s.BdFtDd = 0;
                                        if (cs.BdFtPd12 != null)
                                            s.BdFtPd = cs.BdFtPd12;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (cs.CuFtDd12 != null)
                                            s.CuFtDd = cs.CuFtDd12;
                                        else
                                            s.CuFtDd = 0;
                                        if (cs.CuFtLd12 != null)
                                            s.CuFtLd = cs.CuFtLd12;
                                        else
                                            s.CuFtLd = 0;
                                        if (cs.Length12 != null)
                                            s.Length = cs.Length12.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (cs.Sort12 != null)
                                            s.SortCode = cs.Sort12.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (cs.Grade12 != null)
                                            s.GradeCode = cs.Grade12.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                }
                                if (s.SortCode == "0" || s.GradeCode == "0")
                                    s.Comments = "Cull Segment";
                                addSegments.Add(s);
                                //ctx.Treesegments.Add(s);
                                //ctx = SegmentAddToContext(pDataSource, ctx, s, count, 100, true);
                            }
                        }
                    }
                }
                //if (bCruise)
                //{
                //    mRec[0].CruiseFlag = "Y";
                //    mRec.Save();
                //}
            }
            ctx.Trees.AddRange(addTrees);
            ctx.Treesegments.AddRange(addSegments);
            ctx.SaveChanges();
            //ctx.Dispose();
        }

        private ProjectDbContext TreeAddToContext(string pDataSource, ProjectDbContext ctx, Tree cs, int count, int commitCount, bool p)
        {
            ctx.Trees.Add(cs);
            if (count % commitCount == 0)
            {
                ctx.SaveChanges();
                if (p)
                {
                    ctx.Dispose();
                    ctx = new ProjectDbContext("Data Source = " + pDataSource);
                    ctx.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return ctx;
        }

        private ProjectDbContext SegmentAddToContext(string pDataSource, ProjectDbContext ctx, Treesegment segs, int count, int commitCount, bool p)
        {
            ctx.Treesegments.Add(segs);
            if (count % commitCount == 0)
            {
                ctx.SaveChanges();
                if (p)
                {
                    ctx.Dispose();
                    ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
                    ctx.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return ctx;
        }

        private long MakeLong(float p)
        {
            long lTmp;

            lTmp = (long)p;
            return lTmp;
        }

        private int segment = 0;
        private void ProcessSegments(ref Tree pCru)
        {
            segment = 0;
            while (mdbReader["Continuation"].ToString() != "9")
            {
                ProcessContinuation(ref pCru);
                mdbReader.Read();
            }
            if (!string.IsNullOrEmpty(mdbReader["Length1"].ToString()))
                InsertSegment(segment + 1, ref pCru);
            else
                InitSegment(segment + 1, ref pCru);
            if (!string.IsNullOrEmpty(mdbReader["Length2"].ToString()))
                InsertSegment(segment + 2, ref pCru);
            else
                InitSegment(segment + 2, ref pCru);
            if (!string.IsNullOrEmpty(mdbReader["Length3"].ToString()))
                InsertSegment(segment + 3, ref pCru);
            else
                InitSegment(segment + 3, ref pCru);
        }

        private void ProcessContinuation(ref Tree pCru)
        {
            if (mdbReader["Continuation"].ToString() == "1")
            {
                if (!string.IsNullOrEmpty(mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(1, ref pCru);
                else
                    InitSegment(1, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(2, ref pCru);
                else
                    InitSegment(2, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(3, ref pCru);
                else
                    InitSegment(3, ref pCru);
                segment = 3;
            }
            if (mdbReader["Continuation"].ToString() == "2")
            {
                if (!string.IsNullOrEmpty(mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(4, ref pCru);
                else
                    InitSegment(4, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(5, ref pCru);
                else
                    InitSegment(5, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(6, ref pCru);
                else
                    InitSegment(6, ref pCru);
                segment = 6;
            }
            if (mdbReader["Continuation"].ToString() == "3")
            {
                if (!string.IsNullOrEmpty(mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(7, ref pCru);
                else
                    InitSegment(7, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(8, ref pCru);
                else
                    InitSegment(8, ref pCru);
                if (!string.IsNullOrEmpty(mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(9, ref pCru);
                else
                    InitSegment(9, ref pCru);
                segment = 9;
            }
        }

        private void InsertSegment(int p, ref Tree pCru)
        {
            switch (p)
            {
                case 1:
                    pCru.Sort1 = mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade1 = mdbReader["Grade1"].ToString().Trim();
                    pCru.Length1 = mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd1 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd1 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd1 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd1 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd1 = mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 2:
                    pCru.Sort2 = mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade2 = mdbReader["Grade2"].ToString().Trim();
                    pCru.Length2 = mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd2 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd2 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd2 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd2 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd2 = mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 3:
                    pCru.Sort3 = mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade3 = mdbReader["Grade3"].ToString().Trim();
                    pCru.Length3 = mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd3 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd3 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd3 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd3 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd3 = mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 4:
                    pCru.Sort4 = mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade4 = mdbReader["Grade1"].ToString().Trim();
                    pCru.Length4 = mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd4 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd4 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd4 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd4 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd4 = mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 5:
                    pCru.Sort5 = mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade5 = mdbReader["Grade2"].ToString().Trim();
                    pCru.Length5 = mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd5 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd5 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd5 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd5 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd5 = mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 6:
                    pCru.Sort6 = mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade6 = mdbReader["Grade3"].ToString().Trim();
                    pCru.Length6 = mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd6 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd6 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd6 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd6 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd6 = mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 7:
                    pCru.Sort7 = mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade7 = mdbReader["Grade1"].ToString().Trim();
                    pCru.Length7 = mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd7 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd7 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd7 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd7 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd7 = mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 8:
                    pCru.Sort8 = mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade8 = mdbReader["Grade2"].ToString().Trim();
                    pCru.Length8 = mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd8 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd8 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd8 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd8 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd8 = mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 9:
                    pCru.Sort9 = mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade9 = mdbReader["Grade3"].ToString().Trim();
                    pCru.Length9 = mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd9 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd9 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd9 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd9 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd9 = mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 10:
                    pCru.Sort10 = mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade10 = mdbReader["Grade1"].ToString().Trim();
                    pCru.Length10 = mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd10 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd10 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd10 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd10 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd10 = mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 11:
                    pCru.Sort11 = mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade11 = mdbReader["Grade2"].ToString().Trim();
                    pCru.Length11 = mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd11 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd11 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd11 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd11 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd11 = mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 12:
                    pCru.Sort12 = mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade12 = mdbReader["Grade3"].ToString().Trim();
                    pCru.Length12 = mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd12 = (byte)Utils.ConvertToInt(mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd12 = (byte)Utils.ConvertToInt(mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd12 = (byte)Utils.ConvertToInt(mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd12 = (byte)Utils.ConvertToInt(mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd12 = mdbReader["PctDed3"].ToString().Trim();
                    break;
            }
        }

        private void InitSegment(int p, ref Tree pCru)
        {
            switch (p)
            {
                case 1:
                    pCru.Sort1 = string.Empty;
                    pCru.Grade1 = string.Empty;
                    pCru.Length1 = string.Empty;
                    pCru.BdFtLd1 = 0;
                    pCru.BdFtDd1 = 0;
                    pCru.CuFtLd1 = 0;
                    pCru.CuFtDd1 = 0;
                    pCru.BdFtPd1 = string.Empty;
                    break;
                case 2:
                    pCru.Sort2 = string.Empty;
                    pCru.Grade2 = string.Empty;
                    pCru.Length2 = string.Empty;
                    pCru.BdFtLd2 = 0;
                    pCru.BdFtDd2 = 0;
                    pCru.CuFtLd2 = 0;
                    pCru.CuFtDd2 = 0;
                    pCru.BdFtPd2 = string.Empty;
                    break;
                case 3:
                    pCru.Sort3 = string.Empty;
                    pCru.Grade3 = string.Empty;
                    pCru.Length3 = string.Empty;
                    pCru.BdFtLd3 = 0;
                    pCru.BdFtDd3 = 0;
                    pCru.CuFtLd3 = 0;
                    pCru.CuFtDd3 = 0;
                    pCru.BdFtPd3 = string.Empty;
                    break;
                case 4:
                    pCru.Sort4 = string.Empty;
                    pCru.Grade4 = string.Empty;
                    pCru.Length4 = string.Empty;
                    pCru.BdFtLd4 = 0;
                    pCru.BdFtDd4 = 0;
                    pCru.CuFtLd4 = 0;
                    pCru.CuFtDd4 = 0;
                    pCru.BdFtPd4 = string.Empty;
                    break;
                case 5:
                    pCru.Sort5 = string.Empty;
                    pCru.Grade5 = string.Empty;
                    pCru.Length5 = string.Empty;
                    pCru.BdFtLd5 = 0;
                    pCru.BdFtDd5 = 0;
                    pCru.CuFtLd5 = 0;
                    pCru.CuFtDd5 = 0;
                    pCru.BdFtPd5 = string.Empty;
                    break;
                case 6:
                    pCru.Sort6 = string.Empty;
                    pCru.Grade6 = string.Empty;
                    pCru.Length6 = string.Empty;
                    pCru.BdFtLd6 = 0;
                    pCru.BdFtDd6 = 0;
                    pCru.CuFtLd6 = 0;
                    pCru.CuFtDd6 = 0;
                    pCru.BdFtPd6 = string.Empty;
                    break;
                case 7:
                    pCru.Sort7 = string.Empty;
                    pCru.Grade7 = string.Empty;
                    pCru.Length7 = string.Empty;
                    pCru.BdFtLd7 = 0;
                    pCru.BdFtDd7 = 0;
                    pCru.CuFtLd7 = 0;
                    pCru.CuFtDd7 = 0;
                    pCru.BdFtPd7 = string.Empty;
                    break;
                case 8:
                    pCru.Sort8 = string.Empty;
                    pCru.Grade8 = string.Empty;
                    pCru.Length8 = string.Empty;
                    pCru.BdFtLd8 = 0;
                    pCru.BdFtDd8 = 0;
                    pCru.CuFtLd8 = 0;
                    pCru.CuFtDd8 = 0;
                    pCru.BdFtPd8 = string.Empty;
                    break;
                case 9:
                    pCru.Sort9 = string.Empty;
                    pCru.Grade9 = string.Empty;
                    pCru.Length9 = string.Empty;
                    pCru.BdFtLd9 = 0;
                    pCru.BdFtDd9 = 0;
                    pCru.CuFtLd9 = 0;
                    pCru.CuFtDd9 = 0;
                    pCru.BdFtPd9 = string.Empty;
                    break;
                case 10:
                    pCru.Sort10 = string.Empty;
                    pCru.Grade10 = string.Empty;
                    pCru.Length10 = string.Empty;
                    pCru.BdFtLd10 = 0;
                    pCru.BdFtDd10 = 0;
                    pCru.CuFtLd10 = 0;
                    pCru.CuFtDd10 = 0;
                    pCru.BdFtPd10 = string.Empty;
                    break;
                case 11:
                    pCru.Sort11 = string.Empty;
                    pCru.Grade11 = string.Empty;
                    pCru.Length11 = string.Empty;
                    pCru.BdFtLd11 = 0;
                    pCru.BdFtDd11 = 0;
                    pCru.CuFtLd11 = 0;
                    pCru.CuFtDd11 = 0;
                    pCru.BdFtPd11 = string.Empty;
                    break;
                case 12:
                    pCru.Sort12 = string.Empty;
                    pCru.Grade12 = string.Empty;
                    pCru.Length12 = string.Empty;
                    pCru.BdFtLd12 = 0;
                    pCru.BdFtDd12 = 0;
                    pCru.CuFtLd12 = 0;
                    pCru.CuFtDd12 = 0;
                    pCru.BdFtPd12 = string.Empty;
                    break;
            }
        }

        private void ImportCruiseSegments()
        {
            /*
            ProjectCollection coll = new ProjectCollection();
            coll.LoadAll();

            while (mdbReader.Read())
            {
                Project.DAL.Project cs = coll.AddNew();

                cs.Project = mdbReader["Project"].ToString();
                if (!string.IsNullOrEmpty(mdbReader["Customer Name"].ToString()))
                    cs.CustomerName = mdbReader["Customer Name"].ToString();
                else
                    cs.CustomerName = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Location"].ToString()))
                    cs.Location = mdbReader["Location"].ToString();
                else
                    cs.Location = string.Empty;
                cs.MapData = string.Empty;
                cs.MapProject = string.Empty;
                cs.Meridian = string.Empty;
            }
            coll.Save();
             */
        }

        private void DeleteTable(ref ProjectDbContext projectContext, string pDb, string pTable, string pFieldName, string pNewTable, string pNewFieldName)
        {
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            mdbConn = new OleDbConnection(connectionString);

            mdbConn.Open();

            string sql = string.Empty;
            sql = string.Format("SELECT [{0}] FROM {1} GROUP BY [{2}]", pFieldName, pTable, pFieldName);
            mdbCmd = new OleDbCommand(sql, mdbConn);
            mdbReader = mdbCmd.ExecuteReader();

            while (mdbReader.Read())
            {
                SqlCommand iCmd = new SqlCommand(projectContext.Database.Connection.ConnectionString);
                iCmd.CommandText = string.Format("DELETE FROM {0} WHERE {1} = '{2}'", pNewTable, pNewFieldName, mdbReader[pFieldName].ToString());
                int iRet = iCmd.ExecuteNonQuery();
            }
        }

        //private void DeleteTable(ref ProjectDbContext projectContext, string pDb, string pTable, string pFieldName, string pNewTable, string pNewFieldName, string pWhereFieldName, string pWhereFieldValue)
        //{
        //    string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
        //    mdbConn = new OleDbConnection(connectionString);

        //    mdbConn.Open();

        //    string sql = string.Empty;
        //    sql = string.Format("SELECT [{0}] FROM {1} GROUP BY [{2}]", pFieldName, pTable, pFieldName);
        //    mdbCmd = new OleDbCommand(sql, mdbConn);
        //    mdbReader = mdbCmd.ExecuteReader();

        //    while (mdbReader.Read())
        //    {
        //        SqlCeCommand iCmd = new SqlCeCommand(projectContext.Connection.ConnectionString);
        //        iCmd.CommandText = string.Format("DELETE FROM {0} WHERE {1} = '{2}' AND {3} = '{4}'", pNewTable, pNewFieldName, mdbReader[pFieldName].ToString(), pWhereFieldName, pWhereFieldValue);
        //        int iRet = iCmd.ExecuteNonQuery();
        //    }
        //}

        public bool ProcessSpecies(ref ProjectDbContext projectContext, string pDb, string pTable)
        {
            DeleteTable(ref projectContext, pDb, pTable, "Table Name", "Species", "TableName");
            OpenFile(pDb, pTable, "", "", "");
            ImportSpecies(ref projectContext);
            CloseFile();
            return true;
        }

        private void ImportSpecies(ref ProjectDbContext projectContext)
        {
            //while (mdbReader.Read())
            //{
            //    Species cs = new Species();

            //    cs.TableName = mdbReader["Table Name"].ToString().Trim();
            //    cs.InputCode = Int16.Parse(mdbReader["Code"].ToString().Trim());
            //    cs.Abbreviation = mdbReader["Abrv"].ToString().Trim();
            //    if (!string.IsNullOrEmpty(mdbReader["Description"].ToString().Trim()))
            //        cs.Description = mdbReader["Description"].ToString().Trim();
            //    else
            //        cs.Description = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Bark"].ToString().Trim()))
            //        cs.BarkRatio = Utils.ConvertToDouble(mdbReader["Bark"].ToString().Trim());
            //    else
            //        cs.BarkRatio = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["ASubo"].ToString()))
            //        cs.AsubODisplayCode = mdbReader["ASubo"].ToString();
            //    else
            //        cs.AsubODisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Form Factor"].ToString()))
            //        cs.FormFactorsDisplayCode = mdbReader["Form Factor"].ToString();
            //    else
            //        cs.FormFactorsDisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Wood Type"].ToString()))
            //        cs.WoodTypeDisplayCode = mdbReader["Wood Type"].ToString();
            //    else
            //        cs.WoodTypeDisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Component"].ToString()))
            //        cs.ComponentsDisplayCode = mdbReader["Component"].ToString();
            //    else
            //        cs.ComponentsDisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["YldTable"].ToString().Trim()))
            //        cs.YieldTableName = mdbReader["YldTable"].ToString().Trim();
            //    else
            //        cs.YieldTableName = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Dia"].ToString()))
            //        cs.MinDia = Single.Parse(mdbReader["Min Dia"].ToString());
            //    else
            //        cs.MinDia = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Len"].ToString()))
            //        cs.MinLen = Single.Parse(mdbReader["Min Len"].ToString());
            //    else
            //        cs.MinLen = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Max Len"].ToString()))
            //        cs.MaxLen = Single.Parse(mdbReader["Max Len"].ToString());
            //    else
            //        cs.MaxLen = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["MaxDia"].ToString()))
            //        cs.MaxDia = Single.Parse(mdbReader["MaxDia"].ToString());
            //    else
            //        cs.MaxDia = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["MaxHeight"].ToString()))
            //        cs.MaxHeight = (short)Utils.ConvertToInt(mdbReader["MaxHeight"].ToString());
            //    else
            //        cs.MaxHeight = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Trim"].ToString()))
            //        cs.Trim = Single.Parse(mdbReader["Trim"].ToString());
            //    else
            //        cs.Trim = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["BdFt Rule"].ToString()))
            //        cs.BdFtDisplayCode = mdbReader["BdFt Rule"].ToString();
            //    else
            //        cs.BdFtDisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["CuFt Rule"].ToString()))
            //        cs.CuFtDisplayCode = mdbReader["CuFt Rule"].ToString();
            //    else
            //        cs.CuFtDisplayCode = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Lbs"].ToString()))
            //        cs.Lbs = Int32.Parse(mdbReader["Lbs"].ToString());
            //    else
            //        cs.Lbs = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Lbs Type"].ToString()))
            //        cs.LbsType = mdbReader["Lbs Type"].ToString();
            //    else
            //        cs.LbsType = string.Empty;
            //    cs.BarkFactorsDisplayCode = string.Empty;
            //    cs.BioMass = 0;
            //    cs.CommonName = string.Empty;
            //    cs.GrowthModelTableName = string.Empty;
            //    cs.PercentCarbon = 0;
            //    cs.PercentDryWeight = 0;
            //    cs.SpeciesGroup = cs.Abbreviation;
            //    //if (!string.IsNullOrEmpty(mdbReader["Weight"].ToString()))
            //    //    cs.Weight = mdbReader["Weight"].ToString();
            //    //else
            //    //    cs.Weight = string.Empty;
            //    //if (!string.IsNullOrEmpty(mdbReader["Weight Type"].ToString()))
            //    //    cs.WeightType = mdbReader["Weight Type"].ToString();
            //    //else
            //    //    cs.WeightType = string.Empty;
            //    projectContext.Species.Add(cs);
            //}
            //projectContext.SaveChanges();
        }

        public bool ProcessGrade(ref ProjectDbContext projectContext, string pDb, string pTable)
        {
            DeleteTable(ref projectContext, pDb, pTable, "Table Name", "Grade", "TableName");
            OpenFile(pDb, pTable, "", "Record Code", "G");
            ImportGrade(ref projectContext);
            CloseFile();
            return true;
        }

        private void ImportGrade(ref ProjectDbContext projectContext)
        {
            //while (mdbReader.Read())
            //{
            //    Grade cs = new Grade();

            //    cs.TableName = mdbReader["Table Name"].ToString().Trim();
            //    cs.InputCode = mdbReader["Grade"].ToString().Trim();
            //    cs.SpeciesAbbreviation = mdbReader["Abrv"].ToString().Trim();
            //    if (!string.IsNullOrEmpty(mdbReader["Description"].ToString().Trim()))
            //        cs.Description = mdbReader["Description"].ToString().Trim();
            //    else
            //        cs.Description = string.Empty;
            //    //if (!string.IsNullOrEmpty(mdbReader["Fiber"].ToString()))
            //    //    cs.Fiber = mdbReader["Fiber"].ToString();
            //    //else
            //    //    cs.Fiber = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Dia"].ToString()))
            //        cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Min Dia"].ToString());
            //    else
            //        cs.MaximumDiameter = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Max Dia"].ToString()))
            //        cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Max Dia"].ToString());
            //    else
            //        cs.MaximumDiameter = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Max Butt"].ToString()))
            //        cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Max Butt"].ToString());
            //    else
            //        cs.MaximumDiameter = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Len"].ToString()))
            //        cs.MinimumLength = Utils.ConvertToInt(mdbReader["Min Len"].ToString());
            //    else
            //        cs.MinimumLength = 0;
            //    //if (!string.IsNullOrEmpty(mdbReader["Max Len"].ToString()))
            //    //    cs.MaxLen = Single.Parse(mdbReader["Max Len"].ToString());
            //    //else
            //    //    cs.MaxLen = 0;
            //    //if (!string.IsNullOrEmpty(mdbReader["Defect"].ToString()))
            //    //    cs.Defect = Int16.Parse(mdbReader["Defect"].ToString());
            //    //else
            //    //    cs.Defect = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Vol"].ToString()))
            //        cs.MinimumBDFTPerLog = Utils.ConvertToInt(mdbReader["Min Vol"].ToString());
            //    else
            //        cs.MinimumBDFTPerLog = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Vol Type"].ToString().Trim()))
            //        cs.VolumeUnits = mdbReader["Vol Type"].ToString().Trim();
            //    else
            //        cs.VolumeUnits = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Min Rings"].ToString()))
            //        cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(mdbReader["Min Rings"].ToString());
            //    else
            //        cs.MinimumRingsPerInch = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Knot Size"].ToString()))
            //        cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(mdbReader["Knot Size"].ToString());
            //    else
            //        cs.MinimumRingsPerInch = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Knot Freq"].ToString()))
            //        cs.KnotsPerFoot = (byte)Utils.ConvertToInt(mdbReader["Knot Freq"].ToString());
            //    else
            //        cs.KnotsPerFoot = 0;
            //    //if (!string.IsNullOrEmpty(mdbReader["Straight"].ToString()))
            //    //    cs.Straight = mdbReader["Straight"].ToString();
            //    //else
            //    //    cs.Straight = string.Empty;
            //    //if (!string.IsNullOrEmpty(mdbReader["Sap"].ToString()))
            //    //    cs.Sap = mdbReader["Sap"].ToString();
            //    //else
            //    //    cs.Sap = string.Empty;
            //    if (!string.IsNullOrEmpty(mdbReader["Lbs"].ToString()))
            //        cs.WeightInPounds = Utils.ConvertToInt(mdbReader["Lbs"].ToString());
            //    else
            //        cs.WeightInPounds = 0;
            //    if (!string.IsNullOrEmpty(mdbReader["Lbs Type"].ToString().Trim()))
            //        cs.VolumeUnits = mdbReader["Lbs Type"].ToString().Trim();
            //    else
            //        cs.VolumeUnits = string.Empty;
            //    //if (!string.IsNullOrEmpty(mdbReader["Cords"].ToString()))
            //    //    cs.Cords = Single.Parse(mdbReader["Cords"].ToString());
            //    //else
            //    //    cs.Cords = 0;
            //    //if (!string.IsNullOrEmpty(mdbReader["Cords Type"].ToString()))
            //    //    cs.CordsType = mdbReader["Cords Type"].ToString();
            //    //else
            //    //    cs.CordsType = string.Empty;
            //    //if (!string.IsNullOrEmpty(mdbReader["Product"].ToString()))
            //    //    cs.Product = mdbReader["Product"].ToString();
            //    //else
            //    //    cs.Product = string.Empty;
            //    projectContext.Grade.Add(cs);
            //}
            //projectContext.SaveChanges();
        }

        public bool ProcessSort(ref ProjectDbContext projectContext, string pDb, string pTable)
        {
            DeleteTable(ref projectContext, pDb, pTable, "Table Name", "Sort", "TableName");
            OpenFile(pDb, pTable, "", "Record Code", "O");
            ImportSort(ref projectContext);
            CloseFile();
            return true;
        }

        private void ImportSort(ref ProjectDbContext projectContext)
        {
        //    while (mdbReader.Read())
        //    {
        //        Sort cs = new Sort();

        //        cs.TableName = mdbReader["Table Name"].ToString().Trim();
        //        cs.InputCode = mdbReader["Sort"].ToString().Trim();
        //        cs.SpeciesAbbreviation = mdbReader["Abrv"].ToString().Trim();
        //        if (!string.IsNullOrEmpty(mdbReader["Description"].ToString().Trim()))
        //            cs.Description = mdbReader["Description"].ToString().Trim();
        //        else
        //            cs.Description = string.Empty;
        //        //if (!string.IsNullOrEmpty(mdbReader["Fiber"].ToString()))
        //        //    cs.Fiber = mdbReader["Fiber"].ToString();
        //        //else
        //        //    cs.Fiber = string.Empty;
        //        if (!string.IsNullOrEmpty(mdbReader["Min Dia"].ToString()))
        //            cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Min Dia"].ToString());
        //        else
        //            cs.MaximumDiameter = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Max Dia"].ToString()))
        //            cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Max Dia"].ToString());
        //        else
        //            cs.MaximumDiameter = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Max Butt"].ToString()))
        //            cs.MaximumDiameter = Utils.ConvertToInt(mdbReader["Max Butt"].ToString());
        //        else
        //            cs.MaximumDiameter = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Min Len"].ToString()))
        //            cs.MinimumLength = Utils.ConvertToInt(mdbReader["Min Len"].ToString());
        //        else
        //            cs.MinimumLength = 0;
        //        //if (!string.IsNullOrEmpty(mdbReader["Max Len"].ToString()))
        //        //    cs.MaxLen = Single.Parse(mdbReader["Max Len"].ToString());
        //        //else
        //        //    cs.MaxLen = 0;
        //        //if (!string.IsNullOrEmpty(mdbReader["Defect"].ToString()))
        //        //    cs.Defect = Int16.Parse(mdbReader["Defect"].ToString());
        //        //else
        //        //    cs.Defect = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Min Vol"].ToString()))
        //            cs.MinimumBDFTPerLog = Utils.ConvertToInt(mdbReader["Min Vol"].ToString());
        //        else
        //            cs.MinimumBDFTPerLog = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Vol Type"].ToString().Trim()))
        //            cs.VolumeUnits = mdbReader["Vol Type"].ToString().Trim();
        //        else
        //            cs.VolumeUnits = string.Empty;
        //        if (!string.IsNullOrEmpty(mdbReader["Min Rings"].ToString()))
        //            cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(mdbReader["Min Rings"].ToString());
        //        else
        //            cs.MinimumRingsPerInch = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Knot Size"].ToString()))
        //            cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(mdbReader["Knot Size"].ToString());
        //        else
        //            cs.MinimumRingsPerInch = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Knot Freq"].ToString()))
        //            cs.KnotsPerFoot = (byte)Utils.ConvertToInt(mdbReader["Knot Freq"].ToString());
        //        else
        //            cs.KnotsPerFoot = 0;
        //        //if (!string.IsNullOrEmpty(mdbReader["Straight"].ToString()))
        //        //    cs.Straight = mdbReader["Straight"].ToString();
        //        //else
        //        //    cs.Straight = string.Empty;
        //        //if (!string.IsNullOrEmpty(mdbReader["Sap"].ToString()))
        //        //    cs.Sap = mdbReader["Sap"].ToString();
        //        //else
        //        //    cs.Sap = string.Empty;
        //        if (!string.IsNullOrEmpty(mdbReader["Lbs"].ToString()))
        //            cs.WeightInPounds = Utils.ConvertToInt(mdbReader["Lbs"].ToString());
        //        else
        //            cs.WeightInPounds = 0;
        //        if (!string.IsNullOrEmpty(mdbReader["Lbs Type"].ToString().Trim()))
        //            cs.VolumeUnits = mdbReader["Lbs Type"].ToString().Trim();
        //        else
        //            cs.VolumeUnits = string.Empty;
        //        //if (!string.IsNullOrEmpty(mdbReader["Cords"].ToString()))
        //        //    cs.Cords = Single.Parse(mdbReader["Cords"].ToString());
        //        //else
        //        //    cs.Cords = 0;
        //        //if (!string.IsNullOrEmpty(mdbReader["Cords Type"].ToString()))
        //        //    cs.CordsType = mdbReader["Cords Type"].ToString();
        //        //else
        //        //    cs.CordsType = string.Empty;
        //        //if (!string.IsNullOrEmpty(mdbReader["Product"].ToString()))
        //        //    cs.Product = mdbReader["Product"].ToString();
        //        //else
        //        //    cs.Product = string.Empty;
        //        projectContext.Sort.Add(cs);
        //    }
        //    projectContext.SaveChanges();
        }
    }
}
