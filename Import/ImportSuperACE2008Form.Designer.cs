﻿namespace Import
{
    partial class ImportSuperACE2008Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.cbProject = new DevExpress.XtraEditors.CheckEdit();
            this.btnProjectDb = new DevExpress.XtraEditors.SimpleButton();
            this.tbProjectDb = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.tbNewProjectName = new DevExpress.XtraEditors.TextEdit();
            this.btnNewProjectLocation = new DevExpress.XtraEditors.SimpleButton();
            this.tbNewProjectLocation = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btnProcess = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Import.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProjectDb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewProjectLocation.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.cbProject);
            this.groupControl1.Controls.Add(this.btnProjectDb);
            this.groupControl1.Controls.Add(this.tbProjectDb);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(554, 107);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Location of SuperACE 2008 File";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(338, 73);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Include Table Defaults";
            this.checkEdit2.Size = new System.Drawing.Size(142, 19);
            this.checkEdit2.TabIndex = 11;
            // 
            // cbProject
            // 
            this.cbProject.Location = new System.Drawing.Point(104, 73);
            this.cbProject.Name = "cbProject";
            this.cbProject.Properties.Caption = "Include Tables For Project";
            this.cbProject.Size = new System.Drawing.Size(157, 19);
            this.cbProject.TabIndex = 10;
            // 
            // btnProjectDb
            // 
            this.btnProjectDb.Location = new System.Drawing.Point(495, 32);
            this.btnProjectDb.Name = "btnProjectDb";
            this.btnProjectDb.Size = new System.Drawing.Size(40, 23);
            this.btnProjectDb.TabIndex = 9;
            this.btnProjectDb.Text = ". . .";
            this.btnProjectDb.Click += new System.EventHandler(this.btnProjectDb_Click);
            // 
            // tbProjectDb
            // 
            this.tbProjectDb.Location = new System.Drawing.Point(104, 34);
            this.tbProjectDb.Name = "tbProjectDb";
            this.tbProjectDb.Size = new System.Drawing.Size(376, 20);
            this.tbProjectDb.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(38, 37);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Project Db:";
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.tbNewProjectName);
            this.groupControl2.Controls.Add(this.btnNewProjectLocation);
            this.groupControl2.Controls.Add(this.tbNewProjectLocation);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 107);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(554, 115);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "New Project Details";
            // 
            // tbNewProjectName
            // 
            this.tbNewProjectName.Location = new System.Drawing.Point(104, 73);
            this.tbNewProjectName.Name = "tbNewProjectName";
            this.tbNewProjectName.Size = new System.Drawing.Size(243, 20);
            this.tbNewProjectName.TabIndex = 11;
            // 
            // btnNewProjectLocation
            // 
            this.btnNewProjectLocation.Location = new System.Drawing.Point(495, 37);
            this.btnNewProjectLocation.Name = "btnNewProjectLocation";
            this.btnNewProjectLocation.Size = new System.Drawing.Size(40, 23);
            this.btnNewProjectLocation.TabIndex = 10;
            this.btnNewProjectLocation.Text = ". . .";
            this.btnNewProjectLocation.Click += new System.EventHandler(this.btnNewProjectLocation_Click);
            // 
            // tbNewProjectLocation
            // 
            this.tbNewProjectLocation.Location = new System.Drawing.Point(104, 39);
            this.tbNewProjectLocation.Name = "tbNewProjectLocation";
            this.tbNewProjectLocation.Size = new System.Drawing.Size(376, 20);
            this.tbNewProjectLocation.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(24, 76);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 13);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "Project Name:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(11, 42);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(81, 13);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Project Location:";
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(161, 239);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(322, 239);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // ImportSuperACE2008Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 271);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportSuperACE2008Form";
            this.Text = "Import SuperACE 2008";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImportSuperACE2008Form_FormClosing);
            this.Load += new System.EventHandler(this.ImportSuperACE2008Form_Load);
            this.Shown += new System.EventHandler(this.ImportSuperACE2008Form_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProjectDb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewProjectLocation.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbProjectDb;
        private DevExpress.XtraEditors.SimpleButton btnProjectDb;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnNewProjectLocation;
        private DevExpress.XtraEditors.TextEdit tbNewProjectLocation;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit tbNewProjectName;
        private DevExpress.XtraEditors.SimpleButton btnProcess;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit cbProject;
    }
}