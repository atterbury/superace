﻿using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using Project.DAL;
using SuperACECalcs;
using SuperACEUtils;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using Projects.DAL;
using Temp.DAL;

namespace Import
{
    public class SuperACE2008Import
    {
        private OleDbConnection _mdbConn = null;
        private OleDbCommand _mdbCmd = null;
        private OleDbDataReader _mdbReader = null;

        private List<Stand> _addStands = null;
        private List<StandInput> _addStandInput = null;
        private List<Plot> _addPlots = null;
        private List<Tree> _addTrees = null;
        private List<Treesegment> _addSegments = null;

        public bool ProcessProject(ref ProjectDbContext ctx, string pDb, string pTable, string pOldProject, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Project");
            OpenFile(pDb, pTable, "", "", "");
            ImportProject(ref ctx, pOldProject, pProject);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool CheckForExistingProjectTable(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project {0}", pProject));

            dbTemp.ExistingProjectTables.RemoveRange(dbTemp.ExistingProjectTables);
            CheckSpecies(ref ctx, ref dbTemp, pDb, "TSpecies");
            CheckSort(ref ctx, ref dbTemp, pDb, "TSORGRD");
            CheckGrade(ref ctx, ref dbTemp, pDb, "TSORGRD");
            CheckPrice(ref ctx, ref dbTemp, pDb, "TPRICE");
            CheckCost(ref ctx, ref dbTemp, pDb, "TCOST");
            CheckAspect(ref ctx, ref dbTemp, pDb, "TAspect");

            CheckComponent(ref ctx, ref dbTemp, pDb, "TComponent");
            CheckCrownPosition(ref ctx, ref dbTemp, pDb, "TCrwnPos");
            CheckDamage(ref ctx, ref dbTemp, pDb, "TDamage");
            CheckEnvironment(ref ctx, ref dbTemp, pDb, "TEnvir");
            CheckForm(ref ctx, ref dbTemp, pDb, "TForm");
            CheckHarvest(ref ctx, ref dbTemp, pDb, "THarvest");
            CheckLandForm(ref ctx, ref dbTemp, pDb, "TLandForm");
            CheckNonStocked(ref ctx, ref dbTemp, pDb, "TNStock");
            CheckNonTimbered(ref ctx, ref dbTemp, pDb, "TNTimber");
            CheckRoads(ref ctx, ref dbTemp, pDb, "TRoads");
            CheckSeedZone(ref ctx, ref dbTemp, pDb, "TSeedZone");
            CheckSlash(ref ctx, ref dbTemp, pDb, "TSlash");
            CheckSoils(ref ctx, ref dbTemp, pDb, "TSoils");
            CheckStreams(ref ctx, ref dbTemp, pDb, "TStream");
            CheckTreatment(ref ctx, ref dbTemp, pDb, "TTreatment");
            CheckTreeSource(ref ctx, ref dbTemp, pDb, "TTreeSrc");
            CheckTreeStatus(ref ctx, ref dbTemp, pDb, "TStatus");
            CheckUserDefined(ref ctx, ref dbTemp, pDb, "TT5");
            CheckVegetation(ref ctx, ref dbTemp, pDb, "TVeg");
            CheckVigor(ref ctx, ref dbTemp, pDb, "TVigor");
            CheckWoodType(ref ctx, ref dbTemp, pDb, "TWoodType");
            SplashScreenManager.CloseForm();
            return true;
        }

        public bool ProcessProjectTables(ref ProjectDbContext ctx, string pDb, List<ExistingProjectTable> pTables, string pProject, string pProjectLocationDataSource)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            foreach (var item in pTables)
            {
                if (item != null)
                {
                    if (item.TableType != null && item.ExistingTable != null && item.ReplaceTable != null)
                    {
                        switch (item.TableType)
                        {
                            case "01-Species":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessSpecies(ref ctx, pDb, "TSpecies", item.TableName, pProjectLocationDataSource);
                                break;
                            case "02-Sort":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessSort(ref ctx, pDb, "TSORGRD", item.TableName);
                                break;
                            case "03-Grade":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessGrade(ref ctx, pDb, "TSORGRD", item.TableName);
                                break;
                            case "04-Price":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessPrice(ref ctx, pDb, "TPRICE", item.TableName);
                                break;
                            case "05-Cost":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessCost(ref ctx, pDb, "TCOST", item.TableName);
                                break;
                            case "06-Aspect":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessAspect(ref ctx, pDb, "TAspect", item.TableName);
                                break;
                            case "07-Component":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessComponent(ref ctx, pDb, "TComponent", item.TableName);
                                break;
                            case "08-CrownPosition":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessCrownPosition(ref ctx, pDb, "TCrwnPos", item.TableName);
                                break;
                            case "09-Damage":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessDamage(ref ctx, pDb, "TDamage", item.TableName);
                                break;
                            case "10-Environment":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessEnvironment(ref ctx, pDb, "TEnvir", item.TableName);
                                break;
                            case "11-Form":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessForm(ref ctx, pDb, "TForm", item.TableName);
                                break;
                            case "12-Harvest":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessHarvest(ref ctx, pDb, "THarvest", item.TableName);
                                break;
                            case "13-LandForm":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessLandForm(ref ctx, pDb, "TLandForm", item.TableName);
                                break;
                            case "14-NonStocked":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessNonStocked(ref ctx, pDb, "TNStock", item.TableName);
                                break;
                            case "15-NonTimbered":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessNonTimbered(ref ctx, pDb, "TNTimber", item.TableName);
                                break;
                            case "16-Roads":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessRoads(ref ctx, pDb, "TRoads", item.TableName);
                                break;
                            case "17-SeedZone":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessSeedZone(ref ctx, pDb, "TSeedZone", item.TableName);
                                break;
                            case "18-Slash":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessSlash(ref ctx, pDb, "TSlash", item.TableName);
                                break;
                            case "19-Soils":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessSoils(ref ctx, pDb, "TSoils", item.TableName);
                                break;
                            case "20-Streams":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessStreams(ref ctx, pDb, "TStream", item.TableName);
                                break;
                            case "21-Treatment":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessTreatment(ref ctx, pDb, "TTreatment", item.TableName);
                                break;
                            case "22-TreeSource":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessTreeSource(ref ctx, pDb, "TTreeSrc", item.TableName);
                                break;
                            case "23-TreeStatus":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessTreeStatus(ref ctx, pDb, "TStatus", item.TableName);
                                break;
                            case "24-UserDefined":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessUserDefined(ref ctx, pDb, "TT5", item.TableName);
                                break;
                            case "25-Vegetation":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessVegetation(ref ctx, pDb, "TVeg", item.TableName);
                                break;
                            case "26-Vigor":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessVigor(ref ctx, pDb, "TVigor", item.TableName);
                                break;
                            case "27-WoodType":
                                if (!(bool) item.ExistingTable || (bool) item.ReplaceTable)
                                    ProcessWoodType(ref ctx, pDb, "TWoodType", item.TableName);
                                break;
                        }
                    }
                }
            }
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessProjectDefaultTables(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Default Tables");
            OpenFile(pDb, "TTables", "", "", "");
            ImportProjectDefaultTable(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessStand(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Stands");
            OpenFile(pDb, pTable, "[Township], [Range], [Section], [Tract], [Type]", "", "");
            ImportStand(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();

            return true;
        }

        public bool ProcessStandInput(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Stand Input");
            OpenFile(pDb, pTable, "[Township], [Range], [Section], [Type]", "", "");
            ImportStandInput(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();

            return true;
        }

        public bool ProcessPlots(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Plots");
            OpenFile(pDb, "TPLOTLOC", "[Township], [Range], [Section], [Tract], [Type], [PlotNo]", "", "");
            ImportPlot(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();
            return true;
        }

        public bool ProcessTrees(ref ProjectDbContext ctx, string pDb, string pTable, string pProject)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption(string.Format("Processing Project: {0}", pProject));
            SplashScreenManager.Default.SetWaitFormDescription("Trees");
            OpenFile(pDb, "TCRUISE", "[Township], [Range], [Section], [Tract], [Type], [PlotNo], [TreeNo], [Continuation]", "[Township], [Range], [Section], [Tract], [Type], [PlotNo], [TreeNo], [Continuation]", "");
            ImportTree(ref ctx);
            ProcessTreeSegments(ref ctx);
            SplashScreenManager.CloseForm();
            CloseFile();
            
            return true;
        }

        private void OpenGroupBy(string pDb, string pTable)
        {
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                _mdbConn = new OleDbConnection(connectionString);

                _mdbConn.Open();

                string sql = string.Format("SELECT [Table Name] FROM {0} GROUP BY [Table Name] ORDER BY [Table Name]", pTable);
                _mdbCmd = new OleDbCommand(sql, _mdbConn);
                _mdbReader = _mdbCmd.ExecuteReader();
            }
            catch (InvalidOperationException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            catch
            {
                XtraMessageBox.Show("Error Opening database");
            }
        }

        private void OpenFile(string pDb, string pTable, string pOrderBy, string pFieldName, string pFieldValue) //
        {
            //string sDir = sSourceFile.Substring(0, sSourceFile.LastIndexOf('\\'));
            //string sFile = sSourceFile.Substring(sSourceFile.LastIndexOf('\\') + 1, sSourceFile.LastIndexOf('.') - (sSourceFile.LastIndexOf('\\') + 1));
            //string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}", pDb);
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                _mdbConn = new OleDbConnection(connectionString);

                _mdbConn.Open();

                string sql = string.Empty;
                if (!string.IsNullOrEmpty(pFieldName) && !string.IsNullOrEmpty(pFieldValue))
                    sql = string.Format("SELECT * FROM {0} WHERE [{1}] = '{2}'", pTable, pFieldName, pFieldValue);
                else
                    if (!string.IsNullOrEmpty(pOrderBy))
                        sql = string.Format("SELECT * FROM {0} ORDER BY {1}", pTable, pOrderBy);
                    else
                        sql = string.Format("SELECT * FROM {0}", pTable);
                _mdbCmd = new OleDbCommand(sql, _mdbConn);
                _mdbReader = _mdbCmd.ExecuteReader();
            }
            catch (InvalidOperationException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            catch
            {
                XtraMessageBox.Show("Error Opening database");
            }
        }

        private void OpenFileSortGrade(string pDb, string pTable, string pFieldName, string pFieldValue, string pCode) //
        {
            //string sDir = sSourceFile.Substring(0, sSourceFile.LastIndexOf('\\'));
            //string sFile = sSourceFile.Substring(sSourceFile.LastIndexOf('\\') + 1, sSourceFile.LastIndexOf('.') - (sSourceFile.LastIndexOf('\\') + 1));
            //string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}", pDb);
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                _mdbConn = new OleDbConnection(connectionString);

                _mdbConn.Open();

                string sql = string.Empty;
                sql = string.Format("SELECT * FROM {0} WHERE [{1}] = '{2}' AND [Record Code] = '{3}'", pTable, pFieldName, pFieldValue, pCode);
                _mdbCmd = new OleDbCommand(sql, _mdbConn);
                _mdbReader = _mdbCmd.ExecuteReader();
            }
            catch (InvalidOperationException ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            catch
            {
                XtraMessageBox.Show("Error Opening database");
            }
        }

        private void CloseFile()
        {
            //mdbTbl.Dispose();
            _mdbReader.Close();
            _mdbCmd.Dispose();
            _mdbConn.Close();
        }

        private void ImportProject(ref ProjectDbContext ctx, string pOldProject, string pProject)
        {
            //List<Project.DAL.Project> addProjects = new List<Project.DAL.Project>();
            while (_mdbReader.Read())
            {
                if (_mdbReader["Project"].ToString().Trim() == pOldProject)
                {
                    Project.DAL.Project prj = ctx.Projects.FirstOrDefault(); // new Project();
                    if (prj != null)
                    {
                        prj.ProjectName = pProject;
                        prj.CustomerAddress1 = _mdbReader["Address 1"].ToString().Trim();
                        prj.CustomerAddress2 = _mdbReader["Address 2"].ToString().Trim();
                        prj.CustomerCellPhone = string.Empty;
                        prj.CustomerCity = string.Empty;
                        prj.CustomerCompanyName = _mdbReader["Customer Name"].ToString().Trim();
                        prj.CustomerContactName = _mdbReader["Key Contact"].ToString().Trim();
                        prj.CustomerEmail = _mdbReader["EMail"].ToString().Trim();
                        prj.CustomerFax = _mdbReader["Fax"].ToString().Trim();
                        prj.CustomerNotes = string.Empty;
                        prj.CustomerOfficePhone = _mdbReader["Phone"].ToString().Trim();
                        prj.CustomerPostalCode = string.Empty;
                        prj.CustomerState = string.Empty;
                        prj.ProjectLead = string.Empty;
                        prj.ProjectNumber = string.Empty;
                        prj.SaleName = string.Empty;
                        prj.SureToBeMeasured = string.Empty;
                    }
                }
            }
            ctx.SaveChanges();

            //ctx.Dispose();
        }

        private void ImportProjectDefaultTable(ref ProjectDbContext ctx)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            if (_mdbReader.Read())
            {
                Project.DAL.Project prj = ctx.Projects.FirstOrDefault(); // was p => p.ProjectName == pProjectName
                if (prj != null)
                {
                    prj.AspectsTableName = _mdbReader["Aspect"].ToString().Trim();
                    prj.ASuboTableName = "GENERAL";
                    prj.BarkTableName = "GENERAL";
                    prj.BdFtRule = "GENERAL";
                    prj.ComponentsTableName = "GENERAL";
                    prj.CostTableName = _mdbReader["Cost"].ToString().Trim();
                    prj.CrownPositionTableName = _mdbReader["Crown Position"].ToString().Trim();
                    prj.CruisersTableName = "GENERAL";
                    prj.CuFtRule = "GENERAL";
                    prj.DamageTableName = _mdbReader["Damage"].ToString().Trim();
                    prj.DestinationTableName = "GENERAL";
                    prj.EnvironmentsTableName = _mdbReader["Environment"].ToString().Trim();
                    prj.FormTableName = "GENERAL";
                    prj.GradesTableName = _mdbReader["SortGrade"].ToString().Trim(); //.Replace("SORT", "GRADE");
                    prj.GrowthModelTableName = string.Empty;
                    prj.HabitatTableName = "GENERAL";
                    prj.HarvestSystemsTableName = _mdbReader["Harvest"].ToString().Trim();
                    prj.LandFormsTableName = _mdbReader["Landform"].ToString().Trim();
                    prj.NonStockedTableName = "GENERAL";
                    prj.NonTimberedTableName = "GENERAL";
                    prj.PilingTableName = "GENERAL";
                    prj.PolesTableName = "GENERAL";
                    prj.PriceTableName = _mdbReader["Price"].ToString().Trim();
                    prj.ProjectAdjustmentsTableName = "GENERAL";
                    prj.RoadsTableName = _mdbReader["Roads"].ToString().Trim();
                    prj.SeedZonesTableName = _mdbReader["Seed Zone"].ToString().Trim();
                    prj.SlashDistributionsTableName = _mdbReader["Slash"].ToString().Trim();
                    prj.SoilsTableName = _mdbReader["Soils"].ToString().Trim();
                    prj.SortsTableName = _mdbReader["SortGrade"].ToString().Trim();
                    prj.SpeciesTableName = _mdbReader["Species"].ToString().Trim();
                    prj.StandActivitiesTableName = "GENERAL";
                    prj.StandDataSourcesTableName = "GENERAL";
                    prj.StreamsTableName = _mdbReader["Stream"].ToString().Trim();
                    prj.TreatmentsTableName = _mdbReader["Treatment"].ToString().Trim();
                    prj.TreeSourcesTableName = _mdbReader["Tree Source"].ToString().Trim();
                    prj.TreeStatusTableName = _mdbReader["Status"].ToString().Trim();
                    prj.UserDefinedTableName = _mdbReader["T5"].ToString().Trim();
                    prj.VegetationsTableName = _mdbReader["Vegetation"].ToString().Trim();
                    prj.VigorTableName = _mdbReader["Vigor"].ToString().Trim();
                    prj.WoodTypeTableName = "GENERAL";
                }
            }
            ctx.SaveChanges();
            //ctx.Dispose();
        }
        
        private void ImportStand(ref ProjectDbContext ctx)
        {
            //List<Stand> addStands = new List<Stand>();
            int count = 0;
            _addStands = new List<Stand>();
            Project.DAL.Project prj = ctx.Projects.FirstOrDefault(); // p => p.ProjectName == pProject
            while (_mdbReader.Read())
            {
                Stand stand = new Stand();
                stand.ProjectsId = prj.ProjectsId;
                int month = Utils.ConvertToInt(_mdbReader["Month"].ToString());
                int day = Utils.ConvertToInt(_mdbReader["Day"].ToString());
                int year = Utils.ConvertToInt(_mdbReader["Year"].ToString());

                if (day == 0)
                    day = 1;
                stand.DateOfStandData = Convert.ToDateTime(string.Format("{0}-{1}-{2}", month, day, year));
                stand.GrownToDate = Convert.ToDateTime(string.Format("{0}-{1}-{2}", month, day, year));
                stand.GrownToMonth = Utils.ConvertToInt(_mdbReader["Grown Month"].ToString());
                stand.GrownToYear = Utils.ConvertToInt(_mdbReader["Grown Year"].ToString());
                stand.AvgTotalHeight = 0;
                stand.BasalAreaPerAcre = 0;
                //stand.BirthDate = null;
                stand.CcfPerAcre = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Cost"].ToString().Trim()))
                    stand.CostTableName = _mdbReader["Cost"].ToString().Trim();
                else
                    stand.CostTableName = string.Empty;
                stand.DiameterInterval = string.Empty;
                stand.DiameterRound = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Dia Interval"].ToString()))
                //    stand.DiameterInterval = mdbReader["Dia Interval"].ToString();
                //else
                //stand.DiameterInterval = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Dia Round"].ToString()))
                ////    stand.DiameterRound = mdbReader["Dia Round"].ToString();
                ////else
                //stand.DiameterRound = string.Empty;
                //stand.EnvironmentsDisplayCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Interval Zero"].ToString()))
                ////    stand.IntervalZero = mdbReader["Interval Zero"].ToString();
                ////else
                //    stand.IntervalZero = string.Empty;
                stand.GrossGeographicAcres = Utils.ConvertToDouble(_mdbReader["Acres"].ToString());
                stand.NetGeographicAcres = Utils.ConvertToDouble(_mdbReader["Acres"].ToString());
                stand.LegalAcres = Utils.ConvertToDouble(_mdbReader["Acres"].ToString());
                stand.LogsPerAcre = 0;
                stand.MajorAge = 0;
                stand.MbfPerAcre = 0;
                stand.Meridian = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Price"].ToString().Trim()))
                    stand.PriceTableName = _mdbReader["Price"].ToString().Trim();
                else
                    stand.PriceTableName = string.Empty;
                stand.QmDbh = 0;
                stand.Range = _mdbReader["Range"].ToString().Trim();
                ////if (!string.IsNullOrEmpty(mdbReader["Samples Each Class"].ToString()))
                ////    stand.SamplesEachClass = mdbReader["Samples Each Class"].ToString();
                ////else
                //    stand.SamplesEachClass = string.Empty;
                stand.Section = _mdbReader["Section"].ToString().Trim();
                //stand.SeedZonesDisplayCode = string.Empty;
                //stand.SlashDistributionsDisplayCode = string.Empty;
                //stand.SNCRetentionYears = 0;
                //stand.SoilsDisplayCode = string.Empty;
                //stand.StandAdjustmentsTableName = string.Empty;
                //stand.StandDataSourcesDisplayCode = string.Empty;
                //stand.StandExamDate = null;
                //stand.StandGrowthProjectionDate = null;
                stand.StandName = _mdbReader["Type"].ToString().Trim();
                //stand.StockingStatusPrimaryDisplayCode = string.Empty;
                //stand.StockingStatusSecondaryDisplayCode = string.Empty;
                //stand.StreamsDisplayCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Sure To Be Measured"].ToString()))
                ////    stand.SureToBeMeasured = mdbReader["Sure To Be Measured"].ToString();
                ////else
                //    stand.SureToBeMeasured = string.Empty;
                stand.TonsPerAcre = 0;
                stand.TotalBdFtGross = 0;
                stand.TotalBdFtNet = 0;
                stand.TotalCcf = 0;
                stand.TotalCuFtGross = 0;
                stand.TotalCuFtMerch = 0;
                stand.TotalCuFtNet = 0;
                stand.TotalMbf = 0;
                stand.TotalTons = 0;
                stand.Township = _mdbReader["Township"].ToString().Trim();
                stand.TractName = _mdbReader["Tract"].ToString().Trim();
                stand.TractGroup = _mdbReader["Tract"].ToString().Trim();
                stand.TransportationsTableName = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Source"].ToString().Trim()))
                    stand.Source = _mdbReader["Source"].ToString().Trim();
                else
                    stand.Source = string.Empty;
                stand.TreesPerAcre = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["UseStndDbh"].ToString()))
                ////    stand.UseStandDbh = mdbReader["UseStndDbh"].ToString();
                ////else
                //    stand.UseStandDbh = string.Empty;
                //stand.VegetationsDisplayCode = string.Empty;

                // ToDo
                //stand.Age1 = (short)Utils.ConvertToInt(_mdbReader["Age1"].ToString());
                //stand.Age2 = (short)Utils.ConvertToInt(_mdbReader["Age2"].ToString());
                //stand.Age3 = (short)Utils.ConvertToInt(_mdbReader["Age3"].ToString());
                //stand.Age4 = (short)Utils.ConvertToInt(_mdbReader["Age4"].ToString());
                //stand.Age5 = (short)Utils.ConvertToInt(_mdbReader["Age5"].ToString());
                //stand.Age6 = (short)Utils.ConvertToInt(_mdbReader["Age6"].ToString());
                //stand.Age7 = (short)Utils.ConvertToInt(_mdbReader["Age7"].ToString());
                //stand.Age8 = (short)Utils.ConvertToInt(_mdbReader["Age8"].ToString());
                //stand.Age9 = (short)Utils.ConvertToInt(_mdbReader["Age9"].ToString());

                stand.Baf1 = Utils.ConvertToDouble(_mdbReader["BAFactor1"].ToString());
                stand.BafPlotRadius1 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf1);
                stand.Baf2 = Utils.ConvertToDouble(_mdbReader["BAFactor2"].ToString());
                stand.BafPlotRadius2 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf2);
                stand.Baf3 = Utils.ConvertToDouble(_mdbReader["BAFactor3"].ToString());
                stand.BafPlotRadius3 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf3);
                stand.Baf4 = Utils.ConvertToDouble(_mdbReader["BAFactor4"].ToString());
                stand.BafPlotRadius4 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf4);
                stand.Baf5 = Utils.ConvertToDouble(_mdbReader["BAFactor5"].ToString());
                stand.BafPlotRadius5 = Utils.CalculateLimitingDistanceFactor((float)stand.Baf5);

                PopulateArea(ref stand, _mdbReader);
                // Not used anymore
                //stand.FixedArea1 = Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString());
                //stand.FixedArea2 = Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString());
                //stand.FixedArea3 = Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString());
                //stand.FixedArea4 = Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString());
                //stand.FixedArea5 = Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString());

                //stand.Ref1 = Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString());
                //stand.Ref2 = Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString());
                //stand.Ref3 = Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString());
                //stand.Ref4 = Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString());
                //stand.Ref5 = Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString());

                stand.StripBlowup1 = Utils.ConvertToDouble(_mdbReader["StripFact1"].ToString());
                stand.StripBlowup2 = Utils.ConvertToDouble(_mdbReader["StripFact2"].ToString());
                stand.StripBlowup3 = Utils.ConvertToDouble(_mdbReader["StripFact3"].ToString());
                stand.StripBlowup4 = Utils.ConvertToDouble(_mdbReader["StripFact4"].ToString());
                stand.StripBlowup5 = Utils.ConvertToDouble(_mdbReader["StripFact5"].ToString());

                stand.Species11 = _mdbReader["Species11"].ToString().Trim();
                stand.Species12 = _mdbReader["Species12"].ToString().Trim();
                stand.Species13 = _mdbReader["Species13"].ToString().Trim();
                stand.SiteIndex1 = (short)Utils.ConvertToInt(_mdbReader["SiteIndex1"].ToString());

                stand.Species21 = _mdbReader["Species21"].ToString().Trim();
                stand.Species22 = _mdbReader["Species22"].ToString().Trim();
                stand.Species23 = _mdbReader["Species23"].ToString().Trim();
                stand.SiteIndex2 = (short)Utils.ConvertToInt(_mdbReader["SiteIndex2"].ToString());

                stand.Species31 = _mdbReader["Species31"].ToString().Trim();
                stand.Species32 = _mdbReader["Species32"].ToString().Trim();
                stand.Species33 = _mdbReader["Species33"].ToString().Trim();
                stand.SiteIndex3 = (short)Utils.ConvertToInt(_mdbReader["SiteIndex3"].ToString());

                stand.Species41 = _mdbReader["Species41"].ToString().Trim();
                stand.Species42 = _mdbReader["Species42"].ToString().Trim();
                stand.Species43 = _mdbReader["Species43"].ToString().Trim();
                stand.SiteIndex4 = (short)Utils.ConvertToInt(_mdbReader["SiteIndex4"].ToString());

                //// TODO Cruisers
                ////if (!string.IsNullOrEmpty(mdbReader["Cruiser"].ToString()))
                ////    cs.Cruiser = mdbReader["Cruiser"].ToString();
                ////else
                ////    cs.Cruiser = string.Empty;

                ////if (!string.IsNullOrEmpty(mdbReader["Destination"].ToString()))
                ////    cs.Destination = mdbReader["Destination"].ToString();
                ////else
                ////    cs.Destination = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Hours"].ToString()))
                ////    cs.Hours = float.Parse(mdbReader["Hours"].ToString());
                ////else
                ////    cs.Hours = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Miles"].ToString()))
                ////    cs.Miles = float.Parse(mdbReader["Miles"].ToString());
                ////else
                ////    cs.Miles = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Plots"].ToString()))
                ////    cs.Plots = Int16.Parse(mdbReader["Plots"].ToString());
                ////else
                ////    cs.Plots = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Grown Month"].ToString()))
                    stand.GrownToMonth = Int16.Parse(_mdbReader["Grown Month"].ToString());
                if (!string.IsNullOrEmpty(_mdbReader["Grown Year"].ToString()))
                    stand.GrownToYear = Int16.Parse(_mdbReader["Grown Year"].ToString());
                if (!string.IsNullOrEmpty(_mdbReader["Catagory"].ToString()))
                {
                    if (_mdbReader["Catagory"].ToString().StartsWith("NS"))
                        stand.LandClass = "NS";
                    else
                        if (_mdbReader["Catagory"].ToString().StartsWith("NT"))
                            stand.LandClass = "NT";
                        else
                            stand.LandClass = "TIM";
                }
                else
                {
                    stand.LandClass = "TIM";
                }
                ////if (!string.IsNullOrEmpty(mdbReader["Non Forest"].ToString()))
                ////    cs.NonForest = mdbReader["Non Forest"].ToString();
                ////else
                ////    cs.NonForest = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Site Index"].ToString()))
                    stand.SiteIndex = Int16.Parse(_mdbReader["Site Index"].ToString());
                else
                    stand.SiteIndex = 0;

                ////if (!string.IsNullOrEmpty(mdbReader["Weyco D4h"].ToString()))
                ////    cs.WeycoD4h = mdbReader["Weyco D4h"].ToString();
                ////else
                ////    cs.WeycoD4h = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Inv Code"].ToString()))
                ////    cs.InvCode = mdbReader["Inv Code"].ToString();
                ////else
                ////    cs.InvCode = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["EngMet Code"].ToString()))
                ////    cs.EngMetCode = mdbReader["EngMet Code"].ToString();
                ////else
                ////    cs.EngMetCode = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Inventory Flag"].ToString().Trim()))
                    stand.InventoryFlag = _mdbReader["Inventory Flag"].ToString().Trim();
                else
                    stand.InventoryFlag = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Cruise Flag"].ToString().Trim()))
                    stand.CruiseFlag = _mdbReader["Cruise Flag"].ToString().Trim();
                else
                    stand.CruiseFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Reforestation Flag"].ToString()))
                ////    cs.ReforestationFlag = mdbReader["Reforestation Flag"].ToString();
                ////else
                ////    cs.ReforestationFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Site Flag"].ToString()))
                ////    cs.SiteFlag = mdbReader["Site Flag"].ToString();
                ////else
                ////    cs.SiteFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Class Flag"].ToString()))
                ////    cs.ClassFlag = mdbReader["Class Flag"].ToString();
                ////else
                ////    cs.ClassFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Scale Flag"].ToString()))
                ////    cs.ScaleFlag = mdbReader["Scale Flag"].ToString();
                ////else
                ////    cs.ScaleFlag = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["Cruise Trees"].ToString()))
                ////    cs.CruiseTrees = Int16.Parse(mdbReader["Cruise Trees"].ToString());
                ////else
                ////    cs.CruiseTrees = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Count Trees"].ToString()))
                ////    cs.CountTrees = Int16.Parse(mdbReader["Count Trees"].ToString());
                ////else
                ////    cs.CountTrees = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Percent Trees Cruised"].ToString()))
                ////    cs.PercentTreesCruised = Int16.Parse(mdbReader["Percent Trees Cruised"].ToString());
                ////else
                ////    cs.PercentTreesCruised = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Av Plots Cruised"].ToString()))
                ////    cs.AvPlotsCruised = Single.Parse(mdbReader["Av Plots Cruised"].ToString());
                ////else
                ////    cs.AvPlotsCruised = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["Av Plots Counted"].ToString()))
                ////    cs.AvPlotsCounted = Single.Parse(mdbReader["Av Plots Counted"].ToString());
                ////else
                ////    cs.AvPlotsCounted = 0;
                ////if (!string.IsNullOrEmpty(mdbReader["InvAdjustmentTbl"].ToString()))
                ////    cs.InvAdjustmentTbl = mdbReader["InvAdjustmentTbl"].ToString();
                ////else
                ////    cs.InvAdjustmentTbl = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["CruAdjustmentTbl"].ToString()))
                ////    cs.CruAdjustmentTbl = mdbReader["CruAdjustmentTbl"].ToString();
                ////else
                ////    cs.CruAdjustmentTbl = string.Empty;
                ////if (!string.IsNullOrEmpty(mdbReader["CalcBaTpa"].ToString()))
                ////    cs.CalcBaTpa = mdbReader["CalcBaTpa"].ToString();
                ////else
                ////    cs.CalcBaTpa = string.Empty;
                ++count;
                _addStands.Add(stand);
                if (count > 100)
                {
                    count = 0;
                    ctx.Stands.AddRange(_addStands);
                    _addStands.Clear();
                }
                //ctx.Stands.Add(stand);
                //ctx = StandAddToContext(pDataSource, ctx, stand, count, 100, true);
            }
            ctx.Stands.AddRange(_addStands);
            ctx.SaveChanges();
            _addStands.Clear();
            //ctx.Dispose();
        }

        private void CalcFixed(ref Stand stand, short i, string areaCode, string areaPlotAcres)
        {
            string plotRadius = Utils.CalculateAreaPlotRadius(areaCode, areaPlotAcres);
            string blowup = Utils.CalculateAreaBlowup(areaCode, areaPlotAcres);
            if (!string.IsNullOrEmpty(plotRadius))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaPlotRadius1 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 2:
                        stand.AreaPlotRadius2 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 3:
                        stand.AreaPlotRadius3 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 4:
                        stand.AreaPlotRadius4 = Utils.ConvertToDouble(plotRadius);
                        break;
                    case 5:
                        stand.AreaPlotRadius5 = Utils.ConvertToDouble(plotRadius);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(blowup))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaBlowup1 = Utils.ConvertToDouble(blowup);
                        break;
                    case 2:
                        stand.AreaBlowup2 = Utils.ConvertToDouble(blowup);
                        break;
                    case 3:
                        stand.AreaBlowup3 = Utils.ConvertToDouble(blowup);
                        break;
                    case 4:
                        stand.AreaBlowup4 = Utils.ConvertToDouble(blowup);
                        break;
                    case 5:
                        stand.AreaBlowup5 = Utils.ConvertToDouble(blowup);
                        break;
                }
            }
        }

        private void CalcRef(ref Stand stand, short i, string areaCode, string areaPlotRadius)
        {
            string plotAcres = Utils.CalculateAreaPlotAcres(areaCode, areaPlotRadius);
            string blowup = Utils.CalculateAreaBlowup(areaCode, plotAcres);
            if (!string.IsNullOrEmpty(plotAcres))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaPlotAcres1 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 2:
                        stand.AreaPlotAcres2 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 3:
                        stand.AreaPlotAcres3 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 4:
                        stand.AreaPlotAcres4 = Utils.ConvertToDouble(plotAcres);
                        break;
                    case 5:
                        stand.AreaPlotAcres5 = Utils.ConvertToDouble(plotAcres);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(blowup))
            {
                switch (i)
                {
                    case 1:
                        stand.AreaBlowup1 = Utils.ConvertToDouble(blowup);
                        break;
                    case 2:
                        stand.AreaBlowup2 = Utils.ConvertToDouble(blowup);
                        break;
                    case 3:
                        stand.AreaBlowup3 = Utils.ConvertToDouble(blowup);
                        break;
                    case 4:
                        stand.AreaBlowup4 = Utils.ConvertToDouble(blowup);
                        break;
                    case 5:
                        stand.AreaBlowup5 = Utils.ConvertToDouble(blowup);
                        break;
                }
            }
        }

        private void PopulateArea(ref Stand stand, OleDbDataReader mdbReader)
        {
            if (Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString()) > 0)
            {
                stand.AreaCode1 = "F1";
                stand.AreaPlotAcres1 = Utils.ConvertToDouble(mdbReader["FixPlotA1"].ToString());
                CalcFixed(ref stand, 1, stand.AreaCode1, stand.AreaPlotAcres1.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString()) > 0)
            {
                stand.AreaCode1 = "R1";
                stand.AreaPlotRadius1 = Utils.ConvertToDouble(mdbReader["PlotRad1"].ToString());
                CalcRef(ref stand, 1, stand.AreaCode1, stand.AreaPlotRadius1.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString()) > 0)
            {
                stand.AreaCode2 = "F2";
                stand.AreaPlotAcres2 = Utils.ConvertToDouble(mdbReader["FixPlotA2"].ToString());
                CalcFixed(ref stand, 2, stand.AreaCode2, stand.AreaPlotAcres2.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString()) > 0)
            {
                stand.AreaCode2 = "R2";
                stand.AreaPlotRadius2 = Utils.ConvertToDouble(mdbReader["PlotRad2"].ToString());
                CalcRef(ref stand, 2, stand.AreaCode2, stand.AreaPlotRadius2.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString()) > 0)
            {
                stand.AreaCode3 = "F3";
                stand.AreaPlotAcres3 = Utils.ConvertToDouble(mdbReader["FixPlotA3"].ToString());
                CalcFixed(ref stand, 3, stand.AreaCode3, stand.AreaPlotAcres3.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString()) > 0)
            {
                stand.AreaCode3 = "R3";
                stand.AreaPlotRadius3 = Utils.ConvertToDouble(mdbReader["PlotRad3"].ToString());
                CalcRef(ref stand, 3, stand.AreaCode3, stand.AreaPlotRadius3.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString()) > 0)
            {
                stand.AreaCode4 = "F4";
                stand.AreaPlotAcres4 = Utils.ConvertToDouble(mdbReader["FixPlotA4"].ToString());
                CalcFixed(ref stand, 4, stand.AreaCode4, stand.AreaPlotAcres4.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString()) > 0)
            {
                stand.AreaCode4 = "R4";
                stand.AreaPlotRadius4 = Utils.ConvertToDouble(mdbReader["PlotRad4"].ToString());
                CalcRef(ref stand, 4, stand.AreaCode4, stand.AreaPlotRadius4.ToString());
            }

            if (Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString()) > 0)
            {
                stand.AreaCode5 = "F5";
                stand.AreaPlotAcres5 = Utils.ConvertToDouble(mdbReader["FixPlotA5"].ToString());
                CalcFixed(ref stand, 5, stand.AreaCode5, stand.AreaPlotAcres5.ToString());
            }
            if (Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString()) > 0)
            {
                stand.AreaCode5 = "R5";
                stand.AreaPlotRadius5 = Utils.ConvertToDouble(mdbReader["PlotRad5"].ToString());
                CalcRef(ref stand, 5, stand.AreaCode5, stand.AreaPlotRadius5.ToString());
            }
        }

        private void ImportStandInput(ref ProjectDbContext ctx)
        {
            Stand currentStand = null;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            int count = 0;

            _addStandInput = new List<StandInput>();
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            //Project prj = ctx.Project.FirstOrDefault(p => p.ProjectName == pProject);
            while (_mdbReader.Read())
            {
                //string sTract = mdbReader["Tract"].ToString();
                string sStand = _mdbReader["Type"].ToString();
                string sTownship = _mdbReader["Township"].ToString();
                string sRange = _mdbReader["Range"].ToString();
                string sSection = _mdbReader["Section"].ToString();
                string sTract = string.Empty;
                currentStand = ctx.Stands.FirstOrDefault(p => p.Township == sTownship && p.Range == sRange && p.Section == sSection && p.StandName == sStand);
                if (currentStand != null)
                {
                    sTract = currentStand.TractName;
                    currentKey = string.Format("{0}{1}{2}{3}{4}", sTownship, sRange, sSection, sTract, sStand);
                    if (saveKey != currentKey)
                    {
                        //currentStand = ProjectBLL.GetStand(ref ctx, , sTract, sStand, sTownship, sRange, sSection);
                        currentStand = ctx.Stands.FirstOrDefault(p => p.Township == sTownship && p.Range == sRange && p.Section == sSection && p.StandName == sStand);
                        saveKey = currentKey;
                    }

                    if (currentStand != null)
                    {
                        StandInput standInput = new StandInput();
                        standInput.Age = (short)Utils.ConvertToInt(_mdbReader["Age"].ToString());
                        standInput.BasalArea = Utils.ConvertToDouble(_mdbReader["Basal Area"].ToString());
                        standInput.BirthYear = (short)Utils.ConvertToInt(_mdbReader["Birth Year"].ToString());
                        standInput.Cost = Utils.ConvertToDouble(_mdbReader["Cost"].ToString());
                        standInput.D4H = (short)Utils.ConvertToInt(_mdbReader["D4h"].ToString());
                        standInput.DcHeight = 0;
                        standInput.GrossBdFt = Utils.ConvertToDouble(_mdbReader["Gross Bdft"].ToString());
                        standInput.GrossCuFt = Utils.ConvertToDouble(_mdbReader["Gross CuFt"].ToString());
                        standInput.LogsPerAcre = Utils.ConvertToDouble(_mdbReader["Logs"].ToString());
                        standInput.NetAcres = currentStand.NetGeographicAcres;
                        standInput.NetBdFt = Utils.ConvertToDouble(_mdbReader["Net BdFt"].ToString());
                        standInput.NetCuFt = Utils.ConvertToDouble(_mdbReader["Net CuFt"].ToString());
                        standInput.OrigAge = (short)Utils.ConvertToInt(_mdbReader["OrigAge"].ToString());
                        standInput.OrigBasalArea = Utils.ConvertToDouble(_mdbReader["OrigBasalArea"].ToString());
                        standInput.OrigD4H = (short)Utils.ConvertToInt(_mdbReader["OrigD4h"].ToString());
                        standInput.OrigDcHeight = 0;
                        standInput.OrigGrossBdFt = Utils.ConvertToDouble(_mdbReader["OrigGrossBdFt"].ToString());
                        standInput.OrigGrossCuFt = Utils.ConvertToDouble(_mdbReader["OrigGrossCuFt"].ToString());
                        standInput.OrigLogDib = (short)Utils.ConvertToInt(_mdbReader["OrigLogDib"].ToString());
                        standInput.OrigLogLen = (short)Utils.ConvertToInt(_mdbReader["OrigLogLen"].ToString());
                        standInput.OrigLogsPerAcre = Utils.ConvertToDouble(_mdbReader["OrigLogs"].ToString());
                        standInput.OrigNetBdFt = Utils.ConvertToDouble(_mdbReader["OrigNetBdFt"].ToString());
                        standInput.OrigNetCuFt = Utils.ConvertToDouble(_mdbReader["OrigNetCuFt"].ToString());
                        standInput.OrigStocking = (short)Utils.ConvertToInt(_mdbReader["OrigStocking"].ToString());
                        standInput.OrigTotalHeight = Utils.ConvertToDouble(_mdbReader["OrigTotalHeight"].ToString());
                        standInput.OrigTreesPerAcre = Utils.ConvertToDouble(_mdbReader["OrigTpa"].ToString());
                        standInput.PoleSeq = (short)Utils.ConvertToInt(_mdbReader["Pole Seq"].ToString());
                        standInput.Revenue = Utils.ConvertToDouble(_mdbReader["Revenue"].ToString());
                        standInput.SiteIndex = (short)Utils.ConvertToInt(_mdbReader["Site Index"].ToString());
                        standInput.Species = _mdbReader["Species"].ToString();
                        standInput.StandsId = currentStand.StandsId;
                        standInput.Status = _mdbReader["Status"].ToString();
                        standInput.Stocking = (short)Utils.ConvertToInt(_mdbReader["Stocking"].ToString());
                        standInput.TotalHeight = Utils.ConvertToDouble(_mdbReader["Total Height"].ToString());
                        standInput.TreesPerAcre = Utils.ConvertToDouble(_mdbReader["Tpa"].ToString());
                        //ctx.StandInput.Add(standInput);
                        ++count;
                        _addStandInput.Add(standInput);
                        if (count > 100)
                        {
                            count = 0;
                            ctx.StandInputs.AddRange(_addStandInput);
                            _addStandInput.Clear();
                        }
                    }
                }
            }
            ctx.StandInputs.AddRange(_addStandInput);
            ctx.SaveChanges();
            _addStandInput.Clear();
            //ctx.Dispose();
        }

        //private void ImportSection()
        //{
        //    /*
        //    ProjectCollection coll = new ProjectCollection();
        //    coll.LoadAll();

        //    while (mdbReader.Read())
        //    {
        //        Project.DAL.Project cs = coll.AddNew();

        //        cs.Project = mdbReader["Project"].ToString();
        //        if (!string.IsNullOrEmpty(mdbReader["Customer Name"].ToString()))
        //            cs.CustomerName = mdbReader["Customer Name"].ToString();
        //        else
        //            cs.CustomerName = string.Empty;
        //        if (!string.IsNullOrEmpty(mdbReader["Location"].ToString()))
        //            cs.Location = mdbReader["Location"].ToString();
        //        else
        //            cs.Location = string.Empty;
        //        cs.MapData = string.Empty;
        //        cs.MapProject = string.Empty;
        //        cs.Meridian = string.Empty;
        //    }
        //    coll.Save();
        //     */
        //}

        private void ImportPlot(ref ProjectDbContext ctx)
        {
            //ProjectDbContext ctx = new ProjectDbContext(ProjectBLL.GetConnectionString(pDataSource));
            //List<Plot> addPlots = new List<Plot>();
            _addPlots = new List<Plot>();
            Stand currentStand = null;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            int count = 0;
            //ctx.Configuration.AutoDetectChangesEnabled = false;

            while (_mdbReader.Read())
            {
                try
                {
                    string sTract = _mdbReader["Tract"].ToString();
                    string sStand = _mdbReader["Type"].ToString();
                    string sTownship = _mdbReader["Township"].ToString();
                    string sRange = _mdbReader["Range"].ToString();
                    string sSection = _mdbReader["Section"].ToString();
                    currentKey = string.Format("{0}{1}{2}{3}{4}", sTract, sStand, sTownship, sRange, sSection);
                    if (saveKey != currentKey)
                    {
                        //currentStand = ProjectBLL.GetStand(pDataSource, sTract, sStand, sTownship, sRange, sSection);
                        currentStand = ctx.Stands.FirstOrDefault(p => p.TractName == sTract && p.StandName == sStand && p.Township == sTownship && p.Range == sRange && p.Section == sSection);
                        saveKey = currentKey;
                    }

                    if (currentStand != null)
                    {
                        Plot cs = new Plot();
                        cs.StandsId = currentStand.StandsId;
                        //cs.Stand = currentStand;
                        cs.Notes = string.Empty;
                        //cs.PlotDate = Convert.ToDateTime(string.Format("{0}-{1}-{2}", mdbReader["Month"].ToString(), mdbReader["Day"].ToString(), mdbReader["Year"].ToString()));
                        cs.TreatmentDisplayCode = string.Empty;
                        cs.UserPlotNumber = _mdbReader["PlotNo"].ToString().Trim();
                        if (!string.IsNullOrEmpty(_mdbReader["Cruiser"].ToString()))
                            cs.Cruiser = _mdbReader["Cruiser"].ToString().Trim();
                        else
                            cs.Cruiser = string.Empty;
                        if (!string.IsNullOrEmpty(_mdbReader["CoordX"].ToString()))
                            cs.X = float.Parse(_mdbReader["CoordX"].ToString());
                        else
                            cs.X = 0;
                        if (!string.IsNullOrEmpty(_mdbReader["CoordY"].ToString()))
                            cs.Y = float.Parse(_mdbReader["CoordY"].ToString());
                        else
                            cs.Y = 0;
                        if (!string.IsNullOrEmpty(_mdbReader["CoordZ"].ToString()))
                            cs.Z = float.Parse(_mdbReader["CoordZ"].ToString());
                        else
                            cs.Z = 0;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Bearing"].ToString()))
                        //    cs.Bearing = mdbReader["Bearing"].ToString();
                        //else
                        //    cs.Bearing = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Source"].ToString()))
                        //    cs.Source = mdbReader["Source"].ToString();
                        //else
                        //    cs.Source = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Comments"].ToString()))
                        //    cs.Comments = mdbReader["Comments"].ToString();
                        //else
                        //    cs.Comments = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Weyco D4h"].ToString()))
                        //    cs.WeycoD4h = mdbReader["Weyco D4h"].ToString();
                        //else
                        //    cs.WeycoD4h = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["EngMet Code"].ToString()))
                        //    cs.EngMetCode = mdbReader["EngMet Code"].ToString();
                        //else
                        //    cs.EngMetCode = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Distance"].ToString()))
                        //    cs.Distance = Int16.Parse(mdbReader["Distance"].ToString());
                        //else
                        //    cs.Distance = 0;
                        if (!string.IsNullOrEmpty(_mdbReader["Slope"].ToString()))
                            cs.Slope = _mdbReader["Slope"].ToString();
                        else
                            cs.Slope = string.Empty;
                        if (!string.IsNullOrEmpty(_mdbReader["Aspect"].ToString()))
                            cs.Aspect = _mdbReader["Aspect"].ToString();
                        else
                            cs.Aspect = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Desc"].ToString()))
                        //    cs.Description = mdbReader["Desc"].ToString();
                        //else
                        //    cs.Description = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Ind Species"].ToString()))
                        //    cs.IndSpecies = mdbReader["Ind Species"].ToString();
                        //else
                        //    cs.IndSpecies = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["Cruise Flag"].ToString()))
                        //    cs.CruiseFlag = mdbReader["Cruise Flag"].ToString();
                        //else
                        //    cs.CruiseFlag = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //if (!string.IsNullOrEmpty(mdbReader["CoordSys"].ToString()))
                        //    cs.CoordSys = mdbReader["CoordSys"].ToString();
                        //else
                        //    cs.CoordSys = string.Empty;
                        //ctx.Plot.Add(cs);
                        //addPlots.Add(cs);
                        //++count;
                        //ctx = PlotAddToContext(pDataSource, ctx, cs, count, 100, true);
                        ++count;
                        _addPlots.Add(cs);
                        if (count > 100)
                        {
                            count = 0;
                            ctx.Plots.AddRange(_addPlots);
                            _addPlots.Clear();
                        }
                    }
                }
                catch (NotSupportedException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    XtraMessageBox.Show("Unhandled Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            ctx.Plots.AddRange(_addPlots);
            ctx.SaveChanges();
            _addPlots.Clear();
            //ctx.Dispose();
        }

        private void ImportTree(ref ProjectDbContext ctx)
        {
            _addTrees = new List<Tree>();
            //_addSegments = new List<Treesegment>();
            Stand currentStand = null;
            Plot currentPlot = null;
            string currentStandKey;
            string saveStandKey = string.Empty;
            string currentPlotKey;
            string savePlotKey = string.Empty;
            int count = 0;
            //int segCount = 0;
            int stands = 0;

            while (_mdbReader.Read())
            {
                string sTract = _mdbReader["Tract"].ToString();
                string sStand = _mdbReader["Type"].ToString();
                string sTownship = _mdbReader["Township"].ToString();
                string sRange = _mdbReader["Range"].ToString();
                string sSection = _mdbReader["Section"].ToString();
                string sPlot = _mdbReader["PlotNo"].ToString();
                currentStandKey = string.Format("{0}{1}{2}{3}{4}", sTract, sStand, sTownship, sRange, sSection);
                if (saveStandKey != currentStandKey)
                {
                    currentStand = ctx.Stands.FirstOrDefault(p => p.TractName == sTract && p.StandName == sStand && p.Township == sTownship && p.Range == sRange && p.Section == sSection);
                    //currentStand = ProjectBLL.GetStand(pDataSource, sTract, sStand, sTownship, sRange, sSection);
                    //currentStand = ctx.Stands.FirstOrDefault(s => s.TractName == sTract && s.StandName == sStand && s.Township == sTownship && s.Range == sRange && s.Section == sSection);
                    saveStandKey = currentStandKey;
                    SplashScreenManager.Default.SetWaitFormDescription(string.Format("Importing Trees for Tract: {0} and Stand: {1} : {2}", sTract, sStand, ++stands));
                }
                if (currentStand != null)
                {
                    Project.DAL.Project projectRec = ProjectBLL.GetProject(ref ctx);
                    //Calc calc = new Calc(ref ctx, projectRec, ref currentStand);
                    Calc.SetCalcContextProjectStand(ref ctx, projectRec, ref currentStand);
                    
                    currentPlotKey = sPlot;
                    if (savePlotKey != currentPlotKey)
                    {
                        currentPlot = ctx.Plots.FirstOrDefault(p => p.StandsId == currentStand.StandsId && p.UserPlotNumber == sPlot);
                        savePlotKey = currentPlotKey;
                    }
                    if (currentPlot != null)
                    {
                        Tree cs = new Tree();
                        cs.PlotId = currentPlot.PlotsId;
                        cs.StandsId = currentStand.StandsId;
                        cs.PlotNumber = _mdbReader["PlotNo"].ToString().Trim();
                        cs.GroupPlot = _mdbReader["PlotNo"].ToString().Trim();
                        cs.AgeCode = (byte)Utils.ConvertToInt(_mdbReader["AgeCode"].ToString());
                        cs.BoleHeight = (short)Utils.ConvertToInt(_mdbReader["BoleLength"].ToString());
                        cs.Dbh = Utils.ConvertToDouble(_mdbReader["D4h"].ToString());
                        cs.FormPoint = (byte)Utils.ConvertToInt(_mdbReader["FormPoint"].ToString());
                        cs.PlotFactorInput = _mdbReader["PrsmFactor"].ToString().Trim();
                        cs.ReportedFormFactor = (byte)Utils.ConvertToInt(_mdbReader["FormFactor"].ToString());
                        cs.SpeciesAbbreviation = _mdbReader["Species"].ToString().Trim();
                        if (string.IsNullOrEmpty(_mdbReader["TDF"].ToString().Trim()))
                            cs.TopDiameterFractionCode = string.Empty;
                        else
                        {
                            if (Utils.ConvertToInt(_mdbReader["TDF"].ToString().Trim()) > 0)
                            {
                                if (_mdbReader["TDF"].ToString().Trim().Length == 1)
                                    cs.TopDiameterFractionCode = _mdbReader["TDF"].ToString().Trim() + "0";
                                else
                                    cs.TopDiameterFractionCode = _mdbReader["TDF"].ToString().Trim();
                            }
                            else
                                cs.TopDiameterFractionCode = _mdbReader["TDF"].ToString().Trim();
                        }
                        if (string.IsNullOrEmpty(_mdbReader["Tot Height"].ToString()))
                            cs.TotalHeight = 0;
                        else
                            cs.TotalHeight = (short)Utils.ConvertToInt(_mdbReader["Tot Height"].ToString());
                        if (string.IsNullOrEmpty(_mdbReader["TreeCount"].ToString()))
                            cs.TreeCount = 1;
                        else
                            cs.TreeCount = (byte)Utils.ConvertToInt(_mdbReader["TreeCount"].ToString());
                        cs.TreeNumber = Utils.ConvertToInt(_mdbReader["TreeNo"].ToString().Trim());
                        cs.GroupTree = Utils.ConvertToInt(_mdbReader["TreeNo"].ToString().Trim());
                        cs.TreeStatusDisplayCode = _mdbReader["Status"].ToString().Trim();
                        cs.TreeType = _mdbReader["RecordCode"].ToString();
                        cs.CrownPositionDisplayCode = _mdbReader["T1"].ToString().Trim();
                        cs.CrownRatioDisplayCode = _mdbReader["T2"].ToString().Trim();
                        cs.VigorDisplayCode = _mdbReader["T3"].ToString().Trim();
                        cs.DamageDisplayCode = _mdbReader["T4"].ToString().Trim();
                        cs.UserDefinedDisplayCode = _mdbReader["T5"].ToString().Trim();
                        if (!string.IsNullOrEmpty(_mdbReader["Dbh"].ToString()))
                            cs.Dbh = Utils.ConvertToDouble(_mdbReader["Dbh"].ToString());

                        ProcessSegments(ref cs);

                        cs.Age = Utils.GetAge(ref ctx, currentStand, cs.AgeCode.ToString());
                        Utils.DoCalcForTree(ref ctx, ref cs, ref currentPlot, ref currentStand, projectRec);

                        cs.CalcTotalHeight = (short)Calc.total_tree_height;
                        cs.BasalArea = Calc.basal_area;
                        cs.TreesPerAcre = Calc.trees_per_acre;
                        cs.LogsPerAcre = Calc.logs_pa;
                        cs.Ao = Calc.ao;
                        cs.Bark = Calc.bark;

                        cs.TotalBdFtGrossVolume = Calc.tree_scribner_gross;
                        cs.TotalBdFtNetVolume = Calc.tree_scribner_net;
                        cs.TotalMbf = Calc.tree_mbf;

                        cs.TotalCuFtGrossVolume = Calc.tree_cubic_gross;
                        cs.TotalCuFtNetVolume = Calc.tree_cubic_net;
                        cs.TotalCcf = Calc.tree_ccf;

                        cs.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        cs.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        cs.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        cs.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                        cs.CalcTotalHtUsedYn = "N";
                        if (cs.BoleHeight > 0)
                        {
                            if (cs.TotalHeight == null || cs.TotalHeight == 0)
                            {
                                cs.TotalHeight = cs.CalcTotalHeight;
                                cs.CalcTotalHtUsedYn = "Y";
                            }
                        }
                        ProjectBLL.SetTreeType(ref cs);
                        if (cs.TreeType == "C")
                        {
                            cs.FormPoint = 0;
                            cs.TotalHeight = 0;
                            cs.CalcTotalHtUsedYn = "N";
                        }

                        //cs.TreesPerAcre = Calc.trees_pa;
                        //cs.BasalArea = Calc.basal_area;
                        //cs.LogsPerAcre = Calc.logs_pa;

                        //cs.TotalCcf = Calc.tree_ccf;
                        //cs.TotalMbf = Calc.tree_mbf;
                        //cs.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        //cs.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        //cs.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        //cs.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                        ++count;
                        _addTrees.Add(cs);
                        if (count > 100)
                        {
                            count = 0;
                            ctx.Trees.AddRange(_addTrees);
                            _addTrees.Clear();
                        }
                        //for (int i = 0; i < 12; i++)
                        //{
                        //    if (Calc.seg[i].len > 0) // was scribner_gross
                        //    {
                        //        Treesegment s = new Treesegment();

                        //        s.StandsId = cs.StandsId;
                        //        s.PlotId = cs.PlotId;
                        //        s.TreesId = cs.TreesId;
                        //        //s.Tree = cs;
                        //        s.GroupPlotNumber = cs.PlotNumber;
                        //        s.GroupTreeNumber = cs.TreeNumber;
                        //        s.SegmentNumber = i + 1;

                        //        s.CalcAccumulatedLength = Calc.seg[i].cum_len;
                        //        s.Comments = Calc.seg[i].comments;
                        //        s.LogValue = 0;
                        //        s.Species = cs.SpeciesAbbreviation;
                        //        s.TreeBasalArea = Calc.basal_area;
                        //        s.TreeTreesPerAcre = Calc.trees_per_acre;

                        //        s.CalcLen = Calc.seg[i].len;
                        //        s.CalcTopDia = (double) Math.Round((decimal) Calc.seg[i].tdia, 2);
                        //        s.CalcButtDia = (double) Math.Round((decimal) Calc.seg[i].bdia, 2);
                        //        s.CubicGrossVolume = Calc.seg[i].cubic_gross;
                        //        s.CubicNetVolume = Calc.seg[i].cubic_net;
                        //        s.ScribnerGrossVolume = Calc.seg[i].scribner_gross;
                        //        s.ScribnerNetVolume = Calc.seg[i].scribner_net;
                        //        s.CubicGrossVolumePerAce = Calc.seg[i].cubic_gross_pa;
                        //        s.CubicNetVolumePerAce = Calc.seg[i].cubic_net_pa;
                        //        s.ScribnerGrossVolumePerAce = Calc.seg[i].scribner_gross_pa;
                        //        s.ScribnerNetVolumePerAce = Calc.seg[i].scribner_net_pa;
                        //        s.Ccf = Calc.seg[i].ccf;
                        //        s.Mbf = Calc.seg[i].mbf;
                        //        s.SegmentNumber = i + 1;
                        //        s.Species = cs.SpeciesAbbreviation.Trim();
                        //        s.Age = cs.Age;
                        //        s.Ao = cs.Ao;
                        //        s.Bark = cs.Bark;
                        //        s.BoleHeight = cs.BoleHeight;
                        //        s.CalcTotalHtUsedYn = cs.CalcTotalHtUsedYn;
                        //        s.CrownPosition = cs.CrownPositionDisplayCode;
                        //        s.CrownRatio = cs.CrownRatioDisplayCode;
                        //        s.Damage = cs.DamageDisplayCode;
                        //        s.Dbh = cs.Dbh;
                        //        s.FormFactor = cs.ReportedFormFactor;
                        //        s.FormPoint = cs.FormPoint;
                        //        s.PlotFactorInput = cs.PlotFactorInput;
                        //        s.PlotNumber = cs.PlotNumber;
                        //        s.Tdf = cs.TopDiameterFractionCode;
                        //        s.TotalHeight = cs.TotalHeight;
                        //        s.TreeCount = cs.TreeCount;
                        //        s.TreeNumber = (short) cs.TreeNumber;
                        //        s.TreeStatusDisplayCode = cs.TreeStatusDisplayCode;
                        //        s.UserDefined = cs.UserDefinedDisplayCode;
                        //        s.Vigor = cs.VigorDisplayCode;
                        //        switch (i)
                        //        {
                        //            case 0:
                        //                if (cs.BdFtLd1 != null)
                        //                    s.BdFtLd = cs.BdFtLd1;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd1 != null)
                        //                    s.BdFtDd = cs.BdFtDd1;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd1 != null)
                        //                    s.BdFtPd = cs.BdFtPd1;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd1 != null)
                        //                    s.CuFtDd = cs.CuFtDd1;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd1 != null)
                        //                    s.CuFtLd = cs.CuFtLd1;
                        //                else
                        //                    s.CuFtLd = cs.CuFtLd1;
                        //                if (cs.Length1 != null)
                        //                    s.Length = cs.Length1.Trim();
                        //                else
                        //                    s.Length = cs.Length1.Trim();
                        //                if (cs.Sort1 != null)
                        //                    s.SortCode = cs.Sort1.Trim();
                        //                else
                        //                    s.SortCode = cs.Sort1.Trim();
                        //                if (cs.Grade1 != null)
                        //                    s.GradeCode = cs.Grade1.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 1:
                        //                if (cs.BdFtLd2 != null)
                        //                    s.BdFtLd = cs.BdFtLd2;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd2 != null)
                        //                    s.BdFtDd = cs.BdFtDd2;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd2 != null)
                        //                    s.BdFtPd = cs.BdFtPd2;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd2 != null)
                        //                    s.CuFtDd = cs.CuFtDd2;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd2 != null)
                        //                    s.CuFtLd = cs.CuFtLd2;
                        //                else
                        //                    s.CuFtLd = 01;
                        //                if (cs.Length2 != null)
                        //                    s.Length = cs.Length2.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort2 != null)
                        //                    s.SortCode = cs.Sort2.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade2 != null)
                        //                    s.GradeCode = cs.Grade2.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 2:
                        //                if (cs.BdFtLd3 != null)
                        //                    s.BdFtLd = cs.BdFtLd3;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd3 != null)
                        //                    s.BdFtDd = cs.BdFtDd3;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd3 != null)
                        //                    s.BdFtPd = cs.BdFtPd3;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd3 != null)
                        //                    s.CuFtDd = cs.CuFtDd3;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd3 != null)
                        //                    s.CuFtLd = cs.CuFtLd3;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length3 != null)
                        //                    s.Length = cs.Length3.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort3 != null)
                        //                    s.SortCode = cs.Sort3.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade3 != null)
                        //                    s.GradeCode = cs.Grade3.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 3:
                        //                if (cs.BdFtLd4 != null)
                        //                    s.BdFtLd = cs.BdFtLd4;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd4 != null)
                        //                    s.BdFtDd = cs.BdFtDd4;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd4 != null)
                        //                    s.BdFtPd = cs.BdFtPd4;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd4 != null)
                        //                    s.CuFtDd = cs.CuFtDd4;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd4 != null)
                        //                    s.CuFtLd = cs.CuFtLd4;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length4 != null)
                        //                    s.Length = cs.Length4.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort4 != null)
                        //                    s.SortCode = cs.Sort4.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade4 != null)
                        //                    s.GradeCode = cs.Grade4.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 4:
                        //                if (cs.BdFtLd5 != null)
                        //                    s.BdFtLd = cs.BdFtLd5;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd5 != null)
                        //                    s.BdFtDd = cs.BdFtDd5;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd5 != null)
                        //                    s.BdFtPd = cs.BdFtPd5;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd5 != null)
                        //                    s.CuFtDd = cs.CuFtDd5;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd5 != null)
                        //                    s.CuFtLd = cs.CuFtLd5;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length5 != null)
                        //                    s.Length = cs.Length5.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort5 != null)
                        //                    s.SortCode = cs.Sort5.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade5 != null)
                        //                    s.GradeCode = cs.Grade5.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 5:
                        //                if (cs.BdFtLd6 != null)
                        //                    s.BdFtLd = cs.BdFtLd6;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd6 != null)
                        //                    s.BdFtDd = cs.BdFtDd6;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd6 != null)
                        //                    s.BdFtPd = cs.BdFtPd6;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd6 != null)
                        //                    s.CuFtDd = cs.CuFtDd6;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd6 != null)
                        //                    s.CuFtLd = cs.CuFtLd6;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length6 != null)
                        //                    s.Length = cs.Length6.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort6 != null)
                        //                    s.SortCode = cs.Sort6.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade6 != null)
                        //                    s.GradeCode = cs.Grade6.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 6:
                        //                if (cs.BdFtLd7 != null)
                        //                    s.BdFtLd = cs.BdFtLd7;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd7 != null)
                        //                    s.BdFtDd = cs.BdFtDd7;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd7 != null)
                        //                    s.BdFtPd = cs.BdFtPd7;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd7 != null)
                        //                    s.CuFtDd = cs.CuFtDd7;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd7 != null)
                        //                    s.CuFtLd = cs.CuFtLd7;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length7 != null)
                        //                    s.Length = cs.Length7.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort7 != null)
                        //                    s.SortCode = cs.Sort7.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade7 != null)
                        //                    s.GradeCode = cs.Grade7.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 7:
                        //                if (cs.BdFtLd8 != null)
                        //                    s.BdFtLd = cs.BdFtLd8;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd8 != null)
                        //                    s.BdFtDd = cs.BdFtDd8;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd8 != null)
                        //                    s.BdFtPd = cs.BdFtPd8;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd8 != null)
                        //                    s.CuFtDd = cs.CuFtDd8;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd8 != null)
                        //                    s.CuFtLd = cs.CuFtLd8;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length8 != null)
                        //                    s.Length = cs.Length8.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort8 != null)
                        //                    s.SortCode = cs.Sort8.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade8 != null)
                        //                    s.GradeCode = cs.Grade8.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 8:
                        //                if (cs.BdFtLd9 != null)
                        //                    s.BdFtLd = cs.BdFtLd9;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd9 != null)
                        //                    s.BdFtDd = cs.BdFtDd9;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd9 != null)
                        //                    s.BdFtPd = cs.BdFtPd9;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd9 != null)
                        //                    s.CuFtDd = cs.CuFtDd9;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd9 != null)
                        //                    s.CuFtLd = cs.CuFtLd9;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length9 != null)
                        //                    s.Length = cs.Length9.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort9 != null)
                        //                    s.SortCode = cs.Sort9.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade9 != null)
                        //                    s.GradeCode = cs.Grade9.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 9:
                        //                if (cs.BdFtLd10 != null)
                        //                    s.BdFtLd = cs.BdFtLd10;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd10 != null)
                        //                    s.BdFtDd = cs.BdFtDd10;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd10 != null)
                        //                    s.BdFtPd = cs.BdFtPd10;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd10 != null)
                        //                    s.CuFtDd = cs.CuFtDd10;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd10 != null)
                        //                    s.CuFtLd = cs.CuFtLd10;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length10 != null)
                        //                    s.Length = cs.Length10.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort10 != null)
                        //                    s.SortCode = cs.Sort10.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade10 != null)
                        //                    s.GradeCode = cs.Grade10.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 10:
                        //                if (cs.BdFtLd11 != null)
                        //                    s.BdFtLd = cs.BdFtLd11;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd11 != null)
                        //                    s.BdFtDd = cs.BdFtDd11;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd11 != null)
                        //                    s.BdFtPd = cs.BdFtPd11;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd11 != null)
                        //                    s.CuFtDd = cs.CuFtDd11;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd11 != null)
                        //                    s.CuFtLd = cs.CuFtLd11;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length11 != null)
                        //                    s.Length = cs.Length11.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort11 != null)
                        //                    s.SortCode = cs.Sort11.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade11 != null)
                        //                    s.GradeCode = cs.Grade11.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //            case 11:
                        //                if (cs.BdFtLd12 != null)
                        //                    s.BdFtLd = cs.BdFtLd12;
                        //                else
                        //                    s.BdFtLd = 0;
                        //                if (cs.BdFtDd12 != null)
                        //                    s.BdFtDd = cs.BdFtDd12;
                        //                else
                        //                    s.BdFtDd = 0;
                        //                if (cs.BdFtPd12 != null)
                        //                    s.BdFtPd = cs.BdFtPd12;
                        //                else
                        //                    s.BdFtPd = string.Empty;
                        //                if (cs.CuFtDd12 != null)
                        //                    s.CuFtDd = cs.CuFtDd12;
                        //                else
                        //                    s.CuFtDd = 0;
                        //                if (cs.CuFtLd12 != null)
                        //                    s.CuFtLd = cs.CuFtLd12;
                        //                else
                        //                    s.CuFtLd = 0;
                        //                if (cs.Length12 != null)
                        //                    s.Length = cs.Length12.Trim();
                        //                else
                        //                    s.Length = string.Empty;
                        //                if (cs.Sort12 != null)
                        //                    s.SortCode = cs.Sort12.Trim();
                        //                else
                        //                    s.SortCode = string.Empty;
                        //                if (cs.Grade12 != null)
                        //                    s.GradeCode = cs.Grade12.Trim();
                        //                else
                        //                    s.GradeCode = string.Empty;
                        //                break;
                        //        }
                        //        if (s.SortCode == "0" || s.GradeCode == "0")
                        //            s.Comments = "Cull Segment";
                        //        //addSegments.Add(s);
                        //        //ctx.Treesegments.Add(s);
                        //        //ctx = SegmentAddToContext(pDataSource, ctx, s, segCount, 100, true);
                        //        ++segCount;
                        //        _addSegments.Add(s);
                        //        if (segCount > 100)
                        //        {
                        //            segCount = 0;
                        //            ctx.Treesegments.AddRange(_addSegments);
                        //            _addSegments.Clear();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (i == 0)
                        //        { 
                        //            // no segment data
                        //            Treesegment s = new Treesegment();

                        //            s.StandsId = cs.StandsId;
                        //            s.PlotId = cs.PlotId;
                        //            s.TreesId = cs.TreesId;
                        //            //s.Tree = cs;
                        //            s.GroupPlotNumber = cs.PlotNumber;
                        //            s.GroupTreeNumber = cs.TreeNumber;
                        //            s.SegmentNumber = 0;

                        //            s.CalcAccumulatedLength = 0;
                        //            s.Comments = string.Empty;
                        //            s.LogValue = 0;
                        //            s.Species = cs.SpeciesAbbreviation;
                        //            s.TreeBasalArea = Calc.basal_area;
                        //            s.TreeTreesPerAcre = Calc.trees_per_acre;

                        //            s.CalcLen = 0;
                        //            s.CalcTopDia = 0;
                        //            s.CalcButtDia = 0;
                        //            s.CubicGrossVolume = 0;
                        //            s.CubicNetVolume = 0;
                        //            s.ScribnerGrossVolume = 0;
                        //            s.ScribnerNetVolume = 0;
                        //            s.CubicGrossVolumePerAce = 0;
                        //            s.CubicNetVolumePerAce = 0;
                        //            s.ScribnerGrossVolumePerAce = 0;
                        //            s.ScribnerNetVolumePerAce = 0;
                        //            s.Ccf = 0;
                        //            s.Mbf = 0;
                        //            s.Species = cs.SpeciesAbbreviation.Trim();
                        //            s.Age = cs.Age;
                        //            s.Ao = cs.Ao;
                        //            s.Bark = cs.Bark;
                        //            s.BoleHeight = cs.BoleHeight;
                        //            s.CalcTotalHtUsedYn = "N";
                        //            s.CrownPosition = cs.CrownPositionDisplayCode;
                        //            s.CrownRatio = cs.CrownRatioDisplayCode;
                        //            s.Damage = cs.DamageDisplayCode;
                        //            s.Dbh = cs.Dbh;
                        //            s.FormFactor = cs.ReportedFormFactor;
                        //            s.FormPoint = cs.FormPoint;
                        //            s.PlotFactorInput = cs.PlotFactorInput;
                        //            s.PlotNumber = cs.PlotNumber;
                        //            s.Tdf = cs.TopDiameterFractionCode;
                        //            s.TotalHeight = cs.TotalHeight;
                        //            s.TreeCount = cs.TreeCount;
                        //            s.TreeNumber = (short) cs.TreeNumber;
                        //            s.TreeStatusDisplayCode = cs.TreeStatusDisplayCode;
                        //            s.UserDefined = cs.UserDefinedDisplayCode;
                        //            s.Vigor = cs.VigorDisplayCode;
                        //            s.BdFtLd = 0;
                        //            s.BdFtDd = 0;
                        //            s.BdFtPd = string.Empty;
                        //            s.CuFtDd = 0;
                        //            s.CuFtLd = cs.CuFtLd1;
                        //            s.Length = cs.Length1.Trim();
                        //            s.SortCode = cs.Sort1.Trim();
                        //            s.GradeCode = string.Empty;
                        //            ++segCount;
                        //            _addSegments.Add(s);
                        //            if (segCount > 100)
                        //            {
                        //                segCount = 0;
                        //                ctx.Treesegments.AddRange(_addSegments);
                        //                _addSegments.Clear();
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
            }
            ctx.Trees.AddRange(_addTrees);
            //ctx.Treesegments.AddRange(_addSegments);
            _addTrees.Clear();
            //_addSegments.Clear();
            ctx.SaveChanges();
        }

        private void ProcessTreeSegments(ref ProjectDbContext ctx)
        {
            //_addTrees = new List<Tree>();
            _addSegments = new List<Treesegment>();
            Stand currentStand = null;
            Plot currentPlot = null;
            string currentStandKey;
            string saveStandKey = string.Empty;
            string currentPlotKey;
            string savePlotKey = string.Empty;
            int count = 0;
            int segCount = 0;
            int stands = 0;

            Project.DAL.Project projectRec = ProjectBLL.GetProject(ref ctx);
            List<Stand> standsToProcess = ctx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (Stand standItem in standsToProcess)
            {
                Stand standRec = standItem;
                SplashScreenManager.Default.SetWaitFormDescription(string.Format("Processing Tree Segments for Tract: {0} and Stand: {1} : {2}", 
                    standRec.TractName, standRec.StandName, ++stands));
                Calc.SetCalcContextProjectStand(ref ctx, projectRec, ref standRec);
                List<Plot> plotsToProcess = ctx.Plots.OrderBy(t => t.UserPlotNumber).Where(t => t.StandsId == standItem.StandsId).ToList();
                foreach (Plot plotItem in plotsToProcess)
                {
                    Plot plotRec = plotItem;
                    List<Tree> treesToProcess = ctx.Trees.OrderBy(t => t.TreeNumber).Where(t => t.PlotId == plotItem.PlotsId).ToList();
                    foreach (Tree treeItem in treesToProcess)
                    {
                        Tree treeRec = treeItem;
                        Utils.DoCalcForTree(ref ctx, ref treeRec, ref plotRec, ref standRec, projectRec);

                        //treeRec.Age = Utils.GetAge(standRec, treeRec.AgeCode.ToString());

                        //treeRec.CalcTotalHeight = (short)Calc.total_tree_height;
                        //treeRec.BasalArea = Calc.basal_area;
                        //treeRec.TreesPerAcre = Calc.trees_per_acre;
                        //treeRec.LogsPerAcre = Calc.logs_pa;
                        //treeRec.Ao = Calc.ao;
                        //treeRec.Bark = Calc.bark;

                        //treeRec.TotalBdFtGrossVolume = Calc.tree_scribner_gross;
                        //treeRec.TotalBdFtNetVolume = Calc.tree_scribner_net;
                        //treeRec.TotalMbf = Calc.tree_mbf;

                        //treeRec.TotalCuFtGrossVolume = Calc.tree_cubic_gross;
                        //treeRec.TotalCuFtNetVolume = Calc.tree_cubic_net;
                        //treeRec.TotalCcf = Calc.tree_ccf;

                        //treeRec.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        //treeRec.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        //treeRec.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        //treeRec.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                        //treeRec.CalcTotalHtUsedYn = "N";
                        //if (treeRec.TotalHeight == null || treeRec.TotalHeight == 0)
                        //{
                        //    treeRec.TotalHeight = treeRec.CalcTotalHeight;
                        //    treeRec.CalcTotalHtUsedYn = "Y";
                        //}
                        //ProjectBLL.SetTreeType(ref treeRec);

                        ////treeRec.TreesPerAcre = Calc.trees_pa;
                        ////treeRec.BasalArea = Calc.basal_area;
                        ////treeRec.LogsPerAcre = Calc.logs_pa;

                        ////treeRec.TotalCcf = Calc.tree_ccf;
                        ////treeRec.TotalMbf = Calc.tree_mbf;
                        ////treeRec.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        ////treeRec.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        ////treeRec.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        ////treeRec.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                        for (int i = 0; i < 12; i++)
                        {
                            if (Calc.seg[i].len > 0) // was scribner_gross
                            {
                                Treesegment s = new Treesegment();

                                s.StandsId = treeRec.StandsId;
                                s.PlotId = treeRec.PlotId;
                                s.TreesId = treeRec.TreesId;
                                //s.Tree = cs;
                                s.GroupPlotNumber = treeRec.PlotNumber;
                                s.GroupTreeNumber = treeRec.TreeNumber;
                                s.SegmentNumber = i + 1;
                                s.TreeType = treeRec.TreeType;

                                s.CalcAccumulatedLength = Calc.seg[i].cum_len;
                                s.Comments = Calc.seg[i].comments;
                                s.LogValue = 0;
                                s.Species = treeRec.SpeciesAbbreviation;
                                s.TreeBasalArea = Calc.basal_area;
                                s.TreeTreesPerAcre = Calc.trees_per_acre;

                                s.CalcLen = Calc.seg[i].len;
                                s.CalcTopDia = (double)Math.Round((decimal)Calc.seg[i].tdia, 2);
                                s.CalcButtDia = (double)Math.Round((decimal)Calc.seg[i].bdia, 2);
                                s.CubicGrossVolume = Calc.seg[i].cubic_gross;
                                s.CubicNetVolume = Calc.seg[i].cubic_net;
                                s.ScribnerGrossVolume = Calc.seg[i].scribner_gross;
                                s.ScribnerNetVolume = Calc.seg[i].scribner_net;
                                s.CubicGrossVolumePerAce = Calc.seg[i].cubic_gross_pa;
                                s.CubicNetVolumePerAce = Calc.seg[i].cubic_net_pa;
                                s.ScribnerGrossVolumePerAce = Calc.seg[i].scribner_gross_pa;
                                s.ScribnerNetVolumePerAce = Calc.seg[i].scribner_net_pa;
                                s.Ccf = Calc.seg[i].ccf;
                                s.Mbf = Calc.seg[i].mbf;
                                s.Species = treeRec.SpeciesAbbreviation.Trim();
                                s.Age = treeRec.Age;
                                s.Ao = treeRec.Ao;
                                s.Bark = treeRec.Bark;
                                s.BoleHeight = treeRec.BoleHeight;
                                s.CalcTotalHtUsedYn = treeRec.CalcTotalHtUsedYn;
                                s.CrownPosition = treeRec.CrownPositionDisplayCode;
                                s.CrownRatio = treeRec.CrownRatioDisplayCode;
                                s.Damage = treeRec.DamageDisplayCode;
                                s.Dbh = treeRec.Dbh;
                                s.FormFactor = treeRec.ReportedFormFactor;
                                s.FormPoint = treeRec.FormPoint;
                                s.PlotFactorInput = treeRec.PlotFactorInput;
                                s.PlotNumber = treeRec.PlotNumber;
                                s.Tdf = treeRec.TopDiameterFractionCode;
                                s.TotalHeight = treeRec.TotalHeight;
                                s.TreeCount = treeRec.TreeCount;
                                s.TreeNumber = (short)treeRec.TreeNumber;
                                s.TreeStatusDisplayCode = treeRec.TreeStatusDisplayCode;
                                s.UserDefined = treeRec.UserDefinedDisplayCode;
                                s.Vigor = treeRec.VigorDisplayCode;
                                switch (i)
                                {
                                    case 0:
                                        if (treeRec.BdFtLd1 != null)
                                            s.BdFtLd = treeRec.BdFtLd1;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd1 != null)
                                            s.BdFtDd = treeRec.BdFtDd1;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd1 != null)
                                            s.BdFtPd = treeRec.BdFtPd1;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd1 != null)
                                            s.CuFtDd = treeRec.CuFtDd1;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd1 != null)
                                            s.CuFtLd = treeRec.CuFtLd1;
                                        else
                                            s.CuFtLd = treeRec.CuFtLd1;
                                        if (treeRec.Length1 != null)
                                            s.Length = treeRec.Length1.Trim();
                                        else
                                            s.Length = treeRec.Length1.Trim();
                                        if (treeRec.Sort1 != null)
                                            s.SortCode = treeRec.Sort1.Trim();
                                        else
                                            s.SortCode = treeRec.Sort1.Trim();
                                        if (treeRec.Grade1 != null)
                                            s.GradeCode = treeRec.Grade1.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 1:
                                        if (treeRec.BdFtLd2 != null)
                                            s.BdFtLd = treeRec.BdFtLd2;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd2 != null)
                                            s.BdFtDd = treeRec.BdFtDd2;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd2 != null)
                                            s.BdFtPd = treeRec.BdFtPd2;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd2 != null)
                                            s.CuFtDd = treeRec.CuFtDd2;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd2 != null)
                                            s.CuFtLd = treeRec.CuFtLd2;
                                        else
                                            s.CuFtLd = 01;
                                        if (treeRec.Length2 != null)
                                            s.Length = treeRec.Length2.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort2 != null)
                                            s.SortCode = treeRec.Sort2.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade2 != null)
                                            s.GradeCode = treeRec.Grade2.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 2:
                                        if (treeRec.BdFtLd3 != null)
                                            s.BdFtLd = treeRec.BdFtLd3;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd3 != null)
                                            s.BdFtDd = treeRec.BdFtDd3;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd3 != null)
                                            s.BdFtPd = treeRec.BdFtPd3;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd3 != null)
                                            s.CuFtDd = treeRec.CuFtDd3;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd3 != null)
                                            s.CuFtLd = treeRec.CuFtLd3;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length3 != null)
                                            s.Length = treeRec.Length3.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort3 != null)
                                            s.SortCode = treeRec.Sort3.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade3 != null)
                                            s.GradeCode = treeRec.Grade3.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 3:
                                        if (treeRec.BdFtLd4 != null)
                                            s.BdFtLd = treeRec.BdFtLd4;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd4 != null)
                                            s.BdFtDd = treeRec.BdFtDd4;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd4 != null)
                                            s.BdFtPd = treeRec.BdFtPd4;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd4 != null)
                                            s.CuFtDd = treeRec.CuFtDd4;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd4 != null)
                                            s.CuFtLd = treeRec.CuFtLd4;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length4 != null)
                                            s.Length = treeRec.Length4.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort4 != null)
                                            s.SortCode = treeRec.Sort4.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade4 != null)
                                            s.GradeCode = treeRec.Grade4.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 4:
                                        if (treeRec.BdFtLd5 != null)
                                            s.BdFtLd = treeRec.BdFtLd5;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd5 != null)
                                            s.BdFtDd = treeRec.BdFtDd5;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd5 != null)
                                            s.BdFtPd = treeRec.BdFtPd5;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd5 != null)
                                            s.CuFtDd = treeRec.CuFtDd5;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd5 != null)
                                            s.CuFtLd = treeRec.CuFtLd5;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length5 != null)
                                            s.Length = treeRec.Length5.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort5 != null)
                                            s.SortCode = treeRec.Sort5.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade5 != null)
                                            s.GradeCode = treeRec.Grade5.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 5:
                                        if (treeRec.BdFtLd6 != null)
                                            s.BdFtLd = treeRec.BdFtLd6;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd6 != null)
                                            s.BdFtDd = treeRec.BdFtDd6;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd6 != null)
                                            s.BdFtPd = treeRec.BdFtPd6;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd6 != null)
                                            s.CuFtDd = treeRec.CuFtDd6;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd6 != null)
                                            s.CuFtLd = treeRec.CuFtLd6;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length6 != null)
                                            s.Length = treeRec.Length6.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort6 != null)
                                            s.SortCode = treeRec.Sort6.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade6 != null)
                                            s.GradeCode = treeRec.Grade6.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 6:
                                        if (treeRec.BdFtLd7 != null)
                                            s.BdFtLd = treeRec.BdFtLd7;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd7 != null)
                                            s.BdFtDd = treeRec.BdFtDd7;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd7 != null)
                                            s.BdFtPd = treeRec.BdFtPd7;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd7 != null)
                                            s.CuFtDd = treeRec.CuFtDd7;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd7 != null)
                                            s.CuFtLd = treeRec.CuFtLd7;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length7 != null)
                                            s.Length = treeRec.Length7.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort7 != null)
                                            s.SortCode = treeRec.Sort7.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade7 != null)
                                            s.GradeCode = treeRec.Grade7.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 7:
                                        if (treeRec.BdFtLd8 != null)
                                            s.BdFtLd = treeRec.BdFtLd8;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd8 != null)
                                            s.BdFtDd = treeRec.BdFtDd8;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd8 != null)
                                            s.BdFtPd = treeRec.BdFtPd8;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd8 != null)
                                            s.CuFtDd = treeRec.CuFtDd8;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd8 != null)
                                            s.CuFtLd = treeRec.CuFtLd8;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length8 != null)
                                            s.Length = treeRec.Length8.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort8 != null)
                                            s.SortCode = treeRec.Sort8.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade8 != null)
                                            s.GradeCode = treeRec.Grade8.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 8:
                                        if (treeRec.BdFtLd9 != null)
                                            s.BdFtLd = treeRec.BdFtLd9;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd9 != null)
                                            s.BdFtDd = treeRec.BdFtDd9;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd9 != null)
                                            s.BdFtPd = treeRec.BdFtPd9;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd9 != null)
                                            s.CuFtDd = treeRec.CuFtDd9;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd9 != null)
                                            s.CuFtLd = treeRec.CuFtLd9;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length9 != null)
                                            s.Length = treeRec.Length9.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort9 != null)
                                            s.SortCode = treeRec.Sort9.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade9 != null)
                                            s.GradeCode = treeRec.Grade9.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 9:
                                        if (treeRec.BdFtLd10 != null)
                                            s.BdFtLd = treeRec.BdFtLd10;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd10 != null)
                                            s.BdFtDd = treeRec.BdFtDd10;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd10 != null)
                                            s.BdFtPd = treeRec.BdFtPd10;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd10 != null)
                                            s.CuFtDd = treeRec.CuFtDd10;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd10 != null)
                                            s.CuFtLd = treeRec.CuFtLd10;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length10 != null)
                                            s.Length = treeRec.Length10.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort10 != null)
                                            s.SortCode = treeRec.Sort10.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade10 != null)
                                            s.GradeCode = treeRec.Grade10.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 10:
                                        if (treeRec.BdFtLd11 != null)
                                            s.BdFtLd = treeRec.BdFtLd11;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd11 != null)
                                            s.BdFtDd = treeRec.BdFtDd11;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd11 != null)
                                            s.BdFtPd = treeRec.BdFtPd11;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd11 != null)
                                            s.CuFtDd = treeRec.CuFtDd11;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd11 != null)
                                            s.CuFtLd = treeRec.CuFtLd11;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length11 != null)
                                            s.Length = treeRec.Length11.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort11 != null)
                                            s.SortCode = treeRec.Sort11.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade11 != null)
                                            s.GradeCode = treeRec.Grade11.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                    case 11:
                                        if (treeRec.BdFtLd12 != null)
                                            s.BdFtLd = treeRec.BdFtLd12;
                                        else
                                            s.BdFtLd = 0;
                                        if (treeRec.BdFtDd12 != null)
                                            s.BdFtDd = treeRec.BdFtDd12;
                                        else
                                            s.BdFtDd = 0;
                                        if (treeRec.BdFtPd12 != null)
                                            s.BdFtPd = treeRec.BdFtPd12;
                                        else
                                            s.BdFtPd = string.Empty;
                                        if (treeRec.CuFtDd12 != null)
                                            s.CuFtDd = treeRec.CuFtDd12;
                                        else
                                            s.CuFtDd = 0;
                                        if (treeRec.CuFtLd12 != null)
                                            s.CuFtLd = treeRec.CuFtLd12;
                                        else
                                            s.CuFtLd = 0;
                                        if (treeRec.Length12 != null)
                                            s.Length = treeRec.Length12.Trim();
                                        else
                                            s.Length = string.Empty;
                                        if (treeRec.Sort12 != null)
                                            s.SortCode = treeRec.Sort12.Trim();
                                        else
                                            s.SortCode = string.Empty;
                                        if (treeRec.Grade12 != null)
                                            s.GradeCode = treeRec.Grade12.Trim();
                                        else
                                            s.GradeCode = string.Empty;
                                        break;
                                }
                                if (s.SortCode == "0" || s.GradeCode == "0")
                                    s.Comments = "Cull Segment";
                                ++segCount;
                                _addSegments.Add(s);
                                if (segCount > 100)
                                {
                                    segCount = 0;
                                    ctx.Treesegments.AddRange(_addSegments);
                                    _addSegments.Clear();
                                }
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    // no segment data
                                    Treesegment s = new Treesegment();

                                    s.StandsId = treeRec.StandsId;
                                    s.PlotId = treeRec.PlotId;
                                    s.TreesId = treeRec.TreesId;
                                    //s.Tree = treeRec;
                                    s.GroupPlotNumber = treeRec.PlotNumber;
                                    s.GroupTreeNumber = treeRec.TreeNumber;
                                    s.SegmentNumber = 0;
                                    s.TreeType = treeRec.TreeType;

                                    s.CalcAccumulatedLength = 0;
                                    s.Comments = string.Empty;
                                    s.LogValue = 0;
                                    s.Species = treeRec.SpeciesAbbreviation;
                                    s.TreeBasalArea = 0;
                                    s.TreeTreesPerAcre = 0;

                                    s.CalcLen = 0;
                                    s.CalcTopDia = 0;
                                    s.CalcButtDia = 0;
                                    s.CubicGrossVolume = 0;
                                    s.CubicNetVolume = 0;
                                    s.ScribnerGrossVolume = 0;
                                    s.ScribnerNetVolume = 0;
                                    s.CubicGrossVolumePerAce = 0;
                                    s.CubicNetVolumePerAce = 0;
                                    s.ScribnerGrossVolumePerAce = 0;
                                    s.ScribnerNetVolumePerAce = 0;
                                    s.Ccf = 0;
                                    s.Mbf = 0;
                                    s.Species = treeRec.SpeciesAbbreviation.Trim();
                                    s.Age = treeRec.Age;
                                    s.Ao = 0;
                                    s.Bark = 0;
                                    s.BoleHeight = 0;
                                    s.CalcTotalHtUsedYn = "N";
                                    s.CrownPosition = string.Empty;
                                    s.CrownRatio = string.Empty;
                                    s.Damage = string.Empty;
                                    s.Dbh = treeRec.Dbh;
                                    s.FormFactor = 0;
                                    s.FormPoint = treeRec.FormPoint;
                                    s.PlotFactorInput = treeRec.PlotFactorInput;
                                    s.PlotNumber = treeRec.PlotNumber;
                                    s.Tdf = string.Empty;
                                    s.TotalHeight = 0;
                                    s.TreeCount = treeRec.TreeCount;
                                    s.TreeNumber = (short)treeRec.TreeNumber;
                                    s.TreeStatusDisplayCode = string.Empty;
                                    s.UserDefined = string.Empty;
                                    s.Vigor = string.Empty;
                                    s.BdFtLd = 0;
                                    s.BdFtDd = 0;
                                    s.BdFtPd = string.Empty;
                                    s.CuFtDd = 0;
                                    s.CuFtLd = 0;
                                    s.Length = string.Empty;
                                    s.SortCode = string.Empty;
                                    s.GradeCode = string.Empty;
                                    s.Ao = 0;
                                    s.Bark = 0;
                                    ++segCount;
                                    _addSegments.Add(s);
                                    if (segCount > 100)
                                    {
                                        segCount = 0;
                                        ctx.Treesegments.AddRange(_addSegments);
                                        _addSegments.Clear();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ctx.Treesegments.AddRange(_addSegments);
            _addSegments.Clear();
            ctx.SaveChanges();
        }

        //private int? GetPlotId(ref ProjectDbContext projectContext, int? pId, string p)
        //{
        //    Plot plot = projectContext.Plot.First(l => l.PlotsId == pId && l.UserPlotNumber == p);
        //    if (plot != null)
        //        return plot.PlotsId;
        //    else
        //        return -1;
        //}

        //private void PopulateTree(ref ProjectDbContext projectContext, Tree pTree)
        //{
        //    //tt.SetTreeMetrics((long)pTree.UpToPlotsByPlotId.UpToStandsByStandsId.UpToProjectsByProjectsId.ProjectsId, (long)pTree.UpToPlotsByPlotId.UpToStandsByStandsId.StandsId, (Trees)pTree);
        //}

        private int _segment = 0;
        private void ProcessSegments(ref Tree pCru)
        {
            _segment = 0;
            while (_mdbReader["Continuation"].ToString() != "9")
            {
                ProcessContinuation(ref pCru);
                _mdbReader.Read();
            }
            if (!string.IsNullOrEmpty(_mdbReader["Length1"].ToString()))
                InsertSegment(_segment + 1, ref pCru);
            else
                InitSegment(_segment + 1, ref pCru);
            if (!string.IsNullOrEmpty(_mdbReader["Length2"].ToString()))
                InsertSegment(_segment + 2, ref pCru);
            else
                InitSegment(_segment + 2, ref pCru);
            if (!string.IsNullOrEmpty(_mdbReader["Length3"].ToString()))
                InsertSegment(_segment + 3, ref pCru);
            else
                InitSegment(_segment + 3, ref pCru);
        }

        private void ProcessContinuation(ref Tree pCru)
        {
            if (_mdbReader["Continuation"].ToString() == "1")
            {
                if (!string.IsNullOrEmpty(_mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(1, ref pCru);
                else
                    InitSegment(1, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(2, ref pCru);
                else
                    InitSegment(2, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(3, ref pCru);
                else
                    InitSegment(3, ref pCru);
                _segment = 3;
            }
            if (_mdbReader["Continuation"].ToString() == "2")
            {
                if (!string.IsNullOrEmpty(_mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(4, ref pCru);
                else
                    InitSegment(4, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(5, ref pCru);
                else
                    InitSegment(5, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(6, ref pCru);
                else
                    InitSegment(6, ref pCru);
                _segment = 6;
            }
            if (_mdbReader["Continuation"].ToString() == "3")
            {
                if (!string.IsNullOrEmpty(_mdbReader["Length1"].ToString().Trim()))
                    InsertSegment(7, ref pCru);
                else
                    InitSegment(7, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length2"].ToString().Trim()))
                    InsertSegment(8, ref pCru);
                else
                    InitSegment(8, ref pCru);
                if (!string.IsNullOrEmpty(_mdbReader["Length3"].ToString().Trim()))
                    InsertSegment(9, ref pCru);
                else
                    InitSegment(9, ref pCru);
                _segment = 9;
            }
        }

        private void InsertSegment(int p, ref Tree pCru)
        {
            switch (p)
            {
                case 1:
                    pCru.Sort1 = _mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade1 = _mdbReader["Grade1"].ToString().Trim();
                    pCru.Length1 = _mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd1 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd1 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd1 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd1 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd1 = _mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 2:
                    pCru.Sort2 = _mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade2 = _mdbReader["Grade2"].ToString().Trim();
                    pCru.Length2 = _mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd2 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd2 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd2 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd2 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd2 = _mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 3:
                    pCru.Sort3 = _mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade3 = _mdbReader["Grade3"].ToString().Trim();
                    pCru.Length3 = _mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd3 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd3 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd3 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd3 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd3 = _mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 4:
                    pCru.Sort4 = _mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade4 = _mdbReader["Grade1"].ToString().Trim();
                    pCru.Length4 = _mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd4 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd4 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd4 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd4 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd4 = _mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 5:
                    pCru.Sort5 = _mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade5 = _mdbReader["Grade2"].ToString().Trim();
                    pCru.Length5 = _mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd5 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd5 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd5 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd5 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd5 = _mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 6:
                    pCru.Sort6 = _mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade6 = _mdbReader["Grade3"].ToString().Trim();
                    pCru.Length6 = _mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd6 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd6 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd6 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd6 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd6 = _mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 7:
                    pCru.Sort7 = _mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade7 = _mdbReader["Grade1"].ToString().Trim();
                    pCru.Length7 = _mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd7 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd7 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd7 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd7 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd7 = _mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 8:
                    pCru.Sort8 = _mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade8 = _mdbReader["Grade2"].ToString().Trim();
                    pCru.Length8 = _mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd8 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd8 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd8 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd8 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd8 = _mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 9:
                    pCru.Sort9 = _mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade9 = _mdbReader["Grade3"].ToString().Trim();
                    pCru.Length9 = _mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd9 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd9 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd9 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd9 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd9 = _mdbReader["PctDed3"].ToString().Trim();
                    break;
                case 10:
                    pCru.Sort10 = _mdbReader["Sort1"].ToString().Trim();
                    pCru.Grade10 = _mdbReader["Grade1"].ToString().Trim();
                    pCru.Length10 = _mdbReader["Length1"].ToString().Trim();
                    pCru.BdFtLd10 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd1"].ToString());
                    pCru.BdFtDd10 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd1"].ToString());
                    pCru.CuFtLd10 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd1"].ToString());
                    pCru.CuFtDd10 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd1"].ToString());
                    pCru.BdFtPd10 = _mdbReader["PctDed1"].ToString().Trim();
                    break;
                case 11:
                    pCru.Sort11 = _mdbReader["Sort2"].ToString().Trim();
                    pCru.Grade11 = _mdbReader["Grade2"].ToString().Trim();
                    pCru.Length11 = _mdbReader["Length2"].ToString().Trim();
                    pCru.BdFtLd11 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd2"].ToString());
                    pCru.BdFtDd11 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd2"].ToString());
                    pCru.CuFtLd11 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd2"].ToString());
                    pCru.CuFtDd11 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd2"].ToString());
                    pCru.BdFtPd11 = _mdbReader["PctDed2"].ToString().Trim();
                    break;
                case 12:
                    pCru.Sort12 = _mdbReader["Sort3"].ToString().Trim();
                    pCru.Grade12 = _mdbReader["Grade3"].ToString().Trim();
                    pCru.Length12 = _mdbReader["Length3"].ToString().Trim();
                    pCru.BdFtLd12 = (byte)Utils.ConvertToInt(_mdbReader["BdFtLd3"].ToString());
                    pCru.BdFtDd12 = (byte)Utils.ConvertToInt(_mdbReader["BdFtDd3"].ToString());
                    pCru.CuFtLd12 = (byte)Utils.ConvertToInt(_mdbReader["CuFtLd3"].ToString());
                    pCru.CuFtDd12 = (byte)Utils.ConvertToInt(_mdbReader["CuFtDd3"].ToString());
                    pCru.BdFtPd12 = _mdbReader["PctDed3"].ToString().Trim();
                    break;
            }
        }

        private void InitSegment(int p, ref Tree pCru)
        {
            switch (p)
            {
                case 1:
                    pCru.Sort1 = string.Empty;
                    pCru.Grade1 = string.Empty;
                    pCru.Length1 = string.Empty;
                    pCru.BdFtLd1 = 0;
                    pCru.BdFtDd1 = 0;
                    pCru.CuFtLd1 = 0;
                    pCru.CuFtDd1 = 0;
                    pCru.BdFtPd1 = string.Empty;
                    break;
                case 2:
                    pCru.Sort2 = string.Empty;
                    pCru.Grade2 = string.Empty;
                    pCru.Length2 = string.Empty;
                    pCru.BdFtLd2 = 0;
                    pCru.BdFtDd2 = 0;
                    pCru.CuFtLd2 = 0;
                    pCru.CuFtDd2 = 0;
                    pCru.BdFtPd2 = string.Empty;
                    break;
                case 3:
                    pCru.Sort3 = string.Empty;
                    pCru.Grade3 = string.Empty;
                    pCru.Length3 = string.Empty;
                    pCru.BdFtLd3 = 0;
                    pCru.BdFtDd3 = 0;
                    pCru.CuFtLd3 = 0;
                    pCru.CuFtDd3 = 0;
                    pCru.BdFtPd3 = string.Empty;
                    break;
                case 4:
                    pCru.Sort4 = string.Empty;
                    pCru.Grade4 = string.Empty;
                    pCru.Length4 = string.Empty;
                    pCru.BdFtLd4 = 0;
                    pCru.BdFtDd4 = 0;
                    pCru.CuFtLd4 = 0;
                    pCru.CuFtDd4 = 0;
                    pCru.BdFtPd4 = string.Empty;
                    break;
                case 5:
                    pCru.Sort5 = string.Empty;
                    pCru.Grade5 = string.Empty;
                    pCru.Length5 = string.Empty;
                    pCru.BdFtLd5 = 0;
                    pCru.BdFtDd5 = 0;
                    pCru.CuFtLd5 = 0;
                    pCru.CuFtDd5 = 0;
                    pCru.BdFtPd5 = string.Empty;
                    break;
                case 6:
                    pCru.Sort6 = string.Empty;
                    pCru.Grade6 = string.Empty;
                    pCru.Length6 = string.Empty;
                    pCru.BdFtLd6 = 0;
                    pCru.BdFtDd6 = 0;
                    pCru.CuFtLd6 = 0;
                    pCru.CuFtDd6 = 0;
                    pCru.BdFtPd6 = string.Empty;
                    break;
                case 7:
                    pCru.Sort7 = string.Empty;
                    pCru.Grade7 = string.Empty;
                    pCru.Length7 = string.Empty;
                    pCru.BdFtLd7 = 0;
                    pCru.BdFtDd7 = 0;
                    pCru.CuFtLd7 = 0;
                    pCru.CuFtDd7 = 0;
                    pCru.BdFtPd7 = string.Empty;
                    break;
                case 8:
                    pCru.Sort8 = string.Empty;
                    pCru.Grade8 = string.Empty;
                    pCru.Length8 = string.Empty;
                    pCru.BdFtLd8 = 0;
                    pCru.BdFtDd8 = 0;
                    pCru.CuFtLd8 = 0;
                    pCru.CuFtDd8 = 0;
                    pCru.BdFtPd8 = string.Empty;
                    break;
                case 9:
                    pCru.Sort9 = string.Empty;
                    pCru.Grade9 = string.Empty;
                    pCru.Length9 = string.Empty;
                    pCru.BdFtLd9 = 0;
                    pCru.BdFtDd9 = 0;
                    pCru.CuFtLd9 = 0;
                    pCru.CuFtDd9 = 0;
                    pCru.BdFtPd9 = string.Empty;
                    break;
                case 10:
                    pCru.Sort10 = string.Empty;
                    pCru.Grade10 = string.Empty;
                    pCru.Length10 = string.Empty;
                    pCru.BdFtLd10 = 0;
                    pCru.BdFtDd10 = 0;
                    pCru.CuFtLd10 = 0;
                    pCru.CuFtDd10 = 0;
                    pCru.BdFtPd10 = string.Empty;
                    break;
                case 11:
                    pCru.Sort11 = string.Empty;
                    pCru.Grade11 = string.Empty;
                    pCru.Length11 = string.Empty;
                    pCru.BdFtLd11 = 0;
                    pCru.BdFtDd11 = 0;
                    pCru.CuFtLd11 = 0;
                    pCru.CuFtDd11 = 0;
                    pCru.BdFtPd11 = string.Empty;
                    break;
                case 12:
                    pCru.Sort12 = string.Empty;
                    pCru.Grade12 = string.Empty;
                    pCru.Length12 = string.Empty;
                    pCru.BdFtLd12 = 0;
                    pCru.BdFtDd12 = 0;
                    pCru.CuFtLd12 = 0;
                    pCru.CuFtDd12 = 0;
                    pCru.BdFtPd12 = string.Empty;
                    break;
            }
        }

        private void ImportCruiseSegments()
        {
            /*
            ProjectCollection coll = new ProjectCollection();
            coll.LoadAll();

            while (mdbReader.Read())
            {
                Project.DAL.Project cs = coll.AddNew();

                cs.Project = mdbReader["Project"].ToString();
                if (!string.IsNullOrEmpty(mdbReader["Customer Name"].ToString()))
                    cs.CustomerName = mdbReader["Customer Name"].ToString();
                else
                    cs.CustomerName = string.Empty;
                if (!string.IsNullOrEmpty(mdbReader["Location"].ToString()))
                    cs.Location = mdbReader["Location"].ToString();
                else
                    cs.Location = string.Empty;
                cs.MapData = string.Empty;
                cs.MapProject = string.Empty;
                cs.Meridian = string.Empty;
            }
            coll.Save();
             */
        }

        //private void DeleteTable(ref ProjectDbContext projectContext, string pDb, string pTable, string pFieldName, string pNewTable, string pNewFieldName, string pWhereFieldName, string pWhereFieldValue)
        //{
        //    string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
        //    mdbConn = new OleDbConnection(connectionString);

        //    mdbConn.Open();

        //    string sql = string.Empty;
        //    sql = string.Format("SELECT [{0}] FROM {1} GROUP BY [{2}]", pFieldName, pTable, pFieldName);
        //    mdbCmd = new OleDbCommand(sql, mdbConn);
        //    mdbReader = mdbCmd.ExecuteReader();

        //    while (mdbReader.Read())
        //    {
        //        SqlCeCommand iCmd = new SqlCeCommand(projectContext.Connection.ConnectionString);
        //        iCmd.CommandText = string.Format("DELETE FROM {0} WHERE {1} = '{2}' AND {3} = '{4}'", pNewTable, pNewFieldName, mdbReader[pFieldName].ToString(), pWhereFieldName, pWhereFieldValue);
        //        int iRet = iCmd.ExecuteNonQuery();
        //    }
        //}

        public bool ProcessSpecies(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName, string pProjectLocationDataSource)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Species Tables ...");
            ctx.Species.RemoveRange(ctx.Species.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.Species.RemoveRange(ctx.Species);
            //ctx.SaveChanges();
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportSpecies(ref ctx, pProjectLocationDataSource);
            CloseFile();
            return true;
        }

        private void ImportSpecies(ref ProjectDbContext ctx, string pProjectLocationDataSource)
        {
            while (_mdbReader.Read())
            {
                Species cs = new Species();

                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.InputCode = Int16.Parse(_mdbReader["Code"].ToString().Trim());
                cs.Abbreviation = _mdbReader["Abrv"].ToString().Trim();
                cs.SpeciesGroup = ProjectsBLL.GetPrimarySpecies(pProjectLocationDataSource, cs.Abbreviation);
                if (!string.IsNullOrEmpty(_mdbReader["Description"].ToString().Trim()))
                {
                    cs.Description = _mdbReader["Description"].ToString().Trim();
                    cs.CommonName = _mdbReader["Description"].ToString().Trim();
                }
                else
                {
                    cs.Description = string.Empty;
                    cs.CommonName = string.Empty;
                }
                if (!string.IsNullOrEmpty(_mdbReader["Bark"].ToString().Trim()))
                {
                    if (Utils.IsValidFloat(_mdbReader["Bark"].ToString().Trim()))
                        cs.BarkRatioValue = Utils.ConvertToDouble(_mdbReader["Bark"].ToString().Trim());
                    else
                        cs.BarkRatioDisplayCode = _mdbReader["Bark"].ToString().Trim();
                }
                else
                {
                    cs.BarkRatioValue = 0;
                    cs.BarkRatioDisplayCode = string.Empty;
                }
                if (!string.IsNullOrEmpty(_mdbReader["ASubo"].ToString().Trim()))
                {
                    if (Utils.IsValidFloat(_mdbReader["ASubo"].ToString().Trim()))
                    {
                        cs.ASuboValue = Utils.ConvertToDouble(_mdbReader["ASubo"].ToString().Trim());
                        cs.AsubODisplayCode = _mdbReader["ASubo"].ToString().Trim();
                    }
                    else
                    {
                        cs.AsubODisplayCode = _mdbReader["ASubo"].ToString().Trim();
                        cs.ASuboValue = 0;
                    }
                }
                else
                {
                    cs.AsubODisplayCode = string.Empty;
                    cs.ASuboValue = 0;
                }
                if (!string.IsNullOrEmpty(_mdbReader["Form Factor"].ToString().Trim()))
                {
                    if (Utils.IsValidFloat(_mdbReader["Form Factor"].ToString().Trim()))
                    {
                        cs.FormFactorValue = Utils.ConvertToDouble(_mdbReader["Form Factor"].ToString().Trim());
                        cs.FormFactorsDisplayCode = _mdbReader["Form Factor"].ToString().Trim();
                    }
                    else
                    {
                        cs.FormFactorsDisplayCode = _mdbReader["Form Factor"].ToString().Trim();
                        cs.FormFactorValue = 0;
                    }
                }
                else
                {
                    cs.FormFactorsDisplayCode = string.Empty;
                    cs.FormFactorValue = 0;
                }
                if (!string.IsNullOrEmpty(_mdbReader["Wood Type"].ToString().Trim()))
                    cs.WoodTypeDisplayCode = _mdbReader["Wood Type"].ToString().Trim();
                else
                    cs.WoodTypeDisplayCode = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Component"].ToString().Trim()))
                    cs.ComponentsDisplayCode = _mdbReader["Component"].ToString().Trim();
                else
                    cs.ComponentsDisplayCode = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["YldTable"].ToString().Trim()))
                    cs.YieldTableName = _mdbReader["YldTable"].ToString().Trim();
                else
                    cs.YieldTableName = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Min Dia"].ToString().Trim()))
                    cs.MinDia = Single.Parse(_mdbReader["Min Dia"].ToString().Trim());
                else
                    cs.MinDia = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Min Len"].ToString().Trim()))
                    cs.MinLen = Single.Parse(_mdbReader["Min Len"].ToString().Trim());
                else
                    cs.MinLen = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Max Len"].ToString().Trim()))
                    cs.MaxLen = Single.Parse(_mdbReader["Max Len"].ToString().Trim());
                else
                    cs.MaxLen = 0;
                if (!string.IsNullOrEmpty(_mdbReader["MaxDia"].ToString().Trim()))
                    cs.MaxDia = Single.Parse(_mdbReader["MaxDia"].ToString().Trim());
                else
                    cs.MaxDia = 0;
                if (!string.IsNullOrEmpty(_mdbReader["MaxHeight"].ToString().Trim()))
                    cs.MaxHeight = (short)Utils.ConvertToInt(_mdbReader["MaxHeight"].ToString().Trim());
                else
                    cs.MaxHeight = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Trim"].ToString().Trim()))
                    cs.Trim = Single.Parse(_mdbReader["Trim"].ToString().Trim());
                else
                    cs.Trim = 0;
                if (!string.IsNullOrEmpty(_mdbReader["BdFt Rule"].ToString().Trim()))
                    cs.BdFtDisplayCode = _mdbReader["BdFt Rule"].ToString().Trim();
                else
                    cs.BdFtDisplayCode = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["CuFt Rule"].ToString().Trim()))
                    cs.CuFtDisplayCode = _mdbReader["CuFt Rule"].ToString().Trim();
                else
                    cs.CuFtDisplayCode = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs"].ToString().Trim()))
                    cs.Lbs = Int32.Parse(_mdbReader["Lbs"].ToString().Trim());
                else
                    cs.Lbs = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs Type"].ToString().Trim()))
                    cs.LbsType = _mdbReader["Lbs Type"].ToString().Trim();
                else
                    cs.LbsType = string.Empty;
                cs.BioMass = 0;
                cs.GrowthModelTableName = string.Empty;
                cs.AsubOTableName = "GENERAL";
                cs.BarkFactorsTableName = "GENERAL";
                cs.BoardFootRuleTableName = "GENERAL";
                cs.ComponentsTableName = "GENERAL";
                cs.CubicFootRuleTableName = "GENERAL";
                cs.FormFactorsTableName = "GENERAL";
                cs.WoodTypeTableName = "GENERAL";
                cs.PercentCarbon = 0;
                cs.PercentDryWeight = 0;
                cs.ScientificName = string.Empty;
                cs.SpeciesGroup = GetSpeciesGroup(cs.Abbreviation);
                if (!string.IsNullOrEmpty(_mdbReader["Weight"].ToString().Trim()))
                    cs.Weight = Utils.ConvertToTiny(_mdbReader["Weight"].ToString().Trim());
                else
                    cs.Weight = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Weight Type"].ToString().Trim()))
                    cs.WeightUnits = _mdbReader["Weight Type"].ToString().Trim();
                else
                    cs.WeightUnits = string.Empty;
                ctx.Species.Add(cs);
            }
            ctx.SaveChanges();
        }

        private string GetSpeciesGroup(string p)
        {
            string result;
            switch (p)
            {
                case "DF":
                case "PO":
                    result = "DF";
                    break;
                case "SS":
                    result = "SS";
                    break;
                case "WH":
                case "WF":
                case "RF":
                case "AF":
                case "SF":
                case "GF":
                case "NF":
                case "WL":
                case "MH":
                    result = "WH";
                    break;
                case "PP":
                case "SP":
                case "JP":
                case "IC":
                    result = "PP";
                    break;
                case "WP":
                case "LP":
                case "ES":
                case "BS":
                case "CP":
                    result = "WP";
                    break;
                case "RA":
                case "BM":
                case "MA":
                case "PY":
                case "TO":
                case "OA":
                case "OK":
                case "CQ":
                case "OM":
                    result = "RA";
                    break;
                default:
                    result = p;
                    break;
            }
            return result;
        }

        public bool ProcessGrade(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Grade Tables ...");
            ctx.Grades.RemoveRange(ctx.Grades.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFileSortGrade(pDb, pTable, "Table Name", pTableName, "G");
            //ctx.Grade.RemoveRange(ctx.Grade);
            //ctx.SaveChanges();
            //OpenFile(pDb, pTable, "[Table Name]", "Record Code", "G");
            ImportGrade(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportGrade(ref ProjectDbContext ctx)
        {
            while (_mdbReader.Read())
            {
                Grade cs = new Grade();

                cs.TableName = _mdbReader["Table Name"].ToString().Trim(); //.Replace("SORT", "GRADE");
                cs.InputCode = _mdbReader["Grade"].ToString().Trim();
                cs.Abbreviation = _mdbReader["Abrv"].ToString().Trim();
                if (!string.IsNullOrEmpty(_mdbReader["Description"].ToString().Trim()))
                    cs.Description = _mdbReader["Description"].ToString().Trim();
                else
                    cs.Description = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Fiber"].ToString()))
                //    cs.Fiber = mdbReader["Fiber"].ToString();
                //else
                //    cs.Fiber = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Min Dia"].ToString()))
                    cs.MinimumDiameter = Utils.ConvertToInt(_mdbReader["Min Dia"].ToString());
                else
                    cs.MinimumDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Max Dia"].ToString()))
                    cs.MaximumDiameter = Utils.ConvertToInt(_mdbReader["Max Dia"].ToString());
                else
                    cs.MaximumDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Max Butt"].ToString()))
                    cs.MaximumButtDiameter = Utils.ConvertToInt(_mdbReader["Max Butt"].ToString());
                else
                    cs.MaximumButtDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Min Len"].ToString()))
                    cs.MinimumLength = Utils.ConvertToInt(_mdbReader["Min Len"].ToString());
                else
                    cs.MinimumLength = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Max Len"].ToString()))
                //    cs.MaxLen = Single.Parse(mdbReader["Max Len"].ToString());
                //else
                //    cs.MaxLen = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Defect"].ToString()))
                //    cs..Defect = Int16.Parse(mdbReader["Defect"].ToString());
                //else
                //    cs.Defect = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Min Vol"].ToString()))
                    cs.MinimumBdftPerLog = Utils.ConvertToInt(_mdbReader["Min Vol"].ToString());
                else
                    cs.MinimumBdftPerLog = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Vol Type"].ToString().Trim()))
                    cs.VolumeUnits = _mdbReader["Vol Type"].ToString().Trim();
                else
                    cs.VolumeUnits = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Min Rings"].ToString()))
                    cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(_mdbReader["Min Rings"].ToString());
                else
                    cs.MinimumRingsPerInch = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Knot Size"].ToString()))
                    cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(_mdbReader["Knot Size"].ToString());
                else
                    cs.MinimumRingsPerInch = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Knot Freq"].ToString()))
                    cs.KnotsPerFoot = (byte)Utils.ConvertToInt(_mdbReader["Knot Freq"].ToString());
                else
                    cs.KnotsPerFoot = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Straight"].ToString()))
                //    cs.Straight = mdbReader["Straight"].ToString();
                //else
                //    cs.Straight = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Sap"].ToString()))
                //    cs.Sap = mdbReader["Sap"].ToString();
                //else
                //    cs.Sap = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs"].ToString()))
                    cs.WeightInPounds = Utils.ConvertToInt(_mdbReader["Lbs"].ToString());
                else
                    cs.WeightInPounds = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs Type"].ToString().Trim()))
                    cs.VolumeUnits = _mdbReader["Lbs Type"].ToString().Trim();
                else
                    cs.VolumeUnits = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Cords"].ToString()))
                //    cs.Cords = Single.Parse(mdbReader["Cords"].ToString());
                //else
                //    cs.Cords = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Cords Type"].ToString()))
                //    cs.CordsType = mdbReader["Cords Type"].ToString();
                //else
                //    cs.CordsType = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Product"].ToString()))
                //    cs.Product = mdbReader["Product"].ToString();
                //else
                //    cs.Product = string.Empty;
                ctx.Grades.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessSort(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Sort Tables ...");
            ctx.Sorts.RemoveRange(ctx.Sorts.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFileSortGrade(pDb, pTable, "Table Name", pTableName, "O");
            //ctx.Sort.RemoveRange(ctx.Sort);
            //OpenFile(pDb, pTable, "[Table Name]", "Record Code", "O");
            ImportSort(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportSort(ref ProjectDbContext ctx)
        {
            while (_mdbReader.Read())
            {
                Sort cs = new Sort();

                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.InputCode = _mdbReader["Sort"].ToString().Trim();
                cs.Abbreviation = _mdbReader["Abrv"].ToString().Trim();
                if (!string.IsNullOrEmpty(_mdbReader["Description"].ToString().Trim()))
                    cs.Description = _mdbReader["Description"].ToString().Trim();
                else
                    cs.Description = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Fiber"].ToString()))
                //    cs.Fiber = mdbReader["Fiber"].ToString();
                //else
                //    cs.Fiber = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Min Dia"].ToString()))
                    cs.MinimumDiameter = Utils.ConvertToInt(_mdbReader["Min Dia"].ToString());
                else
                    cs.MinimumDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Max Dia"].ToString()))
                    cs.MaximumDiameter = Utils.ConvertToInt(_mdbReader["Max Dia"].ToString());
                else
                    cs.MaximumDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Max Butt"].ToString()))
                    cs.MaximumButtDiameter = Utils.ConvertToInt(_mdbReader["Max Butt"].ToString());
                else
                    cs.MaximumButtDiameter = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Min Len"].ToString()))
                    cs.MinimumLength = Utils.ConvertToInt(_mdbReader["Min Len"].ToString());
                else
                    cs.MinimumLength = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Max Len"].ToString()))
                //    cs.MaxLen = Single.Parse(mdbReader["Max Len"].ToString());
                //else
                //    cs.MaxLen = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Defect"].ToString()))
                //    cs.Defect = Int16.Parse(mdbReader["Defect"].ToString());
                //else
                //    cs.Defect = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Min Vol"].ToString()))
                    cs.MinimumBdftPerLog = Utils.ConvertToInt(_mdbReader["Min Vol"].ToString());
                else
                    cs.MinimumBdftPerLog = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Vol Type"].ToString().Trim()))
                    cs.VolumeUnits = _mdbReader["Vol Type"].ToString().Trim();
                else
                    cs.VolumeUnits = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Min Rings"].ToString()))
                    cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(_mdbReader["Min Rings"].ToString());
                else
                    cs.MinimumRingsPerInch = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Knot Size"].ToString()))
                    cs.MinimumRingsPerInch = (byte)Utils.ConvertToInt(_mdbReader["Knot Size"].ToString());
                else
                    cs.MinimumRingsPerInch = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Knot Freq"].ToString()))
                    cs.KnotsPerFoot = (byte)Utils.ConvertToInt(_mdbReader["Knot Freq"].ToString());
                else
                    cs.KnotsPerFoot = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Straight"].ToString()))
                //    cs.Straight = mdbReader["Straight"].ToString();
                //else
                //    cs.Straight = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Sap"].ToString()))
                //    cs.Sap = mdbReader["Sap"].ToString();
                //else
                //    cs.Sap = string.Empty;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs"].ToString()))
                    cs.WeightInPounds = Utils.ConvertToInt(_mdbReader["Lbs"].ToString());
                else
                    cs.WeightInPounds = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Lbs Type"].ToString().Trim()))
                    cs.VolumeUnits = _mdbReader["Lbs Type"].ToString().Trim();
                else
                    cs.VolumeUnits = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Cords"].ToString()))
                //    cs.Cords = Single.Parse(mdbReader["Cords"].ToString());
                //else
                //    cs.Cords = 0;
                //if (!string.IsNullOrEmpty(mdbReader["Cords Type"].ToString()))
                //    cs.CordsType = mdbReader["Cords Type"].ToString();
                //else
                //    cs.CordsType = string.Empty;
                //if (!string.IsNullOrEmpty(mdbReader["Product"].ToString()))
                //    cs.Product = mdbReader["Product"].ToString();
                //else
                //    cs.Product = string.Empty;
                ctx.Sorts.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool CheckSpecies(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "01-Species";
                if (ctx.Species.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckSort(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "02-Sort";
                if (ctx.Sorts.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckGrade(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim(); //.Replace("SORT", "GRADE");
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "03-Grade";
                if (ctx.Grades.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckPrice(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "04-Price";
                if (ctx.Prices.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckCost(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "05-Cost";
                if (ctx.Costs.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckAspect(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "06-Aspect";
                if (ctx.Aspects.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckComponent(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "07-Component";
                if (ctx.Components.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckCrownPosition(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "08-CrownPosition";
                if (ctx.Crownpositions.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckDamage(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "09-Damage";
                if (ctx.Damages.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckEnvironment(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "10-Environment";
                if (ctx.Environments.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckForm(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "11-Form";
                if (ctx.Formfactors.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckHarvest(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "12-Harvest";
                if (ctx.Harvestsystems.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckLandForm(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "13-LandForm";
                if (ctx.Landforms.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckNonStocked(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "14-NonStocked";
                if (ctx.Nonstockeds.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckNonTimbered(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "15-NonTimbered";
                if (ctx.Nontimbereds.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckRoads(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "16-Roads";
                if (ctx.Roads.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckSeedZone(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "17-SeedZone";
                if (ctx.Seedzones.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckSlash(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "18-Slash";
                if (ctx.Slashdistributions.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckSoils(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "19-Soils";
                if (ctx.Soils.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckStreams(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "20-Streams";
                if (ctx.Streams.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckTreatment(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "21-Treatment";
                if (ctx.Treatments.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckTreeSource(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "22-TreeSource";
                if (ctx.Treesources.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckTreeStatus(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "23-TreeStatus";
                if (ctx.Treestatus.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckUserDefined(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "24-UserDefined";
                if (ctx.Userdefineds.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckVegetation(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "25-Vegetation";
                if (ctx.Vegetations.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckVigor(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "26-Vigor";
                if (ctx.Vigors.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool CheckWoodType(ref ProjectDbContext ctx, ref TempDbContext dbTemp, string pDb, string pTable)
        {
            OpenGroupBy(pDb, pTable);
            while (_mdbReader.Read())
            {
                string tbl = _mdbReader["Table Name"].ToString().Trim();
                ExistingProjectTable rec = new ExistingProjectTable();
                rec.ReplaceTable = false;
                rec.TableName = tbl;
                rec.TableType = "27-WoodType";
                if (ctx.Woodtypes.Any(t => t.TableName == tbl))
                    rec.ExistingTable = true;
                else
                    rec.ExistingTable = false;
                dbTemp.ExistingProjectTables.Add(rec);
            }
            dbTemp.SaveChanges();
            CloseFile();
            return true;
        }

        public bool ProcessAspect(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Aspect Tables ...");
            ctx.Aspects.RemoveRange(ctx.Aspects.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            ImportAspect(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportAspect(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Aspect cs = new Aspect();

                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = (byte)++order;
                ctx.Aspects.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessComponent(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Component Tables ...");
            ctx.Components.RemoveRange(ctx.Components.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.COMPONENTS.RemoveRange(ctx.COMPONENTS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportComponent(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportComponent(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Component cs = new Component();

                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                ctx.Components.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessCost(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Cost Tables ...");
            ctx.Costs.RemoveRange(ctx.Costs.Where(t => t.TableName == pTableName).ToList());
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.Cost.RemoveRange(ctx.Cost);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportCost(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportCost(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Cost cs = new Cost();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.CostAmount = Utils.ConvertToFloat(_mdbReader["Cost"].ToString());
                if (_mdbReader["Cost Date"] != null && !string.IsNullOrEmpty(_mdbReader["Cost Date"].ToString()))
                    cs.CostDate = Convert.ToDateTime(_mdbReader["Cost Date"]);
                cs.CostType = "V";
                if (!string.IsNullOrEmpty(_mdbReader["Items"].ToString()))
                    cs.CostDescription = _mdbReader["Items"].ToString();
                else
                    cs.CostDescription = string.Empty;
                cs.Percent = 0;
                cs.ReportPrintOrder = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Species"].ToString()))
                    cs.Species = _mdbReader["Species"].ToString().Trim();
                else
                    cs.Species = string.Empty;
                cs.TreatmentAcresPerYear = 0;
                if (!string.IsNullOrEmpty(_mdbReader["Cost Unit"].ToString()))
                    cs.Units = _mdbReader["Cost Unit"].ToString().Trim();
                else
                    cs.Units = string.Empty;
                cs.YearsApplied = "ALL";
                ctx.Costs.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessCrownPosition(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Crown Position Tables ...");
            ctx.Crownpositions.RemoveRange(ctx.Crownpositions.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.CROWNPOSITION.RemoveRange(ctx.CROWNPOSITION);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportCrownPosition(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportCrownPosition(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Crownposition cs = new Crownposition();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Crownpositions.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessDamage(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Damage Tables ...");
            ctx.Damages.RemoveRange(ctx.Damages.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.DAMAGE.RemoveRange(ctx.DAMAGE);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportDamage(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportDamage(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Damage cs = new Damage();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Damages.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessEnvironment(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Environment Tables ...");
            ctx.Environments.RemoveRange(ctx.Environments.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.ENVIRONMENTS.RemoveRange(ctx.ENVIRONMENTS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportEnvironment(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportEnvironment(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Project.DAL.Environment cs = new Project.DAL.Environment();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Environments.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessForm(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Form Tables ...");
            ctx.Formfactors.RemoveRange(ctx.Formfactors.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.Formfactor.RemoveRange(ctx.Formfactor);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportForm(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportForm(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Formfactor cs = new Formfactor();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Avalue = Utils.ConvertToDouble(_mdbReader["A"].ToString());
                cs.Bvalue = Utils.ConvertToDouble(_mdbReader["B"].ToString());
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Species"].ToString().Trim();
                ctx.Formfactors.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessHarvest(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Harvest Tables ...");
            ctx.Harvestsystems.RemoveRange(ctx.Harvestsystems.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.HARVESTSYSTEMS.RemoveRange(ctx.HARVESTSYSTEMS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportHarvest(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportHarvest(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Harvestsystem cs = new Harvestsystem();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Harvestsystems.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessLandForm(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Land Form Tables ...");
            ctx.Landforms.RemoveRange(ctx.Landforms.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.LANDFORMS.RemoveRange(ctx.LANDFORMS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportLandForm(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportLandForm(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Landform cs = new Landform();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Landforms.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessNonStocked(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Non-Stocked Tables ...");
            ctx.Nonstockeds.RemoveRange(ctx.Nonstockeds.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.NONSTOCKED.RemoveRange(ctx.NONSTOCKED);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportNonStocked(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportNonStocked(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Nonstocked cs = new Nonstocked();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.Code = _mdbReader["Code"].ToString().Trim();
                cs.Catagory = _mdbReader["Catagory"].ToString().Trim();
                ctx.Nonstockeds.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessNonTimbered(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Non-Timbered Tables ...");
            ctx.Nontimbereds.RemoveRange(ctx.Nontimbereds.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.NONTIMBERED.RemoveRange(ctx.NONTIMBERED);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportNonTimbered(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportNonTimbered(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Nontimbered cs = new Nontimbered();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.Code = _mdbReader["Code"].ToString().Trim();
                cs.Catagory = _mdbReader["Catagory"].ToString().Trim();
                ctx.Nontimbereds.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessPrice(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Price Tables ...");
            ctx.Prices.RemoveRange(ctx.Prices.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.Price.RemoveRange(ctx.Price);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportPrice(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportPrice(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Price cs = new Price();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Destination = _mdbReader["Destination"].ToString().Trim();
                cs.Grades = _mdbReader["Grades"].ToString().Trim();
                cs.MaximumDiameter = Utils.ConvertToDouble(_mdbReader["Max Dia"].ToString());
                cs.MinimumDiameter = Utils.ConvertToDouble(_mdbReader["Min Dia"].ToString());
                cs.ProductName = _mdbReader["Product Name"].ToString().Trim();
                cs.PriceByLengthCategory1 = 0;
                cs.PriceByLengthCategory2 = 0;
                cs.PriceByLengthCategory3 = 0;
                cs.PriceByLengthCategory4 = 0;
                cs.PriceByLengthCategory5 = 0;
                cs.PriceDate = DateTime.Now;
                cs.SawUtil = "";
                cs.Sorts = _mdbReader["Sorts"].ToString().Trim();
                cs.Species = _mdbReader["Species"].ToString().Trim();
                cs.Units = _mdbReader["Rate Type"].ToString().Trim();
                ctx.Prices.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessRoads(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Roads Tables ...");
            ctx.Roads.RemoveRange(ctx.Roads.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.ROADS.RemoveRange(ctx.ROADS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportRoads(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportRoads(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Road cs = new Road();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Roads.Add(cs);
            }
            ctx.SaveChanges();
        }

        //////////////
        public bool ProcessSeedZone(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Seed Zone Tables ...");
            ctx.Seedzones.RemoveRange(ctx.Seedzones.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.SEEDZONES.RemoveRange(ctx.SEEDZONES);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportSeedZone(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportSeedZone(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Seedzone cs = new Seedzone();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Seedzones.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessSlash(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Slash Tables ...");
            ctx.Slashdistributions.RemoveRange(ctx.Slashdistributions.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.SLASHDISTRIBUTIONS.RemoveRange(ctx.SLASHDISTRIBUTIONS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportSlash(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportSlash(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Slashdistribution cs = new Slashdistribution();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Slashdistributions.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessSoils(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Soils Tables ...");
            ctx.Soils.RemoveRange(ctx.Soils.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.SOILS.RemoveRange(ctx.SOILS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportSoils(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportSoils(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Soil cs = new Soil();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Soils.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessTreatment(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Treatment Tables ...");
            ctx.Treatments.RemoveRange(ctx.Treatments.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.TREATMENTS.RemoveRange(ctx.TREATMENTS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportTreatment(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportTreatment(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Treatment cs = new Treatment();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Treatments.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessStreams(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Stream Tables ...");
            ctx.Streams.RemoveRange(ctx.Streams.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.STREAMS.RemoveRange(ctx.STREAMS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportStreams(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportStreams(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Stream cs = new Stream();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Streams.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessTreeSource(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Tree Source Tables ...");
            ctx.Treesources.RemoveRange(ctx.Treesources.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.TreeOURCES.RemoveRange(ctx.TreeOURCES);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportTreeSource(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportTreeSource(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Treesource cs = new Treesource();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Treesources.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessTreeStatus(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Tree Status Tables ...");
            ctx.Treestatus.RemoveRange(ctx.Treestatus.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.TreeTATUS.RemoveRange(ctx.TreeTATUS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportTreeStatus(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportTreeStatus(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Treestatu cs = new Treestatu();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Treestatus.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessUserDefined(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("User Defined Tables ...");
            ctx.Userdefineds.RemoveRange(ctx.Userdefineds.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.USERDEFINED.RemoveRange(ctx.USERDEFINED);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportUserDefined(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportUserDefined(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Userdefined cs = new Userdefined();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Userdefineds.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessVegetation(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Vegetation Tables ...");
            ctx.Vegetations.RemoveRange(ctx.Vegetations.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.VEGETATIONS.RemoveRange(ctx.VEGETATIONS);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportVegetation(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportVegetation(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Vegetation cs = new Vegetation();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Vegetations.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessWoodType(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Wood Type Tables ...");
            ctx.Woodtypes.RemoveRange(ctx.Woodtypes.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.WOODTYPE.RemoveRange(ctx.WOODTYPE);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportWoodType(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportWoodType(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Woodtype cs = new Woodtype();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.Abbrv = "";
                cs.ConiferHardwood = "";
                cs.PrintOrder = 0;
                ctx.Woodtypes.Add(cs);
            }
            ctx.SaveChanges();
        }

        public bool ProcessVigor(ref ProjectDbContext ctx, string pDb, string pTable, string pTableName)
        {
            SplashScreenManager.Default.SetWaitFormDescription("Vigor Tables ...");
            ctx.Vigors.RemoveRange(ctx.Vigors.Where(t => t.TableName == pTableName).ToList());
            ctx.SaveChanges();
            OpenFile(pDb, pTable, "[Table Name]", "Table Name", pTableName);
            //ctx.VIGOR.RemoveRange(ctx.VIGOR);
            //OpenFile(pDb, pTable, "[Table Name]", "", "");
            ImportVigor(ref ctx);
            CloseFile();
            return true;
        }

        private void ImportVigor(ref ProjectDbContext ctx)
        {
            int order = 0;
            while (_mdbReader.Read())
            {
                Vigor cs = new Vigor();
                cs.TableName = _mdbReader["Table Name"].ToString().Trim();
                cs.Description = _mdbReader["Description"].ToString().Trim();
                cs.DisplayCode = _mdbReader["Code"].ToString().Trim();
                cs.DisplayOrder = 0;
                ctx.Vigors.Add(cs);
            }
            ctx.SaveChanges();
        }
    }
}
