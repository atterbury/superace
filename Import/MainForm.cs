﻿using FluentData;
using ProjectModel;
using ProjectModel.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuperACEImport
{
    public partial class MainForm : Telerik.WinControls.UI.RadForm
    {
        private OleDbConnection mdbConn = null;
        private OleDbCommand mdbCmd = null;
        private OleDbDataReader mdbReader = null;
        private DataTable mdbTbl = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void radMenuItem2_Click(object sender, EventArgs e)
        {
            // Exit
            this.Close();
        }

        private void radMenuItem4_Click(object sender, EventArgs e)
        {
            // SuperACE 2008
        }

        private void radMenuItem5_Click(object sender, EventArgs e)
        {
            // PocketEASY
        }

        private void radMenuItem6_Click(object sender, EventArgs e)
        {
            // StandInput
            ImportStandInputForm frm = new ImportStandInputForm();
            frm.ShowDialog();
            if (frm.bProcess)
            {
                string flipsDb = frm.flipsFileName;
                string defaultsDb = frm.defaultsFileName;
                string projectDb = frm.projectFileName;
                string oldProject = frm.oldProjectFileName;

                //InProcessDialog.ShowDialog("Importing Stand Input ...");
                OpenFile(projectDb, "TSTNDSPC", "[Township], [Range], [Section], [Type]", "", "");
                ImportStandInput(frm.currentLocation + frm.currentProject + ".sdf");
                //InProcessDialog.CloseDialog();
                CloseFile();
            }
            frm.Dispose();
        }

        private void OpenFile(string pDb, string pTable, string pOrderBy, string pFieldName, string pFieldValue)
        {
            //string sDir = sSourceFile.Substring(0, sSourceFile.LastIndexOf('\\'));
            //string sFile = sSourceFile.Substring(sSourceFile.LastIndexOf('\\') + 1, sSourceFile.LastIndexOf('.') - (sSourceFile.LastIndexOf('\\') + 1));
            //string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}", pDb);
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", pDb);
            try
            {
                mdbConn = new OleDbConnection(connectionString);

                mdbConn.Open();

                string sql = string.Empty;
                if (!string.IsNullOrEmpty(pFieldName) && !string.IsNullOrEmpty(pFieldValue))
                    sql = string.Format("SELECT * FROM {0} WHERE [{1}] = '{2}'", pTable, pFieldName, pFieldValue);
                else
                    if (!string.IsNullOrEmpty(pOrderBy))
                        sql = string.Format("SELECT * FROM {0} ORDER BY {1}", pTable, pOrderBy);
                    else
                        sql = string.Format("SELECT * FROM {0}", pTable);
                mdbCmd = new OleDbCommand(sql, mdbConn);
                mdbReader = mdbCmd.ExecuteReader();
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (OleDbException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch
            {
                MessageBox.Show("Error Opening database");
            }
        }

        private void CloseFile()
        {
            //mdbTbl.Dispose();
            mdbReader.Close();
            mdbCmd.Dispose();
            mdbConn.Close();
        }

        private void ImportStandInput(string pDataSource)
        {
            string currentKey = string.Empty;
            string previousKey = string.Empty;
            ProjectModel.STANDS stand = null;

            var ctx = new ProjectModelEntities(ProjectBLL.GetConnectionString(pDataSource));
            while (mdbReader.Read())
            {
                currentKey = mdbReader["Township"].ToString() + mdbReader["Range"].ToString() + mdbReader["Section"].ToString() + mdbReader["Type"].ToString();
                if (currentKey != previousKey)
                {
                    string township = mdbReader["Township"].ToString();
                    string range = mdbReader["Range"].ToString();
                    string section = mdbReader["Section"].ToString();
                    string type = mdbReader["Type"].ToString();
                    stand = ctx.STANDS.FirstOrDefault(s => s.Township == township && s.Range == range && s.Section == section && s.StandName == type);
                    previousKey = currentKey;
                }
                ProjectModel.StandInput si = new ProjectModel.StandInput();
                si.StandsID = stand.StandsID;
                si.Age = Convert.ToInt16(mdbReader["Age"].ToString());
                si.BasalArea = Convert.ToSingle(mdbReader["Basal Area"].ToString());
                si.BirthYear = Convert.ToInt16(mdbReader["Birth Year"].ToString());
                si.Cost = Convert.ToSingle(mdbReader["Cost"].ToString());
                si.D4h = Convert.ToInt16(mdbReader["D4h"].ToString());
                si.DCHeight = 0;
                si.GrossBdFt = Convert.ToSingle(mdbReader["Gross BdFt"].ToString());
                si.NetBdFt = Convert.ToSingle(mdbReader["Net BdFt"].ToString());
                si.GrossCuFt = Convert.ToSingle(mdbReader["Gross CuFt"].ToString());
                si.NetCuFt = Convert.ToSingle(mdbReader["Net CuFt"].ToString());
                si.LogsPerAcre = Convert.ToSingle(mdbReader["Logs"].ToString());
                si.OrigAge = Convert.ToInt16(mdbReader["OrigAge"].ToString());
                si.OrigBasalArea = Convert.ToSingle(mdbReader["OrigBasalArea"].ToString());
                si.OrigD4h = Convert.ToInt16(mdbReader["OrigD4h"].ToString());
                si.OrigDCHeight = 0;
                si.OrigGrossBdFt = Convert.ToSingle(mdbReader["OrigGrossBdFt"].ToString());
                si.OrigNetBdFt = Convert.ToSingle(mdbReader["OrigNetBdFt"].ToString());
                si.OrigGrossCuFt = Convert.ToSingle(mdbReader["OrigGrossCuFt"].ToString());
                si.OrigNetCuFt = Convert.ToSingle(mdbReader["OrigNetCuFt"].ToString());
                si.OrigLogDib = Convert.ToInt16(mdbReader["Av Log Dib"].ToString());
                si.OrigLogLen = Convert.ToInt16(mdbReader["Av Log Len"].ToString());
                si.OrigLogsPerAcre = Convert.ToSingle(mdbReader["OrigLogs"].ToString());
                si.OrigStocking = Convert.ToInt16(mdbReader["OrigStocking"].ToString());
                si.OrigTotalHeight = Convert.ToSingle(mdbReader["OrigTotalHeight"].ToString());
                si.OrigTreesPerAcre = Convert.ToSingle(mdbReader["OrigTpa"].ToString());
                si.PoleSeq = Convert.ToInt16(mdbReader["Pole Seq"].ToString());
                si.Revenue = Convert.ToSingle(mdbReader["Revenue"].ToString());
                si.SiteIndex = Convert.ToInt16(mdbReader["Site Index"].ToString());
                si.Species = mdbReader["Species"].ToString();
                si.Status = mdbReader["Status"].ToString();
                si.Stocking = Convert.ToInt16(mdbReader["Stocking"].ToString());
                si.TotalHeight = Convert.ToSingle(mdbReader["Total Height"].ToString());
                si.TreesPerAcre = Convert.ToSingle(mdbReader["Tpa"].ToString());
                ctx.StandInput.Add(si);
            }

            ctx.SaveChanges();

            if (ctx != null)
            {
                ctx.Dispose();
                ctx = null;
            }
        }
    }
}
