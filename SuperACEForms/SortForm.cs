﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class SortForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        //private string dataSource = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public SortForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            //dataSource = pDataSource;
            tableName = pTableName;
        }

        private void SortForm_Load(object sender, EventArgs e)
        {
            comboBoxEditTableName.Text = tableName;

            LoadTableNames();
            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (sortsBindingSource.Count > 0)
                sortsBindingSource.Clear();
            sortsBindingSource.DataSource = ctx.Sorts.OrderBy(t => t.PrimarySpecies).ThenBy(t => t.InputCode).Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", sortsBindingSource.Count);
        }

        private void LoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetSortTableNames(ref ctx);
            comboBoxEditTableName.Properties.Items.Clear();
            foreach (var tblItem in tbls)
                comboBoxEditTableName.Properties.Items.Add(tblItem);
            comboBoxEditTableName.Text = tableName;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            sortsBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
            bool bProcess = false;
            string tableName = string.Empty;

            CopyTableForm frm = new CopyTableForm(ref ctx, "Sort");
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bProcess = frm.bProcess;
            tableName = frm.tableName;
            frm.Dispose();

            List<Sort> coll = ctx.Sorts.Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            foreach (var item in coll)
            {
                Sort newRec = new Sort();
                newRec.Abbreviation = item.Abbreviation;
                newRec.Description = item.Description;
                newRec.InputCode = item.InputCode;
                newRec.KnotsPerFoot = item.KnotsPerFoot;
                newRec.MarketingCategory = item.MarketingCategory;
                newRec.MaximumButtDiameter = item.MaximumDiameter;
                newRec.MaximumKnotSizePerInch = item.MaximumKnotSizePerInch;
                newRec.MinimumBdftPerLog = item.MinimumBdftPerLog;
                newRec.MinimumDiameter = item.MinimumDiameter;
                newRec.MinimumLength = item.MinimumLength;
                newRec.MinimumRingsPerInch = item.MinimumRingsPerInch;
                newRec.PercentSurfaceClear = item.PercentSurfaceClear;
                newRec.PrimarySpecies = item.PrimarySpecies;
                newRec.ProductRecoveryMinimum = item.ProductRecoveryMinimum;
                newRec.SlopeOfGrain = item.SlopeOfGrain;
                newRec.Species = item.Species;
                newRec.TableName = tableName;
                newRec.VolumeUnits = item.VolumeUnits;
                newRec.WeightInPounds = item.WeightInPounds;
                ctx.Sorts.Add(newRec);
            }

            ctx.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditTableName.Text == currentProject.SortsTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditTableName.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;

            ctx.Sorts.RemoveRange(ctx.Sorts.Where(s => s.TableName == comboBoxEditTableName.Text));
            ctx.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault(); ;
            currentProject.SortsTableName = comboBoxEditTableName.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void SpeciesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Add
            AddNewRecord();
        }

        private void AddNewRecord()
        {
            Sort rec = new Sort();
            rec.TableName = tableName;
            ctx.Sorts.Add(rec);
            ctx.SaveChanges();
            LoadData();

            gridView1.FocusedColumn = gridView1.Columns["PrimarySpecies"];
            gridControl1.Focus();
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete
            if (XtraMessageBox.Show("Delete the Select Row(s)?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            int[] selectedRows = gridView1.GetSelectedRows();
            for (int i = 0; i < selectedRows.Length; i++)
            {
                DeleteRecord(selectedRows[i]);
            }
            ctx.SaveChanges();
            LoadData();
        }

        private void DeleteRecord(int pRow)
        {
            sortsBindingSource.Position = pRow;
            Sort rec = sortsBindingSource.Current as Sort;
            ctx.Sorts.Remove(rec);
        }
    }
}