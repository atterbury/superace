﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class CountAvgForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext projectContext = null;
        private Stand stand = null;

        public bool wasProjectChanged = false;

        public CountAvgForm(ref ProjectDbContext pContext, Stand pStand)
        {
            InitializeComponent();

            projectContext = pContext;
            stand = pStand;
        }

        private void CountAvgForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            LoadDataSource();
        }

        private void LoadDataSource()
        {
            if (adjCruisePlotBindingSource.Count > 0)
                adjCruisePlotBindingSource.Clear();
            adjCruisePlotBindingSource.DataSource = projectContext.AdjCruisePlots.OrderBy(t => t.Species).ThenBy(t => t.Status).ThenBy(t => t.QmDbh).Where(t => t.StandsId == stand.StandsId).ToList();
            //barStaticItem6.Caption = string.Format("{0} Records Loaded", adjCruisePlotBindingSource.Count);
        }

        private void CountAvgForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }
    }
}