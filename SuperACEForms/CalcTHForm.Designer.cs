﻿namespace SuperACEForms
{
    partial class CalcTHForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbHeight = new DevExpress.XtraEditors.TextEdit();
            this.tbTop = new DevExpress.XtraEditors.TextEdit();
            this.tbBottom = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbDistance = new DevExpress.XtraEditors.TextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.tbHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBottom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(199, 204);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 25;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(105, 204);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 24;
            this.simpleButton2.Text = "Update";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(11, 204);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 23;
            this.simpleButton1.Text = "Calc";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(69, 145);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(48, 13);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "Height Ft:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(81, 105);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 13);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Top %:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(65, 79);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Bottom %:";
            // 
            // tbHeight
            // 
            this.tbHeight.EnterMoveNextControl = true;
            this.tbHeight.Location = new System.Drawing.Point(143, 142);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(46, 20);
            this.tbHeight.TabIndex = 17;
            this.tbHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbTop
            // 
            this.tbTop.EnterMoveNextControl = true;
            this.tbTop.Location = new System.Drawing.Point(143, 102);
            this.tbTop.Name = "tbTop";
            this.tbTop.Size = new System.Drawing.Size(46, 20);
            this.tbTop.TabIndex = 16;
            this.tbTop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTop.Validated += new System.EventHandler(this.tbTop_Validated);
            // 
            // tbBottom
            // 
            this.tbBottom.EnterMoveNextControl = true;
            this.tbBottom.Location = new System.Drawing.Point(143, 76);
            this.tbBottom.Name = "tbBottom";
            this.tbBottom.Size = new System.Drawing.Size(46, 20);
            this.tbBottom.TabIndex = 15;
            this.tbBottom.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBottom.Validated += new System.EventHandler(this.tbBottom_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(47, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(70, 13);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Distance Feet:";
            // 
            // tbDistance
            // 
            this.tbDistance.EnterMoveNextControl = true;
            this.tbDistance.Location = new System.Drawing.Point(143, 33);
            this.tbDistance.Name = "tbDistance";
            this.tbDistance.Size = new System.Drawing.Size(46, 20);
            this.tbDistance.TabIndex = 13;
            this.tbDistance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbDistance.Validated += new System.EventHandler(this.tbDistance_Validated);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CalcTHForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.tbHeight);
            this.Controls.Add(this.tbTop);
            this.Controls.Add(this.tbBottom);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.tbDistance);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcTHForm";
            this.Text = "Calc Total Height";
            this.Load += new System.EventHandler(this.CalcTHForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBottom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbHeight;
        private DevExpress.XtraEditors.TextEdit tbTop;
        private DevExpress.XtraEditors.TextEdit tbBottom;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbDistance;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}