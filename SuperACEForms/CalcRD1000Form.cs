﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SuperACEUtils;

namespace SuperACEForms
{
    public partial class CalcRD1000Form : DevExpress.XtraEditors.XtraForm
    {
        public bool mUpdate = false;
        public string mDbh = string.Empty;
        public string mFP = string.Empty;
        public string mFF = string.Empty;
        public string mTDF = string.Empty;
        public string mCrownRatio = string.Empty;

        public CalcRD1000Form()
        {
            InitializeComponent();
        }

        private void CalcRD1000Form_Load(object sender, EventArgs e)
        {

        }

        private void tbDbhHeight_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbDbhHeight.Text))
            {
                dxErrorProvider1.SetError(tbDbhHeight, "Invalid Entry");
                tbDbhHeight.Focus();
            }
            else
                dxErrorProvider1.SetError(tbDbhHeight, "");
        }

        private void tbDbhInches_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbDbhInches.Text))
            {
                dxErrorProvider1.SetError(tbDbhInches, "Invalid Entry");
                tbDbhInches.Focus();
            }
            else
            {
                dxErrorProvider1.SetError(tbDbhInches, "");
                Calc();
            }
        }

        private void tbFPHeight_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbFPHeight.Text))
            {
                dxErrorProvider1.SetError(tbFPHeight, "Invalid Entry");
                tbFPHeight.Focus();
            }
            else
                dxErrorProvider1.SetError(tbFPHeight, "");
        }

        private void tbFPInches_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbFPInches.Text))
            {
                dxErrorProvider1.SetError(tbFPInches, "Invalid Entry");
                tbFPInches.Focus();
            }
            else
            {
                dxErrorProvider1.SetError(tbFPInches, "");
                Calc();
            }
        }

        private void tbTDFHeight_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbTDFHeight.Text))
            {
                dxErrorProvider1.SetError(tbTDFHeight, "Invalid Entry");
                tbTDFHeight.Focus();
            }
            else
                dxErrorProvider1.SetError(tbTDFHeight, "");
        }

        private void textEdit1_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbTDFInches.Text))
            {
                dxErrorProvider1.SetError(tbTDFInches, "Invalid Entry");
                tbTDFInches.Focus();
            }
            else
            {
                dxErrorProvider1.SetError(tbTDFInches, "");
                Calc();
            }
        }

        private void tbTotalHeight_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbTotalHeight.Text))
            {
                dxErrorProvider1.SetError(tbTotalHeight, "Invalid Entry");
                tbTotalHeight.Focus();
            }
            else
            {
                dxErrorProvider1.SetError(tbTotalHeight, "");
                Calc();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Calc();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            mUpdate = true;
            Calc();
            mDbh = tbDbhInches.Text;
            mFP = ((int)Utils.ConvertToFloat(tbFPHeight.Text)).ToString();
            mFF = Utils.ConvertToInt(tbFPTDF.Text).ToString();
            mTDF = Utils.ConvertToInt(tbTDFTDF.Text).ToString();
            mCrownRatio = tbCrownRatioTDF.Text;
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            mUpdate = true;
            mUpdate = false;
            mDbh = string.Empty;
            mFP = string.Empty;
            mFF = string.Empty;
            mTDF = string.Empty;
            mCrownRatio = string.Empty;
            this.Close();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void Calc()
        {
            if (Utils.ConvertToFloat(tbDbhInches.Text) > 0)
                tbDbhTDF.Text = string.Format("{0:0.0}", Utils.ConvertToFloat(tbDbhInches.Text));
            else
                tbDbhTDF.Text = string.Format("{0:0.0}", 0.0);
            if (Utils.ConvertToFloat(tbFPInches.Text) > 0 && Utils.ConvertToFloat(tbDbhInches.Text) > 0)
                tbFPTDF.Text = string.Format("{0:0}", (Utils.ConvertToFloat(tbFPInches.Text) / Utils.ConvertToFloat(tbDbhInches.Text)) * 100);
            else
                tbFPTDF.Text = string.Format("{0:0}", 0);
            if (Utils.ConvertToFloat(tbTDFInches.Text) > 0 && Utils.ConvertToFloat(tbFPInches.Text) > 0)
                tbTDFTDF.Text = string.Format("{0:0}", (Utils.ConvertToFloat(tbTDFInches.Text) / Utils.ConvertToFloat(tbFPInches.Text)) * 100);
            else
                tbTDFTDF.Text = string.Format("{0:0}", 0);
            //if (Utils.ConvertToFloat(tbDbhInches.Text) > 0)
            //    tbDbhTDF.Text = tbDbhInches.Text;
            if (Utils.ConvertToFloat(tbTotalHeight.Text) > 0 && Utils.ConvertToFloat(tbCrowBase.Text) > 0)
                tbCrownRatioTDF.Text = string.Format("{0:0}", (Utils.ConvertToFloat(tbTotalHeight.Text) - Utils.ConvertToFloat(tbCrowBase.Text)) / Utils.ConvertToFloat(tbTotalHeight.Text) * 100);
            else
                tbCrownRatioTDF.Text = string.Format("{0:0}", 0);
        }

        private void tbCrowBase_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbCrowBase.Text))
            {
                dxErrorProvider1.SetError(tbCrowBase, "Invalid Entry");
                tbCrowBase.Focus();
            }
            else
            {
                dxErrorProvider1.SetError(tbCrowBase, "");
                Calc();
            }
        }

        private void teStumpHeight_Validated(object sender, EventArgs e)
        {
            tbDbhInches.Focus();
        }
    }
}