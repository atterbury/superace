﻿namespace SuperACEForms
{
    partial class CalcRDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tbHorizDist = new DevExpress.XtraEditors.TextEdit();
            this.tbPercentTopo = new DevExpress.XtraEditors.TextEdit();
            this.tbStumpHeight = new DevExpress.XtraEditors.TextEdit();
            this.tbStumpAngle = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlStatus = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.tbReadAngleDbhBands = new DevExpress.XtraEditors.TextEdit();
            this.tbReadAngleFormPoint = new DevExpress.XtraEditors.TextEdit();
            this.tbInputFormPoint = new DevExpress.XtraEditors.TextEdit();
            this.tbInputDbhBands = new DevExpress.XtraEditors.TextEdit();
            this.tbInputTDBands = new DevExpress.XtraEditors.TextEdit();
            this.tbInputFPBands = new DevExpress.XtraEditors.TextEdit();
            this.tbInputTotalHtAngle = new DevExpress.XtraEditors.TextEdit();
            this.tbInputTDAngleInput = new DevExpress.XtraEditors.TextEdit();
            this.tbInputCrownBaseAngle = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.tbResultsCR = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsTotalHt = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsBoleHt = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsTDF = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsFF = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsFP = new DevExpress.XtraEditors.TextEdit();
            this.tbResultsDbh = new DevExpress.XtraEditors.TextEdit();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.separatorControl2 = new DevExpress.XtraEditors.SeparatorControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbHorizDist.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPercentTopo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStumpHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStumpAngle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReadAngleDbhBands.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReadAngleFormPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputFormPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDbhBands.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTDBands.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputFPBands.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTotalHtAngle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTDAngleInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputCrownBaseAngle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsCR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsTotalHt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsBoleHt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsTDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsFF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsFP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsDbh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(520, 502);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(87, 28);
            this.simpleButton3.TabIndex = 13;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(370, 502);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(87, 28);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "Update";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(220, 502);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(87, 28);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Calc";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl41);
            this.groupControl1.Controls.Add(this.separatorControl2);
            this.groupControl1.Controls.Add(this.separatorControl1);
            this.groupControl1.Controls.Add(this.tbResultsCR);
            this.groupControl1.Controls.Add(this.tbResultsTotalHt);
            this.groupControl1.Controls.Add(this.tbResultsBoleHt);
            this.groupControl1.Controls.Add(this.tbResultsTDF);
            this.groupControl1.Controls.Add(this.tbResultsFF);
            this.groupControl1.Controls.Add(this.tbResultsFP);
            this.groupControl1.Controls.Add(this.tbResultsDbh);
            this.groupControl1.Controls.Add(this.labelControl34);
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.labelControl36);
            this.groupControl1.Controls.Add(this.labelControl37);
            this.groupControl1.Controls.Add(this.labelControl38);
            this.groupControl1.Controls.Add(this.labelControl39);
            this.groupControl1.Controls.Add(this.labelControl40);
            this.groupControl1.Controls.Add(this.tbInputCrownBaseAngle);
            this.groupControl1.Controls.Add(this.tbInputTotalHtAngle);
            this.groupControl1.Controls.Add(this.tbInputTDAngleInput);
            this.groupControl1.Controls.Add(this.tbInputTDBands);
            this.groupControl1.Controls.Add(this.tbInputFPBands);
            this.groupControl1.Controls.Add(this.tbInputFormPoint);
            this.groupControl1.Controls.Add(this.tbInputDbhBands);
            this.groupControl1.Controls.Add(this.tbReadAngleFormPoint);
            this.groupControl1.Controls.Add(this.tbReadAngleDbhBands);
            this.groupControl1.Controls.Add(this.labelControl33);
            this.groupControl1.Controls.Add(this.labelControl32);
            this.groupControl1.Controls.Add(this.labelControl31);
            this.groupControl1.Controls.Add(this.labelControl30);
            this.groupControl1.Controls.Add(this.labelControl29);
            this.groupControl1.Controls.Add(this.labelControl28);
            this.groupControl1.Controls.Add(this.labelControl27);
            this.groupControl1.Controls.Add(this.labelControl26);
            this.groupControl1.Controls.Add(this.labelControl25);
            this.groupControl1.Controls.Add(this.labelControlStatus);
            this.groupControl1.Controls.Add(this.labelControl24);
            this.groupControl1.Controls.Add(this.labelControl23);
            this.groupControl1.Controls.Add(this.labelControl22);
            this.groupControl1.Controls.Add(this.labelControl21);
            this.groupControl1.Controls.Add(this.tbStumpAngle);
            this.groupControl1.Controls.Add(this.tbStumpHeight);
            this.groupControl1.Controls.Add(this.tbPercentTopo);
            this.groupControl1.Controls.Add(this.tbHorizDist);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(595, 451);
            this.groupControl1.TabIndex = 88;
            this.groupControl1.Text = "Measure Trees From Distance American Scale Relaskop";
            // 
            // tbHorizDist
            // 
            this.tbHorizDist.EditValue = "";
            this.tbHorizDist.EnterMoveNextControl = true;
            this.tbHorizDist.Location = new System.Drawing.Point(14, 43);
            this.tbHorizDist.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbHorizDist.Name = "tbHorizDist";
            this.tbHorizDist.Size = new System.Drawing.Size(62, 22);
            this.tbHorizDist.TabIndex = 1;
            this.tbHorizDist.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbHorizDist.Validated += new System.EventHandler(this.tbHorizDist_Validated);
            // 
            // tbPercentTopo
            // 
            this.tbPercentTopo.EditValue = "";
            this.tbPercentTopo.EnterMoveNextControl = true;
            this.tbPercentTopo.Location = new System.Drawing.Point(14, 73);
            this.tbPercentTopo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPercentTopo.Name = "tbPercentTopo";
            this.tbPercentTopo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbPercentTopo.Size = new System.Drawing.Size(62, 22);
            this.tbPercentTopo.TabIndex = 2;
            this.tbPercentTopo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbPercentTopo.Validated += new System.EventHandler(this.tbPercentTopo_Validated);
            // 
            // tbStumpHeight
            // 
            this.tbStumpHeight.EditValue = "";
            this.tbStumpHeight.EnterMoveNextControl = true;
            this.tbStumpHeight.Location = new System.Drawing.Point(14, 103);
            this.tbStumpHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStumpHeight.Name = "tbStumpHeight";
            this.tbStumpHeight.Size = new System.Drawing.Size(62, 22);
            this.tbStumpHeight.TabIndex = 3;
            this.tbStumpHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStumpHeight.Validated += new System.EventHandler(this.tbStumpHeight_Validated);
            // 
            // tbStumpAngle
            // 
            this.tbStumpAngle.EditValue = "";
            this.tbStumpAngle.EnterMoveNextControl = true;
            this.tbStumpAngle.Location = new System.Drawing.Point(14, 133);
            this.tbStumpAngle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStumpAngle.Name = "tbStumpAngle";
            this.tbStumpAngle.Size = new System.Drawing.Size(62, 22);
            this.tbStumpAngle.TabIndex = 4;
            this.tbStumpAngle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStumpAngle.Validated += new System.EventHandler(this.tbStumpAngle_Validated);
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(92, 46);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(259, 16);
            this.labelControl21.TabIndex = 41;
            this.labelControl21.Text = "Input Horizontal Distance Center of Tree Feet";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(92, 76);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(211, 16);
            this.labelControl22.TabIndex = 42;
            this.labelControl22.Text = "P = Percent Scale or T = Topo Scale";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(92, 106);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(77, 16);
            this.labelControl23.TabIndex = 43;
            this.labelControl23.Text = "Stump Height";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(92, 136);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(73, 16);
            this.labelControl24.TabIndex = 44;
            this.labelControl24.Text = "Stump Angle";
            // 
            // labelControlStatus
            // 
            this.labelControlStatus.Location = new System.Drawing.Point(180, 136);
            this.labelControlStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControlStatus.Name = "labelControlStatus";
            this.labelControlStatus.Size = new System.Drawing.Size(0, 16);
            this.labelControlStatus.TabIndex = 45;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Options.UseTextOptions = true;
            this.labelControl25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl25.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl25.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl25.Location = new System.Drawing.Point(122, 210);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(52, 32);
            this.labelControl25.TabIndex = 65;
            this.labelControl25.Text = "Dbh Bands";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Options.UseTextOptions = true;
            this.labelControl26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl26.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl26.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl26.Location = new System.Drawing.Point(189, 210);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(52, 32);
            this.labelControl26.TabIndex = 66;
            this.labelControl26.Text = "Form Point";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Options.UseTextOptions = true;
            this.labelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl27.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl27.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl27.Location = new System.Drawing.Point(262, 210);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(42, 32);
            this.labelControl27.TabIndex = 67;
            this.labelControl27.Text = "FP Bands";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Options.UseTextOptions = true;
            this.labelControl28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl28.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl28.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl28.Location = new System.Drawing.Point(327, 210);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(45, 32);
            this.labelControl28.TabIndex = 68;
            this.labelControl28.Text = "TD Bands";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Options.UseTextOptions = true;
            this.labelControl29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl29.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl29.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl29.Location = new System.Drawing.Point(393, 210);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(52, 32);
            this.labelControl29.TabIndex = 69;
            this.labelControl29.Text = "TD Angle Input";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Options.UseTextOptions = true;
            this.labelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl30.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl30.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl30.Location = new System.Drawing.Point(461, 210);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(52, 32);
            this.labelControl30.TabIndex = 70;
            this.labelControl30.Text = "Total Ht Angle";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Options.UseTextOptions = true;
            this.labelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl31.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl31.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl31.Location = new System.Drawing.Point(530, 194);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(52, 48);
            this.labelControl31.TabIndex = 71;
            this.labelControl31.Text = "Crown Base Angle";
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(14, 256);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(70, 16);
            this.labelControl32.TabIndex = 72;
            this.labelControl32.Text = "Read Angle:";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(50, 292);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(34, 16);
            this.labelControl33.TabIndex = 73;
            this.labelControl33.Text = "Input:";
            // 
            // tbReadAngleDbhBands
            // 
            this.tbReadAngleDbhBands.EditValue = "";
            this.tbReadAngleDbhBands.EnterMoveNextControl = true;
            this.tbReadAngleDbhBands.Location = new System.Drawing.Point(125, 250);
            this.tbReadAngleDbhBands.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbReadAngleDbhBands.Name = "tbReadAngleDbhBands";
            this.tbReadAngleDbhBands.Properties.ReadOnly = true;
            this.tbReadAngleDbhBands.Size = new System.Drawing.Size(42, 22);
            this.tbReadAngleDbhBands.TabIndex = 74;
            // 
            // tbReadAngleFormPoint
            // 
            this.tbReadAngleFormPoint.EditValue = "";
            this.tbReadAngleFormPoint.EnterMoveNextControl = true;
            this.tbReadAngleFormPoint.Location = new System.Drawing.Point(193, 250);
            this.tbReadAngleFormPoint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbReadAngleFormPoint.Name = "tbReadAngleFormPoint";
            this.tbReadAngleFormPoint.Properties.ReadOnly = true;
            this.tbReadAngleFormPoint.Size = new System.Drawing.Size(42, 22);
            this.tbReadAngleFormPoint.TabIndex = 75;
            // 
            // tbInputFormPoint
            // 
            this.tbInputFormPoint.EditValue = "";
            this.tbInputFormPoint.EnterMoveNextControl = true;
            this.tbInputFormPoint.Location = new System.Drawing.Point(193, 289);
            this.tbInputFormPoint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputFormPoint.Name = "tbInputFormPoint";
            this.tbInputFormPoint.Size = new System.Drawing.Size(42, 22);
            this.tbInputFormPoint.TabIndex = 77;
            this.tbInputFormPoint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputFormPoint.Validated += new System.EventHandler(this.tbInputFormPoint_Validated);
            // 
            // tbInputDbhBands
            // 
            this.tbInputDbhBands.EditValue = "";
            this.tbInputDbhBands.EnterMoveNextControl = true;
            this.tbInputDbhBands.Location = new System.Drawing.Point(125, 289);
            this.tbInputDbhBands.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputDbhBands.Name = "tbInputDbhBands";
            this.tbInputDbhBands.Size = new System.Drawing.Size(42, 22);
            this.tbInputDbhBands.TabIndex = 76;
            this.tbInputDbhBands.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputDbhBands.Validated += new System.EventHandler(this.tbInputDbhBands_Validated);
            // 
            // tbInputTDBands
            // 
            this.tbInputTDBands.EditValue = "";
            this.tbInputTDBands.EnterMoveNextControl = true;
            this.tbInputTDBands.Location = new System.Drawing.Point(330, 289);
            this.tbInputTDBands.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputTDBands.Name = "tbInputTDBands";
            this.tbInputTDBands.Size = new System.Drawing.Size(42, 22);
            this.tbInputTDBands.TabIndex = 79;
            this.tbInputTDBands.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputTDBands.Validated += new System.EventHandler(this.tbInputTDBands_Validated);
            // 
            // tbInputFPBands
            // 
            this.tbInputFPBands.EditValue = "";
            this.tbInputFPBands.EnterMoveNextControl = true;
            this.tbInputFPBands.Location = new System.Drawing.Point(262, 289);
            this.tbInputFPBands.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputFPBands.Name = "tbInputFPBands";
            this.tbInputFPBands.Size = new System.Drawing.Size(42, 22);
            this.tbInputFPBands.TabIndex = 78;
            this.tbInputFPBands.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputFPBands.Validated += new System.EventHandler(this.tbInputFPBands_Validated);
            // 
            // tbInputTotalHtAngle
            // 
            this.tbInputTotalHtAngle.EditValue = "";
            this.tbInputTotalHtAngle.EnterMoveNextControl = true;
            this.tbInputTotalHtAngle.Location = new System.Drawing.Point(466, 289);
            this.tbInputTotalHtAngle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputTotalHtAngle.Name = "tbInputTotalHtAngle";
            this.tbInputTotalHtAngle.Size = new System.Drawing.Size(42, 22);
            this.tbInputTotalHtAngle.TabIndex = 81;
            this.tbInputTotalHtAngle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputTotalHtAngle.Validated += new System.EventHandler(this.tbInputTotalHtAngle_Validated);
            // 
            // tbInputTDAngleInput
            // 
            this.tbInputTDAngleInput.EditValue = "";
            this.tbInputTDAngleInput.EnterMoveNextControl = true;
            this.tbInputTDAngleInput.Location = new System.Drawing.Point(398, 289);
            this.tbInputTDAngleInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputTDAngleInput.Name = "tbInputTDAngleInput";
            this.tbInputTDAngleInput.Size = new System.Drawing.Size(42, 22);
            this.tbInputTDAngleInput.TabIndex = 80;
            this.tbInputTDAngleInput.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputTDAngleInput.Validated += new System.EventHandler(this.tbInputTDAngleInput_Validated);
            // 
            // tbInputCrownBaseAngle
            // 
            this.tbInputCrownBaseAngle.EditValue = "";
            this.tbInputCrownBaseAngle.EnterMoveNextControl = true;
            this.tbInputCrownBaseAngle.Location = new System.Drawing.Point(534, 289);
            this.tbInputCrownBaseAngle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputCrownBaseAngle.Name = "tbInputCrownBaseAngle";
            this.tbInputCrownBaseAngle.Size = new System.Drawing.Size(42, 22);
            this.tbInputCrownBaseAngle.TabIndex = 82;
            this.tbInputCrownBaseAngle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbInputCrownBaseAngle.Validated += new System.EventHandler(this.tbCrownBaseAngle_Validated);
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Options.UseTextOptions = true;
            this.labelControl34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl34.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl34.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl34.Location = new System.Drawing.Point(524, 368);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(52, 16);
            this.labelControl34.TabIndex = 89;
            this.labelControl34.Text = "CR";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Options.UseTextOptions = true;
            this.labelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl35.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl35.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl35.Location = new System.Drawing.Point(455, 368);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(52, 16);
            this.labelControl35.TabIndex = 88;
            this.labelControl35.Text = "Total Ht";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Options.UseTextOptions = true;
            this.labelControl36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl36.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl36.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl36.Location = new System.Drawing.Point(387, 368);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(52, 16);
            this.labelControl36.TabIndex = 87;
            this.labelControl36.Text = "Bole Ht";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Options.UseTextOptions = true;
            this.labelControl37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl37.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl37.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl37.Location = new System.Drawing.Point(321, 368);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(52, 16);
            this.labelControl37.TabIndex = 86;
            this.labelControl37.Text = "TDF";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Options.UseTextOptions = true;
            this.labelControl38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl38.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl38.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl38.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl38.Location = new System.Drawing.Point(256, 368);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(42, 16);
            this.labelControl38.TabIndex = 85;
            this.labelControl38.Text = "FF";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Options.UseTextOptions = true;
            this.labelControl39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl39.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl39.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl39.Location = new System.Drawing.Point(183, 368);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(52, 16);
            this.labelControl39.TabIndex = 84;
            this.labelControl39.Text = "FP";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Options.UseTextOptions = true;
            this.labelControl40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl40.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl40.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl40.Location = new System.Drawing.Point(116, 368);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(52, 16);
            this.labelControl40.TabIndex = 83;
            this.labelControl40.Text = "Dbh";
            // 
            // tbResultsCR
            // 
            this.tbResultsCR.EditValue = "";
            this.tbResultsCR.EnterMoveNextControl = true;
            this.tbResultsCR.Location = new System.Drawing.Point(530, 397);
            this.tbResultsCR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsCR.Name = "tbResultsCR";
            this.tbResultsCR.Properties.ReadOnly = true;
            this.tbResultsCR.Size = new System.Drawing.Size(42, 22);
            this.tbResultsCR.TabIndex = 96;
            // 
            // tbResultsTotalHt
            // 
            this.tbResultsTotalHt.EditValue = "";
            this.tbResultsTotalHt.EnterMoveNextControl = true;
            this.tbResultsTotalHt.Location = new System.Drawing.Point(462, 397);
            this.tbResultsTotalHt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsTotalHt.Name = "tbResultsTotalHt";
            this.tbResultsTotalHt.Properties.ReadOnly = true;
            this.tbResultsTotalHt.Size = new System.Drawing.Size(42, 22);
            this.tbResultsTotalHt.TabIndex = 95;
            // 
            // tbResultsBoleHt
            // 
            this.tbResultsBoleHt.EditValue = "";
            this.tbResultsBoleHt.EnterMoveNextControl = true;
            this.tbResultsBoleHt.Location = new System.Drawing.Point(394, 397);
            this.tbResultsBoleHt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsBoleHt.Name = "tbResultsBoleHt";
            this.tbResultsBoleHt.Properties.ReadOnly = true;
            this.tbResultsBoleHt.Size = new System.Drawing.Size(42, 22);
            this.tbResultsBoleHt.TabIndex = 94;
            // 
            // tbResultsTDF
            // 
            this.tbResultsTDF.EditValue = "";
            this.tbResultsTDF.EnterMoveNextControl = true;
            this.tbResultsTDF.Location = new System.Drawing.Point(326, 397);
            this.tbResultsTDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsTDF.Name = "tbResultsTDF";
            this.tbResultsTDF.Properties.ReadOnly = true;
            this.tbResultsTDF.Size = new System.Drawing.Size(42, 22);
            this.tbResultsTDF.TabIndex = 93;
            // 
            // tbResultsFF
            // 
            this.tbResultsFF.EditValue = "";
            this.tbResultsFF.EnterMoveNextControl = true;
            this.tbResultsFF.Location = new System.Drawing.Point(258, 397);
            this.tbResultsFF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsFF.Name = "tbResultsFF";
            this.tbResultsFF.Properties.ReadOnly = true;
            this.tbResultsFF.Size = new System.Drawing.Size(42, 22);
            this.tbResultsFF.TabIndex = 92;
            // 
            // tbResultsFP
            // 
            this.tbResultsFP.EditValue = "";
            this.tbResultsFP.EnterMoveNextControl = true;
            this.tbResultsFP.Location = new System.Drawing.Point(189, 397);
            this.tbResultsFP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsFP.Name = "tbResultsFP";
            this.tbResultsFP.Properties.ReadOnly = true;
            this.tbResultsFP.Size = new System.Drawing.Size(42, 22);
            this.tbResultsFP.TabIndex = 91;
            // 
            // tbResultsDbh
            // 
            this.tbResultsDbh.EditValue = "";
            this.tbResultsDbh.EnterMoveNextControl = true;
            this.tbResultsDbh.Location = new System.Drawing.Point(121, 397);
            this.tbResultsDbh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbResultsDbh.Name = "tbResultsDbh";
            this.tbResultsDbh.Properties.ReadOnly = true;
            this.tbResultsDbh.Size = new System.Drawing.Size(42, 22);
            this.tbResultsDbh.TabIndex = 90;
            // 
            // separatorControl1
            // 
            this.separatorControl1.LineColor = System.Drawing.Color.Gray;
            this.separatorControl1.Location = new System.Drawing.Point(5, 162);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(586, 23);
            this.separatorControl1.TabIndex = 97;
            // 
            // separatorControl2
            // 
            this.separatorControl2.LineColor = System.Drawing.Color.Gray;
            this.separatorControl2.Location = new System.Drawing.Point(5, 327);
            this.separatorControl2.Name = "separatorControl2";
            this.separatorControl2.Size = new System.Drawing.Size(586, 23);
            this.separatorControl2.TabIndex = 98;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(14, 400);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(53, 16);
            this.labelControl41.TabIndex = 99;
            this.labelControl41.Text = "Results:";
            // 
            // CalcRDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 553);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcRDForm";
            this.Text = "Calc RD";
            this.Load += new System.EventHandler(this.CalcRDForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbHorizDist.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPercentTopo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStumpHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStumpAngle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReadAngleDbhBands.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReadAngleFormPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputFormPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDbhBands.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTDBands.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputFPBands.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTotalHtAngle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputTDAngleInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputCrownBaseAngle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsCR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsTotalHt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsBoleHt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsTDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsFF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsFP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbResultsDbh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.SeparatorControl separatorControl2;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private DevExpress.XtraEditors.TextEdit tbResultsCR;
        private DevExpress.XtraEditors.TextEdit tbResultsTotalHt;
        private DevExpress.XtraEditors.TextEdit tbResultsBoleHt;
        private DevExpress.XtraEditors.TextEdit tbResultsTDF;
        private DevExpress.XtraEditors.TextEdit tbResultsFF;
        private DevExpress.XtraEditors.TextEdit tbResultsFP;
        private DevExpress.XtraEditors.TextEdit tbResultsDbh;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit tbInputCrownBaseAngle;
        private DevExpress.XtraEditors.TextEdit tbInputTotalHtAngle;
        private DevExpress.XtraEditors.TextEdit tbInputTDAngleInput;
        private DevExpress.XtraEditors.TextEdit tbInputTDBands;
        private DevExpress.XtraEditors.TextEdit tbInputFPBands;
        private DevExpress.XtraEditors.TextEdit tbInputFormPoint;
        private DevExpress.XtraEditors.TextEdit tbInputDbhBands;
        private DevExpress.XtraEditors.TextEdit tbReadAngleFormPoint;
        private DevExpress.XtraEditors.TextEdit tbReadAngleDbhBands;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControlStatus;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit tbStumpAngle;
        private DevExpress.XtraEditors.TextEdit tbStumpHeight;
        private DevExpress.XtraEditors.TextEdit tbPercentTopo;
        private DevExpress.XtraEditors.TextEdit tbHorizDist;
    }
}