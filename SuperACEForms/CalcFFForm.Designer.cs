﻿namespace SuperACEForms
{
    partial class CalcFFForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbFF = new DevExpress.XtraEditors.TextEdit();
            this.tbFP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbDbh = new DevExpress.XtraEditors.TextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.tbFF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(198, 157);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 25;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(104, 157);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 24;
            this.simpleButton2.Text = "Update";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(10, 157);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 23;
            this.simpleButton1.Text = "Calc";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(59, 105);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 13);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Form Factor:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(29, 62);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(92, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Dia or Bands @ FP:";
            // 
            // tbFF
            // 
            this.tbFF.EnterMoveNextControl = true;
            this.tbFF.Location = new System.Drawing.Point(143, 102);
            this.tbFF.Name = "tbFF";
            this.tbFF.Size = new System.Drawing.Size(46, 20);
            this.tbFF.TabIndex = 16;
            this.tbFF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbFP
            // 
            this.tbFP.EnterMoveNextControl = true;
            this.tbFP.Location = new System.Drawing.Point(143, 59);
            this.tbFP.Name = "tbFP";
            this.tbFP.Size = new System.Drawing.Size(46, 20);
            this.tbFP.TabIndex = 15;
            this.tbFP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbFP.Validated += new System.EventHandler(this.tbFP_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(22, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 13);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Dia or Bands @ Dbh:";
            // 
            // tbDbh
            // 
            this.tbDbh.EnterMoveNextControl = true;
            this.tbDbh.Location = new System.Drawing.Point(143, 33);
            this.tbDbh.Name = "tbDbh";
            this.tbDbh.Size = new System.Drawing.Size(46, 20);
            this.tbDbh.TabIndex = 13;
            this.tbDbh.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbDbh.Validated += new System.EventHandler(this.tbDbh_Validated);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CalcFFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 205);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.tbFF);
            this.Controls.Add(this.tbFP);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.tbDbh);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcFFForm";
            this.Text = "Calc FF";
            this.Load += new System.EventHandler(this.CalcFFForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbFF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbFF;
        private DevExpress.XtraEditors.TextEdit tbFP;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbDbh;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}