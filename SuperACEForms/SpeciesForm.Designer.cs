﻿namespace SuperACEForms
{
    partial class SpeciesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEditTableName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tblBindingSource = new System.Windows.Forms.BindingSource();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbbreviation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarkRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinLen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxLen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYieldTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colFormFactorGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAsubOTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colFormFactorsTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormFactorsDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colComponentsTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComponentsDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colSpeciesGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarkFactorsTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarkFactorsDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBioMass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentCarbon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentDryWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodTypeTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrowthModelTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBoardFootRuleTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCubicFootRuleTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAsubODisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colMaxTreeDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodTypeDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colBdFtDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colCuFtDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colBarkRatioDisplayCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLbs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLbsType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommanName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScientificName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesConversion = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barLargeButtonItem1,
            this.barButtonItem10,
            this.barButtonItem11});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 38;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Save";
            this.barButtonItem6.Id = 15;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save To New Table";
            this.barButtonItem7.Id = 16;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Delete Table";
            this.barButtonItem8.Id = 17;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Set as Default";
            this.barButtonItem9.Id = 18;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 4";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.Text = "Custom 4";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Add";
            this.barButtonItem10.Id = 36;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Delete";
            this.barButtonItem11.Id = 37;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 609);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 558);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 51);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 558);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboBoxEditTableName);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 51);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 55);
            this.panelControl1.TabIndex = 4;
            // 
            // comboBoxEditTableName
            // 
            this.comboBoxEditTableName.Location = new System.Drawing.Point(93, 19);
            this.comboBoxEditTableName.MenuManager = this.barManager1;
            this.comboBoxEditTableName.Name = "comboBoxEditTableName";
            this.comboBoxEditTableName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTableName.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditTableName.TabIndex = 1;
            this.comboBoxEditTableName.SelectedValueChanged += new System.EventHandler(this.comboBoxEditTableName_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Table Name:";
            // 
            // tblBindingSource
            // 
            this.tblBindingSource.DataSource = typeof(Project.DAL.Species);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tblBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 106);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox9,
            this.repositoryItemComboBox10,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1011, 503);
            this.gridControl1.TabIndex = 10;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 60;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeciesID,
            this.colInputCode,
            this.colAbbreviation,
            this.colDescription,
            this.colBarkRatio,
            this.colMinDia,
            this.colMinLen,
            this.colMaxDia,
            this.colMaxLen,
            this.colMaxHeight,
            this.colTrim,
            this.colWeight,
            this.colYieldTableName,
            this.colFormFactorGroupID,
            this.colAsubOTableName,
            this.colTableName,
            this.colWeightUnits,
            this.colFormFactorsTableName,
            this.colFormFactorsDisplayCode,
            this.colComponentsTableName,
            this.colComponentsDisplayCode,
            this.colSpeciesGroup,
            this.colBarkFactorsTableName,
            this.colBarkFactorsDisplayCode,
            this.colCommonName,
            this.colBioMass,
            this.colPercentCarbon,
            this.colPercentDryWeight,
            this.colWoodTypeTableName,
            this.colGrowthModelTableName,
            this.colBoardFootRuleTableName,
            this.colCubicFootRuleTableName,
            this.colAsubODisplayCode,
            this.colMaxTreeDBH,
            this.colWoodTypeDisplayCode,
            this.colBdFtDisplayCode,
            this.colCuFtDisplayCode,
            this.colBarkRatioDisplayCode,
            this.colLbs,
            this.colLbsType,
            this.colCommanName,
            this.colScientificName,
            this.colSpeciesConversion});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.AllowMultilineHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridView1_PrintInitialize);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeciesID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeciesID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpeciesID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            // 
            // colInputCode
            // 
            this.colInputCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colInputCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputCode.Caption = "Code";
            this.colInputCode.FieldName = "InputCode";
            this.colInputCode.Name = "colInputCode";
            this.colInputCode.Visible = true;
            this.colInputCode.VisibleIndex = 0;
            this.colInputCode.Width = 34;
            // 
            // colAbbreviation
            // 
            this.colAbbreviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colAbbreviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAbbreviation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAbbreviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAbbreviation.Caption = "Abv";
            this.colAbbreviation.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAbbreviation.FieldName = "Abbreviation";
            this.colAbbreviation.Name = "colAbbreviation";
            this.colAbbreviation.Visible = true;
            this.colAbbreviation.VisibleIndex = 1;
            this.colAbbreviation.Width = 36;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colDescription
            // 
            this.colDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescription.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescription.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 95;
            // 
            // colBarkRatio
            // 
            this.colBarkRatio.AppearanceHeader.Options.UseTextOptions = true;
            this.colBarkRatio.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBarkRatio.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBarkRatio.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBarkRatio.Caption = "Bark Ratio";
            this.colBarkRatio.FieldName = "BarkRatioValue";
            this.colBarkRatio.Name = "colBarkRatio";
            this.colBarkRatio.Visible = true;
            this.colBarkRatio.VisibleIndex = 3;
            this.colBarkRatio.Width = 47;
            // 
            // colMinDia
            // 
            this.colMinDia.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinDia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinDia.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinDia.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinDia.Caption = "Min Log Dia";
            this.colMinDia.FieldName = "MinDia";
            this.colMinDia.Name = "colMinDia";
            this.colMinDia.Visible = true;
            this.colMinDia.VisibleIndex = 9;
            this.colMinDia.Width = 36;
            // 
            // colMinLen
            // 
            this.colMinLen.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinLen.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinLen.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinLen.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinLen.Caption = "Min Log Len";
            this.colMinLen.FieldName = "MinLen";
            this.colMinLen.Name = "colMinLen";
            this.colMinLen.Visible = true;
            this.colMinLen.VisibleIndex = 10;
            this.colMinLen.Width = 40;
            // 
            // colMaxDia
            // 
            this.colMaxDia.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxDia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxDia.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxDia.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxDia.Caption = "Max Tree Dia";
            this.colMaxDia.FieldName = "MaxDia";
            this.colMaxDia.Name = "colMaxDia";
            this.colMaxDia.Visible = true;
            this.colMaxDia.VisibleIndex = 12;
            this.colMaxDia.Width = 40;
            // 
            // colMaxLen
            // 
            this.colMaxLen.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxLen.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxLen.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxLen.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxLen.Caption = "Max Log Len";
            this.colMaxLen.FieldName = "MaxLen";
            this.colMaxLen.Name = "colMaxLen";
            this.colMaxLen.Visible = true;
            this.colMaxLen.VisibleIndex = 11;
            this.colMaxLen.Width = 38;
            // 
            // colMaxHeight
            // 
            this.colMaxHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxHeight.Caption = "Max Tree Ht";
            this.colMaxHeight.FieldName = "MaxHeight";
            this.colMaxHeight.Name = "colMaxHeight";
            this.colMaxHeight.Visible = true;
            this.colMaxHeight.VisibleIndex = 14;
            this.colMaxHeight.Width = 39;
            // 
            // colTrim
            // 
            this.colTrim.AppearanceHeader.Options.UseTextOptions = true;
            this.colTrim.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTrim.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTrim.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTrim.Caption = "Log Trim";
            this.colTrim.FieldName = "Trim";
            this.colTrim.Name = "colTrim";
            this.colTrim.Visible = true;
            this.colTrim.VisibleIndex = 13;
            this.colTrim.Width = 35;
            // 
            // colWeight
            // 
            this.colWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeight.Caption = "Lbs";
            this.colWeight.FieldName = "Lbs";
            this.colWeight.Name = "colWeight";
            this.colWeight.Visible = true;
            this.colWeight.VisibleIndex = 17;
            this.colWeight.Width = 48;
            // 
            // colYieldTableName
            // 
            this.colYieldTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colYieldTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYieldTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colYieldTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colYieldTableName.ColumnEdit = this.repositoryItemComboBox8;
            this.colYieldTableName.FieldName = "YieldTableName";
            this.colYieldTableName.Name = "colYieldTableName";
            this.colYieldTableName.Visible = true;
            this.colYieldTableName.VisibleIndex = 8;
            this.colYieldTableName.Width = 98;
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // colFormFactorGroupID
            // 
            this.colFormFactorGroupID.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormFactorGroupID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormFactorGroupID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormFactorGroupID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormFactorGroupID.FieldName = "FormFactorGroupID";
            this.colFormFactorGroupID.Name = "colFormFactorGroupID";
            // 
            // colAsubOTableName
            // 
            this.colAsubOTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAsubOTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAsubOTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAsubOTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAsubOTableName.FieldName = "AsubOTableName";
            this.colAsubOTableName.Name = "colAsubOTableName";
            // 
            // colTableName
            // 
            this.colTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colWeightUnits
            // 
            this.colWeightUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeightUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightUnits.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colWeightUnits.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeightUnits.Caption = "Lbs Per";
            this.colWeightUnits.ColumnEdit = this.repositoryItemComboBox9;
            this.colWeightUnits.FieldName = "LbsType";
            this.colWeightUnits.Name = "colWeightUnits";
            this.colWeightUnits.Visible = true;
            this.colWeightUnits.VisibleIndex = 18;
            this.colWeightUnits.Width = 40;
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.AutoHeight = false;
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // colFormFactorsTableName
            // 
            this.colFormFactorsTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormFactorsTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormFactorsTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormFactorsTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormFactorsTableName.FieldName = "FormFactorsTableName";
            this.colFormFactorsTableName.Name = "colFormFactorsTableName";
            // 
            // colFormFactorsDisplayCode
            // 
            this.colFormFactorsDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormFactorsDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormFactorsDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormFactorsDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormFactorsDisplayCode.Caption = "Form Factor";
            this.colFormFactorsDisplayCode.ColumnEdit = this.repositoryItemComboBox6;
            this.colFormFactorsDisplayCode.FieldName = "FormFactorsDisplayCode";
            this.colFormFactorsDisplayCode.Name = "colFormFactorsDisplayCode";
            this.colFormFactorsDisplayCode.Visible = true;
            this.colFormFactorsDisplayCode.VisibleIndex = 5;
            this.colFormFactorsDisplayCode.Width = 60;
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // colComponentsTableName
            // 
            this.colComponentsTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colComponentsTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComponentsTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colComponentsTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComponentsTableName.FieldName = "ComponentsTableName";
            this.colComponentsTableName.Name = "colComponentsTableName";
            // 
            // colComponentsDisplayCode
            // 
            this.colComponentsDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colComponentsDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComponentsDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colComponentsDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComponentsDisplayCode.Caption = "Species Group";
            this.colComponentsDisplayCode.ColumnEdit = this.repositoryItemComboBox10;
            this.colComponentsDisplayCode.FieldName = "SpeciesGroup";
            this.colComponentsDisplayCode.Name = "colComponentsDisplayCode";
            this.colComponentsDisplayCode.Width = 55;
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            // 
            // colSpeciesGroup
            // 
            this.colSpeciesGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeciesGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeciesGroup.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpeciesGroup.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeciesGroup.FieldName = "SpeciesGroup";
            this.colSpeciesGroup.Name = "colSpeciesGroup";
            this.colSpeciesGroup.Visible = true;
            this.colSpeciesGroup.VisibleIndex = 6;
            this.colSpeciesGroup.Width = 55;
            // 
            // colBarkFactorsTableName
            // 
            this.colBarkFactorsTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colBarkFactorsTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBarkFactorsTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBarkFactorsTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBarkFactorsTableName.FieldName = "BarkFactorsTableName";
            this.colBarkFactorsTableName.Name = "colBarkFactorsTableName";
            // 
            // colBarkFactorsDisplayCode
            // 
            this.colBarkFactorsDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colBarkFactorsDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBarkFactorsDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBarkFactorsDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBarkFactorsDisplayCode.FieldName = "BarkFactorsDisplayCode";
            this.colBarkFactorsDisplayCode.Name = "colBarkFactorsDisplayCode";
            // 
            // colCommonName
            // 
            this.colCommonName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCommonName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCommonName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCommonName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCommonName.FieldName = "CommonName";
            this.colCommonName.Name = "colCommonName";
            // 
            // colBioMass
            // 
            this.colBioMass.AppearanceHeader.Options.UseTextOptions = true;
            this.colBioMass.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBioMass.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBioMass.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBioMass.FieldName = "BioMass";
            this.colBioMass.Name = "colBioMass";
            this.colBioMass.Visible = true;
            this.colBioMass.VisibleIndex = 19;
            this.colBioMass.Width = 47;
            // 
            // colPercentCarbon
            // 
            this.colPercentCarbon.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercentCarbon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercentCarbon.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercentCarbon.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercentCarbon.FieldName = "PercentCarbon";
            this.colPercentCarbon.Name = "colPercentCarbon";
            this.colPercentCarbon.Visible = true;
            this.colPercentCarbon.VisibleIndex = 20;
            // 
            // colPercentDryWeight
            // 
            this.colPercentDryWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercentDryWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercentDryWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercentDryWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercentDryWeight.FieldName = "PercentDryWeight";
            this.colPercentDryWeight.Name = "colPercentDryWeight";
            this.colPercentDryWeight.Visible = true;
            this.colPercentDryWeight.VisibleIndex = 21;
            // 
            // colWoodTypeTableName
            // 
            this.colWoodTypeTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWoodTypeTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWoodTypeTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colWoodTypeTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWoodTypeTableName.FieldName = "WoodTypeTableName";
            this.colWoodTypeTableName.Name = "colWoodTypeTableName";
            // 
            // colGrowthModelTableName
            // 
            this.colGrowthModelTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrowthModelTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrowthModelTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrowthModelTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrowthModelTableName.FieldName = "GrowthModelTableName";
            this.colGrowthModelTableName.Name = "colGrowthModelTableName";
            // 
            // colBoardFootRuleTableName
            // 
            this.colBoardFootRuleTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colBoardFootRuleTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBoardFootRuleTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBoardFootRuleTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBoardFootRuleTableName.FieldName = "BoardFootRuleTableName";
            this.colBoardFootRuleTableName.Name = "colBoardFootRuleTableName";
            // 
            // colCubicFootRuleTableName
            // 
            this.colCubicFootRuleTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCubicFootRuleTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCubicFootRuleTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCubicFootRuleTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCubicFootRuleTableName.FieldName = "CubicFootRuleTableName";
            this.colCubicFootRuleTableName.Name = "colCubicFootRuleTableName";
            // 
            // colAsubODisplayCode
            // 
            this.colAsubODisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colAsubODisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAsubODisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAsubODisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAsubODisplayCode.Caption = "Asubo Const";
            this.colAsubODisplayCode.ColumnEdit = this.repositoryItemComboBox5;
            this.colAsubODisplayCode.FieldName = "AsubODisplayCode";
            this.colAsubODisplayCode.Name = "colAsubODisplayCode";
            this.colAsubODisplayCode.Visible = true;
            this.colAsubODisplayCode.VisibleIndex = 4;
            this.colAsubODisplayCode.Width = 60;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // colMaxTreeDBH
            // 
            this.colMaxTreeDBH.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxTreeDBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxTreeDBH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxTreeDBH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxTreeDBH.Caption = "Max Tree Dbh";
            this.colMaxTreeDBH.FieldName = "MaxTreeDBH";
            this.colMaxTreeDBH.Name = "colMaxTreeDBH";
            this.colMaxTreeDBH.Width = 40;
            // 
            // colWoodTypeDisplayCode
            // 
            this.colWoodTypeDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colWoodTypeDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWoodTypeDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colWoodTypeDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWoodTypeDisplayCode.Caption = "Conifer HW";
            this.colWoodTypeDisplayCode.ColumnEdit = this.repositoryItemComboBox7;
            this.colWoodTypeDisplayCode.FieldName = "WoodTypeDisplayCode";
            this.colWoodTypeDisplayCode.Name = "colWoodTypeDisplayCode";
            this.colWoodTypeDisplayCode.Visible = true;
            this.colWoodTypeDisplayCode.VisibleIndex = 7;
            this.colWoodTypeDisplayCode.Width = 47;
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            // 
            // colBdFtDisplayCode
            // 
            this.colBdFtDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDisplayCode.Caption = "BdFt Rule";
            this.colBdFtDisplayCode.ColumnEdit = this.repositoryItemComboBox3;
            this.colBdFtDisplayCode.FieldName = "BdFtDisplayCode";
            this.colBdFtDisplayCode.Name = "colBdFtDisplayCode";
            this.colBdFtDisplayCode.Visible = true;
            this.colBdFtDisplayCode.VisibleIndex = 15;
            this.colBdFtDisplayCode.Width = 40;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // colCuFtDisplayCode
            // 
            this.colCuFtDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDisplayCode.Caption = "CuFt Rule";
            this.colCuFtDisplayCode.ColumnEdit = this.repositoryItemComboBox4;
            this.colCuFtDisplayCode.FieldName = "CuFtDisplayCode";
            this.colCuFtDisplayCode.Name = "colCuFtDisplayCode";
            this.colCuFtDisplayCode.Visible = true;
            this.colCuFtDisplayCode.VisibleIndex = 16;
            this.colCuFtDisplayCode.Width = 39;
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // colBarkRatioDisplayCode
            // 
            this.colBarkRatioDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colBarkRatioDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBarkRatioDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBarkRatioDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBarkRatioDisplayCode.FieldName = "BarkRatioDisplayCode";
            this.colBarkRatioDisplayCode.Name = "colBarkRatioDisplayCode";
            // 
            // colLbs
            // 
            this.colLbs.AppearanceHeader.Options.UseTextOptions = true;
            this.colLbs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLbs.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLbs.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLbs.FieldName = "Lbs";
            this.colLbs.Name = "colLbs";
            // 
            // colLbsType
            // 
            this.colLbsType.AppearanceHeader.Options.UseTextOptions = true;
            this.colLbsType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLbsType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLbsType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLbsType.FieldName = "LbsType";
            this.colLbsType.Name = "colLbsType";
            // 
            // colCommanName
            // 
            this.colCommanName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCommanName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCommanName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCommanName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCommanName.FieldName = "CommanName";
            this.colCommanName.Name = "colCommanName";
            // 
            // colScientificName
            // 
            this.colScientificName.AppearanceHeader.Options.UseTextOptions = true;
            this.colScientificName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScientificName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colScientificName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colScientificName.FieldName = "ScientificName";
            this.colScientificName.Name = "colScientificName";
            // 
            // colSpeciesConversion
            // 
            this.colSpeciesConversion.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeciesConversion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeciesConversion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpeciesConversion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeciesConversion.FieldName = "SpeciesConversion";
            this.colSpeciesConversion.Name = "colSpeciesConversion";
            // 
            // SpeciesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 636);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "SpeciesForm";
            this.Text = "Species Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpeciesForm_FormClosing);
            this.Load += new System.EventHandler(this.SpeciesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTableName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.BindingSource tblBindingSource;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colInputCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAbbreviation;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colBarkRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colMinDia;
        private DevExpress.XtraGrid.Columns.GridColumn colMinLen;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxDia;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxLen;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTrim;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colYieldTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colFormFactorGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colAsubOTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colFormFactorsTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colFormFactorsDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colComponentsTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colComponentsDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colBarkFactorsTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colBarkFactorsDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCommonName;
        private DevExpress.XtraGrid.Columns.GridColumn colBioMass;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentCarbon;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentDryWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodTypeTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colGrowthModelTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colBoardFootRuleTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colCubicFootRuleTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colAsubODisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxTreeDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodTypeDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBdFtDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCuFtDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBarkRatioDisplayCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLbs;
        private DevExpress.XtraGrid.Columns.GridColumn colLbsType;
        private DevExpress.XtraGrid.Columns.GridColumn colCommanName;
        private DevExpress.XtraGrid.Columns.GridColumn colScientificName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesConversion;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
    }
}