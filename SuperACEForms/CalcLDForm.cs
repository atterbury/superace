﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using SuperACEUtils;

namespace SuperACEForms
{
    public partial class CalcLDForm : DevExpress.XtraEditors.XtraForm
    {
        public bool mUpdate = false;
        public float Dbh = 0;

        public CalcLDForm()
        {
            InitializeComponent();
        }

        private void tbBaf_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbBaf.Text))
            {
                dxErrorProvider1.SetError(tbBaf, "Invalid BAF");
                tbBaf.Focus();
            }
            else
                dxErrorProvider1.SetError(tbBaf, "");
        }

        private void tbDbh_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbDbh.Text))
            {
                dxErrorProvider1.SetError(tbDbh, "Invalid Dbh");
                tbDbh.Focus();
            }
            else
                dxErrorProvider1.SetError(tbDbh, "");
        }

        private void tbActualDistance_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbActualDistance.Text))
            {
                dxErrorProvider1.SetError(tbActualDistance, "Invalid Actual Distance");
                tbActualDistance.Focus();
            }
            else
                dxErrorProvider1.SetError(tbActualDistance, "");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            // Calc
            tbLimitingDistance.Text = string.Format("{0:0.00}", Math.Sqrt((float)75.621 / Utils.ConvertToFloat(tbBaf.Text)) * Utils.ConvertToFloat(tbDbh.Text));
            if (Utils.ConvertToFloat(tbActualDistance.Text) > Utils.ConvertToFloat(tbLimitingDistance.Text))
                tbTreeIs.Text = "Out";
            else
                tbTreeIs.Text = "In";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            // Update
            mUpdate = true;
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            // Cancel
            mUpdate = false;
            this.Close();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }
    }
}