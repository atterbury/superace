﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class CostForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public CostForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            tableName = pTableName;
        }

        private void CostForm_Load(object sender, EventArgs e)
        {
            comboBoxEditTableName.Text = tableName;

            List<CostType> types = ctx.CostTypes.OrderBy(t => t.Code).ToList();
            foreach (var typeItem in types)
	        {
		        repositoryItemComboBox3.Items.Add(typeItem.Code);
	        }

            List<CostUnit> units = ctx.CostUnits.OrderBy(u => u.Code).ToList();
            foreach (var unitItem in units)
	        {
		        repositoryItemComboBox4.Items.Add(unitItem.Code);
	        }

            gridViewCost.ExpandAllGroups();

            LoadTableNames();

            LoadCostData();
            LoadHaulingData();
        }
         
        private float CalcHaulingCost()
        {
            List<Hauling> cols = ctx.Haulings.ToList();
            float total = 0;
            foreach (var item in cols)
            {
                double x = Convert.ToDouble(TimeSpan.Parse(item.Hours.ToString() + ":" + item.Minutes.ToString()).TotalHours);
                total += (float)(x * (double)item.CostPerHour);
            }
            return total;
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (xtraTabControl1.SelectedTabPage.Text.Contains("Cost"))
            {
                LoadCostData();
            }
            else
            {
                LoadHaulingData();
            }
        }

        private void LoadCostData()
        {
            UpdateHaulingCost();

            if (costsBindingSource.Count > 0)
                costsBindingSource.Clear();
            costsBindingSource.DataSource = ctx.Costs.OrderBy(t => t.CostType).ThenBy(t => t.ReportPrintOrder).Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", costsBindingSource.Count);
        }

        private void LoadHaulingData()
        {
            if (haulingBindingSource.Count > 0)
                haulingBindingSource.Clear();
            haulingBindingSource.DataSource = ctx.Haulings.OrderBy(h => h.Destination).Where(h => h.TableName == "GENERAL").ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", haulingBindingSource.Count);
        }

        private void UpdateHaulingCost()
        {
            float haulingCost = CalcHaulingCost();
            Cost rec = ctx.Costs.Where(c => c.TableName == tableName && c.CostDescription == "HAULING").FirstOrDefault();
            rec.CostAmount = haulingCost;
            ctx.SaveChanges();
        }

        private void LoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetCostTableNames(ref ctx);
            comboBoxEditTableName.Properties.Items.Clear();
            foreach (var tblItem in tbls)
                comboBoxEditTableName.Properties.Items.Add(tblItem);
            comboBoxEditTableName.Text = tableName;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            costsBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
            bool bProcess = false;
            string tableName = string.Empty;

            CopyTableForm frm = new CopyTableForm(ref ctx, "Cost");
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bProcess = frm.bProcess;
            tableName = frm.tableName;
            frm.Dispose();

            List<Cost> cColl = ctx.Costs.Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            foreach (var costItem in cColl)
            {
                Cost newCostRec = new Cost();
                newCostRec.CostAmount = costItem.CostAmount;
                newCostRec.CostDate = costItem.CostDate;
                newCostRec.CostDescription = costItem.CostDescription;
                newCostRec.CostType = costItem.CostType;
                newCostRec.CostTypeGroup = costItem.CostTypeGroup;
                newCostRec.Percent = costItem.Percent;
                newCostRec.ReportPrintOrder = costItem.ReportPrintOrder;
                newCostRec.Species = costItem.Species;
                newCostRec.TableName = tableName;
                newCostRec.TreatmentAcresPerYear = costItem.TreatmentAcresPerYear;
                newCostRec.Units = costItem.Units;
                newCostRec.YearsApplied = costItem.YearsApplied;
                ctx.Costs.Add(newCostRec);
            }

            List<Hauling> hColl = ctx.Haulings.Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            foreach (var haulingtItem in hColl)
            {
                Hauling newHaulingRec = new Hauling();
                newHaulingRec.AvgLoadCcf = haulingtItem.AvgLoadCcf;
                newHaulingRec.AvgLoadMbf = haulingtItem.AvgLoadMbf;
                newHaulingRec.AvgLoadSize = haulingtItem.AvgLoadSize;
                newHaulingRec.CalcCostDollarPerMbf = haulingtItem.CalcCostDollarPerMbf;
                newHaulingRec.CostPerHour = haulingtItem.CostPerHour;
                newHaulingRec.Destination = haulingtItem.Destination;
                newHaulingRec.Hours = haulingtItem.Hours;
                newHaulingRec.LoadUnloadHours = haulingtItem.LoadUnloadHours;
                newHaulingRec.Minutes = haulingtItem.Minutes;
                newHaulingRec.RoundTripMiles = haulingtItem.RoundTripMiles;
                newHaulingRec.TableName = tableName;
                ctx.Haulings.Add(newHaulingRec);
            }

            ctx.SaveChanges();

            LoadTableNames();

            LoadCostData();
            LoadHaulingData();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditTableName.Text == currentProject.CostTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditTableName.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;

            ctx.Costs.RemoveRange(ctx.Costs.Where(c => c.TableName == comboBoxEditTableName.Text));
            ctx.Haulings.RemoveRange(ctx.Haulings.Where(c => c.TableName == comboBoxEditTableName.Text));
            ctx.SaveChanges();

            LoadTableNames();

            LoadCostData();
            LoadHaulingData();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault();
            currentProject.CostTableName = comboBoxEditTableName.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void CostForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridControlCost.ShowPrintPreview();
            gridControlHauling.ShowPrintPreview();
        }

        private void gridViewCost_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            GridColumn CostTypeCol = view.Columns["CostType"];
            GridColumn CostTypeGroupCol = view.Columns["CostTypeGroup"];
            view.SetFocusedRowCellValue(CostTypeGroupCol, view.GetRowCellValue(e.RowHandle, CostTypeCol));
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Add
            if (xtraTabControl1.SelectedTabPage.Text.Contains("Cost"))
                AddNewCostRecord();
            else
                AddNewHaulingRecord();
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete
            if (xtraTabControl1.SelectedTabPage.Text.Contains("Cost"))
                DeleteCostRecord();
            else
                DeleteHaulingRecord();
        }

        private void AddNewCostRecord()
        {
            Cost rec = new Cost();
            rec.TableName = tableName;
            //rec.Trim = 1.0;
            ctx.Costs.Add(rec);
            ctx.SaveChanges();

            LoadCostData();

            gridViewCost.FocusedColumn = gridViewCost.Columns["Species"];
            gridControlCost.Focus();
        }

        private void AddNewHaulingRecord()
        {
            Hauling rec = new Hauling();
            rec.TableName = tableName;
            ctx.Haulings.Add(rec);
            ctx.SaveChanges();

            LoadHaulingData();

            gridViewHauling.FocusedColumn = gridViewHauling.Columns["Destination"];
            gridControlHauling.Focus();
        }

        private void DeleteCostRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Delete Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            Cost rec = costsBindingSource.Current as Cost;
            ctx.Costs.Remove(rec);
            ctx.SaveChanges();
            LoadCostData();
        }

        private void DeleteHaulingRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Delete Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            Hauling rec = haulingBindingSource.Current as Hauling;
            ctx.Haulings.Remove(rec);
            ctx.SaveChanges();
            LoadHaulingData();
        }

        private void gridViewCost_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            switch (gridViewCost.FocusedColumn.FieldName)
            {
                case "CostType":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        gridViewCost.SetFocusedRowCellValue("CostTypeGroup", e.Value.ToString());
                    }
                    break;
            }
        }

        private void gridViewCost_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            //if (xtraTabControl1.SelectedTabPage.Text.Contains("Cost"))
            //{
            //    AddNewCostRecord();
            //    //gridViewCost.SetRowCellValue(e.RowHandle, "TableName", tableName);
            //}
            //else
            //{
            //    AddNewHaulingRecord();
            //    //gridViewHauling.SetRowCellValue(e.RowHandle, "TableName", tableName);
            //}
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page.Text.Contains("Cost"))
                barStaticItem6.Caption = string.Format("{0} Records Loaded", costsBindingSource.Count);
            else
                barStaticItem6.Caption = string.Format("{0} Records Loaded", haulingBindingSource.Count);
        }

        private void gridViewHauling_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            haulingBindingSource.EndEdit();
            float truckCostPerHour = (float)Convert.ToSingle(gridViewHauling.GetRowCellValue(e.RowHandle, "CostPerHour").ToString());
            float minutes = (float)Convert.ToSingle(gridViewHauling.GetRowCellValue(e.RowHandle, "Minutes").ToString());
            float hours = (float)Convert.ToSingle(gridViewHauling.GetRowCellValue(e.RowHandle, "Hours").ToString());
            float loadUnloadHours = (float)Convert.ToSingle(gridViewHauling.GetRowCellValue(e.RowHandle, "LoadUnloadHours").ToString());
            float avgLoadMbf = (float)Convert.ToSingle(gridViewHauling.GetRowCellValue(e.RowHandle, "AvgLoadMbf").ToString());

            float calcCostMbf = truckCostPerHour * (minutes / 60 + hours + loadUnloadHours) / avgLoadMbf;
            gridViewHauling.SetRowCellValue(e.RowHandle, "CalcCostDollarPerMbf", calcCostMbf);
            ctx.SaveChanges();

            UpdateHaulingCost();
        }
    } 
}