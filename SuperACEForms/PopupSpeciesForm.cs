﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace SuperACE
{
    public partial class PopupSpeciesForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext ctx = null;
        private string projectTable = null;
        public string code = string.Empty;

        public PopupSpeciesForm(ref ProjectDbContext pCtx, string pProjectTable)
        {
            InitializeComponent();

            ctx = pCtx;
            projectTable = pProjectTable;
        }

        private void PopupSpeciesForm_Load(object sender, EventArgs e)
        {
            List<Species> coll =
                ctx.Species.OrderBy(s => s.Abbreviation).Where(s => s.TableName == projectTable).ToList();
            speciesBindingSource.DataSource = coll;
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    Species rec = speciesBindingSource.Current as Species;
                    code = rec.Abbreviation;
                    this.Close();
                    break;
                case Keys.Escape:
                    code = string.Empty;
                    this.Close();
                    break;
            }
        }
    }
}