﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;
using DevExpress.XtraPrinting;

namespace SuperACEForms
{
    public partial class SpeciesForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        private string projectDataSource = null;
        private string locationDataSource = null;
        private ProjectDbContext projectContext = null;
        private ProjectsDbContext locContext = null;

        public bool wasProjectChanged = false;

        public SpeciesForm(ref ProjectDbContext pContext, ref ProjectsDbContext pLocContext, string pTableName)
        {
            InitializeComponent();

            projectContext = pContext;
            locContext = pLocContext;
            tableName = pTableName;
        }

        private void SpeciesForm_Load(object sender, EventArgs e)
        {
            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            List<Boardfootrule> bdftColl = projectContext.Boardfootrules.OrderBy(t => t.InputCode).ToList();
            foreach (var bdftItem in bdftColl)
            {
                this.repositoryItemComboBox3.Items.Add(bdftItem.InputCode);
            }

            List<Cubicfootrule> cuftColl = projectContext.Cubicfootrules.OrderBy(t => t.InputCode).ToList();
            foreach (var cuftItem in cuftColl)
            {
                this.repositoryItemComboBox4.Items.Add(cuftItem.InputCode);
            }

            List<Woodtype> woodtypeColl = projectContext.Woodtypes.OrderBy(t => t.DisplayCode).ToList();
            foreach (var woodtypeItem in woodtypeColl)
            {
                this.repositoryItemComboBox7.Items.Add(woodtypeItem.DisplayCode);
            }

            List<VolumeUnit> lbsColl = ProjectsBLL.GetVolumeUnits(ref locContext);
            foreach (var lbsItem in lbsColl)
            {
                this.repositoryItemComboBox9.Items.Add(lbsItem.Code);
            }

            List<Asubo> aoColl = projectContext.Asuboes.OrderBy(t => t.DisplayCode).ToList();
            foreach (var aoItem in aoColl)
            {
                this.repositoryItemComboBox5.Items.Add(aoItem.DisplayCode);
            }

            List<Formfactor> ffColl = projectContext.Formfactors.OrderBy(t => t.DisplayCode).ToList();
            foreach (var ffItem in ffColl)
            {
                this.repositoryItemComboBox6.Items.Add(ffItem.DisplayCode);
            }

            List<Project.DAL.Component> sgColl = projectContext.Components.OrderBy(t => t.DisplayCode).ToList();
            foreach (var sgItem in sgColl)
            {
                this.repositoryItemComboBox10.Items.Add(sgItem.DisplayCode);
            }

            List<string> yieldColl = ProjectBLL.GetYieldTableNames(ref projectContext).ToList();
            foreach (var yieldItem in yieldColl)
            {
                this.repositoryItemComboBox8.Items.Add(yieldItem);
            }

            LoadDataSource();
        }

        private void LoadDataSource()
        {
            if (tblBindingSource.Count > 0)
                tblBindingSource.Clear();
            tblBindingSource.DataSource = projectContext.Species.OrderBy(t => t.InputCode).Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", tblBindingSource.Count);
        }

        private void LoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            comboBoxEditTableName.Properties.Items.Clear();
            foreach (var tblItem in tbls)
                comboBoxEditTableName.Properties.Items.Add(tblItem);
            comboBoxEditTableName.Text = tableName;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            tblBindingSource.EndEdit();
            projectContext.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
            bool bProcess = false;
            string tableName = string.Empty;

            CopyTableForm frm = new CopyTableForm(ref projectContext, "Species");
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bProcess = frm.bProcess;
            tableName = frm.tableName;
            frm.Dispose();

            List<Species> coll = projectContext.Species.Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            foreach (var item in coll)
            {
                Species newRec = new Species();
                newRec.Abbreviation = item.Abbreviation;
                newRec.AsubODisplayCode = item.AsubODisplayCode;
                newRec.AsubOTableName = item.AsubOTableName;
                newRec.ASuboValue = item.ASuboValue;
                newRec.BarkFactorsDisplayCode = item.BarkFactorsDisplayCode;
                newRec.BarkFactorsTableName = item.BarkFactorsTableName;
                newRec.BarkRatioDisplayCode = item.BarkRatioDisplayCode;
                newRec.BarkRatioValue = item.BarkRatioValue;
                newRec.BdFtDisplayCode = item.BdFtDisplayCode;
                newRec.BioMass = item.BioMass;
                newRec.BoardFootRuleTableName = item.BoardFootRuleTableName;
                newRec.CommonName = item.CommonName;
                newRec.ComponentsDisplayCode = item.ComponentsDisplayCode;
                newRec.ComponentsTableName = item.ComponentsTableName;
                newRec.CubicFootRuleTableName = item.CubicFootRuleTableName;
                newRec.CuFtDisplayCode = item.CuFtDisplayCode;
                newRec.Description = item.Description;
                newRec.FormFactorsDisplayCode = item.FormFactorsDisplayCode;
                newRec.FormFactorsTableName = item.FormFactorsTableName;
                newRec.FormFactorValue = item.FormFactorValue;
                newRec.GrowthModelTableName = item.GrowthModelTableName;
                newRec.InputCode = item.InputCode;
                newRec.Lbs = item.Lbs;
                newRec.LbsType = item.LbsType;
                newRec.MaxDia = item.MaxDia;
                newRec.MaxHeight = item.MaxHeight;
                newRec.MaxLen = item.MaxLen;
                newRec.MaxTreeDbh = item.MaxTreeDbh;
                newRec.MinDia = item.MinDia;
                newRec.MinLen = item.MinLen;
                newRec.PercentCarbon = item.PercentCarbon;
                newRec.PercentDryWeight = item.PercentDryWeight;
                newRec.ScientificName = item.ScientificName;
                newRec.SpeciesGroup = item.SpeciesGroup;
                newRec.TableName = tableName;
                newRec.Trim = item.Trim;
                newRec.Weight = item.Weight;
                newRec.WeightUnits = item.WeightUnits;
                newRec.WoodTypeDisplayCode = item.WoodTypeDisplayCode;
                newRec.WoodTypeTableName = item.WoodTypeTableName;
                newRec.YieldTableName = item.YieldTableName;
                projectContext.Species.Add(newRec);
            }

            projectContext.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref projectContext);

            if (comboBoxEditTableName.Text == currentProject.SpeciesTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditTableName.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;

            projectContext.Species.RemoveRange(
                projectContext.Species.Where(s => s.TableName == comboBoxEditTableName.Text));
            projectContext.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = projectContext.Projects.FirstOrDefault(); ;
            currentProject.SpeciesTableName = comboBoxEditTableName.Text;
            projectContext.SaveChanges();
            wasProjectChanged = true;
        }

        private void SpeciesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            projectContext.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Add record
            AddNewRecord();
        }

        private void AddNewRecord()
        {
            Species rec = new Species();
            rec.TableName = tableName;
            rec.Trim = 1.0;
            projectContext.Species.Add(rec);
            projectContext.SaveChanges();
            LoadDataSource();
            gridView1.FocusedColumn = gridView1.Columns["InputCode"];
            gridControl1.Focus();
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete
            DeleteRecord();
        }

        private void DeleteRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            Species rec = tblBindingSource.Current as Species;
            projectContext.Species.Remove(rec);
            projectContext.SaveChanges();
            LoadDataSource();
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            switch (gridView1.FocusedColumn.FieldName)
            {
                case "AsubODisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 5)
                        {
                            e.Valid = false;
                            e.ErrorText = "Maximum character size is 5.";
                        }
                        else
                        {
                            e.Valid = true;
                            e.ErrorText = string.Empty;
                        }
                    }
                    break;
                case "FormFactorsDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 4)
                        {
                            e.Valid = false;
                            e.ErrorText = "Maximum character size is 4.";
                        }
                        else
                        {
                            e.Valid = true;
                            e.ErrorText = string.Empty;
                        }
                    }
                    break;
            }
        }

        private void gridView1_PrintInitialize(object sender, DevExpress.XtraGrid.Views.Base.PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }
    }
}