﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SuperACEUtils;

namespace SuperACEForms
{
    public partial class CalcRDForm : DevExpress.XtraEditors.XtraForm
    {
        public bool mUpdate = false;
        public string mDbh = string.Empty;
        public string mFP = string.Empty;
        public string mFF = string.Empty;
        public string mTDF = string.Empty;
        public string mBole = string.Empty;
        public string mTotalHeight = string.Empty;
        public string mCrownRatio = string.Empty;

        public CalcRDForm()
        {
            InitializeComponent();
        }

        private void CalcRDForm_Load(object sender, EventArgs e)
        {
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            // Calc
            Calc();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            // Update
            mUpdate = true;
            Calc();
            mDbh = tbResultsDbh.Text;
            mFP = tbResultsFP.Text;
            mFF = Utils.ConvertToInt(tbResultsFF.Text).ToString();
            mTDF = Utils.ConvertToInt(tbResultsTDF.Text).ToString();
            mBole = Utils.ConvertToInt(tbResultsBoleHt.Text).ToString();
            mTotalHeight = Utils.ConvertToInt(tbResultsTotalHt.Text).ToString();
            mCrownRatio = tbResultsCR.Text;
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            // Cancel
            mUpdate = false;
            mDbh = string.Empty;
            mFP = string.Empty;
            mFF = string.Empty;
            mTDF = string.Empty;
            mBole = string.Empty;
            mTotalHeight = string.Empty;
            mCrownRatio = string.Empty;

            this.Close();
        }

        private void Calc()
        {
            if (Utils.ConvertToFloat(tbStumpHeight.Text) > 0 && !string.IsNullOrEmpty(tbStumpAngle.Text))
                tbReadAngleDbhBands.Text = string.Format("{0:0}", (4.5 - Utils.ConvertToFloat(tbStumpHeight.Text))+Utils.ConvertToFloat(tbStumpAngle.Text));
            else
                tbReadAngleDbhBands.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputFormPoint.Text) > 0 && !string.IsNullOrEmpty(tbStumpAngle.Text))
                tbReadAngleFormPoint.Text = string.Format("{0:0}", Utils.ConvertToFloat(tbInputFormPoint.Text) + Utils.ConvertToFloat(tbStumpAngle.Text));
            else
                tbReadAngleFormPoint.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputDbhBands.Text) > 0 && Utils.ConvertToFloat(tbHorizDist.Text) > 0)
                tbResultsDbh.Text = string.Format("{0:0.0}", 
                    Utils.ConvertToFloat(tbInputDbhBands.Text) * Utils.ConvertToFloat(tbHorizDist.Text)*(1/16.5));
            else
                tbResultsDbh.Text = string.Format("{0:0.0}", 0);

            if (Utils.ConvertToFloat(tbInputFormPoint.Text) > 0)
                tbResultsFP.Text = string.Format("{0:0}", Utils.ConvertToFloat(tbInputFormPoint.Text));
            else
                tbResultsFP.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputFPBands.Text) > 0 && Utils.ConvertToFloat(tbInputDbhBands.Text) > 0)
                tbResultsFF.Text = string.Format("{0:0}",
                    Utils.ConvertToFloat(tbInputFPBands.Text) / Utils.ConvertToFloat(tbInputDbhBands.Text) * 100);
            else
                tbResultsFF.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputTDBands.Text) > 0 && Utils.ConvertToFloat(tbInputFPBands.Text) > 0)
                tbResultsTDF.Text = string.Format("{0:0}", 
                    Utils.ConvertToFloat(tbInputTDBands.Text) / Utils.ConvertToFloat(tbInputFPBands.Text) * 100);
            else
                tbResultsTDF.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputTDAngleInput.Text) > 0 && !string.IsNullOrEmpty(tbStumpAngle.Text) && Utils.ConvertToFloat(tbHorizDist.Text) > 0)
                tbResultsBoleHt.Text = string.Format("{0:0}",
                    (Utils.ConvertToFloat(tbInputTDAngleInput.Text) - Utils.ConvertToFloat(tbStumpAngle.Text)) * (Utils.ConvertToFloat(tbHorizDist.Text)/100));
            else
                tbResultsBoleHt.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputTotalHtAngle.Text) > 0 && !string.IsNullOrEmpty(tbStumpAngle.Text) && 
                Utils.ConvertToFloat(tbHorizDist.Text) > 0 && Utils.ConvertToFloat(tbStumpHeight.Text) > 0)
                tbResultsTotalHt.Text = string.Format("{0:0}",
                    (Utils.ConvertToFloat(tbInputTotalHtAngle.Text) - Utils.ConvertToFloat(tbStumpAngle.Text)) * 
                    (Utils.ConvertToFloat(tbHorizDist.Text) / 100)+ Utils.ConvertToFloat(tbStumpHeight.Text));
            else
                tbResultsTotalHt.Text = string.Format("{0:0}", 0);

            if (Utils.ConvertToFloat(tbInputCrownBaseAngle.Text) > 0 && Utils.ConvertToFloat(tbInputTotalHtAngle.Text) > 0)
                tbResultsCR.Text = string.Format("{0:0}",
                    Utils.ConvertToFloat(tbInputCrownBaseAngle.Text) / Utils.ConvertToFloat(tbInputTotalHtAngle.Text) * 100);
            else
                tbResultsCR.Text = string.Format("{0:0}", 0);
        }

        private void tbHorizDist_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbHorizDist.Text))
            {
                dxErrorProvider1.SetError(tbHorizDist, "Invalid Horizontal Distance");
                tbHorizDist.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbHorizDist.Text))
                {
                    dxErrorProvider1.SetError(tbHorizDist, "Invalid Horizontal Distance");
                    tbHorizDist.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbHorizDist, "");
                    Calc();
                }
            }
        }

        private void tbPercentTopo_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbPercentTopo.Text))
            {
                dxErrorProvider1.SetError(tbPercentTopo, "Invalid Scale");
                tbPercentTopo.Focus();
            }
            else
            {
                switch (tbPercentTopo.Text)
                {
                    case "P":
                        dxErrorProvider1.SetError(tbPercentTopo, "");
                        labelControlStatus.Text = "(% Slope)";
                        Calc();
                        break;
                    case "T":
                        dxErrorProvider1.SetError(tbPercentTopo, "");
                        labelControlStatus.Text = "(Degrees)";
                        Calc();
                        break;
                    default:
                        dxErrorProvider1.SetError(tbPercentTopo, "Invalid Scale");
                        tbPercentTopo.Focus();
                        break;
                }
            }
        }

        private void tbStumpHeight_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbStumpHeight.Text))
            {
                dxErrorProvider1.SetError(tbStumpHeight, "Invalid Stump Height");
                tbStumpHeight.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbStumpHeight.Text))
                {
                    dxErrorProvider1.SetError(tbStumpHeight, "Invalid Stump Height");
                    tbStumpHeight.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbStumpHeight, "");
                    Calc();
                }
            }
        }

        private void tbStumpAngle_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbStumpAngle.Text))
            {
                dxErrorProvider1.SetError(tbStumpAngle, "Invalid Stump Angle");
                tbStumpAngle.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbStumpAngle.Text))
                {
                    dxErrorProvider1.SetError(tbStumpAngle, "Invalid Stump Angle");
                    tbStumpAngle.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbStumpAngle, "");
                    Calc();
                    tbInputDbhBands.Focus();
                }
            }
        }

        private void tbInputDbhBands_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputDbhBands.Text))
            {
                dxErrorProvider1.SetError(tbInputDbhBands, "Invalid Input Dbh Bands");
                tbInputDbhBands.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputDbhBands.Text))
                {
                    dxErrorProvider1.SetError(tbInputDbhBands, "Invalid Input Dbh Bands");
                    tbInputDbhBands.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputDbhBands, "");
                    Calc();
                }
            }
        }

        private void tbInputFormPoint_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputFormPoint.Text))
            {
                dxErrorProvider1.SetError(tbInputFormPoint, "Invalid Input Form Point");
                tbInputFormPoint.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputFormPoint.Text))
                {
                    dxErrorProvider1.SetError(tbInputFormPoint, "Invalid Input Form Point");
                    tbInputFormPoint.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputFormPoint, "");
                }
            }
        }

        private void tbInputFPBands_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputFPBands.Text))
            {
                dxErrorProvider1.SetError(tbInputFPBands, "Invalid Input FP Bands");
                tbInputFPBands.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputFPBands.Text))
                {
                    dxErrorProvider1.SetError(tbInputFPBands, "Invalid Input FP Bands");
                    tbInputFPBands.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputFPBands, "");
                    Calc();
                }
            }
        }

        private void tbInputTDBands_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputTDBands.Text))
            {
                dxErrorProvider1.SetError(tbInputTDBands, "Invalid TD Bands");
                tbInputTDBands.Focus();
            }
            else
            {

                if (!Utils.IsValidFloat(tbInputTDBands.Text))
                {
                    dxErrorProvider1.SetError(tbInputTDBands, "Invalid TD Bands");
                    tbInputTDBands.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputTDBands, "");
                    Calc();
                }
            }
        }

        private void tbInputTDAngleInput_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputTDAngleInput.Text))
            {
                dxErrorProvider1.SetError(tbInputTDAngleInput, "Invalid TD Angle Input");
                tbInputTDAngleInput.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputTDAngleInput.Text))
                {
                    dxErrorProvider1.SetError(tbInputTDAngleInput, "Invalid TD Angle Input");
                    tbInputTDAngleInput.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputTDAngleInput, "");
                    Calc();
                }
            }
        }

        private void tbInputTotalHtAngle_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputTotalHtAngle.Text))
            {
                dxErrorProvider1.SetError(tbInputTotalHtAngle, "Invalid Total Ht Angle");
                tbInputTotalHtAngle.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputTotalHtAngle.Text))
                {
                    dxErrorProvider1.SetError(tbInputTotalHtAngle, "Invalid Total Ht Angle");
                    tbInputTotalHtAngle.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputTotalHtAngle, "");
                    Calc();
                }
            }
        }

        private void tbCrownBaseAngle_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbInputCrownBaseAngle.Text))
            {
                dxErrorProvider1.SetError(tbInputCrownBaseAngle, "Invalid Crown Base Angle");
                tbInputCrownBaseAngle.Focus();
            }
            else
            {
                if (!Utils.IsValidFloat(tbInputCrownBaseAngle.Text))
                {
                    dxErrorProvider1.SetError(tbInputCrownBaseAngle, "Invalid Crown Base Angle");
                    tbInputCrownBaseAngle.Focus();
                }
                else
                {
                    dxErrorProvider1.SetError(tbInputCrownBaseAngle, "");
                    Calc();
                }
            }
        }
    }
}