﻿namespace SuperACEForms
{
    partial class CountAvgForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.adjCruisePlotBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossCuFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetCuFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossBdFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetBdFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasalArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQmDbh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTractName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsSelectedId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossCuFtLogs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetCuFtLogs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossBdFtLogs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetBdFtLogs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBoleHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVBarNetCuFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVBarNetBdFtAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogsPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adjCruisePlotBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.adjCruisePlotBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1071, 555);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // adjCruisePlotBindingSource
            // 
            this.adjCruisePlotBindingSource.DataSource = typeof(Project.DAL.AdjCruisePlot);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colGrossCuFtAcre,
            this.colNetCuFtAcre,
            this.colGrossBdFtAcre,
            this.colNetBdFtAcre,
            this.colBasalArea,
            this.colQmDbh,
            this.gridColumn4,
            this.colTractName,
            this.colStandName,
            this.colStandsSelectedId,
            this.colGrossCuFtLogs,
            this.colNetCuFtLogs,
            this.colGrossBdFtLogs,
            this.colNetBdFtLogs,
            this.gridColumn5,
            this.colBoleHeight,
            this.colTotalHeight,
            this.colVBarNetCuFtAcre,
            this.colVBarNetBdFtAcre,
            this.colTreesPerAcre,
            this.colTreeCount,
            this.colLogsPerAcre,
            this.colTotalCcf,
            this.colTotalMbf});
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "Id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "Species";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 49;
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "Status";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 49;
            // 
            // colGrossCuFtAcre
            // 
            this.colGrossCuFtAcre.Caption = "Gross CuFt/Acre";
            this.colGrossCuFtAcre.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossCuFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossCuFtAcre.FieldName = "GrossCuFtAcre";
            this.colGrossCuFtAcre.Name = "colGrossCuFtAcre";
            this.colGrossCuFtAcre.Visible = true;
            this.colGrossCuFtAcre.VisibleIndex = 6;
            this.colGrossCuFtAcre.Width = 59;
            // 
            // colNetCuFtAcre
            // 
            this.colNetCuFtAcre.Caption = "Net CuFt/Acre";
            this.colNetCuFtAcre.DisplayFormat.FormatString = "{0:n0}";
            this.colNetCuFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetCuFtAcre.FieldName = "NetCuFtAcre";
            this.colNetCuFtAcre.Name = "colNetCuFtAcre";
            this.colNetCuFtAcre.Visible = true;
            this.colNetCuFtAcre.VisibleIndex = 7;
            this.colNetCuFtAcre.Width = 60;
            // 
            // colGrossBdFtAcre
            // 
            this.colGrossBdFtAcre.Caption = "Gross BdFt/Acre";
            this.colGrossBdFtAcre.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossBdFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossBdFtAcre.FieldName = "GrossBdFtAcre";
            this.colGrossBdFtAcre.Name = "colGrossBdFtAcre";
            this.colGrossBdFtAcre.Visible = true;
            this.colGrossBdFtAcre.VisibleIndex = 8;
            this.colGrossBdFtAcre.Width = 66;
            // 
            // colNetBdFtAcre
            // 
            this.colNetBdFtAcre.Caption = "Net BdFt/Acre";
            this.colNetBdFtAcre.DisplayFormat.FormatString = "{0:n0}";
            this.colNetBdFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetBdFtAcre.FieldName = "NetBdFtAcre";
            this.colNetBdFtAcre.Name = "colNetBdFtAcre";
            this.colNetBdFtAcre.Visible = true;
            this.colNetBdFtAcre.VisibleIndex = 9;
            this.colNetBdFtAcre.Width = 63;
            // 
            // colBasalArea
            // 
            this.colBasalArea.DisplayFormat.FormatString = "{0:n3}";
            this.colBasalArea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBasalArea.FieldName = "BasalArea";
            this.colBasalArea.Name = "colBasalArea";
            this.colBasalArea.Visible = true;
            this.colBasalArea.VisibleIndex = 10;
            this.colBasalArea.Width = 48;
            // 
            // colQmDbh
            // 
            this.colQmDbh.DisplayFormat.FormatString = "{0:n1}";
            this.colQmDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQmDbh.FieldName = "QmDbh";
            this.colQmDbh.Name = "colQmDbh";
            this.colQmDbh.Visible = true;
            this.colQmDbh.VisibleIndex = 11;
            this.colQmDbh.Width = 36;
            // 
            // gridColumn4
            // 
            this.gridColumn4.FieldName = "StandsId";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // colTractName
            // 
            this.colTractName.FieldName = "TractName";
            this.colTractName.Name = "colTractName";
            // 
            // colStandName
            // 
            this.colStandName.FieldName = "StandName";
            this.colStandName.Name = "colStandName";
            // 
            // colStandsSelectedId
            // 
            this.colStandsSelectedId.FieldName = "StandsSelectedId";
            this.colStandsSelectedId.Name = "colStandsSelectedId";
            // 
            // colGrossCuFtLogs
            // 
            this.colGrossCuFtLogs.Caption = "Gross CuFt";
            this.colGrossCuFtLogs.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossCuFtLogs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossCuFtLogs.FieldName = "GrossCuFtLogs";
            this.colGrossCuFtLogs.Name = "colGrossCuFtLogs";
            this.colGrossCuFtLogs.Visible = true;
            this.colGrossCuFtLogs.VisibleIndex = 2;
            this.colGrossCuFtLogs.Width = 44;
            // 
            // colNetCuFtLogs
            // 
            this.colNetCuFtLogs.Caption = "Net CuFt";
            this.colNetCuFtLogs.DisplayFormat.FormatString = "{0:n0}";
            this.colNetCuFtLogs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetCuFtLogs.FieldName = "NetCuFtLogs";
            this.colNetCuFtLogs.Name = "colNetCuFtLogs";
            this.colNetCuFtLogs.Visible = true;
            this.colNetCuFtLogs.VisibleIndex = 3;
            this.colNetCuFtLogs.Width = 38;
            // 
            // colGrossBdFtLogs
            // 
            this.colGrossBdFtLogs.Caption = "Gross BdFt";
            this.colGrossBdFtLogs.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossBdFtLogs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossBdFtLogs.FieldName = "GrossBdFtLogs";
            this.colGrossBdFtLogs.Name = "colGrossBdFtLogs";
            this.colGrossBdFtLogs.Visible = true;
            this.colGrossBdFtLogs.VisibleIndex = 4;
            this.colGrossBdFtLogs.Width = 46;
            // 
            // colNetBdFtLogs
            // 
            this.colNetBdFtLogs.Caption = "Net BdFt";
            this.colNetBdFtLogs.DisplayFormat.FormatString = "{0:n0}";
            this.colNetBdFtLogs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetBdFtLogs.FieldName = "NetBdFtLogs";
            this.colNetBdFtLogs.Name = "colNetBdFtLogs";
            this.colNetBdFtLogs.Visible = true;
            this.colNetBdFtLogs.VisibleIndex = 5;
            this.colNetBdFtLogs.Width = 38;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "FF";
            this.gridColumn5.DisplayFormat.FormatString = "{0:n0}";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "Ff";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Width = 32;
            // 
            // colBoleHeight
            // 
            this.colBoleHeight.DisplayFormat.FormatString = "{0:n0}";
            this.colBoleHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBoleHeight.FieldName = "BoleHeight";
            this.colBoleHeight.Name = "colBoleHeight";
            this.colBoleHeight.Width = 43;
            // 
            // colTotalHeight
            // 
            this.colTotalHeight.DisplayFormat.FormatString = "{0:n0}";
            this.colTotalHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalHeight.FieldName = "TotalHeight";
            this.colTotalHeight.Name = "colTotalHeight";
            this.colTotalHeight.Width = 46;
            // 
            // colVBarNetCuFtAcre
            // 
            this.colVBarNetCuFtAcre.Caption = "VBar Net CuFt/Acre";
            this.colVBarNetCuFtAcre.DisplayFormat.FormatString = "{0:n2}";
            this.colVBarNetCuFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colVBarNetCuFtAcre.FieldName = "VBarNetCuFtAcre";
            this.colVBarNetCuFtAcre.Name = "colVBarNetCuFtAcre";
            this.colVBarNetCuFtAcre.Visible = true;
            this.colVBarNetCuFtAcre.VisibleIndex = 12;
            this.colVBarNetCuFtAcre.Width = 63;
            // 
            // colVBarNetBdFtAcre
            // 
            this.colVBarNetBdFtAcre.Caption = "VBar Net BdFt/Acre";
            this.colVBarNetBdFtAcre.DisplayFormat.FormatString = "{0:n2}";
            this.colVBarNetBdFtAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colVBarNetBdFtAcre.FieldName = "VBarNetBdFtAcre";
            this.colVBarNetBdFtAcre.Name = "colVBarNetBdFtAcre";
            this.colVBarNetBdFtAcre.Visible = true;
            this.colVBarNetBdFtAcre.VisibleIndex = 13;
            this.colVBarNetBdFtAcre.Width = 68;
            // 
            // colTreesPerAcre
            // 
            this.colTreesPerAcre.Caption = "Trees /Acre";
            this.colTreesPerAcre.DisplayFormat.FormatString = "{0:n3}";
            this.colTreesPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreesPerAcre.FieldName = "TreesPerAcre";
            this.colTreesPerAcre.Name = "colTreesPerAcre";
            this.colTreesPerAcre.Visible = true;
            this.colTreesPerAcre.VisibleIndex = 14;
            this.colTreesPerAcre.Width = 44;
            // 
            // colTreeCount
            // 
            this.colTreeCount.DisplayFormat.FormatString = "{0:n0}";
            this.colTreeCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeCount.FieldName = "TreeCount";
            this.colTreeCount.Name = "colTreeCount";
            this.colTreeCount.Visible = true;
            this.colTreeCount.VisibleIndex = 15;
            this.colTreeCount.Width = 45;
            // 
            // colLogsPerAcre
            // 
            this.colLogsPerAcre.Caption = "Logs /Acre";
            this.colLogsPerAcre.FieldName = "LogsPerAcre";
            this.colLogsPerAcre.Name = "colLogsPerAcre";
            this.colLogsPerAcre.Width = 45;
            // 
            // colTotalCcf
            // 
            this.colTotalCcf.Caption = "Total CCF";
            this.colTotalCcf.DisplayFormat.FormatString = "{0:n0}";
            this.colTotalCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalCcf.FieldName = "TotalCcf";
            this.colTotalCcf.Name = "colTotalCcf";
            this.colTotalCcf.Width = 50;
            // 
            // colTotalMbf
            // 
            this.colTotalMbf.Caption = "Total MBF";
            this.colTotalMbf.DisplayFormat.FormatString = "{0:n0}";
            this.colTotalMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalMbf.FieldName = "TotalMbf";
            this.colTotalMbf.Name = "colTotalMbf";
            this.colTotalMbf.Width = 47;
            // 
            // CountAvgForm
            // 
            this.ClientSize = new System.Drawing.Size(1071, 555);
            this.Controls.Add(this.gridControl2);
            this.Name = "CountAvgForm";
            this.Text = "Count Averages Table";
            this.Load += new System.EventHandler(this.CountAvgForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adjCruisePlotBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource countWithDbhBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInstance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecies;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbh;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFf;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalHt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVBarBdFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVBarCuFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLogsAcre;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcFf;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCalcCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCalcCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCalcBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCalcBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcVBarBdFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcVBarCuFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcLogsAcre;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource adjCruisePlotBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossCuFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colNetCuFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossBdFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colNetBdFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colBasalArea;
        private DevExpress.XtraGrid.Columns.GridColumn colQmDbh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colTractName;
        private DevExpress.XtraGrid.Columns.GridColumn colStandName;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsSelectedId;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossCuFtLogs;
        private DevExpress.XtraGrid.Columns.GridColumn colNetCuFtLogs;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossBdFtLogs;
        private DevExpress.XtraGrid.Columns.GridColumn colNetBdFtLogs;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colBoleHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colVBarNetCuFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colVBarNetBdFtAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTreesPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLogsPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalMbf;
    }
}