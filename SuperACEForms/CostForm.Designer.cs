﻿namespace SuperACEForms
{
    partial class CostForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEditTableName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.costsBindingSource = new System.Windows.Forms.BindingSource();
            this.haulingBindingSource = new System.Windows.Forms.BindingSource();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageCost = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlCost = new DevExpress.XtraGrid.GridControl();
            this.gridViewCost = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCostsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTypeGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportPrintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colTreatmentAcresPerYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearsApplied = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageHauling = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlHauling = new DevExpress.XtraGrid.GridControl();
            this.gridViewHauling = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHaulingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDestination = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinutes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoundTripMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadUnloadHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcCostDollarPerMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            this.xtraTabPageHauling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHauling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHauling)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barLargeButtonItem1,
            this.barButtonItem10,
            this.barButtonItem11});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Save";
            this.barButtonItem6.Id = 15;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save To New Table";
            this.barButtonItem7.Id = 16;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Delete Table";
            this.barButtonItem8.Id = 17;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Set as Default";
            this.barButtonItem9.Id = 18;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11, true)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 3";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Add";
            this.barButtonItem10.Id = 30;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Delete";
            this.barButtonItem11.Id = 31;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 611);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 560);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 51);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 560);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboBoxEditTableName);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 51);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 55);
            this.panelControl1.TabIndex = 4;
            // 
            // comboBoxEditTableName
            // 
            this.comboBoxEditTableName.Location = new System.Drawing.Point(93, 19);
            this.comboBoxEditTableName.MenuManager = this.barManager1;
            this.comboBoxEditTableName.Name = "comboBoxEditTableName";
            this.comboBoxEditTableName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTableName.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditTableName.TabIndex = 1;
            this.comboBoxEditTableName.SelectedValueChanged += new System.EventHandler(this.comboBoxEditTableName_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Table Name:";
            // 
            // costsBindingSource
            // 
            this.costsBindingSource.DataSource = typeof(Project.DAL.Cost);
            // 
            // haulingBindingSource
            // 
            this.haulingBindingSource.DataSource = typeof(Project.DAL.Hauling);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 106);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageCost;
            this.xtraTabControl1.Size = new System.Drawing.Size(1011, 505);
            this.xtraTabControl1.TabIndex = 20;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageCost,
            this.xtraTabPageHauling});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageCost
            // 
            this.xtraTabPageCost.Controls.Add(this.gridControlCost);
            this.xtraTabPageCost.Name = "xtraTabPageCost";
            this.xtraTabPageCost.Size = new System.Drawing.Size(1005, 477);
            this.xtraTabPageCost.Text = "Cost";
            // 
            // gridControlCost
            // 
            this.gridControlCost.DataSource = this.costsBindingSource;
            this.gridControlCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCost.Location = new System.Drawing.Point(0, 0);
            this.gridControlCost.MainView = this.gridViewCost;
            this.gridControlCost.Name = "gridControlCost";
            this.gridControlCost.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemTextEdit1});
            this.gridControlCost.Size = new System.Drawing.Size(1005, 477);
            this.gridControlCost.TabIndex = 13;
            this.gridControlCost.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCost});
            // 
            // gridViewCost
            // 
            this.gridViewCost.ColumnPanelRowHeight = 40;
            this.gridViewCost.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCostsID,
            this.colCostTypeGroup,
            this.colCostDescription,
            this.colCostDate,
            this.colTableName,
            this.colReportPrintOrder,
            this.colCostAmount,
            this.colUnits,
            this.colTreatmentAcresPerYear,
            this.colYearsApplied,
            this.colCostType,
            this.colPercent,
            this.colSpecies});
            this.gridViewCost.GridControl = this.gridControlCost;
            this.gridViewCost.GroupCount = 1;
            this.gridViewCost.Name = "gridViewCost";
            this.gridViewCost.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewCost.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewCost.OptionsCustomization.AllowFilter = false;
            this.gridViewCost.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewCost.OptionsCustomization.AllowSort = false;
            this.gridViewCost.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewCost.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewCost.OptionsView.ColumnAutoWidth = false;
            this.gridViewCost.OptionsView.ShowFooter = true;
            this.gridViewCost.OptionsView.ShowGroupPanel = false;
            this.gridViewCost.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCostTypeGroup, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewCost.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewCost_InitNewRow);
            this.gridViewCost.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewCost_ValidatingEditor);
            // 
            // colCostsID
            // 
            this.colCostsID.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostsID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostsID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostsID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostsID.FieldName = "CostsID";
            this.colCostsID.Name = "colCostsID";
            // 
            // colCostTypeGroup
            // 
            this.colCostTypeGroup.Caption = "Cost Type";
            this.colCostTypeGroup.FieldName = "CostTypeGroup";
            this.colCostTypeGroup.Name = "colCostTypeGroup";
            this.colCostTypeGroup.OptionsFilter.AllowFilter = false;
            this.colCostTypeGroup.Visible = true;
            this.colCostTypeGroup.VisibleIndex = 9;
            // 
            // colCostDescription
            // 
            this.colCostDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostDescription.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostDescription.Caption = "Item";
            this.colCostDescription.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCostDescription.FieldName = "CostDescription";
            this.colCostDescription.Name = "colCostDescription";
            this.colCostDescription.Visible = true;
            this.colCostDescription.VisibleIndex = 3;
            this.colCostDescription.Width = 200;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colCostDate
            // 
            this.colCostDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostDate.FieldName = "CostDate";
            this.colCostDate.Name = "colCostDate";
            // 
            // colTableName
            // 
            this.colTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colReportPrintOrder
            // 
            this.colReportPrintOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colReportPrintOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReportPrintOrder.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colReportPrintOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colReportPrintOrder.Caption = "Print Order";
            this.colReportPrintOrder.DisplayFormat.FormatString = "{0:#}";
            this.colReportPrintOrder.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colReportPrintOrder.FieldName = "ReportPrintOrder";
            this.colReportPrintOrder.Name = "colReportPrintOrder";
            this.colReportPrintOrder.Visible = true;
            this.colReportPrintOrder.VisibleIndex = 2;
            this.colReportPrintOrder.Width = 50;
            // 
            // colCostAmount
            // 
            this.colCostAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostAmount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostAmount.DisplayFormat.FormatString = "{0:#,0.00}";
            this.colCostAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCostAmount.FieldName = "CostAmount";
            this.colCostAmount.Name = "colCostAmount";
            this.colCostAmount.Visible = true;
            this.colCostAmount.VisibleIndex = 5;
            this.colCostAmount.Width = 125;
            // 
            // colUnits
            // 
            this.colUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUnits.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUnits.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUnits.ColumnEdit = this.repositoryItemComboBox4;
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.Visible = true;
            this.colUnits.VisibleIndex = 6;
            this.colUnits.Width = 51;
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // colTreatmentAcresPerYear
            // 
            this.colTreatmentAcresPerYear.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreatmentAcresPerYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreatmentAcresPerYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreatmentAcresPerYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreatmentAcresPerYear.Caption = "Acres Per Year";
            this.colTreatmentAcresPerYear.DisplayFormat.FormatString = "{0:#.##}";
            this.colTreatmentAcresPerYear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreatmentAcresPerYear.FieldName = "TreatmentAcresPerYear";
            this.colTreatmentAcresPerYear.Name = "colTreatmentAcresPerYear";
            this.colTreatmentAcresPerYear.Visible = true;
            this.colTreatmentAcresPerYear.VisibleIndex = 7;
            this.colTreatmentAcresPerYear.Width = 66;
            // 
            // colYearsApplied
            // 
            this.colYearsApplied.AppearanceHeader.Options.UseTextOptions = true;
            this.colYearsApplied.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearsApplied.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colYearsApplied.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colYearsApplied.Caption = "Years";
            this.colYearsApplied.ColumnEdit = this.repositoryItemTextEdit1;
            this.colYearsApplied.FieldName = "YearsApplied";
            this.colYearsApplied.Name = "colYearsApplied";
            this.colYearsApplied.Visible = true;
            this.colYearsApplied.VisibleIndex = 8;
            this.colYearsApplied.Width = 46;
            // 
            // colCostType
            // 
            this.colCostType.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostType.ColumnEdit = this.repositoryItemComboBox3;
            this.colCostType.FieldName = "CostType";
            this.colCostType.Name = "colCostType";
            this.colCostType.Visible = true;
            this.colCostType.VisibleIndex = 1;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // colPercent
            // 
            this.colPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercent.Caption = "%";
            this.colPercent.FieldName = "Percent";
            this.colPercent.Name = "colPercent";
            this.colPercent.Visible = true;
            this.colPercent.VisibleIndex = 4;
            this.colPercent.Width = 52;
            // 
            // colSpecies
            // 
            this.colSpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 0;
            // 
            // xtraTabPageHauling
            // 
            this.xtraTabPageHauling.Controls.Add(this.gridControlHauling);
            this.xtraTabPageHauling.Name = "xtraTabPageHauling";
            this.xtraTabPageHauling.Size = new System.Drawing.Size(1005, 479);
            this.xtraTabPageHauling.Text = "Hauling";
            // 
            // gridControlHauling
            // 
            this.gridControlHauling.DataSource = this.haulingBindingSource;
            this.gridControlHauling.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHauling.Location = new System.Drawing.Point(0, 0);
            this.gridControlHauling.MainView = this.gridViewHauling;
            this.gridControlHauling.MenuManager = this.barManager1;
            this.gridControlHauling.Name = "gridControlHauling";
            this.gridControlHauling.Size = new System.Drawing.Size(1005, 479);
            this.gridControlHauling.TabIndex = 2;
            this.gridControlHauling.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHauling});
            // 
            // gridViewHauling
            // 
            this.gridViewHauling.ColumnPanelRowHeight = 50;
            this.gridViewHauling.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHaulingID,
            this.colDestination,
            this.colAvgLoadSize,
            this.colMinutes,
            this.colRoundTripMiles,
            this.colHours,
            this.colCostPerHour,
            this.gridColumn1,
            this.colAvgLoadCcf,
            this.colAvgLoadMbf,
            this.colLoadUnloadHours,
            this.colCalcCostDollarPerMbf});
            this.gridViewHauling.GridControl = this.gridControlHauling;
            this.gridViewHauling.Name = "gridViewHauling";
            this.gridViewHauling.OptionsCustomization.AllowFilter = false;
            this.gridViewHauling.OptionsCustomization.AllowGroup = false;
            this.gridViewHauling.OptionsCustomization.AllowRowSizing = true;
            this.gridViewHauling.OptionsCustomization.AllowSort = false;
            this.gridViewHauling.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewHauling.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewHauling.OptionsView.ColumnAutoWidth = false;
            this.gridViewHauling.OptionsView.ShowGroupPanel = false;
            this.gridViewHauling.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewHauling_ValidateRow);
            // 
            // colHaulingID
            // 
            this.colHaulingID.AppearanceHeader.Options.UseTextOptions = true;
            this.colHaulingID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHaulingID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHaulingID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHaulingID.FieldName = "HaulingID";
            this.colHaulingID.Name = "colHaulingID";
            // 
            // colDestination
            // 
            this.colDestination.AppearanceHeader.Options.UseTextOptions = true;
            this.colDestination.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDestination.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDestination.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDestination.FieldName = "Destination";
            this.colDestination.Name = "colDestination";
            this.colDestination.Visible = true;
            this.colDestination.VisibleIndex = 0;
            this.colDestination.Width = 91;
            // 
            // colAvgLoadSize
            // 
            this.colAvgLoadSize.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadSize.Caption = "Avg Load Tons";
            this.colAvgLoadSize.DisplayFormat.FormatString = "{0:0.0}";
            this.colAvgLoadSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadSize.FieldName = "AvgLoadSize";
            this.colAvgLoadSize.Name = "colAvgLoadSize";
            this.colAvgLoadSize.Visible = true;
            this.colAvgLoadSize.VisibleIndex = 1;
            this.colAvgLoadSize.Width = 38;
            // 
            // colMinutes
            // 
            this.colMinutes.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinutes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinutes.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinutes.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinutes.DisplayFormat.FormatString = "{0:0}";
            this.colMinutes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinutes.FieldName = "Minutes";
            this.colMinutes.Name = "colMinutes";
            this.colMinutes.Visible = true;
            this.colMinutes.VisibleIndex = 6;
            this.colMinutes.Width = 52;
            // 
            // colRoundTripMiles
            // 
            this.colRoundTripMiles.AppearanceHeader.Options.UseTextOptions = true;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRoundTripMiles.Caption = "Round Trip Miles";
            this.colRoundTripMiles.DisplayFormat.FormatString = "{0:0}";
            this.colRoundTripMiles.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRoundTripMiles.FieldName = "RoundTripMiles";
            this.colRoundTripMiles.Name = "colRoundTripMiles";
            this.colRoundTripMiles.Visible = true;
            this.colRoundTripMiles.VisibleIndex = 4;
            this.colRoundTripMiles.Width = 42;
            // 
            // colHours
            // 
            this.colHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHours.DisplayFormat.FormatString = "{0:0}";
            this.colHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 5;
            this.colHours.Width = 45;
            // 
            // colCostPerHour
            // 
            this.colCostPerHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostPerHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostPerHour.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostPerHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostPerHour.Caption = "Truck Cost per Hour $";
            this.colCostPerHour.DisplayFormat.FormatString = "{0:0.00}";
            this.colCostPerHour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCostPerHour.FieldName = "CostPerHour";
            this.colCostPerHour.Name = "colCostPerHour";
            this.colCostPerHour.Visible = true;
            this.colCostPerHour.VisibleIndex = 8;
            this.colCostPerHour.Width = 51;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.FieldName = "TableName";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // colAvgLoadCcf
            // 
            this.colAvgLoadCcf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadCcf.Caption = "Avg Load Ccf";
            this.colAvgLoadCcf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadCcf.FieldName = "AvgLoadCcf";
            this.colAvgLoadCcf.Name = "colAvgLoadCcf";
            this.colAvgLoadCcf.Visible = true;
            this.colAvgLoadCcf.VisibleIndex = 2;
            this.colAvgLoadCcf.Width = 46;
            // 
            // colAvgLoadMbf
            // 
            this.colAvgLoadMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadMbf.Caption = "Avg Load Mbf";
            this.colAvgLoadMbf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadMbf.FieldName = "AvgLoadMbf";
            this.colAvgLoadMbf.Name = "colAvgLoadMbf";
            this.colAvgLoadMbf.Visible = true;
            this.colAvgLoadMbf.VisibleIndex = 3;
            this.colAvgLoadMbf.Width = 44;
            // 
            // colLoadUnloadHours
            // 
            this.colLoadUnloadHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoadUnloadHours.Caption = "Load and Unload Hours";
            this.colLoadUnloadHours.DisplayFormat.FormatString = "n2";
            this.colLoadUnloadHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLoadUnloadHours.FieldName = "LoadUnloadHours";
            this.colLoadUnloadHours.Name = "colLoadUnloadHours";
            this.colLoadUnloadHours.Visible = true;
            this.colLoadUnloadHours.VisibleIndex = 7;
            this.colLoadUnloadHours.Width = 58;
            // 
            // colCalcCostDollarPerMbf
            // 
            this.colCalcCostDollarPerMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcCostDollarPerMbf.Caption = "Calculated Cost $/Mbf";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatString = "n0";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcCostDollarPerMbf.FieldName = "CalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.Name = "colCalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.OptionsColumn.AllowEdit = false;
            this.colCalcCostDollarPerMbf.OptionsColumn.AllowFocus = false;
            this.colCalcCostDollarPerMbf.OptionsColumn.ReadOnly = true;
            this.colCalcCostDollarPerMbf.Visible = true;
            this.colCalcCostDollarPerMbf.VisibleIndex = 9;
            this.colCalcCostDollarPerMbf.Width = 68;
            // 
            // CostForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 638);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CostForm";
            this.Text = "Cost and Hauling Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CostForm_FormClosing);
            this.Load += new System.EventHandler(this.CostForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageCost.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            this.xtraTabPageHauling.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHauling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHauling)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTableName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private System.Windows.Forms.BindingSource costsBindingSource;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private System.Windows.Forms.BindingSource haulingBindingSource;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCost;
        private DevExpress.XtraGrid.GridControl gridControlCost;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostsID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colCostDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCostDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportPrintOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCostAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraGrid.Columns.GridColumn colTreatmentAcresPerYear;
        private DevExpress.XtraGrid.Columns.GridColumn colYearsApplied;
        private DevExpress.XtraGrid.Columns.GridColumn colCostType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn colPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHauling;
        private DevExpress.XtraGrid.GridControl gridControlHauling;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHauling;
        private DevExpress.XtraGrid.Columns.GridColumn colHaulingID;
        private DevExpress.XtraGrid.Columns.GridColumn colDestination;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadSize;
        private DevExpress.XtraGrid.Columns.GridColumn colMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colRoundTripMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadUnloadHours;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcCostDollarPerMbf;
    }
}