﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;
using System.Data.Entity;

namespace SuperACEForms
{
    public partial class PoleForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        //private string dataSource = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public PoleForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            //dataSource = pDataSource;
            tableName = pTableName;
        }

        private void PoleForm_Load(object sender, EventArgs e)
        {
            comboBoxEditSpecies.Text = tableName;
            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            PopulateBindingSource(tableName, comboBoxEditSpecies.Text);
        }

        private void LoadData()
        {
            comboBoxEditSpecies.Properties.Items.Clear();
            var groupSpecies = ctx.Poles.OrderBy(p => p.SpeciesCodes).ToList();
            foreach (var item in groupSpecies)
            {
                comboBoxEditSpecies.Properties.Items.Add(item.SpeciesCodes);
            }

            PopulateBindingSource(tableName, "");
        }

        private void PopulateBindingSource(string pTableName, string pSpecies)
        {
            if (tblBindingSource != null)
                tblBindingSource.Clear();

            if (string.IsNullOrEmpty(pSpecies))
                tblBindingSource.DataSource = ctx.Poles.Where(t => t.TableName == pTableName).ToList();
            else
                tblBindingSource.DataSource = ctx.Poles.Where(t => t.TableName == pTableName && t.SpeciesCodes == pSpecies).ToList();

            if (tblBindingSource.Count > 0)
            {
                var id = ((Pole)tblBindingSource.Current).PolesId; // tblColl[0].Id;
                polesDtlsBindingSource.DataSource = ctx.PolesDtls.OrderBy(d => d.Length).Where(d => d.PolesId == id).ToList();

                poleDimensionsDtlsBindingSource.DataSource = ctx.PoleDimensionsDtls.OrderBy(d => d.Length).Where(d => d.PolesId == id).ToList();
            }

            comboBoxEditSpecies.Text = ((Pole)tblBindingSource.Current).SpeciesCodes;
            comboBoxEditPricesPer.Text = ((Pole)tblBindingSource.Current).PiecesPer;
        }

        private void LoadTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref ctx);
            //foreach (var tblItem in tbls)
            //    comboBoxEditTableName.Properties.Items.Add(tblItem);
            //comboBoxEditTableName.Text = tableName;

            //List<PriceSawUtil> suColl = ctx.PriceSawUtil.OrderBy(s => s.Code).Where(s => s.TableName == "GENERAL").ToList();
            //priceSawUtilBindingSource.DataSource = suColl;

            //List<ProjectLocationModel.VolumeUnits> puColl = ProjectLocationBLL.GetVolumeUnits(ref ctx);
            //priceUnitBindingSource.DataSource = puColl;

            //List<PriceDestination> pdColl = ctx.PriceDestination.OrderBy(d => d.Code).Where(d => d.TableName == "GENERAL").ToList();
            //priceDestinationBindingSource.DataSource = pdColl;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            tblBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditSpecies.Text == currentProject.SpeciesTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditSpecies.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault(); ;
            currentProject.SpeciesTableName = comboBoxEditSpecies.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void PoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }

        private void comboBoxEditPricesPer_SelectedValueChanged(object sender, EventArgs e)
        {

        }
    }
}