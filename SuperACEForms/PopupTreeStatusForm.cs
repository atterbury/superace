﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace SuperACE
{
    public partial class PopupTreeStatusForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext ctx = null;
        private string projectTable = null;
        public string code = string.Empty;

        public PopupTreeStatusForm(ref ProjectDbContext pCtx, string pProjectTable)
        {
            InitializeComponent();

            ctx = pCtx;
            projectTable = pProjectTable;
        }

        private void PopupTreeStatusForm_Load(object sender, EventArgs e)
        {
            List<Treestatu> coll =
                ctx.Treestatus.OrderBy(s => s.DisplayCode).Where(s => s.TableName == projectTable).ToList();
            bindingSource.DataSource = coll;
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    Treestatu rec = bindingSource.Current as Treestatu;
                    code = rec.DisplayCode;
                    this.Close();
                    break;
                case Keys.Escape:
                    code = string.Empty;
                    this.Close();
                    break;
            }
        }
    }
}