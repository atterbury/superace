﻿namespace SuperACEForms
{
    partial class CalcLDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBaf = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbDbh = new DevExpress.XtraEditors.TextEdit();
            this.tbActualDistance = new DevExpress.XtraEditors.TextEdit();
            this.tbLimitingDistance = new DevExpress.XtraEditors.TextEdit();
            this.tbTreeIs = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbActualDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLimitingDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreeIs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbBaf
            // 
            this.tbBaf.EnterMoveNextControl = true;
            this.tbBaf.Location = new System.Drawing.Point(144, 22);
            this.tbBaf.Name = "tbBaf";
            this.tbBaf.Size = new System.Drawing.Size(46, 20);
            this.tbBaf.TabIndex = 0;
            this.tbBaf.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(103, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "BAF:";
            // 
            // tbDbh
            // 
            this.tbDbh.EnterMoveNextControl = true;
            this.tbDbh.Location = new System.Drawing.Point(144, 48);
            this.tbDbh.Name = "tbDbh";
            this.tbDbh.Size = new System.Drawing.Size(46, 20);
            this.tbDbh.TabIndex = 2;
            this.tbDbh.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbDbh.Validated += new System.EventHandler(this.tbDbh_Validated);
            // 
            // tbActualDistance
            // 
            this.tbActualDistance.EnterMoveNextControl = true;
            this.tbActualDistance.Location = new System.Drawing.Point(144, 91);
            this.tbActualDistance.Name = "tbActualDistance";
            this.tbActualDistance.Size = new System.Drawing.Size(46, 20);
            this.tbActualDistance.TabIndex = 3;
            this.tbActualDistance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbActualDistance.Validated += new System.EventHandler(this.tbActualDistance_Validated);
            // 
            // tbLimitingDistance
            // 
            this.tbLimitingDistance.EnterMoveNextControl = true;
            this.tbLimitingDistance.Location = new System.Drawing.Point(144, 117);
            this.tbLimitingDistance.Name = "tbLimitingDistance";
            this.tbLimitingDistance.Size = new System.Drawing.Size(46, 20);
            this.tbLimitingDistance.TabIndex = 4;
            this.tbLimitingDistance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbTreeIs
            // 
            this.tbTreeIs.EnterMoveNextControl = true;
            this.tbTreeIs.Location = new System.Drawing.Point(144, 143);
            this.tbTreeIs.Name = "tbTreeIs";
            this.tbTreeIs.Size = new System.Drawing.Size(46, 20);
            this.tbTreeIs.TabIndex = 5;
            this.tbTreeIs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(103, 51);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Dbh:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(48, 94);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(78, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Actual Distance:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(43, 120);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(83, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Limiting Distance:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(88, 146);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(38, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Tree Is:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 193);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 10;
            this.simpleButton1.Text = "Calc";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(106, 193);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 11;
            this.simpleButton2.Text = "Update";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(200, 193);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 12;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CalcLDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 236);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.tbTreeIs);
            this.Controls.Add(this.tbLimitingDistance);
            this.Controls.Add(this.tbActualDistance);
            this.Controls.Add(this.tbDbh);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.tbBaf);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcLDForm";
            this.Text = "Calc Limiting Distance";
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbActualDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLimitingDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreeIs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit tbBaf;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbDbh;
        private DevExpress.XtraEditors.TextEdit tbActualDistance;
        private DevExpress.XtraEditors.TextEdit tbLimitingDistance;
        private DevExpress.XtraEditors.TextEdit tbTreeIs;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}