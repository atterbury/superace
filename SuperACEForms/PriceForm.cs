﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class PriceForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        //private string dataSource = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public PriceForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            //dataSource = pDataSource;
            tableName = pTableName;
        }

        private void PriceForm_Load(object sender, EventArgs e)
        {
            comboBoxEditTableName.Text = tableName;

            LoadTableNames();
            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (tblBindingSource.Count > 0)
                tblBindingSource.Clear();
            tblBindingSource.DataSource = ctx.Prices.OrderBy(t => t.Species).Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", tblBindingSource.Count);
        }

        private void LoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetPriceTableNames(ref ctx);
            comboBoxEditTableName.Properties.Items.Clear();
            foreach (var tblItem in tbls)
                comboBoxEditTableName.Properties.Items.Add(tblItem);
            comboBoxEditTableName.Text = tableName;

            //List<PriceSawUtil> suColl = ctx.PriceSawUtil.OrderBy(s => s.Code).Where(s => s.TableName == "GENERAL").ToList();
            //priceSawUtilBindingSource.DataSource = suColl;

            //List<ProjectLocationModel.VolumeUnits> puColl = ProjectLocationBLL.GetVolumeUnits(ref ctx);
            //priceUnitBindingSource.DataSource = puColl;

            //List<PriceDestination> pdColl = ctx.PriceDestination.OrderBy(d => d.Code).Where(d => d.TableName == "GENERAL").ToList();
            //priceDestinationBindingSource.DataSource = pdColl;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            tblBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
            bool bProcess = false;
            string tableName = string.Empty;

            CopyTableForm frm = new CopyTableForm(ref ctx, "Price");
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bProcess = frm.bProcess;
            tableName = frm.tableName;
            frm.Dispose();

            List<Price> coll = ctx.Prices.Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            foreach (var item in coll)
            {
                Price newRec = new Price();
                newRec.Destination = item.Destination;
                newRec.Grades = item.Grades;
                newRec.LengthClass1215 = item.LengthClass1215;
                newRec.LengthClass1621 = item.LengthClass2223;
                newRec.LengthClass2427 = item.LengthClass2427;
                newRec.LengthClass2831 = item.LengthClass3235;
                newRec.LengthClass3639 = item.LengthClass3639;
                newRec.LengthClass40 = item.LengthClass40;
                newRec.MaximumButt = item.MaximumButt;
                newRec.MaximumDiameter = item.MaximumDiameter;
                newRec.MinimumDiameter = item.MinimumDiameter;
                newRec.PriceByLengthCategory1 = item.PriceByLengthCategory1;
                newRec.PriceByLengthCategory2 = item.PriceByLengthCategory2;
                newRec.PriceByLengthCategory3 = item.PriceByLengthCategory3;
                newRec.PriceByLengthCategory4 = item.PriceByLengthCategory4;
                newRec.PriceByLengthCategory5 = item.PriceByLengthCategory5;
                newRec.PriceDate = item.PriceDate;
                newRec.ProductName = item.ProductName;
                newRec.SawUtil = item.SawUtil;
                newRec.Sorts = item.Sorts;
                newRec.Species = item.Species;
                newRec.TableName = tableName;
                newRec.Units = item.Units;
                ctx.Prices.Add(newRec);
            }

            ctx.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditTableName.Text == currentProject.SpeciesTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditTableName.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;

            ctx.Prices.RemoveRange(ctx.Prices.Where(s => s.TableName == comboBoxEditTableName.Text));
            ctx.SaveChanges();

            comboBoxEditTableName.Text = tableName;

            LoadTableNames();

            LoadData();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault(); ;
            currentProject.SpeciesTableName = comboBoxEditTableName.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void SpeciesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }
    }
}