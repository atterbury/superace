﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using DevExpress.XtraGrid;

namespace SuperACEForms
{
    public partial class PopupReportAsDtls : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext db;
        private DefReportA reportAs;
        private Cost currentCostRecord;
        private string costTableName;

        public PopupReportAsDtls(ref ProjectDbContext pContext, Project.DAL.DefReportA pReportAs, string pCostTableName)
        {
            InitializeComponent();

            db = pContext;
            reportAs = pReportAs;
            costTableName = pCostTableName;
        }

        private void PopupReportAsDtls_Load(object sender, EventArgs e)
        {
            costBindingSource.Clear();
            costBindingSource.DataSource = db.Costs.OrderBy(c => c.CostType).ThenBy(c => c.CostDescription).Where(c => c.TableName == costTableName).ToList();

            LoadDefReportAsDtlsSource();
            //defReportAsDtlBindingSource.DataSource = db.DefReportAsDtls.Where(d => d.DefReportAsId == reportAs.Id).OrderBy(d => d.Item).ToList();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView1.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteDefReportAsDtlsRecord();
                    }
                    break;
            }
        }

        private void LoadDefReportAsDtlsSource()
        {
            if (defReportAsDtlBindingSource.Count > 0)
                defReportAsDtlBindingSource.Clear();
            List<DefReportAsDtl> defReportAsDtlsColl = db.DefReportAsDtls.Where(d => d.DefReportAsId == reportAs.Id).OrderBy(d => d.Item).ToList();
            defReportAsDtlBindingSource.DataSource = defReportAsDtlsColl;
        }

        private void DeleteDefReportAsDtlsRecord()
        {
            DefReportAsDtl rec = defReportAsDtlBindingSource.Current as DefReportAsDtl;

            string msg = string.Format("Delete {0} Cost Item?", rec.Item);
            if (XtraMessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            
            db.DefReportAsDtls.Remove(rec);
            db.SaveChanges();
            LoadDefReportAsDtlsSource();
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
            {
                AddDefReportAsDtlsRecord();
            }
        }

        private void AddDefReportAsDtlsRecord()
        {
            DefReportAsDtl newRec = defReportAsDtlBindingSource.Current as DefReportAsDtl;
            if (newRec != null)
            {
                DefReportAsDtl rec = new DefReportAsDtl();
                rec.DefReportAsId = reportAs.Id;
                rec.CostsId = currentCostRecord.CostsId;
                rec.CostType = currentCostRecord.CostType;
                if (currentCostRecord.Species == null)
                    rec.Species = string.Empty;
                else
                    rec.Species = currentCostRecord.Species;
                rec.Item = currentCostRecord.CostDescription;
                db.DefReportAsDtls.Add(rec);
                db.SaveChanges();
            }
        }

        private void repositoryItemLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit edit = sender as LookUpEdit;
            currentCostRecord = db.Costs.FirstOrDefault(c => c.TableName == costTableName && c.CostDescription == edit.EditValue.ToString());
        }
    }
}