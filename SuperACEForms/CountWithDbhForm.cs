﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class CountWithDbhForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext projectContext = null;
        private Stand stand = null;

        public bool wasProjectChanged = false;

        public CountWithDbhForm(ref ProjectDbContext pContext, Stand pStand)
        {
            InitializeComponent();

            projectContext = pContext;
            stand = pStand;
        }

        private void CountWithDbhForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            LoadDataSource();
        }

        private void LoadDataSource()
        {
            if (countWithDbhBindingSource.Count > 0)
                countWithDbhBindingSource.Clear();
            countWithDbhBindingSource.DataSource = projectContext.CountWithDbhs.OrderBy(t => t.Species).ThenBy(t => t.Status).ThenBy(t => t.Instance).Where(t => t.StandsId == stand.StandsId).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", countWithDbhBindingSource.Count);
        }

        private void CountWithDbhForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }
    }
}