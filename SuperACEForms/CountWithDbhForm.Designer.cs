﻿namespace SuperACEForms
{
    partial class CountWithDbhForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.countWithDbhBindingSource = new System.Windows.Forms.BindingSource();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colInstance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDbh = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStandsId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFf = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalHt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrossCfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetCfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrossBfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetBfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVBarBdFt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVBarCuFt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLogsAcre = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCalcFf = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcTotalHt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrossCalcCfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetCalcCfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrossCalcBfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetCalcBfTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcVBarBdFt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcVBarCuFt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcLogsAcre = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countWithDbhBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barLargeButtonItem1});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 38;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 609);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 585);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 585);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.countWithDbhBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 24);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1011, 585);
            this.gridControl1.TabIndex = 15;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // countWithDbhBindingSource
            // 
            this.countWithDbhBindingSource.DataSource = typeof(Project.DAL.CountWithDbh);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand1,
            this.gridBand2});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colId,
            this.colStandsId,
            this.colInstance,
            this.colSpecies,
            this.colStatus,
            this.colDbh,
            this.colTotalHt,
            this.colCalcTotalHt,
            this.colGrossCfTree,
            this.colGrossCalcCfTree,
            this.colNetCfTree,
            this.colNetCalcCfTree,
            this.colGrossBfTree,
            this.colGrossCalcBfTree,
            this.colNetBfTree,
            this.colNetCalcBfTree,
            this.colVBarBdFt,
            this.colCalcVBarBdFt,
            this.colVBarCuFt,
            this.colCalcVBarCuFt,
            this.colLogsAcre,
            this.colCalcLogsAcre,
            this.colFf,
            this.colCalcFf});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Identification";
            this.gridBand3.Columns.Add(this.colInstance);
            this.gridBand3.Columns.Add(this.colSpecies);
            this.gridBand3.Columns.Add(this.colStatus);
            this.gridBand3.Columns.Add(this.colDbh);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 205;
            // 
            // colInstance
            // 
            this.colInstance.Caption = "Instance";
            this.colInstance.FieldName = "Instance";
            this.colInstance.Name = "colInstance";
            this.colInstance.Visible = true;
            this.colInstance.Width = 56;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.Width = 46;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.Width = 49;
            // 
            // colDbh
            // 
            this.colDbh.Caption = "Dbh";
            this.colDbh.FieldName = "Dbh";
            this.colDbh.Name = "colDbh";
            this.colDbh.Visible = true;
            this.colDbh.Width = 54;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Original";
            this.gridBand1.Columns.Add(this.colId);
            this.gridBand1.Columns.Add(this.colStandsId);
            this.gridBand1.Columns.Add(this.colFf);
            this.gridBand1.Columns.Add(this.colTotalHt);
            this.gridBand1.Columns.Add(this.colGrossCfTree);
            this.gridBand1.Columns.Add(this.colNetCfTree);
            this.gridBand1.Columns.Add(this.colGrossBfTree);
            this.gridBand1.Columns.Add(this.colNetBfTree);
            this.gridBand1.Columns.Add(this.colVBarBdFt);
            this.gridBand1.Columns.Add(this.colVBarCuFt);
            this.gridBand1.Columns.Add(this.colLogsAcre);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 512;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colStandsId
            // 
            this.colStandsId.FieldName = "StandsId";
            this.colStandsId.Name = "colStandsId";
            // 
            // colFf
            // 
            this.colFf.Caption = "FF";
            this.colFf.FieldName = "Ff";
            this.colFf.Name = "colFf";
            this.colFf.Visible = true;
            this.colFf.Width = 34;
            // 
            // colTotalHt
            // 
            this.colTotalHt.Caption = "Total Ht";
            this.colTotalHt.FieldName = "TotalHt";
            this.colTotalHt.Name = "colTotalHt";
            this.colTotalHt.Visible = true;
            this.colTotalHt.Width = 52;
            // 
            // colGrossCfTree
            // 
            this.colGrossCfTree.Caption = "Gross CuFt";
            this.colGrossCfTree.FieldName = "GrossCfTree";
            this.colGrossCfTree.Name = "colGrossCfTree";
            this.colGrossCfTree.Visible = true;
            this.colGrossCfTree.Width = 63;
            // 
            // colNetCfTree
            // 
            this.colNetCfTree.Caption = "Net CuFt";
            this.colNetCfTree.FieldName = "NetCfTree";
            this.colNetCfTree.Name = "colNetCfTree";
            this.colNetCfTree.Visible = true;
            this.colNetCfTree.Width = 55;
            // 
            // colGrossBfTree
            // 
            this.colGrossBfTree.Caption = "Gross BdFt";
            this.colGrossBfTree.FieldName = "GrossBfTree";
            this.colGrossBfTree.Name = "colGrossBfTree";
            this.colGrossBfTree.Visible = true;
            this.colGrossBfTree.Width = 63;
            // 
            // colNetBfTree
            // 
            this.colNetBfTree.Caption = "Net BdFt";
            this.colNetBfTree.FieldName = "NetBfTree";
            this.colNetBfTree.Name = "colNetBfTree";
            this.colNetBfTree.Visible = true;
            this.colNetBfTree.Width = 52;
            // 
            // colVBarBdFt
            // 
            this.colVBarBdFt.Caption = "VBar BdFt";
            this.colVBarBdFt.FieldName = "VBarBdFt";
            this.colVBarBdFt.Name = "colVBarBdFt";
            this.colVBarBdFt.Visible = true;
            this.colVBarBdFt.Width = 57;
            // 
            // colVBarCuFt
            // 
            this.colVBarCuFt.Caption = "VBar CuFt";
            this.colVBarCuFt.FieldName = "VBarCuFt";
            this.colVBarCuFt.Name = "colVBarCuFt";
            this.colVBarCuFt.Visible = true;
            this.colVBarCuFt.Width = 59;
            // 
            // colLogsAcre
            // 
            this.colLogsAcre.Caption = "Logs /Acre";
            this.colLogsAcre.FieldName = "LogsAcre";
            this.colLogsAcre.Name = "colLogsAcre";
            this.colLogsAcre.Visible = true;
            this.colLogsAcre.Width = 77;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Calculated";
            this.gridBand2.Columns.Add(this.colCalcFf);
            this.gridBand2.Columns.Add(this.colCalcTotalHt);
            this.gridBand2.Columns.Add(this.colGrossCalcCfTree);
            this.gridBand2.Columns.Add(this.colNetCalcCfTree);
            this.gridBand2.Columns.Add(this.colGrossCalcBfTree);
            this.gridBand2.Columns.Add(this.colNetCalcBfTree);
            this.gridBand2.Columns.Add(this.colCalcVBarBdFt);
            this.gridBand2.Columns.Add(this.colCalcVBarCuFt);
            this.gridBand2.Columns.Add(this.colCalcLogsAcre);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 503;
            // 
            // colCalcFf
            // 
            this.colCalcFf.Caption = "FF";
            this.colCalcFf.DisplayFormat.FormatString = "{0:n0}";
            this.colCalcFf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcFf.FieldName = "CalcFf";
            this.colCalcFf.Name = "colCalcFf";
            this.colCalcFf.Visible = true;
            this.colCalcFf.Width = 30;
            // 
            // colCalcTotalHt
            // 
            this.colCalcTotalHt.Caption = "Total Ht";
            this.colCalcTotalHt.DisplayFormat.FormatString = "{0:n0}";
            this.colCalcTotalHt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcTotalHt.FieldName = "CalcTotalHt";
            this.colCalcTotalHt.Name = "colCalcTotalHt";
            this.colCalcTotalHt.Visible = true;
            this.colCalcTotalHt.Width = 50;
            // 
            // colGrossCalcCfTree
            // 
            this.colGrossCalcCfTree.Caption = "Gross CuFt";
            this.colGrossCalcCfTree.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossCalcCfTree.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossCalcCfTree.FieldName = "GrossCalcCfTree";
            this.colGrossCalcCfTree.Name = "colGrossCalcCfTree";
            this.colGrossCalcCfTree.Visible = true;
            this.colGrossCalcCfTree.Width = 64;
            // 
            // colNetCalcCfTree
            // 
            this.colNetCalcCfTree.Caption = "Net CuFt";
            this.colNetCalcCfTree.DisplayFormat.FormatString = "{0:n0}";
            this.colNetCalcCfTree.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetCalcCfTree.FieldName = "NetCalcCfTree";
            this.colNetCalcCfTree.Name = "colNetCalcCfTree";
            this.colNetCalcCfTree.Visible = true;
            this.colNetCalcCfTree.Width = 51;
            // 
            // colGrossCalcBfTree
            // 
            this.colGrossCalcBfTree.Caption = "Gross BdFt";
            this.colGrossCalcBfTree.DisplayFormat.FormatString = "{0:n0}";
            this.colGrossCalcBfTree.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossCalcBfTree.FieldName = "GrossCalcBfTree";
            this.colGrossCalcBfTree.Name = "colGrossCalcBfTree";
            this.colGrossCalcBfTree.Visible = true;
            this.colGrossCalcBfTree.Width = 60;
            // 
            // colNetCalcBfTree
            // 
            this.colNetCalcBfTree.Caption = "Net BdFt";
            this.colNetCalcBfTree.DisplayFormat.FormatString = "{0:n0}";
            this.colNetCalcBfTree.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetCalcBfTree.FieldName = "NetCalcBfTree";
            this.colNetCalcBfTree.Name = "colNetCalcBfTree";
            this.colNetCalcBfTree.Visible = true;
            this.colNetCalcBfTree.Width = 53;
            // 
            // colCalcVBarBdFt
            // 
            this.colCalcVBarBdFt.Caption = "VBar BdFt";
            this.colCalcVBarBdFt.DisplayFormat.FormatString = "{0:n0}";
            this.colCalcVBarBdFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcVBarBdFt.FieldName = "CalcVBarBdFt";
            this.colCalcVBarBdFt.Name = "colCalcVBarBdFt";
            this.colCalcVBarBdFt.Visible = true;
            this.colCalcVBarBdFt.Width = 60;
            // 
            // colCalcVBarCuFt
            // 
            this.colCalcVBarCuFt.Caption = "VBar CuFt";
            this.colCalcVBarCuFt.DisplayFormat.FormatString = "{0:n0}";
            this.colCalcVBarCuFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcVBarCuFt.FieldName = "CalcVBarCuFt";
            this.colCalcVBarCuFt.Name = "colCalcVBarCuFt";
            this.colCalcVBarCuFt.Visible = true;
            this.colCalcVBarCuFt.Width = 59;
            // 
            // colCalcLogsAcre
            // 
            this.colCalcLogsAcre.Caption = "Logs /Acre";
            this.colCalcLogsAcre.DisplayFormat.FormatString = "{0:n1}";
            this.colCalcLogsAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcLogsAcre.FieldName = "CalcLogsAcre";
            this.colCalcLogsAcre.Name = "colCalcLogsAcre";
            this.colCalcLogsAcre.Visible = true;
            this.colCalcLogsAcre.Width = 76;
            // 
            // CountWithDbhForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 636);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CountWithDbhForm";
            this.Text = "Count With Dbh Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CountWithDbhForm_FormClosing);
            this.Load += new System.EventHandler(this.CountWithDbhForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countWithDbhBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource countWithDbhBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInstance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecies;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbh;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFf;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalHt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVBarBdFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVBarCuFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLogsAcre;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcFf;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCalcCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCalcCfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrossCalcBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCalcBfTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcVBarBdFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcVBarCuFt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcLogsAcre;
    }
}