﻿namespace SuperACEForms
{
    partial class PoleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.tb10Max = new DevExpress.XtraEditors.TextEdit();
            this.tblBindingSource = new System.Windows.Forms.BindingSource();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.tb10Min = new DevExpress.XtraEditors.TextEdit();
            this.tb9Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.tb9Min = new DevExpress.XtraEditors.TextEdit();
            this.tb8Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.tb8Min = new DevExpress.XtraEditors.TextEdit();
            this.tb7Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.tb7Min = new DevExpress.XtraEditors.TextEdit();
            this.tb6Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.tb6Min = new DevExpress.XtraEditors.TextEdit();
            this.tb5Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.tb5Min = new DevExpress.XtraEditors.TextEdit();
            this.tb4Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.tb4Min = new DevExpress.XtraEditors.TextEdit();
            this.tb3Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.tb3Min = new DevExpress.XtraEditors.TextEdit();
            this.tb2Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.tb2Min = new DevExpress.XtraEditors.TextEdit();
            this.tb1Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.tb1Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH1Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.tbH1Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH2Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.tbH2Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH3Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.tbH3Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH4Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tbH4Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH5Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.tbH5Min = new DevExpress.XtraEditors.TextEdit();
            this.tbH6Max = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tbH6Min = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditPricesPer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditSpecies = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPagePrices = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.polesDtlsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPolesDtlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolesId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageDimensions = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.poleDimensionsDtlsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleDimensionsDtlId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolesId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colH11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colC101 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb10Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb10Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb9Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb9Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb8Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb8Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb7Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb7Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb6Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb6Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb5Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb5Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb4Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb4Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb3Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb3Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb2Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb2Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb1Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb1Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH1Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH1Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH2Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH2Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH3Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH3Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH4Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH4Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH5Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH5Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH6Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH6Min.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPricesPer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditSpecies.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPagePrices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polesDtlsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPageDimensions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poleDimensionsDtlsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barLargeButtonItem1,
            this.barButtonItem10,
            this.barButtonItem11});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Save";
            this.barButtonItem6.Id = 15;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save To New Table";
            this.barButtonItem7.Id = 16;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Delete";
            this.barButtonItem8.Id = 17;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Set as Default";
            this.barButtonItem9.Id = 18;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11, true)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 3";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Add";
            this.barButtonItem10.Id = 30;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Delete";
            this.barButtonItem11.Id = 31;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 514);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 463);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 51);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 463);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl21);
            this.panelControl1.Controls.Add(this.labelControl16);
            this.panelControl1.Controls.Add(this.labelControl15);
            this.panelControl1.Controls.Add(this.tb10Max);
            this.panelControl1.Controls.Add(this.labelControl17);
            this.panelControl1.Controls.Add(this.tb10Min);
            this.panelControl1.Controls.Add(this.tb9Max);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.tb9Min);
            this.panelControl1.Controls.Add(this.tb8Max);
            this.panelControl1.Controls.Add(this.labelControl19);
            this.panelControl1.Controls.Add(this.tb8Min);
            this.panelControl1.Controls.Add(this.tb7Max);
            this.panelControl1.Controls.Add(this.labelControl20);
            this.panelControl1.Controls.Add(this.tb7Min);
            this.panelControl1.Controls.Add(this.tb6Max);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.tb6Min);
            this.panelControl1.Controls.Add(this.tb5Max);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.tb5Min);
            this.panelControl1.Controls.Add(this.tb4Max);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.tb4Min);
            this.panelControl1.Controls.Add(this.tb3Max);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.tb3Min);
            this.panelControl1.Controls.Add(this.tb2Max);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.tb2Min);
            this.panelControl1.Controls.Add(this.tb1Max);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.tb1Min);
            this.panelControl1.Controls.Add(this.tbH1Max);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.tbH1Min);
            this.panelControl1.Controls.Add(this.tbH2Max);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.tbH2Min);
            this.panelControl1.Controls.Add(this.tbH3Max);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.tbH3Min);
            this.panelControl1.Controls.Add(this.tbH4Max);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.tbH4Min);
            this.panelControl1.Controls.Add(this.tbH5Max);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.tbH5Min);
            this.panelControl1.Controls.Add(this.tbH6Max);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.tbH6Min);
            this.panelControl1.Controls.Add(this.comboBoxEditPricesPer);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.comboBoxEditSpecies);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 51);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 142);
            this.panelControl1.TabIndex = 4;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(59, 107);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(41, 13);
            this.labelControl21.TabIndex = 54;
            this.labelControl21.Text = "Max Top";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(63, 81);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(37, 13);
            this.labelControl16.TabIndex = 53;
            this.labelControl16.Text = "Min Top";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(75, 59);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(25, 13);
            this.labelControl15.TabIndex = 52;
            this.labelControl15.Text = "Class";
            // 
            // tb10Max
            // 
            this.tb10Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C10Max", true));
            this.tb10Max.Location = new System.Drawing.Point(831, 104);
            this.tb10Max.MenuManager = this.barManager1;
            this.tb10Max.Name = "tb10Max";
            this.tb10Max.Size = new System.Drawing.Size(32, 20);
            this.tb10Max.TabIndex = 51;
            // 
            // tblBindingSource
            // 
            this.tblBindingSource.DataSource = typeof(Project.DAL.Pole);
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(841, 59);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(12, 13);
            this.labelControl17.TabIndex = 50;
            this.labelControl17.Text = "10";
            // 
            // tb10Min
            // 
            this.tb10Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C10Min", true));
            this.tb10Min.Location = new System.Drawing.Point(831, 78);
            this.tb10Min.MenuManager = this.barManager1;
            this.tb10Min.Name = "tb10Min";
            this.tb10Min.Size = new System.Drawing.Size(32, 20);
            this.tb10Min.TabIndex = 49;
            // 
            // tb9Max
            // 
            this.tb9Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C9Max", true));
            this.tb9Max.Location = new System.Drawing.Point(783, 104);
            this.tb9Max.MenuManager = this.barManager1;
            this.tb9Max.Name = "tb9Max";
            this.tb9Max.Size = new System.Drawing.Size(32, 20);
            this.tb9Max.TabIndex = 48;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(793, 59);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(6, 13);
            this.labelControl18.TabIndex = 47;
            this.labelControl18.Text = "9";
            // 
            // tb9Min
            // 
            this.tb9Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C9Min", true));
            this.tb9Min.Location = new System.Drawing.Point(783, 78);
            this.tb9Min.MenuManager = this.barManager1;
            this.tb9Min.Name = "tb9Min";
            this.tb9Min.Size = new System.Drawing.Size(32, 20);
            this.tb9Min.TabIndex = 46;
            // 
            // tb8Max
            // 
            this.tb8Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C8Max", true));
            this.tb8Max.Location = new System.Drawing.Point(735, 104);
            this.tb8Max.MenuManager = this.barManager1;
            this.tb8Max.Name = "tb8Max";
            this.tb8Max.Size = new System.Drawing.Size(32, 20);
            this.tb8Max.TabIndex = 45;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(745, 59);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(6, 13);
            this.labelControl19.TabIndex = 44;
            this.labelControl19.Text = "8";
            // 
            // tb8Min
            // 
            this.tb8Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C8Min", true));
            this.tb8Min.Location = new System.Drawing.Point(735, 78);
            this.tb8Min.MenuManager = this.barManager1;
            this.tb8Min.Name = "tb8Min";
            this.tb8Min.Size = new System.Drawing.Size(32, 20);
            this.tb8Min.TabIndex = 43;
            // 
            // tb7Max
            // 
            this.tb7Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C7Max", true));
            this.tb7Max.Location = new System.Drawing.Point(687, 104);
            this.tb7Max.MenuManager = this.barManager1;
            this.tb7Max.Name = "tb7Max";
            this.tb7Max.Size = new System.Drawing.Size(32, 20);
            this.tb7Max.TabIndex = 42;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(697, 59);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(6, 13);
            this.labelControl20.TabIndex = 41;
            this.labelControl20.Text = "7";
            // 
            // tb7Min
            // 
            this.tb7Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C7Min", true));
            this.tb7Min.Location = new System.Drawing.Point(687, 78);
            this.tb7Min.MenuManager = this.barManager1;
            this.tb7Min.Name = "tb7Min";
            this.tb7Min.Size = new System.Drawing.Size(32, 20);
            this.tb7Min.TabIndex = 40;
            // 
            // tb6Max
            // 
            this.tb6Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C6Max", true));
            this.tb6Max.Location = new System.Drawing.Point(641, 104);
            this.tb6Max.MenuManager = this.barManager1;
            this.tb6Max.Name = "tb6Max";
            this.tb6Max.Size = new System.Drawing.Size(32, 20);
            this.tb6Max.TabIndex = 39;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(651, 59);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 13);
            this.labelControl9.TabIndex = 38;
            this.labelControl9.Text = "6";
            // 
            // tb6Min
            // 
            this.tb6Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C6Min", true));
            this.tb6Min.Location = new System.Drawing.Point(641, 78);
            this.tb6Min.MenuManager = this.barManager1;
            this.tb6Min.Name = "tb6Min";
            this.tb6Min.Size = new System.Drawing.Size(32, 20);
            this.tb6Min.TabIndex = 37;
            // 
            // tb5Max
            // 
            this.tb5Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C5Max", true));
            this.tb5Max.Location = new System.Drawing.Point(593, 104);
            this.tb5Max.MenuManager = this.barManager1;
            this.tb5Max.Name = "tb5Max";
            this.tb5Max.Size = new System.Drawing.Size(32, 20);
            this.tb5Max.TabIndex = 36;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(603, 59);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 13);
            this.labelControl10.TabIndex = 5;
            this.labelControl10.Text = "5";
            // 
            // tb5Min
            // 
            this.tb5Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C5Min", true));
            this.tb5Min.Location = new System.Drawing.Point(593, 78);
            this.tb5Min.MenuManager = this.barManager1;
            this.tb5Min.Name = "tb5Min";
            this.tb5Min.Size = new System.Drawing.Size(32, 20);
            this.tb5Min.TabIndex = 34;
            // 
            // tb4Max
            // 
            this.tb4Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C4Max", true));
            this.tb4Max.Location = new System.Drawing.Point(545, 104);
            this.tb4Max.MenuManager = this.barManager1;
            this.tb4Max.Name = "tb4Max";
            this.tb4Max.Size = new System.Drawing.Size(32, 20);
            this.tb4Max.TabIndex = 33;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(555, 59);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 32;
            this.labelControl11.Text = "4";
            // 
            // tb4Min
            // 
            this.tb4Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C4Min", true));
            this.tb4Min.Location = new System.Drawing.Point(545, 78);
            this.tb4Min.MenuManager = this.barManager1;
            this.tb4Min.Name = "tb4Min";
            this.tb4Min.Size = new System.Drawing.Size(32, 20);
            this.tb4Min.TabIndex = 31;
            // 
            // tb3Max
            // 
            this.tb3Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C3Max", true));
            this.tb3Max.Location = new System.Drawing.Point(497, 104);
            this.tb3Max.MenuManager = this.barManager1;
            this.tb3Max.Name = "tb3Max";
            this.tb3Max.Size = new System.Drawing.Size(32, 20);
            this.tb3Max.TabIndex = 30;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(507, 59);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 13);
            this.labelControl12.TabIndex = 29;
            this.labelControl12.Text = "3";
            // 
            // tb3Min
            // 
            this.tb3Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C3Min", true));
            this.tb3Min.Location = new System.Drawing.Point(497, 78);
            this.tb3Min.MenuManager = this.barManager1;
            this.tb3Min.Name = "tb3Min";
            this.tb3Min.Size = new System.Drawing.Size(32, 20);
            this.tb3Min.TabIndex = 28;
            // 
            // tb2Max
            // 
            this.tb2Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C2Max", true));
            this.tb2Max.Location = new System.Drawing.Point(449, 104);
            this.tb2Max.MenuManager = this.barManager1;
            this.tb2Max.Name = "tb2Max";
            this.tb2Max.Size = new System.Drawing.Size(32, 20);
            this.tb2Max.TabIndex = 27;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(459, 59);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 13);
            this.labelControl13.TabIndex = 26;
            this.labelControl13.Text = "2";
            // 
            // tb2Min
            // 
            this.tb2Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C2Min", true));
            this.tb2Min.Location = new System.Drawing.Point(449, 78);
            this.tb2Min.MenuManager = this.barManager1;
            this.tb2Min.Name = "tb2Min";
            this.tb2Min.Size = new System.Drawing.Size(32, 20);
            this.tb2Min.TabIndex = 25;
            // 
            // tb1Max
            // 
            this.tb1Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C1Max", true));
            this.tb1Max.Location = new System.Drawing.Point(401, 104);
            this.tb1Max.MenuManager = this.barManager1;
            this.tb1Max.Name = "tb1Max";
            this.tb1Max.Size = new System.Drawing.Size(32, 20);
            this.tb1Max.TabIndex = 24;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(411, 59);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 13);
            this.labelControl14.TabIndex = 23;
            this.labelControl14.Text = "1";
            // 
            // tb1Min
            // 
            this.tb1Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "C1Min", true));
            this.tb1Min.Location = new System.Drawing.Point(401, 78);
            this.tb1Min.MenuManager = this.barManager1;
            this.tb1Min.Name = "tb1Min";
            this.tb1Min.Size = new System.Drawing.Size(32, 20);
            this.tb1Min.TabIndex = 22;
            // 
            // tbH1Max
            // 
            this.tbH1Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H1Max", true));
            this.tbH1Max.Location = new System.Drawing.Point(353, 104);
            this.tbH1Max.MenuManager = this.barManager1;
            this.tbH1Max.Name = "tbH1Max";
            this.tbH1Max.Size = new System.Drawing.Size(32, 20);
            this.tbH1Max.TabIndex = 21;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(363, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(13, 13);
            this.labelControl6.TabIndex = 20;
            this.labelControl6.Text = "H1";
            // 
            // tbH1Min
            // 
            this.tbH1Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H1Min", true));
            this.tbH1Min.Location = new System.Drawing.Point(353, 78);
            this.tbH1Min.MenuManager = this.barManager1;
            this.tbH1Min.Name = "tbH1Min";
            this.tbH1Min.Size = new System.Drawing.Size(32, 20);
            this.tbH1Min.TabIndex = 19;
            // 
            // tbH2Max
            // 
            this.tbH2Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H2Max", true));
            this.tbH2Max.Location = new System.Drawing.Point(305, 104);
            this.tbH2Max.MenuManager = this.barManager1;
            this.tbH2Max.Name = "tbH2Max";
            this.tbH2Max.Size = new System.Drawing.Size(32, 20);
            this.tbH2Max.TabIndex = 18;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(315, 59);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(13, 13);
            this.labelControl7.TabIndex = 17;
            this.labelControl7.Text = "H2";
            // 
            // tbH2Min
            // 
            this.tbH2Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H2Min", true));
            this.tbH2Min.Location = new System.Drawing.Point(305, 78);
            this.tbH2Min.MenuManager = this.barManager1;
            this.tbH2Min.Name = "tbH2Min";
            this.tbH2Min.Size = new System.Drawing.Size(32, 20);
            this.tbH2Min.TabIndex = 16;
            // 
            // tbH3Max
            // 
            this.tbH3Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H3Max", true));
            this.tbH3Max.Location = new System.Drawing.Point(257, 104);
            this.tbH3Max.MenuManager = this.barManager1;
            this.tbH3Max.Name = "tbH3Max";
            this.tbH3Max.Size = new System.Drawing.Size(32, 20);
            this.tbH3Max.TabIndex = 15;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(267, 59);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(13, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "H3";
            // 
            // tbH3Min
            // 
            this.tbH3Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H3Min", true));
            this.tbH3Min.Location = new System.Drawing.Point(257, 78);
            this.tbH3Min.MenuManager = this.barManager1;
            this.tbH3Min.Name = "tbH3Min";
            this.tbH3Min.Size = new System.Drawing.Size(32, 20);
            this.tbH3Min.TabIndex = 13;
            // 
            // tbH4Max
            // 
            this.tbH4Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H4Max", true));
            this.tbH4Max.Location = new System.Drawing.Point(209, 104);
            this.tbH4Max.MenuManager = this.barManager1;
            this.tbH4Max.Name = "tbH4Max";
            this.tbH4Max.Size = new System.Drawing.Size(32, 20);
            this.tbH4Max.TabIndex = 12;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(219, 59);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(13, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "H4";
            // 
            // tbH4Min
            // 
            this.tbH4Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H4Min", true));
            this.tbH4Min.Location = new System.Drawing.Point(209, 78);
            this.tbH4Min.MenuManager = this.barManager1;
            this.tbH4Min.Name = "tbH4Min";
            this.tbH4Min.Size = new System.Drawing.Size(32, 20);
            this.tbH4Min.TabIndex = 10;
            // 
            // tbH5Max
            // 
            this.tbH5Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H5Max", true));
            this.tbH5Max.Location = new System.Drawing.Point(161, 104);
            this.tbH5Max.MenuManager = this.barManager1;
            this.tbH5Max.Name = "tbH5Max";
            this.tbH5Max.Size = new System.Drawing.Size(32, 20);
            this.tbH5Max.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(171, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(13, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "H5";
            // 
            // tbH5Min
            // 
            this.tbH5Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H5Min", true));
            this.tbH5Min.Location = new System.Drawing.Point(161, 78);
            this.tbH5Min.MenuManager = this.barManager1;
            this.tbH5Min.Name = "tbH5Min";
            this.tbH5Min.Size = new System.Drawing.Size(32, 20);
            this.tbH5Min.TabIndex = 7;
            // 
            // tbH6Max
            // 
            this.tbH6Max.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H6Max", true));
            this.tbH6Max.Location = new System.Drawing.Point(113, 104);
            this.tbH6Max.MenuManager = this.barManager1;
            this.tbH6Max.Name = "tbH6Max";
            this.tbH6Max.Size = new System.Drawing.Size(32, 20);
            this.tbH6Max.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(123, 59);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(13, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "H6";
            // 
            // tbH6Min
            // 
            this.tbH6Min.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBindingSource, "H6Min", true));
            this.tbH6Min.Location = new System.Drawing.Point(113, 78);
            this.tbH6Min.MenuManager = this.barManager1;
            this.tbH6Min.Name = "tbH6Min";
            this.tbH6Min.Size = new System.Drawing.Size(32, 20);
            this.tbH6Min.TabIndex = 4;
            // 
            // comboBoxEditPricesPer
            // 
            this.comboBoxEditPricesPer.Location = new System.Drawing.Point(654, 19);
            this.comboBoxEditPricesPer.MenuManager = this.barManager1;
            this.comboBoxEditPricesPer.Name = "comboBoxEditPricesPer";
            this.comboBoxEditPricesPer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPricesPer.Properties.Items.AddRange(new object[] {
            "MBF",
            "CCF",
            "TONS",
            "PIECE",
            "LINEAL FT"});
            this.comboBoxEditPricesPer.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditPricesPer.TabIndex = 3;
            this.comboBoxEditPricesPer.SelectedValueChanged += new System.EventHandler(this.comboBoxEditPricesPer_SelectedValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(588, 22);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(51, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Prices Per:";
            // 
            // comboBoxEditSpecies
            // 
            this.comboBoxEditSpecies.Location = new System.Drawing.Point(113, 19);
            this.comboBoxEditSpecies.MenuManager = this.barManager1;
            this.comboBoxEditSpecies.Name = "comboBoxEditSpecies";
            this.comboBoxEditSpecies.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditSpecies.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditSpecies.TabIndex = 1;
            this.comboBoxEditSpecies.SelectedValueChanged += new System.EventHandler(this.comboBoxEditTableName_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Species Codes:";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 193);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPagePrices;
            this.xtraTabControl1.Size = new System.Drawing.Size(1011, 321);
            this.xtraTabControl1.TabIndex = 15;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPagePrices,
            this.xtraTabPageDimensions});
            // 
            // xtraTabPagePrices
            // 
            this.xtraTabPagePrices.Controls.Add(this.gridControl1);
            this.xtraTabPagePrices.Name = "xtraTabPagePrices";
            this.xtraTabPagePrices.Size = new System.Drawing.Size(1005, 293);
            this.xtraTabPagePrices.Text = "Prices";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.polesDtlsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1005, 293);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // polesDtlsBindingSource
            // 
            this.polesDtlsBindingSource.DataSource = typeof(Project.DAL.PolesDtl);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPolesDtlId,
            this.colPolesId,
            this.colLength,
            this.colH6,
            this.colH5,
            this.colH4,
            this.colH3,
            this.colH2,
            this.colH1,
            this.colC1,
            this.colC2,
            this.colC3,
            this.colC4,
            this.colC5,
            this.colC6,
            this.colC7,
            this.colC8,
            this.colC9,
            this.colC10});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colPolesDtlId
            // 
            this.colPolesDtlId.FieldName = "PolesDtlId";
            this.colPolesDtlId.Name = "colPolesDtlId";
            // 
            // colPolesId
            // 
            this.colPolesId.FieldName = "PolesId";
            this.colPolesId.Name = "colPolesId";
            // 
            // colLength
            // 
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 0;
            // 
            // colH6
            // 
            this.colH6.FieldName = "H6";
            this.colH6.Name = "colH6";
            this.colH6.Visible = true;
            this.colH6.VisibleIndex = 1;
            // 
            // colH5
            // 
            this.colH5.FieldName = "H5";
            this.colH5.Name = "colH5";
            this.colH5.Visible = true;
            this.colH5.VisibleIndex = 2;
            // 
            // colH4
            // 
            this.colH4.FieldName = "H4";
            this.colH4.Name = "colH4";
            this.colH4.Visible = true;
            this.colH4.VisibleIndex = 3;
            // 
            // colH3
            // 
            this.colH3.FieldName = "H3";
            this.colH3.Name = "colH3";
            this.colH3.Visible = true;
            this.colH3.VisibleIndex = 4;
            // 
            // colH2
            // 
            this.colH2.FieldName = "H2";
            this.colH2.Name = "colH2";
            this.colH2.Visible = true;
            this.colH2.VisibleIndex = 5;
            // 
            // colH1
            // 
            this.colH1.FieldName = "H1";
            this.colH1.Name = "colH1";
            this.colH1.Visible = true;
            this.colH1.VisibleIndex = 6;
            // 
            // colC1
            // 
            this.colC1.FieldName = "C1";
            this.colC1.Name = "colC1";
            this.colC1.Visible = true;
            this.colC1.VisibleIndex = 7;
            // 
            // colC2
            // 
            this.colC2.FieldName = "C2";
            this.colC2.Name = "colC2";
            this.colC2.Visible = true;
            this.colC2.VisibleIndex = 8;
            // 
            // colC3
            // 
            this.colC3.FieldName = "C3";
            this.colC3.Name = "colC3";
            this.colC3.Visible = true;
            this.colC3.VisibleIndex = 9;
            // 
            // colC4
            // 
            this.colC4.FieldName = "C4";
            this.colC4.Name = "colC4";
            this.colC4.Visible = true;
            this.colC4.VisibleIndex = 10;
            // 
            // colC5
            // 
            this.colC5.FieldName = "C5";
            this.colC5.Name = "colC5";
            this.colC5.Visible = true;
            this.colC5.VisibleIndex = 11;
            // 
            // colC6
            // 
            this.colC6.FieldName = "C6";
            this.colC6.Name = "colC6";
            this.colC6.Visible = true;
            this.colC6.VisibleIndex = 12;
            // 
            // colC7
            // 
            this.colC7.FieldName = "C7";
            this.colC7.Name = "colC7";
            this.colC7.Visible = true;
            this.colC7.VisibleIndex = 13;
            // 
            // colC8
            // 
            this.colC8.FieldName = "C8";
            this.colC8.Name = "colC8";
            this.colC8.Visible = true;
            this.colC8.VisibleIndex = 14;
            // 
            // colC9
            // 
            this.colC9.FieldName = "C9";
            this.colC9.Name = "colC9";
            this.colC9.Visible = true;
            this.colC9.VisibleIndex = 15;
            // 
            // colC10
            // 
            this.colC10.FieldName = "C10";
            this.colC10.Name = "colC10";
            this.colC10.Visible = true;
            this.colC10.VisibleIndex = 16;
            // 
            // xtraTabPageDimensions
            // 
            this.xtraTabPageDimensions.Controls.Add(this.gridControl2);
            this.xtraTabPageDimensions.Name = "xtraTabPageDimensions";
            this.xtraTabPageDimensions.Size = new System.Drawing.Size(1005, 293);
            this.xtraTabPageDimensions.Text = "Dimensions";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.poleDimensionsDtlsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1005, 293);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // poleDimensionsDtlsBindingSource
            // 
            this.poleDimensionsDtlsBindingSource.DataSource = typeof(Project.DAL.PoleDimensionsDtl);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleDimensionsDtlId,
            this.colPolesId1,
            this.colLength1,
            this.colH61,
            this.colH51,
            this.colH41,
            this.colH31,
            this.colH21,
            this.colH11,
            this.colC11,
            this.colC21,
            this.colC31,
            this.colC41,
            this.colC51,
            this.colC61,
            this.colC71,
            this.colC81,
            this.colC91,
            this.colC101});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colPoleDimensionsDtlId
            // 
            this.colPoleDimensionsDtlId.FieldName = "PoleDimensionsDtlId";
            this.colPoleDimensionsDtlId.Name = "colPoleDimensionsDtlId";
            // 
            // colPolesId1
            // 
            this.colPolesId1.FieldName = "PolesId";
            this.colPolesId1.Name = "colPolesId1";
            // 
            // colLength1
            // 
            this.colLength1.FieldName = "Length";
            this.colLength1.Name = "colLength1";
            this.colLength1.Visible = true;
            this.colLength1.VisibleIndex = 0;
            // 
            // colH61
            // 
            this.colH61.FieldName = "H6";
            this.colH61.Name = "colH61";
            this.colH61.Visible = true;
            this.colH61.VisibleIndex = 1;
            // 
            // colH51
            // 
            this.colH51.FieldName = "H5";
            this.colH51.Name = "colH51";
            this.colH51.Visible = true;
            this.colH51.VisibleIndex = 2;
            // 
            // colH41
            // 
            this.colH41.FieldName = "H4";
            this.colH41.Name = "colH41";
            this.colH41.Visible = true;
            this.colH41.VisibleIndex = 3;
            // 
            // colH31
            // 
            this.colH31.FieldName = "H3";
            this.colH31.Name = "colH31";
            this.colH31.Visible = true;
            this.colH31.VisibleIndex = 4;
            // 
            // colH21
            // 
            this.colH21.FieldName = "H2";
            this.colH21.Name = "colH21";
            this.colH21.Visible = true;
            this.colH21.VisibleIndex = 5;
            // 
            // colH11
            // 
            this.colH11.FieldName = "H1";
            this.colH11.Name = "colH11";
            this.colH11.Visible = true;
            this.colH11.VisibleIndex = 6;
            // 
            // colC11
            // 
            this.colC11.FieldName = "C1";
            this.colC11.Name = "colC11";
            this.colC11.Visible = true;
            this.colC11.VisibleIndex = 7;
            // 
            // colC21
            // 
            this.colC21.FieldName = "C2";
            this.colC21.Name = "colC21";
            this.colC21.Visible = true;
            this.colC21.VisibleIndex = 8;
            // 
            // colC31
            // 
            this.colC31.FieldName = "C3";
            this.colC31.Name = "colC31";
            this.colC31.Visible = true;
            this.colC31.VisibleIndex = 9;
            // 
            // colC41
            // 
            this.colC41.FieldName = "C4";
            this.colC41.Name = "colC41";
            this.colC41.Visible = true;
            this.colC41.VisibleIndex = 10;
            // 
            // colC51
            // 
            this.colC51.FieldName = "C5";
            this.colC51.Name = "colC51";
            this.colC51.Visible = true;
            this.colC51.VisibleIndex = 11;
            // 
            // colC61
            // 
            this.colC61.FieldName = "C6";
            this.colC61.Name = "colC61";
            this.colC61.Visible = true;
            this.colC61.VisibleIndex = 12;
            // 
            // colC71
            // 
            this.colC71.FieldName = "C7";
            this.colC71.Name = "colC71";
            this.colC71.Visible = true;
            this.colC71.VisibleIndex = 13;
            // 
            // colC81
            // 
            this.colC81.FieldName = "C8";
            this.colC81.Name = "colC81";
            this.colC81.Visible = true;
            this.colC81.VisibleIndex = 14;
            // 
            // colC91
            // 
            this.colC91.FieldName = "C9";
            this.colC91.Name = "colC91";
            this.colC91.Visible = true;
            this.colC91.VisibleIndex = 15;
            // 
            // colC101
            // 
            this.colC101.FieldName = "C10";
            this.colC101.Name = "colC101";
            this.colC101.Visible = true;
            this.colC101.VisibleIndex = 16;
            // 
            // PoleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 541);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PoleForm";
            this.Text = "Price Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PoleForm_FormClosing);
            this.Load += new System.EventHandler(this.PoleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb10Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb10Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb9Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb9Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb8Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb8Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb7Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb7Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb6Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb6Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb5Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb5Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb4Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb4Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb3Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb3Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb2Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb2Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb1Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb1Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH1Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH1Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH2Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH2Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH3Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH3Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH4Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH4Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH5Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH5Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH6Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbH6Min.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPricesPer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditSpecies.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPagePrices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polesDtlsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPageDimensions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poleDimensionsDtlsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditSpecies;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.BindingSource tblBindingSource;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePrices;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource polesDtlsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolesDtlId;
        private DevExpress.XtraGrid.Columns.GridColumn colPolesId;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colH6;
        private DevExpress.XtraGrid.Columns.GridColumn colH5;
        private DevExpress.XtraGrid.Columns.GridColumn colH4;
        private DevExpress.XtraGrid.Columns.GridColumn colH3;
        private DevExpress.XtraGrid.Columns.GridColumn colH2;
        private DevExpress.XtraGrid.Columns.GridColumn colH1;
        private DevExpress.XtraGrid.Columns.GridColumn colC1;
        private DevExpress.XtraGrid.Columns.GridColumn colC2;
        private DevExpress.XtraGrid.Columns.GridColumn colC3;
        private DevExpress.XtraGrid.Columns.GridColumn colC4;
        private DevExpress.XtraGrid.Columns.GridColumn colC5;
        private DevExpress.XtraGrid.Columns.GridColumn colC6;
        private DevExpress.XtraGrid.Columns.GridColumn colC7;
        private DevExpress.XtraGrid.Columns.GridColumn colC8;
        private DevExpress.XtraGrid.Columns.GridColumn colC9;
        private DevExpress.XtraGrid.Columns.GridColumn colC10;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDimensions;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit tb10Max;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit tb10Min;
        private DevExpress.XtraEditors.TextEdit tb9Max;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit tb9Min;
        private DevExpress.XtraEditors.TextEdit tb8Max;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit tb8Min;
        private DevExpress.XtraEditors.TextEdit tb7Max;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit tb7Min;
        private DevExpress.XtraEditors.TextEdit tb6Max;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit tb6Min;
        private DevExpress.XtraEditors.TextEdit tb5Max;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit tb5Min;
        private DevExpress.XtraEditors.TextEdit tb4Max;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit tb4Min;
        private DevExpress.XtraEditors.TextEdit tb3Max;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit tb3Min;
        private DevExpress.XtraEditors.TextEdit tb2Max;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit tb2Min;
        private DevExpress.XtraEditors.TextEdit tb1Max;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit tb1Min;
        private DevExpress.XtraEditors.TextEdit tbH1Max;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit tbH1Min;
        private DevExpress.XtraEditors.TextEdit tbH2Max;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit tbH2Min;
        private DevExpress.XtraEditors.TextEdit tbH3Max;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit tbH3Min;
        private DevExpress.XtraEditors.TextEdit tbH4Max;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit tbH4Min;
        private DevExpress.XtraEditors.TextEdit tbH5Max;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit tbH5Min;
        private DevExpress.XtraEditors.TextEdit tbH6Max;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit tbH6Min;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPricesPer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.BindingSource poleDimensionsDtlsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleDimensionsDtlId;
        private DevExpress.XtraGrid.Columns.GridColumn colPolesId1;
        private DevExpress.XtraGrid.Columns.GridColumn colLength1;
        private DevExpress.XtraGrid.Columns.GridColumn colH61;
        private DevExpress.XtraGrid.Columns.GridColumn colH51;
        private DevExpress.XtraGrid.Columns.GridColumn colH41;
        private DevExpress.XtraGrid.Columns.GridColumn colH31;
        private DevExpress.XtraGrid.Columns.GridColumn colH21;
        private DevExpress.XtraGrid.Columns.GridColumn colH11;
        private DevExpress.XtraGrid.Columns.GridColumn colC11;
        private DevExpress.XtraGrid.Columns.GridColumn colC21;
        private DevExpress.XtraGrid.Columns.GridColumn colC31;
        private DevExpress.XtraGrid.Columns.GridColumn colC41;
        private DevExpress.XtraGrid.Columns.GridColumn colC51;
        private DevExpress.XtraGrid.Columns.GridColumn colC61;
        private DevExpress.XtraGrid.Columns.GridColumn colC71;
        private DevExpress.XtraGrid.Columns.GridColumn colC81;
        private DevExpress.XtraGrid.Columns.GridColumn colC91;
        private DevExpress.XtraGrid.Columns.GridColumn colC101;
    }
}