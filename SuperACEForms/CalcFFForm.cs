﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using SuperACEUtils;

namespace SuperACEForms
{
    public partial class CalcFFForm : DevExpress.XtraEditors.XtraForm
    {
        public bool mUpdate = false;
        public string mFF = string.Empty;

        public CalcFFForm()
        {
            InitializeComponent();
        }

        private void CalcFFForm_Load(object sender, EventArgs e)
        {

        }

        private void tbDbh_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbDbh.Text))
            {
                dxErrorProvider1.SetError(tbDbh, "Invalid Dbh");
                tbDbh.Focus();
            }
            else
                dxErrorProvider1.SetError(tbDbh, "");
        }

        private void tbFP_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbFP.Text))
            {
                dxErrorProvider1.SetError(tbFP, "Invalid FP");
                tbFP.Focus();
            }
            else
                dxErrorProvider1.SetError(tbFP, "");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Calc();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            mUpdate = true;
            Calc();
            mFF = tbFF.Text;
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            mUpdate = false;
            mFF = string.Empty;
            this.Close();
        }

        private void Calc()
        {
            tbFF.Text = string.Format("{0:0}", (Utils.ConvertToFloat(tbFP.Text) / Utils.ConvertToFloat(tbDbh.Text)) * 100);
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }
    }
}