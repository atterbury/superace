﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace SuperACE
{
    public partial class PopupGradeForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext ctx = null;
        private string projectTable = null;
        private string species = null;
        public string code = string.Empty;

        public PopupGradeForm(ref ProjectDbContext pCtx, string pProjectTable, string pSpecies)
        {
            InitializeComponent();

            ctx = pCtx;
            projectTable = pProjectTable;
            species = pSpecies;
        }

        private void PopupGradeForm_Load(object sender, EventArgs e)
        {
            List<Grade> coll =
                ctx.Grades.OrderBy(s => s.PrimarySpecies).ThenBy(s => s.InputCode).Where(s => s.TableName == projectTable && (s.PrimarySpecies == species || s.PrimarySpecies == "*")).ToList();
            bindingSource.DataSource = coll;
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    Grade rec = bindingSource.Current as Grade;
                    code = rec.InputCode;
                    this.Close();
                    break;
                case Keys.Escape:
                    code = string.Empty;
                    this.Close();
                    break;
            }
        }
    }
}