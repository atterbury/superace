﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace SuperACEForms
{
    public partial class CopyTableForm : DevExpress.XtraEditors.XtraForm
    {
        public bool bProcess = false;
        public string tableName = string.Empty;
        private ProjectDbContext projectContext;
        private string TableType = string.Empty;

        public CopyTableForm(ref ProjectDbContext pContext, string tableType)
        {
            InitializeComponent();

            projectContext = pContext;
            TableType = tableType;
        }

        private void CopyTableForm_Load(object sender, EventArgs e)
        {

        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTableName.Text))
            {
                XtraMessageBox.Show("Must enter a Table Name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            switch (TableType)
            {
                case "Species":
                    if (projectContext.Species.Any(t => t.TableName == tbTableName.Text))
                    {
                        XtraMessageBox.Show("Table Name already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case "Sort":
                    if (projectContext.Sorts.Any(t => t.TableName == tbTableName.Text))
                    {
                        XtraMessageBox.Show("Table Name already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case "Grade":
                    if (projectContext.Grades.Any(t => t.TableName == tbTableName.Text))
                    {
                        XtraMessageBox.Show("Table Name already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case "Price":
                    if (projectContext.Prices.Any(t => t.TableName == tbTableName.Text))
                    {
                        XtraMessageBox.Show("Table Name already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case "Cost":
                    if (projectContext.Costs.Any(t => t.TableName == tbTableName.Text))
                    {
                        XtraMessageBox.Show("Table Name already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
            }

            tableName = tbTableName.Text;
            bProcess = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            tableName = string.Empty;
            bProcess = false;
            this.Close();
        }
    }
}