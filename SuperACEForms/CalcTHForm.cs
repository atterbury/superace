﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SuperACEUtils;

namespace SuperACEForms
{
    public partial class CalcTHForm : DevExpress.XtraEditors.XtraForm
    {
        public bool mUpdate = false;
        public string mHeight = string.Empty;

        public CalcTHForm()
        {
            InitializeComponent();
        }

        private void CalcTHForm_Load(object sender, EventArgs e)
        {

        }

        private void tbDistance_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidInt(tbDistance.Text))
            {
                dxErrorProvider1.SetError(tbDistance, "Invalid Distance");
                tbDistance.Focus();
            }
            else
                dxErrorProvider1.SetError(tbDistance, "");
        }

        private void tbBottom_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidInt(tbBottom.Text))
            {
                dxErrorProvider1.SetError(tbBottom, "Invalid Bottom");
                tbBottom.Focus();
            }
            else
                dxErrorProvider1.SetError(tbBottom, "");
        }

        private void tbTop_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidInt(tbTop.Text))
            {
                dxErrorProvider1.SetError(tbTop, "Invalid Top");
                tbTop.Focus();
            }
            else
                dxErrorProvider1.SetError(tbTop, "");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            // Calc
            Calc();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            // Update
            mUpdate = true;
            mHeight = tbHeight.Text;
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            // CAncel
            mUpdate = false;
            this.Close();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void Calc()
        {
            if (Utils.ConvertToFloat(tbDistance.Text) > 0 && Utils.ConvertToFloat(tbBottom.Text) > 0 && Utils.ConvertToFloat(tbTop.Text) != 0)
                tbHeight.Text = string.Format("{0:0}", ((Utils.ConvertToFloat(tbBottom.Text) + (-Utils.ConvertToFloat(tbTop.Text))) * Utils.ConvertToFloat(tbDistance.Text)) / 100);
        }
    }
}