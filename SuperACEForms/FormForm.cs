﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;

namespace SuperACEForms
{
    public partial class FormForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        //private string dataSource = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public FormForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            //dataSource = pDataSource;
            tableName = pTableName;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            comboBoxEditTableName.Text = tableName;

            LoadTableNames();
            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (tableBindingSource.Count > 0)
                tableBindingSource.Clear();
            tableBindingSource.DataSource = ctx.Formfactors.OrderBy(t => t.DisplayCode).Where(t => t.TableName == comboBoxEditTableName.Text).ToList();
            barStaticItem6.Caption = string.Format("{0} Records Loaded", tableBindingSource.Count);
        }

        private void LoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetFormTableNames(ref ctx);
            foreach (var tblItem in tbls)
                comboBoxEditTableName.Properties.Items.Add(tblItem);
            comboBoxEditTableName.Text = tableName;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            tableBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditTableName.Text == currentProject.FormTableName)
            {
                XtraMessageBox.Show("Can't delete the Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditTableName.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault(); ;
            currentProject.FormTableName = comboBoxEditTableName.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Add
            AddNewRecord();
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete
            DeleteRecord();
        }

        private void AddNewRecord()
        {
            Formfactor rec = new Formfactor();
            rec.TableName = tableName;
            ctx.Formfactors.Add(rec);
            ctx.SaveChanges();
            LoadData();
            gridView1.FocusedColumn = gridView1.Columns["DisplayCode"];
            gridControl1.Focus();
        }

        private void DeleteRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            Formfactor rec = tableBindingSource.Current as Formfactor;
            ctx.Formfactors.Remove(rec);
            ctx.SaveChanges();
            LoadData();
        }
    }
}