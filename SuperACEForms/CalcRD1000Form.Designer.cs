﻿namespace SuperACEForms
{
    partial class CalcRD1000Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbCrowBase = new DevExpress.XtraEditors.TextEdit();
            this.tbTotalHeight = new DevExpress.XtraEditors.TextEdit();
            this.tbTDFHeight = new DevExpress.XtraEditors.TextEdit();
            this.tbFPHeight = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbDbhHeight = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.tbTDFInches = new DevExpress.XtraEditors.TextEdit();
            this.tbFPInches = new DevExpress.XtraEditors.TextEdit();
            this.tbDbhInches = new DevExpress.XtraEditors.TextEdit();
            this.tbTDFTDF = new DevExpress.XtraEditors.TextEdit();
            this.tbFPTDF = new DevExpress.XtraEditors.TextEdit();
            this.tbDbhTDF = new DevExpress.XtraEditors.TextEdit();
            this.tbCrownRatioTDF = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.teStumpHeight = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbCrowBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTotalHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFInches.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPInches.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhInches.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFTDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPTDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhTDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCrownRatioTDF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teStumpHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(284, 353);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(87, 28);
            this.simpleButton3.TabIndex = 11;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(174, 353);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(87, 28);
            this.simpleButton2.TabIndex = 10;
            this.simpleButton2.Text = "Update";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(65, 353);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(87, 28);
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "Calc";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(25, 194);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(73, 16);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "Crown Base:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(24, 164);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(65, 16);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "Total Ht Ft:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(67, 134);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 16);
            this.labelControl3.TabIndex = 22;
            this.labelControl3.Text = "TDF:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 103);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 16);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "Form Point:";
            // 
            // tbCrowBase
            // 
            this.tbCrowBase.EnterMoveNextControl = true;
            this.tbCrowBase.Location = new System.Drawing.Point(131, 190);
            this.tbCrowBase.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbCrowBase.Name = "tbCrowBase";
            this.tbCrowBase.Size = new System.Drawing.Size(62, 22);
            this.tbCrowBase.TabIndex = 8;
            this.tbCrowBase.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbCrowBase.Validated += new System.EventHandler(this.tbCrowBase_Validated);
            // 
            // tbTotalHeight
            // 
            this.tbTotalHeight.EnterMoveNextControl = true;
            this.tbTotalHeight.Location = new System.Drawing.Point(131, 160);
            this.tbTotalHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTotalHeight.Name = "tbTotalHeight";
            this.tbTotalHeight.Size = new System.Drawing.Size(62, 22);
            this.tbTotalHeight.TabIndex = 7;
            this.tbTotalHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTotalHeight.Validated += new System.EventHandler(this.tbTotalHeight_Validated);
            // 
            // tbTDFHeight
            // 
            this.tbTDFHeight.EnterMoveNextControl = true;
            this.tbTDFHeight.Location = new System.Drawing.Point(130, 130);
            this.tbTDFHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTDFHeight.Name = "tbTDFHeight";
            this.tbTDFHeight.Size = new System.Drawing.Size(62, 22);
            this.tbTDFHeight.TabIndex = 5;
            this.tbTDFHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTDFHeight.Validated += new System.EventHandler(this.tbTDFHeight_Validated);
            // 
            // tbFPHeight
            // 
            this.tbFPHeight.EnterMoveNextControl = true;
            this.tbFPHeight.Location = new System.Drawing.Point(130, 100);
            this.tbFPHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbFPHeight.Name = "tbFPHeight";
            this.tbFPHeight.Size = new System.Drawing.Size(62, 22);
            this.tbFPHeight.TabIndex = 3;
            this.tbFPHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbFPHeight.Validated += new System.EventHandler(this.tbFPHeight_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(29, 73);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 16);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Dbh Inches:";
            // 
            // tbDbhHeight
            // 
            this.tbDbhHeight.EditValue = "4";
            this.tbDbhHeight.EnterMoveNextControl = true;
            this.tbDbhHeight.Location = new System.Drawing.Point(130, 70);
            this.tbDbhHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDbhHeight.Name = "tbDbhHeight";
            this.tbDbhHeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbDbhHeight.Properties.Appearance.Options.UseFont = true;
            this.tbDbhHeight.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbDbhHeight.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.tbDbhHeight.Properties.ReadOnly = true;
            this.tbDbhHeight.Size = new System.Drawing.Size(62, 22);
            this.tbDbhHeight.TabIndex = 18;
            this.tbDbhHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbDbhHeight.Validated += new System.EventHandler(this.tbDbhHeight_Validated);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(23, 229);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 16);
            this.labelControl6.TabIndex = 26;
            this.labelControl6.Text = "Crown Ratio:";
            // 
            // tbTDFInches
            // 
            this.tbTDFInches.EnterMoveNextControl = true;
            this.tbTDFInches.Location = new System.Drawing.Point(199, 130);
            this.tbTDFInches.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTDFInches.Name = "tbTDFInches";
            this.tbTDFInches.Size = new System.Drawing.Size(62, 22);
            this.tbTDFInches.TabIndex = 6;
            this.tbTDFInches.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTDFInches.Validated += new System.EventHandler(this.textEdit1_Validated);
            // 
            // tbFPInches
            // 
            this.tbFPInches.EnterMoveNextControl = true;
            this.tbFPInches.Location = new System.Drawing.Point(199, 100);
            this.tbFPInches.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbFPInches.Name = "tbFPInches";
            this.tbFPInches.Size = new System.Drawing.Size(62, 22);
            this.tbFPInches.TabIndex = 4;
            this.tbFPInches.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbFPInches.Validated += new System.EventHandler(this.tbFPInches_Validated);
            // 
            // tbDbhInches
            // 
            this.tbDbhInches.EnterMoveNextControl = true;
            this.tbDbhInches.Location = new System.Drawing.Point(199, 70);
            this.tbDbhInches.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDbhInches.Name = "tbDbhInches";
            this.tbDbhInches.Size = new System.Drawing.Size(62, 22);
            this.tbDbhInches.TabIndex = 2;
            this.tbDbhInches.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbDbhInches.Validated += new System.EventHandler(this.tbDbhInches_Validated);
            // 
            // tbTDFTDF
            // 
            this.tbTDFTDF.EnterMoveNextControl = true;
            this.tbTDFTDF.Location = new System.Drawing.Point(268, 130);
            this.tbTDFTDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTDFTDF.Name = "tbTDFTDF";
            this.tbTDFTDF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbTDFTDF.Properties.Appearance.Options.UseFont = true;
            this.tbTDFTDF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbTDFTDF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.tbTDFTDF.Properties.ReadOnly = true;
            this.tbTDFTDF.Size = new System.Drawing.Size(62, 22);
            this.tbTDFTDF.TabIndex = 23;
            this.tbTDFTDF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbFPTDF
            // 
            this.tbFPTDF.EnterMoveNextControl = true;
            this.tbFPTDF.Location = new System.Drawing.Point(268, 100);
            this.tbFPTDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbFPTDF.Name = "tbFPTDF";
            this.tbFPTDF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbFPTDF.Properties.Appearance.Options.UseFont = true;
            this.tbFPTDF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbFPTDF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.tbFPTDF.Properties.ReadOnly = true;
            this.tbFPTDF.Size = new System.Drawing.Size(62, 22);
            this.tbFPTDF.TabIndex = 21;
            this.tbFPTDF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbDbhTDF
            // 
            this.tbDbhTDF.EnterMoveNextControl = true;
            this.tbDbhTDF.Location = new System.Drawing.Point(268, 70);
            this.tbDbhTDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDbhTDF.Name = "tbDbhTDF";
            this.tbDbhTDF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbDbhTDF.Properties.Appearance.Options.UseFont = true;
            this.tbDbhTDF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbDbhTDF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.tbDbhTDF.Properties.ReadOnly = true;
            this.tbDbhTDF.Size = new System.Drawing.Size(62, 22);
            this.tbDbhTDF.TabIndex = 19;
            this.tbDbhTDF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbCrownRatioTDF
            // 
            this.tbCrownRatioTDF.EnterMoveNextControl = true;
            this.tbCrownRatioTDF.Location = new System.Drawing.Point(268, 225);
            this.tbCrownRatioTDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbCrownRatioTDF.Name = "tbCrownRatioTDF";
            this.tbCrownRatioTDF.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbCrownRatioTDF.Properties.Appearance.Options.UseFont = true;
            this.tbCrownRatioTDF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.tbCrownRatioTDF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.tbCrownRatioTDF.Properties.ReadOnly = true;
            this.tbCrownRatioTDF.Size = new System.Drawing.Size(62, 22);
            this.tbCrownRatioTDF.TabIndex = 27;
            this.tbCrownRatioTDF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl7.Location = new System.Drawing.Point(130, 30);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 32);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Height Feet";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl8.Location = new System.Drawing.Point(212, 30);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(62, 32);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Diameter Inches";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl9.Location = new System.Drawing.Point(290, 30);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(52, 32);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Input Cruise";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(12, 23);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(82, 16);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "Stump Height:";
            // 
            // teStumpHeight
            // 
            this.teStumpHeight.EnterMoveNextControl = true;
            this.teStumpHeight.Location = new System.Drawing.Point(113, 20);
            this.teStumpHeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.teStumpHeight.Name = "teStumpHeight";
            this.teStumpHeight.Size = new System.Drawing.Size(62, 22);
            this.teStumpHeight.TabIndex = 1;
            this.teStumpHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.teStumpHeight.Validated += new System.EventHandler(this.teStumpHeight_Validated);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(185, 23);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(25, 16);
            this.labelControl11.TabIndex = 13;
            this.labelControl11.Text = "Feet";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.tbDbhTDF);
            this.groupControl1.Controls.Add(this.tbCrownRatioTDF);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.tbDbhHeight);
            this.groupControl1.Controls.Add(this.tbTDFTDF);
            this.groupControl1.Controls.Add(this.tbTDFInches);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.tbFPTDF);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.tbDbhInches);
            this.groupControl1.Controls.Add(this.tbCrowBase);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.tbFPInches);
            this.groupControl1.Controls.Add(this.tbFPHeight);
            this.groupControl1.Controls.Add(this.tbTotalHeight);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.tbTDFHeight);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Location = new System.Drawing.Point(11, 59);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(360, 273);
            this.groupControl1.TabIndex = 30;
            this.groupControl1.Text = "RD 1000 Input Data";
            // 
            // CalcRD1000Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 403);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.teStumpHeight);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcRD1000Form";
            this.Text = "Calc RD1000";
            this.Load += new System.EventHandler(this.CalcRD1000Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbCrowBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTotalHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFInches.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPInches.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhInches.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTDFTDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFPTDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDbhTDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCrownRatioTDF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teStumpHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbCrowBase;
        private DevExpress.XtraEditors.TextEdit tbTotalHeight;
        private DevExpress.XtraEditors.TextEdit tbTDFHeight;
        private DevExpress.XtraEditors.TextEdit tbFPHeight;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbDbhHeight;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit tbTDFInches;
        private DevExpress.XtraEditors.TextEdit tbFPInches;
        private DevExpress.XtraEditors.TextEdit tbDbhInches;
        private DevExpress.XtraEditors.TextEdit tbTDFTDF;
        private DevExpress.XtraEditors.TextEdit tbFPTDF;
        private DevExpress.XtraEditors.TextEdit tbDbhTDF;
        private DevExpress.XtraEditors.TextEdit tbCrownRatioTDF;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit teStumpHeight;
        private DevExpress.XtraEditors.LabelControl labelControl10;
    }
}