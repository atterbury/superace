﻿namespace SuperACEForms
{
    partial class GradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEditTableName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gradesBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGradesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbbreviation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInputCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarketingCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumBDFTPerLog = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumRingsPerInch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentSurfaceClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumKnotSizePerInch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKnotsPerFoot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSlopeOfGrain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductRecoveryMinimum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightInPounds = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolumeUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumButtDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barLargeButtonItem1,
            this.barButtonItem10,
            this.barButtonItem11});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Save";
            this.barButtonItem6.Id = 15;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save To New Table";
            this.barButtonItem7.Id = 16;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Delete";
            this.barButtonItem8.Id = 17;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Set as Default";
            this.barButtonItem9.Id = 18;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // bar1
            // 
            this.bar1.BarName = "Toolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Toolbar";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Add";
            this.barButtonItem10.Id = 30;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Delete";
            this.barButtonItem11.Id = 31;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 465);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 414);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 51);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 414);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboBoxEditTableName);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 51);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 55);
            this.panelControl1.TabIndex = 4;
            // 
            // comboBoxEditTableName
            // 
            this.comboBoxEditTableName.Location = new System.Drawing.Point(93, 19);
            this.comboBoxEditTableName.MenuManager = this.barManager1;
            this.comboBoxEditTableName.Name = "comboBoxEditTableName";
            this.comboBoxEditTableName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTableName.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditTableName.TabIndex = 1;
            this.comboBoxEditTableName.SelectedValueChanged += new System.EventHandler(this.comboBoxEditTableName_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Table Name:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.gradesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 106);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1011, 359);
            this.gridControl1.TabIndex = 10;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gradesBindingSource
            // 
            this.gradesBindingSource.DataSource = typeof(Project.DAL.Grade);
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 60;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGradesID,
            this.colTableName,
            this.colAbbreviation,
            this.colInputCode,
            this.colDescription,
            this.colMarketingCategory,
            this.colMinimumDiameter,
            this.colMinimumLength,
            this.colMaximumDiameter,
            this.colMinimumBDFTPerLog,
            this.colMinimumRingsPerInch,
            this.colPercentSurfaceClear,
            this.colMaximumKnotSizePerInch,
            this.colKnotsPerFoot,
            this.colSlopeOfGrain,
            this.colProductRecoveryMinimum,
            this.colWeightInPounds,
            this.colVolumeUnits,
            this.colSpecies,
            this.colPrimarySpecies,
            this.colMaximumButtDiameter});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colGradesID
            // 
            this.colGradesID.AppearanceHeader.Options.UseTextOptions = true;
            this.colGradesID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGradesID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGradesID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGradesID.FieldName = "GradesID";
            this.colGradesID.Name = "colGradesID";
            // 
            // colTableName
            // 
            this.colTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colAbbreviation
            // 
            this.colAbbreviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colAbbreviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAbbreviation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAbbreviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAbbreviation.Caption = "Abrv";
            this.colAbbreviation.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAbbreviation.FieldName = "Abbreviation";
            this.colAbbreviation.Name = "colAbbreviation";
            this.colAbbreviation.Visible = true;
            this.colAbbreviation.VisibleIndex = 2;
            this.colAbbreviation.Width = 35;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colInputCode
            // 
            this.colInputCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colInputCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputCode.Caption = "Code";
            this.colInputCode.ColumnEdit = this.repositoryItemTextEdit1;
            this.colInputCode.FieldName = "InputCode";
            this.colInputCode.Name = "colInputCode";
            this.colInputCode.Visible = true;
            this.colInputCode.VisibleIndex = 1;
            this.colInputCode.Width = 45;
            // 
            // colDescription
            // 
            this.colDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescription.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescription.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 93;
            // 
            // colMarketingCategory
            // 
            this.colMarketingCategory.AppearanceHeader.Options.UseTextOptions = true;
            this.colMarketingCategory.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMarketingCategory.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMarketingCategory.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMarketingCategory.FieldName = "MarketingCategory";
            this.colMarketingCategory.Name = "colMarketingCategory";
            // 
            // colMinimumDiameter
            // 
            this.colMinimumDiameter.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinimumDiameter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinimumDiameter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinimumDiameter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinimumDiameter.Caption = "Min Dia In.";
            this.colMinimumDiameter.DisplayFormat.FormatString = "{0:#}";
            this.colMinimumDiameter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinimumDiameter.FieldName = "MinimumDiameter";
            this.colMinimumDiameter.Name = "colMinimumDiameter";
            this.colMinimumDiameter.Visible = true;
            this.colMinimumDiameter.VisibleIndex = 4;
            this.colMinimumDiameter.Width = 40;
            // 
            // colMinimumLength
            // 
            this.colMinimumLength.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinimumLength.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinimumLength.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinimumLength.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinimumLength.Caption = "Min Len Ft.";
            this.colMinimumLength.DisplayFormat.FormatString = "{0:#}";
            this.colMinimumLength.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinimumLength.FieldName = "MinimumLength";
            this.colMinimumLength.Name = "colMinimumLength";
            this.colMinimumLength.Visible = true;
            this.colMinimumLength.VisibleIndex = 5;
            this.colMinimumLength.Width = 41;
            // 
            // colMaximumDiameter
            // 
            this.colMaximumDiameter.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaximumDiameter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaximumDiameter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaximumDiameter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaximumDiameter.Caption = "Max Butt Dia";
            this.colMaximumDiameter.DisplayFormat.FormatString = "{0:#}";
            this.colMaximumDiameter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaximumDiameter.FieldName = "MaximumDiameter";
            this.colMaximumDiameter.Name = "colMaximumDiameter";
            this.colMaximumDiameter.Width = 37;
            // 
            // colMinimumBDFTPerLog
            // 
            this.colMinimumBDFTPerLog.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinimumBDFTPerLog.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinimumBDFTPerLog.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinimumBDFTPerLog.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinimumBDFTPerLog.Caption = "Min Net BdFt per Log";
            this.colMinimumBDFTPerLog.DisplayFormat.FormatString = "{0:#}";
            this.colMinimumBDFTPerLog.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinimumBDFTPerLog.FieldName = "MinimumBdftPerLog";
            this.colMinimumBDFTPerLog.Name = "colMinimumBDFTPerLog";
            this.colMinimumBDFTPerLog.Visible = true;
            this.colMinimumBDFTPerLog.VisibleIndex = 7;
            this.colMinimumBDFTPerLog.Width = 64;
            // 
            // colMinimumRingsPerInch
            // 
            this.colMinimumRingsPerInch.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinimumRingsPerInch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinimumRingsPerInch.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinimumRingsPerInch.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinimumRingsPerInch.Caption = "Min Rings per Inch";
            this.colMinimumRingsPerInch.DisplayFormat.FormatString = "{0:#}";
            this.colMinimumRingsPerInch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinimumRingsPerInch.FieldName = "MinimumRingsPerInch";
            this.colMinimumRingsPerInch.Name = "colMinimumRingsPerInch";
            this.colMinimumRingsPerInch.Visible = true;
            this.colMinimumRingsPerInch.VisibleIndex = 8;
            // 
            // colPercentSurfaceClear
            // 
            this.colPercentSurfaceClear.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercentSurfaceClear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercentSurfaceClear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercentSurfaceClear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercentSurfaceClear.Caption = "% Surface Clear";
            this.colPercentSurfaceClear.DisplayFormat.FormatString = "{0:#}";
            this.colPercentSurfaceClear.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercentSurfaceClear.FieldName = "PercentSurfaceClear";
            this.colPercentSurfaceClear.Name = "colPercentSurfaceClear";
            this.colPercentSurfaceClear.Visible = true;
            this.colPercentSurfaceClear.VisibleIndex = 9;
            this.colPercentSurfaceClear.Width = 66;
            // 
            // colMaximumKnotSizePerInch
            // 
            this.colMaximumKnotSizePerInch.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaximumKnotSizePerInch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaximumKnotSizePerInch.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaximumKnotSizePerInch.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaximumKnotSizePerInch.Caption = "Max Knot Size per In.";
            this.colMaximumKnotSizePerInch.DisplayFormat.FormatString = "{0:#.#}";
            this.colMaximumKnotSizePerInch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaximumKnotSizePerInch.FieldName = "MaximumKnotSizePerInch";
            this.colMaximumKnotSizePerInch.Name = "colMaximumKnotSizePerInch";
            this.colMaximumKnotSizePerInch.Visible = true;
            this.colMaximumKnotSizePerInch.VisibleIndex = 10;
            // 
            // colKnotsPerFoot
            // 
            this.colKnotsPerFoot.AppearanceHeader.Options.UseTextOptions = true;
            this.colKnotsPerFoot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKnotsPerFoot.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colKnotsPerFoot.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKnotsPerFoot.Caption = "Knots per Foot";
            this.colKnotsPerFoot.DisplayFormat.FormatString = "{0:#}";
            this.colKnotsPerFoot.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKnotsPerFoot.FieldName = "KnotsPerFoot";
            this.colKnotsPerFoot.Name = "colKnotsPerFoot";
            this.colKnotsPerFoot.Visible = true;
            this.colKnotsPerFoot.VisibleIndex = 11;
            this.colKnotsPerFoot.Width = 61;
            // 
            // colSlopeOfGrain
            // 
            this.colSlopeOfGrain.AppearanceHeader.Options.UseTextOptions = true;
            this.colSlopeOfGrain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSlopeOfGrain.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSlopeOfGrain.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSlopeOfGrain.Caption = "Slope of Grain In./Ft.";
            this.colSlopeOfGrain.DisplayFormat.FormatString = "{0:#.#}";
            this.colSlopeOfGrain.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSlopeOfGrain.FieldName = "SlopeOfGrain";
            this.colSlopeOfGrain.Name = "colSlopeOfGrain";
            this.colSlopeOfGrain.Visible = true;
            this.colSlopeOfGrain.VisibleIndex = 12;
            this.colSlopeOfGrain.Width = 62;
            // 
            // colProductRecoveryMinimum
            // 
            this.colProductRecoveryMinimum.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductRecoveryMinimum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductRecoveryMinimum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colProductRecoveryMinimum.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductRecoveryMinimum.Caption = "Product Recovery Min";
            this.colProductRecoveryMinimum.FieldName = "ProductRecoveryMinimum";
            this.colProductRecoveryMinimum.Name = "colProductRecoveryMinimum";
            this.colProductRecoveryMinimum.Visible = true;
            this.colProductRecoveryMinimum.VisibleIndex = 13;
            // 
            // colWeightInPounds
            // 
            this.colWeightInPounds.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeightInPounds.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightInPounds.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colWeightInPounds.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeightInPounds.Caption = "Lbs Per";
            this.colWeightInPounds.DisplayFormat.FormatString = "{0:#}";
            this.colWeightInPounds.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWeightInPounds.FieldName = "WeightInPounds";
            this.colWeightInPounds.Name = "colWeightInPounds";
            this.colWeightInPounds.Visible = true;
            this.colWeightInPounds.VisibleIndex = 14;
            this.colWeightInPounds.Width = 66;
            // 
            // colVolumeUnits
            // 
            this.colVolumeUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colVolumeUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVolumeUnits.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colVolumeUnits.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVolumeUnits.Caption = "CCF or MBF";
            this.colVolumeUnits.ColumnEdit = this.repositoryItemComboBox3;
            this.colVolumeUnits.FieldName = "VolumeUnits";
            this.colVolumeUnits.Name = "colVolumeUnits";
            this.colVolumeUnits.Visible = true;
            this.colVolumeUnits.VisibleIndex = 15;
            this.colVolumeUnits.Width = 55;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "CCF",
            "MBF"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // colSpecies
            // 
            this.colSpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Width = 58;
            // 
            // colPrimarySpecies
            // 
            this.colPrimarySpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrimarySpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrimarySpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPrimarySpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPrimarySpecies.Caption = "Species";
            this.colPrimarySpecies.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPrimarySpecies.FieldName = "PrimarySpecies";
            this.colPrimarySpecies.Name = "colPrimarySpecies";
            this.colPrimarySpecies.Visible = true;
            this.colPrimarySpecies.VisibleIndex = 0;
            this.colPrimarySpecies.Width = 58;
            // 
            // colMaximumButtDiameter
            // 
            this.colMaximumButtDiameter.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaximumButtDiameter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaximumButtDiameter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaximumButtDiameter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaximumButtDiameter.Caption = "Max Butt Dia";
            this.colMaximumButtDiameter.FieldName = "MaximumButtDiameter";
            this.colMaximumButtDiameter.Name = "colMaximumButtDiameter";
            this.colMaximumButtDiameter.Visible = true;
            this.colMaximumButtDiameter.VisibleIndex = 6;
            this.colMaximumButtDiameter.Width = 41;
            // 
            // GradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 492);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "GradeForm";
            this.Text = "Grade Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GradeForm_FormClosing);
            this.Load += new System.EventHandler(this.GradeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTableName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource gradesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colGradesID;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colAbbreviation;
        private DevExpress.XtraGrid.Columns.GridColumn colInputCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMarketingCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumLength;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumBDFTPerLog;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumRingsPerInch;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentSurfaceClear;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumKnotSizePerInch;
        private DevExpress.XtraGrid.Columns.GridColumn colKnotsPerFoot;
        private DevExpress.XtraGrid.Columns.GridColumn colSlopeOfGrain;
        private DevExpress.XtraGrid.Columns.GridColumn colProductRecoveryMinimum;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightInPounds;
        private DevExpress.XtraGrid.Columns.GridColumn colVolumeUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySpecies;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumButtDiameter;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}