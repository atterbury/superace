﻿namespace SuperACEForms
{
    partial class PriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemSaveChanges = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEditTableName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tblBindingSource = new System.Windows.Forms.BindingSource();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPricesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSorts = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrades = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDestination = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumButt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass36_39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass32_35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass28_31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass24_27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass22_23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass16_21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthClass12_15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSawUtil = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemSaveChanges,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barEditItem1,
            this.barEditItem2,
            this.barStaticItem6,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barLargeButtonItem1});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 30;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Save";
            this.barButtonItem6.Id = 15;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save To New Table";
            this.barButtonItem7.Id = 16;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Delete";
            this.barButtonItem8.Id = 17;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Set as Default";
            this.barButtonItem9.Id = 18;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Print";
            this.barLargeButtonItem1.Id = 29;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem6)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 9;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 465);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 441);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 441);
            // 
            // barStaticItemSaveChanges
            // 
            this.barStaticItemSaveChanges.Caption = "Save Changes";
            this.barStaticItemSaveChanges.Id = 1;
            this.barStaticItemSaveChanges.Name = "barStaticItemSaveChanges";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 19;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Id = 20;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Id = 21;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Id = 22;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Id = 23;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 7;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.Id = 8;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 24;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 26;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Id = 27;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 28;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboBoxEditTableName);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1011, 55);
            this.panelControl1.TabIndex = 4;
            // 
            // comboBoxEditTableName
            // 
            this.comboBoxEditTableName.Location = new System.Drawing.Point(93, 19);
            this.comboBoxEditTableName.MenuManager = this.barManager1;
            this.comboBoxEditTableName.Name = "comboBoxEditTableName";
            this.comboBoxEditTableName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTableName.Size = new System.Drawing.Size(209, 20);
            this.comboBoxEditTableName.TabIndex = 1;
            this.comboBoxEditTableName.SelectedValueChanged += new System.EventHandler(this.comboBoxEditTableName_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Table Name:";
            // 
            // tblBindingSource
            // 
            this.tblBindingSource.DataSource = typeof(Project.DAL.Price);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tblBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 79);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1011, 386);
            this.gridControl1.TabIndex = 10;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPricesID,
            this.colSorts,
            this.colGrades,
            this.colDestination,
            this.colTableName,
            this.colProductName,
            this.colMinimumDiameter,
            this.colMaximumDiameter,
            this.colMaximumButt,
            this.colPriceDate,
            this.colLengthClass40,
            this.colLengthClass36_39,
            this.colLengthClass32_35,
            this.colLengthClass28_31,
            this.colLengthClass24_27,
            this.colLengthClass22_23,
            this.colLengthClass16_21,
            this.colLengthClass12_15,
            this.colSawUtil,
            this.colUnits,
            this.colSpecies});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colPricesID
            // 
            this.colPricesID.FieldName = "PricesID";
            this.colPricesID.Name = "colPricesID";
            // 
            // colSorts
            // 
            this.colSorts.FieldName = "Sorts";
            this.colSorts.Name = "colSorts";
            this.colSorts.Visible = true;
            this.colSorts.VisibleIndex = 3;
            this.colSorts.Width = 47;
            // 
            // colGrades
            // 
            this.colGrades.FieldName = "Grades";
            this.colGrades.Name = "colGrades";
            this.colGrades.Visible = true;
            this.colGrades.VisibleIndex = 4;
            this.colGrades.Width = 49;
            // 
            // colDestination
            // 
            this.colDestination.FieldName = "Destination";
            this.colDestination.Name = "colDestination";
            this.colDestination.Visible = true;
            this.colDestination.VisibleIndex = 17;
            // 
            // colTableName
            // 
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colProductName
            // 
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 1;
            this.colProductName.Width = 100;
            // 
            // colMinimumDiameter
            // 
            this.colMinimumDiameter.Caption = "Min Diam";
            this.colMinimumDiameter.FieldName = "MinimumDiameter";
            this.colMinimumDiameter.Name = "colMinimumDiameter";
            this.colMinimumDiameter.Visible = true;
            this.colMinimumDiameter.VisibleIndex = 5;
            this.colMinimumDiameter.Width = 41;
            // 
            // colMaximumDiameter
            // 
            this.colMaximumDiameter.Caption = "Max Diam";
            this.colMaximumDiameter.FieldName = "MaximumDiameter";
            this.colMaximumDiameter.Name = "colMaximumDiameter";
            this.colMaximumDiameter.Visible = true;
            this.colMaximumDiameter.VisibleIndex = 6;
            this.colMaximumDiameter.Width = 41;
            // 
            // colMaximumButt
            // 
            this.colMaximumButt.Caption = "Max Butt";
            this.colMaximumButt.FieldName = "MaximumButt";
            this.colMaximumButt.Name = "colMaximumButt";
            this.colMaximumButt.Visible = true;
            this.colMaximumButt.VisibleIndex = 7;
            this.colMaximumButt.Width = 40;
            // 
            // colPriceDate
            // 
            this.colPriceDate.FieldName = "PriceDate";
            this.colPriceDate.Name = "colPriceDate";
            // 
            // colLengthClass40
            // 
            this.colLengthClass40.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass40.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass40.Caption = "40";
            this.colLengthClass40.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass40.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass40.FieldName = "LengthClass40";
            this.colLengthClass40.Name = "colLengthClass40";
            this.colLengthClass40.Visible = true;
            this.colLengthClass40.VisibleIndex = 8;
            this.colLengthClass40.Width = 34;
            // 
            // colLengthClass36_39
            // 
            this.colLengthClass36_39.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass36_39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass36_39.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass36_39.Caption = "36-39";
            this.colLengthClass36_39.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass36_39.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass36_39.FieldName = "LengthClass3639";
            this.colLengthClass36_39.Name = "colLengthClass36_39";
            this.colLengthClass36_39.Visible = true;
            this.colLengthClass36_39.VisibleIndex = 9;
            this.colLengthClass36_39.Width = 40;
            // 
            // colLengthClass32_35
            // 
            this.colLengthClass32_35.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass32_35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass32_35.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass32_35.Caption = "32-35";
            this.colLengthClass32_35.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass32_35.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass32_35.FieldName = "LengthClass3235";
            this.colLengthClass32_35.Name = "colLengthClass32_35";
            this.colLengthClass32_35.Visible = true;
            this.colLengthClass32_35.VisibleIndex = 10;
            this.colLengthClass32_35.Width = 40;
            // 
            // colLengthClass28_31
            // 
            this.colLengthClass28_31.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass28_31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass28_31.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass28_31.Caption = "28-31";
            this.colLengthClass28_31.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass28_31.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass28_31.FieldName = "LengthClass2831";
            this.colLengthClass28_31.Name = "colLengthClass28_31";
            this.colLengthClass28_31.Visible = true;
            this.colLengthClass28_31.VisibleIndex = 11;
            this.colLengthClass28_31.Width = 40;
            // 
            // colLengthClass24_27
            // 
            this.colLengthClass24_27.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass24_27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass24_27.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass24_27.Caption = "24-27";
            this.colLengthClass24_27.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass24_27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass24_27.FieldName = "LengthClass2427";
            this.colLengthClass24_27.Name = "colLengthClass24_27";
            this.colLengthClass24_27.Visible = true;
            this.colLengthClass24_27.VisibleIndex = 12;
            this.colLengthClass24_27.Width = 40;
            // 
            // colLengthClass22_23
            // 
            this.colLengthClass22_23.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass22_23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass22_23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass22_23.Caption = "22-23";
            this.colLengthClass22_23.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass22_23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass22_23.FieldName = "LengthClass2223";
            this.colLengthClass22_23.Name = "colLengthClass22_23";
            this.colLengthClass22_23.Visible = true;
            this.colLengthClass22_23.VisibleIndex = 13;
            this.colLengthClass22_23.Width = 40;
            // 
            // colLengthClass16_21
            // 
            this.colLengthClass16_21.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass16_21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass16_21.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass16_21.Caption = "16-21";
            this.colLengthClass16_21.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass16_21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass16_21.FieldName = "LengthClass1621";
            this.colLengthClass16_21.Name = "colLengthClass16_21";
            this.colLengthClass16_21.Visible = true;
            this.colLengthClass16_21.VisibleIndex = 14;
            this.colLengthClass16_21.Width = 40;
            // 
            // colLengthClass12_15
            // 
            this.colLengthClass12_15.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthClass12_15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthClass12_15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLengthClass12_15.Caption = "12-15";
            this.colLengthClass12_15.DisplayFormat.FormatString = "{0:#}";
            this.colLengthClass12_15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLengthClass12_15.FieldName = "LengthClass1215";
            this.colLengthClass12_15.Name = "colLengthClass12_15";
            this.colLengthClass12_15.Visible = true;
            this.colLengthClass12_15.VisibleIndex = 15;
            this.colLengthClass12_15.Width = 40;
            // 
            // colSawUtil
            // 
            this.colSawUtil.Caption = "Util";
            this.colSawUtil.FieldName = "SawUtil";
            this.colSawUtil.Name = "colSawUtil";
            this.colSawUtil.Visible = true;
            this.colSawUtil.VisibleIndex = 2;
            this.colSawUtil.Width = 45;
            // 
            // colUnits
            // 
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.Visible = true;
            this.colUnits.VisibleIndex = 16;
            this.colUnits.Width = 44;
            // 
            // colSpecies
            // 
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 0;
            this.colSpecies.Width = 100;
            // 
            // PriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 492);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PriceForm";
            this.Text = "Price Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpeciesForm_FormClosing);
            this.Load += new System.EventHandler(this.PriceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTableName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTableName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSaveChanges;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.BindingSource tblBindingSource;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPricesID;
        private DevExpress.XtraGrid.Columns.GridColumn colSorts;
        private DevExpress.XtraGrid.Columns.GridColumn colGrades;
        private DevExpress.XtraGrid.Columns.GridColumn colDestination;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSawUtil;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass40;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass36_39;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass32_35;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass28_31;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass24_27;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass22_23;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass16_21;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthClass12_15;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumButt;
    }
}