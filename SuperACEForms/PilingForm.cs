﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;
using Projects.DAL;
using Temp.DAL;
using System.Data.Entity;

namespace SuperACEForms
{
    public partial class PilingForm : DevExpress.XtraEditors.XtraForm
    {
        private string tableName = null;
        //private string dataSource = null;
        private ProjectDbContext ctx = null;

        public bool wasProjectChanged = false;

        public PilingForm(ref ProjectDbContext pContext, string pTableName)
        {
            InitializeComponent();

            ctx = pContext;
            //dataSource = pDataSource;
            tableName = pTableName;
        }

        private void PilingForm_Load(object sender, EventArgs e)
        {
            comboBoxEditSpecies.Text = tableName;

            LoadTableNames();
            LoadData();
        }

        private void comboBoxEditTableName_SelectedValueChanged(object sender, EventArgs e)
        {
            PopulateBindingSource(tableName, comboBoxEditSpecies.Text);
        }

        private void LoadData()
        {
            comboBoxEditSpecies.Properties.Items.Clear();
            var groupSpecies = ctx.Pilings.OrderBy(t => t.Species).ToList();
            foreach (var item in groupSpecies)
            {
                comboBoxEditSpecies.Properties.Items.Add(item.Species);
            }

            PopulateBindingSource(tableName, "");
            
        }

        private void PopulateBindingSource(string pTableName, string pSpecies)
        {
            if (tblBindingSource != null)
                tblBindingSource.Clear();
            if (string.IsNullOrEmpty(pSpecies))
            {
                tblBindingSource.DataSource = ctx.Pilings.Where(t => t.TableName == pTableName).ToList();
            }
            else
            {
                tblBindingSource.DataSource =
                    ctx.Pilings.Where(t => t.TableName == pTableName && t.Species == pSpecies).ToList();
            }

            if (tblBindingSource.Count > 0)
            {
                try
                {
                    var id = ((Piling) tblBindingSource.Current).Id; // tblColl[0].Id;
                    pilingPricesBindingSource.DataSource = ctx.PilingPrices.OrderBy(d => d.Length).Where(p => p.PilingId == id).ToList();

                    pilingDimensionsBindingSource.DataSource = ctx.PilingDimensions.OrderBy(d => d.Length).Where(d => d.PilingId == id).ToList();
                }
                catch
                {
                    XtraMessageBox.Show("Error reading Piling Prices or Piling Dimensions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            comboBoxEditSpecies.Text = ((Piling) tblBindingSource.Current).Species;
            comboBoxEditPricesPer.Text = ((Piling)tblBindingSource.Current).PricesPer;
        }

        private void LoadTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref ctx);
            //foreach (var tblItem in tbls)
            //    comboBoxEditTableName.Properties.Items.Add(tblItem);
            //comboBoxEditTableName.Text = tableName;

            //List<PriceSawUtil> suColl = ctx.PriceSawUtil.OrderBy(s => s.Code).Where(s => s.TableName == "GENERAL").ToList();
            //priceSawUtilBindingSource.DataSource = suColl;

            //List<ProjectLocationModel.VolumeUnits> puColl = ProjectLocationBLL.GetVolumeUnits(ref ctx);
            //priceUnitBindingSource.DataSource = puColl;

            //List<PriceDestination> pdColl = ctx.PriceDestination.OrderBy(d => d.Code).Where(d => d.TableName == "GENERAL").ToList();
            //priceDestinationBindingSource.DataSource = pdColl;
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save - real
            tblBindingSource.EndEdit();
            ctx.SaveChanges();
            this.Close();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save to new Table - real
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Delete table - real
            Project.DAL.Project currentProject = ProjectBLL.GetProject(ref ctx);

            if (comboBoxEditSpecies.Text == currentProject.SpeciesTableName)
            {
                XtraMessageBox.Show("Can't delete the Project.DAL.Project default table.\nTo delete, change the default table to a differnet table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete Table
            DialogResult deleteConfirmation = XtraMessageBox.Show(string.Format("You are about to delete {0} Table. Are you sure?", comboBoxEditSpecies.Text), "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteConfirmation != DialogResult.Yes)
                return;
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set as Default - real
            Project.DAL.Project currentProject = ctx.Projects.FirstOrDefault(); ;
            currentProject.SpeciesTableName = comboBoxEditSpecies.Text;
            ctx.SaveChanges();
            wasProjectChanged = true;
        }

        private void PilingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctx.SaveChanges();
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Print
            gridControl1.ShowPrintPreview();
        }
    }
}