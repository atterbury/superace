﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace Update
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext _dbContext;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _dbContext = new ProjectDbContext();
        }

        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barBtnCreateNewTrees_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<Tree> _trees = _dbContext.Trees.OrderBy(t => t.StandsId).ThenBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            foreach (var treeItem in _trees)
            {
                NewTree rec = new NewTree();
                rec.PlotId = treeItem.PlotId;
                rec.CrownPositionDisplayCode = treeItem.CrownPositionDisplayCode;
                rec.CrownRatioDisplayCode = treeItem.CrownRatioDisplayCode;
                rec.DamageDisplayCode = treeItem.DamageDisplayCode;
                rec.TreeStatusDisplayCode = treeItem.TreeStatusDisplayCode;
                rec.UserDefinedDisplayCode = treeItem.UserDefinedDisplayCode;
                rec.PlotFactorInput = treeItem.PlotFactorInput;
                rec.AgeCode = (byte)treeItem.AgeCode;
                rec.SpeciesAbbreviation = treeItem.SpeciesAbbreviation;
                rec.TopDiameterFractionCode = treeItem.TopDiameterFractionCode;
                rec.TreeNumber = treeItem.TreeNumber;
                rec.Dbh = treeItem.Dbh;
                rec.TreeCount = (byte)treeItem.TreeCount;
                rec.FormPoint = (byte)treeItem.FormPoint;
                rec.ReportedFormFactor = (byte)treeItem.ReportedFormFactor;
                rec.BoleHeight = treeItem.BoleHeight;
                rec.TotalHeight = treeItem.CalcTotalHeight;
                rec.VigorDisplayCode = treeItem.VigorDisplayCode;
                rec.TotalBdFtGrossVolume = treeItem.TotalBdFtGrossVolume;
                rec.TotalBdFtNetVolume = treeItem.TotalBdFtNetVolume;
                rec.TotalCuFtGrossVolume = treeItem.TotalCuFtGrossVolume;
                rec.TotalCuFtNetVolume = treeItem.TotalCuFtNetVolume;
                rec.TreesPerAcre = treeItem.TreesPerAcre;
                rec.BasalArea = treeItem.BasalArea;
                rec.LogsPerAcre = treeItem.LogsPerAcre;
                rec.TotalCcf = treeItem.TotalCcf;
                rec.TotalMbf = treeItem.TotalMbf;
                rec.TotalGrossCubicPerAcre = treeItem.TotalGrossCubicPerAcre;
                rec.TotalNetCubicPerAcre = treeItem.TotalNetCubicPerAcre;
                rec.TotalGrossScribnerPerAcre = treeItem.TotalGrossScribnerPerAcre;
                rec.TotalNetScribnerPerAcre = treeItem.TotalNetScribnerPerAcre;
                rec.StandsId = treeItem.StandsId;
                rec.PlotNumber = treeItem.PlotNumber;
                rec.GroupPlot = treeItem.GroupPlot;
                rec.GroupTree = treeItem.GroupTree;
                rec.Ao = treeItem.Ao;
                rec.Bark = treeItem.Bark;
                rec.TotalTonsPerAcre = treeItem.TotalTonsPerAcre;
                rec.TreeType = treeItem.TreeType;
                rec.Age = (byte)treeItem.Age;
                rec.CalcTotalHeight = treeItem.CalcTotalHeight;
                rec.CalcTotalHtUsedYn = treeItem.CalcTotalHtUsedYn;
                rec.PfValue = treeItem.PfValue;

            } 
            _dbContext.SaveChanges();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            XtraMessageBox.Show(string.Format("Elapsed Seconds: {0}", ts.Seconds), "Info");
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Create Segments
        }
    }
}
