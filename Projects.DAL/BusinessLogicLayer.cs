﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System;

namespace Projects.DAL
{
    public class ProjectsBLL
    {
        #region Gets
        public static List<VolumeUnit> GetVolumeUnits(ref ProjectsDbContext pCtx)
        {
            var result = pCtx.VolumeUnits.OrderBy(v => v.Code).ToList();
            return result;
        }

        public static bool DoesProjectExist(ref ProjectsDbContext pCtx, string pProject)
        {
            var result = pCtx.AllProjects.Any(s => s.ProjectName == pProject);
            return result;
        }

        public static List<Report> GetReports(ref ProjectsDbContext pCtx)
        {
            var result = pCtx.Reports.OrderBy(r => r.Seq).ToList();
            return result;
        }

        public static List<ReportItem> GetReportItems(ref ProjectsDbContext pCtx, Report report)
        {
            var result = pCtx.ReportItems.OrderBy(i => i.Seq).Where(i => i.ReportsId == report.ReportsId).ToList();
            return result;
        }

        public static string GetReportItem(ref ProjectsDbContext pCtx, string pHeading)
        {
            var result = pCtx.ReportItems.FirstOrDefault(i => i.Heading == pHeading);
            return result.ReportName;
        }

        //public static ProjectLocationModel.Project GetLastProject()
        //{
        //    ProjectLocationModel.Project ps = new ProjectLocationModel.Project();
        //    try
        //    {
        //        ps = pCtx.PROGRAMSETTINGS.FirstOrDefault().Project;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    return ps;
        //}

        //public static ProjectLocationModel.PROGRAMSETTINGS GetProgramSettings(ref ProjectLocationModelEntities pCtx)
        //{
        //    ProjectLocationModel.PROGRAMSETTINGS ps = new ProjectLocationModel.PROGRAMSETTINGS();
        //    try
        //    {
        //        ps = pCtx.PROGRAMSETTINGS.FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    return ps;
        //}

        public static AllProject GetProjectLocation(ref ProjectsDbContext pCtx, int id)
        {
            var project = pCtx.AllProjects.FirstOrDefault(p => p.ProjectsId == id);
            return project;
        }

        public static AllProject GetProjectLocationByProjectName(ref ProjectsDbContext pCtx, string pProjectName)
        {
            var project = pCtx.AllProjects.SingleOrDefault(p => p.ProjectName.Equals(pProjectName));
            return project;
        }

        public static AllProject GetProjectLocationByProjectName(string pDataSource, string pProjectName)
        {
            AllProject project;
            using (var context = new ProjectsDbContext("Data Source = " + pDataSource))
            {
                project = context.AllProjects.SingleOrDefault(p => p.ProjectName.Equals(pProjectName));
            }
            return project;
        }

        public static List<AllProject> GetProjects(ref ProjectsDbContext pCtx)
        {
            var projects = pCtx.AllProjects.OrderBy(p => p.ProjectName).ToList();
            return projects;
        }

        public static List<Table> GetTables(ref ProjectsDbContext pCtx)
        {
            var tbls = pCtx.Tables.OrderBy(t => t.Seq).ToList();
            return tbls;
        }

        public static List<TableItem> GetTableItems(ref ProjectsDbContext pCtx, int id)
        {
            var tbls = pCtx.TableItems.OrderBy(t => t.Seq).Where(t => t.TablesId.Equals(id)).ToList();
            return tbls;
        }

        public static List<ColumnsStand> GetStandColumns(ref ProjectsDbContext pCtx)
        {
            var cols = pCtx.ColumnsStands.ToList();
            return cols;
        }

        public static List<ColumnsPlot> GetPlotColumns(ref ProjectsDbContext pCtx)
        {
            var cols = pCtx.ColumnsPlots.ToList();
            return cols;
        }

        public static List<ColumnsTree> GetTreeColumns(ref ProjectsDbContext pCtx)
        {
            var cols = pCtx.ColumnsTrees.ToList();
            return cols;
        }

        public static List<ColumnsSegment> GetSegmentColumns(ref ProjectsDbContext pCtx)
        {
            var cols = pCtx.ColumnsSegments.ToList();
            return cols;
        }

        public static List<State> GetStates(ref ProjectsDbContext locContext)
        {
            var result = locContext.States.OrderBy(s => s.Abbr).ToList();
            return result;
        }

        public static List<StateCounty> GetStateCounty(ref ProjectsDbContext locContext, string pState)
        {
            var result = locContext.StateCounties.OrderBy(s => s.CountyAbbr).Where(s => s.StateAbbr == pState).ToList();
            return result;
        }

        public static string GetConnectionString(string pDataSource)
        {
            //string CONNECTIONSTRING = string.Format(@"data source=(LocalDB)\v11.0;attachdbfilename={0};integrated security=True;connect timeout=30;connect timeout=30;MultipleActiveResultSets=True;App=EntityFramework", pDataSource);
            //return CONNECTIONSTRING; // ecb.ToString();

            var connectionString = string.Format("Data Source={0}", pDataSource);
            System.Data.SqlClient.SqlConnectionStringBuilder scsb = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);

            var ecb = new EntityConnectionStringBuilder();
            ecb.Metadata = "res://*/ProjectLocationModel.csdl|res://*/ProjectLocationModel.ssdl|res://*/ProjectLocationModel.msl";
            ecb.Provider = "System.Data.SqlServerCe.4.0";
            ecb.ProviderConnectionString = connectionString;

            return ecb.ToString();
        }
        #endregion

        #region Save
        //public static ProjectLocationModel.PROGRAMSETTINGS SaveProgramSetting(ref ProjectLocationModelEntities pCtx, ProjectLocationModel.PROGRAMSETTINGS p)
        //{
        //    ProjectLocationModel.PROGRAMSETTINGS updatedPS = p;
        //    try
        //    {
        //        ProjectLocationModel.PROGRAMSETTINGS currentPS = pCtx.PROGRAMSETTINGS.FirstOrDefault();
        //        currentPS.LastProjectViewedID = p.LastProjectViewedID;
        //        currentPS.Project = p.Project;
        //        pCtx.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    return updatedPS;
        //}

        public static AllProject SaveProjectLocation(ref ProjectsDbContext pCtx, AllProject p)
        {
            AllProject project = p;
            if (project.ProjectsId > 0)
            {
                pCtx.Entry(project).State = EntityState.Modified;
                pCtx.SaveChanges();
            }
            else
            {
                pCtx.Entry(project).State = EntityState.Added;
                pCtx.AllProjects.Add(project);
                pCtx.SaveChanges();
            }
            return project;
        }

        public static AllProject SaveProjectLocation(string pConnection, AllProject p)
        {
            AllProject project = p;
            using (var context = new ProjectsDbContext("Data Source = " + pConnection))
            {
                if (project.ProjectsId > 0)
                {
                    context.Entry(project).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    context.Entry(project).State = EntityState.Added;
                    context.AllProjects.Add(project);
                    context.SaveChanges();
                }
            }
            return project;
        }
        #endregion

        #region Delete
        public static bool DeleteProjectLocation(ref ProjectsDbContext pCtx, AllProject p)
        {
            bool bSuccess;
            AllProject project = p;
            try
            {
                pCtx.AllProjects.Attach(project);
                pCtx.Entry(project).State = EntityState.Deleted;
                pCtx.SaveChanges();
                bSuccess = true;
            }
            catch
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        public static bool DeleteProjectLocation(string pConnection, string pProjectName)
        {
            bool bSuccess;
            //ProjectLocationModel.Project project = p;
            using (var context = new ProjectsDbContext("Data Source = " + pConnection))
            {
                try
                {
                    List<AllProject> projects = context.AllProjects.Where(p => p.ProjectName == pProjectName).ToList();
                    context.AllProjects.RemoveRange(projects);
                    //context.Project.Attach(project);
                    //context.Entry(project).State = EntityState.Deleted;
                    context.SaveChanges();
                    bSuccess = true;
                }
                catch
                {
                    bSuccess = false;
                }
            }
            return bSuccess;
        }

        public static bool DeleteProjectLocation(ref ProjectsDbContext pCtx, string pProjectName)
        {
            bool bSuccess;
            //ProjectLocationModel.Project project = p;
            try
            {
                List<AllProject> projects = pCtx.AllProjects.Where(p => p.ProjectName == pProjectName).ToList();
                pCtx.AllProjects.RemoveRange(projects);
                pCtx.SaveChanges();
                bSuccess = true;
            }
            catch
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        public static string GetPrimarySpecies(string pProjectLocationDataSource, string abbreviation)
        {
            PrimarySpecy result;
            using (var context = new ProjectsDbContext("Data Source = " + pProjectLocationDataSource))
            {
                result = context.PrimarySpecies.SingleOrDefault(p => p.Species.Equals(abbreviation));
            }
            if (result == null)
                return abbreviation;
            else
                return result.PrimarySpecies;
        }

        public static List<string> GetSources(ref ProjectsDbContext locContext)
        {
            List<string> varList = new List<string>();
            var result = (from s in locContext.Sources
                          orderby s.Code
                          select new { Code = s.Code }).ToList();
            foreach (var item in result)
                varList.Add(item.Code);
            return varList;
        }
        #endregion
    }
}
