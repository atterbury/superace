using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using Project.DAL;
using SuperACECalcs;

namespace SuperACEUtils
{
	/// <summary>
	/// Summary description for Utils.
	/// </summary>
	public static class Utils
    {
        public static bool Setting_RptPrepHardWrite { get; set; } = false;

        public static List<Price> tPrices = null;

        public static string CalculateAreaPlotAcres(string pAreaCode, string pPlotRadius)
        {
            double plotRadiusNumber;
            bool isNum = double.TryParse(pPlotRadius, out plotRadiusNumber);
            if (isNum && plotRadiusNumber > 0)
            {
                return string.Format("{0:0.000}", (plotRadiusNumber * plotRadiusNumber) / (43560 / Math.PI));
            }
            else
            {
                return "";
            }
        }

        public static string CalculateAreaPlotAcres(string pAreaCode, string pSide1, string pSide2)
        {
            if (Utils.IsValidFloat(pSide1) && Utils.IsValidFloat(pSide2))
            {
                return string.Format("{0:0.000}", ((Utils.ConvertToFloat(pSide1) * Utils.ConvertToFloat(pSide2)) / (float)43560));
            }
            else
            {
                return "";
            }
        }

        public static string CalculateAreaPlotRadius(string pAreaCode, string pPlotAcres)
        {
            double plotRadiusNumber;
            bool isNum = double.TryParse(pPlotAcres, out plotRadiusNumber);
            if (isNum && plotRadiusNumber > 0)
            {
                return string.Format("{0:0.00}", Math.Sqrt((43560 / Math.PI) * Utils.ConvertToFloat(pPlotAcres)));
            }
            else
            {
                return "";
            }
        }

        public static string CalculateAreaBlowup(string pAreaCode, string pPlotAcres)
        {
            float blowup;
            float tOne = 1.0f;
            bool isNum = float.TryParse(pPlotAcres, out blowup);
            if (isNum && blowup > 0)
            {
                return string.Format("{0:0.00}", tOne / (float)Utils.ConvertToFloat(pPlotAcres));
            }
            else
            {
                return "";
            }
        }

        #region Strip Cruise
        public static string CalculateStripCruiseExpansion(string pInterval, string pWidth, string pBlowup)
        {   //  interval and width are both in feet
            //  expansion factor is a percent
            Double intervalNumber;
            Double widthNumber;
            bool intervalIsANumber = double.TryParse(pInterval, out intervalNumber);
            bool widthIsANumber = double.TryParse(pWidth, out widthNumber);

            if (intervalIsANumber && widthIsANumber && intervalNumber > 0 && widthNumber > 0)
            {
                return string.Format("{0:0.00}", intervalNumber / widthNumber); // Convert.ToString(Math.Round(widthNumber / intervalNumber, 2));
            }
            else
            {
                return string.Format("{0:0.00}", ConvertToFloat(pBlowup));
            }
        }
        #endregion

        #region BAF
        public static string CalculateLimitingDistanceFactor(string pBaf)
        {
            //  takes a string that may or may not be a number
            //  returns BAF limiting distance factor
            //  multiply by DBH to get limiting distance
            Double number;
            bool isNum = double.TryParse(pBaf, out number);
            if (isNum && number > 0)
            {
                return string.Format("{0:0.0000}", Math.Sqrt(75.621 / number)); // Convert.ToString(Math.Sqrt(75.621 / number));
            }
            else
            {
                return "";
            }
        }

        public static float CalculateLimitingDistanceFactor(float pBaf)
        {
            //  takes a string that may or may not be a number
            //  returns BAF limiting distance factor
            //  multiply by DBH to get limiting distance
            //Double number;
            //bool isNum = double.TryParse(pBaf, out number);
            if (IsValidFloat(pBaf.ToString()))
            {
                return ConvertToFloat(string.Format("{0:0.0000}", Math.Sqrt(75.621 / pBaf))); // Convert.ToString(Math.Sqrt(75.621 / number));
            }
            else
            {
                return 0;
            }
        }
        #endregion

        public static double CalcPriceForSegment(Treesegment itemSeg, List<Price> tPrices)
        {
            bool bFound = false;
            double price = 0.0;

            if (itemSeg.SortCode == "0" || itemSeg.GradeCode == "0")
            {
                bFound = true;
                return 0.0;
            }
            // check if matching species
            foreach (Price item in tPrices)
            {
                if (item.Species.Contains(itemSeg.Species))
                {
                    if (item.Sorts.Contains(itemSeg.SortCode))
                    {
                        if (item.Grades.Contains(itemSeg.GradeCode))
                        {
                            if (itemSeg.CalcTopDia >= item.MinimumDiameter && itemSeg.CalcTopDia <= item.MaximumDiameter)
                            {
                                if (itemSeg.CalcLen > 0)
                                {
                                    if (itemSeg.CalcLen >= 12 && itemSeg.CalcLen <= 15)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass1215);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass1215);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 16 && itemSeg.CalcLen <= 21)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass1621);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass1621);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 21 && itemSeg.CalcLen <= 23)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2223);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2223);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 24 && itemSeg.CalcLen <= 27)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2427);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2427);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 28 && itemSeg.CalcLen <= 31)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2831);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2831);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 32 && itemSeg.CalcLen <= 35)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass3235);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass3235);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 36 && itemSeg.CalcLen <= 39)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass3639);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass3639);
                                        break;
                                    }
                                    if (itemSeg.CalcLen >= 40)
                                    {
                                        bFound = true;
                                        if (item.Units == "Ccf")
                                            price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass40);
                                        else
                                            price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass40);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Species was NOT found - use "*"
            if (bFound == false)
            {
                foreach (Price item in tPrices)
                {
                    if (item.Species == "*")
                    {
                        if (itemSeg.CalcTopDia >= item.MinimumDiameter && itemSeg.CalcTopDia <= item.MaximumDiameter)
                        {
                            if (itemSeg.CalcLen > 0)
                            {
                                if (itemSeg.CalcLen >= 12 && itemSeg.CalcLen <= 15)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass1215);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass1215);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 16 && itemSeg.CalcLen <= 21)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass1621);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass1621);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 21 && itemSeg.CalcLen <= 23)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2223);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2223);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 24 && itemSeg.CalcLen <= 27)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2427);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2427);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 28 && itemSeg.CalcLen <= 31)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass2831);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass2831);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 32 && itemSeg.CalcLen <= 35)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass3235);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass3235);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 36 && itemSeg.CalcLen <= 39)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass3639);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass3639);
                                    break;
                                }
                                if (itemSeg.CalcLen >= 40)
                                {
                                    bFound = true;
                                    if (item.Units == "Ccf")
                                        price = (double)((itemSeg.CubicNetVolume / 100) * (double)item.LengthClass40);
                                    else
                                        price = (double)((itemSeg.ScribnerNetVolume / 1000) * (double)item.LengthClass40);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return price;
        }

        ///
        public static void DeleteSegmentsForStand(ref ProjectDbContext pContext, Stand pStand)
        {
            List<Tree> treesColl = pContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == pStand.StandsId).ToList();

            foreach (Tree item in treesColl)
            {
                // Delete and existing segments for the tree
                pContext.Treesegments.RemoveRange(pContext.Treesegments.Where(s => s.TreesId == item.TreesId));
            }
        }

        public static void CreateSegmentsFromTree(ref ProjectDbContext pContext, Tree item)
        {
            //Tree saveTree = new Tree();
            //saveTree = item;
            //if (!string.IsNullOrEmpty(item.Length1))
            //{
            Treesegment s1 = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
            s1.TreesId = item.TreesId;
            s1.StandsId = item.StandsId;
            s1.PlotId = item.PlotId;
            s1.TreeBasalArea = item.BasalArea;
            s1.TreeTreesPerAcre = item.TreesPerAcre;
            s1.GroupPlotNumber = item.PlotNumber;
            s1.GroupTreeNumber = item.TreeNumber;
            s1.ScribnerGrossVolume = (short)Calc.seg[0].scribner_gross;
            s1.ScribnerNetVolume = (short)Calc.seg[0].scribner_net;
            s1.CubicGrossVolume = Calc.seg[0].cubic_gross;
            s1.CubicNetVolume = Calc.seg[0].cubic_net;

            s1.ScribnerGrossVolumePerAce = (double)Calc.seg[0].scribner_gross_pa;
            s1.ScribnerNetVolumePerAce = (double)Calc.seg[0].scribner_net_pa;
            s1.CubicGrossVolumePerAce = (double)Calc.seg[0].cubic_gross_pa;
            s1.CubicNetVolumePerAce = (double)Calc.seg[0].cubic_net_pa;

            s1.Ccf = ConvertToFloat(Calc.seg[0].ccf.ToString());
            s1.Mbf = ConvertToFloat(Calc.seg[0].mbf.ToString());
            if (Calc.seg[0].len > 0)
                s1.CalcLen = (byte)(Calc.seg[0].len);
            else
                s1.CalcLen = 0;
            if (Calc.seg[0].cum_len > 0)
                s1.CalcAccumulatedLength = (byte)(Calc.seg[0].cum_len);
            else
                s1.CalcAccumulatedLength = 0;
            s1.CalcTopDia = (float)Math.Round((decimal)Calc.seg[0].tdia, 2);
            s1.CalcButtDia = (float)Math.Round((decimal)Calc.seg[0].bdia, 2);
            s1.SegmentNumber = 1;
            s1.TreeType = item.TreeType;
            s1.Species = item.SpeciesAbbreviation.Trim();
            s1.Age = item.Age;
            s1.Ao = item.Ao;
            s1.Bark = item.Bark;
            s1.BoleHeight = item.BoleHeight;
            s1.CrownPosition = item.CrownPositionDisplayCode;
            s1.CrownRatio = item.CrownRatioDisplayCode;
            s1.Damage = item.DamageDisplayCode;
            if (item.Dbh != null)
                s1.Dbh = item.Dbh;
            else
                s1.Dbh = 0;
            if (item.ReportedFormFactor != null)
                s1.FormFactor = item.ReportedFormFactor;
            else
                s1.FormFactor = 0;
            if (item.FormPoint != null)
                s1.FormPoint = item.FormPoint;
            else
                s1.FormPoint = 0;
            s1.PlotFactorInput = item.PlotFactorInput;
            s1.PfType = item.PfType;
            s1.PlotNumber = item.PlotNumber;
            s1.Tdf = item.TopDiameterFractionCode;
            s1.TotalHeight = item.TotalHeight;
            s1.TreeCount = item.TreeCount;
            s1.TreeNumber = (short)item.TreeNumber;
            s1.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
            s1.UserDefined = item.UserDefinedDisplayCode;
            s1.Vigor = item.VigorDisplayCode;
            s1.CalcTotalHtUsedYn = item.CalcTotalHtUsedYn;
            if (item.BdFtLd1 != null)
                s1.BdFtLd = (byte)item.BdFtLd1;
            else
                s1.BdFtLd = 0;
            if (item.BdFtDd1 != null)
                s1.BdFtDd = (byte)item.BdFtDd1;
            else
                s1.BdFtDd = 0;
            if (item.BdFtPd1 != null)
                s1.BdFtPd = item.BdFtPd1;
            else
                s1.BdFtPd = string.Empty;
            if (item.CuFtDd1 != null)
                s1.CuFtDd = (byte)item.CuFtDd1;
            else
                s1.CuFtDd = 0;
            if (item.CuFtLd1 != null)
                s1.CuFtLd = (byte)item.CuFtLd1;
            else
                s1.CuFtLd = 0;
            if (Calc.seg[0].len > 0)
                s1.Length = Calc.seg[0].len.ToString();
            else
                s1.Length = string.Empty;
            if (item.Sort1 != null)
                s1.SortCode = item.Sort1.Trim();
            else
                s1.SortCode = string.Empty;
            if (item.Grade1 != null)
                s1.GradeCode = item.Grade1.Trim();
            else
                s1.GradeCode = string.Empty;

            double price1 = CalcPriceForSegment(s1, tPrices);
            if (s1.SortCode == "0" || s1.GradeCode == "0")
                s1.LogValue = 0.0;
            else
                s1.LogValue = price1;
            s1.Comments = string.Empty;
            //if (item.Sort1 == "0" || item.Grade1 == "0")
            //    s1.Comments = "Cull Segment";
            foreach (var teItem in Calc.teList)
            {
                if (teItem.segment == 1)
                {
                    if (s1.Comments.Length > 0)
                        s1.Comments += string.Format("; {0}", teItem.ErrorMsg);
                    else
                        s1.Comments = string.Format("{0}", teItem.ErrorMsg);
                }
            }
            pContext.Treesegments.Add(s1);
            //}
            if (Calc.seg[1].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.ScribnerGrossVolume = (short)Calc.seg[1].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[1].scribner_net;
                s.CubicGrossVolume = Calc.seg[1].cubic_gross;
                s.CubicNetVolume = Calc.seg[1].cubic_net;

                s.ScribnerGrossVolumePerAce = (double)Calc.seg[1].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (double)Calc.seg[1].scribner_net_pa;
                s.CubicGrossVolumePerAce = (double)Calc.seg[1].cubic_gross_pa;
                s.CubicNetVolumePerAce = (double)Calc.seg[1].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[1].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[1].mbf.ToString());
                if (Calc.seg[1].len > 0)
                    s.CalcLen = (byte)(Calc.seg[1].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[1].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[1].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[1].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[1].bdia, 2);
                
                s.SegmentNumber = 2;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd2 != null)
                    s.BdFtLd = (byte)item.BdFtLd2;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd2 != null)
                    s.BdFtDd = (byte)item.BdFtDd2;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd2 != null)
                    s.BdFtPd = item.BdFtPd2;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd2 != null)
                    s.CuFtDd = (byte)item.CuFtDd2;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd2 != null)
                    s.CuFtLd = (byte)item.CuFtLd2;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[1].len > 0)
                    s.Length = Calc.seg[1].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort2 != null)
                    s.SortCode = item.Sort2.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade2 != null)
                    s.GradeCode = item.Grade2.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort2 == "0" || item.Grade2 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 2)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[2].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[2].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[2].scribner_net;
                s.CubicGrossVolume = Calc.seg[2].cubic_gross;
                s.CubicNetVolume = Calc.seg[2].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[2].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[2].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[2].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[2].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[2].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[2].mbf.ToString());
                if (Calc.seg[2].len > 0)
                    s.CalcLen = (byte)(Calc.seg[2].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[2].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[2].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[2].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[2].bdia, 2);

                s.SegmentNumber = 3;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd3 != null)
                    s.BdFtLd = (byte)item.BdFtLd3;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd3 != null)
                    s.BdFtDd = (byte)item.BdFtDd3;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd3 != null)
                    s.BdFtPd = item.BdFtPd3;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd3 != null)
                    s.CuFtDd = (byte)item.CuFtDd3;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd3 != null)
                    s.CuFtLd = (byte)item.CuFtLd3;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[2].len > 0)
                    s.Length = Calc.seg[2].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort3 != null)
                    s.SortCode = item.Sort3.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade3 != null)
                    s.GradeCode = item.Grade3.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort3 == "0" || item.Grade3 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 3)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[3].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[3].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[3].scribner_net;
                s.CubicGrossVolume = Calc.seg[3].cubic_gross;
                s.CubicNetVolume = Calc.seg[3].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[3].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[3].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[3].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[3].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[3].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[3].mbf.ToString());
                if (Calc.seg[3].len > 0)
                    s.CalcLen = (byte)(Calc.seg[3].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[3].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[3].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[3].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[3].bdia, 2);

                s.SegmentNumber = 4;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd4 != null)
                    s.BdFtLd = (byte)item.BdFtLd4;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd4 != null)
                    s.BdFtDd = (byte)item.BdFtDd4;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd4 != null)
                    s.BdFtPd = item.BdFtPd4;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd4 != null)
                    s.CuFtDd = (byte)item.CuFtDd4;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd4 != null)
                    s.CuFtLd = (byte)item.CuFtLd4;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[3].len > 0)
                    s.Length = Calc.seg[3].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort4 != null)
                    s.SortCode = item.Sort4.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade4 != null)
                    s.GradeCode = item.Grade4.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort4 == "0" || item.Grade4 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 4)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[4].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[4].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[4].scribner_net;
                s.CubicGrossVolume = Calc.seg[4].cubic_gross;
                s.CubicNetVolume = Calc.seg[4].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[4].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[4].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[4].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[4].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[4].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[4].mbf.ToString());
                if (Calc.seg[4].len > 0)
                    s.CalcLen = (byte)(Calc.seg[4].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[4].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[4].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[4].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[4].bdia, 2);

                s.SegmentNumber = 5;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd5 != null)
                    s.BdFtLd = (byte)item.BdFtLd5;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd5 != null)
                    s.BdFtDd = (byte)item.BdFtDd5;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd5 != null)
                    s.BdFtPd = item.BdFtPd5;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd5 != null)
                    s.CuFtDd = (byte)item.CuFtDd5;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd5 != null)
                    s.CuFtLd = (byte)item.CuFtLd5;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[4].len > 0)
                    s.Length = Calc.seg[4].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort5 != null)
                    s.SortCode = item.Sort5.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade5 != null)
                    s.GradeCode = item.Grade5.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort5 == "0" || item.Grade5 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 5)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[5].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[5].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[5].scribner_net;
                s.CubicGrossVolume = Calc.seg[5].cubic_gross;
                s.CubicNetVolume = Calc.seg[5].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[5].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[5].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[5].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[5].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[5].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[5].mbf.ToString());
                if (Calc.seg[5].len > 0)
                    s.CalcLen = (byte)(Calc.seg[5].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[5].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[5].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[5].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[5].bdia, 2);
                s.SegmentNumber = 6;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd6 != null)
                    s.BdFtLd = (byte)item.BdFtLd6;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd6 != null)
                    s.BdFtDd = (byte)item.BdFtDd6;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd6 != null)
                    s.BdFtPd = item.BdFtPd6;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd6 != null)
                    s.CuFtDd = (byte)item.CuFtDd6;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd6 != null)
                    s.CuFtLd = (byte)item.CuFtLd6;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[5].len > 0)
                    s.Length = Calc.seg[5].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort6 != null)
                    s.SortCode = item.Sort6.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade6 != null)
                    s.GradeCode = item.Grade6.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort6 == "0" || item.Grade6 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 6)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[6].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[6].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[6].scribner_net;
                s.CubicGrossVolume = Calc.seg[6].cubic_gross;
                s.CubicNetVolume = Calc.seg[6].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[6].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[6].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[6].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[6].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[6].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[6].mbf.ToString());
                if (Calc.seg[6].len > 0)
                    s.CalcLen = (byte)(Calc.seg[6].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[6].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[6].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[6].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[6].bdia, 2);

                s.SegmentNumber = 7;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd7 != null)
                    s.BdFtLd = (byte)item.BdFtLd7;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd7 != null)
                    s.BdFtDd = (byte)item.BdFtDd7;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd7 != null)
                    s.BdFtPd = item.BdFtPd7;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd7 != null)
                    s.CuFtDd = (byte)item.CuFtDd7;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd7 != null)
                    s.CuFtLd = (byte)item.CuFtLd7;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[6].len > 0)
                    s.Length = Calc.seg[6].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort7 != null)
                    s.SortCode = item.Sort7.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade7 != null)
                    s.GradeCode = item.Grade7.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort7 == "0" || item.Grade7 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 7)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[7].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[7].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[7].scribner_net;
                s.CubicGrossVolume = Calc.seg[7].cubic_gross;
                s.CubicNetVolume = Calc.seg[7].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[7].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[7].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[7].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[7].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[7].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[7].mbf.ToString());
                if (Calc.seg[7].len > 0)
                    s.CalcLen = (byte)(Calc.seg[7].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[7].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[7].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[7].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[7].bdia, 2);
                
                s.SegmentNumber = 8;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd8 != null)
                    s.BdFtLd = (byte)item.BdFtLd8;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd8 != null)
                    s.BdFtDd = (byte)item.BdFtDd8;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd8 != null)
                    s.BdFtPd = item.BdFtPd8;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd8 != null)
                    s.CuFtDd = (byte)item.CuFtDd8;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd8 != null)
                    s.CuFtLd = (byte)item.CuFtLd8;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[7].len > 0)
                    s.Length = Calc.seg[7].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort8 != null)
                    s.SortCode = item.Sort8.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade8 != null)
                    s.GradeCode = item.Grade8.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort8 == "0" || item.Grade8 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 8)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[8].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[8].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[8].scribner_net;
                s.CubicGrossVolume = Calc.seg[8].cubic_gross;
                s.CubicNetVolume = Calc.seg[8].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[8].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[8].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[8].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[8].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[8].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[8].mbf.ToString());
                if (Calc.seg[8].len > 0)
                    s.CalcLen = (byte)(Calc.seg[8].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[8].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[8].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[8].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[8].bdia, 2);
                
                s.SegmentNumber = 9;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd9 != null)
                    s.BdFtLd = (byte)item.BdFtLd9;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd9 != null)
                    s.BdFtDd = (byte)item.BdFtDd9;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd9 != null)
                    s.BdFtPd = item.BdFtPd9;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd9 != null)
                    s.CuFtDd = (byte)item.CuFtDd9;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd9 != null)
                    s.CuFtLd = (byte)item.CuFtLd9;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[8].len > 0)
                    s.Length = Calc.seg[8].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort9 != null)
                    s.SortCode = item.Sort9.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade9 != null)
                    s.GradeCode = item.Grade9.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort9 == "0" || item.Grade9 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 9)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[9].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[9].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[9].scribner_net;
                s.CubicGrossVolume = Calc.seg[9].cubic_gross;
                s.CubicNetVolume = Calc.seg[9].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[9].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[9].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[9].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[9].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[9].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[9].mbf.ToString());
                if (Calc.seg[9].len > 0)
                    s.CalcLen = (byte)(Calc.seg[9].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[9].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[9].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[9].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[9].bdia, 2);

                s.SegmentNumber = 10;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd10 != null)
                    s.BdFtLd = (byte)item.BdFtLd10;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd10 != null)
                    s.BdFtDd = (byte)item.BdFtDd10;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd10 != null)
                    s.BdFtPd = item.BdFtPd10;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd10 != null)
                    s.CuFtDd = (byte)item.CuFtDd10;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd10 != null)
                    s.CuFtLd = (byte)item.CuFtLd10;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[9].len > 0)
                    s.Length = Calc.seg[9].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort10 != null)
                    s.SortCode = item.Sort10.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade10 != null)
                    s.GradeCode = item.Grade10.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort10 == "0" || item.Grade10 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 10)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[10].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[10].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[10].scribner_net;
                s.CubicGrossVolume = Calc.seg[10].cubic_gross;
                s.CubicNetVolume = Calc.seg[10].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[10].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[10].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[10].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[10].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[10].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[10].mbf.ToString());
                if (Calc.seg[10].len > 0)
                    s.CalcLen = (byte)(Calc.seg[10].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[10].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[10].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[10].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[10].bdia, 2);
                
                s.SegmentNumber = 11;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd11 != null)
                    s.BdFtLd = (byte)item.BdFtLd11;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd11 != null)
                    s.BdFtDd = (byte)item.BdFtDd11;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd11 != null)
                    s.BdFtPd = item.BdFtPd11;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd11 != null)
                    s.CuFtDd = (byte)item.CuFtDd11;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd11 != null)
                    s.CuFtLd = (byte)item.CuFtLd11;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[10].len > 0)
                    s.Length = Calc.seg[10].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort2 != null)
                    s.SortCode = item.Sort2.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade11 != null)
                    s.GradeCode = item.Grade11.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort11 == "0" || item.Grade11 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 11)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            if (Calc.seg[11].len > 0)
            {
                Treesegment s = new Treesegment(); // item.TreesegmentsCollectionByTreesId.AddNew();
                s.TreesId = item.TreesId;
                s.StandsId = item.StandsId;
                s.PlotId = item.PlotId;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.TreeBasalArea = 0; //item.BasalArea;
                s.TreeTreesPerAcre = 0; //item.TreesPerAcre;
                s.ScribnerGrossVolume = (short)Calc.seg[11].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[11].scribner_net;
                s.CubicGrossVolume = Calc.seg[11].cubic_gross;
                s.CubicNetVolume = Calc.seg[11].cubic_net;

                s.ScribnerGrossVolumePerAce = (short)Calc.seg[11].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (short)Calc.seg[11].scribner_net_pa;
                s.CubicGrossVolumePerAce = Calc.seg[11].cubic_gross_pa;
                s.CubicNetVolumePerAce = Calc.seg[11].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[11].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[11].mbf.ToString());
                if (Calc.seg[11].len > 0)
                    s.CalcLen = (byte)(Calc.seg[11].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[11].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[1].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[11].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[11].bdia, 2);
                
                s.SegmentNumber = 12;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                s.Dbh = item.Dbh;
                s.FormFactor = item.ReportedFormFactor;
                s.FormPoint = item.FormPoint;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PfType = item.PfType;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                if (item.BdFtLd12 != null)
                    s.BdFtLd = (byte)item.BdFtLd12;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd12 != null)
                    s.BdFtDd = (byte)item.BdFtDd12;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd12 != null)
                    s.BdFtPd = item.BdFtPd12;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd12 != null)
                    s.CuFtDd = (byte)item.CuFtDd12;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd12 != null)
                    s.CuFtLd = (byte)item.CuFtLd12;
                else
                    s.CuFtLd = 0;
                if (Calc.seg[11].len > 0)
                    s.Length = Calc.seg[11].len.ToString();
                else
                    s.Length = string.Empty;
                if (item.Sort12 != null)
                    s.SortCode = item.Sort12.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade12 != null)
                    s.GradeCode = item.Grade12.Trim();
                else
                    s.GradeCode = string.Empty;

                double price = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price;
                s.Comments = string.Empty;
                //if (item.Sort12 == "0" || item.Grade12 == "0")
                //    s.Comments = "Cull Segment";
                foreach (var teItem in Calc.teList)
                {
                    if (teItem.segment == 12)
                    {
                        if (s.Comments.Length > 0)
                            s.Comments += string.Format("; {0}", teItem.ErrorMsg);
                        else
                            s.Comments = string.Format("{0}", teItem.ErrorMsg);
                    }
                }
                pContext.Treesegments.Add(s);
            }
            //pContext.SaveChanges();
        }

        public static void UpdateSegmentsFromTree(ref ProjectDbContext pContext, Tree item)
        {
            //Tree saveTree = new Tree();
            //saveTree = item;
            //if (!string.IsNullOrEmpty(item.Length1))
            //{
            for (int i = 0; i < Calc.number_segments+1; i++)
            {
                Treesegment s = pContext.Treesegments.FirstOrDefault(t => t.TreesId == item.TreesId && t.SegmentNumber == i + 1);
                s.TreeBasalArea = item.BasalArea;
                s.TreeTreesPerAcre = item.TreesPerAcre;
                s.GroupPlotNumber = item.PlotNumber;
                s.GroupTreeNumber = item.TreeNumber;
                s.ScribnerGrossVolume = (short)Calc.seg[i].scribner_gross;
                s.ScribnerNetVolume = (short)Calc.seg[i].scribner_net;
                s.CubicGrossVolume = Calc.seg[i].cubic_gross;
                s.CubicNetVolume = Calc.seg[i].cubic_net;

                s.ScribnerGrossVolumePerAce = (double)Calc.seg[i].scribner_gross_pa;
                s.ScribnerNetVolumePerAce = (double)Calc.seg[i].scribner_net_pa;
                s.CubicGrossVolumePerAce = (double)Calc.seg[i].cubic_gross_pa;
                s.CubicNetVolumePerAce = (double)Calc.seg[i].cubic_net_pa;

                s.Ccf = ConvertToFloat(Calc.seg[i].ccf.ToString());
                s.Mbf = ConvertToFloat(Calc.seg[i].mbf.ToString());
                if (Calc.seg[i].len > 0)
                    s.CalcLen = (byte)(Calc.seg[i].len);
                else
                    s.CalcLen = 0;
                if (Calc.seg[i].cum_len > 0)
                    s.CalcAccumulatedLength = (byte)(Calc.seg[i].cum_len);
                else
                    s.CalcAccumulatedLength = 0;
                s.CalcTopDia = (float)Math.Round((decimal)Calc.seg[i].tdia, 2);
                s.CalcButtDia = (float)Math.Round((decimal)Calc.seg[i].bdia, 2);
                if (item.Sort1 == "0" || item.Grade1 == "0")
                    s.Comments = "Cull Segment";
                s.SegmentNumber = i+1;
                s.TreeType = item.TreeType;
                s.Species = item.SpeciesAbbreviation.Trim();
                s.Age = item.Age;
                s.Ao = item.Ao;
                s.Bark = item.Bark;
                s.BoleHeight = item.BoleHeight;
                s.CrownPosition = item.CrownPositionDisplayCode;
                s.CrownRatio = item.CrownRatioDisplayCode;
                s.Damage = item.DamageDisplayCode;
                if (item.Dbh != null)
                    s.Dbh = item.Dbh;
                else
                    s.Dbh = 0;
                if (item.ReportedFormFactor != null)
                    s.FormFactor = item.ReportedFormFactor;
                else
                    s.FormFactor = 0;
                if (item.FormPoint != null)
                    s.FormPoint = item.FormPoint;
                else
                    s.FormPoint = 0;
                s.PlotFactorInput = item.PlotFactorInput;
                s.PlotNumber = item.PlotNumber;
                s.Tdf = item.TopDiameterFractionCode;
                s.TotalHeight = item.TotalHeight;
                s.TreeCount = item.TreeCount;
                s.TreeNumber = (short)item.TreeNumber;
                s.TreeStatusDisplayCode = item.TreeStatusDisplayCode;
                s.UserDefined = item.UserDefinedDisplayCode;
                s.Vigor = item.VigorDisplayCode;
                s.CalcTotalHtUsedYn = item.CalcTotalHtUsedYn;
                if (item.BdFtLd1 != null)
                    s.BdFtLd = (byte)item.BdFtLd1;
                else
                    s.BdFtLd = 0;
                if (item.BdFtDd1 != null)
                    s.BdFtDd = (byte)item.BdFtDd1;
                else
                    s.BdFtDd = 0;
                if (item.BdFtPd1 != null)
                    s.BdFtPd = item.BdFtPd1;
                else
                    s.BdFtPd = string.Empty;
                if (item.CuFtDd1 != null)
                    s.CuFtDd = (byte)item.CuFtDd1;
                else
                    s.CuFtDd = 0;
                if (item.CuFtLd1 != null)
                    s.CuFtLd = (byte)item.CuFtLd1;
                else
                    s.CuFtLd = 0;
                if (item.Length1 != null)
                    s.Length = item.Length1.Trim();
                else
                    s.Length = string.Empty;
                if (item.Sort1 != null)
                    s.SortCode = item.Sort1.Trim();
                else
                    s.SortCode = string.Empty;
                if (item.Grade1 != null)
                    s.GradeCode = item.Grade1.Trim();
                else
                    s.GradeCode = string.Empty;

                double price1 = CalcPriceForSegment(s, tPrices);
                if (s.SortCode == "0" || s.GradeCode == "0")
                    s.LogValue = 0.0;
                else
                    s.LogValue = price1;
            }
        }

        public static void UpdateCountPlotsForStand(ref ProjectDbContext projectContext, Project.DAL.Project pProject, Stand st)
        {
            int i = st.StandsId;
            var countPlots = projectContext.Trees.Where(t => t.StandsId == i && t.TreeType == "C").ToList();
            // check to see if there are count plots in the stand
            if (countPlots.Count == 0)
                return;

            var q = from t in projectContext.Trees
                    group t by t.SpeciesAbbreviation into myGroup
                    orderby myGroup.Key
                    select new { Species = myGroup.Key, BasalArea = myGroup.Sum(t => t.BasalArea) };
            foreach (var item in q)
            {
                string t = item.Species;
                float ba = (float)item.BasalArea;
            }
        }

        public static void UpdatePlotsForStand(ref ProjectDbContext pContext, Project.DAL.Project pProject, Stand pStand)
        {
            Stand st = pContext.Stands.FirstOrDefault(s => s.StandsId == pStand.StandsId);
            List<Plot> tPlots = pContext.Plots.OrderBy(p => p.UserPlotNumber).Where(p => p.StandsId == st.StandsId).ToList();

            foreach (Plot itemPlot in tPlots)
            {
                List<Tree> tTrees = pContext.Trees.Where(t => t.PlotId == itemPlot.PlotsId).ToList();
                float ba = 0;
                float tpa = 0;
                float netcfpa = 0;
                float netbfpa = 0;
                foreach (var itemTree in tTrees)
                {
                    if (itemTree.BasalArea != null)
                        ba += (float)itemTree.BasalArea;
                    if (itemTree.TreesPerAcre != null)
                        tpa += (float)itemTree.TreesPerAcre;
                    if (itemTree.TotalNetCubicPerAcre != null)
                        netcfpa += (float)itemTree.TotalNetCubicPerAcre;
                    if (itemTree.TotalNetScribnerPerAcre != null)
                        netbfpa += (float)itemTree.TotalNetScribnerPerAcre;
                }
                itemPlot.BasalAreaAcre = ba;
                itemPlot.TreesPerAcre = tpa;
                itemPlot.NetCfAcre = netcfpa;
                itemPlot.NetBfAcre = netbfpa;
                pContext.SaveChanges();
            }
        }

        public static void UpdateTotalsForStand(ref ProjectDbContext pContext, Project.DAL.Project pProject, Stand pStand, bool pCalc)
        {
            Stand st = pContext.Stands.FirstOrDefault(s => s.StandsId == pStand.StandsId);
            Calc.SetCalcContextProjectStand(ref pContext, pProject, ref st);
            List<Tree> treesColl = pContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == st.StandsId).ToList();

            if (pCalc)
            {
                foreach (Tree item in treesColl)
                {
                    if (item.TreeCount > 0)
                    {
                        Calc.tree = item;
                        if (item.TreeCount > 0)
                            Calc.CalcVolume(true);
                        item.BasalArea = (double)Calc.basal_area;
                        item.TreesPerAcre = (double)Calc.trees_per_acre;
                        item.LogsPerAcre = (double)Calc.logs_pa;
                        item.Ao = (double)Calc.ao;
                        item.Bark = Calc.bark;
                        item.TotalBdFtGrossVolume = (double)Calc.tree_scribner_gross;
                        item.TotalBdFtNetVolume = (double)Calc.tree_scribner_net;
                        item.TotalMbf = (double)Calc.tree_mbf;

                        item.TotalCuFtGrossVolume = (double)Calc.tree_cubic_gross;
                        item.TotalCuFtNetVolume = (double)Calc.tree_cubic_net;
                        item.TotalCcf = (double)Calc.tree_ccf;

                        item.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                        item.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                        item.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                        item.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;
                    }
                }
                pContext.SaveChanges();
            }

            //Stand mRec = pStand;
            Calc.CalcStandFields(ref pContext, st);
        }

        public static void DoCalcForTree(ref ProjectDbContext ctx, ref Tree pTree, ref Plot pPlot, ref Stand pStand, Project.DAL.Project pProject)
        {
            Calc.Init();
            if (string.IsNullOrEmpty(pTree.SpeciesAbbreviation))
                return;
            if (string.IsNullOrEmpty(pTree.PlotFactorInput))
                return;
            ////if (pTree.Dbh == 0)
            ////    return;
            //if (pTree.BoleHeight == 0)
            //    return;

            //Calc calc = new Calc(ref ctx, pProject, ref pStand);
            Calc.tree = pTree;
            Calc.CalcVolume(false);

            //MoveCalcToTree(ref pTree);
            pTree.CalcTotalHeight = (short)Calc.total_tree_height;
            pTree.BasalArea = Calc.basal_area;
            pTree.TreesPerAcre = Calc.trees_per_acre;
            pTree.LogsPerAcre = Calc.logs_pa;
            pTree.Ao = Calc.ao;
            pTree.Bark = Calc.bark;

            pTree.TotalBdFtGrossVolume = Calc.tree_scribner_gross;
            pTree.TotalBdFtNetVolume = Calc.tree_scribner_net;
            pTree.TotalMbf = Calc.tree_mbf;

            pTree.TotalCuFtGrossVolume = Calc.tree_cubic_gross;
            pTree.TotalCuFtNetVolume = Calc.tree_cubic_net;
            pTree.TotalCcf = Calc.tree_ccf;

            pTree.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
            pTree.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
            pTree.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
            pTree.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

            pTree.CalcTotalHtUsedYn = "N";
            if (pTree.TotalHeight == null || pTree.TotalHeight == 0)
            {
                pTree.TotalHeight = pTree.CalcTotalHeight;
                pTree.CalcTotalHtUsedYn = "Y";
            }
            ProjectBLL.SetTreeType(ref pTree);

            var spc = ProjectBLL.GetSpecieByAbbrev(ref ctx, pProject.SpeciesTableName, pTree.SpeciesAbbreviation);
            float weight = 0;
            if (spc.Lbs != null)
                weight = (float)spc.Lbs;
            else
                if (spc.Weight != null)
                    weight = (float)spc.Weight;
            //float weight = (float)ProjectBLL.GetSpecieByAbbrev(ref ctx, pProject.SpeciesTableName, pTree.SpeciesAbbreviation).Lbs;
            pTree.TotalTonsPerAcre = ((pTree.TotalCcf * weight) / 2000) / pStand.NetGeographicAcres;

            if (pTree.PlotId == null)
                pTree.PlotId = pPlot.PlotsId;

            //Utils.DeleteSegmentsForStand(ref ctx, pStand);
            //Utils.CreateSegmentsFromTree(ref ctx, pTree, calc);
            //float tAcres = 0;
            //Utils.UpdateCountPlotsForStand(ref ctx, pProject, ref pStand);
            //Utils.UpdatePlotsForStand(ref ctx, pProject, ref pStand);
            //Utils.UpdateTotalsForStand(ref ctx, pProject, ref pStand);
        }

        public static void MoveCalcToTree(ref Tree pTree)
        {
            //pTree.Age = (byte)Calc.GetAge(pTree.AgeCode.ToString());
            pTree.CalcTotalHeight = (short)Calc.total_tree_height;
            pTree.BasalArea = Calc.basal_area;
            pTree.TreesPerAcre = Calc.trees_per_acre;
            pTree.LogsPerAcre = Calc.logs_pa;
            pTree.Ao = Calc.ao;
            pTree.Bark = Calc.bark;

            pTree.TotalBdFtGrossVolume = Calc.tree_scribner_gross;
            pTree.TotalBdFtNetVolume = Calc.tree_scribner_net;
            pTree.TotalMbf = Calc.tree_mbf;

            pTree.TotalCuFtGrossVolume = Calc.tree_cubic_gross;
            pTree.TotalCuFtNetVolume = Calc.tree_cubic_net;
            pTree.TotalCcf = Calc.tree_ccf;

            pTree.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
            pTree.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
            pTree.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
            pTree.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

            pTree.CalcTotalHtUsedYn = "N";
            if (pTree.TotalHeight == null || pTree.TotalHeight == 0)
            {
                pTree.TotalHeight = pTree.CalcTotalHeight;
                pTree.CalcTotalHtUsedYn = "Y";
            }
            ProjectBLL.SetTreeType(ref pTree);
        }

        public static short GetAge(ref ProjectDbContext ctx, Stand pStand, string p)
        {
            byte age = Utils.ConvertToTiny(p);
            StandAge rec = ctx.StandAges.FirstOrDefault(a => a.StandsId == pStand.StandsId && a.AgeCode == age);
            if (rec != null)
                return rec.Age;
            else
                return 50;
            //switch (p)
            //{
            //    case "1":
            //        return (short)pStand.Age1;
            //    case "2":
            //        return (short)pStand.Age2;
            //    case "3":
            //        return (short)pStand.Age3;
            //    case "4":
            //        return (short)pStand.Age4;
            //    case "5":
            //        return (short)pStand.Age5;
            //    case "6":
            //        return (short)pStand.Age6;
            //    case "7":
            //        return (short)pStand.Age7;
            //    case "8":
            //        return (short)pStand.Age8;
            //    case "9":
            //        return (short)pStand.Age9;
            //    default:
            //        return 50;
            //}
        }

        public static byte ConvertToTiny(string sTmp)
        {
            byte f;

            try
            {
                f = Convert.ToByte(sTmp);
            }
            catch
            {
                f = 0;
            }
            return (f);
        }

        public static float ConvertToFloat(string sTmp)
		{
			float f;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

			try
			{
				f = Convert.ToSingle(tmp);
			}
			catch
			{
				f = 0.0f;
			}
			return(f);
		}

        public static int ConvertToInt(string sTmp)
		{
			int i;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

			try
			{
				i = Convert.ToInt32(tmp);
			}
			catch
			{
				i = 0;
			}
			return(i);
		}

        public static double ConvertToDouble(string sTmp)
		{
			double f;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

			try
			{
				f = Convert.ToDouble(tmp);
			}
			catch
			{
				f = 0.0f;
			}
			return(f);
		}

        public static decimal ConvertToDecimal(string sTmp)
        {
            decimal f;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

            try
            {
                f = Convert.ToDecimal(tmp);
            }
            catch
            {
                f = 0;
            }
            return (f);
        }

        public static long ConvertToLong(string sTmp)
		{
			long l;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

			try
			{
				l = Convert.ToInt32(tmp);
			}
			catch
			{
				l = 0;
			}
			return(l);
		}

        public static bool IsEven(string sTmp)
		{
			int iNum = ConvertToInt(sTmp);

			if(iNum % 2 == 0)
			{
				return(true); // It's even
			}
			else
			{
				return(false);  // It's odd
			}
		}

        public static bool IsOdd(string sTmp)
		{
			int iNum = ConvertToInt(sTmp);

			if(iNum % 2 == 0)
			{
				return(false); // It's even
			}
			else
			{
				return(true);  // It's odd
			}
		}

        public static bool IsDigit(char c)
		{
			bool b;

			try
			{
				b = Char.IsDigit(c);
			}
			catch
			{
				b = false;
			}
			return(b);
		}

        public static bool IsAlpha(char c)
		{
			bool b;

			try
			{
				b = Char.IsLetter(c);
			}
			catch
			{
				b = false;
			}
			return(b);
		}

        public static bool IsValidInt(string sTmp)
		{
			try
			{
				int iTmp = Int16.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}

        public static bool IsValidLong(string sTmp)
		{
			try
			{
				long lTmp = Int32.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}

        public static bool IsValidFloat(string sTmp)
		{
			try
			{
				float fTmp = Single.Parse(sTmp);
				return(true);
			}
			catch
			{
				return(false);
			}
		}

        public static void DeleteSegmentsForTree(ref ProjectDbContext projectContext, Tree pTree)
        {
            // Delete and existing segments for the tree
            projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(s => s.TreesId == pTree.TreesId));
            //projectContext.SaveChanges();
        }
    }
}
