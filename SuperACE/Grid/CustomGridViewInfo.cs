﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;

namespace SuperACE.Grid
{
    public class CustomGridViewInfo :GridViewInfo {
        public CustomGridViewInfo(GridView view) : base(view) { }

        public override int MinRowHeight {
            get { return 0; }
        }
    }
}
