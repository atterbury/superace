﻿using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace SuperACE.Grid
{
    public class CustomGridView : GridView {
        public CustomGridView() : base() { }
        public CustomGridView(GridControl grid) : base(grid) { }

        internal const string CustomName = "CustomGridView";
        protected override string ViewName {
            get { return CustomName; }
        }
    }
}
