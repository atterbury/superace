﻿using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace SuperACE.Grid
{
    public class CustomAdvBandedGridView : AdvBandedGridView {
        public CustomAdvBandedGridView() : base() { }
        public CustomAdvBandedGridView(GridControl grid) : base(grid) { }

        internal const string CustomName = "CustomAdvBandedGridView";
        protected override string ViewName {
            get { return CustomName; }
        }
    }
}
