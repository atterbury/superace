﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Projects.DAL;
using Project.DAL;
using Temp.DAL;
using DevExpress.XtraSplashScreen;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;

using SuperACEUtils;
using System.IO;
using System.Data.Entity.Core;

namespace SuperACE
{
    public partial class PlotCopyForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext = null;
        private ProjectDbContext projectContext = null;
        private string _projectDataSource = string.Empty;
        private TempDbContext dbContext = null;
        private ProjectDbContext ctx = null;
        private Stand origStand = null;

        public PlotCopyForm(ref ProjectsDbContext pLocContext, ref ProjectDbContext pProjectContext, Stand pOrigStand)
        {
            InitializeComponent();

            locContext = pLocContext;
            projectContext = pProjectContext;
            origStand = pOrigStand;
        }

        private void PlotCopyForm_Load(object sender, EventArgs e)
        {
            tbOrigProject.Text =  Properties.Settings.Default["CurrentProject"].ToString();
            tbOriginalFolder.Text = Properties.Settings.Default["CurrentDataDir"].ToString();

            cbDestinationProject.Enabled = false;
            tbDestinationFolder.Enabled = false;

            List<AllProject> projects = locContext.AllProjects.OrderBy(p => p.ProjectName).ToList();
            foreach (var item in projects)
            {
                //if (item.ProjectName != tbOrigProject.Text)
                cbDestinationProject.Properties.Items.Add(item.ProjectName);
            }

            // Instantiate a new DBContext
            dbContext = new Temp.DAL.TempDbContext();

            LoadData();
        }

        private void LoadData()
        {
            tempCopyPlotsBindingSource.Clear();
            tempCopyPlotsBindingSource.DataSource = dbContext.TempCopyPlots.ToList();
        }

        private void cbOrigProject_SelectedValueChanged(object sender, EventArgs e)
        {
            Projects.DAL.AllProject rec = locContext.AllProjects.FirstOrDefault(p => p.ProjectName == tbOrigProject.Text);
            tbOriginalFolder.Text = rec.ProjectLocation;

            List<Projects.DAL.AllProject> projects = locContext.AllProjects.OrderBy(p => p.ProjectName).ToList();
            foreach (var item in projects)
            {
                if (item.ProjectName != tbOrigProject.Text)
		            cbDestinationProject.Properties.Items.Add(item.ProjectName);
            }
        }

        private void tbDestinationFolder_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = Properties.Settings.Default["DbLocation"].ToString();
            frm.ShowDialog();

            if (!string.IsNullOrEmpty(frm.SelectedPath))
            {
                tbDestinationFolder.Text = frm.SelectedPath + "\\";
            }
            frm.Dispose();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Process
            bandedGridView1.MoveFirst();
            tempCopyStandsBindingSource.EndEdit();
            dbContext.SaveChanges();

            if (string.IsNullOrEmpty(cbDestinationProject.Text))
            {
                XtraMessageBox.Show("Destination Project is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbDestinationProject.Focus();
                return;
            }

            if (string.IsNullOrEmpty(tbDestinationFolder.Text))
            {
                XtraMessageBox.Show("Destination Folder is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbDestinationFolder.Focus();
                return;
            }

            if (!CheckTractStandTownshipRangeSection())
                return;

            Stand rec = ctx.Stands.Where(s => s.TractName == tbTract.Text && s.StandName == tbStand.Text && s.Township == tbTownship.Text && s.Range == tbRange.Text && s.Section == tbSection.Text).FirstOrDefault();

            if (rec == null)
            {
                XtraMessageBox.Show("Destination Stand does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (CheckForExistingPlots(ref ctx))
            {
                LoadData();
                XtraMessageBox.Show("Plots in Red are already existing.\nExisting plots must be removed from the list or changed to another plot number.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");

            Stopwatch sw = new Stopwatch();
            sw.Start();

            SplashScreenManager.Default.SetWaitFormDescription(string.Format("Copying Plots for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
                    tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));

            List<TempCopyPlot> tempPlots = dbContext.TempCopyPlots.ToList();
            foreach (TempCopyPlot tempItem in tempPlots)
            {
                if (!string.IsNullOrEmpty(tempItem.Plot1))
                    AddPlotTreesSegments(tempItem.Plot1, tempItem.OrigPlot1);
                if (!string.IsNullOrEmpty(tempItem.Plot2))
                    AddPlotTreesSegments(tempItem.Plot2, tempItem.OrigPlot2);
                if (!string.IsNullOrEmpty(tempItem.Plot3))
                    AddPlotTreesSegments(tempItem.Plot3, tempItem.OrigPlot3);
                if (!string.IsNullOrEmpty(tempItem.Plot4))
                    AddPlotTreesSegments(tempItem.Plot4, tempItem.OrigPlot4);
                if (!string.IsNullOrEmpty(tempItem.Plot5))
                    AddPlotTreesSegments(tempItem.Plot5, tempItem.OrigPlot5);
                if (!string.IsNullOrEmpty(tempItem.Plot6))
                    AddPlotTreesSegments(tempItem.Plot6, tempItem.OrigPlot6);
                if (!string.IsNullOrEmpty(tempItem.Plot7))
                    AddPlotTreesSegments(tempItem.Plot7, tempItem.OrigPlot7);
                if (!string.IsNullOrEmpty(tempItem.Plot8))
                    AddPlotTreesSegments(tempItem.Plot8, tempItem.OrigPlot8);
                if (!string.IsNullOrEmpty(tempItem.Plot9))
                    AddPlotTreesSegments(tempItem.Plot9, tempItem.OrigPlot9);
                if (!string.IsNullOrEmpty(tempItem.Plot10))
                    AddPlotTreesSegments(tempItem.Plot10, tempItem.OrigPlot10);
            }
            //int newPlotCount = 0;
            //List<Plot> addPlots = new List<Plot>();
            //List<Plot> plotsToCopy = projectContext.Plots.Where(p => p.StandsId == origStand.StandsId)
            //        .OrderBy(p => p.UserPlotNumber)
            //        .ToList();
            //foreach (var plotItem in plotsToCopy)
            //{
            //    Plot newPlot = new Plot();
            //    CopyNewPlot(plotItem, ref newPlot, rec.StandsId);
            //    ++newPlotCount;
            //    addPlots.Add(newPlot);
            //    if (newPlotCount > 100)
            //    {
            //        newPlotCount = 0;
            //        ctx.Plots.AddRange(addPlots);
            //        addPlots.Clear();
            //    }
            //}
            //ctx.Plots.AddRange(addPlots);
            //ctx.SaveChanges();
            //addPlots.Clear();

            //int newTreeCount = 0;
            //List<Tree> addTrees = new List<Tree>();
            //SplashScreenManager.Default.SetWaitFormDescription(string.Format("Copying Trees for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
            //    tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            //List<Tree> origTrees = projectContext.Trees.Where(t => t.StandsId == origStand.StandsId).ToList();
            //foreach (var treeItem in origTrees)
            //{
            //    Tree newTree = new Tree();
            //    int newPlot = ctx.Plots.Where(p => p.StandsId == rec.StandsId && p.UserPlotNumber == treeItem.PlotNumber).Select(p => p.PlotsId).FirstOrDefault();
            //    CopyNewTree(treeItem, ref newTree, newPlot, rec.StandsId);
            //    ++newTreeCount;
            //    addTrees.Add(newTree);
            //    if (newTreeCount > 100)
            //    {
            //        newTreeCount = 0;
            //        ctx.Trees.AddRange(addTrees);
            //        addTrees.Clear();
            //    }
            //}
            //ctx.Trees.AddRange(addTrees);
            //ctx.SaveChanges();
            //addTrees.Clear();

            //int newSegmentCount = 0;
            //List<Treesegment> addSegments = new List<Treesegment>();
            //SplashScreenManager.Default.SetWaitFormDescription(string.Format("Copying Segments for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
            //    tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            //List<Treesegment> origSegments = projectContext.Treesegments.Where(t => t.StandsId == origStand.StandsId).OrderBy(t => t.GroupPlotNumber)
            //    .ThenBy(t => t.GroupTreeNumber).ThenBy(t => t.SegmentNumber).ToList();
            //foreach (var segmentItem in origSegments)
            //{
            //    Treesegment newSegment = new Treesegment();
            //    int newPlot = ctx.Plots.Where(p => p.StandsId == rec.StandsId && p.UserPlotNumber == segmentItem.GroupPlotNumber).Select(p => p.PlotsId).FirstOrDefault();
            //    int newTree = ctx.Trees.Where(p => p.StandsId == rec.StandsId && p.PlotNumber == segmentItem.GroupPlotNumber && p.TreeNumber == segmentItem.GroupTreeNumber)
            //        .Select(p => p.TreesId).FirstOrDefault();
            //    CopyNewSegment(segmentItem, ref newSegment, newTree, newPlot, rec.StandsId);
            //    ++newSegmentCount;
            //    addSegments.Add(newSegment);
            //    if (newSegmentCount > 100)
            //    {
            //        newSegmentCount = 0;
            //        ctx.Treesegments.AddRange(addSegments);
            //        addSegments.Clear();
            //    }
            //}
            //ctx.Treesegments.AddRange(addSegments);
            //ctx.SaveChanges();
            //addSegments.Clear();

            SplashScreenManager.Default.SetWaitFormDescription(string.Format("Updating Count Tables for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
                tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            ProjectBLL.BuildAdjCruisePlots(ref ctx, rec); // was ref projectContext, origStand
            ProjectBLL.BuildCountWithDiaTable(ref ctx, rec.StandsId); // was ref projectContext, origStand.StandsId

            //SplashScreenManager.Default.SetWaitFormDescription(string.Format("Processing Count Trees for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
            //    tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            //ProjectBLL.ResetCountRecords(ref ctx); // was ref projectContext
            //ProjectBLL.ProcessCountRecords(ref ctx); // was ref projectContext

            Utils.UpdateCountPlotsForStand(ref ctx, MainForm.CurrentProject, rec);
            Utils.UpdatePlotsForStand(ref ctx, MainForm.CurrentProject, rec);
            SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Totals from Trees");
            Utils.UpdateTotalsForStand(ref ctx, MainForm.CurrentProject, rec, false);
            SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Stand Input from Tree Input");
            ProjectBLL.ProcessCruiseToForestInventory(ref ctx, MainForm.CurrentProject, rec, true);

            SplashScreenManager.CloseForm();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            Debug.WriteLine(string.Format("Loading Time - {0} secs", ts.Seconds));

            ctx.Dispose();
            ctx = null;

            this.Close();
        }

        private void AddPlotTreesSegments(string plot, string origPlot)
        {
            //string newPlotNumber = string.Empty;
            //int newPlotId = -1;
            Stand tmpStand = null;
            Plot tmpPlot = null;

            tmpStand = ctx.Stands.FirstOrDefault(s => s.TractName == tbTract.Text && s.StandName == tbStand.Text);
            if (tmpStand == null)
                return;

            Plot plotToCopy = projectContext.Plots.FirstOrDefault(p => p.StandsId == origStand.StandsId && p.UserPlotNumber == origPlot);
            Plot newPlot = new Plot();
            CopyNewPlot(plotToCopy, ref newPlot, tmpStand.StandsId);
            ctx.Plots.Add(newPlot);
            ctx.SaveChanges();

            //tmpPlot = ctx.Plots.FirstOrDefault(p => p.StandsId == tmpStand.StandsId && p.UserPlotNumber == plot);
            //if (tmpStand != null && tmpPlot != null)
            //{
            //    newPlotNumber = tmpPlot.UserPlotNumber;
            //    newPlotId = tmpPlot.PlotsId;
            //}

            int newTreeCount = 0;
            List<Tree> addTrees = new List<Tree>();
            SplashScreenManager.Default.SetWaitFormDescription(string.Format("Copying Trees for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
                tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            List<Tree> origTrees = projectContext.Trees.Where(t => t.StandsId == origStand.StandsId && t.PlotNumber == origPlot).ToList();
            foreach (var treeItem in origTrees)
            {
                Tree newTree = new Tree();
                //Plot newPlot1 = ctx.Plots.FirstOrDefault(p => p.StandsId == origStand.StandsId && p.UserPlotNumber == origPlot);
                CopyNewTree(treeItem, ref newTree, newPlot.PlotsId, newPlot.UserPlotNumber, tmpStand.StandsId);
                ++newTreeCount;
                addTrees.Add(newTree);
                if (newTreeCount > 100)
                {
                    newTreeCount = 0;
                    ctx.Trees.AddRange(addTrees);
                    addTrees.Clear();
                }
            }
            ctx.Trees.AddRange(addTrees);
            ctx.SaveChanges();
            addTrees.Clear();

            int newSegmentCount = 0;
            List<Treesegment> addSegments = new List<Treesegment>();
            SplashScreenManager.Default.SetWaitFormDescription(string.Format("Copying Segments for Tract:{0} Stand:{1} Township:{2} Range:{3} Section:{4}",
                tbTract.Text, tbStand.Text, tbTownship.Text, tbRange.Text, tbSection.Text));
            List<Treesegment> origSegments = projectContext.Treesegments.Where(t => t.StandsId == origStand.StandsId && t.PlotNumber == origPlot).OrderBy(t => t.GroupPlotNumber)
                .ThenBy(t => t.GroupTreeNumber).ThenBy(t => t.SegmentNumber).ToList();
            foreach (var segmentItem in origSegments)
            {
                Treesegment newSegment = new Treesegment();
                //int newPlot2 = ctx.Plots.Where(p => p.StandsId == origStand.StandsId && p.UserPlotNumber == plot).Select(p.TreeNumber == ;
                //int newTree2 = ctx.Trees.Where(p => p.StandsId == origStand.StandsId && p.PlotNumber == plot && p.TreeNumber == segmentItem.GroupTreeNumber)
                //    .Select(p.Tree)
                CopyNewSegment(segmentItem, ref newSegment, (int)segmentItem.TreeNumber, newPlot.PlotsId, newPlot.UserPlotNumber, tmpStand.StandsId);
                ++newSegmentCount;
                addSegments.Add(newSegment);
                if (newSegmentCount > 100)
                {
                    newSegmentCount = 0;
                    ctx.Treesegments.AddRange(addSegments);
                    addSegments.Clear();
                }
            }
            ctx.Treesegments.AddRange(addSegments);
            ctx.SaveChanges();
            addSegments.Clear();
        }

        private bool CheckTractStandTownshipRangeSection()
        {
            if (string.IsNullOrEmpty(tbTract.Text))
            {
                XtraMessageBox.Show("Tract is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbTract.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(tbStand.Text))
            {
                XtraMessageBox.Show("Stand is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbStand.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(tbTownship.Text))
            {
                XtraMessageBox.Show("Township is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbTownship.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(tbRange.Text))
            {
                XtraMessageBox.Show("Range is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbRange.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(tbSection.Text))
            {
                XtraMessageBox.Show("Section is missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbSection.Focus();
                return false;
            }
            return true;
        }

        private bool CheckForExistingPlots(ref ProjectDbContext pCtx)
        {
            Stand rec = pCtx.Stands.Where(s => s.TractName == tbTract.Text && s.StandName == tbStand.Text && s.Township == tbTownship.Text && s.Range == tbRange.Text && s.Section == tbSection.Text).FirstOrDefault();
            bool hasExistingPlots = false;
            List<TempCopyPlot> plots = dbContext.TempCopyPlots.ToList();
            foreach (var item in plots)
            {
                if (!string.IsNullOrEmpty(item.Plot1))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot1))
                    {
                        item.Plot1Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot1Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot2))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot2))
                    {
                        item.Plot2Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot2Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot3))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot3))
                    {
                        item.Plot3Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot3Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot4))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot4))
                    {
                        item.Plot4Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot4Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot5))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot5))
                    {
                        item.Plot5Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot5Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot6))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot6))
                    {
                        item.Plot6Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot6Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot7))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot7))
                    {
                        item.Plot7Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot7Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot8))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot8))
                    {
                        item.Plot8Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot8Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot9))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot9))
                    {
                        item.Plot9Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot9Exist = false;
                }

                if (!string.IsNullOrEmpty(item.Plot10))
                {
                    if (pCtx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == item.Plot10))
                    {
                        item.Plot10Exist = true;
                        hasExistingPlots = true;
                    }
                    else
                        item.Plot10Exist = false;
                }
            }
            return hasExistingPlots;
        }
        
        private void CopyNewStand(ref Stand origStand, ref Stand newStand, TempCopyStand item, int p)
        {
            newStand.TractName = item.NewTractName;
            newStand.StandName = item.NewStandName;
            newStand.Township = item.NewTownship;
            newStand.Range = item.NewRange;
            newStand.Section = item.NewSection;
            //newStand.Age1 = origStand.Age1;
            //newStand.Age2 = origStand.Age2;
            //newStand.Age3 = origStand.Age3;
            //newStand.Age4 = origStand.Age4;
            //newStand.Age5 = origStand.Age5;
            //newStand.Age6 = origStand.Age6;
            //newStand.Age7 = origStand.Age7;
            //newStand.Age8 = origStand.Age8;
            //newStand.Age9 = origStand.Age9;
            newStand.AreaBlowup1 = origStand.AreaBlowup1;
            newStand.AreaBlowup2 = origStand.AreaBlowup2;
            newStand.AreaBlowup3 = origStand.AreaBlowup3;
            newStand.AreaBlowup4 = origStand.AreaBlowup4;
            newStand.AreaBlowup5 = origStand.AreaBlowup5;
            newStand.AreaCode1 = origStand.AreaCode1;
            newStand.AreaCode2 = origStand.AreaCode2;
            newStand.AreaCode3 = origStand.AreaCode3;
            newStand.AreaCode4 = origStand.AreaCode4;
            newStand.AreaCode5 = origStand.AreaCode5;
            newStand.AreaPlotAcres1 = origStand.AreaPlotAcres1;
            newStand.AreaPlotAcres2 = origStand.AreaPlotAcres2;
            newStand.AreaPlotAcres3 = origStand.AreaPlotAcres3;
            newStand.AreaPlotAcres4 = origStand.AreaPlotAcres4;
            newStand.AreaPlotAcres5 = origStand.AreaPlotAcres5;
            newStand.AreaPlotRadius1 = origStand.AreaPlotRadius1;
            newStand.AreaPlotRadius2 = origStand.AreaPlotRadius2;
            newStand.AreaPlotRadius3 = origStand.AreaPlotRadius3;
            newStand.AreaPlotRadius4 = origStand.AreaPlotRadius4;
            newStand.AreaPlotRadius5 = origStand.AreaPlotRadius5;
            newStand.AreaSide11 = origStand.AreaSide11;
            newStand.AreaSide12 = origStand.AreaSide12;
            newStand.AreaSide21 = origStand.AreaSide21;
            newStand.AreaSide22 = origStand.AreaSide22;
            newStand.AreaSide31 = origStand.AreaSide31;
            newStand.AreaSide32 = origStand.AreaSide32;
            newStand.AreaSide41 = origStand.AreaSide41;
            newStand.AreaSide42 = origStand.AreaSide42;
            newStand.AreaSide51 = origStand.AreaSide51;
            newStand.AreaSide52 = origStand.AreaSide52;
            newStand.Attachments = origStand.Attachments;
            newStand.AutoSegmentLength = origStand.AutoSegmentLength;
            newStand.AvgTotalHeight = origStand.AvgTotalHeight;
            newStand.Baf1 = origStand.Baf1;
            newStand.Baf2 = origStand.Baf2;
            newStand.Baf3 = origStand.Baf3;
            newStand.Baf4 = origStand.Baf4;
            newStand.Baf5 = origStand.Baf5;
            newStand.BafPlotRadius1 = origStand.BafPlotRadius1;
            newStand.BafPlotRadius2 = origStand.BafPlotRadius2;
            newStand.BafPlotRadius3 = origStand.BafPlotRadius3;
            newStand.BafPlotRadius4 = origStand.BafPlotRadius4;
            newStand.BafPlotRadius5 = origStand.BafPlotRadius5;
            newStand.BasalAreaPerAcre = origStand.BasalAreaPerAcre;
            newStand.BirthDate = origStand.BirthDate;
            newStand.CcfPerAcre = origStand.CcfPerAcre;
            newStand.CostTableName = origStand.CostTableName;
            newStand.County = origStand.County;
            newStand.CruiseFlag = origStand.CruiseFlag;
            newStand.DateOfStandData = origStand.DateOfStandData;
            newStand.DiameterInterval = origStand.DiameterInterval;
            newStand.DiameterRound = origStand.DiameterRound;
            newStand.DownWoddyFlag = origStand.DownWoddyFlag;
            newStand.GradeTableName = origStand.GradeTableName;
            newStand.GrossGeographicAcres = origStand.GrossGeographicAcres;
            newStand.GrownToDate = origStand.GrownToDate;
            newStand.GrownToMonth = origStand.GrownToMonth;
            newStand.GrownToYear = origStand.GrownToYear;
            newStand.GrowthModelTableName = origStand.GrowthModelTableName;
            newStand.Harvest = origStand.Harvest;
            newStand.HarvestFlag = origStand.HarvestFlag;
            newStand.IntervalZero = origStand.IntervalZero;
            newStand.InventoryFlag = origStand.InventoryFlag;
            newStand.LandClass = origStand.LandClass;
            newStand.LegalAcres = origStand.LegalAcres;
            newStand.LogsPerAcre = origStand.LogsPerAcre;
            newStand.MajorAge = origStand.MajorAge;
            newStand.MajorSpecies = origStand.MajorSpecies;
            newStand.MbfPerAcre = origStand.MbfPerAcre;
            newStand.Meridian = origStand.Meridian;
            newStand.NetGeographicAcres = origStand.NetGeographicAcres;
            newStand.NonStocked = origStand.NonStocked;
            newStand.NonTimbered = origStand.NonTimbered;
            //newStand.PermPlotDate1 = origStand.PermPlotDate1;
            //newStand.PermPlotDate10 = origStand.PermPlotDate10;
            //newStand.PermPlotDate2 = origStand.PermPlotDate2;
            //newStand.PermPlotDate3 = origStand.PermPlotDate3;
            //newStand.PermPlotDate4 = origStand.PermPlotDate4;
            //newStand.PermPlotDate5 = origStand.PermPlotDate5;
            //newStand.PermPlotDate6 = origStand.PermPlotDate6;
            //newStand.PermPlotDate7 = origStand.PermPlotDate7;
            //newStand.PermPlotDate8 = origStand.PermPlotDate8;
            //newStand.PermPlotDate9 = origStand.PermPlotDate9;
            //newStand.PermPlotInterval1 = origStand.PermPlotInterval1;
            //newStand.PermPlotInterval10 = origStand.PermPlotInterval10;
            //newStand.PermPlotInterval2 = origStand.PermPlotInterval2;
            //newStand.PermPlotInterval3 = origStand.PermPlotInterval3;
            //newStand.PermPlotInterval4 = origStand.PermPlotInterval4;
            //newStand.PermPlotInterval5 = origStand.PermPlotInterval5;
            //newStand.PermPlotInterval6 = origStand.PermPlotInterval6;
            //newStand.PermPlotInterval7 = origStand.PermPlotInterval7;
            //newStand.PermPlotInterval8 = origStand.PermPlotInterval8;
            //newStand.PermPlotInterval9 = origStand.PermPlotInterval9;
            //newStand.PermPlotOccasion1 = origStand.PermPlotOccasion1;
            //newStand.PermPlotOccasion10 = origStand.PermPlotOccasion10;
            //newStand.PermPlotOccasion2 = origStand.PermPlotOccasion2;
            //newStand.PermPlotOccasion3 = origStand.PermPlotOccasion3;
            //newStand.PermPlotOccasion4 = origStand.PermPlotOccasion4;
            //newStand.PermPlotOccasion5 = origStand.PermPlotOccasion5;
            //newStand.PermPlotOccasion6 = origStand.PermPlotOccasion6;
            //newStand.PermPlotOccasion7 = origStand.PermPlotOccasion7;
            //newStand.PermPlotOccasion8 = origStand.PermPlotOccasion8;
            //newStand.PermPlotOccasion9 = origStand.PermPlotOccasion9;
            newStand.Plots = origStand.Plots;
            //newStand.Plot1 = origStand.Plot1;
            newStand.PlotsMustBeEntered = origStand.PlotsMustBeEntered;
            newStand.PriceTableName = origStand.PriceTableName;
            newStand.ProjectsId = p;
            newStand.QmDbh = origStand.QmDbh;
            newStand.SiteIndex = origStand.SiteIndex;
            newStand.SiteIndex1 = origStand.SiteIndex1;
            newStand.SiteIndex2 = origStand.SiteIndex2;
            newStand.SiteIndex3 = origStand.SiteIndex3;
            newStand.SiteIndex4 = origStand.SiteIndex4;
            newStand.SiteIndexBark = origStand.SiteIndexBark;
            newStand.SortTableName = origStand.SortTableName;
            newStand.Source = origStand.Source;
            newStand.Species11 = origStand.Species11;
            newStand.Species12 = origStand.Species12;
            newStand.Species13 = origStand.Species13;
            newStand.Species21 = origStand.Species21;
            newStand.Species22 = origStand.Species22;
            newStand.Species23 = origStand.Species23;
            newStand.Species31 = origStand.Species31;
            newStand.Species32 = origStand.Species32;
            newStand.Species33 = origStand.Species33;
            newStand.Species41 = origStand.Species41;
            newStand.Species42 = origStand.Species42;
            newStand.Species43 = origStand.Species43;
            newStand.SpeciesTableName = origStand.SpeciesTableName;
            newStand.StandAdjustmentsTableName = origStand.StandAdjustmentsTableName;
            newStand.StandGrowthProjectionDate = origStand.StandGrowthProjectionDate;
            newStand.State = origStand.State;
            newStand.Strip1 = origStand.Strip1;
            newStand.Strip2 = origStand.Strip2;
            newStand.Strip3 = origStand.Strip3;
            newStand.Strip4 = origStand.Strip4;
            newStand.Strip5 = origStand.Strip5;
            newStand.StripBlowup1 = origStand.StripBlowup1;
            newStand.StripBlowup2 = origStand.StripBlowup2;
            newStand.StripBlowup3 = origStand.StripBlowup3;
            newStand.StripBlowup4 = origStand.StripBlowup4;
            newStand.StripBlowup5 = origStand.StripBlowup5;
            newStand.StripWidth1 = origStand.StripWidth1;
            newStand.StripWidth2 = origStand.StripWidth2;
            newStand.StripWidth3 = origStand.StripWidth3;
            newStand.StripWidth4 = origStand.StripWidth4;
            newStand.StripWidth5 = origStand.StripWidth5;
            newStand.SureToBeMeasured = origStand.SureToBeMeasured;
            newStand.TonsPerAcre = origStand.TonsPerAcre;
            newStand.TotalBdFtGross = origStand.TotalBdFtGross;
            newStand.TotalBdFtNet = origStand.TotalBdFtNet;
            newStand.TotalCcf = origStand.TotalCcf;
            newStand.TotalCuFtGross = origStand.TotalCuFtGross;
            newStand.TotalCuFtMerch = origStand.TotalCuFtMerch;
            newStand.TotalCuFtNet = origStand.TotalCuFtNet;
            newStand.TotalMbf = origStand.TotalMbf;
            newStand.TotalTons = origStand.TotalTons;
            newStand.TractGroup = item.NewTractName;
            newStand.TransportationsTableName = origStand.TransportationsTableName;
            newStand.TreesPerAcre = origStand.TreesPerAcre;
            newStand.UseStandDbh = origStand.UseStandDbh;
            newStand.VigorDisplayCode = origStand.VigorDisplayCode;
        }

        private void CopyNewStandInput(StandInput siItem, ref StandInput newStandInput, int p)
        {
            newStandInput.Age = siItem.Age;
            newStandInput.BasalArea = siItem.BasalArea;
            newStandInput.BirthYear = siItem.BirthYear;
            newStandInput.Cost = siItem.Cost;
            newStandInput.D4H = siItem.D4H;
            newStandInput.DcHeight = siItem.DcHeight;
            newStandInput.GrossBdFt = siItem.GrossBdFt;
            newStandInput.GrossCuFt = siItem.GrossCuFt;
            newStandInput.LogsPerAcre = siItem.LogsPerAcre;
            newStandInput.NetAcres = siItem.NetAcres;
            newStandInput.NetBdFt = siItem.NetBdFt;
            newStandInput.NetCuFt = siItem.NetCuFt;
            newStandInput.OrigAge = siItem.OrigAge;
            newStandInput.OrigBasalArea = siItem.OrigBasalArea;
            newStandInput.OrigD4H = siItem.OrigD4H;
            newStandInput.OrigDcHeight = siItem.OrigDcHeight;
            newStandInput.OrigGrossBdFt = siItem.OrigGrossBdFt;
            newStandInput.OrigGrossCuFt = siItem.OrigGrossCuFt;
            newStandInput.OrigLogDib = siItem.OrigLogDib;
            newStandInput.OrigLogLen = siItem.OrigLogLen;
            newStandInput.OrigLogsPerAcre = siItem.OrigLogsPerAcre;
            newStandInput.OrigNetBdFt = siItem.OrigNetBdFt;
            newStandInput.OrigNetCuFt = siItem.OrigNetCuFt;
            newStandInput.OrigStocking = siItem.OrigStocking;
            newStandInput.OrigTotalHeight = siItem.OrigTotalHeight;
            newStandInput.OrigTreesPerAcre = siItem.OrigTreesPerAcre;
            newStandInput.PoleSeq = siItem.PoleSeq;
            newStandInput.Revenue = siItem.Revenue;
            newStandInput.SiteIndex = siItem.SiteIndex;
            newStandInput.Species = siItem.Species;
            newStandInput.StandsId = p;
            newStandInput.Status = siItem.Status;
            newStandInput.Stocking = siItem.Stocking;
            newStandInput.TotalHeight = siItem.TotalHeight;
            newStandInput.TreesPerAcre = siItem.TreesPerAcre;
        }
        
        private void CopyNewSegment(Treesegment segItem, ref Treesegment newSegment, int pTree, int pPlotId, string pPlotNumber, int pStand)
        {
            newSegment.Age = segItem.Age;
            newSegment.Ao = segItem.Ao;
            newSegment.Bark = segItem.Bark;
            newSegment.BdFtDd = segItem.BdFtDd;
            newSegment.BdFtLd = segItem.BdFtLd;
            newSegment.BdFtPd = segItem.BdFtPd;
            newSegment.BoleHeight = segItem.BoleHeight;
            newSegment.CalcAccumulatedLength = segItem.CalcAccumulatedLength;
            newSegment.CalcButtDia = segItem.CalcButtDia;
            newSegment.CalcLen = segItem.CalcLen;
            newSegment.CalcTopDia = segItem.CalcTopDia;
            newSegment.CalcTotalHtUsedYn = segItem.CalcTotalHtUsedYn;
            newSegment.Ccf = segItem.Ccf;
            newSegment.Comments = segItem.Comments;
            newSegment.CrownPosition = segItem.CrownPosition;
            newSegment.CrownRatio = segItem.CrownRatio;
            newSegment.CubicGrossVolume = segItem.CubicGrossVolume;
            newSegment.CubicGrossVolumePerAce = segItem.CubicGrossVolumePerAce;
            newSegment.CubicNetVolume = segItem.CubicNetVolume;
            newSegment.CubicNetVolumePerAce = segItem.CubicNetVolumePerAce;
            newSegment.CuFtDd = segItem.CuFtDd;
            newSegment.CuFtLd = segItem.CuFtLd;
            newSegment.Damage = segItem.Damage;
            newSegment.Dbh = segItem.Dbh;
            newSegment.FormFactor = segItem.FormFactor;
            newSegment.FormPoint = segItem.FormPoint;
            newSegment.GradeCode = segItem.GradeCode;
            newSegment.GroupPlotNumber = segItem.GroupPlotNumber;
            newSegment.GroupTreeNumber = segItem.GroupTreeNumber;
            newSegment.Length = segItem.Length;
            newSegment.LogValue = segItem.LogValue;
            newSegment.Mbf = segItem.Mbf;
            newSegment.PlotFactorInput = segItem.PlotFactorInput;
            newSegment.PlotId = pPlotId;
            newSegment.PlotNumber = pPlotNumber;
            newSegment.ScribnerGrossVolume = segItem.ScribnerGrossVolume;
            newSegment.ScribnerGrossVolumePerAce = segItem.ScribnerGrossVolumePerAce;
            newSegment.ScribnerNetVolume = segItem.ScribnerNetVolume;
            newSegment.ScribnerNetVolumePerAce = segItem.ScribnerNetVolumePerAce;
            newSegment.SegmentNumber = segItem.SegmentNumber;
            newSegment.SortCode = segItem.SortCode;
            newSegment.Species = segItem.Species;
            newSegment.StandsId = pStand;
            newSegment.Tdf = segItem.Tdf;
            newSegment.TotalHeight = segItem.TotalHeight;
            newSegment.TreeBasalArea = segItem.TreeBasalArea;
            newSegment.TreeCount = segItem.TreeCount;
            newSegment.TreeNumber = segItem.TreeNumber;
            newSegment.TreesId = pTree;
            newSegment.TreeStatusDisplayCode = segItem.TreeStatusDisplayCode;
            newSegment.TreeTreesPerAcre = segItem.TreeTreesPerAcre;
            newSegment.UserDefined = segItem.UserDefined;
            newSegment.Vigor = segItem.Vigor;
            newSegment.TreeType = segItem.TreeType;
        }

        private void CopyNewTree(Tree treeItem, ref Tree newTree, int pPlotId, string pPlotNumber, int pStand)
        {
            newTree.Age = treeItem.Age;
            newTree.AgeCode = treeItem.AgeCode;
            newTree.Ao = treeItem.Ao;
            newTree.Bark = treeItem.Bark;
            newTree.BasalArea = treeItem.BasalArea;
            newTree.BoleHeight = treeItem.BoleHeight;
            newTree.CalcTotalHeight = treeItem.CalcTotalHeight;
            newTree.CalcTotalHtUsedYn = treeItem.CalcTotalHtUsedYn;
            newTree.CrownPositionDisplayCode = treeItem.CrownPositionDisplayCode;
            newTree.CrownRatioDisplayCode = treeItem.CrownRatioDisplayCode;
            newTree.DamageDisplayCode = treeItem.DamageDisplayCode;
            newTree.Dbh = treeItem.Dbh;
            newTree.FormPoint = treeItem.FormPoint;
            newTree.GroupPlot = treeItem.GroupPlot;
            newTree.GroupTree = treeItem.GroupTree;
            newTree.LogsPerAcre = treeItem.LogsPerAcre;
            newTree.PfValue = treeItem.PfValue;
            newTree.PfType = treeItem.PfType;
            newTree.PlotFactorInput = treeItem.PlotFactorInput;
            newTree.PlotId = pPlotId;
            newTree.PlotNumber = pPlotNumber;
            newTree.ReportedFormFactor = treeItem.ReportedFormFactor;
            newTree.SpeciesAbbreviation = treeItem.SpeciesAbbreviation;
            newTree.StandsId = pStand;
            newTree.TopDiameterFractionCode = treeItem.TopDiameterFractionCode;
            newTree.TotalBdFtGrossVolume = treeItem.TotalBdFtGrossVolume;
            newTree.TotalBdFtNetVolume = treeItem.TotalBdFtNetVolume;
            newTree.TotalCcf = treeItem.TotalCcf;
            newTree.TotalCuFtGrossVolume = treeItem.TotalCuFtGrossVolume;
            newTree.TotalCuFtNetVolume = treeItem.TotalCuFtNetVolume;
            newTree.TotalGrossCubicPerAcre = treeItem.TotalGrossCubicPerAcre;
            newTree.TotalGrossScribnerPerAcre = treeItem.TotalGrossScribnerPerAcre;
            newTree.TotalHeight = treeItem.TotalHeight;
            newTree.TotalMbf = treeItem.TotalMbf;
            newTree.TotalNetCubicPerAcre = treeItem.TotalNetCubicPerAcre;
            newTree.TotalNetScribnerPerAcre = treeItem.TotalNetScribnerPerAcre;
            newTree.TotalTonsPerAcre = treeItem.TotalTonsPerAcre;
            newTree.TreeCount = treeItem.TreeCount;
            newTree.TreeNumber = treeItem.TreeNumber;
            newTree.TreesPerAcre = treeItem.TreesPerAcre;
            newTree.TreeStatusDisplayCode = treeItem.TreeStatusDisplayCode;
            newTree.TreeType = treeItem.TreeType;
            newTree.UserDefinedDisplayCode = treeItem.UserDefinedDisplayCode;
            newTree.VigorDisplayCode = treeItem.VigorDisplayCode;

            newTree.BdFtDd1 = treeItem.BdFtDd1;
            newTree.BdFtLd1 = treeItem.BdFtLd1;
            newTree.BdFtPd1 = treeItem.BdFtPd1;
            newTree.CuFtDd1 = treeItem.CuFtDd1;
            newTree.CuFtLd1 = treeItem.CuFtLd1;
            newTree.Grade1 = treeItem.Grade1;
            newTree.Length1 = treeItem.Length1;
            newTree.Sort1 = treeItem.Sort1;

            newTree.BdFtDd2 = treeItem.BdFtDd2;
            newTree.BdFtLd2 = treeItem.BdFtLd2;
            newTree.BdFtPd2 = treeItem.BdFtPd2;
            newTree.CuFtDd2 = treeItem.CuFtDd2;
            newTree.CuFtLd2 = treeItem.CuFtLd2;
            newTree.Grade2 = treeItem.Grade2;
            newTree.Length2 = treeItem.Length2;
            newTree.Sort2 = treeItem.Sort2;

            newTree.BdFtDd3 = treeItem.BdFtDd3;
            newTree.BdFtLd3 = treeItem.BdFtLd3;
            newTree.BdFtPd3 = treeItem.BdFtPd3;
            newTree.CuFtDd3 = treeItem.CuFtDd3;
            newTree.CuFtLd3 = treeItem.CuFtLd3;
            newTree.Grade3 = treeItem.Grade3;
            newTree.Length3 = treeItem.Length3;
            newTree.Sort3 = treeItem.Sort3;

            newTree.BdFtDd4 = treeItem.BdFtDd4;
            newTree.BdFtLd4 = treeItem.BdFtLd4;
            newTree.BdFtPd4 = treeItem.BdFtPd4;
            newTree.CuFtDd4 = treeItem.CuFtDd4;
            newTree.CuFtLd4 = treeItem.CuFtLd4;
            newTree.Grade4 = treeItem.Grade4;
            newTree.Length4 = treeItem.Length4;
            newTree.Sort4 = treeItem.Sort4;

            newTree.BdFtDd5 = treeItem.BdFtDd5;
            newTree.BdFtLd5 = treeItem.BdFtLd5;
            newTree.BdFtPd5 = treeItem.BdFtPd5;
            newTree.CuFtDd5 = treeItem.CuFtDd5;
            newTree.CuFtLd5 = treeItem.CuFtLd5;
            newTree.Grade5 = treeItem.Grade5;
            newTree.Length5 = treeItem.Length5;
            newTree.Sort5 = treeItem.Sort5;

            newTree.BdFtDd6 = treeItem.BdFtDd6;
            newTree.BdFtLd6 = treeItem.BdFtLd6;
            newTree.BdFtPd6 = treeItem.BdFtPd6;
            newTree.CuFtDd6 = treeItem.CuFtDd6;
            newTree.CuFtLd6 = treeItem.CuFtLd6;
            newTree.Grade6 = treeItem.Grade6;
            newTree.Length6 = treeItem.Length6;
            newTree.Sort6 = treeItem.Sort6;

            newTree.BdFtDd7 = treeItem.BdFtDd7;
            newTree.BdFtLd7 = treeItem.BdFtLd7;
            newTree.BdFtPd7 = treeItem.BdFtPd7;
            newTree.CuFtDd7 = treeItem.CuFtDd7;
            newTree.CuFtLd7 = treeItem.CuFtLd7;
            newTree.Grade7 = treeItem.Grade7;
            newTree.Length7 = treeItem.Length7;
            newTree.Sort7 = treeItem.Sort7;

            newTree.BdFtDd8 = treeItem.BdFtDd8;
            newTree.BdFtLd8 = treeItem.BdFtLd8;
            newTree.BdFtPd8 = treeItem.BdFtPd8;
            newTree.CuFtDd8 = treeItem.CuFtDd8;
            newTree.CuFtLd8 = treeItem.CuFtLd8;
            newTree.Grade8 = treeItem.Grade8;
            newTree.Length8 = treeItem.Length8;
            newTree.Sort8 = treeItem.Sort8;

            newTree.BdFtDd9 = treeItem.BdFtDd9;
            newTree.BdFtLd9 = treeItem.BdFtLd9;
            newTree.BdFtPd9 = treeItem.BdFtPd9;
            newTree.CuFtDd9 = treeItem.CuFtDd9;
            newTree.CuFtLd9 = treeItem.CuFtLd9;
            newTree.Grade9 = treeItem.Grade9;
            newTree.Length9 = treeItem.Length9;
            newTree.Sort9 = treeItem.Sort9;

            newTree.BdFtDd10 = treeItem.BdFtDd10;
            newTree.BdFtLd10 = treeItem.BdFtLd10;
            newTree.BdFtPd10 = treeItem.BdFtPd10;
            newTree.CuFtDd10 = treeItem.CuFtDd10;
            newTree.CuFtLd10 = treeItem.CuFtLd10;
            newTree.Grade10 = treeItem.Grade10;
            newTree.Length10 = treeItem.Length10;
            newTree.Sort10 = treeItem.Sort10;

            newTree.BdFtDd11 = treeItem.BdFtDd11;
            newTree.BdFtLd11 = treeItem.BdFtLd11;
            newTree.BdFtPd11 = treeItem.BdFtPd11;
            newTree.CuFtDd11 = treeItem.CuFtDd11;
            newTree.CuFtLd11 = treeItem.CuFtLd11;
            newTree.Grade11 = treeItem.Grade11;
            newTree.Length11 = treeItem.Length11;
            newTree.Sort11 = treeItem.Sort11;

            newTree.BdFtDd12 = treeItem.BdFtDd12;
            newTree.BdFtLd12 = treeItem.BdFtLd12;
            newTree.BdFtPd12 = treeItem.BdFtPd12;
            newTree.CuFtDd12 = treeItem.CuFtDd12;
            newTree.CuFtLd12 = treeItem.CuFtLd12;
            newTree.Grade12 = treeItem.Grade12;
            newTree.Length12 = treeItem.Length12;
            newTree.Sort12 = treeItem.Sort12;
        }
        
        private void CopyNewPlot(Plot plotItem, ref Plot newPlot, int pStand)
        {
            string aNewPlotNumber = GetNewPlotNumber(plotItem.UserPlotNumber);
            newPlot.AddFlag = plotItem.AddFlag;
            newPlot.Aspect = plotItem.Aspect;
            newPlot.Attachments = plotItem.Attachments;
            newPlot.BafInAt = plotItem.BafInAt;
            newPlot.BasalAreaAcre = plotItem.BasalAreaAcre;
            newPlot.CruFlag = plotItem.CruFlag;
            newPlot.Cruiser = plotItem.Cruiser;
            newPlot.DwFlag = plotItem.DwFlag;
            newPlot.Environmental = plotItem.Environmental;
            newPlot.Habitat = plotItem.Habitat;
            newPlot.NetBfAcre = plotItem.NetBfAcre;
            newPlot.NetCfAcre = plotItem.NetCfAcre;
            newPlot.Notes = plotItem.Notes;
            newPlot.PlotDate = plotItem.PlotDate;
            newPlot.RefFlag = plotItem.RefFlag;
            newPlot.Slope = plotItem.Slope;
            newPlot.Species = plotItem.Species;
            newPlot.StandsId = pStand;
            newPlot.TreatmentDisplayCode = plotItem.TreatmentDisplayCode;
            newPlot.TreesPerAcre = plotItem.TreesPerAcre;
            newPlot.UserPlotNumber = aNewPlotNumber;
            newPlot.X = plotItem.X;
            newPlot.Y = plotItem.Y;
            newPlot.Z = plotItem.Z;
        }

        private string GetNewPlotNumber(string plotItemUserPlotNumber)
        {
            string aPlot = string.Empty;
            List<TempCopyPlot> plots = dbContext.TempCopyPlots.ToList();

            foreach (var item in plots)
            {
                if (item.OrigPlot1 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot1;
                    break;
                }
                if (item.OrigPlot2 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot2;
                    break;
                }
                if (item.OrigPlot3 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot3;
                    break;
                }
                if (item.OrigPlot4 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot4;
                    break;
                }
                if (item.OrigPlot5 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot5;
                    break;
                }
                if (item.OrigPlot6 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot6;
                    break;
                }
                if (item.OrigPlot7 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot7;
                    break;
                }
                if (item.OrigPlot8 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot8;
                    break;
                }
                if (item.OrigPlot9 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot9;
                    break;
                }
                if (item.OrigPlot10 == plotItemUserPlotNumber)
                {
                    aPlot = item.Plot10;
                    break;
                }
            }
            return aPlot;
        }

        private bool DoStandsExist(ref ProjectDbContext ctx)
        {
            bool doStandsExist = false;
            List<TempCopyStand> coll = dbContext.TempCopyStands.ToList();
            foreach (var item in coll)
            {
                if (ctx.Stands.Any(s => s.TractName == item.NewTractName && s.StandName == item.NewStandName && s.Township == item.NewTownship && s.Range == item.NewRange && s.Section == item.NewSection))
                    doStandsExist = true;
            }
            return doStandsExist;
        }

        private bool DoesStandExist(ref ProjectDbContext ctx, TempCopyStand item)
        {
            return (ctx.Stands.Any(s => s.TractName == item.NewTractName && s.StandName == item.NewStandName && s.Township == item.NewTownship && s.Range == item.NewRange && s.Section == item.NewSection));
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void cbDestinationProject_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbDestinationProject.Text))
            {
                tbDestinationFolder.Text = Properties.Settings.Default["DbLocation"].ToString();
                if (!File.Exists(string.Format("{0}{1}.sdf", tbDestinationFolder.Text, cbDestinationProject.Text)))
                {
                    DialogResult result = XtraMessageBox.Show("Project does not exist. Would you like to add a new Project?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (result == DialogResult.Yes)
                    {
                        AddNewProject(tbDestinationFolder.Text, cbDestinationProject.Text);
                    }
                    else
                    {
                        cbDestinationProject.Focus();
                        return;
                    }
                }
                _projectDataSource = string.Format("{0}{1}.sdf", tbDestinationFolder.Text, cbDestinationProject.Text);
                ctx = new ProjectDbContext("Data Source = " + _projectDataSource);

                if (!ctx.Stands.Any(s => s.TractName == tbTract.Text && s.StandName == tbStand.Text && s.Township == tbTownship.Text && s.Range == tbRange.Text && s.Section == tbSection.Text))
                {
                    DialogResult result = XtraMessageBox.Show("Stand does not exist. Do you want to create the Stand", "Status", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        Stand newRec = new Stand();

                        newRec.ProjectsId = origStand.ProjectsId;
                        newRec.TractGroup = tbTract.Text;
                        newRec.TractName = tbTract.Text;
                        newRec.StandName = tbStand.Text;
                        newRec.Township = tbTownship.Text;
                        newRec.Range = tbRange.Text;
                        newRec.Section = tbSection.Text;

                        newRec.State = origStand.State;
                        newRec.County = origStand.County;
                        newRec.NetGeographicAcres = origStand.NetGeographicAcres;

                        newRec.DateOfStandData = origStand.DateOfStandData;
                        newRec.GrownToDate = origStand.GrownToDate;

                        newRec.AutoSegmentLength = origStand.AutoSegmentLength;

                        newRec.Harvest = origStand.Harvest;

                        newRec.Species11 = origStand.Species11;
                        newRec.Species12 = origStand.Species12;
                        newRec.Species13 = origStand.Species13;
                        newRec.SiteIndex1 = origStand.SiteIndex1;

                        newRec.Species21 = origStand.Species21;
                        newRec.Species22 = origStand.Species22;
                        newRec.Species23 = origStand.Species23;
                        newRec.SiteIndex2 = origStand.SiteIndex2;

                        newRec.Species31 = origStand.Species31;
                        newRec.Species32 = origStand.Species32;
                        newRec.Species33 = origStand.Species33;
                        newRec.SiteIndex3 = origStand.SiteIndex3;

                        newRec.Species41 = origStand.Species41;
                        newRec.Species42 = origStand.Species42;
                        newRec.Species43 = origStand.Species43;
                        newRec.SiteIndex4 = origStand.SiteIndex4;

                        //newRec.Age1 = origStand.Age1;
                        //newRec.Age2 = origStand.Age2;
                        //newRec.Age3 = origStand.Age3;
                        //newRec.Age4 = origStand.Age4;
                        //newRec.Age5 = origStand.Age5;
                        //newRec.Age6 = origStand.Age6;
                        //newRec.Age7 = origStand.Age7;
                        //newRec.Age8 = origStand.Age8;
                        //newRec.Age9 = origStand.Age9;

                        newRec.Baf1 = origStand.Baf1;
                        newRec.Baf2 = origStand.Baf2;
                        newRec.Baf3 = origStand.Baf3;
                        newRec.Baf4 = origStand.Baf4;
                        newRec.Baf5 = origStand.Baf5;

                        newRec.AreaCode1 = origStand.AreaCode1;
                        newRec.AreaPlotAcres1 = origStand.AreaPlotAcres1;
                        newRec.AreaPlotRadius1 = origStand.AreaPlotRadius1;
                        newRec.AreaSide11 = origStand.AreaSide11;
                        newRec.AreaSide12 = origStand.AreaSide12;
                        newRec.AreaBlowup1 = origStand.AreaBlowup1;

                        newRec.AreaCode2 = origStand.AreaCode2;
                        newRec.AreaPlotAcres2 = origStand.AreaPlotAcres2;
                        newRec.AreaPlotRadius2 = origStand.AreaPlotRadius2;
                        newRec.AreaSide21 = origStand.AreaSide21;
                        newRec.AreaSide22 = origStand.AreaSide22;
                        newRec.AreaBlowup2 = origStand.AreaBlowup2;

                        newRec.AreaCode3 = origStand.AreaCode3;
                        newRec.AreaPlotAcres3 = origStand.AreaPlotAcres3;
                        newRec.AreaPlotRadius3 = origStand.AreaPlotRadius3;
                        newRec.AreaSide31 = origStand.AreaSide31;
                        newRec.AreaSide32 = origStand.AreaSide32;
                        newRec.AreaBlowup3 = origStand.AreaBlowup3;

                        newRec.AreaCode4 = origStand.AreaCode4;
                        newRec.AreaPlotAcres4 = origStand.AreaPlotAcres4;
                        newRec.AreaPlotRadius4 = origStand.AreaPlotRadius4;
                        newRec.AreaSide41 = origStand.AreaSide41;
                        newRec.AreaSide42 = origStand.AreaSide42;
                        newRec.AreaBlowup4 = origStand.AreaBlowup4;

                        newRec.AreaCode5 = this.origStand.AreaCode5;
                        newRec.AreaPlotAcres5 = origStand.AreaPlotAcres5;
                        newRec.AreaPlotRadius5 = origStand.AreaPlotRadius5;
                        newRec.AreaSide51 = origStand.AreaSide51;
                        newRec.AreaSide52 = origStand.AreaSide52;
                        newRec.AreaBlowup5 = origStand.AreaBlowup5;

                        newRec.Strip1 = origStand.Strip1;
                        newRec.StripWidth1 = origStand.StripWidth1;
                        newRec.StripBlowup1 = origStand.StripBlowup1;
                        newRec.Strip2 = origStand.Strip2;
                        newRec.StripWidth2 = origStand.StripWidth2;
                        newRec.StripBlowup2 = origStand.StripBlowup2;
                        newRec.Strip3 = origStand.Strip3;
                        newRec.StripWidth3 = origStand.StripWidth3;
                        newRec.StripBlowup3 = origStand.StripBlowup3;
                        newRec.Strip4 = origStand.Strip4;
                        newRec.StripWidth4 = origStand.StripWidth4;
                        newRec.StripBlowup4 = origStand.StripBlowup4;
                        newRec.Strip5 = origStand.Strip5;
                        newRec.StripWidth5 = origStand.StripWidth5;
                        newRec.StripBlowup5 = origStand.StripBlowup5;

                        newRec.SpeciesTableName = origStand.SpeciesTableName;
                        newRec.SortTableName = origStand.SortTableName;
                        newRec.GradeTableName = origStand.GradeTableName;
                        newRec.PriceTableName = origStand.PriceTableName;
                        newRec.CostTableName = origStand.CostTableName;
                        newRec.NonStocked = origStand.NonStocked;
                        newRec.NonTimbered = origStand.NonTimbered;

                        if (string.IsNullOrEmpty(newRec.NonStocked) && string.IsNullOrEmpty(newRec.NonTimbered))
                            newRec.LandClass = "TIM";
                        else
                            if (!string.IsNullOrEmpty(newRec.NonStocked))
                            newRec.LandClass = "NS";
                        else
                                if (!string.IsNullOrEmpty(newRec.NonTimbered))
                            newRec.LandClass = "NT";

                        ctx.Stands.Add(newRec);
                        ctx.SaveChanges();
                    }
                    else
                        return;
                }

                if (CheckForExistingPlots(ref ctx))
                    LoadData();
            }
        }

        private void AddNewProject(string pDestinationFolder, string pDestinationProject)
        {
            string dblocation = Properties.Settings.Default["DbLocation"].ToString();

            try
            {
                File.Copy(string.Format("{0}Empty.sdf", dblocation), string.Format("{0}{1}.sdf", pDestinationFolder, pDestinationProject));
            }
            catch
            {
                MessageBox.Show("Problem with the Project Location", "Error");
                tbDestinationFolder.Focus();
                return;
            }

            ProjectDbContext newDb = new ProjectDbContext(string.Format("Data Source = {0}{1}.sdf", pDestinationFolder, pDestinationProject));
            Project.DAL.Project newProject = newDb.Projects.FirstOrDefault();

            try
            {
                newProject.ProjectName = pDestinationProject;
                newProject.ProjectNumber = string.Empty;
                newProject.GisProjectName = string.Empty;
                newProject.ProjectLead = string.Empty;
                newProject.SaleName = string.Empty;
                newProject.ProjectNotes = string.Empty;
                string dt = DateTime.Now.ToShortDateString();
                newProject.ProjectDate = Convert.ToDateTime(dt);

                newDb.SaveChanges();

                newDb.Dispose();
            }
            catch (EntityCommandExecutionException ex)
            {
                MessageBox.Show(ex.Message +
                System.Environment.NewLine + "Exception message:" +
                System.Environment.NewLine + ex.InnerException,
                "Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }

            Projects.DAL.AllProject newProjectLocation = new Projects.DAL.AllProject();
            newProjectLocation.ProjectName = pDestinationProject;
            if (pDestinationFolder.EndsWith("\\"))
                newProjectLocation.ProjectLocation = pDestinationFolder;
            else
                newProjectLocation.ProjectLocation = pDestinationFolder + "\\";

            try
            {
                ProjectsBLL.SaveProjectLocation(ref locContext, newProjectLocation);
            }
            catch (Exception ex)
            {
                this.HandleExceptionOnSaveChanges(ex.Message);
                return;
            }
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        private void StandCopyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
                dbContext = null;
            }
        }

        private void cbDestinationProject_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void bandedGridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            BandedGridView view = sender as BandedGridView;
            switch (e.Column.FieldName)
            {
                case "Plot1":
                    bool p1Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot1Exist"]));
                    if (p1Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot2":
                    bool p2Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot2Exist"]));
                    if (p2Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot3":
                    bool p3Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot3Exist"]));
                    if (p3Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot4":
                    bool p4Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot4Exist"]));
                    if (p4Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot5":
                    bool p5Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot5Exist"]));
                    if (p5Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot6":
                    bool p6Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot6Exist"]));
                    if (p6Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot7":
                    bool p7Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot7Exist"]));
                    if (p7Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot8":
                    bool p8Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot8Exist"]));
                    if (p8Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot9":
                    bool p9Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot9Exist"]));
                    if (p9Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "Plot10":
                    bool p10Exist = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["Plot10Exist"]));
                    if (p10Exist)
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
            }
        }

        private void bandedGridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            BandedGridView view = sender as BandedGridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "Plot1":
                case "Plot2":
                case "Plot3":
                case "Plot4":
                case "Plot5":
                case "Plot6":
                case "Plot7":
                case "Plot8":
                case "Plot9":
                case "Plot10":
                    Stand rec = ctx.Stands.Where(s => s.TractName == tbTract.Text && s.StandName == tbStand.Text && s.Township == tbTownship.Text && s.Range == tbRange.Text && s.Section == tbSection.Text).FirstOrDefault();
                    if (ctx.Plots.Any(p => p.StandsId == rec.StandsId && p.UserPlotNumber == e.Value.ToString()))
                    {
                        e.Valid = false;
                        e.ErrorText = "Plot Number already exists";
                        view.SetRowCellValue(view.FocusedRowHandle, view.FocusedColumn.FieldName +  "Exist", true);
                    }
                    else
                    {
                        e.Valid = true;
                        e.ErrorText = string.Empty;
                        view.SetRowCellValue(view.FocusedRowHandle, view.FocusedColumn.FieldName + "Exist", false);
                    }
                    break;
            }
        }

        private void PlotCopyForm_Shown(object sender, EventArgs e)
        {
            tbTract.Focus();
        }

        private void tbTract_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnableDestinationProject();
        }

        private void EnableDestinationProject()
        {
            bool enable = false;
            if (!string.IsNullOrEmpty(tbTract.Text) && !string.IsNullOrEmpty(tbStand.Text) &&
                !string.IsNullOrEmpty(tbTownship.Text) && !string.IsNullOrEmpty(tbRange.Text) &&
                !string.IsNullOrEmpty(tbSection.Text))
            {
                cbDestinationProject.Enabled = true;
                tbDestinationFolder.Enabled = true;
            }
            else
            {
                cbDestinationProject.Enabled = false;
                tbDestinationFolder.Enabled = false;
            }
        }

        private void tbStand_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnableDestinationProject();
        }

        private void tbTownship_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnableDestinationProject();
        }

        private void tbRange_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnableDestinationProject();
        }

        private void tbSection_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnableDestinationProject();
        }

        private void bandedGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {
                if (XtraMessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;
                int[] rowHandles = bandedGridView1.GetSelectedRows();
                for (int i = 0; i < rowHandles.Length; i++)
                {
                    tempCopyPlotsBindingSource.Position = rowHandles[i];
                    TempCopyPlot rec = tempCopyPlotsBindingSource.Current as TempCopyPlot;
                    dbContext.TempCopyPlots.Remove(rec);
                }
                dbContext.SaveChanges();

                LoadData();
            }
            if (e.KeyCode == Keys.Delete)
            {
                if (XtraMessageBox.Show("Clear Cells?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;
                bandedGridView1.BeginDataUpdate();
                GridCell[] cells = bandedGridView1.GetSelectedCells();
                foreach (GridCell cell in cells)
                {
                    try
                    {
                        bandedGridView1.SetRowCellValue(cell.RowHandle, cell.Column, string.Empty);
                    }
                    // ReSharper disable EmptyGeneralCatchClause
                    catch
                    // ReSharper restore EmptyGeneralCatchClause
                    {
                        //ignore any exceptions
                    }
                }
                bandedGridView1.EndDataUpdate();
            }
        }

        private void tbTract_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 12)
                tbStand.Focus();
        }

        private void tbStand_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 4)
                tbTownship.Focus();
        }

        private void tbTownship_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 3)
                tbRange.Focus();
        }

        private void tbRange_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 3)
                tbSection.Focus();
        }

        private void tbSection_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 2)
            {
                EnableDestinationProject();
                cbDestinationProject.Focus();
            }
        }

        private void tbDestinationFolder_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = Properties.Settings.Default["DbLocation"].ToString();
            frm.ShowDialog();

            if (!string.IsNullOrEmpty(frm.SelectedPath))
            {
                tbDestinationFolder.Text = frm.SelectedPath + "\\";
            }
            frm.Dispose();
        }

        private void cbDestinationProject_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }
}