﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;
using System.IO;
using Projects.DAL;
using System.Data.Entity.Core;
using Project.DAL;
using System.Text.RegularExpressions;

namespace SuperACE
{
    public partial class ProjectAddForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext = null;
        private ProjectDbContext projectContext = null;

        public ProjectAddForm(ref ProjectDbContext pPrjCtx, ref ProjectsDbContext pLocCtx)
        {
            InitializeComponent();

            locContext = pLocCtx;
            projectContext = pPrjCtx;
        }

        private void ProjectAddForm_Load(object sender, EventArgs e)
        {
            //cbProjectName.ReadOnly = false;
            //tbProjectLocation.ReadOnly = false;
            simpleButton1.Visible = true;

            tbProjectLocation.Text = Properties.Settings.Default["DbLocation"].ToString(); // ConfigurationManager.AppSettings["dblocation"];
            PopulateProjectDropDown(tbProjectLocation.Text);
            this.Text = "Add Project";
            this.ProjectNameComboBoxEdit.Focus();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            projectsBindingSource.EndEdit();
            if (!CheckProperties())
                return;

            string dblocation = Properties.Settings.Default["DbLocation"].ToString(); // ConfigurationManager.AppSettings["dblocation"];

            if (File.Exists(string.Format("{0}\\{1}.sdf", tbProjectLocation.Text, this.ProjectNameComboBoxEdit.Text)))
            {
                if (ProjectsBLL.DoesProjectExist(ref locContext, this.ProjectNameComboBoxEdit.Text))
                {
                    XtraMessageBox.Show("Project already exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {

                try
                {
                    File.Copy(string.Format("{0}\\Empty.sdf", dblocation), string.Format("{0}\\{1}.sdf", tbProjectLocation.Text, ProjectNameComboBoxEdit.Text));
                    //File.Copy(string.Format("{0}Empty_log.ldf", dblocation), string.Format("{0}{1}_log.ldf", radTextBox63.Text, radTextBox57.Text));
                }
                catch
                {
                    MessageBox.Show("Problem with the Project Location", "Error");
                    tbProjectLocation.Focus();
                    return;
                }

                ProjectDbContext newDb = new ProjectDbContext(string.Format("Data Source = {0}\\{1}.sdf", tbProjectLocation.Text, ProjectNameComboBoxEdit.Text));
                Project.DAL.Project newProject = newDb.Projects.FirstOrDefault();

                try
                {
                    // get detail tables from ProgrammSettings
                    //ProjectLocationModel.PROGRAMSETTINGS ps = ProjectsBLL.GetProgramSettings(ref locContext);
                    //newProject.AspectsTableName = ps.DefAspectsTable;
                    //newProject.CrownPositionTableName = ps.DefCrownPositionTable;
                    //newProject.CruisersTableName = ps.DefCruisersTable;
                    //newProject.DamageTableName = ps.DefDamageTable;
                    //newProject.EnvironmentsTableName = ps.DefEnvironmentsTable;
                    //newProject.HarvestSystemsTableName = ps.DefHarvestSystemsTable;
                    //newProject.LandFormsTableName = ps.DefLandFormTable;
                    //newProject.ProjectAdjustmentsTableName = ps.DefProjectAdjustmentsTable;
                    //newProject.RoadsTableName = ps.DefRoadsTable;
                    //newProject.SeedZonesTableName = ps.DefSeedZonesTable;
                    //newProject.SlashDistributionsTableName = ps.DefSlashTable;
                    //newProject.SoilsTableName = ps.DefSoilsTable;
                    //newProject.SortsTableName = ps.DefSortsTable;
                    //newProject.GradesTableName = ps.DefGradesTable;
                    //newProject.SpeciesTableName = ps.DefSpeciesTable;
                    //newProject.StandActivitiesTableName = ps.DefStandActivitiesTable;
                    //newProject.StandDataSourcesTableName = "GENERAL";
                    //newProject.NonStockedTableName = ps.DefNonstockedTable;
                    //newProject.NonTimberedTableName = ps.DefNontimberedTable;
                    //newProject.StreamsTableName = ps.DefStreamsTable;
                    //newProject.TreatmentsTableName = ps.DefTreatmentsTable;
                    //newProject.TreeSourcesTableName = ps.DefTreeSourcesTable;
                    //newProject.TreeStatusTableName = ps.DefTreeStatusTable;
                    //newProject.UserDefinedTableName = ps.DefUserDefinedTable;
                    //newProject.VegetationsTableName = ps.DefVegetationTable;
                    //newProject.VigorTableName = ps.DefVigorTable;
                    //newProject.ComponentsTableName = ps.DefComponentsTable;
                    //newProject.PolesTableName = ps.DefPolesTable;
                    //newProject.PilingTableName = ps.DefPilingTable;
                    //newProject.PriceTableName = ps.DefPriceTable;
                    //newProject.CostTableName = ps.DefCostTable;

                    newProject.ProjectName = ProjectNameComboBoxEdit.Text;
                    newProject.ProjectNumber = string.IsNullOrEmpty(this.ProjectNumberTextEdit.Text.Trim()) ? string.Empty : this.ProjectNumberTextEdit.Text.Trim();
                    newProject.GisProjectName = string.IsNullOrEmpty(this.GISProjectNameTextEdit.Text.Trim()) ? string.Empty : this.GISProjectNameTextEdit.Text.Trim();
                    newProject.ProjectLead = string.IsNullOrEmpty(this.ProjectLeadTextEdit.Text.Trim()) ? string.Empty : this.ProjectLeadTextEdit.Text.Trim();
                    newProject.SaleName = string.IsNullOrEmpty(this.SaleNameTextEdit.Text.Trim()) ? string.Empty : this.SaleNameTextEdit.Text.Trim();
                    newProject.ProjectNotes = string.IsNullOrEmpty(this.ProjectNotesMemoEdit.Text.Trim()) ? string.Empty : this.ProjectNotesMemoEdit.Text.Trim();
                    if (!string.IsNullOrEmpty(this.ProjectDateDateEdit.Text.Trim()))
                        newProject.ProjectDate = Convert.ToDateTime(this.ProjectDateDateEdit.Text.Trim());

                    newDb.SaveChanges();

                    newDb.Dispose();
                }
                catch (EntityCommandExecutionException ex)
                {
                    MessageBox.Show(ex.Message +
                    System.Environment.NewLine + "Exception message:" +
                    System.Environment.NewLine + ex.InnerException,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                }
            }

            Projects.DAL.AllProject newProjectLocation = new Projects.DAL.AllProject();
            newProjectLocation.ProjectName = ProjectNameComboBoxEdit.Text;
            if (tbProjectLocation.Text.EndsWith("\\"))
                newProjectLocation.ProjectLocation = tbProjectLocation.Text;
            else
                newProjectLocation.ProjectLocation = tbProjectLocation.Text + "\\";
            //prjLocContext.Project.Add(newProjectLocation);

            try
            {
                ProjectsBLL.SaveProjectLocation(ref locContext, newProjectLocation);
                //ProjectsBLL.SetDefaultProject(ref locContext, newProjectLocation);
            }
            catch (Exception ex)
            {
                this.HandleExceptionOnSaveChanges(ex.Message);
                return;
            }

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private bool CheckProperties()
        {
            bool bReturn = true;

            if (string.IsNullOrEmpty(ProjectNameComboBoxEdit.Text.Trim()))
            {
                dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "Project Name is required");
                bReturn = false;
            }
            if (string.IsNullOrEmpty(tbProjectLocation.Text.Trim()))
            {
                dxErrorProvider1.SetError(tbProjectLocation, "Project Location is required");
                bReturn = false;
            }
            return bReturn;
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        //private void BindControlsToProject()
        //{
        //    cbProjectName.Text = MainForm.CurrentProjectLocation.ProjectName;
        //    tbProjectLocation.Text = MainForm.CurrentProjectLocation.ProjectLocation;

        //    tbProjectNumber.Text = MainForm.CurrentProject.ProjectNumber;
        //    tbProjectGISName.Text = MainForm.CurrentProject.GISProjectName;
        //    tbProjectLead.Text = MainForm.CurrentProject.ProjectLead;
        //    tbSaleName.Text = MainForm.CurrentProject.SaleName;
        //    tbProjectDate.Text = MainForm.CurrentProject.ProjectDate.ToString();
        //    memoProjectNotes.Text = MainForm.CurrentProject.ProjectNotes;

        //    tbCustomerName.Text = MainForm.CurrentProject.CustomerCompanyName;
        //    //tbCustomerOfficePhone.Text = MainForm.CurrentProject.CustomerContactName;
        //    tbCustomerOfficePhone.Text = MainForm.CurrentProject.CustomerOfficePhone;
        //    tbCustomerCellPhone.Text = MainForm.CurrentProject.CustomerCellPhone;
        //    tbCustomerFax.Text = MainForm.CurrentProject.CustomerFax;
        //    tbCustomerEMail.Text = MainForm.CurrentProject.CustomerEmail;
        //    tbCustomerAddress1.Text = MainForm.CurrentProject.CustomerAddress1;
        //    tbCustomerAddress2.Text = MainForm.CurrentProject.CustomerAddress2;
        //    tbCustomerCity.Text = MainForm.CurrentProject.CustomerCity;
        //    tbCustomerState.Text = MainForm.CurrentProject.CustomerState;
        //    tbCustomerZip.Text = MainForm.CurrentProject.CustomerPostalCode;
        //    memoCustomerNotes.Text = MainForm.CurrentProject.CustomerNotes;
        //}

        //private void SetProjectProperties(Project pProject)
        //{
        //    pProject.ProjectName = string.IsNullOrEmpty(cbProjectName.Text.Trim()) ? string.Empty : this.cbProjectName.Text.Trim();
        //    pProject.ProjectNumber = string.IsNullOrEmpty(tbProjectNumber.Text.Trim()) ? string.Empty : this.tbProjectNumber.Text.Trim();
        //    pProject.GISProjectName = string.IsNullOrEmpty(tbProjectGISName.Text.Trim()) ? string.Empty : this.tbProjectGISName.Text.Trim();
        //    pProject.ProjectLead = string.IsNullOrEmpty(tbProjectLead.Text.Trim()) ? string.Empty : this.tbProjectLead.Text.Trim();
        //    pProject.SaleName = string.IsNullOrEmpty(tbSaleName.Text.Trim()) ? string.Empty : this.tbSaleName.Text.Trim();
        //    //pProject.ProjectDate = string.IsNullOrEmpty(textBoxProjectDate.Text.Trim()) ? string.Empty : this.textBoxProjectDate.Text.Trim();
        //    pProject.ProjectNotes = string.IsNullOrEmpty(memoProjectNotes.Text.Trim()) ? string.Empty : this.memoProjectNotes.Text.Trim();

        //    pProject.CustomerCompanyName = string.IsNullOrEmpty(tbCustomerName.Text.Trim()) ? string.Empty : this.tbCustomerName.Text.Trim();
        //    //MainForm.CurrentProject.CustomerContactName = string.IsNullOrEmpty(radTextBox34.Text.Trim()) ? string.Empty : this.radTextBox34.Text.Trim();
        //    pProject.CustomerOfficePhone = string.IsNullOrEmpty(tbCustomerOfficePhone.Text.Trim()) ? string.Empty : this.tbCustomerOfficePhone.Text.Trim();
        //    pProject.CustomerCellPhone = string.IsNullOrEmpty(tbCustomerCellPhone.Text.Trim()) ? string.Empty : this.tbCustomerCellPhone.Text.Trim();
        //    pProject.CustomerFax = string.IsNullOrEmpty(tbCustomerFax.Text.Trim()) ? string.Empty : this.tbCustomerFax.Text.Trim();
        //    pProject.CustomerEmail = string.IsNullOrEmpty(tbCustomerEMail.Text.Trim()) ? string.Empty : this.tbCustomerEMail.Text.Trim();
        //    pProject.CustomerAddress1 = string.IsNullOrEmpty(tbCustomerAddress1.Text.Trim()) ? string.Empty : this.tbCustomerAddress1.Text.Trim();
        //    pProject.CustomerAddress2 = string.IsNullOrEmpty(tbCustomerAddress2.Text.Trim()) ? string.Empty : this.tbCustomerAddress2.Text.Trim();
        //    pProject.CustomerCity = string.IsNullOrEmpty(tbCustomerCity.Text.Trim()) ? string.Empty : this.tbCustomerCity.Text.Trim();
        //    pProject.CustomerState = string.IsNullOrEmpty(tbCustomerState.Text.Trim()) ? string.Empty : this.tbCustomerState.Text.Trim();
        //    pProject.CustomerPostalCode = string.IsNullOrEmpty(tbCustomerZip.Text.Trim()) ? string.Empty : this.tbCustomerZip.Text.Trim();
        //    pProject.CustomerNotes = string.IsNullOrEmpty(memoCustomerNotes.Text.Trim()) ? string.Empty : this.memoCustomerNotes.Text.Trim();
        //}

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.ShowDialog();
            tbProjectLocation.Text = string.Format("{0}\\", frm.SelectedPath);

            PopulateProjectDropDown(frm.SelectedPath);
            
            frm.Dispose();
        }

        private void PopulateProjectDropDown(string p)
        {
            string[] files = Directory.GetFiles(p, "*.sdf");
            foreach (var item in files)
            {
                string filename = Path.GetFileName(item).Replace(".sdf", "");
                if (!ProjectsBLL.DoesProjectExist(ref locContext, filename))
                {
                    switch (filename)
                    { 
                        case "Projects":
                        case "Empty":
                        case "Temp":
                            break;
                        default:
                            this.ProjectNameComboBoxEdit.Properties.Items.Add(filename);
                            break;
                    }
                }
            }
        }

        private void ProjectAddForm_Shown(object sender, EventArgs e)
        {
            ProjectNameComboBoxEdit.Focus();
        }

        private void ProjectNameComboBoxEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9-\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (string.IsNullOrEmpty(rtb.Text))
                {
                    e.Cancel = true;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                }
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                }
            }
        }

        private void buttonEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = Properties.Settings.Default["DbLocation"].ToString();
            frm.ShowDialog();

            if (!string.IsNullOrEmpty(frm.SelectedPath))
            {
                tbProjectLocation.Text = frm.SelectedPath + "\\";
            }
            frm.Dispose();
        }
    }
}