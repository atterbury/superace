﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SuperACE
{
    public partial class ReplaceForm : DevExpress.XtraEditors.XtraForm
    {
        private string message = string.Empty;
        public bool bReplace = false;
        public bool bReplaceAll = false;
        public bool bCombine = false;
        public bool bCombineAll = false;
        public bool bCancel = false;
        public bool bSkip = false;

        public ReplaceForm(string pMessage)
        {
            InitializeComponent();

            message = pMessage;
        }

        private void ReplaceForm_Load(object sender, EventArgs e)
        {
            tbMessage.Text = message;
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            bReplace = true;
            this.Close();
        }

        private void btnReplaceAll_Click(object sender, EventArgs e)
        {
            bReplaceAll = true;
            this.Close();
        }

        private void btnCombine_Click(object sender, EventArgs e)
        {
            bCombine = true;
            this.Close();
        }

        private void btnCombineAll_Click(object sender, EventArgs e)
        {
            bCombineAll = true;
            this.Close();
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            bSkip = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bCancel = true;
            this.Close();
        }
    }
}