﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;

using Projects.DAL;

namespace SuperACE
{
    public partial class ColumnChooserForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext;
        private string colTable = null;

        public ColumnChooserForm(ref ProjectsDbContext pContext, string pTable)
        {
            InitializeComponent();

            colTable = pTable;
            locContext = pContext;
        }

        private void ColumnChooser_Load(object sender, EventArgs e)
        {
            //db = new ProjectLocationModelEntities(); // ProjectLocationBLL.GetConnectionString(MainForm.ProjectLocationDataSource)
            switch (colTable)
            {
                case "ColumnsStand":
                    //db.ColumnsStand.Load();
                    columnsBindingSource.DataSource = locContext.ColumnsStands.ToList(); // db.ColumnsStand.Local.ToBindingList();
                    break;
                case "ColumnsPlot":
                    locContext.ColumnsPlots.Load();
                    columnsBindingSource.DataSource = locContext.ColumnsPlots.Local.ToBindingList();
                    break;
                case "ColumnsTree":
                    locContext.ColumnsTrees.Load();
                    columnsBindingSource.DataSource = locContext.ColumnsTrees.Local.ToBindingList();
                    break;
                case "ColumnsSegment":
                    locContext.ColumnsSegments.Load();
                    columnsBindingSource.DataSource = locContext.ColumnsSegments.Local.ToBindingList();
                    break;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            columnsBindingSource.EndEdit();
            columnsBindingSource.MoveFirst();
            locContext.SaveChanges();

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void ColumnChooser_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (db != null)
            //    db.Dispose();
        }
    }
}