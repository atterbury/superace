﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Project.DAL;
using System.Data.Entity.Validation;

using SuperACEUtils;

namespace SuperACE
{
    public partial class DefCalculationsForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext context = null;
        private ProjectDbContext projectContext = null;
        private DefCalculation rec = null;

        public DefCalculationsForm(ref ProjectDbContext pContext)
        {
            InitializeComponent();

            projectContext = pContext;
        }

        private void DefCalculationsForm_Load(object sender, EventArgs e)
        {
            try
            {
                List<Boardfootrule> bdftColl = ProjectBLL.GetBdFtRules(ref MainForm.projectContext, MainForm.CurrentProject.BdFtRule);
                cbBdFtRule.Properties.Items.Clear();
                foreach (var bdFtItem in bdftColl)
                {
                    cbBdFtRule.Properties.Items.Add(bdFtItem.InputCode);
                }

                List<Cubicfootrule> cuftColl = ProjectBLL.GetCuFtRules(ref MainForm.projectContext, MainForm.CurrentProject.CuFtRule);
                cbCuFtRule.Properties.Items.Clear();
                foreach (var cuFtItem in cuftColl)
                {
                    cbCuFtRule.Properties.Items.Add(cuFtItem.InputCode);
                }

                rec = projectContext.DefCalculations.FirstOrDefault();
                BindData();
            }
            catch
            {

            }
        }

        private void BindData()
        {
            cbBdFtRule.Text = rec.BdFtRule;
            cbCuFtRule.Text = rec.CuFtRule;
            tbSegmentLength.Text = rec.SegmentLength.ToString();
            tbMinDiameter.Text = rec.MinDia.ToString();
            tbMinLength.Text = rec.MinLen.ToString();
            cbTaperEquations.Text = rec.TaperEq;
        }

        private void SetData()
        {
            rec.BdFtRule = cbBdFtRule.Text;
            rec.CuFtRule = cbCuFtRule.Text;
            rec.SegmentLength = (short)Utils.ConvertToInt(tbSegmentLength.Text);
            rec.MinDia = (short)Utils.ConvertToInt(tbMinDiameter.Text);
            rec.MinLen = (short)Utils.ConvertToInt(tbMinLength.Text);
            rec.TaperEq = cbTaperEquations.Text;
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            try
            {
                SetData();
                context.Entry(rec).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {

            }
            this.Close();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void DefCalculationsForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}