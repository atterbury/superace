﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Project.DAL;

namespace SuperACE
{
    public partial class StandInputErrorsForm : DevExpress.XtraEditors.XtraForm
    {
        public bool Process = false;
        public TmpStandInputException siRec = null;
        private ProjectDbContext ctx = null;
        private List<TmpStandInputException> siErrors = null;

        public StandInputErrorsForm(ref ProjectDbContext pCtx)
        {
            InitializeComponent();

            ctx = pCtx;
        }

        private void StandInputErrorsForm_Load(object sender, EventArgs e)
        {
            siErrors = ctx.TmpStandInputExceptions.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ThenBy(s => s.Species).ThenBy(s => s.SiteIndex).ToList();
            tmpStandInputExceptionBindingSource.DataSource = siErrors;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            siRec = tmpStandInputExceptionBindingSource.Current as TmpStandInputException;
            Process = true;

            this.Close();
        }
    }
}