﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;

using Projects.DAL;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;

namespace SuperACE
{
    public partial class ReportForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext;
        public bool bCombined = false;
        public bool mProcess = false;
        private int mReports = 0;
        public List<string> selectedReports = new List<string>();

        public ReportForm(ref ProjectsDbContext pCtx)
        {
            InitializeComponent();

            locContext = pCtx;

            radioGroup1.SelectedIndexChanged += RadioGroup1_SelectedIndexChanged;
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            //db = new ProjectLocationModelEntities(ref locContext);
            treeList1.BeginUnboundLoad();
            TreeListNode parentNodes = null;

            List<Report> reports = locContext.Reports.OrderBy(r => r.Seq).ToList();
            foreach (var reportItem in reports)
	        {
                TreeListNode rootNode = treeList1.AppendNode(new object[] { reportItem.Heading }, parentNodes);
                List<Projects.DAL.ReportItem> itemsColl = ProjectsBLL.GetReportItems(ref locContext, reportItem);
                foreach (var item in itemsColl)
                {
                    if (item.Active == "Y")
                        treeList1.AppendNode(new object[] { item.Heading }, rootNode);
                }
	        }
            treeList1.EndUnboundLoad();

            treeList1.ExpandAll();
            bCombined = false;
        }

        private void RadioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioGroup edit = sender as RadioGroup;
            if (edit.SelectedIndex == 0)
                bCombined = false;
            if (edit.SelectedIndex == 1)
                bCombined = true;
        }

        private void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (db != null)
            //    db.Dispose();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Process
            mProcess = true;
            //bCombined = Convert.ToBoolean(radioGroup1.Properties.Items[1].Value);
            var nodes = treeList1.GetAllCheckedNodes();
            foreach (TreeListNode node in nodes)
                selectedReports.Add(node.GetValue(treeListColumnReport).ToString());
            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            mProcess = false;
            bCombined = false;
            this.Close();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save Report
            XtraMessageBox.Show("Comming soon", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Recent Reports
            XtraMessageBox.Show("Comming soon", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void treeList1_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            TreeListNode node = e.Node;
            if (node.ParentNode == null)
            {
                SwitchNodesCheckState(node);
                return;
            }
            while (node.ParentNode.Level >= 1)
            {
                node = node.ParentNode;
            }
            SwitchNodesCheckState(node);
        }

        void SwitchNodesCheckState(TreeListNode node)
        {
            if (node.Checked)
            {
                node.UncheckAll();
            }
            else
            {
                node.CheckAll();
            }
            SwitchParentNodesCheckState(node.ParentNode);
        }

        void SwitchParentNodesCheckState(TreeListNode node)
        {
            if (node == null) 
                return;
            bool oneOfChildIsChecked = OneOfChildsIsChecked(node);
            if (oneOfChildIsChecked)
            {
                node.CheckState = CheckState.Checked;
            }
            else
            {
                node.CheckState = CheckState.Unchecked;
            }
        }

        private bool OneOfChildsIsChecked(TreeListNode node)
        {
            bool result = false;
            foreach (TreeListNode item in node.Nodes)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    result = true;
                }
            }
            return result;
        }

        private void treeList1_MouseDown(object sender, MouseEventArgs e)
        {
            var tl = sender as TreeList;
            if (tl == null)
            {
                return;
            }
            var hi = tl.CalcHitInfo(e.Location);
            if (hi.HitInfoType == HitInfoType.NodeCheckBox)
            {
                return;
            }
            var node = hi.Node;
            if (node != null)
            {
                SetNodeCheckState(tl, node);
            }
        }

        private void SetNodeCheckState(TreeList tl, TreeListNode node)
        {
            var checkState = node.CheckState;
            var resCheckState = CheckState.Checked; // CheckState.Indeterminate;
            switch (checkState)
            {
                case CheckState.Checked:
                    resCheckState = CheckState.Unchecked;
                    break;
                case CheckState.Unchecked:
                    resCheckState = CheckState.Checked; //.Indeterminate;
                    break;
                //case CheckState.Indeterminate:
                //    resCheckState = CheckState.Checked;
                //    break;
            }
            tl.SetNodeCheckState(node, resCheckState, true);
        }
    }
}