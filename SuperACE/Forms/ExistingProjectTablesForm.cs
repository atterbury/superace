﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;

using Projects.DAL;
using DevExpress.XtraTreeList.Nodes;
using Temp.DAL;

namespace SuperACE
{
    public partial class ExistingProjectTablesForm : DevExpress.XtraEditors.XtraForm
    {
        private TempDbContext dbTemp;
        public bool bCombined = false;
        public bool mProcess = false;
        public List<string> selectedReports = new List<string>();

        public ExistingProjectTablesForm(ref TempDbContext pCtx)
        {
            InitializeComponent();

            dbTemp = pCtx;
        }

        private void ExistingProjectTablesForm_Load(object sender, EventArgs e)
        {
            //db = new ProjectLocationModelEntities(ref locContext);
            treeList1.BeginUnboundLoad();
            TreeListNode parentNodes = null;

            List<string> varList = new List<string>();
            var result = (from s in dbTemp.ExistingProjectTables
                          group s by s.TableType into myGroup
                          orderby myGroup.Key
                          select new { TableType = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableType);
            foreach (var tblItem in varList)
	        {
                TreeListNode rootNode = treeList1.AppendNode(new object[] { tblItem }, parentNodes);
	            List<ExistingProjectTable> itemsColl = dbTemp.ExistingProjectTables.Where(t => t.TableType == tblItem && t.ExistingTable == true).ToList();
                foreach (var item in itemsColl)
                    treeList1.AppendNode(new object[] { item.TableName }, rootNode);
	        }
            treeList1.EndUnboundLoad();

            treeList1.ExpandAll();
        }

        private void ExistingProjectTablesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (db != null)
            //    db.Dispose();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Process
            mProcess = true;
            //bCombined = Convert.ToBoolean(radioGroup1.EditValue);
            var nodes = treeList1.GetAllCheckedNodes();
            string tableType = string.Empty;
            foreach (TreeListNode node in nodes)
            {
                if (node.HasChildren == true)
                    tableType = node.GetValue(treeListColumnReport).ToString();
                else
                {
                    UpdateTable(tableType, node.GetValue(treeListColumnReport).ToString());
                    //selectedReports.Add(string.Format("{0}:{1}", tableType, node.GetValue(treeListColumnReport).ToString()));
                }
            }
            dbTemp.SaveChanges();
            this.Close();
        }

        private void UpdateTable(string tableType, string p)
        {
            ExistingProjectTable rec = dbTemp.ExistingProjectTables.FirstOrDefault(t => t.TableType == tableType && t.TableName == p);
            if (rec != null)
                rec.ReplaceTable = true;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            mProcess = false;
            bCombined = false;
            this.Close();
        }

        private void treeList1_BeforeCheckNode(object sender, DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            TreeListNode node = e.Node;
            if (node.ParentNode == null)
            {
                SwitchNodesCheckState(node);
                return;
            }
            while (node.ParentNode.Level >= 1)
            {
                node = node.ParentNode;
            }
            SwitchNodesCheckState(node);
        }

        void SwitchNodesCheckState(TreeListNode node)
        {
            if (node.Checked)
            {
                node.UncheckAll();
            }
            else
            {
                node.CheckAll();
            }
            SwitchParentNodesCheckState(node.ParentNode);
        }

        void SwitchParentNodesCheckState(TreeListNode node)
        {
            if (node == null) 
                return;
            bool oneOfChildIsChecked = OneOfChildsIsChecked(node);
            if (oneOfChildIsChecked)
            {
                node.CheckState = CheckState.Checked;
            }
            else
            {
                node.CheckState = CheckState.Unchecked;
            }
        }

        private bool OneOfChildsIsChecked(TreeListNode node)
        {
            bool result = false;
            foreach (TreeListNode item in node.Nodes)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}