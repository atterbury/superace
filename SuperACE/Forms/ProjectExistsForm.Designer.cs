﻿namespace SuperACE
{
    partial class ProjectExistsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcess = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.rbtnReplace = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnRename = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNewProjectName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(34, 128);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(222, 128);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rbtnReplace
            // 
            this.rbtnReplace.AutoSize = true;
            this.rbtnReplace.Location = new System.Drawing.Point(15, 34);
            this.rbtnReplace.Name = "rbtnReplace";
            this.rbtnReplace.Size = new System.Drawing.Size(63, 17);
            this.rbtnReplace.TabIndex = 3;
            this.rbtnReplace.Text = "Replace";
            this.rbtnReplace.UseVisualStyleBackColor = true;
            this.rbtnReplace.CheckedChanged += new System.EventHandler(this.rbtnReplace_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Project Already Exists. What do you want to do?";
            // 
            // rbtnRename
            // 
            this.rbtnRename.AutoSize = true;
            this.rbtnRename.Checked = true;
            this.rbtnRename.Location = new System.Drawing.Point(15, 57);
            this.rbtnRename.Name = "rbtnRename";
            this.rbtnRename.Size = new System.Drawing.Size(64, 17);
            this.rbtnRename.TabIndex = 5;
            this.rbtnRename.TabStop = true;
            this.rbtnRename.Text = "Rename";
            this.rbtnRename.UseVisualStyleBackColor = true;
            this.rbtnRename.CheckedChanged += new System.EventHandler(this.rbtnRename_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "New Project Name:";
            // 
            // tbNewProjectName
            // 
            this.tbNewProjectName.Location = new System.Drawing.Point(136, 84);
            this.tbNewProjectName.Name = "tbNewProjectName";
            this.tbNewProjectName.Size = new System.Drawing.Size(161, 21);
            this.tbNewProjectName.TabIndex = 7;
            // 
            // ProjectExistsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 175);
            this.Controls.Add(this.tbNewProjectName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtnRename);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtnReplace);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnProcess);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectExistsForm";
            this.Text = "Project Exists";
            this.Load += new System.EventHandler(this.ProjectExistsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnProcess;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private System.Windows.Forms.RadioButton rbtnReplace;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnRename;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNewProjectName;
    }
}