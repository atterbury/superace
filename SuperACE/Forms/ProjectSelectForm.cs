﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Projects.DAL;
using System.IO;
using System.Configuration;

namespace SuperACE
{
    public partial class ProjectSelectForm : DevExpress.XtraEditors.XtraForm
    {
        public bool bSelected = false;
        private ProjectsDbContext locContext = null;
        private bool bSelect = true;

        public ProjectSelectForm(ref ProjectsDbContext pCtx, bool pSelect)
        {
            InitializeComponent();

            locContext = pCtx;
            bSelect = pSelect;
        }

        private void ProjectSelectForm_Load(object sender, EventArgs e)
        {
            if (bSelect)
            {
                this.Text = "Select Project";
                barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            else
            {
                this.Text = "Delete Project";
                barButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }

            CheckProjects();

            try
            {
                tbProject.Text = Properties.Settings.Default["CurrentProject"].ToString(); // ConfigurationManager.AppSettings["currentProject"]; // psRec.ProjectName;

                LoadProjects();
            }
            catch (InvalidOperationException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// CheckProjects - if a Project.DAL.Project File is missing, then remove it from the database
        /// </summary>
        private void CheckProjects()
        {
            bool currentProjectDeleted = false;
            List<Projects.DAL.AllProject> projects = ProjectsBLL.GetProjects(ref locContext);
            foreach (var item in projects)
            {
                string fileName = item.ProjectLocation + item.ProjectName + ".sdf";
                if (!File.Exists(fileName))
                {
                    locContext.AllProjects.Remove(item);
                    if (fileName == SuperACE.Properties.Settings.Default["CurrentDb"].ToString())
                        currentProjectDeleted = true;
                }
            }
            locContext.SaveChanges();

            // if current project was deleted - get the first project
            if (currentProjectDeleted)
            {
                Projects.DAL.AllProject rec = locContext.AllProjects.FirstOrDefault();
                SuperACE.Properties.Settings.Default["CurrentDb"] = rec.ProjectLocation + rec.ProjectName + ".sdf";
                SuperACE.Properties.Settings.Default["CurrentProject"] = rec.ProjectName;
                SuperACE.Properties.Settings.Default.Save();
            }
        }

        private void LoadProjects()
        {
            if (projectsBindingSource.Count > 0)
                projectsBindingSource.Clear();
            projectsBindingSource.DataSource = ProjectsBLL.GetProjects(ref locContext);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Set Default
            if (bSelect)
            {
                SetDefaultProject();

                this.Close();
            }
        }

        private void SetDefaultProject()
        {
            Projects.DAL.AllProject selectedProject = projectsBindingSource.Current as Projects.DAL.AllProject;

            //make changes
            Properties.Settings.Default["CurrentDb"] = string.Format("{0}{1}.sdf", selectedProject.ProjectLocation, selectedProject.ProjectName);
            Properties.Settings.Default["CurrentDataDir"] = selectedProject.ProjectLocation;
            Properties.Settings.Default["CurrentProject"] = selectedProject.ProjectName;
            //config.AppSettings.Settings["currentDb"].Value = string.Format("{0}{1}.sdf", selectedProject.ProjectLocation, selectedProject.ProjectName);

            //save to apply changes
            Properties.Settings.Default.Save();
            bSelected = true;

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!bSelect)
            {
                // Delete
                Projects.DAL.AllProject selectedProject = projectsBindingSource.Current as Projects.DAL.AllProject; // MainForm.CurrentProjectLocation;

                if (selectedProject == null)
                {
                    XtraMessageBox.Show("You must first select a Project in order to delete it!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                if (tbProject.Text == gridView1.GetFocusedRowCellValue("ProjectName").ToString())
                {
                    XtraMessageBox.Show("You can NOT delete the active Project", "Error");
                    return;
                }

                DialogResult deleteConfirmation = MessageBox.Show(string.Format("You are about to delete Project {0}. Are you sure?", selectedProject.ProjectName),
                                                            "Confirm delete",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);

                if (deleteConfirmation != DialogResult.Yes)
                    return;

                try
                {
                    if (File.Exists(string.Format("{0}\\{1}.sdf", selectedProject.ProjectLocation, selectedProject.ProjectName)))
                        File.Delete(string.Format("{0}\\{1}.sdf", selectedProject.ProjectLocation, selectedProject.ProjectName));

                    bool saveChangesIsSuccessful = false;
                    if (DeleteSelectedProject(selectedProject.ProjectName, selectedProject.ProjectLocation))
                        saveChangesIsSuccessful = ProjectsBLL.DeleteProjectLocation(ref locContext, selectedProject);

                    if (saveChangesIsSuccessful)
                    {
                        this.LoadProjects();
                    }
                }
                catch (IOException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            bSelected = false;
            this.Close();
        }

        private bool DeleteSelectedProject(string pProject, string pLocation)
        {
            bool bStatus = true;

            try
            {
                File.Delete(string.Format("{0}{1}.sdf", pLocation, pProject));
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bStatus = false;
            }
            catch (System.IO.IOException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bStatus = false;
            }
            catch (System.UnauthorizedAccessException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bStatus = false;
            }
            catch
            {
                XtraMessageBox.Show(string.Format("Problem deleting {0}{1}.sdf.", pLocation, pProject));
                bStatus = false;
            }
            return bStatus;
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (bSelect)
                SetDefaultProject();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            MainForm.CurrentProjectLocation = projectsBindingSource.Current as Projects.DAL.AllProject;
        }
    }
}