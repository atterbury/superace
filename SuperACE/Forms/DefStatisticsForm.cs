﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Project.DAL;
using System.Data.Entity.Validation;

using SuperACEUtils;
using Projects.DAL;

namespace SuperACE
{
    public partial class DefStatisticsForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext context = null;
        private ProjectDbContext db = null;
        private DefStat rec = null;

        public DefStatisticsForm(ref ProjectsDbContext pProjectsContext, ref ProjectDbContext pContext)
        {
            InitializeComponent();

            db = pContext;
            context = pProjectsContext;
        }

        private void DefStatisticsForm_Load(object sender, EventArgs e)
        {
            try
            {
                //cbStdDev.Properties.Items.Clear();
                //List<SdCl> sdClColl = context.SdCls.ToList();
                //foreach (var sdItem in sdClColl)
                //{
                //    cbStdDev.Properties.Items.Add(sdItem.Sd);
                //}

                rec = db.DefStats.FirstOrDefault();
                BindData();
            }
            catch
            {

            }
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void BindData()
        {
            if (rec == null)
            {
                rec = new DefStat();
                rec.Confidence = 0;
                rec.FiniteFlag = "N";
                rec.InfiniteFlag = "Y";
                rec.PlotReq1 = 5;
                rec.PlotReq2 = 10;
                rec.PlotReq3 = 15;
                rec.StdDeviation = 1.0;
                //projectContext.DefStatistics.Add(rec);
            }
            if (rec.InfiniteFlag == "Y")
            {
                radioGroup1.EditValue = 'Y';
                radioGroup1.Properties.Items[0].Value = 'Y';
            }
            else
            {
                radioGroup1.EditValue = 'N';
                radioGroup1.Properties.Items[1].Value = 'N';
            }
            tbStdDev.Text = rec.StdDeviation.ToString();
            tbConfLevel.Text = rec.Confidence.ToString();
            tbPlotReq1.Text = rec.PlotReq1.ToString();
            tbPlotReq2.Text = rec.PlotReq2.ToString();
            tbPlotReq3.Text = rec.PlotReq3.ToString();
        }

        private void SetData()
        {
            if (rec != null)
            {
                if (Convert.ToChar(radioGroup1.Properties.Items[0].Tag.ToString()) == 'Y')
                {
                    rec.InfiniteFlag = "Y"; ;
                    rec.FiniteFlag = "N";
                }
                else
                {
                    rec.InfiniteFlag = "N"; ;
                    rec.FiniteFlag = "Y";
                }
                rec.StdDeviation = Utils.ConvertToDouble(tbStdDev.Text);
                rec.Confidence = Utils.ConvertToDouble(tbConfLevel.Text);
                rec.PlotReq1 = Utils.ConvertToDouble(tbPlotReq1.Text);
                rec.PlotReq2 = Utils.ConvertToDouble(tbPlotReq2.Text);
                rec.PlotReq3 = Utils.ConvertToDouble(tbPlotReq3.Text);
                db.SaveChanges();
            }
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            SetData();
            this.Close();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void BindFormToRec()
        {
            
        }

        private void DefStatisticsForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
        
        private void tbConfLevel_Validating(object sender, CancelEventArgs e)
        {
            string txt = tbConfLevel.Text;
            double f = Convert.ToDouble(tbConfLevel.Text);

            if (f < 0.80)
            {
                e.Cancel = true;
                XtraMessageBox.Show("Cl must be greater than 0.80.");
                return;
            }
            if (f > 98.8)
            {
                e.Cancel = true;
                XtraMessageBox.Show("Cl must be less than 98.8.");
                return;
            }
            SdCl sdcl = context.SdCls.FirstOrDefault(s => s.Cl == f);
            if (sdcl != null)
                tbStdDev.Text = string.Format("{0:0.000}", sdcl.Sd);
            else
                tbStdDev.Text = GetSDFromCL(ref context, f);
        }

        private void tbStdDev_Validating(object sender, CancelEventArgs e)
        {
            string txt = tbStdDev.Text;
            double f = Convert.ToDouble(tbStdDev.Text);

            if (f < 0.01)
            {
                e.Cancel = true;
                XtraMessageBox.Show("Sd must be greater than 0.01.");
                return;
            }
            if (f > 3.09)
            {
                e.Cancel = true;
                XtraMessageBox.Show("Sd must be less than 3.09.");
                return;
            }
            SdCl sdcl = context.SdCls.FirstOrDefault(s => s.Sd == f);
            if (sdcl != null)
                tbConfLevel.Text = string.Format("{0:0.000}", sdcl.Cl);
            else
                tbConfLevel.Text = GetCLFromSD(ref context, f);
        }

        //private void cbStdDev_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    string txt = cbStdDev.Text;
        //    double f = Convert.ToDouble(cbStdDev.Text);
        //    SdCl sdcl = context.SdCls.FirstOrDefault(s => s.Sd.Equals(f));
        //    if (sdcl != null)
        //        tbConfLevel.Text = string.Format("{0:0.0}", sdcl.Cl);
        //    else
        //        tbConfLevel.Text = string.Empty;
        //}

        private string GetSDFromCL(ref ProjectsDbContext context, double cl)
        {
            SdCl clLow = new SdCl();
            SdCl clHigh = new SdCl();
            float result = 0;
            List<SdCl> tbls = context.SdCls.OrderBy(t => t.Cl).ToList();
            int xLow = 0;
            int xHigh = 0;
            bool haveLow = false;
            foreach (SdCl item in tbls)
            {
                if (item.Cl < cl)
                    clLow = item;
                if (item.Cl > cl)
                {
                    if (!haveLow)
                    {
                        clLow = tbls[xHigh - 1];
                        clHigh = item;
                        haveLow = true;
                    }
                }
                if (haveLow)
                {
                    if (clHigh.Cl != item.Cl)
                    {
                        clHigh = tbls[xHigh - 1];
                        break;
                    }
                }
                xHigh++;
            }

            if (clLow.Sd > 0 && clHigh.Sd > 0)
                result = interpolation((float)cl, (float)clLow.Cl, (float)clHigh.Cl, (float)clLow.Sd, (float)clHigh.Sd);
            else
                result = 0;
            
            return string.Format("{0:0.000}", result);
        }

        private string GetCLFromSD(ref ProjectsDbContext context, double sd)
        {
            SdCl clLow = new SdCl();
            SdCl clHigh = new SdCl();
            float result = 0;
            List<SdCl> tbls = context.SdCls.OrderBy(t => t.Sd).ToList();
            int xLow = 0;
            int xHigh = 0;
            bool haveLow = false;
            foreach (SdCl item in tbls)
            {
                if (item.Sd < sd)
                    clLow = item;
                if (item.Sd > sd)
                {
                    if (!haveLow)
                    {
                        clLow = tbls[xHigh - 1];
                        clHigh = item;
                        haveLow = true;
                    }
                }
                if (haveLow)
                {
                    if (clHigh.Sd != item.Sd)
                    {
                        clHigh = tbls[xHigh - 1];
                        break;
                    }
                }
                xHigh++;
            }

            if (clLow.Sd > 0 && clHigh.Sd > 0)
                result = interpolation((float)sd, (float)clLow.Sd, (float)clHigh.Sd, (float)clLow.Cl, (float)clHigh.Cl);
            else
                result = 0;

            return string.Format("{0:0.000}", result);
        }

        private float interpolation(float cl, float clLow, float clHigh, float dataLow, float dataHigh)
        {
            return dataLow + ((cl - clLow) / (clHigh - clLow)) * (dataHigh - dataLow);

        }
    }
}