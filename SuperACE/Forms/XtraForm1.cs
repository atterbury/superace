﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Project.DAL;

namespace SuperACE
{
    public partial class XtraForm1 : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext ctx = null;
        private Stand currentStand = null;
        public XtraForm1(ref ProjectDbContext pCtx, Stand pStand)
        {
            InitializeComponent();

            ctx = pCtx;
            currentStand = pStand;
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
            //BindingSource bs = new BindingSource();
            // Test new trees and new segments
            string sql = "SELECT"
                + " NewTree.StandsID"
                + ",NewTree.TreesID"
                + ",NewTree.PlotNumber"
                + ",NewTree.TreeNumber"
                + ",NewTree.PlotFactorInput"
                + ",NewTree.AgeCode"
                + ",NewTree.SpeciesAbbreviation"
                + ",NewTree.TreeStatusDisplayCode"
                + ",NewTree.TreeCount"
                + ",NewTree.Dbh"
                + ",NewTree.FormPoint"
                + ",NewTree.ReportedFormFactor"
                + ",NewTree.TopDiameterFractionCode"
                + ",NewTree.BoleHeight"
                + ",NewTree.TotalHeight"
                + ",NewTree.CrownPositionDisplayCode"
                + ",NewTree.CrownRatioDisplayCode"
                + ",NewTree.VigorDisplayCode"
                + ",NewTree.DamageDisplayCode"
                + ",NewTree.UserDefinedDisplayCode"
                + ",NewTree.TreeType"
                + ",Segment1.SortCode AS Sort1"
                + ",Segment1.GradeCode AS Grade1"
                + ",Segment1.Length AS Length1"
                + ",Segment1.BdFtLD AS BdFtLD1"
                + ",Segment1.BdFtDD AS BdFtDD1"
                + ",Segment1.CuFtLD AS CuFtLD1"
                + ",Segment1.CuFtDD AS CuFtDD1"
                + ",Segment1.BdFtPD AS BdFtPD1"
                + ",Segment2.SortCode AS Sort2"
                + ",Segment2.GradeCode AS Grade2"
                + ",Segment2.Length AS Length2"
                + ",Segment2.BdFtLD AS BdFtLD2"
                + ",Segment2.BdFtDD AS BdFtDD2"
                + ",Segment2.CuFtLD AS CuFtLD2"
                + ",Segment2.CuFtDD AS CuFtDD2"
                + ",Segment2.BdFtPD AS BdFtPD2"
                + ",Segment3.SortCode AS Sort3"
                + ",Segment3.GradeCode AS Grade3"
                + ",Segment3.Length AS Length3"
                + ",Segment3.BdFtLD AS BdFtLD3"
                + ",Segment3.BdFtDD AS BdFtDD3"
                + ",Segment3.CuFtLD AS CuFtLD3"
                + ",Segment3.CuFtDD AS CuFtDD3"
                + ",Segment3.BdFtPD AS BdFtPD3"
                + ",Segment4.SortCode AS Sort4"
                + ",Segment4.GradeCode AS Grade4"
                + ",Segment4.Length AS Length4"
                + ",Segment4.BdFtLD AS BdFtLD4"
                + ",Segment4.BdFtDD AS BdFtDD4"
                + ",Segment4.CuFtLD AS CuFtLD4"
                + ",Segment4.CuFtDD AS CuFtDD4"
                + ",Segment4.BdFtPD AS BdFtPD4"
                + ",Segment5.SortCode AS Sort5"
                + ",Segment5.GradeCode AS Grade5"
                + ",Segment5.Length AS Length5"
                + ",Segment5.BdFtLD AS BdFtLD5"
                + ",Segment5.BdFtDD AS BdFtDD5"
                + ",Segment5.CuFtLD AS CuFtLD5"
                + ",Segment5.CuFtDD AS CuFtDD5"
                + ",Segment5.BdFtPD AS BdFtPD5"
                + ",Segment6.SortCode AS Sort6"
                + ",Segment6.GradeCode AS Grade62"
                + ",Segment6.Length AS Length6"
                + ",Segment6.BdFtLD AS BdFtLD6"
                + ",Segment6.BdFtDD AS BdFtDD6"
                + ",Segment6.CuFtLD AS CuFtLD6"
                + ",Segment6.CuFtDD AS CuFtDD6"
                + ",Segment6.BdFtPD AS BdFtPD6"
                + ",Segment7.SortCode AS Sort7"
                + ",Segment7.GradeCode AS Grade7"
                + ",Segment7.Length AS Length7"
                + ",Segment7.BdFtLD AS BdFtLD7"
                + ",Segment7.BdFtDD AS BdFtDD7"
                + ",Segment7.CuFtLD AS CuFtLD7"
                + ",Segment7.CuFtDD AS CuFtDD7"
                + ",Segment7.BdFtPD AS BdFtPD7"
                + ",Segment8.SortCode AS Sort8"
                + ",Segment8.GradeCode AS Grade8"
                + ",Segment8.Length AS Length8"
                + ",Segment8.BdFtLD AS BdFtLD8"
                + ",Segment8.BdFtDD AS BdFtDD8"
                + ",Segment8.CuFtLD AS CuFtLD8"
                + ",Segment8.CuFtDD AS CuFtDD8"
                + ",Segment8.BdFtPD AS BdFtPD8"
                + ",Segment9.SortCode AS Sort9"
                + ",Segment9.GradeCode AS Grade9"
                + ",Segment9.Length AS Length9"
                + ",Segment9.BdFtLD AS BdFtLD9"
                + ",Segment9.BdFtDD AS BdFtDD9"
                + ",Segment9.CuFtLD AS CuFtLD9"
                + ",Segment9.CuFtDD AS CuFtDD9"
                + ",Segment9.BdFtPD AS BdFtPD9"
                + ",Segment10.SortCode AS Sort10"
                + ",Segment10.GradeCode AS Grade10"
                + ",Segment10.Length AS Length10"
                + ",Segment10.BdFtLD AS BdFtLD10"
                + ",Segment10.BdFtDD AS BdFtDD10"
                + ",Segment10.CuFtLD AS CuFtLD10"
                + ",Segment10.CuFtDD AS CuFtDD10"
                + ",Segment10.BdFtPD AS BdFtPD10"
                + ",Segment11.SortCode AS Sort11"
                + ",Segment11.GradeCode AS Grade11"
                + ",Segment11.Length AS Length11"
                + ",Segment11.BdFtLD AS BdFtLD11"
                + ",Segment11.BdFtDD AS BdFtDD11"
                + ",Segment11.CuFtLD AS CuFtLD11"
                + ",Segment11.CuFtDD AS CuFtDD11"
                + ",Segment11.BdFtPD AS BdFtPD11"
                + ",Segment12.SortCode AS Sort12"
                + ",Segment12.GradeCode AS Grade12"
                + ",Segment12.Length AS Length12"
                + ",Segment12.BdFtLD AS BdFtLD12"
                + ",Segment12.BdFtDD AS BdFtDD12"
                + ",Segment12.CuFtLD AS CuFtLD12"
                + ",Segment12.CuFtDD AS CuFtDD12"
                + ",Segment12.BdFtPD AS BdFtPD12"
                + " FROM NewTree"
                + " LEFT JOIN NewSegment AS Segment1 ON Segment1.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 1"
                + " LEFT JOIN NewSegment AS Segment2 ON Segment2.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 2"
                + " LEFT JOIN NewSegment AS Segment3 ON Segment3.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 3"
                + " LEFT JOIN NewSegment AS Segment4 ON Segment4.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 4"
                + " LEFT JOIN NewSegment AS Segment5 ON Segment5.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 5"
                + " LEFT JOIN NewSegment AS Segment6 ON Segment6.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 6"
                + " LEFT JOIN NewSegment AS Segment7 ON Segment7.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 7"
                + " LEFT JOIN NewSegment AS Segment8 ON Segment8.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 8"
                + " LEFT JOIN NewSegment AS Segment9 ON Segment9.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 9"
                + " LEFT JOIN NewSegment AS Segment10 ON Segment10.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 10"
                + " LEFT JOIN NewSegment AS Segment11 ON Segment11.TreesID = NewTree.TreesID AND Segment1.SegmentNumber = 11"
                + " LEFT JOIN NewSegment AS Segment12 ON Segment12.TreesID = NewTree.TreesID AND Segment2.SegmentNumber = 12"
                + " WHERE NewTree.StandsID = "
                + "'" + currentStand.StandsId + "'"
                + "ORDER BY PlotNumber, TreeNumber"
                + ";";
            /*
            var result = (from tree in ctx.NewTrees
                      join s1 in ctx.NewSegments on new { A = tree.TreesId, B = 1 } equals new { A = s1.TreesId, B = s1.SegmentNumber }
                      select new Project.DAL.TreeView
                      {
                          StandsId = tree.StandsId,
                          TreesId = tree.TreesId,
                          UserPlotNumber = tree.PlotNumber,
                          TreeNumber = tree.TreeNumber,
                          PlotFactorInput = tree.PlotFactorInput,
                          AgeCode = tree.AgeCode,
                          SpeciesAbbreviation = tree.SpeciesAbbreviation,
                          TreeStatusDisplayCode = tree.TreeStatusDisplayCode,
                          TreeCount = tree.TreeCount,
                          Dbh = tree.Dbh,
                          FormPoint = tree.FormPoint,
                          ReportedFormFactor = tree.ReportedFormFactor,
                          TopDiameterFractionCode = tree.TopDiameterFractionCode,
                          BoleHeight = tree.BoleHeight,
                          TotalHeight = tree.TotalHeight,
                          Sort1 = s1.SortCode,
                          Grade1 = s1.GradeCode,
                          Length1 = s1.Length,
                          BdFtLd1 = s1.BdFtLd,
                          BdFtDd1 = s1.BdFtDd,
                          CuFtLd1 = s1.CuFtLd,
                          CuFtDd1 = s1.CuFtDd,
                          BdFtPd1 = s1.BdFtPd
                          //Sort2 = s2.SortCode,
                          //Grade2 = s2.GradeCode,
                          //Length2 = s2.Length,
                          //BdFtLd2 = s2.BdFtLd,
                          //BdFtDd2 = s2.BdFtDd,
                          //CuFtLd2 = s2.CuFtLd,
                          //CuFtDd2 = s2.CuFtDd,
                          //BdFtPd2 = s2.BdFtPd,
                      }).ToList();

            gridControl1.DataSource = result;
            */
            gridControlTrees.DataSource = ProjectBLL.GetDataTableFromQuery(ref ctx, sql);
        }
    }
}