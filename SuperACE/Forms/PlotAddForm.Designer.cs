﻿namespace SuperACE
{
    partial class PlotAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.plotBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.tbSection = new DevExpress.XtraEditors.TextEdit();
            this.tbRange = new DevExpress.XtraEditors.TextEdit();
            this.tbTownship = new DevExpress.XtraEditors.TextEdit();
            this.tbStand = new DevExpress.XtraEditors.TextEdit();
            this.tbTract = new DevExpress.XtraEditors.TextEdit();
            this.tbCounty = new DevExpress.XtraEditors.TextEdit();
            this.tbState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbProject = new DevExpress.XtraEditors.TextEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.UserPlotNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PlotDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.NotesTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CruiserTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BafInAtComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CruFlagComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.RefFlagComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AddFlagComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.DWFlagComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AspectLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.aspectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.HabitatLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.habitatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SlopeLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.SpeciesLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.speciesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EnvironmentalLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.envBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.XTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.YTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ZTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForUserPlotNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlotDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCruiser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBafInAt = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCruFlag = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRefFlag = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddFlag = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDWFlag = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAspect = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHabitat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSlope = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpecies = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEnvironmental = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForZ = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCounty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPlotNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlotDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlotDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiserTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BafInAtComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruFlagComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefFlagComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddFlagComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DWFlagComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HabitatLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.habitatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SlopeLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentalLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.envBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPlotNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlotDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBafInAt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDWFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAspect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHabitat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSlope)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForZ)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(678, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 416);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 392);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(678, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 392);
            // 
            // plotBindingSource
            // 
            this.plotBindingSource.DataSource = typeof(Project.DAL.Plot);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            this.dxErrorProvider1.DataSource = this.plotBindingSource;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.tbNetAcres);
            this.panelControl1.Controls.Add(this.tbSection);
            this.panelControl1.Controls.Add(this.tbRange);
            this.panelControl1.Controls.Add(this.tbTownship);
            this.panelControl1.Controls.Add(this.tbStand);
            this.panelControl1.Controls.Add(this.tbTract);
            this.panelControl1.Controls.Add(this.tbCounty);
            this.panelControl1.Controls.Add(this.tbState);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.tbProject);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(678, 60);
            this.panelControl1.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(595, 6);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(47, 13);
            this.labelControl9.TabIndex = 17;
            this.labelControl9.Text = "Net Acres";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(556, 6);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(17, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Sec";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(510, 6);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(19, 13);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Rge";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(458, 6);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(20, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Twn";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(393, 6);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(39, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Stand #";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(292, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(25, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Tract";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(189, 6);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(17, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Cty";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(147, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(10, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "St";
            // 
            // tbNetAcres
            // 
            this.tbNetAcres.Location = new System.Drawing.Point(587, 25);
            this.tbNetAcres.MenuManager = this.barManager1;
            this.tbNetAcres.Name = "tbNetAcres";
            this.tbNetAcres.Properties.ReadOnly = true;
            this.tbNetAcres.Size = new System.Drawing.Size(59, 20);
            this.tbNetAcres.TabIndex = 9;
            // 
            // tbSection
            // 
            this.tbSection.Location = new System.Drawing.Point(550, 25);
            this.tbSection.MenuManager = this.barManager1;
            this.tbSection.Name = "tbSection";
            this.tbSection.Properties.ReadOnly = true;
            this.tbSection.Size = new System.Drawing.Size(31, 20);
            this.tbSection.TabIndex = 8;
            // 
            // tbRange
            // 
            this.tbRange.Location = new System.Drawing.Point(498, 25);
            this.tbRange.MenuManager = this.barManager1;
            this.tbRange.Name = "tbRange";
            this.tbRange.Properties.ReadOnly = true;
            this.tbRange.Size = new System.Drawing.Size(46, 20);
            this.tbRange.TabIndex = 7;
            // 
            // tbTownship
            // 
            this.tbTownship.Location = new System.Drawing.Point(446, 25);
            this.tbTownship.MenuManager = this.barManager1;
            this.tbTownship.Name = "tbTownship";
            this.tbTownship.Properties.ReadOnly = true;
            this.tbTownship.Size = new System.Drawing.Size(46, 20);
            this.tbTownship.TabIndex = 6;
            // 
            // tbStand
            // 
            this.tbStand.Location = new System.Drawing.Point(385, 25);
            this.tbStand.MenuManager = this.barManager1;
            this.tbStand.Name = "tbStand";
            this.tbStand.Properties.ReadOnly = true;
            this.tbStand.Size = new System.Drawing.Size(55, 20);
            this.tbStand.TabIndex = 5;
            // 
            // tbTract
            // 
            this.tbTract.Location = new System.Drawing.Point(237, 25);
            this.tbTract.MenuManager = this.barManager1;
            this.tbTract.Name = "tbTract";
            this.tbTract.Properties.ReadOnly = true;
            this.tbTract.Size = new System.Drawing.Size(142, 20);
            this.tbTract.TabIndex = 4;
            // 
            // tbCounty
            // 
            this.tbCounty.Location = new System.Drawing.Point(180, 25);
            this.tbCounty.MenuManager = this.barManager1;
            this.tbCounty.Name = "tbCounty";
            this.tbCounty.Properties.ReadOnly = true;
            this.tbCounty.Size = new System.Drawing.Size(37, 20);
            this.tbCounty.TabIndex = 3;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(137, 25);
            this.tbState.MenuManager = this.barManager1;
            this.tbState.Name = "tbState";
            this.tbState.Properties.ReadOnly = true;
            this.tbState.Size = new System.Drawing.Size(37, 20);
            this.tbState.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(40, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Project";
            // 
            // tbProject
            // 
            this.tbProject.Location = new System.Drawing.Point(5, 25);
            this.tbProject.MenuManager = this.barManager1;
            this.tbProject.Name = "tbProject";
            this.tbProject.Properties.ReadOnly = true;
            this.tbProject.Size = new System.Drawing.Size(115, 20);
            this.tbProject.TabIndex = 0;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.UserPlotNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PlotDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.NotesTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CruiserTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BafInAtComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.CruFlagComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.RefFlagComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.AddFlagComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.DWFlagComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.AspectLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.HabitatLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SlopeLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SpeciesLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.EnvironmentalLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.XTextEdit);
            this.dataLayoutControl1.Controls.Add(this.YTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ZTextEdit);
            this.dataLayoutControl1.DataSource = this.plotBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 84);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(678, 332);
            this.dataLayoutControl1.TabIndex = 10;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // UserPlotNumberTextEdit
            // 
            this.UserPlotNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "UserPlotNumber", true));
            this.UserPlotNumberTextEdit.Location = new System.Drawing.Point(24, 59);
            this.UserPlotNumberTextEdit.MenuManager = this.barManager1;
            this.UserPlotNumberTextEdit.Name = "UserPlotNumberTextEdit";
            this.UserPlotNumberTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UserPlotNumberTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.UserPlotNumberTextEdit.Size = new System.Drawing.Size(155, 20);
            this.UserPlotNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.UserPlotNumberTextEdit.TabIndex = 4;
            // 
            // PlotDateDateEdit
            // 
            this.PlotDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "PlotDate", true));
            this.PlotDateDateEdit.EditValue = null;
            this.PlotDateDateEdit.Location = new System.Drawing.Point(183, 59);
            this.PlotDateDateEdit.MenuManager = this.barManager1;
            this.PlotDateDateEdit.Name = "PlotDateDateEdit";
            this.PlotDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.PlotDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlotDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlotDateDateEdit.Size = new System.Drawing.Size(154, 20);
            this.PlotDateDateEdit.StyleController = this.dataLayoutControl1;
            this.PlotDateDateEdit.TabIndex = 5;
            // 
            // NotesTextEdit
            // 
            this.NotesTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Notes", true));
            this.NotesTextEdit.Location = new System.Drawing.Point(73, 296);
            this.NotesTextEdit.MenuManager = this.barManager1;
            this.NotesTextEdit.Name = "NotesTextEdit";
            this.NotesTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.NotesTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NotesTextEdit.Size = new System.Drawing.Size(593, 20);
            this.NotesTextEdit.StyleController = this.dataLayoutControl1;
            this.NotesTextEdit.TabIndex = 6;
            this.NotesTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CruiserTextEdit
            // 
            this.CruiserTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Cruiser", true));
            this.CruiserTextEdit.Location = new System.Drawing.Point(341, 59);
            this.CruiserTextEdit.MenuManager = this.barManager1;
            this.CruiserTextEdit.Name = "CruiserTextEdit";
            this.CruiserTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CruiserTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CruiserTextEdit.Size = new System.Drawing.Size(155, 20);
            this.CruiserTextEdit.StyleController = this.dataLayoutControl1;
            this.CruiserTextEdit.TabIndex = 7;
            // 
            // BafInAtComboBoxEdit
            // 
            this.BafInAtComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "BafInAt", true));
            this.BafInAtComboBoxEdit.Location = new System.Drawing.Point(500, 59);
            this.BafInAtComboBoxEdit.MenuManager = this.barManager1;
            this.BafInAtComboBoxEdit.Name = "BafInAtComboBoxEdit";
            this.BafInAtComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BafInAtComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BafInAtComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BafInAtComboBoxEdit.Properties.Items.AddRange(new object[] {
            "FP",
            "DBH"});
            this.BafInAtComboBoxEdit.Size = new System.Drawing.Size(154, 20);
            this.BafInAtComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.BafInAtComboBoxEdit.TabIndex = 8;
            // 
            // CruFlagComboBoxEdit
            // 
            this.CruFlagComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "CruFlag", true));
            this.CruFlagComboBoxEdit.Location = new System.Drawing.Point(85, 126);
            this.CruFlagComboBoxEdit.MenuManager = this.barManager1;
            this.CruFlagComboBoxEdit.Name = "CruFlagComboBoxEdit";
            this.CruFlagComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CruFlagComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CruFlagComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CruFlagComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.CruFlagComboBoxEdit.Size = new System.Drawing.Size(94, 20);
            this.CruFlagComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.CruFlagComboBoxEdit.TabIndex = 9;
            // 
            // RefFlagComboBoxEdit
            // 
            this.RefFlagComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "RefFlag", true));
            this.RefFlagComboBoxEdit.Location = new System.Drawing.Point(244, 126);
            this.RefFlagComboBoxEdit.MenuManager = this.barManager1;
            this.RefFlagComboBoxEdit.Name = "RefFlagComboBoxEdit";
            this.RefFlagComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RefFlagComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.RefFlagComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RefFlagComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.RefFlagComboBoxEdit.Size = new System.Drawing.Size(93, 20);
            this.RefFlagComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.RefFlagComboBoxEdit.TabIndex = 10;
            // 
            // AddFlagComboBoxEdit
            // 
            this.AddFlagComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "AddFlag", true));
            this.AddFlagComboBoxEdit.Location = new System.Drawing.Point(402, 126);
            this.AddFlagComboBoxEdit.MenuManager = this.barManager1;
            this.AddFlagComboBoxEdit.Name = "AddFlagComboBoxEdit";
            this.AddFlagComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AddFlagComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.AddFlagComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AddFlagComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.AddFlagComboBoxEdit.Size = new System.Drawing.Size(94, 20);
            this.AddFlagComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.AddFlagComboBoxEdit.TabIndex = 11;
            // 
            // DWFlagComboBoxEdit
            // 
            this.DWFlagComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "DWFlag", true));
            this.DWFlagComboBoxEdit.Location = new System.Drawing.Point(561, 126);
            this.DWFlagComboBoxEdit.MenuManager = this.barManager1;
            this.DWFlagComboBoxEdit.Name = "DWFlagComboBoxEdit";
            this.DWFlagComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DWFlagComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DWFlagComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DWFlagComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.DWFlagComboBoxEdit.Size = new System.Drawing.Size(93, 20);
            this.DWFlagComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.DWFlagComboBoxEdit.TabIndex = 12;
            // 
            // AspectLookUpEdit
            // 
            this.AspectLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Aspect", true));
            this.AspectLookUpEdit.Location = new System.Drawing.Point(85, 193);
            this.AspectLookUpEdit.MenuManager = this.barManager1;
            this.AspectLookUpEdit.Name = "AspectLookUpEdit";
            this.AspectLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AspectLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.AspectLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AspectLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.AspectLookUpEdit.Properties.DataSource = this.aspectBindingSource;
            this.AspectLookUpEdit.Properties.DisplayMember = "DisplayCode";
            this.AspectLookUpEdit.Properties.NullText = "";
            this.AspectLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.AspectLookUpEdit.Properties.ValueMember = "DisplayCode";
            this.AspectLookUpEdit.Size = new System.Drawing.Size(62, 20);
            this.AspectLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AspectLookUpEdit.TabIndex = 13;
            // 
            // aspectBindingSource
            // 
            this.aspectBindingSource.DataSource = typeof(Project.DAL.Aspect);
            // 
            // HabitatLookUpEdit
            // 
            this.HabitatLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Habitat", true));
            this.HabitatLookUpEdit.Location = new System.Drawing.Point(212, 193);
            this.HabitatLookUpEdit.MenuManager = this.barManager1;
            this.HabitatLookUpEdit.Name = "HabitatLookUpEdit";
            this.HabitatLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HabitatLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.HabitatLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HabitatLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.HabitatLookUpEdit.Properties.DataSource = this.habitatBindingSource;
            this.HabitatLookUpEdit.Properties.DisplayMember = "DisplayCode";
            this.HabitatLookUpEdit.Properties.NullText = "";
            this.HabitatLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.HabitatLookUpEdit.Properties.ValueMember = "DisplayCode";
            this.HabitatLookUpEdit.Size = new System.Drawing.Size(62, 20);
            this.HabitatLookUpEdit.StyleController = this.dataLayoutControl1;
            this.HabitatLookUpEdit.TabIndex = 14;
            // 
            // habitatBindingSource
            // 
            this.habitatBindingSource.DataSource = typeof(Project.DAL.Habitat);
            // 
            // SlopeLookUpEdit
            // 
            this.SlopeLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Slope", true));
            this.SlopeLookUpEdit.Location = new System.Drawing.Point(339, 193);
            this.SlopeLookUpEdit.MenuManager = this.barManager1;
            this.SlopeLookUpEdit.Name = "SlopeLookUpEdit";
            this.SlopeLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SlopeLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SlopeLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SlopeLookUpEdit.Properties.NullText = "";
            this.SlopeLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SlopeLookUpEdit.Size = new System.Drawing.Size(62, 20);
            this.SlopeLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SlopeLookUpEdit.TabIndex = 15;
            // 
            // SpeciesLookUpEdit
            // 
            this.SpeciesLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Species", true));
            this.SpeciesLookUpEdit.Location = new System.Drawing.Point(466, 193);
            this.SpeciesLookUpEdit.MenuManager = this.barManager1;
            this.SpeciesLookUpEdit.Name = "SpeciesLookUpEdit";
            this.SpeciesLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SpeciesLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SpeciesLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpeciesLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.SpeciesLookUpEdit.Properties.DataSource = this.speciesBindingSource;
            this.SpeciesLookUpEdit.Properties.DisplayMember = "Abbreviation";
            this.SpeciesLookUpEdit.Properties.NullText = "";
            this.SpeciesLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SpeciesLookUpEdit.Properties.ValueMember = "Abbreviation";
            this.SpeciesLookUpEdit.Size = new System.Drawing.Size(61, 20);
            this.SpeciesLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SpeciesLookUpEdit.TabIndex = 16;
            // 
            // speciesBindingSource
            // 
            this.speciesBindingSource.DataSource = typeof(Project.DAL.Species);
            // 
            // EnvironmentalLookUpEdit
            // 
            this.EnvironmentalLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Environmental", true));
            this.EnvironmentalLookUpEdit.Location = new System.Drawing.Point(592, 193);
            this.EnvironmentalLookUpEdit.MenuManager = this.barManager1;
            this.EnvironmentalLookUpEdit.Name = "EnvironmentalLookUpEdit";
            this.EnvironmentalLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.EnvironmentalLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.EnvironmentalLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EnvironmentalLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.EnvironmentalLookUpEdit.Properties.DataSource = this.envBindingSource;
            this.EnvironmentalLookUpEdit.Properties.DisplayMember = "DisplayCode";
            this.EnvironmentalLookUpEdit.Properties.NullText = "";
            this.EnvironmentalLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.EnvironmentalLookUpEdit.Properties.ValueMember = "DisplayCode";
            this.EnvironmentalLookUpEdit.Size = new System.Drawing.Size(62, 20);
            this.EnvironmentalLookUpEdit.StyleController = this.dataLayoutControl1;
            this.EnvironmentalLookUpEdit.TabIndex = 17;
            // 
            // envBindingSource
            // 
            this.envBindingSource.DataSource = typeof(Project.DAL.Environment);
            // 
            // XTextEdit
            // 
            this.XTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "X", true));
            this.XTextEdit.Location = new System.Drawing.Point(85, 260);
            this.XTextEdit.MenuManager = this.barManager1;
            this.XTextEdit.Name = "XTextEdit";
            this.XTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.XTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.XTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XTextEdit.Size = new System.Drawing.Size(146, 20);
            this.XTextEdit.StyleController = this.dataLayoutControl1;
            this.XTextEdit.TabIndex = 18;
            this.XTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // YTextEdit
            // 
            this.YTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Y", true));
            this.YTextEdit.Location = new System.Drawing.Point(296, 260);
            this.YTextEdit.MenuManager = this.barManager1;
            this.YTextEdit.Name = "YTextEdit";
            this.YTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.YTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.YTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.YTextEdit.Size = new System.Drawing.Size(147, 20);
            this.YTextEdit.StyleController = this.dataLayoutControl1;
            this.YTextEdit.TabIndex = 19;
            this.YTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // ZTextEdit
            // 
            this.ZTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.plotBindingSource, "Z", true));
            this.ZTextEdit.Location = new System.Drawing.Point(508, 260);
            this.ZTextEdit.MenuManager = this.barManager1;
            this.ZTextEdit.Name = "ZTextEdit";
            this.ZTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ZTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ZTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ZTextEdit.Size = new System.Drawing.Size(146, 20);
            this.ZTextEdit.StyleController = this.dataLayoutControl1;
            this.ZTextEdit.TabIndex = 20;
            this.ZTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(678, 332);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(658, 312);
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesTextEdit;
            this.ItemForNotes.Location = new System.Drawing.Point(0, 284);
            this.ItemForNotes.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(658, 28);
            this.ItemForNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForUserPlotNumber,
            this.ItemForPlotDate,
            this.ItemForCruiser,
            this.ItemForBafInAt});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(658, 83);
            this.layoutControlGroup3.Text = "Plot Information";
            // 
            // ItemForUserPlotNumber
            // 
            this.ItemForUserPlotNumber.Control = this.UserPlotNumberTextEdit;
            this.ItemForUserPlotNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForUserPlotNumber.MinSize = new System.Drawing.Size(72, 40);
            this.ItemForUserPlotNumber.Name = "ItemForUserPlotNumber";
            this.ItemForUserPlotNumber.Size = new System.Drawing.Size(159, 40);
            this.ItemForUserPlotNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForUserPlotNumber.Text = "Plot Number";
            this.ItemForUserPlotNumber.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForUserPlotNumber.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForPlotDate
            // 
            this.ItemForPlotDate.Control = this.PlotDateDateEdit;
            this.ItemForPlotDate.Location = new System.Drawing.Point(159, 0);
            this.ItemForPlotDate.MinSize = new System.Drawing.Size(72, 40);
            this.ItemForPlotDate.Name = "ItemForPlotDate";
            this.ItemForPlotDate.Size = new System.Drawing.Size(158, 40);
            this.ItemForPlotDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlotDate.Text = "Plot Date";
            this.ItemForPlotDate.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForPlotDate.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForCruiser
            // 
            this.ItemForCruiser.Control = this.CruiserTextEdit;
            this.ItemForCruiser.Location = new System.Drawing.Point(317, 0);
            this.ItemForCruiser.MinSize = new System.Drawing.Size(72, 40);
            this.ItemForCruiser.Name = "ItemForCruiser";
            this.ItemForCruiser.Size = new System.Drawing.Size(159, 40);
            this.ItemForCruiser.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCruiser.Text = "Cruiser";
            this.ItemForCruiser.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForCruiser.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForBafInAt
            // 
            this.ItemForBafInAt.Control = this.BafInAtComboBoxEdit;
            this.ItemForBafInAt.Location = new System.Drawing.Point(476, 0);
            this.ItemForBafInAt.MinSize = new System.Drawing.Size(72, 40);
            this.ItemForBafInAt.Name = "ItemForBafInAt";
            this.ItemForBafInAt.Size = new System.Drawing.Size(158, 40);
            this.ItemForBafInAt.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForBafInAt.Text = "Baf In At";
            this.ItemForBafInAt.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForBafInAt.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCruFlag,
            this.ItemForRefFlag,
            this.ItemForAddFlag,
            this.ItemForDWFlag});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 83);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(658, 67);
            this.layoutControlGroup4.Text = "Data Taken";
            // 
            // ItemForCruFlag
            // 
            this.ItemForCruFlag.Control = this.CruFlagComboBoxEdit;
            this.ItemForCruFlag.Location = new System.Drawing.Point(0, 0);
            this.ItemForCruFlag.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForCruFlag.Name = "ItemForCruFlag";
            this.ItemForCruFlag.Size = new System.Drawing.Size(159, 24);
            this.ItemForCruFlag.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCruFlag.Text = "Cru Flag";
            this.ItemForCruFlag.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForRefFlag
            // 
            this.ItemForRefFlag.Control = this.RefFlagComboBoxEdit;
            this.ItemForRefFlag.Location = new System.Drawing.Point(159, 0);
            this.ItemForRefFlag.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForRefFlag.Name = "ItemForRefFlag";
            this.ItemForRefFlag.Size = new System.Drawing.Size(158, 24);
            this.ItemForRefFlag.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefFlag.Text = "Ref Flag";
            this.ItemForRefFlag.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForAddFlag
            // 
            this.ItemForAddFlag.Control = this.AddFlagComboBoxEdit;
            this.ItemForAddFlag.Location = new System.Drawing.Point(317, 0);
            this.ItemForAddFlag.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForAddFlag.Name = "ItemForAddFlag";
            this.ItemForAddFlag.Size = new System.Drawing.Size(159, 24);
            this.ItemForAddFlag.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAddFlag.Text = "Add Flag";
            this.ItemForAddFlag.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForDWFlag
            // 
            this.ItemForDWFlag.Control = this.DWFlagComboBoxEdit;
            this.ItemForDWFlag.Location = new System.Drawing.Point(476, 0);
            this.ItemForDWFlag.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForDWFlag.Name = "ItemForDWFlag";
            this.ItemForDWFlag.Size = new System.Drawing.Size(158, 24);
            this.ItemForDWFlag.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDWFlag.Text = "DW Flag";
            this.ItemForDWFlag.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAspect,
            this.ItemForHabitat,
            this.ItemForSlope,
            this.ItemForSpecies,
            this.ItemForEnvironmental});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 150);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(658, 67);
            this.layoutControlGroup5.Text = "Land Data";
            // 
            // ItemForAspect
            // 
            this.ItemForAspect.Control = this.AspectLookUpEdit;
            this.ItemForAspect.Location = new System.Drawing.Point(0, 0);
            this.ItemForAspect.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForAspect.Name = "ItemForAspect";
            this.ItemForAspect.Size = new System.Drawing.Size(127, 24);
            this.ItemForAspect.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAspect.Text = "Aspect";
            this.ItemForAspect.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForHabitat
            // 
            this.ItemForHabitat.Control = this.HabitatLookUpEdit;
            this.ItemForHabitat.Location = new System.Drawing.Point(127, 0);
            this.ItemForHabitat.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForHabitat.Name = "ItemForHabitat";
            this.ItemForHabitat.Size = new System.Drawing.Size(127, 24);
            this.ItemForHabitat.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHabitat.Text = "Habitat";
            this.ItemForHabitat.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForSlope
            // 
            this.ItemForSlope.Control = this.SlopeLookUpEdit;
            this.ItemForSlope.Location = new System.Drawing.Point(254, 0);
            this.ItemForSlope.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForSlope.Name = "ItemForSlope";
            this.ItemForSlope.Size = new System.Drawing.Size(127, 24);
            this.ItemForSlope.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSlope.Text = "Slope";
            this.ItemForSlope.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForSpecies
            // 
            this.ItemForSpecies.Control = this.SpeciesLookUpEdit;
            this.ItemForSpecies.Location = new System.Drawing.Point(381, 0);
            this.ItemForSpecies.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForSpecies.Name = "ItemForSpecies";
            this.ItemForSpecies.Size = new System.Drawing.Size(126, 24);
            this.ItemForSpecies.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSpecies.Text = "Species";
            this.ItemForSpecies.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForEnvironmental
            // 
            this.ItemForEnvironmental.Control = this.EnvironmentalLookUpEdit;
            this.ItemForEnvironmental.Location = new System.Drawing.Point(507, 0);
            this.ItemForEnvironmental.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForEnvironmental.Name = "ItemForEnvironmental";
            this.ItemForEnvironmental.Size = new System.Drawing.Size(127, 24);
            this.ItemForEnvironmental.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEnvironmental.Text = "Env";
            this.ItemForEnvironmental.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForX,
            this.ItemForY,
            this.ItemForZ});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 217);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(658, 67);
            this.layoutControlGroup6.Text = "Coordinates";
            // 
            // ItemForX
            // 
            this.ItemForX.Control = this.XTextEdit;
            this.ItemForX.Location = new System.Drawing.Point(0, 0);
            this.ItemForX.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForX.Name = "ItemForX";
            this.ItemForX.Size = new System.Drawing.Size(211, 24);
            this.ItemForX.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForX.Text = "X";
            this.ItemForX.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForY
            // 
            this.ItemForY.Control = this.YTextEdit;
            this.ItemForY.Location = new System.Drawing.Point(211, 0);
            this.ItemForY.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForY.Name = "ItemForY";
            this.ItemForY.Size = new System.Drawing.Size(212, 24);
            this.ItemForY.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForY.Text = "Y";
            this.ItemForY.TextSize = new System.Drawing.Size(58, 13);
            // 
            // ItemForZ
            // 
            this.ItemForZ.Control = this.ZTextEdit;
            this.ItemForZ.Location = new System.Drawing.Point(423, 0);
            this.ItemForZ.MinSize = new System.Drawing.Size(126, 24);
            this.ItemForZ.Name = "ItemForZ";
            this.ItemForZ.Size = new System.Drawing.Size(211, 24);
            this.ItemForZ.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForZ.Text = "Z";
            this.ItemForZ.TextSize = new System.Drawing.Size(58, 13);
            // 
            // PlotAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 416);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlotAddForm";
            this.Text = "Add New Plot";
            this.Load += new System.EventHandler(this.PlotAddForm_Load);
            this.Shown += new System.EventHandler(this.PlotAddForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCounty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserPlotNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlotDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlotDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiserTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BafInAtComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruFlagComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefFlagComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddFlagComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DWFlagComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HabitatLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.habitatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SlopeLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentalLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.envBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPlotNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlotDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBafInAt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDWFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAspect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHabitat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSlope)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForZ)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private System.Windows.Forms.BindingSource plotBindingSource;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbProject;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbNetAcres;
        private DevExpress.XtraEditors.TextEdit tbSection;
        private DevExpress.XtraEditors.TextEdit tbRange;
        private DevExpress.XtraEditors.TextEdit tbTownship;
        private DevExpress.XtraEditors.TextEdit tbStand;
        private DevExpress.XtraEditors.TextEdit tbTract;
        private DevExpress.XtraEditors.TextEdit tbCounty;
        private DevExpress.XtraEditors.TextEdit tbState;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit UserPlotNumberTextEdit;
        private DevExpress.XtraEditors.DateEdit PlotDateDateEdit;
        private DevExpress.XtraEditors.TextEdit NotesTextEdit;
        private DevExpress.XtraEditors.TextEdit CruiserTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit BafInAtComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CruFlagComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit RefFlagComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit AddFlagComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit DWFlagComboBoxEdit;
        private DevExpress.XtraEditors.LookUpEdit AspectLookUpEdit;
        private System.Windows.Forms.BindingSource aspectBindingSource;
        private DevExpress.XtraEditors.LookUpEdit HabitatLookUpEdit;
        private System.Windows.Forms.BindingSource habitatBindingSource;
        private DevExpress.XtraEditors.LookUpEdit SlopeLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit SpeciesLookUpEdit;
        private System.Windows.Forms.BindingSource speciesBindingSource;
        private DevExpress.XtraEditors.LookUpEdit EnvironmentalLookUpEdit;
        private System.Windows.Forms.BindingSource envBindingSource;
        private DevExpress.XtraEditors.TextEdit XTextEdit;
        private DevExpress.XtraEditors.TextEdit YTextEdit;
        private DevExpress.XtraEditors.TextEdit ZTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPlotNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlotDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCruiser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBafInAt;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCruFlag;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefFlag;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddFlag;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDWFlag;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAspect;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHabitat;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSlope;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpecies;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEnvironmental;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForX;
        private DevExpress.XtraLayout.LayoutControlItem ItemForY;
        private DevExpress.XtraLayout.LayoutControlItem ItemForZ;
    }
}