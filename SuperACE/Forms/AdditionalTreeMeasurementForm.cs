﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;
using System.IO;
using Projects.DAL;
using System.Data.Entity.Core;
using Project.DAL;

using SuperACEUtils;

namespace SuperACE
{
    public partial class AdditionalTreeMeasurementForm : DevExpress.XtraEditors.XtraForm
    {
        private AdditionalTreeMeasurement currentATM;
        private Plot currentPlot;
        private Tree currentTree;
        private Plot newPlot = null;
        private Stand currentStand = null;
        private Project.DAL.Project currentProject = null;
        private ProjectDbContext projectContext = null;

        public int plotID = -1;

        public AdditionalTreeMeasurementForm(ref ProjectDbContext pPrjCtx, Project.DAL.Project pProject, Stand pStand, Plot pPlot, Tree pTree)
        {
            InitializeComponent();

            currentPlot = pPlot;
            projectContext = pPrjCtx;
            currentTree = pTree;
            currentStand = pStand;
            currentProject = pProject;
        }

        private void AdditionalTreeMeasurementForm_Load(object sender, EventArgs e)
        {
            this.Text = "Additional Tree Measurements";

            LoadData();
        }

        private void LoadData()
        {
            Cursor.Current = Cursors.WaitCursor;

            tbProject.Text = currentProject.ProjectName;
            tbState.Text = currentStand.State;
            tbCounty.Text = currentStand.County;
            tbTract.Text = currentStand.TractName;
            tbStand.Text = currentStand.StandName;
            tbTownship.Text = currentStand.Township;
            tbRange.Text = currentStand.Range;
            tbSection.Text = currentStand.Section;
            tbNetAcres.Text = currentStand.NetGeographicAcres.ToString();

            if (additionalTreeMeasurementBindingSource.Count > 0)
                additionalTreeMeasurementBindingSource.Clear();

            if (currentStand != null)
                LoadDataSource();
            else
                MessageBox.Show("No selected Stand.", "Warning", MessageBoxButtons.OK);

            if (currentPlot != null)
            {
                int treeRowHandle = bandedGridView1.LocateByValue("PlotNumber", currentPlot.UserPlotNumber);
                if (treeRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                    bandedGridView1.FocusedRowHandle = treeRowHandle;
            }
            else
            {
                if (bandedGridView1.DataRowCount > 0)
                    bandedGridView1.MoveFirst();
            }

            Cursor.Current = Cursors.Default;
        }

        private void LoadDataSource()
        {
            additionalTreeMeasurementBindingSource.Clear();
            //treesBindingSource.DataSource = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == CurrentStand.StandsId).ToList();
            additionalTreeMeasurementBindingSource.DataSource =
                projectContext.AdditionalTreeMeasurements.OrderBy(t => t.PlotNumber)
                    .ThenBy(t => t.TreeNumber)
                    .Where(t => t.StandsId == currentStand.StandsId)
                    .ToList();
            if (additionalTreeMeasurementBindingSource.Count == 0)
            {
                //defTreeAge = 0;
                //defTreeFP = 0;
                //defTreePF = string.Empty;
                //defTreePlotNumber = string.Empty;
                //defTreeTreeNumber = 0;
                //defTreeTDF = string.Empty;
                AddAdditionalTreeMeasurement();
            }
            else
            {
                // Set defaults for tree
                bandedGridView1.MoveLast();
                //defTreePlotNumber = bandedGridView1.GetFocusedRowCellDisplayText("PlotNumber");
                //defTreeTreeNumber = Utils.ConvertToInt(bandedGridView1.GetFocusedRowCellDisplayText("TreeNumber"));
                //defTreePF = bandedGridView1.GetFocusedRowCellDisplayText("PlotFactorInput");
                //defTreeAge = Utils.ConvertToTiny(bandedGridView1.GetFocusedRowCellDisplayText("AgeCode"));
                //defTreeFP = Utils.ConvertToTiny(bandedGridView1.GetFocusedRowCellDisplayText("FormPoint"));
                //defTreeTDF = bandedGridView1.GetFocusedRowCellDisplayText("TopDiameterFractionCode");
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private bool CheckProperties()
        {
            bool bReturn = true;

            //if (string.IsNullOrEmpty(ProjectNameComboBoxEdit.Text.Trim()))
            //{
            //    dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "Project Name is required");
            //    bReturn = false;
            //}
            //if (string.IsNullOrEmpty(tbProjectLocation.Text.Trim()))
            //{
            //    dxErrorProvider1.SetError(tbProjectLocation, "Project Location is required");
            //    bReturn = false;
            //}
            return bReturn;
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        private void AdditionalTreeMeasurementForm_Shown(object sender, EventArgs e)
        {
            //ProjectNameComboBoxEdit.Focus();
        }

        private void AddAdditionalTreeMeasurement()
        {
            AdditionalTreeMeasurement rec = new AdditionalTreeMeasurement();
            rec.BearingAzimuth = string.Empty;
            rec.BhAge = 0;
            rec.Bt1 = 0;
            rec.Bt2 = 0;
            rec.Btr = 0;
            rec.CrownPosition = string.Empty;
            rec.CrownRatio = string.Empty;
            rec.Dbh = 0;
            rec.DbhInc = 0;
            rec.Distance = 0;
            rec.HtInc = 0;
            rec.IncYears = 0;
            rec.PlotNumber = string.Empty; //
            rec.SiteTree = string.Empty;
            rec.Species = string.Empty;
            rec.StandsId = currentStand.StandsId;
            rec.TotalAge = 0;
            rec.TotalHeight = 0;
            rec.TreeNumber = 0;
            rec.TreesId = currentTree.TreesId;

            projectContext.AdditionalTreeMeasurements.Add(rec);
            projectContext.SaveChanges();

            LoadDataSource();
            bandedGridView1.MoveLast(); //.FocusedRowHandle = advBandedGridViewPlots.FocusedRowHandle + 1;
            //if (string.IsNullOrEmpty(defTreePlotNumber))
            //    bandedGridView1.FocusedColumn = bandedGridView1.Columns["PlotNumber"];
            //else
            //    bandedGridView1.FocusedColumn = bandedGridView1.Columns["SpeciesAbbreviation"];
            gridControl1.Focus();
        }

        private void bandedGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    bandedGridView1.UpdateCurrentRow();
                    if (additionalTreeMeasurementBindingSource.Position == additionalTreeMeasurementBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        AddAdditionalTreeMeasurement();
                    }
                    else
                    {
                        currentATM = additionalTreeMeasurementBindingSource.Current as AdditionalTreeMeasurement;
                        //if (defTreeSpecies.Abbreviation != currentATM.Species)
                        //    defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, currentProject.SpeciesTableName, currentTree.SpeciesAbbreviation);
                    }
                    break;
            }
        }

        private void gridControl1_EditorKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    //advBandedGridViewTrees.ValidateEditor();
                    //SendKeys.Send("{ENTER}");
                    bandedGridView1.UpdateCurrentRow();
                    if (additionalTreeMeasurementBindingSource.Position == additionalTreeMeasurementBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        //DoCalcForTree(true, null);
                        AddAdditionalTreeMeasurement();
                    }
                    else
                    {
                        currentATM = additionalTreeMeasurementBindingSource.Current as AdditionalTreeMeasurement;
                        //if (defTreeSpecies != null)
                        //{
                        //    if (defTreeSpecies.Abbreviation != CurrentTree.SpeciesAbbreviation)
                        //        defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
                        //}
                        //else
                        //    defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
                    }
                    break;
                case Keys.Enter:
                case Keys.Right:
                    CheckMoveToNextColumn();
                    break;
            }
        }

        private void CheckMoveToNextColumn()
        {
            //string tmp = advBandedGridViewTrees.GetFocusedRowCellDisplayText(advBandedGridViewTrees.FocusedColumn.FieldName);
            if (bandedGridView1.ActiveEditor == null)
                return;

            string tmp = bandedGridView1.ActiveEditor.Text;

            //switch (bandedGridView1.FocusedColumn.FieldName)
            //{
            //    case "Sort1":
            //    case "Sort2":
            //    case "Sort3":
            //    case "Sort4":
            //    case "Sort5":
            //    case "Sort6":
            //    case "Sort7":
            //    case "Sort8":
            //    case "Sort9":
            //    case "Sort10":
            //    case "Sort11":
            //    case "Sort12":
            //        if (!string.IsNullOrEmpty(tmp))
            //            return;
            //        break;
            //    case "BdFtLd1":
            //    case "BdFtLd2":
            //    case "BdFtLd3":
            //    case "BdFtLd4":
            //    case "BdFtLd5":
            //    case "BdFtLd6":
            //    case "BdFtLd7":
            //    case "BdFtLd8":
            //    case "BdFtLd9":
            //    case "BdFtLd10":
            //    case "BdFtLd11":
            //    case "BdFtLd12":
            //        if (Utils.ConvertToTiny(tmp) != 0)
            //            return;
            //        break;
            //}

            //switch (bandedGridView1.FocusedColumn.FieldName)
            //{
            //    case "Sort1":
            //    case "Sort2":
            //    case "Sort3":
            //    case "Sort4":
            //    case "Sort5":
            //    case "Sort6":
            //    case "Sort7":
            //    case "Sort8":
            //    case "Sort9":
            //    case "Sort10":
            //    case "Sort11":
            //    case "Sort12":
            //        advBandedGridViewTrees.UpdateCurrentRow();
            //        if (treesBindingSource.Position == treesBindingSource.Count - 1)
            //        {
            //            //DoCalcForTree(true, null);
            //            AddAdditionalTreeMeasurementee();
            //        }
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
            //        break;
            //    case "BdFtLd1":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort2"];
            //        break;
            //    case "BdFtLd2":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort3"];
            //        break;
            //    case "BdFtLd3":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort4"];
            //        break;
            //    case "BdFtLd4":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort5"];
            //        break;
            //    case "BdFtLd5":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort6"];
            //        break;
            //    case "BdFtLd6":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort7"];
            //        break;
            //    case "BdFtLd7":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort8"];
            //        break;
            //    case "BdFtLd8":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort9"];
            //        break;
            //    case "BdFtLd9":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort10"];
            //        break;
            //    case "BdFtLd10":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort11"];
            //        break;
            //    case "BdFtLd11":
            //        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort12"];
            //        break;
            //    case "BdFtLd12":
            //        if (treesBindingSource.Position == treesBindingSource.Count - 1)
            //        {
            //            //DoCalcForTree(true, null);
            //            AddAdditionalTreeMeasurement();
            //        }
            //        bandedGridView1.FocusedColumn = bandedGridView1.Columns["SpeciesAbbreviation"];
            //        break;
            //}
        }

        private void bandedGridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            float dbh = 0;
            float bt1 = 0;
            float bt2 = 0;

            switch (bandedGridView1.FocusedColumn.FieldName)
            {
                case "Dbh":
                    dbh = Utils.ConvertToFloat(e.Value.ToString());
                    bt1 = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Bt1").ToString());
                    bt2 = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Bt2").ToString());
                    bandedGridView1.SetFocusedRowCellValue("Btr", CalcBTR(dbh, bt1, bt2));
                    break;
                case "Bt1":
                    dbh = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Dbh").ToString());
                    bt1 = Utils.ConvertToFloat(e.Value.ToString());
                    bt2 = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Bt2").ToString());
                    bandedGridView1.SetFocusedRowCellValue("Btr", CalcBTR(dbh, bt1, bt2));
                    break;
                case "Bt2":
                    dbh = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Dbh").ToString());
                    bt1 = Utils.ConvertToFloat(bandedGridView1.GetFocusedRowCellValue("Bt1").ToString());
                    bt2 = Utils.ConvertToFloat(e.Value.ToString());
                    bandedGridView1.SetFocusedRowCellValue("Btr", CalcBTR(dbh, bt1, bt2));
                    break;
            }
        }

        private object CalcBTR(float dbh, float bt1, float bt2)
        {
            string tmp = string.Format("{0:n3}", (dbh - (bt1 + bt2)) / dbh);
            float result = Utils.ConvertToFloat(tmp);
            return result;
        }
    }
}