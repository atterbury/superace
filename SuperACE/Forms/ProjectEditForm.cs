﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;
using System.IO;
using Projects.DAL;
using System.Data.Entity.Core;
using Project.DAL;

namespace SuperACE
{
    public partial class ProjectEditForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext = null;
        private ProjectDbContext projectContext = null;

        public ProjectEditForm(ref ProjectDbContext pPrjCtx, ref ProjectsDbContext pLocCtx)
        {
            InitializeComponent();

            locContext = pLocCtx;
            projectContext = pPrjCtx;
        }

        private void ProjectEditForm_Load(object sender, EventArgs e)
        {
            //cbProjectName.ReadOnly = false;
            //tbProjectLocation.ReadOnly = false;
            simpleButton1.Visible = true;

            tbProjectLocation.Text = Properties.Settings.Default["DbLocation"].ToString(); // ConfigurationManager.AppSettings["dblocation"];
            PopulateProjectDropDown(tbProjectLocation.Text);
            this.Text = "Edit/View Project";

            LoadTableName();

            projectsBindingSource.DataSource = MainForm.CurrentProject;
            this.ProjectNameComboBoxEdit.Focus();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void LoadTableName()
        {
            LoadSpeciesTableNames();
            LoadSortTableNames();
            LoadGradeTableNames();
            LoadPriceTableNames();
            LoadCostTableNames();

            LoadCrownPositionableNames();
            LoadDamageTableNames();
            LoadHarvestTableNames();
            LoadHistoryPlanningTableNames();
            LoadSeedZoneTableNames();
            LoadSoilsTableNames();
            LoadTreatmentTableNames();
            LoadTreeStatusTableNames();
            LoadVegetationTableNames();
            LoadComponentTableNames();
            LoadNonStockedTableNames();
            LoadNonTimberedTableNames();
            LoadBdFtRuleTableNames();
            LoadASuboTableNames();
            LoadFormTableNames();
            LoadTrimTableNames();
            LoadPilingTableNames();
            LoadAspectTableNames();
            LoadCruiserTableNames();
            LoadEnvironmentTableNames();
            LoadLandFormTableNames();
            LoadRoadTableNames();
            LoadSlashTableNames();
            LoadStreamTableNames();
            LoadTreeSourceTableNames();
            LoadUserDefinedTableNames();
            LoadVigorTableNames();
            LoadHabitatTableNames();
            LoadWoodTypeTableNames();
            LoadCuFtRuleTableNames();
            LoadPoleTableNames();
            LoadDestinationTableNames();
        }

        private void LoadDestinationTableNames()
        {
            //List<string> tbls = ProjectBLL.GetD.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            DestinationTableNameComboBoxEdit.Text = MainForm.CurrentProject.DestinationTableName;
        }

        private void LoadPoleTableNames()
        {
            //List<string> tbls = ProjectBLL.Get.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            PolesTableNameComboBoxEdit.Text = MainForm.CurrentProject.PolesTableName;
        }

        private void LoadCuFtRuleTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            CuFtRuleComboBoxEdit.Text = MainForm.CurrentProject.CuFtRule;
        }

        private void LoadWoodTypeTableNames()
        {
            List<string> tbls = ProjectBLL.GetWoodTypeTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                WoodTypeTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            WoodTypeTableNameComboBoxEdit.Text = MainForm.CurrentProject.WoodTypeTableName;
        }

        private void LoadHabitatTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            HabitatTableNameComboBoxEdit.Text = MainForm.CurrentProject.HabitatTableName;
        }

        private void LoadVigorTableNames()
        {
            List<string> tbls = ProjectBLL.GetVigorTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                VigorTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            VigorTableNameComboBoxEdit.Text = MainForm.CurrentProject.VigorTableName;
        }

        private void LoadUserDefinedTableNames()
        {
            List<string> tbls = ProjectBLL.GetUserDefinedTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                UserDefinedTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            UserDefinedTableNameComboBoxEdit.Text = MainForm.CurrentProject.UserDefinedTableName;
        }

        private void LoadTreeSourceTableNames()
        {
            List<string> tbls = ProjectBLL.GetTreeSourceTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                TreeSourcesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            TreeSourcesTableNameComboBoxEdit.Text = MainForm.CurrentProject.TreeSourcesTableName;
        }

        private void LoadStreamTableNames()
        {
            List<string> tbls = ProjectBLL.GetStreamTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                StreamsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            StreamsTableNameComboBoxEdit.Text = MainForm.CurrentProject.StreamsTableName;
        }

        private void LoadSlashTableNames()
        {
            List<string> tbls = ProjectBLL.GetSlashTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                SlashDistributionsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            SlashDistributionsTableNameComboBoxEdit.Text = MainForm.CurrentProject.SlashDistributionsTableName;
        }

        private void LoadRoadTableNames()
        {
            List<string> tbls = ProjectBLL.GetRoadTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                RoadsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            RoadsTableNameComboBoxEdit.Text = MainForm.CurrentProject.RoadsTableName;
        }

        private void LoadLandFormTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            LandFormsTableNameComboBoxEdit.Text = MainForm.CurrentProject.LandFormsTableName;
        }

        private void LoadEnvironmentTableNames()
        {
            List<string> tbls = ProjectBLL.GetEnvironmentTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                EnvironmentsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            EnvironmentsTableNameComboBoxEdit.Text = MainForm.CurrentProject.EnvironmentsTableName;
        }

        private void LoadCruiserTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            CruisersTableNameComboBoxEdit.Text = MainForm.CurrentProject.CruisersTableName;
        }

        private void LoadAspectTableNames()
        {
            List<string> tbls = ProjectBLL.GetAspectTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                AspectsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            AspectsTableNameComboBoxEdit.Text = MainForm.CurrentProject.AspectsTableName;
        }

        private void LoadPilingTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            PilingTableNameComboBoxEdit.Text = MainForm.CurrentProject.PilingTableName;
        }

        private void LoadTrimTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            TrimTableNameComboBoxEdit.Text = MainForm.CurrentProject.TrimTableName;
        }

        private void LoadFormTableNames()
        {
            List<string> tbls = ProjectBLL.GetFormTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                FormTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            FormTableNameComboBoxEdit.Text = MainForm.CurrentProject.FormTableName;
        }

        private void LoadASuboTableNames()
        {
            List<string> tbls = ProjectBLL.GetAspectTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                ASuboTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            ASuboTableNameComboBoxEdit.Text = MainForm.CurrentProject.ASuboTableName;
        }

        private void LoadBdFtRuleTableNames()
        {
            //List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            //foreach (var tblItem in tbls)
            //    SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            BdFtRuleComboBoxEdit.Text = MainForm.CurrentProject.BdFtRule;
        }

        private void LoadNonTimberedTableNames()
        {
            List<string> tbls = ProjectBLL.GetNonTimberedTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                NonTimberedTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            NonTimberedTableNameComboBoxEdit.Text = MainForm.CurrentProject.NonTimberedTableName;
        }

        private void LoadNonStockedTableNames()
        {
            List<string> tbls = ProjectBLL.GetNonStockedTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                NonStockedTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            NonStockedTableNameComboBoxEdit.Text = MainForm.CurrentProject.NonStockedTableName;
        }

        private void LoadComponentTableNames()
        {
            List<string> tbls = ProjectBLL.GetComponentTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                ComponentsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            ComponentsTableNameComboBoxEdit.Text = MainForm.CurrentProject.ComponentsTableName;
        }

        private void LoadVegetationTableNames()
        {
            List<string> tbls = ProjectBLL.GetVegetationTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                VegetationsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            VegetationsTableNameComboBoxEdit.Text = MainForm.CurrentProject.VegetationsTableName;
        }

        private void LoadTreeStatusTableNames()
        {
            List<string> tbls = ProjectBLL.GetTreeSourceTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                TreeStatusTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            TreeStatusTableNameComboBoxEdit.Text = MainForm.CurrentProject.TreeStatusTableName;
        }

        private void LoadTreatmentTableNames()
        {
            List<string> tbls = ProjectBLL.GetTreatmentTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                TreatmentsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            TreatmentsTableNameComboBoxEdit.Text = MainForm.CurrentProject.TreatmentsTableName;
        }

        private void LoadSoilsTableNames()
        {
            List<string> tbls = ProjectBLL.GetSoilTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                SoilsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            SoilsTableNameComboBoxEdit.Text = MainForm.CurrentProject.SoilsTableName;
        }

        private void LoadSeedZoneTableNames()
        {
            List<string> tbls = ProjectBLL.GetSeedZoneTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                SeedZonesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            SeedZonesTableNameComboBoxEdit.Text = MainForm.CurrentProject.SeedZonesTableName;
        }

        private void LoadHarvestTableNames()
        {
            List<string> tbls = ProjectBLL.GetHarvestTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                HarvestSystemsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            HarvestSystemsTableNameComboBoxEdit.Text = MainForm.CurrentProject.HarvestSystemsTableName;
        }

        private void LoadHistoryPlanningTableNames()
        {
            List<string> tbls = ProjectBLL.GetHistoryPlanningTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                HistoryPlanningTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            HistoryPlanningTableNameComboBoxEdit.Text = MainForm.CurrentProject.HistoryPlanningTableName;
        }

        private void LoadDamageTableNames()
        {
            List<string> tbls = ProjectBLL.GetDamageTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                DamageTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            DamageTableNameComboBoxEdit.Text = MainForm.CurrentProject.DamageTableName;
        }

        private void LoadCrownPositionableNames()
        {
            List<string> tbls = ProjectBLL.GetCrownPositionTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                CrownPositionTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            CrownPositionTableNameComboBoxEdit.Text = MainForm.CurrentProject.CrownPositionTableName;
        }

        private void LoadSpeciesTableNames()
        {
            List<string> tbls = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                SpeciesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            SpeciesTableNameComboBoxEdit.Text = MainForm.CurrentProject.SpeciesTableName;
        }

        private void LoadSortTableNames()
        {
            List<string> tbls = ProjectBLL.GetSortTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                SortsTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            SortsTableNameComboBoxEdit.Text = MainForm.CurrentProject.SortsTableName;
        }

        private void LoadGradeTableNames()
        {
            List<string> tbls = ProjectBLL.GetGradeTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                GradesTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            GradesTableNameComboBoxEdit.Text = MainForm.CurrentProject.GradesTableName;
        }

        private void LoadPriceTableNames()
        {
            List<string> tbls = ProjectBLL.GetPriceTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                PriceTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            PriceTableNameComboBoxEdit.Text = MainForm.CurrentProject.PriceTableName;
        }

        private void LoadCostTableNames()
        {
            List<string> tbls = ProjectBLL.GetCostTableNames(ref projectContext);
            foreach (var tblItem in tbls)
                CostTableNameComboBoxEdit.Properties.Items.Add(tblItem);
            CostTableNameComboBoxEdit.Text = MainForm.CurrentProject.CostTableName;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            projectsBindingSource.EndEdit();
            if (!CheckProperties())
                return;

            try
            {
                ProjectBLL.SaveProject(ref projectContext, MainForm.CurrentProject);
            }
            catch (Exception ex)
            {
                this.HandleExceptionOnSaveChanges(ex.Message);
                return;
            }

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private bool CheckProperties()
        {
            bool bReturn = true;

            if (string.IsNullOrEmpty(ProjectNameComboBoxEdit.Text.Trim()))
            {
                dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "Project Name is required");
                bReturn = false;
            }
            if (string.IsNullOrEmpty(tbProjectLocation.Text.Trim()))
            {
                dxErrorProvider1.SetError(tbProjectLocation, "Project Location is required");
                bReturn = false;
            }
            return bReturn;
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        //private void BindControlsToProject()
        //{
        //    cbProjectName.Text = MainForm.CurrentProjectLocation.ProjectName;
        //    tbProjectLocation.Text = MainForm.CurrentProjectLocation.ProjectLocation;

        //    tbProjectNumber.Text = MainForm.CurrentProject.ProjectNumber;
        //    tbProjectGISName.Text = MainForm.CurrentProject.GISProjectName;
        //    tbProjectLead.Text = MainForm.CurrentProject.ProjectLead;
        //    tbSaleName.Text = MainForm.CurrentProject.SaleName;
        //    tbProjectDate.Text = MainForm.CurrentProject.ProjectDate.ToString();
        //    memoProjectNotes.Text = MainForm.CurrentProject.ProjectNotes;

        //    tbCustomerName.Text = MainForm.CurrentProject.CustomerCompanyName;
        //    //tbCustomerOfficePhone.Text = MainForm.CurrentProject.CustomerContactName;
        //    tbCustomerOfficePhone.Text = MainForm.CurrentProject.CustomerOfficePhone;
        //    tbCustomerCellPhone.Text = MainForm.CurrentProject.CustomerCellPhone;
        //    tbCustomerFax.Text = MainForm.CurrentProject.CustomerFax;
        //    tbCustomerEMail.Text = MainForm.CurrentProject.CustomerEmail;
        //    tbCustomerAddress1.Text = MainForm.CurrentProject.CustomerAddress1;
        //    tbCustomerAddress2.Text = MainForm.CurrentProject.CustomerAddress2;
        //    tbCustomerCity.Text = MainForm.CurrentProject.CustomerCity;
        //    tbCustomerState.Text = MainForm.CurrentProject.CustomerState;
        //    tbCustomerZip.Text = MainForm.CurrentProject.CustomerPostalCode;
        //    memoCustomerNotes.Text = MainForm.CurrentProject.CustomerNotes;
        //}

        //private void SetProjectProperties(Project pProject)
        //{
        //    pProject.ProjectName = string.IsNullOrEmpty(cbProjectName.Text.Trim()) ? string.Empty : this.cbProjectName.Text.Trim();
        //    pProject.ProjectNumber = string.IsNullOrEmpty(tbProjectNumber.Text.Trim()) ? string.Empty : this.tbProjectNumber.Text.Trim();
        //    pProject.GISProjectName = string.IsNullOrEmpty(tbProjectGISName.Text.Trim()) ? string.Empty : this.tbProjectGISName.Text.Trim();
        //    pProject.ProjectLead = string.IsNullOrEmpty(tbProjectLead.Text.Trim()) ? string.Empty : this.tbProjectLead.Text.Trim();
        //    pProject.SaleName = string.IsNullOrEmpty(tbSaleName.Text.Trim()) ? string.Empty : this.tbSaleName.Text.Trim();
        //    //pProject.ProjectDate = string.IsNullOrEmpty(textBoxProjectDate.Text.Trim()) ? string.Empty : this.textBoxProjectDate.Text.Trim();
        //    pProject.ProjectNotes = string.IsNullOrEmpty(memoProjectNotes.Text.Trim()) ? string.Empty : this.memoProjectNotes.Text.Trim();

        //    pProject.CustomerCompanyName = string.IsNullOrEmpty(tbCustomerName.Text.Trim()) ? string.Empty : this.tbCustomerName.Text.Trim();
        //    //MainForm.CurrentProject.CustomerContactName = string.IsNullOrEmpty(radTextBox34.Text.Trim()) ? string.Empty : this.radTextBox34.Text.Trim();
        //    pProject.CustomerOfficePhone = string.IsNullOrEmpty(tbCustomerOfficePhone.Text.Trim()) ? string.Empty : this.tbCustomerOfficePhone.Text.Trim();
        //    pProject.CustomerCellPhone = string.IsNullOrEmpty(tbCustomerCellPhone.Text.Trim()) ? string.Empty : this.tbCustomerCellPhone.Text.Trim();
        //    pProject.CustomerFax = string.IsNullOrEmpty(tbCustomerFax.Text.Trim()) ? string.Empty : this.tbCustomerFax.Text.Trim();
        //    pProject.CustomerEmail = string.IsNullOrEmpty(tbCustomerEMail.Text.Trim()) ? string.Empty : this.tbCustomerEMail.Text.Trim();
        //    pProject.CustomerAddress1 = string.IsNullOrEmpty(tbCustomerAddress1.Text.Trim()) ? string.Empty : this.tbCustomerAddress1.Text.Trim();
        //    pProject.CustomerAddress2 = string.IsNullOrEmpty(tbCustomerAddress2.Text.Trim()) ? string.Empty : this.tbCustomerAddress2.Text.Trim();
        //    pProject.CustomerCity = string.IsNullOrEmpty(tbCustomerCity.Text.Trim()) ? string.Empty : this.tbCustomerCity.Text.Trim();
        //    pProject.CustomerState = string.IsNullOrEmpty(tbCustomerState.Text.Trim()) ? string.Empty : this.tbCustomerState.Text.Trim();
        //    pProject.CustomerPostalCode = string.IsNullOrEmpty(tbCustomerZip.Text.Trim()) ? string.Empty : this.tbCustomerZip.Text.Trim();
        //    pProject.CustomerNotes = string.IsNullOrEmpty(memoCustomerNotes.Text.Trim()) ? string.Empty : this.memoCustomerNotes.Text.Trim();
        //}

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.ShowDialog();
            tbProjectLocation.Text = string.Format("{0}\\", frm.SelectedPath);

            PopulateProjectDropDown(frm.SelectedPath);
            
            frm.Dispose();
        }

        private void PopulateProjectDropDown(string p)
        {
            string[] files = Directory.GetFiles(p, "*.sdf");
            foreach (var item in files)
            {
                string filename = Path.GetFileName(item).Replace(".sdf", "");
                if (!ProjectsBLL.DoesProjectExist(ref locContext, filename))
                {
                    switch (filename)
                    { 
                        case "Projects":
                        case "Empty":
                        case "Temp":
                            break;
                        default:
                            this.ProjectNameComboBoxEdit.Properties.Items.Add(filename);
                            break;
                    }
                }
            }
        }

        private void ProjectEditForm_Shown(object sender, EventArgs e)
        {
            ProjectNameComboBoxEdit.Focus();
        }

        private void ProjectNameComboBoxEdit_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(ProjectNameComboBoxEdit.Text.Trim()))
            {
                dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "Project Name is required");
                e.Cancel = true;
            }
            else
            {
                dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "");
                e.Cancel = false;
            }
        }

        private void buttonEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.ShowDialog();
            tbProjectLocation.Text = string.Format("{0}\\", frm.SelectedPath);

            PopulateProjectDropDown(frm.SelectedPath);

            frm.Dispose();
        }
    }
}