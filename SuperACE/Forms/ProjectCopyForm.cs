﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Projects.DAL;
using System.IO;
using Project.DAL;

namespace SuperACE
{
    public partial class ProjectCopyForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext = null;
        private string ProjectDataSource = string.Empty;

        public ProjectCopyForm(ref ProjectsDbContext pLocContext)
        {
            InitializeComponent();

            locContext = pLocContext;
        }

        private void ProjectCopyForm_Load(object sender, EventArgs e)
        {
            List<Projects.DAL.AllProject> projects = locContext.AllProjects.OrderBy(p => p.ProjectName).ToList();
            foreach (var item in projects)
		        cbOrigProject.Properties.Items.Add(item.ProjectName);
        }

        private void cbOrigProject_SelectedValueChanged(object sender, EventArgs e)
        {
            cbDestinationProject.Enabled = true;
            tbDestinationFolder.Enabled = true;

            Projects.DAL.AllProject rec = locContext.AllProjects.FirstOrDefault(p => p.ProjectName == cbOrigProject.Text);
            tbOriginalFolder.Text = rec.ProjectLocation;

            List<Projects.DAL.AllProject> projects = locContext.AllProjects.OrderBy(p => p.ProjectName).ToList();
            foreach (var item in projects)
            {
                if (item.ProjectName != cbOrigProject.Text)
		            cbDestinationProject.Properties.Items.Add(item.ProjectName);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Process
            // if new project exists
            if (File.Exists(string.Format("{0}{1}.sdf", tbDestinationFolder.Text,  cbDestinationProject.Text)))
            {
                XtraMessageBox.Show("Project already exists. \nPlease rename the project to continue.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                // Create the new project database from New.sdf
                File.Copy(string.Format("{0}{1}.sdf",  tbOriginalFolder.Text, cbOrigProject.Text),
                    string.Format("{0}{1}.sdf", tbDestinationFolder.Text, cbDestinationProject.Text));
                ProjectDataSource = string.Format("{0}{1}.sdf", tbDestinationFolder.Text, cbDestinationProject.Text);
            }
            catch (UnauthorizedAccessException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FileNotFoundException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Problem with Coping Project\nCopy " + string.Format("{0}{1}.sdf", tbOriginalFolder, cbOrigProject.Text) + " " + string.Format("{0}{1}.sdf", tbDestinationFolder.Text, cbDestinationProject.Text), "Error");
                return;
            }

            // add the new project to Project.DAL.Project Locations database
            Projects.DAL.AllProject newPrjLocation = new Projects.DAL.AllProject();
            newPrjLocation.ProjectName = cbDestinationProject.Text;
            newPrjLocation.ProjectLocation = tbDestinationFolder.Text;

            try
            {
                ProjectsBLL.SaveProjectLocation(ref locContext, newPrjLocation); // prjLocContext.SaveChanges();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + ex, "Error", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.None;
                return;
            }

            ProjectDbContext ctx = new ProjectDbContext("Data Source = " + ProjectDataSource);
            ctx.Configuration.AutoDetectChangesEnabled = false;
            Project.DAL.Project rec = ProjectBLL.GetProject(ref ctx);
            rec.ProjectName = cbDestinationProject.Text;
            ctx.SaveChanges();
            ctx.Dispose();
            ctx = null;

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void cbDestinationProject_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbDestinationProject.Text))
            {
                tbDestinationFolder.Text = Properties.Settings.Default["DbLocation"].ToString();
            }
        }

        private void tbDestinationFolder_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = Properties.Settings.Default["DbLocation"].ToString();
            frm.ShowDialog();

            if (!string.IsNullOrEmpty(frm.SelectedPath))
            {
                tbDestinationFolder.Text = frm.SelectedPath + "\\";
            }
            frm.Dispose();
        }

        private void cbDestinationProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Projects.DAL.AllProject rec = locContext.AllProjects.FirstOrDefault(p => p.ProjectName == cbOrigProject.Text);
            tbDestinationFolder.Text = rec.ProjectLocation;
        }
    }
}