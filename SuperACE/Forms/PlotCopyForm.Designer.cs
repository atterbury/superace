﻿namespace SuperACE
{
    partial class PlotCopyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tbSection = new DevExpress.XtraEditors.TextEdit();
            this.tbRange = new DevExpress.XtraEditors.TextEdit();
            this.tbTownship = new DevExpress.XtraEditors.TextEdit();
            this.tbStand = new DevExpress.XtraEditors.TextEdit();
            this.tbTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.tbOriginalFolder = new DevExpress.XtraEditors.TextEdit();
            this.tbDestinationFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbDestinationProject = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbOrigProject = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tempCopyPlotsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPlot2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlot10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.tempCopyStandsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOriginalFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDestinationFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDestinationProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrigProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyPlotsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyStandsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Process";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(541, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 419);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(541, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 395);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(541, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 395);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tbSection);
            this.panelControl1.Controls.Add(this.tbRange);
            this.panelControl1.Controls.Add(this.tbTownship);
            this.panelControl1.Controls.Add(this.tbStand);
            this.panelControl1.Controls.Add(this.tbTract);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.tbOriginalFolder);
            this.panelControl1.Controls.Add(this.tbDestinationFolder);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cbDestinationProject);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.tbOrigProject);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(541, 214);
            this.panelControl1.TabIndex = 20;
            // 
            // tbSection
            // 
            this.tbSection.EnterMoveNextControl = true;
            this.tbSection.Location = new System.Drawing.Point(460, 107);
            this.tbSection.MenuManager = this.barManager1;
            this.tbSection.Name = "tbSection";
            this.tbSection.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSection.Size = new System.Drawing.Size(35, 20);
            this.tbSection.TabIndex = 33;
            this.tbSection.TextChanged += new System.EventHandler(this.tbSection_TextChanged);
            this.tbSection.Validating += new System.ComponentModel.CancelEventHandler(this.tbSection_Validating);
            // 
            // tbRange
            // 
            this.tbRange.EnterMoveNextControl = true;
            this.tbRange.Location = new System.Drawing.Point(365, 107);
            this.tbRange.MenuManager = this.barManager1;
            this.tbRange.Name = "tbRange";
            this.tbRange.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbRange.Size = new System.Drawing.Size(35, 20);
            this.tbRange.TabIndex = 32;
            this.tbRange.TextChanged += new System.EventHandler(this.tbRange_TextChanged);
            this.tbRange.Validating += new System.ComponentModel.CancelEventHandler(this.tbRange_Validating);
            // 
            // tbTownship
            // 
            this.tbTownship.EnterMoveNextControl = true;
            this.tbTownship.Location = new System.Drawing.Point(270, 107);
            this.tbTownship.MenuManager = this.barManager1;
            this.tbTownship.Name = "tbTownship";
            this.tbTownship.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTownship.Size = new System.Drawing.Size(35, 20);
            this.tbTownship.TabIndex = 31;
            this.tbTownship.TextChanged += new System.EventHandler(this.tbTownship_TextChanged);
            this.tbTownship.Validating += new System.ComponentModel.CancelEventHandler(this.tbTownship_Validating);
            // 
            // tbStand
            // 
            this.tbStand.EnterMoveNextControl = true;
            this.tbStand.Location = new System.Drawing.Point(166, 107);
            this.tbStand.MenuManager = this.barManager1;
            this.tbStand.Name = "tbStand";
            this.tbStand.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbStand.Size = new System.Drawing.Size(44, 20);
            this.tbStand.TabIndex = 30;
            this.tbStand.TextChanged += new System.EventHandler(this.tbStand_TextChanged);
            this.tbStand.Validating += new System.ComponentModel.CancelEventHandler(this.tbStand_Validating);
            // 
            // tbTract
            // 
            this.tbTract.EnterMoveNextControl = true;
            this.tbTract.Location = new System.Drawing.Point(11, 107);
            this.tbTract.MenuManager = this.barManager1;
            this.tbTract.Name = "tbTract";
            this.tbTract.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTract.Size = new System.Drawing.Size(95, 20);
            this.tbTract.TabIndex = 29;
            this.tbTract.TextChanged += new System.EventHandler(this.tbTract_TextChanged);
            this.tbTract.Validating += new System.ComponentModel.CancelEventHandler(this.tbTract_Validating);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(470, 88);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(17, 13);
            this.labelControl9.TabIndex = 28;
            this.labelControl9.Text = "Sec";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(374, 88);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(19, 13);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "Rge";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(277, 88);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(20, 13);
            this.labelControl7.TabIndex = 26;
            this.labelControl7.Text = "Twn";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(176, 88);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(28, 13);
            this.labelControl6.TabIndex = 25;
            this.labelControl6.Text = "Stand";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(46, 88);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(25, 13);
            this.labelControl5.TabIndex = 24;
            this.labelControl5.Text = "Tract";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(33, 45);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 13);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Original Folder:";
            // 
            // tbOriginalFolder
            // 
            this.tbOriginalFolder.Enabled = false;
            this.tbOriginalFolder.Location = new System.Drawing.Point(119, 42);
            this.tbOriginalFolder.MenuManager = this.barManager1;
            this.tbOriginalFolder.Name = "tbOriginalFolder";
            this.tbOriginalFolder.Size = new System.Drawing.Size(376, 20);
            this.tbOriginalFolder.TabIndex = 22;
            // 
            // tbDestinationFolder
            // 
            this.tbDestinationFolder.Enabled = false;
            this.tbDestinationFolder.Location = new System.Drawing.Point(119, 172);
            this.tbDestinationFolder.MenuManager = this.barManager1;
            this.tbDestinationFolder.Name = "tbDestinationFolder";
            this.tbDestinationFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tbDestinationFolder.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.tbDestinationFolder_Properties_ButtonClick);
            this.tbDestinationFolder.Size = new System.Drawing.Size(376, 20);
            this.tbDestinationFolder.TabIndex = 21;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 175);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(91, 13);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Destination Folder:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 149);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(95, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Destination Project:";
            // 
            // cbDestinationProject
            // 
            this.cbDestinationProject.Enabled = false;
            this.cbDestinationProject.Location = new System.Drawing.Point(119, 146);
            this.cbDestinationProject.MenuManager = this.barManager1;
            this.cbDestinationProject.Name = "cbDestinationProject";
            this.cbDestinationProject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDestinationProject.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbDestinationProject.Size = new System.Drawing.Size(188, 20);
            this.cbDestinationProject.TabIndex = 18;
            this.cbDestinationProject.SelectedValueChanged += new System.EventHandler(this.cbDestinationProject_SelectedValueChanged);
            this.cbDestinationProject.Validating += new System.ComponentModel.CancelEventHandler(this.cbDestinationProject_Validating);
            this.cbDestinationProject.Validated += new System.EventHandler(this.cbDestinationProject_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(29, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(77, 13);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Original Project:";
            // 
            // tbOrigProject
            // 
            this.tbOrigProject.Enabled = false;
            this.tbOrigProject.Location = new System.Drawing.Point(119, 16);
            this.tbOrigProject.MenuManager = this.barManager1;
            this.tbOrigProject.Name = "tbOrigProject";
            this.tbOrigProject.Size = new System.Drawing.Size(188, 20);
            this.tbOrigProject.TabIndex = 16;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 238);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(541, 181);
            this.panelControl2.TabIndex = 21;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tempCopyPlotsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(537, 177);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // tempCopyPlotsBindingSource
            // 
            this.tempCopyPlotsBindingSource.DataSource = typeof(Temp.DAL.TempCopyPlot);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colId,
            this.colPlot1,
            this.colPlot2,
            this.colPlot3,
            this.colPlot4,
            this.colPlot5,
            this.colPlot6,
            this.colPlot7,
            this.colPlot8,
            this.colPlot9,
            this.colPlot10,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.bandedGridView1.OptionsCustomization.AllowColumnMoving = false;
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.bandedGridView1_RowCellStyle);
            this.bandedGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bandedGridView1_KeyDown);
            this.bandedGridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.bandedGridView1_ValidatingEditor);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Plots";
            this.gridBand1.Columns.Add(this.colId);
            this.gridBand1.Columns.Add(this.colPlot1);
            this.gridBand1.Columns.Add(this.colPlot2);
            this.gridBand1.Columns.Add(this.colPlot3);
            this.gridBand1.Columns.Add(this.colPlot4);
            this.gridBand1.Columns.Add(this.colPlot5);
            this.gridBand1.Columns.Add(this.colPlot6);
            this.gridBand1.Columns.Add(this.colPlot7);
            this.gridBand1.Columns.Add(this.colPlot8);
            this.gridBand1.Columns.Add(this.colPlot9);
            this.gridBand1.Columns.Add(this.colPlot10);
            this.gridBand1.Columns.Add(this.gridColumn1);
            this.gridBand1.Columns.Add(this.gridColumn2);
            this.gridBand1.Columns.Add(this.gridColumn3);
            this.gridBand1.Columns.Add(this.gridColumn4);
            this.gridBand1.Columns.Add(this.gridColumn5);
            this.gridBand1.Columns.Add(this.gridColumn6);
            this.gridBand1.Columns.Add(this.gridColumn7);
            this.gridBand1.Columns.Add(this.gridColumn8);
            this.gridBand1.Columns.Add(this.gridColumn9);
            this.gridBand1.Columns.Add(this.gridColumn10);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 750;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colPlot1
            // 
            this.colPlot1.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot1.FieldName = "Plot1";
            this.colPlot1.Name = "colPlot1";
            this.colPlot1.Visible = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.MaxLength = 4;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colPlot2
            // 
            this.colPlot2.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot2.FieldName = "Plot2";
            this.colPlot2.Name = "colPlot2";
            this.colPlot2.Visible = true;
            // 
            // colPlot3
            // 
            this.colPlot3.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot3.FieldName = "Plot3";
            this.colPlot3.Name = "colPlot3";
            this.colPlot3.Visible = true;
            // 
            // colPlot4
            // 
            this.colPlot4.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot4.FieldName = "Plot4";
            this.colPlot4.Name = "colPlot4";
            this.colPlot4.Visible = true;
            // 
            // colPlot5
            // 
            this.colPlot5.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot5.FieldName = "Plot5";
            this.colPlot5.Name = "colPlot5";
            this.colPlot5.Visible = true;
            // 
            // colPlot6
            // 
            this.colPlot6.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot6.FieldName = "Plot6";
            this.colPlot6.Name = "colPlot6";
            this.colPlot6.Visible = true;
            // 
            // colPlot7
            // 
            this.colPlot7.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot7.FieldName = "Plot7";
            this.colPlot7.Name = "colPlot7";
            this.colPlot7.Visible = true;
            // 
            // colPlot8
            // 
            this.colPlot8.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot8.FieldName = "Plot8";
            this.colPlot8.Name = "colPlot8";
            this.colPlot8.Visible = true;
            // 
            // colPlot9
            // 
            this.colPlot9.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot9.FieldName = "Plot9";
            this.colPlot9.Name = "colPlot9";
            this.colPlot9.Visible = true;
            // 
            // colPlot10
            // 
            this.colPlot10.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPlot10.FieldName = "Plot10";
            this.colPlot10.Name = "colPlot10";
            this.colPlot10.Visible = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "Plot1Exist";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.FieldName = "Plot2Exist";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.FieldName = "Plot3Exist";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "gridColumn4";
            this.gridColumn4.FieldName = "Plot4Exist";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "gridColumn5";
            this.gridColumn5.FieldName = "Plot5Exist";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "gridColumn6";
            this.gridColumn6.FieldName = "Plot6Exist";
            this.gridColumn6.Name = "gridColumn6";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "gridColumn7";
            this.gridColumn7.FieldName = "Plot7Exist";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "gridColumn8";
            this.gridColumn8.FieldName = "Plot8Exist";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "gridColumn9";
            this.gridColumn9.FieldName = "Plot9Exist";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "gridColumn10";
            this.gridColumn10.FieldName = "Plot10Exist";
            this.gridColumn10.Name = "gridColumn10";
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "bandedGridColumn1";
            this.bandedGridColumn1.FieldName = "OrigPlot1";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "bandedGridColumn2";
            this.bandedGridColumn2.FieldName = "OrigPlot2";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "bandedGridColumn3";
            this.bandedGridColumn3.FieldName = "OrigPlot3";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "bandedGridColumn4";
            this.bandedGridColumn4.FieldName = "OrigPlot4";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "bandedGridColumn5";
            this.bandedGridColumn5.FieldName = "OrigPlot5";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "bandedGridColumn6";
            this.bandedGridColumn6.FieldName = "OrigPlot6";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "bandedGridColumn7";
            this.bandedGridColumn7.FieldName = "OrigPlot7";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "bandedGridColumn8";
            this.bandedGridColumn8.FieldName = "OrigPlot8";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "bandedGridColumn9";
            this.bandedGridColumn9.FieldName = "OrigPlot9";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "bandedGridColumn10";
            this.bandedGridColumn10.FieldName = "OrigPlot10";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            // 
            // tempCopyStandsBindingSource
            // 
            this.tempCopyStandsBindingSource.DataSource = typeof(Temp.DAL.TempCopyStand);
            // 
            // PlotCopyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 419);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlotCopyForm";
            this.Text = "Plot Copy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StandCopyForm_FormClosing);
            this.Load += new System.EventHandler(this.PlotCopyForm_Load);
            this.Shown += new System.EventHandler(this.PlotCopyForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOriginalFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDestinationFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDestinationProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrigProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyPlotsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyStandsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.BindingSource tempCopyStandsBindingSource;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit tbOriginalFolder;
        private DevExpress.XtraEditors.ButtonEdit tbDestinationFolder;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbDestinationProject;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbOrigProject;
        private System.Windows.Forms.BindingSource tempCopyPlotsBindingSource;
        private DevExpress.XtraEditors.TextEdit tbSection;
        private DevExpress.XtraEditors.TextEdit tbRange;
        private DevExpress.XtraEditors.TextEdit tbTownship;
        private DevExpress.XtraEditors.TextEdit tbStand;
        private DevExpress.XtraEditors.TextEdit tbTract;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlot10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
    }
}