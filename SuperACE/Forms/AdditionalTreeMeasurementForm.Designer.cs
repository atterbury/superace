﻿namespace SuperACE
{
    partial class AdditionalTreeMeasurementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.tbSection = new DevExpress.XtraEditors.TextEdit();
            this.tbRange = new DevExpress.XtraEditors.TextEdit();
            this.tbTownship = new DevExpress.XtraEditors.TextEdit();
            this.tbStand = new DevExpress.XtraEditors.TextEdit();
            this.tbTract = new DevExpress.XtraEditors.TextEdit();
            this.tbCounty = new DevExpress.XtraEditors.TextEdit();
            this.tbState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbProject = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.additionalTreeMeasurementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPlotNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAdditionalTreeMeasurementId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIncYears = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStandsId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreesId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSiteTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBearingAzimuth = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDistance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSpecies = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDbh = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCrownPosition = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCrownRatio = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBhAge = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalAge = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDbhInc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHtInc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBt1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBt2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBtr = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCounty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.additionalTreeMeasurementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(780, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 416);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(780, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 392);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(780, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 392);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.tbNetAcres);
            this.panelControl1.Controls.Add(this.tbSection);
            this.panelControl1.Controls.Add(this.tbRange);
            this.panelControl1.Controls.Add(this.tbTownship);
            this.panelControl1.Controls.Add(this.tbStand);
            this.panelControl1.Controls.Add(this.tbTract);
            this.panelControl1.Controls.Add(this.tbCounty);
            this.panelControl1.Controls.Add(this.tbState);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.tbProject);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(780, 60);
            this.panelControl1.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(595, 6);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(47, 13);
            this.labelControl9.TabIndex = 17;
            this.labelControl9.Text = "Net Acres";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(556, 6);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(17, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Sec";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(510, 6);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(19, 13);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Rge";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(458, 6);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(20, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Twn";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(393, 6);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(39, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Stand #";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(292, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(25, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Tract";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(189, 6);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(17, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Cty";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(147, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(10, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "St";
            // 
            // tbNetAcres
            // 
            this.tbNetAcres.Location = new System.Drawing.Point(587, 25);
            this.tbNetAcres.MenuManager = this.barManager1;
            this.tbNetAcres.Name = "tbNetAcres";
            this.tbNetAcres.Properties.ReadOnly = true;
            this.tbNetAcres.Size = new System.Drawing.Size(59, 20);
            this.tbNetAcres.TabIndex = 9;
            // 
            // tbSection
            // 
            this.tbSection.Location = new System.Drawing.Point(550, 25);
            this.tbSection.MenuManager = this.barManager1;
            this.tbSection.Name = "tbSection";
            this.tbSection.Properties.ReadOnly = true;
            this.tbSection.Size = new System.Drawing.Size(31, 20);
            this.tbSection.TabIndex = 8;
            // 
            // tbRange
            // 
            this.tbRange.Location = new System.Drawing.Point(498, 25);
            this.tbRange.MenuManager = this.barManager1;
            this.tbRange.Name = "tbRange";
            this.tbRange.Properties.ReadOnly = true;
            this.tbRange.Size = new System.Drawing.Size(46, 20);
            this.tbRange.TabIndex = 7;
            // 
            // tbTownship
            // 
            this.tbTownship.Location = new System.Drawing.Point(446, 25);
            this.tbTownship.MenuManager = this.barManager1;
            this.tbTownship.Name = "tbTownship";
            this.tbTownship.Properties.ReadOnly = true;
            this.tbTownship.Size = new System.Drawing.Size(46, 20);
            this.tbTownship.TabIndex = 6;
            // 
            // tbStand
            // 
            this.tbStand.Location = new System.Drawing.Point(385, 25);
            this.tbStand.MenuManager = this.barManager1;
            this.tbStand.Name = "tbStand";
            this.tbStand.Properties.ReadOnly = true;
            this.tbStand.Size = new System.Drawing.Size(55, 20);
            this.tbStand.TabIndex = 5;
            // 
            // tbTract
            // 
            this.tbTract.Location = new System.Drawing.Point(237, 25);
            this.tbTract.MenuManager = this.barManager1;
            this.tbTract.Name = "tbTract";
            this.tbTract.Properties.ReadOnly = true;
            this.tbTract.Size = new System.Drawing.Size(142, 20);
            this.tbTract.TabIndex = 4;
            // 
            // tbCounty
            // 
            this.tbCounty.Location = new System.Drawing.Point(180, 25);
            this.tbCounty.MenuManager = this.barManager1;
            this.tbCounty.Name = "tbCounty";
            this.tbCounty.Properties.ReadOnly = true;
            this.tbCounty.Size = new System.Drawing.Size(37, 20);
            this.tbCounty.TabIndex = 3;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(137, 25);
            this.tbState.MenuManager = this.barManager1;
            this.tbState.Name = "tbState";
            this.tbState.Properties.ReadOnly = true;
            this.tbState.Size = new System.Drawing.Size(37, 20);
            this.tbState.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(40, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Project";
            // 
            // tbProject
            // 
            this.tbProject.Location = new System.Drawing.Point(5, 25);
            this.tbProject.MenuManager = this.barManager1;
            this.tbProject.Name = "tbProject";
            this.tbProject.Properties.ReadOnly = true;
            this.tbProject.Size = new System.Drawing.Size(115, 20);
            this.tbProject.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 84);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 332);
            this.panelControl2.TabIndex = 14;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.additionalTreeMeasurementBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(776, 328);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            this.gridControl1.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControl1_EditorKeyDown);
            // 
            // additionalTreeMeasurementBindingSource
            // 
            this.additionalTreeMeasurementBindingSource.DataSource = typeof(Project.DAL.AdditionalTreeMeasurement);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6});
            this.bandedGridView1.ColumnPanelRowHeight = 50;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colAdditionalTreeMeasurementId,
            this.colSiteTree,
            this.colBearingAzimuth,
            this.colDistance,
            this.colSpecies,
            this.colDbh,
            this.colTotalHeight,
            this.colCrownPosition,
            this.colCrownRatio,
            this.colBhAge,
            this.colTotalAge,
            this.colIncYears,
            this.colDbhInc,
            this.colHtInc,
            this.colBt1,
            this.colBt2,
            this.colBtr,
            this.colStandsId,
            this.colPlotNumber,
            this.colTreeNumber,
            this.colTreesId});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.bandedGridView1.OptionsCustomization.AllowBandMoving = false;
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bandedGridView1_KeyDown);
            this.bandedGridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.bandedGridView1_ValidatingEditor);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Location";
            this.gridBand1.Columns.Add(this.colPlotNumber);
            this.gridBand1.Columns.Add(this.colTreeNumber);
            this.gridBand1.Columns.Add(this.colAdditionalTreeMeasurementId);
            this.gridBand1.Columns.Add(this.colIncYears);
            this.gridBand1.Columns.Add(this.colStandsId);
            this.gridBand1.Columns.Add(this.colTreesId);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 80;
            // 
            // colPlotNumber
            // 
            this.colPlotNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotNumber.Caption = "Plot #";
            this.colPlotNumber.FieldName = "PlotNumber";
            this.colPlotNumber.Name = "colPlotNumber";
            this.colPlotNumber.Visible = true;
            this.colPlotNumber.Width = 42;
            // 
            // colTreeNumber
            // 
            this.colTreeNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeNumber.Caption = "Tree #";
            this.colTreeNumber.FieldName = "TreeNumber";
            this.colTreeNumber.Name = "colTreeNumber";
            this.colTreeNumber.Visible = true;
            this.colTreeNumber.Width = 38;
            // 
            // colAdditionalTreeMeasurementId
            // 
            this.colAdditionalTreeMeasurementId.FieldName = "AdditionalTreeMeasurementId";
            this.colAdditionalTreeMeasurementId.Name = "colAdditionalTreeMeasurementId";
            this.colAdditionalTreeMeasurementId.Width = 36;
            // 
            // colIncYears
            // 
            this.colIncYears.AppearanceHeader.Options.UseTextOptions = true;
            this.colIncYears.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIncYears.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colIncYears.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIncYears.Caption = "Inc Yr";
            this.colIncYears.FieldName = "IncYears";
            this.colIncYears.Name = "colIncYears";
            // 
            // colStandsId
            // 
            this.colStandsId.FieldName = "StandsId";
            this.colStandsId.Name = "colStandsId";
            this.colStandsId.Width = 33;
            // 
            // colTreesId
            // 
            this.colTreesId.FieldName = "TreesId";
            this.colTreesId.Name = "colTreesId";
            this.colTreesId.Width = 65;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Tree Location";
            this.gridBand2.Columns.Add(this.colSiteTree);
            this.gridBand2.Columns.Add(this.colBearingAzimuth);
            this.gridBand2.Columns.Add(this.colDistance);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 157;
            // 
            // colSiteTree
            // 
            this.colSiteTree.AppearanceHeader.Options.UseTextOptions = true;
            this.colSiteTree.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSiteTree.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSiteTree.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSiteTree.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSiteTree.FieldName = "SiteTree";
            this.colSiteTree.Name = "colSiteTree";
            this.colSiteTree.Visible = true;
            this.colSiteTree.Width = 31;
            // 
            // colBearingAzimuth
            // 
            this.colBearingAzimuth.AppearanceHeader.Options.UseTextOptions = true;
            this.colBearingAzimuth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBearingAzimuth.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBearingAzimuth.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBearingAzimuth.Caption = "Bearing or Azimuth";
            this.colBearingAzimuth.ColumnEdit = this.repositoryItemTextEdit1;
            this.colBearingAzimuth.FieldName = "BearingAzimuth";
            this.colBearingAzimuth.Name = "colBearingAzimuth";
            this.colBearingAzimuth.Visible = true;
            this.colBearingAzimuth.Width = 57;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Distance (feet,tens)";
            this.colDistance.DisplayFormat.FormatString = "n1";
            this.colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.Visible = true;
            this.colDistance.Width = 69;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Tree Measurements";
            this.gridBand3.Columns.Add(this.colSpecies);
            this.gridBand3.Columns.Add(this.colDbh);
            this.gridBand3.Columns.Add(this.colTotalHeight);
            this.gridBand3.Columns.Add(this.colCrownPosition);
            this.gridBand3.Columns.Add(this.colCrownRatio);
            this.gridBand3.Columns.Add(this.colBhAge);
            this.gridBand3.Columns.Add(this.colTotalAge);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 264;
            // 
            // colSpecies
            // 
            this.colSpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.Width = 49;
            // 
            // colDbh
            // 
            this.colDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDbh.Caption = "Dbh inches";
            this.colDbh.DisplayFormat.FormatString = "n1";
            this.colDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDbh.FieldName = "Dbh";
            this.colDbh.Name = "colDbh";
            this.colDbh.Visible = true;
            this.colDbh.Width = 42;
            // 
            // colTotalHeight
            // 
            this.colTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalHeight.Caption = "Total Ht";
            this.colTotalHeight.FieldName = "TotalHeight";
            this.colTotalHeight.Name = "colTotalHeight";
            this.colTotalHeight.Visible = true;
            this.colTotalHeight.Width = 39;
            // 
            // colCrownPosition
            // 
            this.colCrownPosition.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownPosition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownPosition.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownPosition.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownPosition.Caption = "CP";
            this.colCrownPosition.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCrownPosition.FieldName = "CrownPosition";
            this.colCrownPosition.Name = "colCrownPosition";
            this.colCrownPosition.Visible = true;
            this.colCrownPosition.Width = 24;
            // 
            // colCrownRatio
            // 
            this.colCrownRatio.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownRatio.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownRatio.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownRatio.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownRatio.Caption = "CR";
            this.colCrownRatio.FieldName = "CrownRatio";
            this.colCrownRatio.Name = "colCrownRatio";
            this.colCrownRatio.Visible = true;
            this.colCrownRatio.Width = 25;
            // 
            // colBhAge
            // 
            this.colBhAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colBhAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBhAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBhAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBhAge.Caption = "Bh Age";
            this.colBhAge.FieldName = "BhAge";
            this.colBhAge.Name = "colBhAge";
            this.colBhAge.Visible = true;
            this.colBhAge.Width = 30;
            // 
            // colTotalAge
            // 
            this.colTotalAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTotalAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAge.Caption = "Total Age";
            this.colTotalAge.FieldName = "TotalAge";
            this.colTotalAge.Name = "colTotalAge";
            this.colTotalAge.Visible = true;
            this.colTotalAge.Width = 55;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Growth";
            this.gridBand4.Columns.Add(this.colDbhInc);
            this.gridBand4.Columns.Add(this.colHtInc);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 81;
            // 
            // colDbhInc
            // 
            this.colDbhInc.AppearanceHeader.Options.UseTextOptions = true;
            this.colDbhInc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDbhInc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDbhInc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDbhInc.Caption = "Dbh Inc Yr";
            this.colDbhInc.DisplayFormat.FormatString = "n1";
            this.colDbhInc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDbhInc.FieldName = "DbhInc";
            this.colDbhInc.Name = "colDbhInc";
            this.colDbhInc.Visible = true;
            this.colDbhInc.Width = 29;
            // 
            // colHtInc
            // 
            this.colHtInc.AppearanceHeader.Options.UseTextOptions = true;
            this.colHtInc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHtInc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHtInc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHtInc.Caption = "Ht Inc";
            this.colHtInc.FieldName = "HtInc";
            this.colHtInc.Name = "colHtInc";
            this.colHtInc.Visible = true;
            this.colHtInc.Width = 52;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Bark";
            this.gridBand5.Columns.Add(this.colBt1);
            this.gridBand5.Columns.Add(this.colBt2);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 77;
            // 
            // colBt1
            // 
            this.colBt1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBt1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBt1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBt1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBt1.DisplayFormat.FormatString = "n2";
            this.colBt1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBt1.FieldName = "Bt1";
            this.colBt1.Name = "colBt1";
            this.colBt1.Visible = true;
            this.colBt1.Width = 32;
            // 
            // colBt2
            // 
            this.colBt2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBt2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBt2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBt2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBt2.DisplayFormat.FormatString = "n2";
            this.colBt2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBt2.FieldName = "Bt2";
            this.colBt2.Name = "colBt2";
            this.colBt2.Visible = true;
            this.colBt2.Width = 45;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Calc";
            this.gridBand6.Columns.Add(this.colBtr);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 55;
            // 
            // colBtr
            // 
            this.colBtr.AppearanceHeader.Options.UseTextOptions = true;
            this.colBtr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBtr.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBtr.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBtr.DisplayFormat.FormatString = "n3";
            this.colBtr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBtr.FieldName = "Btr";
            this.colBtr.Name = "colBtr";
            this.colBtr.Visible = true;
            this.colBtr.Width = 55;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // AdditionalTreeMeasurementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 416);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdditionalTreeMeasurementForm";
            this.Text = "Add New Plot";
            this.Load += new System.EventHandler(this.AdditionalTreeMeasurementForm_Load);
            this.Shown += new System.EventHandler(this.AdditionalTreeMeasurementForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCounty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.additionalTreeMeasurementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbProject;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbNetAcres;
        private DevExpress.XtraEditors.TextEdit tbSection;
        private DevExpress.XtraEditors.TextEdit tbRange;
        private DevExpress.XtraEditors.TextEdit tbTownship;
        private DevExpress.XtraEditors.TextEdit tbStand;
        private DevExpress.XtraEditors.TextEdit tbTract;
        private DevExpress.XtraEditors.TextEdit tbCounty;
        private DevExpress.XtraEditors.TextEdit tbState;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource additionalTreeMeasurementBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAdditionalTreeMeasurementId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIncYears;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreesId;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSiteTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBearingAzimuth;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDistance;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecies;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbh;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownPosition;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownRatio;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBhAge;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalAge;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbhInc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHtInc;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBt1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBt2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBtr;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}