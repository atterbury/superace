﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SuperACE
{
    public partial class ProjectExistsForm : DevExpress.XtraEditors.XtraForm
    {
        public bool bReplace = false;
        public bool bRename = false;
        public bool bProcess = false;
        public string renameProject = string.Empty;

        public ProjectExistsForm()
        {
            InitializeComponent();
        }

        private void ProjectExistsForm_Load(object sender, EventArgs e)
        {
            
        }

        private void rbtnReplace_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnReplace.Checked)
                tbNewProjectName.Enabled = false;
            else
                tbNewProjectName.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (rbtnRename.Checked)
            {
                if (string.IsNullOrEmpty(tbNewProjectName.Text))
                {
                    XtraMessageBox.Show("New Project Name is required to rename.", "Error", MessageBoxButtons.OK);
                    tbNewProjectName.Focus();
                    return;
                }
            }

            bReplace = rbtnReplace.Checked;
            bRename = rbtnRename.Checked;
            if (bRename)
                renameProject = tbNewProjectName.Text;
            bProcess = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bProcess = false;
            this.Close();
        }

        private void rbtnRename_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnRename.Checked)
                tbNewProjectName.Enabled = true;
            else
                tbNewProjectName.Enabled = false;
        }
    }
}