﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;
using System.IO;
using Projects.DAL;
using System.Data.Entity.Core;
using Project.DAL;

namespace SuperACE
{
    public partial class PlotAddForm : DevExpress.XtraEditors.XtraForm
    {
        private string plot = string.Empty;
        private Plot newPlot = null;
        private Stand currentStand = null;
        private Project.DAL.Project currentProject = null;
        private ProjectDbContext projectContext = null;

        public int plotID = -1;

        public PlotAddForm(ref ProjectDbContext pPrjCtx, Project.DAL.Project pProject, Stand pStand, string pPlot)
        {
            InitializeComponent();

            plot = pPlot;
            projectContext = pPrjCtx;
            currentStand = pStand;
            currentProject = pProject;
        }

        private void PlotAddForm_Load(object sender, EventArgs e)
        {
            this.Text = "Add Plot";
            tbProject.Text = currentProject.ProjectName;
            tbState.Text = currentStand.State;
            tbCounty.Text = currentStand.County;
            tbTract.Text = currentStand.TractName;
            tbStand.Text = currentStand.StandName;
            tbTownship.Text = currentStand.Township;
            tbRange.Text = currentStand.Range;
            tbSection.Text = currentStand.Section;
            tbNetAcres.Text = currentStand.NetGeographicAcres.ToString();

            speciesBindingSource.Clear();
            speciesBindingSource.DataSource = projectContext.Species.OrderBy(s => s.Abbreviation).Where(s => s.TableName == currentProject.SpeciesTableName).ToList();

            aspectBindingSource.Clear();
            aspectBindingSource.DataSource = projectContext.Aspects.OrderBy(s => s.DisplayCode).Where(s => s.TableName == currentProject.AspectsTableName).ToList();

            habitatBindingSource.Clear();
            habitatBindingSource.DataSource = projectContext.Habitats.OrderBy(s => s.DisplayCode).Where(s => s.TableName == currentProject.HabitatTableName).ToList();

            envBindingSource.Clear();
            envBindingSource.DataSource = projectContext.Environments.OrderBy(s => s.DisplayCode).Where(s => s.TableName == currentProject.EnvironmentsTableName).ToList();

            AddPlot();

            BindControlsToPlot();
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void BindControlsToPlot()
        {
            UserPlotNumberTextEdit.Text = newPlot.UserPlotNumber;
            AddFlagComboBoxEdit.Text =  newPlot.AddFlag;
            AspectLookUpEdit.Text = newPlot.Aspect;
            BafInAtComboBoxEdit.Text = newPlot.BafInAt;
            CruFlagComboBoxEdit.Text = newPlot.CruFlag;
            CruiserTextEdit.Text = newPlot.Cruiser;
            DWFlagComboBoxEdit.Text = newPlot.DwFlag;
            EnvironmentalLookUpEdit.Text = newPlot.Environmental;
            HabitatLookUpEdit.Text = newPlot.Habitat;
            NotesTextEdit.Text = newPlot.Notes;
            PlotDateDateEdit.Text = newPlot.PlotDate.Value.ToShortDateString();
            RefFlagComboBoxEdit.Text = newPlot.RefFlag;
            SlopeLookUpEdit.Text = newPlot.Slope;
            SpeciesLookUpEdit.Text = newPlot.Species;
            newPlot.UserPlotNumber = plot;
            XTextEdit.Text = newPlot.X.ToString();
            YTextEdit.Text = newPlot.Y.ToString();
            ZTextEdit.Text = newPlot.Z.ToString();
        }

        private void AddPlot()
        {
            newPlot = new Plot();
            newPlot.AddFlag = string.Empty;
            newPlot.Aspect = string.Empty;
            newPlot.Attachments = false;
            newPlot.BafInAt = "FP";
            newPlot.BasalAreaAcre = 0;
            newPlot.CruFlag = "Y";
            newPlot.Cruiser = string.Empty;
            newPlot.DwFlag = string.Empty;
            newPlot.Environmental = string.Empty;
            newPlot.Habitat = string.Empty;
            newPlot.NetBfAcre = 0;
            newPlot.NetCfAcre = 0;
            newPlot.Notes = string.Empty;
            newPlot.PlotDate = DateTime.Now;
            newPlot.RefFlag = string.Empty;
            newPlot.Slope = string.Empty;
            newPlot.Species = string.Empty;
            newPlot.StandsId = currentStand.StandsId;
            newPlot.TreatmentDisplayCode = string.Empty;
            newPlot.TreesPerAcre = 0;
            newPlot.UserPlotNumber = plot;
            newPlot.X = 0;
            newPlot.Y = 0;
            newPlot.Z = 0;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            plotBindingSource.EndEdit();
            if (!CheckProperties())
                return;

            try
            {
                projectContext.Plots.Add(newPlot);
                projectContext.SaveChanges();
            }
            catch (Exception ex)
            {
                this.HandleExceptionOnSaveChanges(ex.Message);
                return;
            }

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private bool CheckProperties()
        {
            bool bReturn = true;

            //if (string.IsNullOrEmpty(ProjectNameComboBoxEdit.Text.Trim()))
            //{
            //    dxErrorProvider1.SetError(ProjectNameComboBoxEdit, "Project Name is required");
            //    bReturn = false;
            //}
            //if (string.IsNullOrEmpty(tbProjectLocation.Text.Trim()))
            //{
            //    dxErrorProvider1.SetError(tbProjectLocation, "Project Location is required");
            //    bReturn = false;
            //}
            return bReturn;
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            XtraMessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        private void PlotAddForm_Shown(object sender, EventArgs e)
        {
            //ProjectNameComboBoxEdit.Focus();
        }
    }
}