﻿namespace SuperACE
{
    partial class XtraForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlTrees = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridViewTrees = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTreesId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStandsId2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotsId1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemUpper = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTreeNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotFactorInput = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGroupPlot = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGroupTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAge = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcTotalHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcTotalHtUsedYN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPFValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colAgeCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSpeciesAbbreviation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeStatusDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDbh = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFormPoint = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colReportedFormFactor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTopDiameterFractionCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBoleHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCrownPositionDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCrownRatioDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVigorDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDamageDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUserDefinedDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemFormPoint = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemFormFactor = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGrade = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlTrees
            // 
            this.gridControlTrees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTrees.Location = new System.Drawing.Point(0, 0);
            this.gridControlTrees.MainView = this.advBandedGridViewTrees;
            this.gridControlTrees.Name = "gridControlTrees";
            this.gridControlTrees.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItem2,
            this.repositoryItemFormPoint,
            this.repositoryItemFormFactor,
            this.repositoryItemGrade,
            this.repositoryItem1,
            this.repositoryItemUpper,
            this.repositoryItem3});
            this.gridControlTrees.Size = new System.Drawing.Size(1219, 532);
            this.gridControlTrees.TabIndex = 3;
            this.gridControlTrees.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridViewTrees});
            // 
            // advBandedGridViewTrees
            // 
            this.advBandedGridViewTrees.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand6,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23});
            this.advBandedGridViewTrees.ColumnPanelRowHeight = 40;
            this.advBandedGridViewTrees.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colTreesId,
            this.colStandsId2,
            this.colPlotsId1,
            this.colPlotNumber,
            this.colTreeNumber,
            this.colPlotFactorInput,
            this.colAgeCode,
            this.colSpeciesAbbreviation,
            this.colTreeStatusDisplayCode,
            this.colTreeCount,
            this.colDbh,
            this.colFormPoint,
            this.colReportedFormFactor,
            this.colTopDiameterFractionCode,
            this.colBoleHeight,
            this.colTotalHeight,
            this.colCrownPositionDisplayCode,
            this.colCrownRatioDisplayCode,
            this.colVigorDisplayCode,
            this.colDamageDisplayCode,
            this.colUserDefinedDisplayCode,
            this.colSort1,
            this.colGrade1,
            this.colLength1,
            this.colBdFtLD1,
            this.colBdFtDD1,
            this.colCuFtLD1,
            this.colCuFtDD1,
            this.colBdFtPD1,
            this.colSort2,
            this.colGrade2,
            this.colLength2,
            this.colBdFtLD2,
            this.colBdFtDD2,
            this.colCuFtLD2,
            this.colCuFtDD2,
            this.colBdFtPD2,
            this.colSort3,
            this.colGrade3,
            this.colLength3,
            this.colBdFtLD3,
            this.colBdFtDD3,
            this.colCuFtLD3,
            this.colCuFtDD3,
            this.colSort4,
            this.colGrade4,
            this.colLength4,
            this.colBdFtLD4,
            this.colBdFtDD4,
            this.colCuFtLD4,
            this.colCuFtDD4,
            this.colSort5,
            this.colGrade5,
            this.colLength5,
            this.colBdFtLD5,
            this.colBdFtDD5,
            this.colCuFtLD5,
            this.colCuFtDD5,
            this.colSort6,
            this.colGrade6,
            this.colLength6,
            this.colBdFtLD6,
            this.colBdFtDD6,
            this.colCuFtLD6,
            this.colCuFtDD6,
            this.colSort7,
            this.colGrade7,
            this.colLength7,
            this.colBdFtLD7,
            this.colBdFtDD7,
            this.colCuFtLD7,
            this.colCuFtDD7,
            this.colSort8,
            this.colGrade8,
            this.colLength8,
            this.colBdFtLD8,
            this.colBdFtDD8,
            this.colCuFtLD8,
            this.colCuFtDD8,
            this.colSort9,
            this.colGrade9,
            this.colLength9,
            this.colBdFtLD9,
            this.colBdFtDD9,
            this.colCuFtLD9,
            this.colCuFtDD9,
            this.colSort10,
            this.colGrade10,
            this.colLength10,
            this.colBdFtLD10,
            this.colBdFtDD10,
            this.colCuFtLD10,
            this.colCuFtDD10,
            this.colSort11,
            this.colGrade11,
            this.colLength11,
            this.colBdFtLD11,
            this.colBdFtDD11,
            this.colCuFtLD11,
            this.colCuFtDD11,
            this.colSort12,
            this.colGrade12,
            this.colLength12,
            this.colBdFtLD12,
            this.colBdFtDD12,
            this.colCuFtLD12,
            this.colCuFtDD12,
            this.colGroupPlot,
            this.colGroupTree,
            this.colTreeType,
            this.colAge,
            this.colBdFtPD3,
            this.colBdFtPD4,
            this.colBdFtPD5,
            this.colBdFtPD6,
            this.colBdFtPD7,
            this.colBdFtPD8,
            this.colBdFtPD9,
            this.colBdFtPD10,
            this.colBdFtPD11,
            this.colBdFtPD12,
            this.colCalcTotalHeight,
            this.colCalcTotalHtUsedYN,
            this.colPFValue});
            this.advBandedGridViewTrees.GridControl = this.gridControlTrees;
            this.advBandedGridViewTrees.GroupCount = 1;
            this.advBandedGridViewTrees.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "PlotId", this.colTreeNumber, "")});
            this.advBandedGridViewTrees.Name = "advBandedGridViewTrees";
            this.advBandedGridViewTrees.OptionsCustomization.AllowFilter = false;
            this.advBandedGridViewTrees.OptionsCustomization.AllowSort = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowColumnMRUFilterList = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowFilterEditor = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowMRUFilterList = false;
            this.advBandedGridViewTrees.OptionsNavigation.EnterMoveNextColumn = true;
            this.advBandedGridViewTrees.OptionsView.ShowGroupPanel = false;
            this.advBandedGridViewTrees.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPlotsId1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridBand1
            // 
            this.gridBand1.Columns.Add(this.colTreesId);
            this.gridBand1.Columns.Add(this.colStandsId2);
            this.gridBand1.Columns.Add(this.colPlotsId1);
            this.gridBand1.Columns.Add(this.colPlotNumber);
            this.gridBand1.Columns.Add(this.colTreeNumber);
            this.gridBand1.Columns.Add(this.colPlotFactorInput);
            this.gridBand1.Columns.Add(this.colGroupPlot);
            this.gridBand1.Columns.Add(this.colGroupTree);
            this.gridBand1.Columns.Add(this.colTreeType);
            this.gridBand1.Columns.Add(this.colAge);
            this.gridBand1.Columns.Add(this.colCalcTotalHeight);
            this.gridBand1.Columns.Add(this.colCalcTotalHtUsedYN);
            this.gridBand1.Columns.Add(this.colPFValue);
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 108;
            // 
            // colTreesId
            // 
            this.colTreesId.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreesId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreesId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreesId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreesId.FieldName = "TreesId";
            this.colTreesId.Name = "colTreesId";
            // 
            // colStandsId2
            // 
            this.colStandsId2.AppearanceHeader.Options.UseTextOptions = true;
            this.colStandsId2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStandsId2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStandsId2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStandsId2.FieldName = "StandsId";
            this.colStandsId2.Name = "colStandsId2";
            // 
            // colPlotsId1
            // 
            this.colPlotsId1.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotsId1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotsId1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotsId1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotsId1.FieldName = "PlotsId";
            this.colPlotsId1.Name = "colPlotsId1";
            // 
            // colPlotNumber
            // 
            this.colPlotNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotNumber.Caption = "Plot #";
            this.colPlotNumber.ColumnEdit = this.repositoryItemUpper;
            this.colPlotNumber.FieldName = "PlotNumber";
            this.colPlotNumber.Name = "colPlotNumber";
            this.colPlotNumber.Visible = true;
            this.colPlotNumber.Width = 41;
            // 
            // repositoryItemUpper
            // 
            this.repositoryItemUpper.AutoHeight = false;
            this.repositoryItemUpper.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemUpper.Name = "repositoryItemUpper";
            // 
            // colTreeNumber
            // 
            this.colTreeNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeNumber.Caption = "Tree #";
            this.colTreeNumber.DisplayFormat.FormatString = "{0:#}";
            this.colTreeNumber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeNumber.FieldName = "TreeNumber";
            this.colTreeNumber.Name = "colTreeNumber";
            this.colTreeNumber.Visible = true;
            this.colTreeNumber.Width = 41;
            // 
            // colPlotFactorInput
            // 
            this.colPlotFactorInput.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotFactorInput.Caption = "PF";
            this.colPlotFactorInput.ColumnEdit = this.repositoryItem2;
            this.colPlotFactorInput.FieldName = "PlotFactorInput";
            this.colPlotFactorInput.Name = "colPlotFactorInput";
            this.colPlotFactorInput.Visible = true;
            this.colPlotFactorInput.Width = 26;
            // 
            // repositoryItem2
            // 
            this.repositoryItem2.AutoHeight = false;
            this.repositoryItem2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem2.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItem2.MaxLength = 2;
            this.repositoryItem2.Name = "repositoryItem2";
            // 
            // colGroupPlot
            // 
            this.colGroupPlot.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupPlot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupPlot.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGroupPlot.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGroupPlot.FieldName = "GroupPlot";
            this.colGroupPlot.Name = "colGroupPlot";
            // 
            // colGroupTree
            // 
            this.colGroupTree.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupTree.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupTree.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGroupTree.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGroupTree.FieldName = "GroupTree";
            this.colGroupTree.Name = "colGroupTree";
            // 
            // colTreeType
            // 
            this.colTreeType.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            // 
            // colAge
            // 
            this.colAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAge.FieldName = "Age";
            this.colAge.Name = "colAge";
            // 
            // colCalcTotalHeight
            // 
            this.colCalcTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTotalHeight.FieldName = "CalcTotalHeight";
            this.colCalcTotalHeight.Name = "colCalcTotalHeight";
            // 
            // colCalcTotalHtUsedYN
            // 
            this.colCalcTotalHtUsedYN.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTotalHtUsedYN.FieldName = "CalcTotalHtUsedYn";
            this.colCalcTotalHtUsedYN.Name = "colCalcTotalHtUsedYN";
            // 
            // colPFValue
            // 
            this.colPFValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colPFValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPFValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPFValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPFValue.FieldName = "PFValue";
            this.colPFValue.Name = "colPFValue";
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Identification";
            this.gridBand6.Columns.Add(this.colAgeCode);
            this.gridBand6.Columns.Add(this.colSpeciesAbbreviation);
            this.gridBand6.Columns.Add(this.colTreeStatusDisplayCode);
            this.gridBand6.Columns.Add(this.colTreeCount);
            this.gridBand6.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 115;
            // 
            // colAgeCode
            // 
            this.colAgeCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgeCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgeCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAgeCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgeCode.Caption = "Age";
            this.colAgeCode.ColumnEdit = this.repositoryItem1;
            this.colAgeCode.DisplayFormat.FormatString = "{0:#}";
            this.colAgeCode.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAgeCode.FieldName = "AgeCode";
            this.colAgeCode.Name = "colAgeCode";
            this.colAgeCode.Visible = true;
            this.colAgeCode.Width = 29;
            // 
            // repositoryItem1
            // 
            this.repositoryItem1.AutoHeight = false;
            this.repositoryItem1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem1.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItem1.MaxLength = 1;
            this.repositoryItem1.Name = "repositoryItem1";
            // 
            // colSpeciesAbbreviation
            // 
            this.colSpeciesAbbreviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeciesAbbreviation.Caption = "Spp";
            this.colSpeciesAbbreviation.ColumnEdit = this.repositoryItemUpper;
            this.colSpeciesAbbreviation.FieldName = "SpeciesAbbreviation";
            this.colSpeciesAbbreviation.Name = "colSpeciesAbbreviation";
            this.colSpeciesAbbreviation.Visible = true;
            this.colSpeciesAbbreviation.Width = 34;
            // 
            // colTreeStatusDisplayCode
            // 
            this.colTreeStatusDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeStatusDisplayCode.Caption = "St";
            this.colTreeStatusDisplayCode.ColumnEdit = this.repositoryItemUpper;
            this.colTreeStatusDisplayCode.FieldName = "TreeStatusDisplayCode";
            this.colTreeStatusDisplayCode.Name = "colTreeStatusDisplayCode";
            this.colTreeStatusDisplayCode.Visible = true;
            this.colTreeStatusDisplayCode.Width = 24;
            // 
            // colTreeCount
            // 
            this.colTreeCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeCount.Caption = "Ct";
            this.colTreeCount.FieldName = "TreeCount";
            this.colTreeCount.Name = "colTreeCount";
            this.colTreeCount.Visible = true;
            this.colTreeCount.Width = 28;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Measurements";
            this.gridBand10.Columns.Add(this.colDbh);
            this.gridBand10.Columns.Add(this.colFormPoint);
            this.gridBand10.Columns.Add(this.colReportedFormFactor);
            this.gridBand10.Columns.Add(this.colTopDiameterFractionCode);
            this.gridBand10.Columns.Add(this.colBoleHeight);
            this.gridBand10.Columns.Add(this.colTotalHeight);
            this.gridBand10.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 179;
            // 
            // colDbh
            // 
            this.colDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDbh.Caption = "Dbh";
            this.colDbh.DisplayFormat.FormatString = "{0:0.0}";
            this.colDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDbh.FieldName = "Dbh";
            this.colDbh.Name = "colDbh";
            this.colDbh.Visible = true;
            this.colDbh.Width = 36;
            // 
            // colFormPoint
            // 
            this.colFormPoint.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormPoint.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormPoint.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormPoint.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormPoint.Caption = "FP";
            this.colFormPoint.ColumnEdit = this.repositoryItem2;
            this.colFormPoint.DisplayFormat.FormatString = "{0:#}";
            this.colFormPoint.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFormPoint.FieldName = "FormPoint";
            this.colFormPoint.Name = "colFormPoint";
            this.colFormPoint.Visible = true;
            this.colFormPoint.Width = 20;
            // 
            // colReportedFormFactor
            // 
            this.colReportedFormFactor.AppearanceHeader.Options.UseTextOptions = true;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colReportedFormFactor.Caption = "FF";
            this.colReportedFormFactor.ColumnEdit = this.repositoryItem2;
            this.colReportedFormFactor.DisplayFormat.FormatString = "{0:#}";
            this.colReportedFormFactor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colReportedFormFactor.FieldName = "ReportedFormFactor";
            this.colReportedFormFactor.Name = "colReportedFormFactor";
            this.colReportedFormFactor.Visible = true;
            this.colReportedFormFactor.Width = 20;
            // 
            // colTopDiameterFractionCode
            // 
            this.colTopDiameterFractionCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTopDiameterFractionCode.Caption = "TDF";
            this.colTopDiameterFractionCode.ColumnEdit = this.repositoryItem2;
            this.colTopDiameterFractionCode.FieldName = "TopDiameterFractionCode";
            this.colTopDiameterFractionCode.Name = "colTopDiameterFractionCode";
            this.colTopDiameterFractionCode.Visible = true;
            this.colTopDiameterFractionCode.Width = 29;
            // 
            // colBoleHeight
            // 
            this.colBoleHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colBoleHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBoleHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBoleHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBoleHeight.Caption = "Bole Ht";
            this.colBoleHeight.ColumnEdit = this.repositoryItem3;
            this.colBoleHeight.DisplayFormat.FormatString = "{0:#}";
            this.colBoleHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBoleHeight.FieldName = "BoleHeight";
            this.colBoleHeight.Name = "colBoleHeight";
            this.colBoleHeight.Visible = true;
            this.colBoleHeight.Width = 34;
            // 
            // repositoryItem3
            // 
            this.repositoryItem3.AutoHeight = false;
            this.repositoryItem3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem3.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItem3.MaxLength = 3;
            this.repositoryItem3.Name = "repositoryItem3";
            // 
            // colTotalHeight
            // 
            this.colTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalHeight.Caption = "Total Ht";
            this.colTotalHeight.ColumnEdit = this.repositoryItem3;
            this.colTotalHeight.DisplayFormat.FormatString = "{0:#}";
            this.colTotalHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalHeight.FieldName = "TotalHeight";
            this.colTotalHeight.Name = "colTotalHeight";
            this.colTotalHeight.Visible = true;
            this.colTotalHeight.Width = 40;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Classification";
            this.gridBand11.Columns.Add(this.colCrownPositionDisplayCode);
            this.gridBand11.Columns.Add(this.colCrownRatioDisplayCode);
            this.gridBand11.Columns.Add(this.colVigorDisplayCode);
            this.gridBand11.Columns.Add(this.colDamageDisplayCode);
            this.gridBand11.Columns.Add(this.colUserDefinedDisplayCode);
            this.gridBand11.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 3;
            this.gridBand11.Width = 115;
            // 
            // colCrownPositionDisplayCode
            // 
            this.colCrownPositionDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownPositionDisplayCode.Caption = "PO";
            this.colCrownPositionDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colCrownPositionDisplayCode.FieldName = "CrownPositionDisplayCode";
            this.colCrownPositionDisplayCode.Name = "colCrownPositionDisplayCode";
            this.colCrownPositionDisplayCode.Visible = true;
            this.colCrownPositionDisplayCode.Width = 26;
            // 
            // colCrownRatioDisplayCode
            // 
            this.colCrownRatioDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownRatioDisplayCode.Caption = "CR";
            this.colCrownRatioDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colCrownRatioDisplayCode.FieldName = "CrownRatioDisplayCode";
            this.colCrownRatioDisplayCode.Name = "colCrownRatioDisplayCode";
            this.colCrownRatioDisplayCode.Visible = true;
            this.colCrownRatioDisplayCode.Width = 24;
            // 
            // colVigorDisplayCode
            // 
            this.colVigorDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVigorDisplayCode.Caption = "VI";
            this.colVigorDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colVigorDisplayCode.FieldName = "VigorDisplayCode";
            this.colVigorDisplayCode.Name = "colVigorDisplayCode";
            this.colVigorDisplayCode.Visible = true;
            this.colVigorDisplayCode.Width = 21;
            // 
            // colDamageDisplayCode
            // 
            this.colDamageDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDamageDisplayCode.Caption = "DA";
            this.colDamageDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colDamageDisplayCode.FieldName = "DamageDisplayCode";
            this.colDamageDisplayCode.Name = "colDamageDisplayCode";
            this.colDamageDisplayCode.Visible = true;
            this.colDamageDisplayCode.Width = 22;
            // 
            // colUserDefinedDisplayCode
            // 
            this.colUserDefinedDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserDefinedDisplayCode.Caption = "UD";
            this.colUserDefinedDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colUserDefinedDisplayCode.FieldName = "UserDefinedDisplayCode";
            this.colUserDefinedDisplayCode.Name = "colUserDefinedDisplayCode";
            this.colUserDefinedDisplayCode.Visible = true;
            this.colUserDefinedDisplayCode.Width = 22;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Segment 1";
            this.gridBand12.Columns.Add(this.colSort1);
            this.gridBand12.Columns.Add(this.colGrade1);
            this.gridBand12.Columns.Add(this.colLength1);
            this.gridBand12.Columns.Add(this.colBdFtLD1);
            this.gridBand12.Columns.Add(this.colBdFtDD1);
            this.gridBand12.Columns.Add(this.colCuFtLD1);
            this.gridBand12.Columns.Add(this.colCuFtDD1);
            this.gridBand12.Columns.Add(this.colBdFtPD1);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 4;
            this.gridBand12.Width = 166;
            // 
            // colSort1
            // 
            this.colSort1.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort1.Caption = "S";
            this.colSort1.ColumnEdit = this.repositoryItem1;
            this.colSort1.FieldName = "Sort1";
            this.colSort1.Name = "colSort1";
            this.colSort1.Visible = true;
            this.colSort1.Width = 20;
            // 
            // colGrade1
            // 
            this.colGrade1.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade1.Caption = "G";
            this.colGrade1.ColumnEdit = this.repositoryItem1;
            this.colGrade1.FieldName = "Grade1";
            this.colGrade1.Name = "colGrade1";
            this.colGrade1.Visible = true;
            this.colGrade1.Width = 20;
            // 
            // colLength1
            // 
            this.colLength1.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength1.Caption = "Ln";
            this.colLength1.ColumnEdit = this.repositoryItem2;
            this.colLength1.FieldName = "Length1";
            this.colLength1.Name = "colLength1";
            this.colLength1.Visible = true;
            this.colLength1.Width = 20;
            // 
            // colBdFtLD1
            // 
            this.colBdFtLD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD1.Caption = "F";
            this.colBdFtLD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD1.FieldName = "BdFtLd1";
            this.colBdFtLD1.Name = "colBdFtLD1";
            this.colBdFtLD1.Visible = true;
            this.colBdFtLD1.Width = 20;
            // 
            // colBdFtDD1
            // 
            this.colBdFtDD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD1.Caption = "I";
            this.colBdFtDD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD1.FieldName = "BdFtDd1";
            this.colBdFtDD1.Name = "colBdFtDD1";
            this.colBdFtDD1.Visible = true;
            this.colBdFtDD1.Width = 20;
            // 
            // colCuFtLD1
            // 
            this.colCuFtLD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD1.Caption = "F";
            this.colCuFtLD1.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD1.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD1.FieldName = "CuFtLd1";
            this.colCuFtLD1.Name = "colCuFtLD1";
            this.colCuFtLD1.Visible = true;
            this.colCuFtLD1.Width = 20;
            // 
            // colCuFtDD1
            // 
            this.colCuFtDD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD1.Caption = "I";
            this.colCuFtDD1.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD1.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD1.FieldName = "CuFtDd1";
            this.colCuFtDD1.Name = "colCuFtDD1";
            this.colCuFtDD1.Visible = true;
            this.colCuFtDD1.Width = 20;
            // 
            // colBdFtPD1
            // 
            this.colBdFtPD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD1.Caption = "%";
            this.colBdFtPD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtPD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD1.FieldName = "BdFtPd1";
            this.colBdFtPD1.Name = "colBdFtPD1";
            this.colBdFtPD1.Visible = true;
            this.colBdFtPD1.Width = 26;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Segment 2";
            this.gridBand13.Columns.Add(this.colSort2);
            this.gridBand13.Columns.Add(this.colGrade2);
            this.gridBand13.Columns.Add(this.colLength2);
            this.gridBand13.Columns.Add(this.colBdFtLD2);
            this.gridBand13.Columns.Add(this.colBdFtDD2);
            this.gridBand13.Columns.Add(this.colCuFtLD2);
            this.gridBand13.Columns.Add(this.colCuFtDD2);
            this.gridBand13.Columns.Add(this.colBdFtPD2);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 5;
            this.gridBand13.Width = 160;
            // 
            // colSort2
            // 
            this.colSort2.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort2.Caption = "S";
            this.colSort2.ColumnEdit = this.repositoryItem1;
            this.colSort2.FieldName = "Sort2";
            this.colSort2.Name = "colSort2";
            this.colSort2.Visible = true;
            this.colSort2.Width = 20;
            // 
            // colGrade2
            // 
            this.colGrade2.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade2.Caption = "G";
            this.colGrade2.ColumnEdit = this.repositoryItem1;
            this.colGrade2.FieldName = "Grade2";
            this.colGrade2.Name = "colGrade2";
            this.colGrade2.Visible = true;
            this.colGrade2.Width = 20;
            // 
            // colLength2
            // 
            this.colLength2.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength2.Caption = "Ln";
            this.colLength2.ColumnEdit = this.repositoryItem2;
            this.colLength2.FieldName = "Length2";
            this.colLength2.Name = "colLength2";
            this.colLength2.Visible = true;
            this.colLength2.Width = 20;
            // 
            // colBdFtLD2
            // 
            this.colBdFtLD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD2.Caption = "F";
            this.colBdFtLD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD2.FieldName = "BdFtLd2";
            this.colBdFtLD2.Name = "colBdFtLD2";
            this.colBdFtLD2.Visible = true;
            this.colBdFtLD2.Width = 20;
            // 
            // colBdFtDD2
            // 
            this.colBdFtDD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD2.Caption = "I";
            this.colBdFtDD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD2.FieldName = "BdFtDd2";
            this.colBdFtDD2.Name = "colBdFtDD2";
            this.colBdFtDD2.Visible = true;
            this.colBdFtDD2.Width = 20;
            // 
            // colCuFtLD2
            // 
            this.colCuFtLD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD2.Caption = "F";
            this.colCuFtLD2.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD2.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD2.FieldName = "CuFtLd2";
            this.colCuFtLD2.Name = "colCuFtLD2";
            this.colCuFtLD2.Visible = true;
            this.colCuFtLD2.Width = 20;
            // 
            // colCuFtDD2
            // 
            this.colCuFtDD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD2.Caption = "I";
            this.colCuFtDD2.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD2.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD2.FieldName = "CuFtDd2";
            this.colCuFtDD2.Name = "colCuFtDD2";
            this.colCuFtDD2.Visible = true;
            this.colCuFtDD2.Width = 20;
            // 
            // colBdFtPD2
            // 
            this.colBdFtPD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD2.Caption = "%";
            this.colBdFtPD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtPD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD2.FieldName = "BdFtPd2";
            this.colBdFtPD2.Name = "colBdFtPD2";
            this.colBdFtPD2.Visible = true;
            this.colBdFtPD2.Width = 20;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Segment 3";
            this.gridBand14.Columns.Add(this.colSort3);
            this.gridBand14.Columns.Add(this.colGrade3);
            this.gridBand14.Columns.Add(this.colLength3);
            this.gridBand14.Columns.Add(this.colBdFtLD3);
            this.gridBand14.Columns.Add(this.colBdFtDD3);
            this.gridBand14.Columns.Add(this.colCuFtLD3);
            this.gridBand14.Columns.Add(this.colCuFtDD3);
            this.gridBand14.Columns.Add(this.colBdFtPD3);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 6;
            this.gridBand14.Width = 160;
            // 
            // colSort3
            // 
            this.colSort3.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort3.Caption = "S";
            this.colSort3.ColumnEdit = this.repositoryItem1;
            this.colSort3.FieldName = "Sort3";
            this.colSort3.Name = "colSort3";
            this.colSort3.Visible = true;
            this.colSort3.Width = 20;
            // 
            // colGrade3
            // 
            this.colGrade3.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade3.Caption = "G";
            this.colGrade3.ColumnEdit = this.repositoryItem1;
            this.colGrade3.FieldName = "Grade3";
            this.colGrade3.Name = "colGrade3";
            this.colGrade3.Visible = true;
            this.colGrade3.Width = 20;
            // 
            // colLength3
            // 
            this.colLength3.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength3.Caption = "Ln";
            this.colLength3.ColumnEdit = this.repositoryItem2;
            this.colLength3.FieldName = "Length3";
            this.colLength3.Name = "colLength3";
            this.colLength3.Visible = true;
            this.colLength3.Width = 20;
            // 
            // colBdFtLD3
            // 
            this.colBdFtLD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD3.Caption = "F";
            this.colBdFtLD3.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD3.FieldName = "BdFtLd3";
            this.colBdFtLD3.Name = "colBdFtLD3";
            this.colBdFtLD3.Visible = true;
            this.colBdFtLD3.Width = 20;
            // 
            // colBdFtDD3
            // 
            this.colBdFtDD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD3.Caption = "I";
            this.colBdFtDD3.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD3.FieldName = "BdFtDd3";
            this.colBdFtDD3.Name = "colBdFtDD3";
            this.colBdFtDD3.Visible = true;
            this.colBdFtDD3.Width = 20;
            // 
            // colCuFtLD3
            // 
            this.colCuFtLD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD3.Caption = "F";
            this.colCuFtLD3.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD3.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD3.FieldName = "CuFtLd3";
            this.colCuFtLD3.Name = "colCuFtLD3";
            this.colCuFtLD3.Visible = true;
            this.colCuFtLD3.Width = 20;
            // 
            // colCuFtDD3
            // 
            this.colCuFtDD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD3.Caption = "I";
            this.colCuFtDD3.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD3.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD3.FieldName = "CuFtDd3";
            this.colCuFtDD3.Name = "colCuFtDD3";
            this.colCuFtDD3.Visible = true;
            this.colCuFtDD3.Width = 20;
            // 
            // colBdFtPD3
            // 
            this.colBdFtPD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD3.Caption = "%";
            this.colBdFtPD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD3.FieldName = "BdFtPd3";
            this.colBdFtPD3.Name = "colBdFtPD3";
            this.colBdFtPD3.Visible = true;
            this.colBdFtPD3.Width = 20;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Segment 4";
            this.gridBand15.Columns.Add(this.colSort4);
            this.gridBand15.Columns.Add(this.colGrade4);
            this.gridBand15.Columns.Add(this.colLength4);
            this.gridBand15.Columns.Add(this.colBdFtLD4);
            this.gridBand15.Columns.Add(this.colBdFtDD4);
            this.gridBand15.Columns.Add(this.colCuFtLD4);
            this.gridBand15.Columns.Add(this.colCuFtDD4);
            this.gridBand15.Columns.Add(this.colBdFtPD4);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 7;
            this.gridBand15.Width = 160;
            // 
            // colSort4
            // 
            this.colSort4.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort4.Caption = "S";
            this.colSort4.ColumnEdit = this.repositoryItem1;
            this.colSort4.FieldName = "Sort4";
            this.colSort4.Name = "colSort4";
            this.colSort4.Visible = true;
            this.colSort4.Width = 20;
            // 
            // colGrade4
            // 
            this.colGrade4.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade4.Caption = "G";
            this.colGrade4.ColumnEdit = this.repositoryItem1;
            this.colGrade4.FieldName = "Grade4";
            this.colGrade4.Name = "colGrade4";
            this.colGrade4.Visible = true;
            this.colGrade4.Width = 20;
            // 
            // colLength4
            // 
            this.colLength4.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength4.Caption = "Ln";
            this.colLength4.ColumnEdit = this.repositoryItem2;
            this.colLength4.FieldName = "Length4";
            this.colLength4.Name = "colLength4";
            this.colLength4.Visible = true;
            this.colLength4.Width = 20;
            // 
            // colBdFtLD4
            // 
            this.colBdFtLD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD4.Caption = "F";
            this.colBdFtLD4.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD4.FieldName = "BdFtLd4";
            this.colBdFtLD4.Name = "colBdFtLD4";
            this.colBdFtLD4.Visible = true;
            this.colBdFtLD4.Width = 20;
            // 
            // colBdFtDD4
            // 
            this.colBdFtDD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD4.Caption = "I";
            this.colBdFtDD4.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD4.FieldName = "BdFtDd4";
            this.colBdFtDD4.Name = "colBdFtDD4";
            this.colBdFtDD4.Visible = true;
            this.colBdFtDD4.Width = 20;
            // 
            // colCuFtLD4
            // 
            this.colCuFtLD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD4.Caption = "F";
            this.colCuFtLD4.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD4.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD4.FieldName = "CuFtLd4";
            this.colCuFtLD4.Name = "colCuFtLD4";
            this.colCuFtLD4.Visible = true;
            this.colCuFtLD4.Width = 20;
            // 
            // colCuFtDD4
            // 
            this.colCuFtDD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD4.Caption = "I";
            this.colCuFtDD4.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD4.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD4.FieldName = "CuFtDd4";
            this.colCuFtDD4.Name = "colCuFtDD4";
            this.colCuFtDD4.Visible = true;
            this.colCuFtDD4.Width = 20;
            // 
            // colBdFtPD4
            // 
            this.colBdFtPD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD4.Caption = "%";
            this.colBdFtPD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD4.FieldName = "BdFtPd4";
            this.colBdFtPD4.Name = "colBdFtPD4";
            this.colBdFtPD4.Visible = true;
            this.colBdFtPD4.Width = 20;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "Segment 5";
            this.gridBand16.Columns.Add(this.colSort5);
            this.gridBand16.Columns.Add(this.colGrade5);
            this.gridBand16.Columns.Add(this.colLength5);
            this.gridBand16.Columns.Add(this.colBdFtLD5);
            this.gridBand16.Columns.Add(this.colBdFtDD5);
            this.gridBand16.Columns.Add(this.colCuFtLD5);
            this.gridBand16.Columns.Add(this.colCuFtDD5);
            this.gridBand16.Columns.Add(this.colBdFtPD5);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 8;
            this.gridBand16.Width = 160;
            // 
            // colSort5
            // 
            this.colSort5.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort5.Caption = "S";
            this.colSort5.ColumnEdit = this.repositoryItem1;
            this.colSort5.FieldName = "Sort5";
            this.colSort5.Name = "colSort5";
            this.colSort5.Visible = true;
            this.colSort5.Width = 20;
            // 
            // colGrade5
            // 
            this.colGrade5.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade5.Caption = "G";
            this.colGrade5.ColumnEdit = this.repositoryItem1;
            this.colGrade5.FieldName = "Grade5";
            this.colGrade5.Name = "colGrade5";
            this.colGrade5.Visible = true;
            this.colGrade5.Width = 20;
            // 
            // colLength5
            // 
            this.colLength5.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength5.Caption = "Ln";
            this.colLength5.ColumnEdit = this.repositoryItem2;
            this.colLength5.FieldName = "Length5";
            this.colLength5.Name = "colLength5";
            this.colLength5.Visible = true;
            this.colLength5.Width = 20;
            // 
            // colBdFtLD5
            // 
            this.colBdFtLD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD5.Caption = "F";
            this.colBdFtLD5.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD5.FieldName = "BdFtLd5";
            this.colBdFtLD5.Name = "colBdFtLD5";
            this.colBdFtLD5.Visible = true;
            this.colBdFtLD5.Width = 20;
            // 
            // colBdFtDD5
            // 
            this.colBdFtDD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD5.Caption = "I";
            this.colBdFtDD5.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD5.FieldName = "BdFtDd5";
            this.colBdFtDD5.Name = "colBdFtDD5";
            this.colBdFtDD5.Visible = true;
            this.colBdFtDD5.Width = 20;
            // 
            // colCuFtLD5
            // 
            this.colCuFtLD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD5.Caption = "F";
            this.colCuFtLD5.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD5.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD5.FieldName = "CuFtLd5";
            this.colCuFtLD5.Name = "colCuFtLD5";
            this.colCuFtLD5.Visible = true;
            this.colCuFtLD5.Width = 20;
            // 
            // colCuFtDD5
            // 
            this.colCuFtDD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD5.Caption = "I";
            this.colCuFtDD5.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD5.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD5.FieldName = "CuFtDd5";
            this.colCuFtDD5.Name = "colCuFtDD5";
            this.colCuFtDD5.Visible = true;
            this.colCuFtDD5.Width = 20;
            // 
            // colBdFtPD5
            // 
            this.colBdFtPD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD5.Caption = "%";
            this.colBdFtPD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD5.FieldName = "BdFtPd5";
            this.colBdFtPD5.Name = "colBdFtPD5";
            this.colBdFtPD5.Visible = true;
            this.colBdFtPD5.Width = 20;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "Segment 6";
            this.gridBand17.Columns.Add(this.colSort6);
            this.gridBand17.Columns.Add(this.colGrade6);
            this.gridBand17.Columns.Add(this.colLength6);
            this.gridBand17.Columns.Add(this.colBdFtLD6);
            this.gridBand17.Columns.Add(this.colBdFtDD6);
            this.gridBand17.Columns.Add(this.colCuFtLD6);
            this.gridBand17.Columns.Add(this.colCuFtDD6);
            this.gridBand17.Columns.Add(this.colBdFtPD6);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 9;
            this.gridBand17.Width = 160;
            // 
            // colSort6
            // 
            this.colSort6.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort6.Caption = "S";
            this.colSort6.ColumnEdit = this.repositoryItem1;
            this.colSort6.FieldName = "Sort6";
            this.colSort6.Name = "colSort6";
            this.colSort6.Visible = true;
            this.colSort6.Width = 20;
            // 
            // colGrade6
            // 
            this.colGrade6.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade6.Caption = "G";
            this.colGrade6.ColumnEdit = this.repositoryItem1;
            this.colGrade6.FieldName = "Grade6";
            this.colGrade6.Name = "colGrade6";
            this.colGrade6.Visible = true;
            this.colGrade6.Width = 20;
            // 
            // colLength6
            // 
            this.colLength6.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength6.Caption = "Ln";
            this.colLength6.ColumnEdit = this.repositoryItem2;
            this.colLength6.FieldName = "Length6";
            this.colLength6.Name = "colLength6";
            this.colLength6.Visible = true;
            this.colLength6.Width = 20;
            // 
            // colBdFtLD6
            // 
            this.colBdFtLD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD6.Caption = "F";
            this.colBdFtLD6.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD6.FieldName = "BdFtLd6";
            this.colBdFtLD6.Name = "colBdFtLD6";
            this.colBdFtLD6.Visible = true;
            this.colBdFtLD6.Width = 20;
            // 
            // colBdFtDD6
            // 
            this.colBdFtDD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD6.Caption = "I";
            this.colBdFtDD6.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD6.FieldName = "BdFtDd6";
            this.colBdFtDD6.Name = "colBdFtDD6";
            this.colBdFtDD6.Visible = true;
            this.colBdFtDD6.Width = 20;
            // 
            // colCuFtLD6
            // 
            this.colCuFtLD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD6.Caption = "F";
            this.colCuFtLD6.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD6.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD6.FieldName = "CuFtLd6";
            this.colCuFtLD6.Name = "colCuFtLD6";
            this.colCuFtLD6.Visible = true;
            this.colCuFtLD6.Width = 20;
            // 
            // colCuFtDD6
            // 
            this.colCuFtDD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD6.Caption = "I";
            this.colCuFtDD6.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD6.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD6.FieldName = "CuFtDd6";
            this.colCuFtDD6.Name = "colCuFtDD6";
            this.colCuFtDD6.Visible = true;
            this.colCuFtDD6.Width = 20;
            // 
            // colBdFtPD6
            // 
            this.colBdFtPD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD6.Caption = "%";
            this.colBdFtPD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD6.FieldName = "BdFtPd6";
            this.colBdFtPD6.Name = "colBdFtPD6";
            this.colBdFtPD6.Visible = true;
            this.colBdFtPD6.Width = 20;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "Segment 7";
            this.gridBand18.Columns.Add(this.colSort7);
            this.gridBand18.Columns.Add(this.colGrade7);
            this.gridBand18.Columns.Add(this.colLength7);
            this.gridBand18.Columns.Add(this.colBdFtLD7);
            this.gridBand18.Columns.Add(this.colBdFtDD7);
            this.gridBand18.Columns.Add(this.colCuFtLD7);
            this.gridBand18.Columns.Add(this.colCuFtDD7);
            this.gridBand18.Columns.Add(this.colBdFtPD7);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 10;
            this.gridBand18.Width = 160;
            // 
            // colSort7
            // 
            this.colSort7.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort7.Caption = "S";
            this.colSort7.ColumnEdit = this.repositoryItem1;
            this.colSort7.FieldName = "Sort7";
            this.colSort7.Name = "colSort7";
            this.colSort7.Visible = true;
            this.colSort7.Width = 20;
            // 
            // colGrade7
            // 
            this.colGrade7.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade7.Caption = "G";
            this.colGrade7.ColumnEdit = this.repositoryItem1;
            this.colGrade7.FieldName = "Grade7";
            this.colGrade7.Name = "colGrade7";
            this.colGrade7.Visible = true;
            this.colGrade7.Width = 20;
            // 
            // colLength7
            // 
            this.colLength7.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength7.Caption = "Ln";
            this.colLength7.ColumnEdit = this.repositoryItem2;
            this.colLength7.FieldName = "Length7";
            this.colLength7.Name = "colLength7";
            this.colLength7.Visible = true;
            this.colLength7.Width = 20;
            // 
            // colBdFtLD7
            // 
            this.colBdFtLD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD7.Caption = "F";
            this.colBdFtLD7.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD7.FieldName = "BdFtLd7";
            this.colBdFtLD7.Name = "colBdFtLD7";
            this.colBdFtLD7.Visible = true;
            this.colBdFtLD7.Width = 20;
            // 
            // colBdFtDD7
            // 
            this.colBdFtDD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD7.Caption = "I";
            this.colBdFtDD7.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD7.FieldName = "BdFtDd7";
            this.colBdFtDD7.Name = "colBdFtDD7";
            this.colBdFtDD7.Visible = true;
            this.colBdFtDD7.Width = 20;
            // 
            // colCuFtLD7
            // 
            this.colCuFtLD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD7.Caption = "F";
            this.colCuFtLD7.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD7.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD7.FieldName = "CuFtLd7";
            this.colCuFtLD7.Name = "colCuFtLD7";
            this.colCuFtLD7.Visible = true;
            this.colCuFtLD7.Width = 20;
            // 
            // colCuFtDD7
            // 
            this.colCuFtDD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD7.Caption = "I";
            this.colCuFtDD7.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD7.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD7.FieldName = "CuFtDd7";
            this.colCuFtDD7.Name = "colCuFtDD7";
            this.colCuFtDD7.Visible = true;
            this.colCuFtDD7.Width = 20;
            // 
            // colBdFtPD7
            // 
            this.colBdFtPD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD7.Caption = "%";
            this.colBdFtPD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD7.FieldName = "BdFtPd7";
            this.colBdFtPD7.Name = "colBdFtPD7";
            this.colBdFtPD7.Visible = true;
            this.colBdFtPD7.Width = 20;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "Segment 8";
            this.gridBand19.Columns.Add(this.colSort8);
            this.gridBand19.Columns.Add(this.colGrade8);
            this.gridBand19.Columns.Add(this.colLength8);
            this.gridBand19.Columns.Add(this.colBdFtLD8);
            this.gridBand19.Columns.Add(this.colBdFtDD8);
            this.gridBand19.Columns.Add(this.colCuFtLD8);
            this.gridBand19.Columns.Add(this.colCuFtDD8);
            this.gridBand19.Columns.Add(this.colBdFtPD8);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 11;
            this.gridBand19.Width = 160;
            // 
            // colSort8
            // 
            this.colSort8.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort8.Caption = "S";
            this.colSort8.ColumnEdit = this.repositoryItem1;
            this.colSort8.FieldName = "Sort8";
            this.colSort8.Name = "colSort8";
            this.colSort8.Visible = true;
            this.colSort8.Width = 20;
            // 
            // colGrade8
            // 
            this.colGrade8.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade8.Caption = "G";
            this.colGrade8.ColumnEdit = this.repositoryItem1;
            this.colGrade8.FieldName = "Grade8";
            this.colGrade8.Name = "colGrade8";
            this.colGrade8.Visible = true;
            this.colGrade8.Width = 20;
            // 
            // colLength8
            // 
            this.colLength8.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength8.Caption = "Ln";
            this.colLength8.ColumnEdit = this.repositoryItem2;
            this.colLength8.FieldName = "Length8";
            this.colLength8.Name = "colLength8";
            this.colLength8.Visible = true;
            this.colLength8.Width = 20;
            // 
            // colBdFtLD8
            // 
            this.colBdFtLD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD8.Caption = "F";
            this.colBdFtLD8.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD8.FieldName = "BdFtLd8";
            this.colBdFtLD8.Name = "colBdFtLD8";
            this.colBdFtLD8.Visible = true;
            this.colBdFtLD8.Width = 20;
            // 
            // colBdFtDD8
            // 
            this.colBdFtDD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD8.Caption = "I";
            this.colBdFtDD8.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD8.FieldName = "BdFtDd8";
            this.colBdFtDD8.Name = "colBdFtDD8";
            this.colBdFtDD8.Visible = true;
            this.colBdFtDD8.Width = 20;
            // 
            // colCuFtLD8
            // 
            this.colCuFtLD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD8.Caption = "F";
            this.colCuFtLD8.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD8.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD8.FieldName = "CuFtLd8";
            this.colCuFtLD8.Name = "colCuFtLD8";
            this.colCuFtLD8.Visible = true;
            this.colCuFtLD8.Width = 20;
            // 
            // colCuFtDD8
            // 
            this.colCuFtDD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD8.Caption = "I";
            this.colCuFtDD8.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD8.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD8.FieldName = "CuFtDd8";
            this.colCuFtDD8.Name = "colCuFtDD8";
            this.colCuFtDD8.Visible = true;
            this.colCuFtDD8.Width = 20;
            // 
            // colBdFtPD8
            // 
            this.colBdFtPD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD8.Caption = "%";
            this.colBdFtPD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD8.FieldName = "BdFtPd8";
            this.colBdFtPD8.Name = "colBdFtPD8";
            this.colBdFtPD8.Visible = true;
            this.colBdFtPD8.Width = 20;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "Segment 9";
            this.gridBand20.Columns.Add(this.colSort9);
            this.gridBand20.Columns.Add(this.colGrade9);
            this.gridBand20.Columns.Add(this.colLength9);
            this.gridBand20.Columns.Add(this.colBdFtLD9);
            this.gridBand20.Columns.Add(this.colBdFtDD9);
            this.gridBand20.Columns.Add(this.colCuFtLD9);
            this.gridBand20.Columns.Add(this.colCuFtDD9);
            this.gridBand20.Columns.Add(this.colBdFtPD9);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 12;
            this.gridBand20.Width = 160;
            // 
            // colSort9
            // 
            this.colSort9.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort9.Caption = "S";
            this.colSort9.ColumnEdit = this.repositoryItem1;
            this.colSort9.FieldName = "Sort9";
            this.colSort9.Name = "colSort9";
            this.colSort9.Visible = true;
            this.colSort9.Width = 20;
            // 
            // colGrade9
            // 
            this.colGrade9.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade9.Caption = "G";
            this.colGrade9.ColumnEdit = this.repositoryItem1;
            this.colGrade9.FieldName = "Grade9";
            this.colGrade9.Name = "colGrade9";
            this.colGrade9.Visible = true;
            this.colGrade9.Width = 20;
            // 
            // colLength9
            // 
            this.colLength9.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength9.Caption = "Ln";
            this.colLength9.ColumnEdit = this.repositoryItem2;
            this.colLength9.FieldName = "Length9";
            this.colLength9.Name = "colLength9";
            this.colLength9.Visible = true;
            this.colLength9.Width = 20;
            // 
            // colBdFtLD9
            // 
            this.colBdFtLD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD9.Caption = "F";
            this.colBdFtLD9.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD9.FieldName = "BdFtLd9";
            this.colBdFtLD9.Name = "colBdFtLD9";
            this.colBdFtLD9.Visible = true;
            this.colBdFtLD9.Width = 20;
            // 
            // colBdFtDD9
            // 
            this.colBdFtDD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD9.Caption = "I";
            this.colBdFtDD9.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD9.FieldName = "BdFtDd9";
            this.colBdFtDD9.Name = "colBdFtDD9";
            this.colBdFtDD9.Visible = true;
            this.colBdFtDD9.Width = 20;
            // 
            // colCuFtLD9
            // 
            this.colCuFtLD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD9.Caption = "F";
            this.colCuFtLD9.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD9.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD9.FieldName = "CuFtLd9";
            this.colCuFtLD9.Name = "colCuFtLD9";
            this.colCuFtLD9.Visible = true;
            this.colCuFtLD9.Width = 20;
            // 
            // colCuFtDD9
            // 
            this.colCuFtDD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD9.Caption = "I";
            this.colCuFtDD9.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD9.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD9.FieldName = "CuFtDd9";
            this.colCuFtDD9.Name = "colCuFtDD9";
            this.colCuFtDD9.Visible = true;
            this.colCuFtDD9.Width = 20;
            // 
            // colBdFtPD9
            // 
            this.colBdFtPD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD9.Caption = "%";
            this.colBdFtPD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD9.FieldName = "BdFtPd9";
            this.colBdFtPD9.Name = "colBdFtPD9";
            this.colBdFtPD9.Visible = true;
            this.colBdFtPD9.Width = 20;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Segment 10";
            this.gridBand21.Columns.Add(this.colSort10);
            this.gridBand21.Columns.Add(this.colGrade10);
            this.gridBand21.Columns.Add(this.colLength10);
            this.gridBand21.Columns.Add(this.colBdFtLD10);
            this.gridBand21.Columns.Add(this.colBdFtDD10);
            this.gridBand21.Columns.Add(this.colCuFtLD10);
            this.gridBand21.Columns.Add(this.colCuFtDD10);
            this.gridBand21.Columns.Add(this.colBdFtPD10);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 13;
            this.gridBand21.Width = 160;
            // 
            // colSort10
            // 
            this.colSort10.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort10.Caption = "S";
            this.colSort10.ColumnEdit = this.repositoryItem1;
            this.colSort10.FieldName = "Sort10";
            this.colSort10.Name = "colSort10";
            this.colSort10.Visible = true;
            this.colSort10.Width = 20;
            // 
            // colGrade10
            // 
            this.colGrade10.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade10.Caption = "G";
            this.colGrade10.ColumnEdit = this.repositoryItem1;
            this.colGrade10.FieldName = "Grade10";
            this.colGrade10.Name = "colGrade10";
            this.colGrade10.Visible = true;
            this.colGrade10.Width = 20;
            // 
            // colLength10
            // 
            this.colLength10.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength10.Caption = "Ln";
            this.colLength10.ColumnEdit = this.repositoryItem2;
            this.colLength10.FieldName = "Length10";
            this.colLength10.Name = "colLength10";
            this.colLength10.Visible = true;
            this.colLength10.Width = 20;
            // 
            // colBdFtLD10
            // 
            this.colBdFtLD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD10.Caption = "F";
            this.colBdFtLD10.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD10.FieldName = "BdFtLd10";
            this.colBdFtLD10.Name = "colBdFtLD10";
            this.colBdFtLD10.Visible = true;
            this.colBdFtLD10.Width = 20;
            // 
            // colBdFtDD10
            // 
            this.colBdFtDD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD10.Caption = "I";
            this.colBdFtDD10.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD10.FieldName = "BdFtDd10";
            this.colBdFtDD10.Name = "colBdFtDD10";
            this.colBdFtDD10.Visible = true;
            this.colBdFtDD10.Width = 20;
            // 
            // colCuFtLD10
            // 
            this.colCuFtLD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD10.Caption = "F";
            this.colCuFtLD10.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD10.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD10.FieldName = "CuFtLd10";
            this.colCuFtLD10.Name = "colCuFtLD10";
            this.colCuFtLD10.Visible = true;
            this.colCuFtLD10.Width = 20;
            // 
            // colCuFtDD10
            // 
            this.colCuFtDD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD10.Caption = "I";
            this.colCuFtDD10.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD10.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD10.FieldName = "CuFtDd10";
            this.colCuFtDD10.Name = "colCuFtDD10";
            this.colCuFtDD10.Visible = true;
            this.colCuFtDD10.Width = 20;
            // 
            // colBdFtPD10
            // 
            this.colBdFtPD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD10.Caption = "%";
            this.colBdFtPD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD10.FieldName = "BdFtPd10";
            this.colBdFtPD10.Name = "colBdFtPD10";
            this.colBdFtPD10.Visible = true;
            this.colBdFtPD10.Width = 20;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "Segment 11";
            this.gridBand22.Columns.Add(this.colSort11);
            this.gridBand22.Columns.Add(this.colGrade11);
            this.gridBand22.Columns.Add(this.colLength11);
            this.gridBand22.Columns.Add(this.colBdFtLD11);
            this.gridBand22.Columns.Add(this.colBdFtDD11);
            this.gridBand22.Columns.Add(this.colCuFtLD11);
            this.gridBand22.Columns.Add(this.colCuFtDD11);
            this.gridBand22.Columns.Add(this.colBdFtPD11);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 14;
            this.gridBand22.Width = 160;
            // 
            // colSort11
            // 
            this.colSort11.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort11.Caption = "S";
            this.colSort11.ColumnEdit = this.repositoryItem1;
            this.colSort11.FieldName = "Sort11";
            this.colSort11.Name = "colSort11";
            this.colSort11.Visible = true;
            this.colSort11.Width = 20;
            // 
            // colGrade11
            // 
            this.colGrade11.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade11.Caption = "G";
            this.colGrade11.ColumnEdit = this.repositoryItem1;
            this.colGrade11.FieldName = "Grade11";
            this.colGrade11.Name = "colGrade11";
            this.colGrade11.Visible = true;
            this.colGrade11.Width = 20;
            // 
            // colLength11
            // 
            this.colLength11.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength11.Caption = "Ln";
            this.colLength11.ColumnEdit = this.repositoryItem2;
            this.colLength11.FieldName = "Length11";
            this.colLength11.Name = "colLength11";
            this.colLength11.Visible = true;
            this.colLength11.Width = 20;
            // 
            // colBdFtLD11
            // 
            this.colBdFtLD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD11.Caption = "F";
            this.colBdFtLD11.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD11.FieldName = "BdFtLd11";
            this.colBdFtLD11.Name = "colBdFtLD11";
            this.colBdFtLD11.Visible = true;
            this.colBdFtLD11.Width = 20;
            // 
            // colBdFtDD11
            // 
            this.colBdFtDD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD11.Caption = "I";
            this.colBdFtDD11.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD11.FieldName = "BdFtDd11";
            this.colBdFtDD11.Name = "colBdFtDD11";
            this.colBdFtDD11.Visible = true;
            this.colBdFtDD11.Width = 20;
            // 
            // colCuFtLD11
            // 
            this.colCuFtLD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD11.Caption = "F";
            this.colCuFtLD11.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD11.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD11.FieldName = "CuFtLd11";
            this.colCuFtLD11.Name = "colCuFtLD11";
            this.colCuFtLD11.Visible = true;
            this.colCuFtLD11.Width = 20;
            // 
            // colCuFtDD11
            // 
            this.colCuFtDD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD11.Caption = "I";
            this.colCuFtDD11.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD11.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD11.FieldName = "CuFtDd11";
            this.colCuFtDD11.Name = "colCuFtDD11";
            this.colCuFtDD11.Visible = true;
            this.colCuFtDD11.Width = 20;
            // 
            // colBdFtPD11
            // 
            this.colBdFtPD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD11.Caption = "%";
            this.colBdFtPD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD11.FieldName = "BdFtPd11";
            this.colBdFtPD11.Name = "colBdFtPD11";
            this.colBdFtPD11.Visible = true;
            this.colBdFtPD11.Width = 20;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "Segment 12";
            this.gridBand23.Columns.Add(this.colSort12);
            this.gridBand23.Columns.Add(this.colGrade12);
            this.gridBand23.Columns.Add(this.colLength12);
            this.gridBand23.Columns.Add(this.colBdFtLD12);
            this.gridBand23.Columns.Add(this.colBdFtDD12);
            this.gridBand23.Columns.Add(this.colCuFtLD12);
            this.gridBand23.Columns.Add(this.colCuFtDD12);
            this.gridBand23.Columns.Add(this.colBdFtPD12);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 15;
            this.gridBand23.Width = 160;
            // 
            // colSort12
            // 
            this.colSort12.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort12.Caption = "S";
            this.colSort12.ColumnEdit = this.repositoryItem1;
            this.colSort12.FieldName = "Sort12";
            this.colSort12.Name = "colSort12";
            this.colSort12.Visible = true;
            this.colSort12.Width = 20;
            // 
            // colGrade12
            // 
            this.colGrade12.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade12.Caption = "G";
            this.colGrade12.ColumnEdit = this.repositoryItem1;
            this.colGrade12.FieldName = "Grade12";
            this.colGrade12.Name = "colGrade12";
            this.colGrade12.Visible = true;
            this.colGrade12.Width = 20;
            // 
            // colLength12
            // 
            this.colLength12.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength12.Caption = "Ln";
            this.colLength12.ColumnEdit = this.repositoryItem2;
            this.colLength12.FieldName = "Length12";
            this.colLength12.Name = "colLength12";
            this.colLength12.Visible = true;
            this.colLength12.Width = 20;
            // 
            // colBdFtLD12
            // 
            this.colBdFtLD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD12.Caption = "F";
            this.colBdFtLD12.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD12.FieldName = "BdFtLd12";
            this.colBdFtLD12.Name = "colBdFtLD12";
            this.colBdFtLD12.Visible = true;
            this.colBdFtLD12.Width = 20;
            // 
            // colBdFtDD12
            // 
            this.colBdFtDD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD12.Caption = "I";
            this.colBdFtDD12.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD12.FieldName = "BdFtDd12";
            this.colBdFtDD12.Name = "colBdFtDD12";
            this.colBdFtDD12.Visible = true;
            this.colBdFtDD12.Width = 20;
            // 
            // colCuFtLD12
            // 
            this.colCuFtLD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD12.Caption = "F";
            this.colCuFtLD12.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD12.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD12.FieldName = "CuFtLd12";
            this.colCuFtLD12.Name = "colCuFtLD12";
            this.colCuFtLD12.Visible = true;
            this.colCuFtLD12.Width = 20;
            // 
            // colCuFtDD12
            // 
            this.colCuFtDD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD12.Caption = "I";
            this.colCuFtDD12.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD12.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD12.FieldName = "CuFtDd12";
            this.colCuFtDD12.Name = "colCuFtDD12";
            this.colCuFtDD12.Visible = true;
            this.colCuFtDD12.Width = 20;
            // 
            // colBdFtPD12
            // 
            this.colBdFtPD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD12.Caption = "%";
            this.colBdFtPD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD12.FieldName = "BdFtPd12";
            this.colBdFtPD12.Name = "colBdFtPD12";
            this.colBdFtPD12.Visible = true;
            this.colBdFtPD12.Width = 20;
            // 
            // repositoryItemFormPoint
            // 
            this.repositoryItemFormPoint.AutoHeight = false;
            this.repositoryItemFormPoint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemFormPoint.MaxLength = 2;
            this.repositoryItemFormPoint.Name = "repositoryItemFormPoint";
            // 
            // repositoryItemFormFactor
            // 
            this.repositoryItemFormFactor.AutoHeight = false;
            this.repositoryItemFormFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemFormFactor.MaxLength = 2;
            this.repositoryItemFormFactor.Name = "repositoryItemFormFactor";
            // 
            // repositoryItemGrade
            // 
            this.repositoryItemGrade.AutoHeight = false;
            this.repositoryItemGrade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemGrade.MaxLength = 1;
            this.repositoryItemGrade.Name = "repositoryItemGrade";
            // 
            // XtraForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 532);
            this.Controls.Add(this.gridControlTrees);
            this.Name = "XtraForm1";
            this.Text = "XtraForm1";
            this.Load += new System.EventHandler(this.XtraForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlTrees;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridViewTrees;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreesId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotsId1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemUpper;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotFactorInput;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGroupPlot;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGroupTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAge;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHtUsedYN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPFValue;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgeCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpeciesAbbreviation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeStatusDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbh;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFormPoint;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colReportedFormFactor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTopDiameterFractionCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBoleHeight;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownPositionDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownRatioDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVigorDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDamageDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUserDefinedDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD12;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemFormPoint;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemFormFactor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemGrade;
    }
}