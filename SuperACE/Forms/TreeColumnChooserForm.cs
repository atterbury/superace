﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;

using Projects.DAL;

namespace SuperACE
{
    public partial class TreeColumnChooserForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectsDbContext locContext;
        private string colTable = null;

        public TreeColumnChooserForm(ref ProjectsDbContext pContext, string pTable)
        {
            InitializeComponent();

            colTable = pTable;
            locContext = pContext;
        }

        private void TreeColumnChooser_Load(object sender, EventArgs e)
        {
            //db = new ProjectLocationModelEntities(); // ProjectLocationBLL.GetConnectionString(MainForm.ProjectLocationDataSource)
            locContext.ColumnsTrees.Load();
            columnsTreeBindingSource.DataSource = locContext.ColumnsTrees.Local.ToBindingList();

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            columnsTreeBindingSource.EndEdit();
            columnsTreeBindingSource.MoveFirst();
            locContext.SaveChanges();

            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void ColumnChooser_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (db != null)
            //    db.Dispose();
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            string field = gridView1.GetRowCellDisplayText(e.RowHandle, gridView1.Columns["HeaderText"]);
            switch (field)
            {
                case "Plot #":
                case "Tree #":
                    if (e.Column.FieldName == "Show" || e.Column.FieldName == "Repeat" || e.Column.FieldName == "Required")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                        e.Appearance.BackColor2 = Color.LightGray;
                        //e.Column.ReadOnly = true;
                        e.Column.OptionsColumn.AllowEdit = false;
                    }
                    break;
                case "PF":
                case "Age":
                case "Spp":
                    if (e.Column.FieldName == "Show" || e.Column.FieldName == "Required")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                        e.Appearance.BackColor2 = Color.LightGray;
                        //e.Column.ReadOnly = true;
                        e.Column.OptionsColumn.AllowEdit = false;
                    }
                    else
                        e.Column.OptionsColumn.AllowEdit = true;
                    break;
                case "Dbh":
                case "FP":
                    if (e.Column.FieldName == "Show")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                        e.Appearance.BackColor2 = Color.LightGray;
                        //e.Column.ReadOnly = true;
                        e.Column.OptionsColumn.AllowEdit = false;
                    }
                    else
                        e.Column.OptionsColumn.AllowEdit = true;
                    break;
                case "FF":
                case "Bole Ht":
                    if (e.Column.FieldName == "Show" || e.Column.FieldName == "Repeat")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                        e.Appearance.BackColor2 = Color.LightGray;
                        //e.Column.ReadOnly = true;
                        e.Column.OptionsColumn.AllowEdit = false;
                    }
                    else
                        e.Column.OptionsColumn.AllowEdit = true;
                    break;
                case "Total Ht":
                case "PO":
                case "CR":
                case "VI":
                case "DA":
                case "T5":
                    if (e.Column.FieldName == "Repeat")
                    {
                        e.Appearance.BackColor = Color.LightGray;
                        e.Appearance.BackColor2 = Color.LightGray;
                        //e.Column.ReadOnly = true;
                        e.Column.OptionsColumn.AllowEdit = false;
                    }
                    else
                        e.Column.OptionsColumn.AllowEdit = true;
                    break;
            }
        }
    }
}