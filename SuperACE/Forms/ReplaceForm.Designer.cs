﻿namespace SuperACE
{
    partial class ReplaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbMessage = new DevExpress.XtraEditors.TextEdit();
            this.btnReplace = new DevExpress.XtraEditors.SimpleButton();
            this.btnReplaceAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnCombine = new DevExpress.XtraEditors.SimpleButton();
            this.btnCombineAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnSkip = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.tbMessage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tbMessage
            // 
            this.tbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMessage.Location = new System.Drawing.Point(12, 12);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Properties.ReadOnly = true;
            this.tbMessage.Size = new System.Drawing.Size(444, 20);
            this.tbMessage.TabIndex = 0;
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(12, 58);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(75, 23);
            this.btnReplace.TabIndex = 1;
            this.btnReplace.Text = "Replace";
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // btnReplaceAll
            // 
            this.btnReplaceAll.Location = new System.Drawing.Point(135, 58);
            this.btnReplaceAll.Name = "btnReplaceAll";
            this.btnReplaceAll.Size = new System.Drawing.Size(75, 23);
            this.btnReplaceAll.TabIndex = 2;
            this.btnReplaceAll.Text = "Replace All";
            this.btnReplaceAll.Click += new System.EventHandler(this.btnReplaceAll_Click);
            // 
            // btnCombine
            // 
            this.btnCombine.Location = new System.Drawing.Point(258, 58);
            this.btnCombine.Name = "btnCombine";
            this.btnCombine.Size = new System.Drawing.Size(75, 23);
            this.btnCombine.TabIndex = 3;
            this.btnCombine.Text = "Combine";
            this.btnCombine.Click += new System.EventHandler(this.btnCombine_Click);
            // 
            // btnCombineAll
            // 
            this.btnCombineAll.Location = new System.Drawing.Point(381, 58);
            this.btnCombineAll.Name = "btnCombineAll";
            this.btnCombineAll.Size = new System.Drawing.Size(75, 23);
            this.btnCombineAll.TabIndex = 4;
            this.btnCombineAll.Text = "Combine All";
            this.btnCombineAll.Click += new System.EventHandler(this.btnCombineAll_Click);
            // 
            // btnSkip
            // 
            this.btnSkip.Enabled = false;
            this.btnSkip.Location = new System.Drawing.Point(12, 97);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(75, 23);
            this.btnSkip.TabIndex = 5;
            this.btnSkip.Text = "Skip";
            this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(381, 97);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ReplaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 141);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.btnCombineAll);
            this.Controls.Add(this.btnCombine);
            this.Controls.Add(this.btnReplaceAll);
            this.Controls.Add(this.btnReplace);
            this.Controls.Add(this.tbMessage);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReplaceForm";
            this.Text = "Replace";
            this.Load += new System.EventHandler(this.ReplaceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbMessage.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit tbMessage;
        private DevExpress.XtraEditors.SimpleButton btnReplace;
        private DevExpress.XtraEditors.SimpleButton btnReplaceAll;
        private DevExpress.XtraEditors.SimpleButton btnCombine;
        private DevExpress.XtraEditors.SimpleButton btnCombineAll;
        private DevExpress.XtraEditors.SimpleButton btnSkip;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}