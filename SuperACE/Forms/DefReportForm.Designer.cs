﻿namespace SuperACE
{
    partial class DefReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridViewReportAsDtls = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.defReportABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewReportAs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBoxCostType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colReportAs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDefReportAsDtls = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemLookUpEditItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.costBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.defReportDomPriBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDomPri = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.defReportReforestationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewRef = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinTreesPerACre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.defReportSortParamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSort = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.defReportStandTblSizeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSizeClasses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinDbh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxDbh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.defReportLogLengthClassesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLenClasses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.defReportScalingDiaClassesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDiamClasses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItem2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReportAsDtls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReportAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxCostType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportDomPriBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDomPri)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportReforestationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportSortParamsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportStandTblSizeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSizeClasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportLogLengthClassesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLenClasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportScalingDiaClassesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiamClasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewReportAsDtls
            // 
            this.gridViewReportAsDtls.GridControl = this.gridControl7;
            this.gridViewReportAsDtls.Name = "gridViewReportAsDtls";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.defReportABindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(2, 21);
            this.gridControl7.MainView = this.gridViewReportAs;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBoxCostType,
            this.repositoryItemLookUpEditItem,
            this.repositoryItemTextEdit1});
            this.gridControl7.Size = new System.Drawing.Size(344, 236);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReportAs,
            this.gridViewReportAsDtls});
            // 
            // defReportABindingSource
            // 
            this.defReportABindingSource.DataSource = typeof(Project.DAL.DefReportA);
            // 
            // gridViewReportAs
            // 
            this.gridViewReportAs.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId6,
            this.colCostType,
            this.colReportAs,
            this.colDefReportAsDtls});
            this.gridViewReportAs.GridControl = this.gridControl7;
            this.gridViewReportAs.Name = "gridViewReportAs";
            this.gridViewReportAs.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewReportAs.OptionsCustomization.AllowFilter = false;
            this.gridViewReportAs.OptionsCustomization.AllowGroup = false;
            this.gridViewReportAs.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewReportAs.OptionsCustomization.AllowSort = false;
            this.gridViewReportAs.OptionsDetail.ShowDetailTabs = false;
            this.gridViewReportAs.OptionsView.ColumnAutoWidth = false;
            this.gridViewReportAs.OptionsView.ShowGroupPanel = false;
            this.gridViewReportAs.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gridViewReportAs_MasterRowEmpty);
            this.gridViewReportAs.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gridViewReportAs_MasterRowExpanded);
            this.gridViewReportAs.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridViewReportAs_MasterRowGetChildList);
            this.gridViewReportAs.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridViewReportAs_MasterRowGetRelationName);
            this.gridViewReportAs.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridViewReportAs_MasterRowGetRelationCount);
            this.gridViewReportAs.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewReportAs_RowUpdated);
            this.gridViewReportAs.DoubleClick += new System.EventHandler(this.gridViewReportAs_DoubleClick);
            // 
            // colId6
            // 
            this.colId6.FieldName = "Id";
            this.colId6.Name = "colId6";
            this.colId6.Width = 150;
            // 
            // colCostType
            // 
            this.colCostType.Caption = "Cost Type";
            this.colCostType.ColumnEdit = this.repositoryItemComboBoxCostType;
            this.colCostType.FieldName = "CostType";
            this.colCostType.Name = "colCostType";
            this.colCostType.Visible = true;
            this.colCostType.VisibleIndex = 0;
            // 
            // repositoryItemComboBoxCostType
            // 
            this.repositoryItemComboBoxCostType.AutoHeight = false;
            this.repositoryItemComboBoxCostType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxCostType.Items.AddRange(new object[] {
            "ANNUAL",
            "FIXED",
            "HAULING",
            "VARIABLE",
            "YARDING"});
            this.repositoryItemComboBoxCostType.Name = "repositoryItemComboBoxCostType";
            this.repositoryItemComboBoxCostType.SelectedValueChanged += new System.EventHandler(this.repositoryItemComboBoxCostType_SelectedValueChanged);
            // 
            // colReportAs
            // 
            this.colReportAs.ColumnEdit = this.repositoryItemTextEdit1;
            this.colReportAs.FieldName = "ReportAs";
            this.colReportAs.Name = "colReportAs";
            this.colReportAs.Visible = true;
            this.colReportAs.VisibleIndex = 1;
            this.colReportAs.Width = 150;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colDefReportAsDtls
            // 
            this.colDefReportAsDtls.FieldName = "DefReportAsDtls";
            this.colDefReportAsDtls.Name = "colDefReportAsDtls";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barLargeButtonItem1,
            this.barLargeButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem2)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Save";
            this.barLargeButtonItem1.Id = 0;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Caption = "Cancel";
            this.barLargeButtonItem2.Id = 1;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(924, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 568);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(924, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 544);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(924, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 544);
            // 
            // repositoryItemLookUpEditItem
            // 
            this.repositoryItemLookUpEditItem.AutoHeight = false;
            this.repositoryItemLookUpEditItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditItem.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CostType", "Cost Type", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CostDescription", "Item", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Species", "Species", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEditItem.DataSource = this.costBindingSource;
            this.repositoryItemLookUpEditItem.DisplayMember = "CostDescription";
            this.repositoryItemLookUpEditItem.Name = "repositoryItemLookUpEditItem";
            this.repositoryItemLookUpEditItem.NullText = "";
            this.repositoryItemLookUpEditItem.PopupWidth = 300;
            this.repositoryItemLookUpEditItem.ValueMember = "CostDescription";
            // 
            // costBindingSource
            // 
            this.costBindingSource.DataSource = typeof(Project.DAL.Cost);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 28);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(269, 259);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Dominant Priority";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.defReportDomPriBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 21);
            this.gridControl1.MainView = this.gridViewDomPri;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(265, 236);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDomPri});
            // 
            // defReportDomPriBindingSource
            // 
            this.defReportDomPriBindingSource.DataSource = typeof(Project.DAL.DefReportDomPri);
            // 
            // gridViewDomPri
            // 
            this.gridViewDomPri.ColumnPanelRowHeight = 40;
            this.gridViewDomPri.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colItem,
            this.colPriority,
            this.colMinimum});
            this.gridViewDomPri.GridControl = this.gridControl1;
            this.gridViewDomPri.Name = "gridViewDomPri";
            this.gridViewDomPri.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewDomPri.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewDomPri.OptionsCustomization.AllowFilter = false;
            this.gridViewDomPri.OptionsCustomization.AllowGroup = false;
            this.gridViewDomPri.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewDomPri.OptionsCustomization.AllowSort = false;
            this.gridViewDomPri.OptionsView.ColumnAutoWidth = false;
            this.gridViewDomPri.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colItem
            // 
            this.colItem.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem.FieldName = "Item";
            this.colItem.Name = "colItem";
            this.colItem.Visible = true;
            this.colItem.VisibleIndex = 0;
            this.colItem.Width = 128;
            // 
            // colPriority
            // 
            this.colPriority.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriority.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriority.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 1;
            this.colPriority.Width = 50;
            // 
            // colMinimum
            // 
            this.colMinimum.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinimum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinimum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinimum.FieldName = "Minimum";
            this.colMinimum.Name = "colMinimum";
            this.colMinimum.Visible = true;
            this.colMinimum.VisibleIndex = 2;
            this.colMinimum.Width = 56;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl2);
            this.groupControl2.Location = new System.Drawing.Point(14, 291);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(265, 259);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Reforestation";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.defReportReforestationBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 21);
            this.gridControl2.MainView = this.gridViewRef;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(261, 236);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRef});
            // 
            // defReportReforestationBindingSource
            // 
            this.defReportReforestationBindingSource.DataSource = typeof(Project.DAL.DefReportReforestation);
            // 
            // gridViewRef
            // 
            this.gridViewRef.ColumnPanelRowHeight = 40;
            this.gridViewRef.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId1,
            this.colItem1,
            this.colMinTreesPerACre,
            this.colMaxTreesPerAcre});
            this.gridViewRef.GridControl = this.gridControl2;
            this.gridViewRef.Name = "gridViewRef";
            this.gridViewRef.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewRef.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewRef.OptionsCustomization.AllowColumnResizing = false;
            this.gridViewRef.OptionsCustomization.AllowFilter = false;
            this.gridViewRef.OptionsCustomization.AllowGroup = false;
            this.gridViewRef.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewRef.OptionsCustomization.AllowSort = false;
            this.gridViewRef.OptionsView.ColumnAutoWidth = false;
            this.gridViewRef.OptionsView.ShowGroupPanel = false;
            // 
            // colId1
            // 
            this.colId1.AppearanceHeader.Options.UseTextOptions = true;
            this.colId1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colId1.FieldName = "Id";
            this.colId1.Name = "colId1";
            // 
            // colItem1
            // 
            this.colItem1.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colItem1.FieldName = "Item";
            this.colItem1.Name = "colItem1";
            this.colItem1.Visible = true;
            this.colItem1.VisibleIndex = 0;
            this.colItem1.Width = 150;
            // 
            // colMinTreesPerACre
            // 
            this.colMinTreesPerACre.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinTreesPerACre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinTreesPerACre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinTreesPerACre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinTreesPerACre.Caption = "Min T/A";
            this.colMinTreesPerACre.FieldName = "MinTreesPerACre";
            this.colMinTreesPerACre.Name = "colMinTreesPerACre";
            this.colMinTreesPerACre.Visible = true;
            this.colMinTreesPerACre.VisibleIndex = 1;
            this.colMinTreesPerACre.Width = 40;
            // 
            // colMaxTreesPerAcre
            // 
            this.colMaxTreesPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxTreesPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxTreesPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxTreesPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxTreesPerAcre.Caption = "Max T/A";
            this.colMaxTreesPerAcre.FieldName = "MaxTreesPerAcre";
            this.colMaxTreesPerAcre.Name = "colMaxTreesPerAcre";
            this.colMaxTreesPerAcre.Visible = true;
            this.colMaxTreesPerAcre.VisibleIndex = 2;
            this.colMaxTreesPerAcre.Width = 40;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridControl3);
            this.groupControl3.Location = new System.Drawing.Point(289, 28);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(269, 259);
            this.groupControl3.TabIndex = 12;
            this.groupControl3.Text = "Report Sort Parameters";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.defReportSortParamsBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(2, 21);
            this.gridControl3.MainView = this.gridViewSort;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(265, 236);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSort});
            // 
            // defReportSortParamsBindingSource
            // 
            this.defReportSortParamsBindingSource.DataSource = typeof(Project.DAL.DefReportSortParam);
            // 
            // gridViewSort
            // 
            this.gridViewSort.ColumnPanelRowHeight = 40;
            this.gridViewSort.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId5,
            this.colItem5});
            this.gridViewSort.GridControl = this.gridControl3;
            this.gridViewSort.Name = "gridViewSort";
            this.gridViewSort.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewSort.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewSort.OptionsCustomization.AllowColumnResizing = false;
            this.gridViewSort.OptionsCustomization.AllowFilter = false;
            this.gridViewSort.OptionsCustomization.AllowGroup = false;
            this.gridViewSort.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewSort.OptionsCustomization.AllowSort = false;
            this.gridViewSort.OptionsView.ShowGroupPanel = false;
            // 
            // colId5
            // 
            this.colId5.AppearanceHeader.Options.UseTextOptions = true;
            this.colId5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId5.FieldName = "Id";
            this.colId5.Name = "colId5";
            // 
            // colItem5
            // 
            this.colItem5.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem5.FieldName = "Item";
            this.colItem5.Name = "colItem5";
            this.colItem5.Visible = true;
            this.colItem5.VisibleIndex = 0;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gridControl4);
            this.groupControl4.Location = new System.Drawing.Point(289, 291);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(269, 259);
            this.groupControl4.TabIndex = 13;
            this.groupControl4.Text = "Stand Table Size Classes";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.defReportStandTblSizeBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(2, 21);
            this.gridControl4.MainView = this.gridViewSizeClasses;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(265, 236);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSizeClasses});
            // 
            // defReportStandTblSizeBindingSource
            // 
            this.defReportStandTblSizeBindingSource.DataSource = typeof(Project.DAL.DefReportStandTblSize);
            // 
            // gridViewSizeClasses
            // 
            this.gridViewSizeClasses.ColumnPanelRowHeight = 40;
            this.gridViewSizeClasses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId4,
            this.colItem4,
            this.colMinDbh,
            this.colMaxDbh});
            this.gridViewSizeClasses.GridControl = this.gridControl4;
            this.gridViewSizeClasses.Name = "gridViewSizeClasses";
            this.gridViewSizeClasses.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewSizeClasses.OptionsCustomization.AllowFilter = false;
            this.gridViewSizeClasses.OptionsCustomization.AllowGroup = false;
            this.gridViewSizeClasses.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewSizeClasses.OptionsCustomization.AllowSort = false;
            this.gridViewSizeClasses.OptionsView.ColumnAutoWidth = false;
            this.gridViewSizeClasses.OptionsView.ShowGroupPanel = false;
            // 
            // colId4
            // 
            this.colId4.AppearanceHeader.Options.UseTextOptions = true;
            this.colId4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId4.FieldName = "Id";
            this.colId4.Name = "colId4";
            // 
            // colItem4
            // 
            this.colItem4.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colItem4.FieldName = "Item";
            this.colItem4.Name = "colItem4";
            this.colItem4.Visible = true;
            this.colItem4.VisibleIndex = 0;
            this.colItem4.Width = 150;
            // 
            // colMinDbh
            // 
            this.colMinDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinDbh.FieldName = "MinDbh";
            this.colMinDbh.Name = "colMinDbh";
            this.colMinDbh.Visible = true;
            this.colMinDbh.VisibleIndex = 1;
            this.colMinDbh.Width = 40;
            // 
            // colMaxDbh
            // 
            this.colMaxDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMaxDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxDbh.FieldName = "MaxDbh";
            this.colMaxDbh.Name = "colMaxDbh";
            this.colMaxDbh.Visible = true;
            this.colMaxDbh.VisibleIndex = 2;
            this.colMaxDbh.Width = 45;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.gridControl5);
            this.groupControl5.Location = new System.Drawing.Point(564, 28);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(171, 259);
            this.groupControl5.TabIndex = 14;
            this.groupControl5.Text = "Log Length Classes";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.defReportLogLengthClassesBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.Location = new System.Drawing.Point(2, 21);
            this.gridControl5.MainView = this.gridViewLenClasses;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(167, 236);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLenClasses});
            // 
            // defReportLogLengthClassesBindingSource
            // 
            this.defReportLogLengthClassesBindingSource.DataSource = typeof(Project.DAL.DefReportLogLengthClass);
            // 
            // gridViewLenClasses
            // 
            this.gridViewLenClasses.ColumnPanelRowHeight = 40;
            this.gridViewLenClasses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId3,
            this.colItem3});
            this.gridViewLenClasses.GridControl = this.gridControl5;
            this.gridViewLenClasses.Name = "gridViewLenClasses";
            this.gridViewLenClasses.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewLenClasses.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewLenClasses.OptionsCustomization.AllowColumnResizing = false;
            this.gridViewLenClasses.OptionsCustomization.AllowFilter = false;
            this.gridViewLenClasses.OptionsCustomization.AllowGroup = false;
            this.gridViewLenClasses.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewLenClasses.OptionsCustomization.AllowSort = false;
            this.gridViewLenClasses.OptionsView.ShowGroupPanel = false;
            // 
            // colId3
            // 
            this.colId3.AppearanceHeader.Options.UseTextOptions = true;
            this.colId3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId3.FieldName = "Id";
            this.colId3.Name = "colId3";
            // 
            // colItem3
            // 
            this.colItem3.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem3.FieldName = "Item";
            this.colItem3.Name = "colItem3";
            this.colItem3.Visible = true;
            this.colItem3.VisibleIndex = 0;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.gridControl6);
            this.groupControl6.Location = new System.Drawing.Point(741, 30);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(171, 257);
            this.groupControl6.TabIndex = 15;
            this.groupControl6.Text = "Log Diameter Classes";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.defReportScalingDiaClassesBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.Location = new System.Drawing.Point(2, 21);
            this.gridControl6.MainView = this.gridViewDiamClasses;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.Size = new System.Drawing.Size(167, 234);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDiamClasses});
            // 
            // defReportScalingDiaClassesBindingSource
            // 
            this.defReportScalingDiaClassesBindingSource.DataSource = typeof(Project.DAL.DefReportScalingDiaClass);
            // 
            // gridViewDiamClasses
            // 
            this.gridViewDiamClasses.ColumnPanelRowHeight = 40;
            this.gridViewDiamClasses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId2,
            this.colItem2});
            this.gridViewDiamClasses.GridControl = this.gridControl6;
            this.gridViewDiamClasses.Name = "gridViewDiamClasses";
            this.gridViewDiamClasses.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewDiamClasses.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewDiamClasses.OptionsCustomization.AllowColumnResizing = false;
            this.gridViewDiamClasses.OptionsCustomization.AllowFilter = false;
            this.gridViewDiamClasses.OptionsCustomization.AllowGroup = false;
            this.gridViewDiamClasses.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewDiamClasses.OptionsCustomization.AllowSort = false;
            this.gridViewDiamClasses.OptionsView.ShowGroupPanel = false;
            // 
            // colId2
            // 
            this.colId2.AppearanceHeader.Options.UseTextOptions = true;
            this.colId2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId2.FieldName = "Id";
            this.colId2.Name = "colId2";
            // 
            // colItem2
            // 
            this.colItem2.AppearanceHeader.Options.UseTextOptions = true;
            this.colItem2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItem2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colItem2.FieldName = "Item";
            this.colItem2.Name = "colItem2";
            this.colItem2.Visible = true;
            this.colItem2.VisibleIndex = 0;
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.gridControl7);
            this.groupControl7.Location = new System.Drawing.Point(564, 289);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(348, 259);
            this.groupControl7.TabIndex = 20;
            this.groupControl7.Text = "Report As";
            // 
            // DefReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 568);
            this.Controls.Add(this.groupControl7);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefReportForm";
            this.Text = "Default Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefReportForm_FormClosing);
            this.Load += new System.EventHandler(this.DefReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReportAsDtls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReportAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxCostType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportDomPriBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDomPri)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportReforestationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportSortParamsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportStandTblSizeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSizeClasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportLogLengthClassesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLenClasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defReportScalingDiaClassesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiamClasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource defReportScalingDiaClassesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDiamClasses;
        private DevExpress.XtraGrid.Columns.GridColumn colId2;
        private DevExpress.XtraGrid.Columns.GridColumn colItem2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private System.Windows.Forms.BindingSource defReportLogLengthClassesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLenClasses;
        private DevExpress.XtraGrid.Columns.GridColumn colId3;
        private DevExpress.XtraGrid.Columns.GridColumn colItem3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource defReportStandTblSizeBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSizeClasses;
        private DevExpress.XtraGrid.Columns.GridColumn colId4;
        private DevExpress.XtraGrid.Columns.GridColumn colItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colMinDbh;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxDbh;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource defReportSortParamsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSort;
        private DevExpress.XtraGrid.Columns.GridColumn colId5;
        private DevExpress.XtraGrid.Columns.GridColumn colItem5;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource defReportReforestationBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRef;
        private DevExpress.XtraGrid.Columns.GridColumn colId1;
        private DevExpress.XtraGrid.Columns.GridColumn colItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colMinTreesPerACre;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxTreesPerAcre;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource defReportDomPriBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDomPri;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colItem;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimum;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private System.Windows.Forms.BindingSource defReportABindingSource;
        private System.Windows.Forms.BindingSource costBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReportAs;
        private DevExpress.XtraGrid.Columns.GridColumn colId6;
        private DevExpress.XtraGrid.Columns.GridColumn colReportAs;
        private DevExpress.XtraGrid.Columns.GridColumn colDefReportAsDtls;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxCostType;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReportAsDtls;
        private DevExpress.XtraGrid.Columns.GridColumn colCostType;
    }
}