﻿namespace SuperACE
{
    partial class ProjectEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.tbProjectLocation = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.cbProjectName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ProjectNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.projectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GISProjectNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ProjectNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ProjectLeadTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ProjectNotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SaleNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ProjectDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CustomerCompanyNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerContactNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerAddress1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerAddress2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerCityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerStateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerPostalCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerOfficePhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerCellPhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerFaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomerNotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AspectsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CrownPositionTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CruisersTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.DamageTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.EnvironmentsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.HarvestSystemsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LandFormsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ProjectAdjustmentsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.RoadsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SeedZonesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SlashDistributionsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SoilsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SpeciesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.StandActivitiesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.StandDataSourcesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.StreamsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TreatmentsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TreeSourcesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TreeStatusTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.UserDefinedTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.VegetationsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.VigorTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ComponentsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.HabitatTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.PriceTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CostTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SortsTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.GradesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.NonStockedTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.NonTimberedTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.PilingTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.PolesTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TrimTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.YieldTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.FormTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.BarkTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ASuboTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CuFtRuleComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.BdFtRuleComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.WoodTypeTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.GrowthModelTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.DestinationTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.HistoryPlanningTableNameComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCrownPositionTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDamageTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHarvestSystemsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProjectAdjustmentsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSeedZonesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSoilsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStandDataSourcesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreatmentsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeStatusTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVegetationsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForComponentsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStockedTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPilingTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTrimTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFormTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForASuboTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBdFtRule = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrowthModelTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpeciesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGradesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSortsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPriceTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAspectsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCruisersTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEnvironmentsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLandFormsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoadsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSlashDistributionsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStandActivitiesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStreamsTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeSourcesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserDefinedTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVigorTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHabitatTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonTimberedTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodTypeTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCuFtRule = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBarkTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForYieldTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPolesTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDestinationTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHistoryPlanningTableName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForProjectName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGISProjectName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProjectNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProjectNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaleName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProjectDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProjectLead = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCustomerCompanyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerContactName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerAddress1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerAddress2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerState = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerPostalCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerOfficePhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerCellPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbProjectLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GISProjectNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectLeadTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaleNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCompanyNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerContactNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerAddress1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerAddress2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerStateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerPostalCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerOfficePhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCellPhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerFaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerNotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownPositionTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruisersTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DamageTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HarvestSystemsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandFormsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectAdjustmentsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedZonesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SlashDistributionsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoilsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StandActivitiesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StandDataSourcesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreatmentsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeSourcesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeStatusTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserDefinedTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VegetationsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VigorTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComponentsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HabitatTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortsTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GradesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStockedTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonTimberedTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PilingTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolesTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrimTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YieldTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarkTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ASuboTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuFtRuleComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BdFtRuleComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodTypeTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthModelTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryPlanningTableNameComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownPositionTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDamageTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHarvestSystemsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectAdjustmentsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeedZonesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoilsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStandDataSourcesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreatmentsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeStatusTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVegetationsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComponentsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStockedTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPilingTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrimTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFormTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForASuboTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBdFtRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrowthModelTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGradesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSortsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPriceTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAspectsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruisersTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmentsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandFormsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoadsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSlashDistributionsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStandActivitiesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStreamsTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeSourcesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserDefinedTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVigorTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHabitatTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonTimberedTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodTypeTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCuFtRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBarkTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYieldTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolesTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDestinationTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHistoryPlanningTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGISProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectLead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerOfficePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCellPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(719, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(719, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 629);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(719, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 629);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.cbProjectName);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(719, 59);
            this.panelControl1.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.tbProjectLocation);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(715, 55);
            this.panelControl2.TabIndex = 5;
            // 
            // tbProjectLocation
            // 
            this.tbProjectLocation.Location = new System.Drawing.Point(116, 20);
            this.tbProjectLocation.MenuManager = this.barManager1;
            this.tbProjectLocation.Name = "tbProjectLocation";
            this.tbProjectLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tbProjectLocation.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_Properties_ButtonClick);
            this.tbProjectLocation.Size = new System.Drawing.Size(539, 20);
            this.tbProjectLocation.TabIndex = 3;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(19, 23);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(81, 13);
            this.labelControl20.TabIndex = 2;
            this.labelControl20.Text = "Project Location:";
            // 
            // cbProjectName
            // 
            this.cbProjectName.Location = new System.Drawing.Point(531, 20);
            this.cbProjectName.MenuManager = this.barManager1;
            this.cbProjectName.Name = "cbProjectName";
            this.cbProjectName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbProjectName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbProjectName.Size = new System.Drawing.Size(166, 20);
            this.cbProjectName.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(379, 18);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(45, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = ". . .";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(19, 23);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(81, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Project Location:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(457, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Project Name:";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ProjectNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.GISProjectNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ProjectNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ProjectLeadTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ProjectNotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SaleNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ProjectDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerCompanyNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerContactNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerAddress1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerAddress2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerCityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerStateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerPostalCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerOfficePhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerCellPhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerFaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerNotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AspectsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.CrownPositionTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.CruisersTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.DamageTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.EnvironmentsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.HarvestSystemsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.LandFormsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.ProjectAdjustmentsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.RoadsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SeedZonesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SlashDistributionsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SoilsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SpeciesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.StandActivitiesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.StandDataSourcesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.StreamsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.TreatmentsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeSourcesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeStatusTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.UserDefinedTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.VegetationsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.VigorTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.ComponentsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.HabitatTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.PriceTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SortsTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.GradesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.NonStockedTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.NonTimberedTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.PilingTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.PolesTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.TrimTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.YieldTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.FormTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.BarkTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.ASuboTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.CuFtRuleComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.BdFtRuleComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodTypeTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.GrowthModelTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.DestinationTableNameComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.HistoryPlanningTableNameComboBoxEdit);
            this.dataLayoutControl1.DataSource = this.projectsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 83);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(960, 267, 450, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(719, 570);
            this.dataLayoutControl1.TabIndex = 43;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ProjectNameComboBoxEdit
            // 
            this.ProjectNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectName", true));
            this.ProjectNameComboBoxEdit.EnterMoveNextControl = true;
            this.ProjectNameComboBoxEdit.Location = new System.Drawing.Point(183, 34);
            this.ProjectNameComboBoxEdit.MenuManager = this.barManager1;
            this.ProjectNameComboBoxEdit.Name = "ProjectNameComboBoxEdit";
            this.ProjectNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProjectNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ProjectNameComboBoxEdit.Size = new System.Drawing.Size(495, 20);
            this.ProjectNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ProjectNameComboBoxEdit.TabIndex = 0;
            this.ProjectNameComboBoxEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ProjectNameComboBoxEdit_Validating);
            // 
            // projectsBindingSource
            // 
            this.projectsBindingSource.DataSource = typeof(Project.DAL.Project);
            // 
            // GISProjectNameTextEdit
            // 
            this.GISProjectNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "GISProjectName", true));
            this.GISProjectNameTextEdit.EnterMoveNextControl = true;
            this.GISProjectNameTextEdit.Location = new System.Drawing.Point(183, 82);
            this.GISProjectNameTextEdit.MenuManager = this.barManager1;
            this.GISProjectNameTextEdit.Name = "GISProjectNameTextEdit";
            this.GISProjectNameTextEdit.Size = new System.Drawing.Size(495, 20);
            this.GISProjectNameTextEdit.StyleController = this.dataLayoutControl1;
            this.GISProjectNameTextEdit.TabIndex = 2;
            this.GISProjectNameTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // ProjectNumberTextEdit
            // 
            this.ProjectNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectNumber", true));
            this.ProjectNumberTextEdit.EnterMoveNextControl = true;
            this.ProjectNumberTextEdit.Location = new System.Drawing.Point(183, 58);
            this.ProjectNumberTextEdit.MenuManager = this.barManager1;
            this.ProjectNumberTextEdit.Name = "ProjectNumberTextEdit";
            this.ProjectNumberTextEdit.Size = new System.Drawing.Size(495, 20);
            this.ProjectNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ProjectNumberTextEdit.TabIndex = 1;
            this.ProjectNumberTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // ProjectLeadTextEdit
            // 
            this.ProjectLeadTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectLead", true));
            this.ProjectLeadTextEdit.EnterMoveNextControl = true;
            this.ProjectLeadTextEdit.Location = new System.Drawing.Point(183, 106);
            this.ProjectLeadTextEdit.MenuManager = this.barManager1;
            this.ProjectLeadTextEdit.Name = "ProjectLeadTextEdit";
            this.ProjectLeadTextEdit.Size = new System.Drawing.Size(495, 20);
            this.ProjectLeadTextEdit.StyleController = this.dataLayoutControl1;
            this.ProjectLeadTextEdit.TabIndex = 3;
            this.ProjectLeadTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // ProjectNotesMemoEdit
            // 
            this.ProjectNotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectNotes", true));
            this.ProjectNotesMemoEdit.EnterMoveNextControl = true;
            this.ProjectNotesMemoEdit.Location = new System.Drawing.Point(183, 178);
            this.ProjectNotesMemoEdit.MenuManager = this.barManager1;
            this.ProjectNotesMemoEdit.Name = "ProjectNotesMemoEdit";
            this.ProjectNotesMemoEdit.Size = new System.Drawing.Size(495, 380);
            this.ProjectNotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.ProjectNotesMemoEdit.TabIndex = 6;
            this.ProjectNotesMemoEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // SaleNameTextEdit
            // 
            this.SaleNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SaleName", true));
            this.SaleNameTextEdit.EnterMoveNextControl = true;
            this.SaleNameTextEdit.Location = new System.Drawing.Point(183, 130);
            this.SaleNameTextEdit.MenuManager = this.barManager1;
            this.SaleNameTextEdit.Name = "SaleNameTextEdit";
            this.SaleNameTextEdit.Size = new System.Drawing.Size(495, 20);
            this.SaleNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SaleNameTextEdit.TabIndex = 4;
            this.SaleNameTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // ProjectDateDateEdit
            // 
            this.ProjectDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectDate", true));
            this.ProjectDateDateEdit.EditValue = null;
            this.ProjectDateDateEdit.EnterMoveNextControl = true;
            this.ProjectDateDateEdit.Location = new System.Drawing.Point(183, 154);
            this.ProjectDateDateEdit.MenuManager = this.barManager1;
            this.ProjectDateDateEdit.Name = "ProjectDateDateEdit";
            this.ProjectDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ProjectDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProjectDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProjectDateDateEdit.Size = new System.Drawing.Size(495, 20);
            this.ProjectDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ProjectDateDateEdit.TabIndex = 5;
            // 
            // CustomerCompanyNameTextEdit
            // 
            this.CustomerCompanyNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerCompanyName", true));
            this.CustomerCompanyNameTextEdit.EnterMoveNextControl = true;
            this.CustomerCompanyNameTextEdit.Location = new System.Drawing.Point(183, 34);
            this.CustomerCompanyNameTextEdit.MenuManager = this.barManager1;
            this.CustomerCompanyNameTextEdit.Name = "CustomerCompanyNameTextEdit";
            this.CustomerCompanyNameTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerCompanyNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerCompanyNameTextEdit.TabIndex = 11;
            this.CustomerCompanyNameTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerContactNameTextEdit
            // 
            this.CustomerContactNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerContactName", true));
            this.CustomerContactNameTextEdit.EnterMoveNextControl = true;
            this.CustomerContactNameTextEdit.Location = new System.Drawing.Point(183, 58);
            this.CustomerContactNameTextEdit.MenuManager = this.barManager1;
            this.CustomerContactNameTextEdit.Name = "CustomerContactNameTextEdit";
            this.CustomerContactNameTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerContactNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerContactNameTextEdit.TabIndex = 12;
            this.CustomerContactNameTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerAddress1TextEdit
            // 
            this.CustomerAddress1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerAddress1", true));
            this.CustomerAddress1TextEdit.EnterMoveNextControl = true;
            this.CustomerAddress1TextEdit.Location = new System.Drawing.Point(183, 82);
            this.CustomerAddress1TextEdit.MenuManager = this.barManager1;
            this.CustomerAddress1TextEdit.Name = "CustomerAddress1TextEdit";
            this.CustomerAddress1TextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerAddress1TextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerAddress1TextEdit.TabIndex = 13;
            this.CustomerAddress1TextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerAddress2TextEdit
            // 
            this.CustomerAddress2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerAddress2", true));
            this.CustomerAddress2TextEdit.EnterMoveNextControl = true;
            this.CustomerAddress2TextEdit.Location = new System.Drawing.Point(183, 106);
            this.CustomerAddress2TextEdit.MenuManager = this.barManager1;
            this.CustomerAddress2TextEdit.Name = "CustomerAddress2TextEdit";
            this.CustomerAddress2TextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerAddress2TextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerAddress2TextEdit.TabIndex = 14;
            this.CustomerAddress2TextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerCityTextEdit
            // 
            this.CustomerCityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerCity", true));
            this.CustomerCityTextEdit.EnterMoveNextControl = true;
            this.CustomerCityTextEdit.Location = new System.Drawing.Point(183, 130);
            this.CustomerCityTextEdit.MenuManager = this.barManager1;
            this.CustomerCityTextEdit.Name = "CustomerCityTextEdit";
            this.CustomerCityTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerCityTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerCityTextEdit.TabIndex = 15;
            this.CustomerCityTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerStateTextEdit
            // 
            this.CustomerStateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerState", true));
            this.CustomerStateTextEdit.EnterMoveNextControl = true;
            this.CustomerStateTextEdit.Location = new System.Drawing.Point(183, 154);
            this.CustomerStateTextEdit.MenuManager = this.barManager1;
            this.CustomerStateTextEdit.Name = "CustomerStateTextEdit";
            this.CustomerStateTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerStateTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerStateTextEdit.TabIndex = 16;
            this.CustomerStateTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerPostalCodeTextEdit
            // 
            this.CustomerPostalCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerPostalCode", true));
            this.CustomerPostalCodeTextEdit.EnterMoveNextControl = true;
            this.CustomerPostalCodeTextEdit.Location = new System.Drawing.Point(183, 178);
            this.CustomerPostalCodeTextEdit.MenuManager = this.barManager1;
            this.CustomerPostalCodeTextEdit.Name = "CustomerPostalCodeTextEdit";
            this.CustomerPostalCodeTextEdit.Properties.Mask.EditMask = "00000";
            this.CustomerPostalCodeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.CustomerPostalCodeTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerPostalCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerPostalCodeTextEdit.TabIndex = 17;
            this.CustomerPostalCodeTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerOfficePhoneTextEdit
            // 
            this.CustomerOfficePhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerOfficePhone", true));
            this.CustomerOfficePhoneTextEdit.EnterMoveNextControl = true;
            this.CustomerOfficePhoneTextEdit.Location = new System.Drawing.Point(183, 202);
            this.CustomerOfficePhoneTextEdit.MenuManager = this.barManager1;
            this.CustomerOfficePhoneTextEdit.Name = "CustomerOfficePhoneTextEdit";
            this.CustomerOfficePhoneTextEdit.Properties.Mask.EditMask = "(999) 000-0000";
            this.CustomerOfficePhoneTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.CustomerOfficePhoneTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerOfficePhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerOfficePhoneTextEdit.TabIndex = 18;
            this.CustomerOfficePhoneTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerCellPhoneTextEdit
            // 
            this.CustomerCellPhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerCellPhone", true));
            this.CustomerCellPhoneTextEdit.EnterMoveNextControl = true;
            this.CustomerCellPhoneTextEdit.Location = new System.Drawing.Point(183, 226);
            this.CustomerCellPhoneTextEdit.MenuManager = this.barManager1;
            this.CustomerCellPhoneTextEdit.Name = "CustomerCellPhoneTextEdit";
            this.CustomerCellPhoneTextEdit.Properties.Mask.EditMask = "(999) 000-0000";
            this.CustomerCellPhoneTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.CustomerCellPhoneTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerCellPhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerCellPhoneTextEdit.TabIndex = 19;
            this.CustomerCellPhoneTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerFaxTextEdit
            // 
            this.CustomerFaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerFax", true));
            this.CustomerFaxTextEdit.EnterMoveNextControl = true;
            this.CustomerFaxTextEdit.Location = new System.Drawing.Point(183, 250);
            this.CustomerFaxTextEdit.MenuManager = this.barManager1;
            this.CustomerFaxTextEdit.Name = "CustomerFaxTextEdit";
            this.CustomerFaxTextEdit.Properties.Mask.EditMask = "(999) 000-0000";
            this.CustomerFaxTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.CustomerFaxTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerFaxTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerFaxTextEdit.TabIndex = 20;
            this.CustomerFaxTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerEmailTextEdit
            // 
            this.CustomerEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerEmail", true));
            this.CustomerEmailTextEdit.EnterMoveNextControl = true;
            this.CustomerEmailTextEdit.Location = new System.Drawing.Point(183, 274);
            this.CustomerEmailTextEdit.MenuManager = this.barManager1;
            this.CustomerEmailTextEdit.Name = "CustomerEmailTextEdit";
            this.CustomerEmailTextEdit.Size = new System.Drawing.Size(495, 20);
            this.CustomerEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomerEmailTextEdit.TabIndex = 21;
            this.CustomerEmailTextEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // CustomerNotesMemoEdit
            // 
            this.CustomerNotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CustomerNotes", true));
            this.CustomerNotesMemoEdit.EnterMoveNextControl = true;
            this.CustomerNotesMemoEdit.Location = new System.Drawing.Point(183, 298);
            this.CustomerNotesMemoEdit.MenuManager = this.barManager1;
            this.CustomerNotesMemoEdit.Name = "CustomerNotesMemoEdit";
            this.CustomerNotesMemoEdit.Size = new System.Drawing.Size(495, 260);
            this.CustomerNotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.CustomerNotesMemoEdit.TabIndex = 22;
            this.CustomerNotesMemoEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // AspectsTableNameComboBoxEdit
            // 
            this.AspectsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "AspectsTableName", true));
            this.AspectsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 82);
            this.AspectsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.AspectsTableNameComboBoxEdit.Name = "AspectsTableNameComboBoxEdit";
            this.AspectsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AspectsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.AspectsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.AspectsTableNameComboBoxEdit.TabIndex = 65;
            // 
            // CrownPositionTableNameComboBoxEdit
            // 
            this.CrownPositionTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CrownPositionTableName", true));
            this.CrownPositionTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 106);
            this.CrownPositionTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.CrownPositionTableNameComboBoxEdit.Name = "CrownPositionTableNameComboBoxEdit";
            this.CrownPositionTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CrownPositionTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.CrownPositionTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.CrownPositionTableNameComboBoxEdit.TabIndex = 66;
            // 
            // CruisersTableNameComboBoxEdit
            // 
            this.CruisersTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CruisersTableName", true));
            this.CruisersTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 106);
            this.CruisersTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.CruisersTableNameComboBoxEdit.Name = "CruisersTableNameComboBoxEdit";
            this.CruisersTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CruisersTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.CruisersTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.CruisersTableNameComboBoxEdit.TabIndex = 67;
            // 
            // DamageTableNameComboBoxEdit
            // 
            this.DamageTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "DamageTableName", true));
            this.DamageTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 130);
            this.DamageTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.DamageTableNameComboBoxEdit.Name = "DamageTableNameComboBoxEdit";
            this.DamageTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DamageTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.DamageTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.DamageTableNameComboBoxEdit.TabIndex = 68;
            // 
            // EnvironmentsTableNameComboBoxEdit
            // 
            this.EnvironmentsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "EnvironmentsTableName", true));
            this.EnvironmentsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 130);
            this.EnvironmentsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.EnvironmentsTableNameComboBoxEdit.Name = "EnvironmentsTableNameComboBoxEdit";
            this.EnvironmentsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EnvironmentsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.EnvironmentsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.EnvironmentsTableNameComboBoxEdit.TabIndex = 69;
            // 
            // HarvestSystemsTableNameComboBoxEdit
            // 
            this.HarvestSystemsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "HarvestSystemsTableName", true));
            this.HarvestSystemsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 154);
            this.HarvestSystemsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.HarvestSystemsTableNameComboBoxEdit.Name = "HarvestSystemsTableNameComboBoxEdit";
            this.HarvestSystemsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HarvestSystemsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.HarvestSystemsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.HarvestSystemsTableNameComboBoxEdit.TabIndex = 70;
            // 
            // LandFormsTableNameComboBoxEdit
            // 
            this.LandFormsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "LandFormsTableName", true));
            this.LandFormsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 154);
            this.LandFormsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.LandFormsTableNameComboBoxEdit.Name = "LandFormsTableNameComboBoxEdit";
            this.LandFormsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LandFormsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.LandFormsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.LandFormsTableNameComboBoxEdit.TabIndex = 71;
            // 
            // ProjectAdjustmentsTableNameComboBoxEdit
            // 
            this.ProjectAdjustmentsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ProjectAdjustmentsTableName", true));
            this.ProjectAdjustmentsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 178);
            this.ProjectAdjustmentsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.ProjectAdjustmentsTableNameComboBoxEdit.Name = "ProjectAdjustmentsTableNameComboBoxEdit";
            this.ProjectAdjustmentsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProjectAdjustmentsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.ProjectAdjustmentsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ProjectAdjustmentsTableNameComboBoxEdit.TabIndex = 72;
            // 
            // RoadsTableNameComboBoxEdit
            // 
            this.RoadsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "RoadsTableName", true));
            this.RoadsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 178);
            this.RoadsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.RoadsTableNameComboBoxEdit.Name = "RoadsTableNameComboBoxEdit";
            this.RoadsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoadsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.RoadsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.RoadsTableNameComboBoxEdit.TabIndex = 73;
            // 
            // SeedZonesTableNameComboBoxEdit
            // 
            this.SeedZonesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SeedZonesTableName", true));
            this.SeedZonesTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 202);
            this.SeedZonesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.SeedZonesTableNameComboBoxEdit.Name = "SeedZonesTableNameComboBoxEdit";
            this.SeedZonesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SeedZonesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.SeedZonesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.SeedZonesTableNameComboBoxEdit.TabIndex = 74;
            // 
            // SlashDistributionsTableNameComboBoxEdit
            // 
            this.SlashDistributionsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SlashDistributionsTableName", true));
            this.SlashDistributionsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 202);
            this.SlashDistributionsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.SlashDistributionsTableNameComboBoxEdit.Name = "SlashDistributionsTableNameComboBoxEdit";
            this.SlashDistributionsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SlashDistributionsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.SlashDistributionsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.SlashDistributionsTableNameComboBoxEdit.TabIndex = 75;
            // 
            // SoilsTableNameComboBoxEdit
            // 
            this.SoilsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SoilsTableName", true));
            this.SoilsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 226);
            this.SoilsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.SoilsTableNameComboBoxEdit.Name = "SoilsTableNameComboBoxEdit";
            this.SoilsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SoilsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.SoilsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.SoilsTableNameComboBoxEdit.TabIndex = 76;
            // 
            // SpeciesTableNameComboBoxEdit
            // 
            this.SpeciesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SpeciesTableName", true));
            this.SpeciesTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 34);
            this.SpeciesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.SpeciesTableNameComboBoxEdit.Name = "SpeciesTableNameComboBoxEdit";
            this.SpeciesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpeciesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.SpeciesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.SpeciesTableNameComboBoxEdit.TabIndex = 77;
            // 
            // StandActivitiesTableNameComboBoxEdit
            // 
            this.StandActivitiesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "StandActivitiesTableName", true));
            this.StandActivitiesTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 226);
            this.StandActivitiesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.StandActivitiesTableNameComboBoxEdit.Name = "StandActivitiesTableNameComboBoxEdit";
            this.StandActivitiesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StandActivitiesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.StandActivitiesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.StandActivitiesTableNameComboBoxEdit.TabIndex = 78;
            // 
            // StandDataSourcesTableNameComboBoxEdit
            // 
            this.StandDataSourcesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "StandDataSourcesTableName", true));
            this.StandDataSourcesTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 250);
            this.StandDataSourcesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.StandDataSourcesTableNameComboBoxEdit.Name = "StandDataSourcesTableNameComboBoxEdit";
            this.StandDataSourcesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StandDataSourcesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.StandDataSourcesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.StandDataSourcesTableNameComboBoxEdit.TabIndex = 79;
            // 
            // StreamsTableNameComboBoxEdit
            // 
            this.StreamsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "StreamsTableName", true));
            this.StreamsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 250);
            this.StreamsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.StreamsTableNameComboBoxEdit.Name = "StreamsTableNameComboBoxEdit";
            this.StreamsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StreamsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.StreamsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.StreamsTableNameComboBoxEdit.TabIndex = 80;
            // 
            // TreatmentsTableNameComboBoxEdit
            // 
            this.TreatmentsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "TreatmentsTableName", true));
            this.TreatmentsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 274);
            this.TreatmentsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.TreatmentsTableNameComboBoxEdit.Name = "TreatmentsTableNameComboBoxEdit";
            this.TreatmentsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TreatmentsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.TreatmentsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.TreatmentsTableNameComboBoxEdit.TabIndex = 81;
            // 
            // TreeSourcesTableNameComboBoxEdit
            // 
            this.TreeSourcesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "TreeSourcesTableName", true));
            this.TreeSourcesTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 274);
            this.TreeSourcesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.TreeSourcesTableNameComboBoxEdit.Name = "TreeSourcesTableNameComboBoxEdit";
            this.TreeSourcesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TreeSourcesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.TreeSourcesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.TreeSourcesTableNameComboBoxEdit.TabIndex = 82;
            // 
            // TreeStatusTableNameComboBoxEdit
            // 
            this.TreeStatusTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "TreeStatusTableName", true));
            this.TreeStatusTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 298);
            this.TreeStatusTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.TreeStatusTableNameComboBoxEdit.Name = "TreeStatusTableNameComboBoxEdit";
            this.TreeStatusTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TreeStatusTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.TreeStatusTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.TreeStatusTableNameComboBoxEdit.TabIndex = 83;
            // 
            // UserDefinedTableNameComboBoxEdit
            // 
            this.UserDefinedTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "UserDefinedTableName", true));
            this.UserDefinedTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 298);
            this.UserDefinedTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.UserDefinedTableNameComboBoxEdit.Name = "UserDefinedTableNameComboBoxEdit";
            this.UserDefinedTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UserDefinedTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.UserDefinedTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.UserDefinedTableNameComboBoxEdit.TabIndex = 84;
            // 
            // VegetationsTableNameComboBoxEdit
            // 
            this.VegetationsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "VegetationsTableName", true));
            this.VegetationsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 322);
            this.VegetationsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.VegetationsTableNameComboBoxEdit.Name = "VegetationsTableNameComboBoxEdit";
            this.VegetationsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VegetationsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.VegetationsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.VegetationsTableNameComboBoxEdit.TabIndex = 85;
            // 
            // VigorTableNameComboBoxEdit
            // 
            this.VigorTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "VigorTableName", true));
            this.VigorTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 322);
            this.VigorTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.VigorTableNameComboBoxEdit.Name = "VigorTableNameComboBoxEdit";
            this.VigorTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VigorTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.VigorTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.VigorTableNameComboBoxEdit.TabIndex = 86;
            // 
            // ComponentsTableNameComboBoxEdit
            // 
            this.ComponentsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ComponentsTableName", true));
            this.ComponentsTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 346);
            this.ComponentsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.ComponentsTableNameComboBoxEdit.Name = "ComponentsTableNameComboBoxEdit";
            this.ComponentsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComponentsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.ComponentsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ComponentsTableNameComboBoxEdit.TabIndex = 87;
            // 
            // HabitatTableNameComboBoxEdit
            // 
            this.HabitatTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "HabitatTableName", true));
            this.HabitatTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 346);
            this.HabitatTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.HabitatTableNameComboBoxEdit.Name = "HabitatTableNameComboBoxEdit";
            this.HabitatTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HabitatTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.HabitatTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.HabitatTableNameComboBoxEdit.TabIndex = 88;
            // 
            // PriceTableNameComboBoxEdit
            // 
            this.PriceTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "PriceTableName", true));
            this.PriceTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 58);
            this.PriceTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.PriceTableNameComboBoxEdit.Name = "PriceTableNameComboBoxEdit";
            this.PriceTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PriceTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.PriceTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PriceTableNameComboBoxEdit.TabIndex = 89;
            // 
            // CostTableNameComboBoxEdit
            // 
            this.CostTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CostTableName", true));
            this.CostTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 82);
            this.CostTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.CostTableNameComboBoxEdit.Name = "CostTableNameComboBoxEdit";
            this.CostTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.CostTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.CostTableNameComboBoxEdit.TabIndex = 90;
            // 
            // SortsTableNameComboBoxEdit
            // 
            this.SortsTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "SortsTableName", true));
            this.SortsTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 34);
            this.SortsTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.SortsTableNameComboBoxEdit.Name = "SortsTableNameComboBoxEdit";
            this.SortsTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SortsTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.SortsTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.SortsTableNameComboBoxEdit.TabIndex = 91;
            // 
            // GradesTableNameComboBoxEdit
            // 
            this.GradesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "GradesTableName", true));
            this.GradesTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 58);
            this.GradesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.GradesTableNameComboBoxEdit.Name = "GradesTableNameComboBoxEdit";
            this.GradesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GradesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.GradesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.GradesTableNameComboBoxEdit.TabIndex = 92;
            // 
            // NonStockedTableNameComboBoxEdit
            // 
            this.NonStockedTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "NonStockedTableName", true));
            this.NonStockedTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 370);
            this.NonStockedTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.NonStockedTableNameComboBoxEdit.Name = "NonStockedTableNameComboBoxEdit";
            this.NonStockedTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NonStockedTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.NonStockedTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.NonStockedTableNameComboBoxEdit.TabIndex = 93;
            // 
            // NonTimberedTableNameComboBoxEdit
            // 
            this.NonTimberedTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "NonTimberedTableName", true));
            this.NonTimberedTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 370);
            this.NonTimberedTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.NonTimberedTableNameComboBoxEdit.Name = "NonTimberedTableNameComboBoxEdit";
            this.NonTimberedTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NonTimberedTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.NonTimberedTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.NonTimberedTableNameComboBoxEdit.TabIndex = 94;
            // 
            // PilingTableNameComboBoxEdit
            // 
            this.PilingTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "PilingTableName", true));
            this.PilingTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 514);
            this.PilingTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.PilingTableNameComboBoxEdit.Name = "PilingTableNameComboBoxEdit";
            this.PilingTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PilingTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.PilingTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PilingTableNameComboBoxEdit.TabIndex = 105;
            // 
            // PolesTableNameComboBoxEdit
            // 
            this.PolesTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "PolesTableName", true));
            this.PolesTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 490);
            this.PolesTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.PolesTableNameComboBoxEdit.Name = "PolesTableNameComboBoxEdit";
            this.PolesTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PolesTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.PolesTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PolesTableNameComboBoxEdit.TabIndex = 104;
            // 
            // TrimTableNameComboBoxEdit
            // 
            this.TrimTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "TrimTableName", true));
            this.TrimTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 490);
            this.TrimTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.TrimTableNameComboBoxEdit.Name = "TrimTableNameComboBoxEdit";
            this.TrimTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TrimTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.TrimTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.TrimTableNameComboBoxEdit.TabIndex = 103;
            // 
            // YieldTableNameComboBoxEdit
            // 
            this.YieldTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "YieldTableName", true));
            this.YieldTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 466);
            this.YieldTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.YieldTableNameComboBoxEdit.Name = "YieldTableNameComboBoxEdit";
            this.YieldTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.YieldTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.YieldTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.YieldTableNameComboBoxEdit.TabIndex = 102;
            // 
            // FormTableNameComboBoxEdit
            // 
            this.FormTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "FormTableName", true));
            this.FormTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 466);
            this.FormTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.FormTableNameComboBoxEdit.Name = "FormTableNameComboBoxEdit";
            this.FormTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FormTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.FormTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.FormTableNameComboBoxEdit.TabIndex = 101;
            // 
            // BarkTableNameComboBoxEdit
            // 
            this.BarkTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "BarkTableName", true));
            this.BarkTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 442);
            this.BarkTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.BarkTableNameComboBoxEdit.Name = "BarkTableNameComboBoxEdit";
            this.BarkTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BarkTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.BarkTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.BarkTableNameComboBoxEdit.TabIndex = 100;
            // 
            // ASuboTableNameComboBoxEdit
            // 
            this.ASuboTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "ASuboTableName", true));
            this.ASuboTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 442);
            this.ASuboTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.ASuboTableNameComboBoxEdit.Name = "ASuboTableNameComboBoxEdit";
            this.ASuboTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ASuboTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.ASuboTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ASuboTableNameComboBoxEdit.TabIndex = 99;
            // 
            // CuFtRuleComboBoxEdit
            // 
            this.CuFtRuleComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "CuFtRule", true));
            this.CuFtRuleComboBoxEdit.Location = new System.Drawing.Point(512, 418);
            this.CuFtRuleComboBoxEdit.MenuManager = this.barManager1;
            this.CuFtRuleComboBoxEdit.Name = "CuFtRuleComboBoxEdit";
            this.CuFtRuleComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CuFtRuleComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.CuFtRuleComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.CuFtRuleComboBoxEdit.TabIndex = 98;
            // 
            // BdFtRuleComboBoxEdit
            // 
            this.BdFtRuleComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "BdFtRule", true));
            this.BdFtRuleComboBoxEdit.Location = new System.Drawing.Point(183, 418);
            this.BdFtRuleComboBoxEdit.MenuManager = this.barManager1;
            this.BdFtRuleComboBoxEdit.Name = "BdFtRuleComboBoxEdit";
            this.BdFtRuleComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BdFtRuleComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.BdFtRuleComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.BdFtRuleComboBoxEdit.TabIndex = 97;
            // 
            // WoodTypeTableNameComboBoxEdit
            // 
            this.WoodTypeTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "WoodTypeTableName", true));
            this.WoodTypeTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 394);
            this.WoodTypeTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.WoodTypeTableNameComboBoxEdit.Name = "WoodTypeTableNameComboBoxEdit";
            this.WoodTypeTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WoodTypeTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.WoodTypeTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.WoodTypeTableNameComboBoxEdit.TabIndex = 96;
            // 
            // GrowthModelTableNameComboBoxEdit
            // 
            this.GrowthModelTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "GrowthModelTableName", true));
            this.GrowthModelTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 394);
            this.GrowthModelTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.GrowthModelTableNameComboBoxEdit.Name = "GrowthModelTableNameComboBoxEdit";
            this.GrowthModelTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrowthModelTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.GrowthModelTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.GrowthModelTableNameComboBoxEdit.TabIndex = 95;
            // 
            // DestinationTableNameComboBoxEdit
            // 
            this.DestinationTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "DestinationTableName", true));
            this.DestinationTableNameComboBoxEdit.Location = new System.Drawing.Point(512, 514);
            this.DestinationTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.DestinationTableNameComboBoxEdit.Name = "DestinationTableNameComboBoxEdit";
            this.DestinationTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DestinationTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.DestinationTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.DestinationTableNameComboBoxEdit.TabIndex = 106;
            // 
            // HistoryPlanningTableNameComboBoxEdit
            // 
            this.HistoryPlanningTableNameComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.projectsBindingSource, "HistoryPlanningTableName", true));
            this.HistoryPlanningTableNameComboBoxEdit.Location = new System.Drawing.Point(183, 538);
            this.HistoryPlanningTableNameComboBoxEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HistoryPlanningTableNameComboBoxEdit.MenuManager = this.barManager1;
            this.HistoryPlanningTableNameComboBoxEdit.Name = "HistoryPlanningTableNameComboBoxEdit";
            this.HistoryPlanningTableNameComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HistoryPlanningTableNameComboBoxEdit.Size = new System.Drawing.Size(166, 20);
            this.HistoryPlanningTableNameComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.HistoryPlanningTableNameComboBoxEdit.TabIndex = 107;
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 1054);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = null;
            this.tabbedControlGroup2.SelectedTabPageIndex = -1;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(682, 49);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(702, 594);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(682, 574);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 2;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(682, 574);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup7,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCrownPositionTableName,
            this.ItemForDamageTableName,
            this.ItemForHarvestSystemsTableName,
            this.ItemForProjectAdjustmentsTableName,
            this.ItemForSeedZonesTableName,
            this.ItemForSoilsTableName,
            this.ItemForStandDataSourcesTableName,
            this.ItemForTreatmentsTableName,
            this.ItemForTreeStatusTableName,
            this.ItemForVegetationsTableName,
            this.ItemForComponentsTableName,
            this.ItemForNonStockedTableName,
            this.ItemForPilingTableName,
            this.ItemForTrimTableName,
            this.ItemForFormTableName,
            this.ItemForASuboTableName,
            this.ItemForBdFtRule,
            this.ItemForGrowthModelTableName,
            this.ItemForSpeciesTableName,
            this.ItemForGradesTableName,
            this.ItemForSortsTableName,
            this.ItemForCostTableName,
            this.ItemForPriceTableName,
            this.ItemForAspectsTableName,
            this.ItemForCruisersTableName,
            this.ItemForEnvironmentsTableName,
            this.ItemForLandFormsTableName,
            this.ItemForRoadsTableName,
            this.ItemForSlashDistributionsTableName,
            this.ItemForStandActivitiesTableName,
            this.ItemForStreamsTableName,
            this.ItemForTreeSourcesTableName,
            this.ItemForUserDefinedTableName,
            this.ItemForVigorTableName,
            this.ItemForHabitatTableName,
            this.ItemForNonTimberedTableName,
            this.ItemForWoodTypeTableName,
            this.ItemForCuFtRule,
            this.ItemForBarkTableName,
            this.ItemForYieldTableName,
            this.ItemForPolesTableName,
            this.ItemForDestinationTableName,
            this.ItemForHistoryPlanningTableName});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(658, 528);
            this.layoutControlGroup5.Text = "Default Tables";
            // 
            // ItemForCrownPositionTableName
            // 
            this.ItemForCrownPositionTableName.Control = this.CrownPositionTableNameComboBoxEdit;
            this.ItemForCrownPositionTableName.Location = new System.Drawing.Point(0, 72);
            this.ItemForCrownPositionTableName.Name = "ItemForCrownPositionTableName";
            this.ItemForCrownPositionTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForCrownPositionTableName.Text = "Crown Position Table Name";
            this.ItemForCrownPositionTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForDamageTableName
            // 
            this.ItemForDamageTableName.Control = this.DamageTableNameComboBoxEdit;
            this.ItemForDamageTableName.Location = new System.Drawing.Point(0, 96);
            this.ItemForDamageTableName.Name = "ItemForDamageTableName";
            this.ItemForDamageTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForDamageTableName.Text = "Damage Table Name";
            this.ItemForDamageTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForHarvestSystemsTableName
            // 
            this.ItemForHarvestSystemsTableName.Control = this.HarvestSystemsTableNameComboBoxEdit;
            this.ItemForHarvestSystemsTableName.Location = new System.Drawing.Point(0, 120);
            this.ItemForHarvestSystemsTableName.Name = "ItemForHarvestSystemsTableName";
            this.ItemForHarvestSystemsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForHarvestSystemsTableName.Text = "Harvest Systems Table Name";
            this.ItemForHarvestSystemsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForProjectAdjustmentsTableName
            // 
            this.ItemForProjectAdjustmentsTableName.Control = this.ProjectAdjustmentsTableNameComboBoxEdit;
            this.ItemForProjectAdjustmentsTableName.Location = new System.Drawing.Point(0, 144);
            this.ItemForProjectAdjustmentsTableName.Name = "ItemForProjectAdjustmentsTableName";
            this.ItemForProjectAdjustmentsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForProjectAdjustmentsTableName.Text = "Project Adjustments Table Name";
            this.ItemForProjectAdjustmentsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSeedZonesTableName
            // 
            this.ItemForSeedZonesTableName.Control = this.SeedZonesTableNameComboBoxEdit;
            this.ItemForSeedZonesTableName.Location = new System.Drawing.Point(0, 168);
            this.ItemForSeedZonesTableName.Name = "ItemForSeedZonesTableName";
            this.ItemForSeedZonesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForSeedZonesTableName.Text = "Seed Zones Table Name";
            this.ItemForSeedZonesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSoilsTableName
            // 
            this.ItemForSoilsTableName.Control = this.SoilsTableNameComboBoxEdit;
            this.ItemForSoilsTableName.Location = new System.Drawing.Point(0, 192);
            this.ItemForSoilsTableName.Name = "ItemForSoilsTableName";
            this.ItemForSoilsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForSoilsTableName.Text = "Soils Table Name";
            this.ItemForSoilsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForStandDataSourcesTableName
            // 
            this.ItemForStandDataSourcesTableName.Control = this.StandDataSourcesTableNameComboBoxEdit;
            this.ItemForStandDataSourcesTableName.Location = new System.Drawing.Point(0, 216);
            this.ItemForStandDataSourcesTableName.Name = "ItemForStandDataSourcesTableName";
            this.ItemForStandDataSourcesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForStandDataSourcesTableName.Text = "Stand Data Sources Table Name";
            this.ItemForStandDataSourcesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForTreatmentsTableName
            // 
            this.ItemForTreatmentsTableName.Control = this.TreatmentsTableNameComboBoxEdit;
            this.ItemForTreatmentsTableName.Location = new System.Drawing.Point(0, 240);
            this.ItemForTreatmentsTableName.Name = "ItemForTreatmentsTableName";
            this.ItemForTreatmentsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForTreatmentsTableName.Text = "Treatments Table Name";
            this.ItemForTreatmentsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForTreeStatusTableName
            // 
            this.ItemForTreeStatusTableName.Control = this.TreeStatusTableNameComboBoxEdit;
            this.ItemForTreeStatusTableName.Location = new System.Drawing.Point(0, 264);
            this.ItemForTreeStatusTableName.Name = "ItemForTreeStatusTableName";
            this.ItemForTreeStatusTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForTreeStatusTableName.Text = "Tree Status Table Name";
            this.ItemForTreeStatusTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForVegetationsTableName
            // 
            this.ItemForVegetationsTableName.Control = this.VegetationsTableNameComboBoxEdit;
            this.ItemForVegetationsTableName.Location = new System.Drawing.Point(0, 288);
            this.ItemForVegetationsTableName.Name = "ItemForVegetationsTableName";
            this.ItemForVegetationsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForVegetationsTableName.Text = "Vegetations Table Name";
            this.ItemForVegetationsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForComponentsTableName
            // 
            this.ItemForComponentsTableName.Control = this.ComponentsTableNameComboBoxEdit;
            this.ItemForComponentsTableName.Location = new System.Drawing.Point(0, 312);
            this.ItemForComponentsTableName.Name = "ItemForComponentsTableName";
            this.ItemForComponentsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForComponentsTableName.Text = "Components Table Name";
            this.ItemForComponentsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForNonStockedTableName
            // 
            this.ItemForNonStockedTableName.Control = this.NonStockedTableNameComboBoxEdit;
            this.ItemForNonStockedTableName.Location = new System.Drawing.Point(0, 336);
            this.ItemForNonStockedTableName.Name = "ItemForNonStockedTableName";
            this.ItemForNonStockedTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForNonStockedTableName.Text = "Non Stocked Table Name";
            this.ItemForNonStockedTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForPilingTableName
            // 
            this.ItemForPilingTableName.Control = this.PilingTableNameComboBoxEdit;
            this.ItemForPilingTableName.Location = new System.Drawing.Point(0, 480);
            this.ItemForPilingTableName.Name = "ItemForPilingTableName";
            this.ItemForPilingTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForPilingTableName.Text = "Piling Table Name";
            this.ItemForPilingTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForTrimTableName
            // 
            this.ItemForTrimTableName.Control = this.TrimTableNameComboBoxEdit;
            this.ItemForTrimTableName.Location = new System.Drawing.Point(0, 456);
            this.ItemForTrimTableName.Name = "ItemForTrimTableName";
            this.ItemForTrimTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForTrimTableName.Text = "Trim Table Name";
            this.ItemForTrimTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForFormTableName
            // 
            this.ItemForFormTableName.Control = this.FormTableNameComboBoxEdit;
            this.ItemForFormTableName.Location = new System.Drawing.Point(0, 432);
            this.ItemForFormTableName.Name = "ItemForFormTableName";
            this.ItemForFormTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForFormTableName.Text = "Form Table Name";
            this.ItemForFormTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForASuboTableName
            // 
            this.ItemForASuboTableName.Control = this.ASuboTableNameComboBoxEdit;
            this.ItemForASuboTableName.Location = new System.Drawing.Point(0, 408);
            this.ItemForASuboTableName.Name = "ItemForASuboTableName";
            this.ItemForASuboTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForASuboTableName.Text = "ASubo Table Name";
            this.ItemForASuboTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForBdFtRule
            // 
            this.ItemForBdFtRule.Control = this.BdFtRuleComboBoxEdit;
            this.ItemForBdFtRule.Location = new System.Drawing.Point(0, 384);
            this.ItemForBdFtRule.Name = "ItemForBdFtRule";
            this.ItemForBdFtRule.Size = new System.Drawing.Size(329, 24);
            this.ItemForBdFtRule.Text = "Bd Ft Rule";
            this.ItemForBdFtRule.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForGrowthModelTableName
            // 
            this.ItemForGrowthModelTableName.Control = this.GrowthModelTableNameComboBoxEdit;
            this.ItemForGrowthModelTableName.Location = new System.Drawing.Point(0, 360);
            this.ItemForGrowthModelTableName.Name = "ItemForGrowthModelTableName";
            this.ItemForGrowthModelTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForGrowthModelTableName.Text = "Growth Model Table Name";
            this.ItemForGrowthModelTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSpeciesTableName
            // 
            this.ItemForSpeciesTableName.Control = this.SpeciesTableNameComboBoxEdit;
            this.ItemForSpeciesTableName.Location = new System.Drawing.Point(0, 0);
            this.ItemForSpeciesTableName.Name = "ItemForSpeciesTableName";
            this.ItemForSpeciesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForSpeciesTableName.Text = "Species Table Name";
            this.ItemForSpeciesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForGradesTableName
            // 
            this.ItemForGradesTableName.Control = this.GradesTableNameComboBoxEdit;
            this.ItemForGradesTableName.Location = new System.Drawing.Point(0, 24);
            this.ItemForGradesTableName.Name = "ItemForGradesTableName";
            this.ItemForGradesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForGradesTableName.Text = "Grades Table Name";
            this.ItemForGradesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSortsTableName
            // 
            this.ItemForSortsTableName.Control = this.SortsTableNameComboBoxEdit;
            this.ItemForSortsTableName.Location = new System.Drawing.Point(329, 0);
            this.ItemForSortsTableName.Name = "ItemForSortsTableName";
            this.ItemForSortsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForSortsTableName.Text = "Sorts Table Name";
            this.ItemForSortsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCostTableName
            // 
            this.ItemForCostTableName.Control = this.CostTableNameComboBoxEdit;
            this.ItemForCostTableName.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostTableName.Name = "ItemForCostTableName";
            this.ItemForCostTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForCostTableName.Text = "Cost Table Name";
            this.ItemForCostTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForPriceTableName
            // 
            this.ItemForPriceTableName.Control = this.PriceTableNameComboBoxEdit;
            this.ItemForPriceTableName.Location = new System.Drawing.Point(329, 24);
            this.ItemForPriceTableName.Name = "ItemForPriceTableName";
            this.ItemForPriceTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForPriceTableName.Text = "Price Table Name";
            this.ItemForPriceTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForAspectsTableName
            // 
            this.ItemForAspectsTableName.Control = this.AspectsTableNameComboBoxEdit;
            this.ItemForAspectsTableName.Location = new System.Drawing.Point(329, 48);
            this.ItemForAspectsTableName.Name = "ItemForAspectsTableName";
            this.ItemForAspectsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForAspectsTableName.Text = "Aspects Table Name";
            this.ItemForAspectsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCruisersTableName
            // 
            this.ItemForCruisersTableName.Control = this.CruisersTableNameComboBoxEdit;
            this.ItemForCruisersTableName.Location = new System.Drawing.Point(329, 72);
            this.ItemForCruisersTableName.Name = "ItemForCruisersTableName";
            this.ItemForCruisersTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForCruisersTableName.Text = "Cruisers Table Name";
            this.ItemForCruisersTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForEnvironmentsTableName
            // 
            this.ItemForEnvironmentsTableName.Control = this.EnvironmentsTableNameComboBoxEdit;
            this.ItemForEnvironmentsTableName.Location = new System.Drawing.Point(329, 96);
            this.ItemForEnvironmentsTableName.Name = "ItemForEnvironmentsTableName";
            this.ItemForEnvironmentsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForEnvironmentsTableName.Text = "Environments Table Name";
            this.ItemForEnvironmentsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForLandFormsTableName
            // 
            this.ItemForLandFormsTableName.Control = this.LandFormsTableNameComboBoxEdit;
            this.ItemForLandFormsTableName.Location = new System.Drawing.Point(329, 120);
            this.ItemForLandFormsTableName.Name = "ItemForLandFormsTableName";
            this.ItemForLandFormsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForLandFormsTableName.Text = "Land Forms Table Name";
            this.ItemForLandFormsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForRoadsTableName
            // 
            this.ItemForRoadsTableName.Control = this.RoadsTableNameComboBoxEdit;
            this.ItemForRoadsTableName.Location = new System.Drawing.Point(329, 144);
            this.ItemForRoadsTableName.Name = "ItemForRoadsTableName";
            this.ItemForRoadsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForRoadsTableName.Text = "Roads Table Name";
            this.ItemForRoadsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSlashDistributionsTableName
            // 
            this.ItemForSlashDistributionsTableName.Control = this.SlashDistributionsTableNameComboBoxEdit;
            this.ItemForSlashDistributionsTableName.Location = new System.Drawing.Point(329, 168);
            this.ItemForSlashDistributionsTableName.Name = "ItemForSlashDistributionsTableName";
            this.ItemForSlashDistributionsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForSlashDistributionsTableName.Text = "Slash Distributions Table Name";
            this.ItemForSlashDistributionsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForStandActivitiesTableName
            // 
            this.ItemForStandActivitiesTableName.Control = this.StandActivitiesTableNameComboBoxEdit;
            this.ItemForStandActivitiesTableName.Location = new System.Drawing.Point(329, 192);
            this.ItemForStandActivitiesTableName.Name = "ItemForStandActivitiesTableName";
            this.ItemForStandActivitiesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForStandActivitiesTableName.Text = "Stand Activities Table Name";
            this.ItemForStandActivitiesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForStreamsTableName
            // 
            this.ItemForStreamsTableName.Control = this.StreamsTableNameComboBoxEdit;
            this.ItemForStreamsTableName.Location = new System.Drawing.Point(329, 216);
            this.ItemForStreamsTableName.Name = "ItemForStreamsTableName";
            this.ItemForStreamsTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForStreamsTableName.Text = "Streams Table Name";
            this.ItemForStreamsTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForTreeSourcesTableName
            // 
            this.ItemForTreeSourcesTableName.Control = this.TreeSourcesTableNameComboBoxEdit;
            this.ItemForTreeSourcesTableName.Location = new System.Drawing.Point(329, 240);
            this.ItemForTreeSourcesTableName.Name = "ItemForTreeSourcesTableName";
            this.ItemForTreeSourcesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForTreeSourcesTableName.Text = "Tree Sources Table Name";
            this.ItemForTreeSourcesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForUserDefinedTableName
            // 
            this.ItemForUserDefinedTableName.Control = this.UserDefinedTableNameComboBoxEdit;
            this.ItemForUserDefinedTableName.Location = new System.Drawing.Point(329, 264);
            this.ItemForUserDefinedTableName.Name = "ItemForUserDefinedTableName";
            this.ItemForUserDefinedTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForUserDefinedTableName.Text = "User Defined Table Name";
            this.ItemForUserDefinedTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForVigorTableName
            // 
            this.ItemForVigorTableName.Control = this.VigorTableNameComboBoxEdit;
            this.ItemForVigorTableName.Location = new System.Drawing.Point(329, 288);
            this.ItemForVigorTableName.Name = "ItemForVigorTableName";
            this.ItemForVigorTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForVigorTableName.Text = "Vigor Table Name";
            this.ItemForVigorTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForHabitatTableName
            // 
            this.ItemForHabitatTableName.Control = this.HabitatTableNameComboBoxEdit;
            this.ItemForHabitatTableName.Location = new System.Drawing.Point(329, 312);
            this.ItemForHabitatTableName.Name = "ItemForHabitatTableName";
            this.ItemForHabitatTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForHabitatTableName.Text = "Habitat Table Name";
            this.ItemForHabitatTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForNonTimberedTableName
            // 
            this.ItemForNonTimberedTableName.Control = this.NonTimberedTableNameComboBoxEdit;
            this.ItemForNonTimberedTableName.Location = new System.Drawing.Point(329, 336);
            this.ItemForNonTimberedTableName.Name = "ItemForNonTimberedTableName";
            this.ItemForNonTimberedTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForNonTimberedTableName.Text = "Non Timbered Table Name";
            this.ItemForNonTimberedTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForWoodTypeTableName
            // 
            this.ItemForWoodTypeTableName.Control = this.WoodTypeTableNameComboBoxEdit;
            this.ItemForWoodTypeTableName.Location = new System.Drawing.Point(329, 360);
            this.ItemForWoodTypeTableName.Name = "ItemForWoodTypeTableName";
            this.ItemForWoodTypeTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForWoodTypeTableName.Text = "Wood Type Table Name";
            this.ItemForWoodTypeTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCuFtRule
            // 
            this.ItemForCuFtRule.Control = this.CuFtRuleComboBoxEdit;
            this.ItemForCuFtRule.Location = new System.Drawing.Point(329, 384);
            this.ItemForCuFtRule.Name = "ItemForCuFtRule";
            this.ItemForCuFtRule.Size = new System.Drawing.Size(329, 24);
            this.ItemForCuFtRule.Text = "Cu Ft Rule";
            this.ItemForCuFtRule.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForBarkTableName
            // 
            this.ItemForBarkTableName.Control = this.BarkTableNameComboBoxEdit;
            this.ItemForBarkTableName.Location = new System.Drawing.Point(329, 408);
            this.ItemForBarkTableName.Name = "ItemForBarkTableName";
            this.ItemForBarkTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForBarkTableName.Text = "Bark Table Name";
            this.ItemForBarkTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForYieldTableName
            // 
            this.ItemForYieldTableName.Control = this.YieldTableNameComboBoxEdit;
            this.ItemForYieldTableName.Location = new System.Drawing.Point(329, 432);
            this.ItemForYieldTableName.Name = "ItemForYieldTableName";
            this.ItemForYieldTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForYieldTableName.Text = "Yield Table Name";
            this.ItemForYieldTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForPolesTableName
            // 
            this.ItemForPolesTableName.Control = this.PolesTableNameComboBoxEdit;
            this.ItemForPolesTableName.Location = new System.Drawing.Point(329, 456);
            this.ItemForPolesTableName.Name = "ItemForPolesTableName";
            this.ItemForPolesTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForPolesTableName.Text = "Poles Table Name";
            this.ItemForPolesTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForDestinationTableName
            // 
            this.ItemForDestinationTableName.Control = this.DestinationTableNameComboBoxEdit;
            this.ItemForDestinationTableName.Location = new System.Drawing.Point(329, 480);
            this.ItemForDestinationTableName.Name = "ItemForDestinationTableName";
            this.ItemForDestinationTableName.Size = new System.Drawing.Size(329, 48);
            this.ItemForDestinationTableName.Text = "Destination Table Name";
            this.ItemForDestinationTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForHistoryPlanningTableName
            // 
            this.ItemForHistoryPlanningTableName.Control = this.HistoryPlanningTableNameComboBoxEdit;
            this.ItemForHistoryPlanningTableName.Location = new System.Drawing.Point(0, 504);
            this.ItemForHistoryPlanningTableName.Name = "ItemForHistoryPlanningTableName";
            this.ItemForHistoryPlanningTableName.Size = new System.Drawing.Size(329, 24);
            this.ItemForHistoryPlanningTableName.Text = "History Planning Table Name";
            this.ItemForHistoryPlanningTableName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForProjectName,
            this.ItemForGISProjectName,
            this.ItemForProjectNotes,
            this.ItemForProjectNumber,
            this.ItemForSaleName,
            this.ItemForProjectDate,
            this.ItemForProjectLead});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(658, 528);
            this.layoutControlGroup3.Text = "Project";
            // 
            // ItemForProjectName
            // 
            this.ItemForProjectName.Control = this.ProjectNameComboBoxEdit;
            this.ItemForProjectName.Location = new System.Drawing.Point(0, 0);
            this.ItemForProjectName.Name = "ItemForProjectName";
            this.ItemForProjectName.Size = new System.Drawing.Size(658, 24);
            this.ItemForProjectName.Text = "Project Name";
            this.ItemForProjectName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForGISProjectName
            // 
            this.ItemForGISProjectName.Control = this.GISProjectNameTextEdit;
            this.ItemForGISProjectName.Location = new System.Drawing.Point(0, 48);
            this.ItemForGISProjectName.Name = "ItemForGISProjectName";
            this.ItemForGISProjectName.Size = new System.Drawing.Size(658, 24);
            this.ItemForGISProjectName.Text = "GIS Project Name";
            this.ItemForGISProjectName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForProjectNotes
            // 
            this.ItemForProjectNotes.Control = this.ProjectNotesMemoEdit;
            this.ItemForProjectNotes.Location = new System.Drawing.Point(0, 144);
            this.ItemForProjectNotes.Name = "ItemForProjectNotes";
            this.ItemForProjectNotes.Size = new System.Drawing.Size(658, 384);
            this.ItemForProjectNotes.StartNewLine = true;
            this.ItemForProjectNotes.Text = "Project Notes";
            this.ItemForProjectNotes.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForProjectNumber
            // 
            this.ItemForProjectNumber.Control = this.ProjectNumberTextEdit;
            this.ItemForProjectNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForProjectNumber.Name = "ItemForProjectNumber";
            this.ItemForProjectNumber.Size = new System.Drawing.Size(658, 24);
            this.ItemForProjectNumber.Text = "Project Number";
            this.ItemForProjectNumber.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForSaleName
            // 
            this.ItemForSaleName.Control = this.SaleNameTextEdit;
            this.ItemForSaleName.Location = new System.Drawing.Point(0, 96);
            this.ItemForSaleName.Name = "ItemForSaleName";
            this.ItemForSaleName.Size = new System.Drawing.Size(658, 24);
            this.ItemForSaleName.Text = "Sale Name";
            this.ItemForSaleName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForProjectDate
            // 
            this.ItemForProjectDate.Control = this.ProjectDateDateEdit;
            this.ItemForProjectDate.Location = new System.Drawing.Point(0, 120);
            this.ItemForProjectDate.Name = "ItemForProjectDate";
            this.ItemForProjectDate.Size = new System.Drawing.Size(658, 24);
            this.ItemForProjectDate.Text = "Project Date";
            this.ItemForProjectDate.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForProjectLead
            // 
            this.ItemForProjectLead.Control = this.ProjectLeadTextEdit;
            this.ItemForProjectLead.Location = new System.Drawing.Point(0, 72);
            this.ItemForProjectLead.Name = "ItemForProjectLead";
            this.ItemForProjectLead.Size = new System.Drawing.Size(658, 24);
            this.ItemForProjectLead.Text = "Project Lead";
            this.ItemForProjectLead.TextSize = new System.Drawing.Size(156, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCustomerCompanyName,
            this.ItemForCustomerContactName,
            this.ItemForCustomerAddress1,
            this.ItemForCustomerAddress2,
            this.ItemForCustomerCity,
            this.ItemForCustomerState,
            this.ItemForCustomerPostalCode,
            this.ItemForCustomerOfficePhone,
            this.ItemForCustomerCellPhone,
            this.ItemForCustomerFax,
            this.ItemForCustomerEmail,
            this.ItemForCustomerNotes});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(658, 528);
            this.layoutControlGroup7.Text = "Customer";
            // 
            // ItemForCustomerCompanyName
            // 
            this.ItemForCustomerCompanyName.Control = this.CustomerCompanyNameTextEdit;
            this.ItemForCustomerCompanyName.Location = new System.Drawing.Point(0, 0);
            this.ItemForCustomerCompanyName.Name = "ItemForCustomerCompanyName";
            this.ItemForCustomerCompanyName.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerCompanyName.Text = "Customer Company Name";
            this.ItemForCustomerCompanyName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerContactName
            // 
            this.ItemForCustomerContactName.Control = this.CustomerContactNameTextEdit;
            this.ItemForCustomerContactName.Location = new System.Drawing.Point(0, 24);
            this.ItemForCustomerContactName.Name = "ItemForCustomerContactName";
            this.ItemForCustomerContactName.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerContactName.Text = "Customer Contact Name";
            this.ItemForCustomerContactName.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerAddress1
            // 
            this.ItemForCustomerAddress1.Control = this.CustomerAddress1TextEdit;
            this.ItemForCustomerAddress1.Location = new System.Drawing.Point(0, 48);
            this.ItemForCustomerAddress1.Name = "ItemForCustomerAddress1";
            this.ItemForCustomerAddress1.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerAddress1.Text = "Customer Address1";
            this.ItemForCustomerAddress1.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerAddress2
            // 
            this.ItemForCustomerAddress2.Control = this.CustomerAddress2TextEdit;
            this.ItemForCustomerAddress2.Location = new System.Drawing.Point(0, 72);
            this.ItemForCustomerAddress2.Name = "ItemForCustomerAddress2";
            this.ItemForCustomerAddress2.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerAddress2.Text = "Customer Address2";
            this.ItemForCustomerAddress2.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerCity
            // 
            this.ItemForCustomerCity.Control = this.CustomerCityTextEdit;
            this.ItemForCustomerCity.Location = new System.Drawing.Point(0, 96);
            this.ItemForCustomerCity.Name = "ItemForCustomerCity";
            this.ItemForCustomerCity.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerCity.Text = "Customer City";
            this.ItemForCustomerCity.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerState
            // 
            this.ItemForCustomerState.Control = this.CustomerStateTextEdit;
            this.ItemForCustomerState.Location = new System.Drawing.Point(0, 120);
            this.ItemForCustomerState.Name = "ItemForCustomerState";
            this.ItemForCustomerState.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerState.Text = "Customer State";
            this.ItemForCustomerState.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerPostalCode
            // 
            this.ItemForCustomerPostalCode.Control = this.CustomerPostalCodeTextEdit;
            this.ItemForCustomerPostalCode.Location = new System.Drawing.Point(0, 144);
            this.ItemForCustomerPostalCode.Name = "ItemForCustomerPostalCode";
            this.ItemForCustomerPostalCode.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerPostalCode.Text = "Customer Postal Code";
            this.ItemForCustomerPostalCode.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerOfficePhone
            // 
            this.ItemForCustomerOfficePhone.Control = this.CustomerOfficePhoneTextEdit;
            this.ItemForCustomerOfficePhone.Location = new System.Drawing.Point(0, 168);
            this.ItemForCustomerOfficePhone.Name = "ItemForCustomerOfficePhone";
            this.ItemForCustomerOfficePhone.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerOfficePhone.Text = "Customer Office Phone";
            this.ItemForCustomerOfficePhone.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerCellPhone
            // 
            this.ItemForCustomerCellPhone.Control = this.CustomerCellPhoneTextEdit;
            this.ItemForCustomerCellPhone.Location = new System.Drawing.Point(0, 192);
            this.ItemForCustomerCellPhone.Name = "ItemForCustomerCellPhone";
            this.ItemForCustomerCellPhone.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerCellPhone.Text = "Customer Cell Phone";
            this.ItemForCustomerCellPhone.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerFax
            // 
            this.ItemForCustomerFax.Control = this.CustomerFaxTextEdit;
            this.ItemForCustomerFax.Location = new System.Drawing.Point(0, 216);
            this.ItemForCustomerFax.Name = "ItemForCustomerFax";
            this.ItemForCustomerFax.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerFax.Text = "Customer Fax";
            this.ItemForCustomerFax.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerEmail
            // 
            this.ItemForCustomerEmail.Control = this.CustomerEmailTextEdit;
            this.ItemForCustomerEmail.Location = new System.Drawing.Point(0, 240);
            this.ItemForCustomerEmail.Name = "ItemForCustomerEmail";
            this.ItemForCustomerEmail.Size = new System.Drawing.Size(658, 24);
            this.ItemForCustomerEmail.Text = "Customer Email";
            this.ItemForCustomerEmail.TextSize = new System.Drawing.Size(156, 13);
            // 
            // ItemForCustomerNotes
            // 
            this.ItemForCustomerNotes.Control = this.CustomerNotesMemoEdit;
            this.ItemForCustomerNotes.Location = new System.Drawing.Point(0, 264);
            this.ItemForCustomerNotes.Name = "ItemForCustomerNotes";
            this.ItemForCustomerNotes.Size = new System.Drawing.Size(658, 264);
            this.ItemForCustomerNotes.StartNewLine = true;
            this.ItemForCustomerNotes.Text = "Customer Notes";
            this.ItemForCustomerNotes.TextSize = new System.Drawing.Size(156, 13);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            this.dxErrorProvider1.DataSource = this.projectsBindingSource;
            // 
            // ProjectEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 653);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectEditForm";
            this.Text = "Edit Project";
            this.Load += new System.EventHandler(this.ProjectEditForm_Load);
            this.Shown += new System.EventHandler(this.ProjectEditForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbProjectLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GISProjectNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectLeadTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaleNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCompanyNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerContactNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerAddress1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerAddress2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerStateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerPostalCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerOfficePhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCellPhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerFaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerNotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownPositionTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruisersTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DamageTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HarvestSystemsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandFormsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectAdjustmentsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedZonesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SlashDistributionsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoilsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StandActivitiesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StandDataSourcesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreatmentsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeSourcesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeStatusTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserDefinedTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VegetationsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VigorTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComponentsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HabitatTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortsTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GradesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStockedTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonTimberedTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PilingTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolesTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrimTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YieldTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarkTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ASuboTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuFtRuleComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BdFtRuleComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodTypeTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthModelTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryPlanningTableNameComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownPositionTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDamageTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHarvestSystemsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectAdjustmentsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeedZonesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoilsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStandDataSourcesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreatmentsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeStatusTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVegetationsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComponentsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStockedTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPilingTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrimTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFormTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForASuboTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBdFtRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrowthModelTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGradesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSortsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPriceTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAspectsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCruisersTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmentsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandFormsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoadsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSlashDistributionsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStandActivitiesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStreamsTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeSourcesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserDefinedTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVigorTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHabitatTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonTimberedTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodTypeTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCuFtRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBarkTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYieldTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPolesTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDestinationTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHistoryPlanningTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGISProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProjectLead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerOfficePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerCellPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbProjectName;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit ProjectNameComboBoxEdit;
        private System.Windows.Forms.BindingSource projectsBindingSource;
        private DevExpress.XtraEditors.TextEdit GISProjectNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ProjectNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit ProjectLeadTextEdit;
        private DevExpress.XtraEditors.MemoEdit ProjectNotesMemoEdit;
        private DevExpress.XtraEditors.TextEdit SaleNameTextEdit;
        private DevExpress.XtraEditors.DateEdit ProjectDateDateEdit;
        private DevExpress.XtraEditors.TextEdit CustomerCompanyNameTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerContactNameTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerAddress1TextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerAddress2TextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerCityTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerStateTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerPostalCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerOfficePhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerCellPhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerFaxTextEdit;
        private DevExpress.XtraEditors.TextEdit CustomerEmailTextEdit;
        private DevExpress.XtraEditors.MemoEdit CustomerNotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.ComboBoxEdit AspectsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CrownPositionTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CruisersTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit DamageTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit EnvironmentsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit HarvestSystemsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit LandFormsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit ProjectAdjustmentsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit RoadsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SeedZonesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SlashDistributionsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SoilsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SpeciesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit StandActivitiesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit StandDataSourcesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit StreamsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit TreatmentsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit TreeSourcesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit TreeStatusTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit UserDefinedTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit VegetationsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit VigorTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit ComponentsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit HabitatTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit PriceTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CostTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SortsTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit GradesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit NonStockedTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit NonTimberedTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit PilingTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit PolesTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit TrimTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit YieldTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit FormTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit BarkTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit ASuboTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CuFtRuleComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit BdFtRuleComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit WoodTypeTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit GrowthModelTableNameComboBoxEdit;
        private DevExpress.XtraEditors.ComboBoxEdit DestinationTableNameComboBoxEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerCompanyName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerContactName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerAddress1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerAddress2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerCity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerState;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerPostalCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerOfficePhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerCellPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCrownPositionTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDamageTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHarvestSystemsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectAdjustmentsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSeedZonesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoilsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStandDataSourcesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreatmentsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeStatusTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVegetationsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForComponentsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStockedTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPilingTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTrimTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFormTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForASuboTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBdFtRule;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrowthModelTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpeciesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGradesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSortsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPriceTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAspectsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCruisersTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEnvironmentsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLandFormsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoadsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSlashDistributionsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStandActivitiesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStreamsTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeSourcesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserDefinedTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVigorTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHabitatTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonTimberedTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodTypeTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCuFtRule;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBarkTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYieldTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPolesTableName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDestinationTableName;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGISProjectName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaleName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProjectLead;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.ButtonEdit tbProjectLocation;
        private DevExpress.XtraEditors.ComboBoxEdit HistoryPlanningTableNameComboBoxEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHistoryPlanningTableName;
    }
}