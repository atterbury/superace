﻿namespace SuperACE
{
    partial class StandInputErrorsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tmpStandInputExceptionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTractName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErrorMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmpStandInputExceptionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tmpStandInputExceptionBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(622, 407);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // tmpStandInputExceptionBindingSource
            // 
            this.tmpStandInputExceptionBindingSource.DataSource = typeof(Project.DAL.TmpStandInputException);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colStandsId,
            this.colTractName,
            this.colStandName,
            this.colSpecies,
            this.colSiteIndex,
            this.colErrorMessage});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colStandsId
            // 
            this.colStandsId.FieldName = "StandsId";
            this.colStandsId.Name = "colStandsId";
            // 
            // colTractName
            // 
            this.colTractName.Caption = "Tract Name";
            this.colTractName.FieldName = "TractName";
            this.colTractName.Name = "colTractName";
            this.colTractName.Visible = true;
            this.colTractName.VisibleIndex = 0;
            // 
            // colStandName
            // 
            this.colStandName.Caption = "Stand";
            this.colStandName.FieldName = "StandName";
            this.colStandName.Name = "colStandName";
            this.colStandName.Visible = true;
            this.colStandName.VisibleIndex = 1;
            this.colStandName.Width = 50;
            // 
            // colSpecies
            // 
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 2;
            // 
            // colSiteIndex
            // 
            this.colSiteIndex.FieldName = "SiteIndex";
            this.colSiteIndex.Name = "colSiteIndex";
            this.colSiteIndex.Visible = true;
            this.colSiteIndex.VisibleIndex = 3;
            // 
            // colErrorMessage
            // 
            this.colErrorMessage.FieldName = "ErrorMessage";
            this.colErrorMessage.Name = "colErrorMessage";
            this.colErrorMessage.Visible = true;
            this.colErrorMessage.VisibleIndex = 4;
            this.colErrorMessage.Width = 250;
            // 
            // StandInputErrorsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 407);
            this.Controls.Add(this.gridControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StandInputErrorsForm";
            this.Text = "Stand Input Errors";
            this.Load += new System.EventHandler(this.StandInputErrorsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmpStandInputExceptionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource tmpStandInputExceptionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colErrorMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colTractName;
        private DevExpress.XtraGrid.Columns.GridColumn colStandName;
    }
}