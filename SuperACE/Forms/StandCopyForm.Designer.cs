﻿namespace SuperACE
{
    partial class StandCopyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.tbOriginalFolder = new DevExpress.XtraEditors.TextEdit();
            this.tbDestinationFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbDestinationProject = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbOrigProject = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tempCopyStandsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrigTractName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrigStandName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrigTownship = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrigRange = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrigSection = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colNewTractName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewStandName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewTownship = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewRange = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNewSection = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOriginalFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDestinationFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDestinationProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrigProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyStandsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Process";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(517, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 419);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(517, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 395);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(517, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 395);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.tbOriginalFolder);
            this.panelControl1.Controls.Add(this.tbDestinationFolder);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cbDestinationProject);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.tbOrigProject);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(517, 155);
            this.panelControl1.TabIndex = 20;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(33, 45);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 13);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Original Folder:";
            // 
            // tbOriginalFolder
            // 
            this.tbOriginalFolder.Enabled = false;
            this.tbOriginalFolder.Location = new System.Drawing.Point(119, 42);
            this.tbOriginalFolder.MenuManager = this.barManager1;
            this.tbOriginalFolder.Name = "tbOriginalFolder";
            this.tbOriginalFolder.Size = new System.Drawing.Size(376, 20);
            this.tbOriginalFolder.TabIndex = 22;
            // 
            // tbDestinationFolder
            // 
            this.tbDestinationFolder.Enabled = false;
            this.tbDestinationFolder.Location = new System.Drawing.Point(119, 115);
            this.tbDestinationFolder.MenuManager = this.barManager1;
            this.tbDestinationFolder.Name = "tbDestinationFolder";
            this.tbDestinationFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tbDestinationFolder.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.tbDestinationFolder_Properties_ButtonClick);
            this.tbDestinationFolder.Size = new System.Drawing.Size(376, 20);
            this.tbDestinationFolder.TabIndex = 21;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 118);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(91, 13);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Destination Folder:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 92);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(95, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Destination Project:";
            // 
            // cbDestinationProject
            // 
            this.cbDestinationProject.Enabled = false;
            this.cbDestinationProject.Location = new System.Drawing.Point(119, 89);
            this.cbDestinationProject.MenuManager = this.barManager1;
            this.cbDestinationProject.Name = "cbDestinationProject";
            this.cbDestinationProject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDestinationProject.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbDestinationProject.Size = new System.Drawing.Size(188, 20);
            this.cbDestinationProject.TabIndex = 18;
            this.cbDestinationProject.SelectedValueChanged += new System.EventHandler(this.cbDestinationProject_SelectedValueChanged);
            this.cbDestinationProject.Validated += new System.EventHandler(this.cbDestinationProject_Validated);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(29, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(77, 13);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Original Project:";
            // 
            // tbOrigProject
            // 
            this.tbOrigProject.Enabled = false;
            this.tbOrigProject.Location = new System.Drawing.Point(119, 16);
            this.tbOrigProject.MenuManager = this.barManager1;
            this.tbOrigProject.Name = "tbOrigProject";
            this.tbOrigProject.Size = new System.Drawing.Size(188, 20);
            this.tbOrigProject.TabIndex = 16;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 179);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(517, 240);
            this.panelControl2.TabIndex = 21;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tempCopyStandsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4});
            this.gridControl1.Size = new System.Drawing.Size(513, 236);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // tempCopyStandsBindingSource
            // 
            this.tempCopyStandsBindingSource.DataSource = typeof(Temp.DAL.TempCopyStand);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colId,
            this.colOrigTractName,
            this.colOrigStandName,
            this.colOrigTownship,
            this.colOrigRange,
            this.colOrigSection,
            this.colNewTractName,
            this.colNewStandName,
            this.colNewTownship,
            this.colNewRange,
            this.colNewSection});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsCustomization.AllowGroup = false;
            this.bandedGridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridBand1.Caption = "Original";
            this.gridBand1.Columns.Add(this.colId);
            this.gridBand1.Columns.Add(this.colOrigTractName);
            this.gridBand1.Columns.Add(this.colOrigStandName);
            this.gridBand1.Columns.Add(this.colOrigTownship);
            this.gridBand1.Columns.Add(this.colOrigRange);
            this.gridBand1.Columns.Add(this.colOrigSection);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 205;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colOrigTractName
            // 
            this.colOrigTractName.Caption = "Tract";
            this.colOrigTractName.FieldName = "OrigTractName";
            this.colOrigTractName.Name = "colOrigTractName";
            this.colOrigTractName.OptionsColumn.AllowEdit = false;
            this.colOrigTractName.OptionsColumn.ReadOnly = true;
            this.colOrigTractName.Visible = true;
            this.colOrigTractName.Width = 65;
            // 
            // colOrigStandName
            // 
            this.colOrigStandName.Caption = "Stand";
            this.colOrigStandName.FieldName = "OrigStandName";
            this.colOrigStandName.Name = "colOrigStandName";
            this.colOrigStandName.OptionsColumn.AllowEdit = false;
            this.colOrigStandName.OptionsColumn.ReadOnly = true;
            this.colOrigStandName.Visible = true;
            this.colOrigStandName.Width = 45;
            // 
            // colOrigTownship
            // 
            this.colOrigTownship.Caption = "Twn";
            this.colOrigTownship.FieldName = "OrigTownship";
            this.colOrigTownship.Name = "colOrigTownship";
            this.colOrigTownship.OptionsColumn.AllowEdit = false;
            this.colOrigTownship.OptionsColumn.ReadOnly = true;
            this.colOrigTownship.Visible = true;
            this.colOrigTownship.Width = 35;
            // 
            // colOrigRange
            // 
            this.colOrigRange.Caption = "Rge";
            this.colOrigRange.FieldName = "OrigRange";
            this.colOrigRange.Name = "colOrigRange";
            this.colOrigRange.OptionsColumn.AllowEdit = false;
            this.colOrigRange.OptionsColumn.ReadOnly = true;
            this.colOrigRange.Visible = true;
            this.colOrigRange.Width = 35;
            // 
            // colOrigSection
            // 
            this.colOrigSection.Caption = "Sec";
            this.colOrigSection.FieldName = "OrigSection";
            this.colOrigSection.Name = "colOrigSection";
            this.colOrigSection.OptionsColumn.AllowEdit = false;
            this.colOrigSection.OptionsColumn.ReadOnly = true;
            this.colOrigSection.Visible = true;
            this.colOrigSection.Width = 25;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridBand2.Caption = "New";
            this.gridBand2.Columns.Add(this.colNewTractName);
            this.gridBand2.Columns.Add(this.colNewStandName);
            this.gridBand2.Columns.Add(this.colNewTownship);
            this.gridBand2.Columns.Add(this.colNewRange);
            this.gridBand2.Columns.Add(this.colNewSection);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 205;
            // 
            // colNewTractName
            // 
            this.colNewTractName.Caption = "Tract";
            this.colNewTractName.ColumnEdit = this.repositoryItemTextEdit1;
            this.colNewTractName.FieldName = "NewTractName";
            this.colNewTractName.Name = "colNewTractName";
            this.colNewTractName.Visible = true;
            this.colNewTractName.Width = 65;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.MaxLength = 12;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemTextEdit1_EditValueChanged);
            // 
            // colNewStandName
            // 
            this.colNewStandName.Caption = "Stand";
            this.colNewStandName.ColumnEdit = this.repositoryItemTextEdit2;
            this.colNewStandName.FieldName = "NewStandName";
            this.colNewStandName.Name = "colNewStandName";
            this.colNewStandName.Visible = true;
            this.colNewStandName.Width = 45;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit2.MaxLength = 4;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemTextEdit2_EditValueChanged);
            // 
            // colNewTownship
            // 
            this.colNewTownship.Caption = "Twn";
            this.colNewTownship.ColumnEdit = this.repositoryItemTextEdit3;
            this.colNewTownship.FieldName = "NewTownship";
            this.colNewTownship.Name = "colNewTownship";
            this.colNewTownship.Visible = true;
            this.colNewTownship.Width = 35;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit3.MaxLength = 3;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.EditValueChanged += new System.EventHandler(this.repositoryItemTextEdit3_EditValueChanged);
            // 
            // colNewRange
            // 
            this.colNewRange.Caption = "Rge";
            this.colNewRange.ColumnEdit = this.repositoryItemTextEdit3;
            this.colNewRange.FieldName = "NewRange";
            this.colNewRange.Name = "colNewRange";
            this.colNewRange.Visible = true;
            this.colNewRange.Width = 35;
            // 
            // colNewSection
            // 
            this.colNewSection.Caption = "Sec";
            this.colNewSection.ColumnEdit = this.repositoryItemTextEdit4;
            this.colNewSection.FieldName = "NewSection";
            this.colNewSection.Name = "colNewSection";
            this.colNewSection.Visible = true;
            this.colNewSection.Width = 25;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit4.MaxLength = 2;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            this.repositoryItemTextEdit4.EditValueChanged += new System.EventHandler(this.repositoryItemTextEdit4_EditValueChanged);
            // 
            // StandCopyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 419);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StandCopyForm";
            this.Text = "Stand Copy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StandCopyForm_FormClosing);
            this.Load += new System.EventHandler(this.StandCopyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOriginalFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDestinationFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDestinationProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrigProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempCopyStandsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource tempCopyStandsBindingSource;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit tbOriginalFolder;
        private DevExpress.XtraEditors.ButtonEdit tbDestinationFolder;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbDestinationProject;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbOrigProject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrigTractName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrigStandName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrigTownship;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrigRange;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrigSection;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNewTractName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNewStandName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNewTownship;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNewRange;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNewSection;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
    }
}