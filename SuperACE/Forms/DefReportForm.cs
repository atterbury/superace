﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Project.DAL;
using System.Data.Entity.Validation;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using SuperACEForms;

namespace SuperACE
{
    public partial class DefReportForm : DevExpress.XtraEditors.XtraForm
    {
        private ProjectDbContext db = null;
        private Project.DAL.Project currentProject = null;
        private Stand currentStand = null;
        //private GridView detailView = null;

        public DefReportForm(ref ProjectDbContext pContext, Project.DAL.Project pProject, Stand pStand)
        {
            InitializeComponent();

            db = pContext;
            currentProject = pProject;
            currentStand = pStand;
        }

        private void DefReportForm_Load(object sender, EventArgs e)
        {
            try
            {
                //context = new ProjectDbContext(ProjectBLL.GetConnectionString(MainForm.ProjectDataSource));
                defReportDomPriBindingSource.DataSource = db.DefReportDomPris.ToList();
                defReportSortParamsBindingSource.DataSource = db.DefReportSortParams.ToList();
                defReportReforestationBindingSource.DataSource = db.DefReportReforestations.ToList();
                defReportScalingDiaClassesBindingSource.DataSource = db.DefReportScalingDiaClasses.ToList();
                defReportLogLengthClassesBindingSource.DataSource = db.DefReportLogLengthClasses.ToList();
                defReportStandTblSizeBindingSource.DataSource = db.DefReportStandTblSizes.ToList();

                LoadDefReportAsSource();
                //defReportABindingSource.DataSource = db.DefReportAs.OrderBy(t => t.ReportAs).ToList();

                costBindingSource.Clear();
                if (currentStand != null)
                    costBindingSource.DataSource = db.Costs.OrderBy(c => c.CostType).ThenBy(c => c.CostDescription).Where(c => c.TableName == currentStand.CostTableName).ToList();
                else
                    costBindingSource.DataSource = db.Costs.OrderBy(c => c.CostType).ThenBy(c => c.CostDescription).Where(c => c.TableName == currentProject.CostTableName).ToList();

                (gridViewReportAs as BaseView).DataController.AllowIEnumerableDetails = true;
            }
            catch
            {

            }
        }

        private DataTable GetReportAsData()
        {
            string sql = "SELECT * FROM DefReportAs ORDER BY ReportAs";
            DataTable result = ProjectBLL.GetDataTableFromQuery(ref db, sql);
            return result;
        }

        private DataTable GetReportAsDtlData()
        {
            string sql = "SELECT * FROM DefReportAsDtls ORDER BY Item";
            DataTable result = ProjectBLL.GetDataTableFromQuery(ref db, sql);
            return result;
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            db.SaveChanges();
            this.Close();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            this.Close();
        }

        private void DefReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.SaveChanges();
        }

        private void gridViewReportAs_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {

        }

        private void gridViewReportAs_MasterRowExpanded(object sender, DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs e)
        {
            GridView detailView = gridViewReportAs.GetDetailView(e.RowHandle, 0) as GridView;

            detailView.OptionsView.ColumnAutoWidth = false;
            //detailView.OptionsView.NewItemRowPosition = NewItemRowPosition.Bottom;
            //detailView.OptionsDetail.AllowExpandEmptyDetails = true;


            detailView.Columns["Id"].Visible = false;
            detailView.Columns["Id"].FieldName = "Id";
            detailView.Columns["DefReportAsId"].Visible = false;
            detailView.Columns["DefReportAsId"].FieldName = "DefReportAsId";
            detailView.Columns["CostsId"].Visible = false;
            detailView.Columns["CostsId"].FieldName = "CostsId";
            detailView.Columns["DefReportA"].Visible = false;
            detailView.Columns["DefReportA"].FieldName = "DefReportA";

            detailView.Columns["CostType"].FieldName = "CostType";
            detailView.Columns["CostType"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            detailView.Columns["CostType"].AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            detailView.Columns["CostType"].Width = 75;
            detailView.Columns["CostType"].ColumnEdit = repositoryItemComboBoxCostType;

            detailView.Columns["Item"].FieldName = "Item";
            detailView.Columns["Item"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            detailView.Columns["Item"].AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            detailView.Columns["Item"].Width = 100;
            detailView.Columns["Item"].ColumnEdit = repositoryItemLookUpEditItem;
            detailView.Columns["Item"].FieldName = "Item";

            detailView.Columns["Species"].FieldName = "Species";
            detailView.Columns["Species"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            detailView.Columns["Species"].AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            detailView.Columns["Species"].Width = 75;
            detailView.ColumnPanelRowHeight = -1;
        }

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            // Report As
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridViewReportAs.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteDefReportAsRecord();
                    }
                    break;
            }
        }

        private void DeleteDefReportAsRecord()
        {
            DefReportA rec = defReportABindingSource.Current as DefReportA;
            string msg = string.Format("Delete {0} Report As?", rec.ReportAs);
            if (XtraMessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            db.DefReportAsDtls.RemoveRange(db.DefReportAsDtls.Where(r => r.DefReportAsId == rec.Id));
            db.DefReportAs.Remove(rec);
            db.SaveChanges();
            LoadDefReportAsSource();
        }

        private void LoadDefReportAsSource()
        {
            if (defReportABindingSource.Count > 0)
                defReportABindingSource.Clear();
            List<DefReportA> defReportAsColl = db.DefReportAs.OrderBy(h => h.ReportAs).ToList();
            defReportABindingSource.DataSource = defReportAsColl;
        }

        private void gridViewReportAs_RowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
            {
                AddDefReportAsRecord();
                //for (int i = 0; i < gridViewReportAs.RowCount; i++)
                //{
                //    if (gridViewReportAs.GetRow(i) == e.Row)
                //    {
                //        gridViewReportAs.SetMasterRowExpanded(i, true);
                //        ColumnView detailView = (ColumnView)gridViewReportAs.GetDetailView(i, 0);
                //        detailView.AddNewRow();
                //        detailView.UpdateCurrentRow();
                //    }
                //}
            }
        }

        private void AddDefReportAsRecord()
        {
            DefReportA newRec = defReportABindingSource.Current as DefReportA;
            if (newRec != null)
            {
                DefReportA rec = new DefReportA();
                rec.ReportAs = newRec.ReportAs;
                db.DefReportAs.Add(rec);
                db.SaveChanges();

                //detailView = gridViewReportAs.GetDetailView(gridViewReportAs.FocusedRowHandle, 0) as GridView;
                //if (detailView != null)
                //    detailView.AddNewRow();
            }
        }

        private void repositoryItemComboBoxCostType_SelectedValueChanged(object sender, EventArgs e)
        {
            var cbox = sender as ComboBoxEdit;
            if (cbox.SelectedText != null)
            {
                costBindingSource.Clear();
                if (currentStand != null)
                    costBindingSource.DataSource = db.Costs.OrderBy(c => c.CostType).ThenBy(c => c.CostDescription).Where(c => c.TableName == currentStand.CostTableName).ToList();
                else
                    costBindingSource.DataSource = db.Costs.OrderBy(c => c.CostType).ThenBy(c => c.CostDescription).Where(c => c.TableName == currentProject.CostTableName).ToList();
            }
        }

        private void gridViewReportAsDtls_InitNewRow(object sender, InitNewRowEventArgs e)
        {

        }

        private void gridViewReportAs_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
        }

        private void gridViewReportAs_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            //e.RelationCount = 1;
        }

        private void gridViewReportAs_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            //e.RelationName = "ReportAs";
        }

        private void gridViewReportAs_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            //DefReportAsDtl dtls = (DefReportAsDtl)gridViewReportAs.GetRow(e.RowHandle);
            //e.ChildList = new BindingSource(dtls, "DefReportAsDtl");
        }

        private void gridViewReportAs_DoubleClick(object sender, EventArgs e)
        {
            string costTableName = string.Empty;
            DefReportA rec = defReportABindingSource.Current as DefReportA;

            if (currentStand != null)
                costTableName = currentStand.CostTableName;
            else
                costTableName = currentProject.CostTableName;

            PopupReportAsDtls frm = new PopupReportAsDtls(ref db, rec, costTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}