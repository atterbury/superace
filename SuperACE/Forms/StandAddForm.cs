﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Project.DAL;
using Projects.DAL;
using System.Data.Entity.Validation;
using System.Text.RegularExpressions;
using System.Diagnostics;

using SuperACEUtils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;

namespace SuperACE
{
    public partial class StandAddForm : DevExpress.XtraEditors.XtraForm
    {
        public string projectDataSource = string.Empty;
        private bool bActivated = false;
        private int? standId;
        private int? pos;
        private Project.DAL.Project currentProject;
        //private Project.DAL.Stand currentStand;
        public Stand newStand = null;
        private ProjectDbContext projectContext = null;
        private ProjectsDbContext projectLocationContext = null;
        //private ProjectDbContext tblContext = null;

        public StandAddForm(ref ProjectDbContext pContext, ref ProjectsDbContext pLocContext, Project.DAL.Project pProject)
        {
            InitializeComponent();

            projectContext = pContext;
            projectLocationContext = pLocContext;
            currentProject = pProject;
        }

        private void StandAddForm_Load(object sender, EventArgs e)
        {
            stateBindingSource.DataSource = projectLocationContext.States.OrderBy(s => s.Abbr).ToList();
            speciesBindingSource.DataSource = projectContext.Species.OrderBy(s => s.Abbreviation).Where(s => s.TableName == currentProject.SpeciesTableName).ToList();
            harvestBindingSource.DataSource = projectContext.Harvestsystems.OrderBy(h => h.DisplayCode).Where(h => h.TableName == currentProject.HarvestSystemsTableName).ToList();
            nonStockedBindingSource.DataSource = projectContext.Nonstockeds.OrderBy(h => h.Description).Where(h => h.TableName == currentProject.NonStockedTableName).ToList();
            nonTimberedBindingSource.DataSource = projectContext.Nontimbereds.OrderBy(h => h.Description).Where(h => h.TableName == currentProject.NonTimberedTableName).ToList();

            tbProject.Text = currentProject.ProjectName;

            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);

            AddStand();

            LoadStandHaulingSource();
            LoadStandAgeDataSource();
            LoadStandPermPlotDataSource();
            LoadStandYardingSystemDataSource();

            repositoryItemComboBoxYarding.Items.Clear();
            List<string> yarding = ProjectBLL.GetYardingCosts(ref projectContext, newStand.CostTableName);
            foreach (var yardItem in yarding)
                repositoryItemComboBoxYarding.Items.Add(yardItem);

            this.Text = "Add Stand Master";

            this.BindControlsToStand(newStand);
            tbTract.Focus();
        }


        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // New
            SaveStand(false);

            tbProject.Text = currentProject.ProjectName;

            AddStand();
            this.Text = "Add Stand Master";

            this.BindControlsToStand(newStand);
            tbTract.Focus();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save
            SaveStand(true);
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Cancel
            projectContext.StandAges.RemoveRange(projectContext.StandAges.Where(s => s.StandsId == newStand.StandsId));
            projectContext.StandPermPlots.RemoveRange(projectContext.StandPermPlots.Where(s => s.StandsId == newStand.StandsId));
            projectContext.StandHaulings.RemoveRange(projectContext.StandHaulings.Where(s => s.StandsId == newStand.StandsId));
            projectContext.StandYardingSystems.RemoveRange(projectContext.StandYardingSystems.Where(s => s.StandsId == newStand.StandsId));
            Stand rec = projectContext.Stands.FirstOrDefault(s => s.StandsId == newStand.StandsId);
            if (rec != null)
                projectContext.Stands.Remove(rec);
            projectContext.SaveChanges();
            this.Close();
        }

        private void AddStand()
        {
            newStand = new Stand();
            InitStand(ref newStand);
            newStand.ProjectsId = currentProject.ProjectsId;
            newStand.DateOfStandData = DateTime.Now;
            newStand.GrownToDate = newStand.DateOfStandData;
            newStand.SortTableName = currentProject.SortsTableName;
            newStand.GradeTableName = currentProject.GradesTableName;
            if (string.IsNullOrEmpty(currentProject.PriceTableName))
                newStand.PriceTableName = "GENERAL";
            else
                newStand.PriceTableName = currentProject.PriceTableName;
            if (string.IsNullOrEmpty(currentProject.CostTableName))
                newStand.CostTableName = "GENERAL";
            else
                newStand.CostTableName = currentProject.CostTableName;
            newStand.SpeciesTableName = currentProject.SpeciesTableName;
            newStand.GrowthModelTableName = currentProject.GrowthModelTableName;
            newStand.Source = "TC";
            newStand.LandClass = string.Empty;

            projectContext.Stands.Add(this.newStand);
            this.projectContext.SaveChanges();
            //currentStand = newStand;
        }

        private void InitStand(ref Stand newStand)
        {
            newStand.Meridian = string.Empty;
            newStand.LegalAcres = 0;
            newStand.GrossGeographicAcres = 0;
            newStand.NetGeographicAcres = 0;
            newStand.DiameterInterval = string.Empty;
            newStand.DiameterRound = string.Empty;
            newStand.IntervalZero = string.Empty;
            newStand.SureToBeMeasured = string.Empty;
            newStand.UseStandDbh = string.Empty;
            newStand.StandAdjustmentsTableName = string.Empty;
            newStand.PriceTableName = string.Empty;
            newStand.CostTableName = string.Empty;
            newStand.TransportationsTableName = string.Empty;
            newStand.BasalAreaPerAcre = 0;
            newStand.TreesPerAcre = 0;
            newStand.TonsPerAcre = 0;
            newStand.MbfPerAcre = 0;
            newStand.CcfPerAcre = 0;
            newStand.TotalMbf = 0;
            newStand.TotalCcf = 0;
            newStand.TotalBdFtGross = 0;
            newStand.TotalBdFtNet = 0;
            newStand.TotalCuFtGross = 0;
            newStand.TotalCuFtNet = 0;
            newStand.TotalCuFtMerch = 0;
            newStand.TotalTons = 0;
            newStand.LogsPerAcre = 0;
            newStand.QmDbh = 0;
            newStand.MajorAge = 0;
            newStand.AvgTotalHeight = 0;
            newStand.TractGroup = string.Empty;
            newStand.VigorDisplayCode = string.Empty;
            newStand.CruiseFlag = string.Empty;
            newStand.InventoryFlag = string.Empty;
            newStand.DownWoddyFlag = string.Empty;
            newStand.SiteIndexBark = string.Empty;
            newStand.HarvestFlag = string.Empty;
            newStand.MajorSpecies = string.Empty;
            newStand.Baf1 = 0;
            newStand.Baf2 = 0;
            newStand.Baf3 = 0;
            newStand.Baf4 = 0;
            newStand.Baf5 = 0;
            newStand.Strip1 = 0;
            newStand.Strip2 = 0;
            newStand.Strip3 = 0;
            newStand.Strip4 = 0;
            newStand.Strip5 = 0;
            newStand.Species11 = string.Empty;
            newStand.Species12 = string.Empty;
            newStand.Species13 = string.Empty;
            newStand.SiteIndex1 = 0;
            newStand.Species21 = string.Empty;
            newStand.Species22 = string.Empty;
            newStand.Species23 = string.Empty;
            newStand.SiteIndex2 = 0;
            newStand.Species31 = string.Empty;
            newStand.Species32 = string.Empty;
            newStand.Species33 = string.Empty;
            newStand.SiteIndex3 = 0;
            newStand.Species41 = string.Empty;
            newStand.Species42 = string.Empty;
            newStand.Species43 = string.Empty;
            newStand.SiteIndex4 = 0;
            newStand.StripWidth1 = 0;
            newStand.StripWidth2 = 0;
            newStand.StripWidth3 = 0;
            newStand.StripWidth4 = 0;
            newStand.StripWidth5 = 0;
            newStand.SiteIndex = 0;
            newStand.AreaCode1 = string.Empty;
            newStand.AreaPlotAcres1 = 0;
            newStand.AreaPlotRadius1 = 0;
            newStand.AreaSide11 = 0;
            newStand.AreaSide12 = 0;
            newStand.AreaBlowup1 = 0;
            newStand.AreaCode2 = string.Empty;
            newStand.AreaPlotAcres2 = 0;
            newStand.AreaPlotRadius2 = 0;
            newStand.AreaSide21 = 0;
            newStand.AreaSide22 = 0;
            newStand.AreaBlowup2 = 0;
            newStand.AreaCode3 = string.Empty;
            newStand.AreaPlotAcres3 = 0;
            newStand.AreaPlotRadius3 = 0;
            newStand.AreaSide31 = 0;
            newStand.AreaSide32 = 0;
            newStand.AreaBlowup3 = 0;
            newStand.AreaCode4 = string.Empty;;
            newStand.AreaPlotAcres4 = 0;
            newStand.AreaPlotRadius4 = 0;
            newStand.AreaSide41 = 0;
            newStand.AreaSide42 = 0;
            newStand.AreaBlowup4 = 0;
            newStand.AreaCode5 = string.Empty;
            newStand.AreaPlotAcres5 = 0;
            newStand.AreaPlotRadius5 = 0;
            newStand.AreaSide51 = 0;
            newStand.AreaSide52 = 0;
            newStand.AreaBlowup5 = 0;
            newStand.PlotsMustBeEntered = string.Empty;
            newStand.AutoSegmentLength = 0;
            newStand.GrownToMonth = 0;
            newStand.GrownToYear = 0;
            newStand.Plots = 0;
            newStand.SpeciesTableName = string.Empty;
            newStand.SortTableName = string.Empty;
            newStand.GradeTableName = string.Empty;
            newStand.GrowthModelTableName = string.Empty;
            newStand.Harvest = string.Empty;
            newStand.NonStocked = string.Empty;
            newStand.NonTimbered = string.Empty;
            newStand.StripBlowup1 = 0;
            newStand.StripBlowup2 = 0;
            newStand.StripBlowup3 = 0;
            newStand.StripBlowup4 = 0;
            newStand.StripBlowup5 = 0;
            newStand.Source = string.Empty;
            newStand.State = string.Empty;
            newStand.County = string.Empty;
            newStand.LandClass = string.Empty;
            newStand.Attachments = false;
            newStand.BafPlotRadius1 = 0;
            newStand.BafPlotRadius2 = 0;
            newStand.BafPlotRadius3 = 0;
            newStand.BafPlotRadius4 = 0;
            newStand.BafPlotRadius5 = 0;
    }

        public void BindControlsToStand(Stand pStand)
        {
            newStand = pStand;

            PopulateMainTables();

            if (standAgeBindingSource.Count > 0)
                standAgeBindingSource.Clear();
            standAgeBindingSource.DataSource = projectContext.StandAges.Where(s => s.StandsId == newStand.StandsId).OrderBy(s => s.AgeCode).ToList();
            if (standPermPlotBindingSource.Count > 0)
                standPermPlotBindingSource.Clear();
            standPermPlotBindingSource.DataSource = projectContext.StandPermPlots.Where(s => s.StandsId == newStand.StandsId).OrderBy(s => s.PermPlotOccasion).ToList();
            LoadStandYardingSystemDataSource();

            this.cbGrowthModel.Text = "Growth Table Missing";

            tbProject.Text = currentProject.ProjectName;
            cbState.Text = newStand.State;
            cbCounty.Text = newStand.County;
            tbTract.Text = newStand.TractName;
            tbStand.Text = newStand.StandName;
            tbTownship.Text = newStand.Township;
            tbRange.Text = newStand.Range;
            tbSection.Text = newStand.Section;
            tbNetAcres.Text = newStand.NetGeographicAcres.ToString();

            cbSpeciesTable.Text = currentProject.SpeciesTableName;
            cbSortTable.Text = currentProject.SortsTableName;
            cbGradeTable.Text = currentProject.GradesTableName;
            cbPriceTable.Text = currentProject.PriceTableName;
            cbCostTable.Text = currentProject.CostTableName;

            cbNonStockedTable.Text = newStand.NonStocked;
            cbNonTimberedTable.Text = newStand.NonTimbered;
            cbHarvestTable.Text = newStand.Harvest;

            DateTime d1 = (DateTime)newStand.DateOfStandData;
            tbInputDate.Text = d1.ToShortDateString();
            DateTime d2 = (DateTime)newStand.GrownToDate;
            tbGrownToDate.Text = d1.ToShortDateString();
            //tbGrownToDate.Text = newStand.GrownToMonth.ToString();
            //tbGrownToYear.Text = newStand.GrownToYear.ToString();

            tbAutoSegmentLength.Text = newStand.AutoSegmentLength.ToString();

            tbSpecies11.Text = newStand.Species11;
            tbSpecies12.Text = newStand.Species12;
            tbSpecies13.Text = newStand.Species13;
            tbSiteIndex1.Text = newStand.SiteIndex1.ToString();
            tbSpecies21.Text = newStand.Species21;
            tbSpecies22.Text = newStand.Species22;
            tbSpecies23.Text = newStand.Species23;
            tbSiteIndex2.Text = newStand.SiteIndex2.ToString();
            tbSpecies31.Text = newStand.Species31;
            tbSpecies32.Text = newStand.Species32;
            tbSpecies33.Text = newStand.Species33;
            tbSiteIndex3.Text = newStand.SiteIndex3.ToString();
            tbSpecies41.Text = newStand.Species41;
            tbSpecies42.Text = newStand.Species42;
            tbSpecies43.Text = newStand.Species43;
            tbSiteIndex4.Text = newStand.SiteIndex4.ToString();
            //tbAge1.Text = newStand.Age1.ToString();
            //tbAge2.Text = newStand.Age2.ToString();
            //tbAge3.Text = newStand.Age3.ToString();
            //tbAge4.Text = newStand.Age4.ToString();
            //tbAge5.Text = newStand.Age5.ToString();
            //tbAge6.Text = newStand.Age6.ToString();
            //tbAge7.Text = newStand.Age7.ToString();
            //tbAge8.Text = newStand.Age8.ToString();
            //tbAge9.Text = newStand.Age9.ToString();
            tbBaf1.Text = string.Format("{0:0.00}", newStand.Baf1);
            tbBafLimit1.Text = Utils.CalculateLimitingDistanceFactor(tbBaf1.Text);
            tbBaf2.Text = string.Format("{0:0.00}", newStand.Baf2);
            tbBafLimit2.Text = Utils.CalculateLimitingDistanceFactor(tbBaf2.Text);
            tbBaf3.Text = string.Format("{0:0.00}", newStand.Baf3);
            tbBafLimit3.Text = Utils.CalculateLimitingDistanceFactor(tbBaf3.Text);
            tbBaf4.Text = string.Format("{0:0.00}", newStand.Baf4);
            tbBafLimit4.Text = Utils.CalculateLimitingDistanceFactor(tbBaf4.Text);
            tbBaf5.Text = string.Format("{0:0.00}", newStand.Baf5);
            tbBafLimit5.Text = Utils.CalculateLimitingDistanceFactor(tbBaf5.Text);

            //if (string.IsNullOrEmpty(newStand.AreaCode1))
            //    LoadAreaEntries();

            tbAreaCode1.Text = newStand.AreaCode1;
            tbAreaPlotAcres1.Text = string.Format("{0:0.000}", newStand.AreaPlotAcres1);
            tbAreaPlotRadius1.Text = string.Format("{0:0.00}", newStand.AreaPlotRadius1);
            tbAreaSide11.Text = string.Format("{0:0.00}", newStand.AreaSide11);
            tbAreaSide12.Text = string.Format("{0:0.00}", newStand.AreaSide12);
            tbAreaBlowup1.Text = string.Format("{0:0.00}", newStand.AreaBlowup1);
            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);

            tbAreaCode2.Text = newStand.AreaCode2;
            tbAreaPlotAcres2.Text = string.Format("{0:0.000}", newStand.AreaPlotAcres2);
            tbAreaPlotRadius2.Text = string.Format("{0:0.00}", newStand.AreaPlotRadius2);
            tbAreaSide21.Text = string.Format("{0:0.00}", newStand.AreaSide21);
            tbAreaSide22.Text = string.Format("{0:0.00}", newStand.AreaSide22);
            tbAreaBlowup2.Text = string.Format("{0:0.00}", newStand.AreaBlowup2);
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);

            tbAreaCode3.Text = newStand.AreaCode3;
            tbAreaPlotAcres3.Text = string.Format("{0:0.000}", newStand.AreaPlotAcres3);
            tbAreaPlotRadius3.Text = string.Format("{0:0.00}", newStand.AreaPlotRadius3);
            tbAreaSide31.Text = string.Format("{0:0.00}", newStand.AreaSide31);
            tbAreaSide32.Text = string.Format("{0:0.00}", newStand.AreaSide32);
            tbAreaBlowup3.Text = string.Format("{0:0.00}", newStand.AreaBlowup3);
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);

            tbAreaCode4.Text = newStand.AreaCode4;
            tbAreaPlotAcres4.Text = string.Format("{0:0.000}", newStand.AreaPlotAcres4);
            tbAreaPlotRadius4.Text = string.Format("{0:0.00}", newStand.AreaPlotRadius4);
            tbAreaSide41.Text = string.Format("{0:0.00}", newStand.AreaSide41);
            tbAreaSide42.Text = string.Format("{0:0.00}", newStand.AreaSide42);
            tbAreaBlowup4.Text = string.Format("{0:0.00}", newStand.AreaBlowup4);
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);

            tbAreaCode5.Text = newStand.AreaCode5;
            tbAreaPlotAcres5.Text = string.Format("{0:0.000}", newStand.AreaPlotAcres5);
            tbAreaPlotRadius5.Text = string.Format("{0:0.00}", newStand.AreaPlotRadius5);
            tbAreaSide51.Text = string.Format("{0:0.00}", newStand.AreaSide51);
            tbAreaSide52.Text = string.Format("{0:0.00}", newStand.AreaSide52);
            tbAreaBlowup5.Text = string.Format("{0:0.00}", newStand.AreaBlowup5);
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);

            tbStripInterval1.Text = newStand.Strip1.ToString();
            tbStripWidth1.Text = newStand.StripWidth1.ToString();
            if (Utils.IsValidFloat(tbStripInterval1.Text) && Utils.IsValidFloat(tbStripWidth1.Text))
                tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text, tbStripExpansion1.Text);
            tbStripInterval2.Text = newStand.Strip2.ToString();
            tbStripWidth2.Text = newStand.StripWidth2.ToString();
            if (Utils.IsValidFloat(tbStripInterval2.Text) && Utils.IsValidFloat(tbStripWidth2.Text))
                tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text, tbStripExpansion2.Text);
            tbStripInterval3.Text = newStand.Strip3.ToString();
            tbStripWidth3.Text = newStand.StripWidth3.ToString();
            if (Utils.IsValidFloat(tbStripInterval3.Text) && Utils.IsValidFloat(tbStripWidth3.Text))
                tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text, tbStripExpansion3.Text);
            tbStripInterval4.Text = newStand.Strip4.ToString();
            tbStripWidth4.Text = newStand.StripWidth4.ToString();
            if (Utils.IsValidFloat(tbStripInterval4.Text) && Utils.IsValidFloat(tbStripWidth4.Text))
                tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text, tbStripExpansion4.Text);
            tbStripInterval5.Text = newStand.Strip5.ToString();
            tbStripWidth5.Text = newStand.StripWidth5.ToString();
            if (Utils.IsValidFloat(tbStripInterval5.Text) && Utils.IsValidFloat(tbStripWidth5.Text))
                tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text, tbStripExpansion5.Text);

            tbTract.Focus();
        }

        private void PopulateMainTables()
        {
            var qSpc = (from s in projectContext.Species
                        group s by s.TableName into myGroup
                        orderby myGroup.Key
                        select new { TableName = myGroup.Key, myGroup }).ToList();
            foreach (var spcItem in qSpc)
            {
                cbSpeciesTable.Properties.Items.Add(spcItem.TableName);
            }

            var qSort = (from s in projectContext.Sorts
                         group s by s.TableName into myGroup
                         orderby myGroup.Key
                         select new { TableName = myGroup.Key, myGroup }).ToList();
            foreach (var srtItem in qSort)
            {
                cbSortTable.Properties.Items.Add(srtItem.TableName);
            }

            var qGrade = (from s in projectContext.Grades
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key, myGroup }).ToList();
            foreach (var grdItem in qGrade)
            {
                cbGradeTable.Properties.Items.Add(grdItem.TableName);
            }

            var qPrice = (from s in projectContext.Prices
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key, myGroup }).ToList();
            foreach (var priceItem in qPrice)
            {
                cbPriceTable.Properties.Items.Add(priceItem.TableName);
            }
            
            var qCost = (from s in projectContext.Costs
                         group s by s.TableName into myGroup
                         orderby myGroup.Key
                         select new { TableName = myGroup.Key, myGroup }).ToList();
            foreach (var costItem in qCost)
            {
                cbCostTable.Properties.Items.Add(costItem.TableName);
            }

            if (haulingBindingSource.Count > 0)
                haulingBindingSource.Clear();
            List<Hauling> hauling = projectContext.Haulings.Where(h => h.TableName == "GENERAL").ToList();
            haulingBindingSource.DataSource = hauling;
        }

        private void HandleExceptionOnSaveChanges(string exceptionMessage)
        {
            MessageBox.Show("Saving operation failed! Internal error has occured. Exception message: \n\r" + exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.None;
        }

        private bool CheckProperties()
        {
            bool bReturn = true;
            if (ValidateString(tbTract, "Tract is required"))
            {
                bReturn = false;
            }
            if (ValidateString(tbStand, "Stand is required"))
            {
                bReturn = false;
            }
            if (ValidateString(tbTownship, "Township is required"))
            {
                bReturn = false;
            }
            if (ValidateString(tbRange, "Range is required"))
            {
                bReturn = false;
            }
            if (ValidateString(tbSection, "Section is required"))
            {
                bReturn = false;
            }
            if (ValidateDouble(true, tbNetAcres, "Invalid Acres entered"))
            {
                bReturn = false;
            }
            if (ValidateDate(tbInputDate, "Date is required"))
            {
                bReturn = false;
            }
            if (newStand == null)
            {
                if (DoesStandExist())
                {
                    dxErrorProvider1.SetError(tbStand, "Stand Already Exists");
                    bReturn = false;
                }
                else
                {
                    dxErrorProvider1.SetError(tbStand, string.Empty);
                    bReturn = true;
                }
            }

            //int age = (Utils.ConvertToInt(tbAge1.Text) + Utils.ConvertToInt(tbAge2.Text) + Utils.ConvertToInt(tbAge3.Text) +
            //    Utils.ConvertToInt(tbAge4.Text) + Utils.ConvertToInt(tbAge5.Text) + Utils.ConvertToInt(tbAge6.Text) +
            //    Utils.ConvertToInt(tbAge7.Text) + Utils.ConvertToInt(tbAge8.Text) + Utils.ConvertToInt(tbAge9.Text));
            //if (age == 0)
            //{
            //    dxErrorProvider1.SetError(tbAge1, "Must have an Age");
            //    bReturn = false;
            //}
            //else
            //{
            //    dxErrorProvider1.SetError(tbAge1, string.Empty);
            //    bReturn = true;
            //}

            if (ValidateDouble(false, tbBaf1, "Invalid BAF entered"))
                bReturn = false;
            if (ValidateDouble(false, tbBaf2, "Invalid BAF entered"))
                bReturn = false;
            if (ValidateDouble(false, tbBaf3, "Invalid BAF entered"))
                bReturn = false;
            if (ValidateDouble(false, tbBaf4, "Invalid BAF entered"))
                bReturn = false;
            if (ValidateDouble(false, tbBaf5, "Invalid BAF entered"))
                bReturn = false;

            if (!string.IsNullOrEmpty(tbAreaCode1.Text))
            {
                switch (tbAreaCode1.Text)
                {
                    case "A1":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A2":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A3":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A4":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A5":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;

                    case "F1":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F2":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F3":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F4":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F5":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;

                    case "R1":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R2":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R3":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R4":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R5":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(tbAreaCode2.Text))
            {
                switch (tbAreaCode2.Text)
                {
                    case "A1":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A2":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A3":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A4":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A5":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "F1":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F2":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F3":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F4":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F5":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;

                    case "R1":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R2":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R3":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R4":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R5":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(tbAreaCode3.Text))
            {
                switch (tbAreaCode3.Text)
                {
                    case "A1":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A2":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A3":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A4":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A5":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "F1":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F2":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F3":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F4":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F5":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;

                    case "R1":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R2":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R3":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R4":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R5":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(tbAreaCode4.Text))
            {
                switch (tbAreaCode4.Text)
                {
                    case "A1":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A2":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A3":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A4":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A5":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "F1":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F2":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F3":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F4":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F5":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;

                    case "R1":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R2":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R3":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R4":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R5":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(tbAreaCode5.Text))
            {
                switch (tbAreaCode5.Text)
                {
                    case "A1":
                        if (ValidateDouble(true, tbAreaSide11, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide12, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A2":
                        if (ValidateDouble(true, tbAreaSide21, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide22, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A3":
                        if (ValidateDouble(true, tbAreaSide31, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide32, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A4":
                        if (ValidateDouble(true, tbAreaSide41, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide42, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "A5":
                        if (ValidateDouble(true, tbAreaSide51, "Side 1 is missing"))
                            bReturn = false;
                        if (ValidateDouble(true, tbAreaSide52, "Side 2 is missing"))
                            bReturn = false;
                        break;
                    case "F1":
                        if (ValidateDouble(true, tbAreaPlotAcres1, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F2":
                        if (ValidateDouble(true, tbAreaPlotAcres2, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F3":
                        if (ValidateDouble(true, tbAreaPlotAcres3, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F4":
                        if (ValidateDouble(true, tbAreaPlotAcres4, "Plot Acres is missing"))
                            bReturn = false;
                        break;
                    case "F5":
                        if (ValidateDouble(true, tbAreaPlotAcres5, "Plot Acres is missing"))
                            bReturn = false;
                        break;

                    case "R1":
                        if (ValidateDouble(true, tbAreaPlotRadius1, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R2":
                        if (ValidateDouble(true, tbAreaPlotRadius2, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R3":
                        if (ValidateDouble(true, tbAreaPlotRadius3, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R4":
                        if (ValidateDouble(true, tbAreaPlotRadius4, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                    case "R5":
                        if (ValidateDouble(true, tbAreaPlotRadius5, "Plot Radius is missing"))
                            bReturn = false;
                        break;
                }
            }

            return bReturn;
        }

        private bool DoesStandExist()
        {
            bool exist = projectContext.Stands.Any(s => s.Township == tbTownship.Text && s.Range == tbRange.Text &&
                s.Section == tbSection.Text && s.StandName == tbStand.Text);
            return (exist);
        }

        private void SetStandProperties()
        {
            this.newStand.State = string.IsNullOrEmpty(this.cbState.Text.Trim()) ? string.Empty : this.cbState.Text.Trim();
            this.newStand.County = string.IsNullOrEmpty(this.cbCounty.Text.Trim()) ? string.Empty : this.cbCounty.Text.Trim();
            this.newStand.TractName = string.IsNullOrEmpty(this.tbTract.Text.Trim()) ? string.Empty : this.tbTract.Text.Trim();
            this.newStand.StandName = string.IsNullOrEmpty(this.tbStand.Text.Trim()) ? string.Empty : this.tbStand.Text.Trim();
            this.newStand.Township = string.IsNullOrEmpty(this.tbTownship.Text.Trim()) ? string.Empty : this.tbTownship.Text.Trim();
            this.newStand.Range = string.IsNullOrEmpty(this.tbRange.Text.Trim()) ? string.Empty : this.tbRange.Text.Trim();
            this.newStand.Section = string.IsNullOrEmpty(this.tbSection.Text.Trim()) ? string.Empty : this.tbSection.Text.Trim();
            this.newStand.NetGeographicAcres = Utils.ConvertToDouble(this.tbNetAcres.Text.Trim());
            this.newStand.TractGroup = newStand.TractName;

            this.newStand.DateOfStandData = Convert.ToDateTime(tbInputDate.Text);
            this.newStand.GrownToDate = Convert.ToDateTime(tbGrownToDate.Text);
            //this.newStand.GrownToMonth = Utils.ConvertToInt(tbGrownToDate.Text);
            //this.newStand.GrownToYear = Utils.ConvertToInt(tbGrownToYear.Text);

            newStand.AutoSegmentLength = (short)Utils.ConvertToInt(tbAutoSegmentLength.Text);

            this.newStand.Harvest = string.IsNullOrEmpty(this.cbHarvestTable.Text.Trim()) ? string.Empty : this.cbHarvestTable.Text.Trim();

            this.newStand.Species11 = string.IsNullOrEmpty(this.tbSpecies11.Text.Trim()) ? string.Empty : this.tbSpecies11.Text.Trim();
            this.newStand.Species12 = string.IsNullOrEmpty(this.tbSpecies12.Text.Trim()) ? string.Empty : this.tbSpecies12.Text.Trim();
            this.newStand.Species13 = string.IsNullOrEmpty(this.tbSpecies13.Text.Trim()) ? string.Empty : this.tbSpecies13.Text.Trim();
            this.newStand.SiteIndex1 = (short)Utils.ConvertToInt(tbSiteIndex1.Text);

            this.newStand.Species21 = string.IsNullOrEmpty(this.tbSpecies21.Text.Trim()) ? string.Empty : this.tbSpecies21.Text.Trim();
            this.newStand.Species22 = string.IsNullOrEmpty(this.tbSpecies22.Text.Trim()) ? string.Empty : this.tbSpecies22.Text.Trim();
            this.newStand.Species23 = string.IsNullOrEmpty(this.tbSpecies23.Text.Trim()) ? string.Empty : this.tbSpecies23.Text.Trim();
            this.newStand.SiteIndex2 = (short)Utils.ConvertToInt(tbSiteIndex2.Text);

            this.newStand.Species31 = string.IsNullOrEmpty(this.tbSpecies31.Text.Trim()) ? string.Empty : this.tbSpecies31.Text.Trim();
            this.newStand.Species32 = string.IsNullOrEmpty(this.tbSpecies32.Text.Trim()) ? string.Empty : this.tbSpecies32.Text.Trim();
            this.newStand.Species33 = string.IsNullOrEmpty(this.tbSpecies33.Text.Trim()) ? string.Empty : this.tbSpecies33.Text.Trim();
            this.newStand.SiteIndex3 = (short)Utils.ConvertToInt(tbSiteIndex3.Text);

            this.newStand.Species41 = string.IsNullOrEmpty(this.tbSpecies41.Text.Trim()) ? string.Empty : this.tbSpecies41.Text.Trim();
            this.newStand.Species42 = string.IsNullOrEmpty(this.tbSpecies42.Text.Trim()) ? string.Empty : this.tbSpecies42.Text.Trim();
            this.newStand.Species43 = string.IsNullOrEmpty(this.tbSpecies43.Text.Trim()) ? string.Empty : this.tbSpecies43.Text.Trim();
            this.newStand.SiteIndex4 = (short)Utils.ConvertToInt(this.tbSiteIndex4.Text);

            //this.newStand.Age1 = (short)Utils.ConvertToInt(this.tbAge1.Text);
            //this.newStand.Age2 = (short)Utils.ConvertToInt(this.tbAge2.Text);
            //this.newStand.Age3 = (short)Utils.ConvertToInt(this.tbAge3.Text);
            //this.newStand.Age4 = (short)Utils.ConvertToInt(this.tbAge4.Text);
            //this.newStand.Age5 = (short)Utils.ConvertToInt(this.tbAge5.Text);
            //this.newStand.Age6 = (short)Utils.ConvertToInt(this.tbAge6.Text);
            //this.newStand.Age7 = (short)Utils.ConvertToInt(this.tbAge7.Text);
            //this.newStand.Age8 = (short)Utils.ConvertToInt(this.tbAge8.Text);
            //this.newStand.Age9 = (short)Utils.ConvertToInt(this.tbAge9.Text);

            this.newStand.Baf1 = Utils.ConvertToDouble(this.tbBaf1.Text);
            this.newStand.Baf2 = Utils.ConvertToDouble(this.tbBaf2.Text);
            this.newStand.Baf3 = Utils.ConvertToDouble(this.tbBaf3.Text);
            this.newStand.Baf4 = Utils.ConvertToDouble(this.tbBaf4.Text);
            this.newStand.Baf5 = Utils.ConvertToDouble(this.tbBaf5.Text);

            this.newStand.AreaCode1 = this.tbAreaCode1.Text;
            this.newStand.AreaPlotAcres1 = Utils.ConvertToFloat(this.tbAreaPlotAcres1.Text);
            this.newStand.AreaPlotRadius1 = Utils.ConvertToFloat(this.tbAreaPlotRadius1.Text);
            this.newStand.AreaSide11 = Utils.ConvertToFloat(this.tbAreaSide11.Text);
            this.newStand.AreaSide12 = Utils.ConvertToFloat(this.tbAreaSide12.Text);
            this.newStand.AreaBlowup1 = Utils.ConvertToFloat(this.tbAreaBlowup1.Text);

            this.newStand.AreaCode2 = this.tbAreaCode2.Text;
            this.newStand.AreaPlotAcres2 = Utils.ConvertToFloat(this.tbAreaPlotAcres2.Text);
            this.newStand.AreaPlotRadius2 = Utils.ConvertToFloat(this.tbAreaPlotRadius2.Text);
            this.newStand.AreaSide21 = Utils.ConvertToFloat(this.tbAreaSide21.Text);
            this.newStand.AreaSide22 = Utils.ConvertToFloat(this.tbAreaSide22.Text);
            this.newStand.AreaBlowup2 = Utils.ConvertToFloat(this.tbAreaBlowup2.Text);

            this.newStand.AreaCode3 = tbAreaCode3.Text;
            this.newStand.AreaPlotAcres3 = Utils.ConvertToFloat(this.tbAreaPlotAcres3.Text);
            this.newStand.AreaPlotRadius3 = Utils.ConvertToFloat(this.tbAreaPlotRadius3.Text);
            this.newStand.AreaSide31 = Utils.ConvertToFloat(this.tbAreaSide31.Text);
            this.newStand.AreaSide32 = Utils.ConvertToFloat(this.tbAreaSide32.Text);
            this.newStand.AreaBlowup3 = Utils.ConvertToFloat(this.tbAreaBlowup3.Text);

            this.newStand.AreaCode4 = this.tbAreaCode4.Text;
            this.newStand.AreaPlotAcres4 = Utils.ConvertToFloat(this.tbAreaPlotAcres4.Text);
            this.newStand.AreaPlotRadius4 = Utils.ConvertToFloat(tbAreaPlotRadius4.Text);
            this.newStand.AreaSide41 = Utils.ConvertToFloat(this.tbAreaSide41.Text);
            this.newStand.AreaSide42 = Utils.ConvertToFloat(this.tbAreaSide42.Text);
            this.newStand.AreaBlowup4 = Utils.ConvertToFloat(this.tbAreaBlowup4.Text);

            this.newStand.AreaCode5 = this.tbAreaCode5.Text;
            this.newStand.AreaPlotAcres5 = Utils.ConvertToFloat(this.tbAreaPlotAcres5.Text);
            this.newStand.AreaPlotRadius5 = Utils.ConvertToFloat(this.tbAreaPlotRadius5.Text);
            this.newStand.AreaSide51 = Utils.ConvertToFloat(this.tbAreaSide51.Text);
            this.newStand.AreaSide52 = Utils.ConvertToFloat(this.tbAreaSide52.Text);
            this.newStand.AreaBlowup5 = Utils.ConvertToFloat(this.tbAreaBlowup5.Text);

            this.newStand.Strip1 = Utils.ConvertToDouble(tbStripInterval1.Text);
            this.newStand.StripWidth1 = Utils.ConvertToDouble(tbStripWidth1.Text);
            this.newStand.StripBlowup1 = Utils.ConvertToDouble(tbStripExpansion1.Text);
            this.newStand.Strip2 = Utils.ConvertToDouble(tbStripInterval2.Text);
            this.newStand.StripWidth2 = Utils.ConvertToDouble(tbStripWidth2.Text);
            this.newStand.StripBlowup2 = Utils.ConvertToDouble(tbStripExpansion2.Text);
            this.newStand.Strip3 = Utils.ConvertToDouble(tbStripInterval3.Text);
            this.newStand.StripWidth3 = Utils.ConvertToDouble(tbStripWidth3.Text);
            this.newStand.StripBlowup3 = Utils.ConvertToDouble(tbStripExpansion3.Text);
            this.newStand.Strip4 = Utils.ConvertToDouble(tbStripInterval4.Text);
            this.newStand.StripWidth4 = Utils.ConvertToDouble(tbStripWidth4.Text);
            this.newStand.StripBlowup4 = Utils.ConvertToDouble(tbStripExpansion4.Text);
            this.newStand.Strip5 = Utils.ConvertToDouble(tbStripInterval5.Text);
            this.newStand.StripWidth5 = Utils.ConvertToDouble(tbStripWidth5.Text);
            this.newStand.StripBlowup5 = Utils.ConvertToDouble(tbStripExpansion5.Text);

            this.newStand.SpeciesTableName = cbSpeciesTable.Text;
            this.newStand.SortTableName = cbSortTable.Text;
            this.newStand.GradeTableName = cbGradeTable.Text;
            this.newStand.PriceTableName = cbPriceTable.Text;
            this.newStand.CostTableName = cbCostTable.Text;
            this.newStand.NonStocked = cbNonStockedTable.Text;
            this.newStand.NonTimbered = cbNonTimberedTable.Text;

            if (string.IsNullOrEmpty(this.newStand.NonStocked) && string.IsNullOrEmpty(this.newStand.NonTimbered))
                this.newStand.LandClass = "TIM";
            else
                if (!string.IsNullOrEmpty(this.newStand.NonStocked))
                    this.newStand.LandClass = "NS";
                else
                    if (!string.IsNullOrEmpty(this.newStand.NonTimbered))
                        this.newStand.LandClass = "NT";
        }

        private void gridControl4_Validating(object sender, CancelEventArgs e)
        {
            int p = 0;
            for (int i = 0; i < gridView4.RowCount; i++)
                p += Convert.ToInt32(gridView4.GetRowCellValue(i, "Percent"));
            if (p != 100)
            {
                XtraMessageBox.Show("Percent must Total 100 %.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
                e.Cancel = false;
        }

        private void SaveStand(bool bClose)
        {
            // Save
            bool bSuccess = true;

            try
            {
                gridView1.PostEditor();
                gridView1.UpdateCurrentRow();
                haulingBindingSource.EndEdit();
                gridView2.PostEditor();
                gridView2.UpdateCurrentRow();
                standAgeBindingSource.EndEdit();
                gridView3.PostEditor();
                gridView3.UpdateCurrentRow();
                standPermPlotBindingSource.EndEdit();
                gridView4.PostEditor();
                gridView4.UpdateCurrentRow();
                standYardingSystemBindingSource.EndEdit();

                SetStandProperties();

                for (int i = 0; i < gridView4.DataRowCount; i++)
                {
                    string desc = gridView4.GetRowCellValue(i, "Description").ToString();
                    string unit = gridView4.GetRowCellValue(i, "CostUnit").ToString();
                    float percent = Utils.ConvertToFloat(gridView4.GetRowCellValue(i, "Percent").ToString());

                    StandYardingSystem syRec = projectContext.StandYardingSystems.FirstOrDefault(s => s.StandsId == newStand.StandsId && s.Description == desc);
                    if (syRec != null)
                    {
                        if (syRec.Percent != percent)
                            syRec.Percent = percent;
                        if (syRec.CostUnit != unit)
                            syRec.CostUnit = unit;
                    }
                }

                projectContext.SaveChanges();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Saving operation failed! Provided Stand information is invalid" +
                    System.Environment.NewLine + "Exception message:" +
                    System.Environment.NewLine + ex.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                return;
            }

            if (!CheckStandYardingSystem())
            {
                XtraMessageBox.Show("Percent must Total 100 %.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            //if (CheckProperties())
            //{
                try
                {
                    //projectContext.Stands.Add(this.newStand);
                    this.projectContext.SaveChanges();
                    if (bClose)
                        this.Close();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.HandleExceptionOnSaveChanges(ex.Message);
                    return;
                }
            //}
        }

        private bool ValidateString(TextEdit p, string pMsg)
        {
            if (string.IsNullOrEmpty(p.Text))
            {
                dxErrorProvider1.SetError(p, pMsg);
                p.Focus();
                p.SelectionStart = 0;
                return true;
            }
            dxErrorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateDate(TextEdit p, string pMsg)
        {
            DateTime temp;
            if (DateTime.TryParse(p.Text, out temp))
            {
                dxErrorProvider1.SetError(p, string.Empty);
                return false;
            }
            else
            {
                dxErrorProvider1.SetError(p, pMsg);
                p.Focus();
                return true;
            }
        }

        private bool ValidateDouble(bool pRequired, TextEdit p, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                if (!Utils.IsValidFloat(p.Text))
                {
                    dxErrorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
                else
                {
                    dxErrorProvider1.SetError(p, string.Empty);
                    return false;
                }
            }
            else
            {
                if (pRequired)
                {
                    dxErrorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
            }
            dxErrorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateInt(bool pRequired, TextEdit p, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                if (!Utils.IsValidFloat(p.Text))
                {
                    dxErrorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
                else
                {
                    dxErrorProvider1.SetError(p, string.Empty);
                    return false;
                }
            }
            else
            {
                if (pRequired)
                {
                    dxErrorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
            }
            dxErrorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateSpecies(TextEdit p, string pTable, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                bool bExist = ProjectBLL.DoesSpeciesExist(ref MainForm.projectContext, pTable, p.Text.Trim());
                //mRec.Query.Where(mRec.Query.TableName == pTable && mRec.Query.Abbreviation == p.Text.Trim());
                if (!bExist)
                {
                    dxErrorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return false;
                }
                else
                {
                    dxErrorProvider1.SetError(p, string.Empty);
                    return true;
                }
            }
            return false;
        }

        private void SetReadOnly(ref ComboBoxEdit pAreaCode, ref TextEdit pAreaPlotAcres, ref TextEdit pAreaPlotRadius, ref TextEdit pAreaSide1,
            ref TextEdit pAreaSide2, ref TextEdit pAreaBlowup, int p)
        {
            if (string.IsNullOrEmpty(pAreaCode.Text.Trim()))
            {
                pAreaPlotAcres.Properties.ReadOnly = true;
                pAreaPlotRadius.Properties.ReadOnly = true;
                pAreaSide1.Properties.ReadOnly = true;
                pAreaSide2.Properties.ReadOnly = true;
                pAreaBlowup.Properties.ReadOnly = true;
                return;
            }

            switch (pAreaCode.Text[0])
            {
                case 'R':
                    pAreaPlotAcres.Properties.ReadOnly = true;
                    //pAreaPlotAcres.Enabled = false;
                    pAreaPlotRadius.Properties.ReadOnly = false;
                    //pAreaPlotRadius.Enabled = true;
                    //pAreaSide1.Enabled = false;
                    //pAreaSide2.Enabled = false;
                    pAreaSide1.Properties.ReadOnly = true;
                    pAreaSide2.Properties.ReadOnly = true;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaPlotRadius.Focus();
                    break;
                case 'A':
                    pAreaPlotAcres.Properties.ReadOnly = true;
                    //pAreaPlotAcres.Enabled = false;
                    pAreaPlotRadius.Properties.ReadOnly = true;
                    //pAreaPlotRadius.Enabled = false;
                    //pAreaSide1.Enabled = true;
                    //pAreaSide2.Enabled = true;
                    pAreaSide1.Properties.ReadOnly = false;
                    pAreaSide2.Properties.ReadOnly = false;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaSide1.Focus();
                    break;
                case 'F':
                    pAreaPlotAcres.Properties.ReadOnly = false;
                    //pAreaPlotAcres.Enabled = true;
                    pAreaPlotRadius.Properties.ReadOnly = true;
                    //pAreaPlotRadius.Enabled = false;
                    //pAreaSide1.Enabled = false;
                    //pAreaSide2.Enabled = false;
                    pAreaSide1.Properties.ReadOnly = true;
                    pAreaSide2.Properties.ReadOnly = true;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaPlotAcres.Focus();
                    break;
                default:
                    break;
            }
        }

        private bool CheckAreaCode(string p)
        {
            if (string.IsNullOrEmpty(p))
                return true;

            switch (p)
            {
                case "A1":
                case "A2":
                case "A3":
                case "A4":
                case "A5":
                case "F1":
                case "F2":
                case "F3":
                case "F4":
                case "F5":
                case "R1":
                case "R2":
                case "R3":
                case "R4":
                case "R5":
                    return true;
                default:
                    return false;
            }
        }

        private void StandAddForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (tblContext != null)
            //{
            //    tblContext.Dispose();
            //    tblContext = null;
            //}
            //if (projectLocationContext != null)
            //{
            //    projectLocationContext.Dispose();
            //    projectLocationContext = null;
            //}
        }

        private void tbBaf1_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit1.Text = Utils.CalculateLimitingDistanceFactor(tbBaf1.Text);
        }

        private void tbBaf2_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit2.Text = Utils.CalculateLimitingDistanceFactor(tbBaf2.Text);
        }

        private void tbBaf3_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit3.Text = Utils.CalculateLimitingDistanceFactor(tbBaf3.Text);
        }

        private void tbBaf4_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit4.Text = Utils.CalculateLimitingDistanceFactor(tbBaf4.Text);
        }

        private void tbBaf5_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit5.Text = Utils.CalculateLimitingDistanceFactor(tbBaf5.Text);
        }

        private void tbStripInterval1_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text, tbStripExpansion1.Text);
        }

        private void tbStripInterval2_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text, tbStripExpansion2.Text);
        }

        private void tbStripInterval3_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text, tbStripExpansion3.Text);
        }

        private void tbStripInterval4_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text, tbStripExpansion4.Text);
        }

        private void tbStripInterval5_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text, tbStripExpansion5.Text);
        }

        private void tbStripWidth1_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text, tbStripExpansion1.Text);
        }

        private void tbStripWidth2_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text, tbStripExpansion2.Text);
        }

        private void tbStripWidth3_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text, tbStripExpansion3.Text);
        }

        private void tbStripWidth4_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text, tbStripExpansion4.Text);
        }

        private void tbStripWidth5_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text, tbStripExpansion5.Text);
        }

        private void tbAreaCode1_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode1.Text))
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaPlotRadius1.Text = string.Empty;
                tbAreaSide11.Text = string.Empty;
                tbAreaSide12.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
        }

        private void tbAreaPlotAcres1_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotAcres1, "Invalid Area Plot Acres was entered."))
            {
                string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode1.Text, tbAreaPlotAcres1.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, tbAreaPlotAcres1.Text);
                if (!string.IsNullOrEmpty(plotRadius))
                    tbAreaPlotRadius1.Text = plotRadius;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup1.Text = blowup;
                tbAreaCode2.Focus();
            }
        }

        private void tbAreaPlotRadius1_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotRadius1, "Invalid Area Plot Radius was entered."))
            {
                string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaPlotRadius1.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                if (!string.IsNullOrEmpty(plotAcres))
                    tbAreaPlotAcres1.Text = plotAcres;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup1.Text = blowup;
                tbAreaCode2.Focus();
            }
        }

        private void tbAreaSide11_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide11, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide12, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaSide11.Text, tbAreaSide12.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres1.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup1.Text = blowup;
                    tbAreaSide12.Focus();
                }
            }
        }

        private void tbAreaSide12_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide11, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide12, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaSide11.Text, tbAreaSide12.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres1.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup1.Text = blowup;
                    tbAreaCode2.Focus();
                }
            }
        }

        private void tbAreaCode2_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode2.Text))
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaPlotRadius2.Text = string.Empty;
                tbAreaSide21.Text = string.Empty;
                tbAreaSide22.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
        }

        private void tbAreaPlotAcres2_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotAcres2, "Invalid Area Plot Acres was entered."))
            {
                string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode2.Text, tbAreaPlotAcres2.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, tbAreaPlotAcres2.Text);
                if (!string.IsNullOrEmpty(plotRadius))
                    tbAreaPlotRadius2.Text = plotRadius;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup2.Text = blowup;
                tbAreaCode3.Focus();
            }
        }

        private void tbAreaPlotRadius2_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotRadius2, "Invalid Area Plot Radius was entered."))
            {
                string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaPlotRadius2.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                if (!string.IsNullOrEmpty(plotAcres))
                    tbAreaPlotAcres2.Text = plotAcres;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup2.Text = blowup;
                tbAreaCode3.Focus();
            }
        }

        private void tbAreaSide21_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide21, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide22, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaSide21.Text, tbAreaSide22.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres2.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup2.Text = blowup;
                    tbAreaSide22.Focus();
                }
            }
        }

        private void tbAreaSide22_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide21, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide22, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaSide21.Text, tbAreaSide22.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres2.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup2.Text = blowup;
                    tbAreaCode3.Focus();
                }
            }
        }

        private void tbAreaCode3_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode3.Text))
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaPlotRadius3.Text = string.Empty;
                tbAreaSide31.Text = string.Empty;
                tbAreaSide32.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
        }

        private void tbAreaPlotAcres3_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotAcres3, "Invalid Area Plot Acres was entered."))
            {
                string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode3.Text, tbAreaPlotAcres3.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, tbAreaPlotAcres3.Text);
                if (!string.IsNullOrEmpty(plotRadius))
                    tbAreaPlotRadius3.Text = plotRadius;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup3.Text = blowup;
                tbAreaCode4.Focus();
            }
        }

        private void tbAreaPlotRadius3_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotRadius3, "Invalid Area Plot Radius was entered."))
            {
                string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaPlotRadius3.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                if (!string.IsNullOrEmpty(plotAcres))
                    tbAreaPlotAcres3.Text = plotAcres;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup3.Text = blowup;
                tbAreaCode4.Focus();
            }
        }

        private void tbAreaSide31_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide31, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide32, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaSide31.Text, tbAreaSide32.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres3.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup3.Text = blowup;
                    tbAreaSide32.Focus();
                }
            }
        }

        private void tbAreaSide32_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide31, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide32, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaSide31.Text, tbAreaSide32.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres3.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup3.Text = blowup;
                    tbAreaCode4.Focus();
                }
            }
        }

        private void tbAreaCode4_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode4.Text))
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaPlotRadius4.Text = string.Empty;
                tbAreaSide41.Text = string.Empty;
                tbAreaSide42.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
        }

        private void tbAreaPlotAcres4_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotAcres4, "Invalid Area Plot Acres was entered."))
            {
                string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode4.Text, tbAreaPlotAcres4.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, tbAreaPlotAcres4.Text);
                if (!string.IsNullOrEmpty(plotRadius))
                    tbAreaPlotRadius4.Text = plotRadius;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup4.Text = blowup;
                tbAreaCode5.Focus();
            }
        }

        private void tbAreaPlotRadius4_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotRadius4, "Invalid Area Plot Radius was entered."))
            {
                string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaPlotRadius4.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                if (!string.IsNullOrEmpty(plotAcres))
                    tbAreaPlotAcres4.Text = plotAcres;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup4.Text = blowup;
                tbAreaCode5.Focus();
            }
        }

        private void tbAreaSide41_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide41, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide42, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaSide41.Text, tbAreaSide42.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres4.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup4.Text = blowup;
                    tbAreaSide42.Focus();
                }
            }
        }

        private void tbAreaSide42_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide41, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide42, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaSide41.Text, tbAreaSide42.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres4.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup4.Text = blowup;
                    tbAreaCode5.Focus();
                }
            }
        }

        private void tbAreaCode5_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode5.Text))
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaPlotRadius5.Text = string.Empty;
                tbAreaSide51.Text = string.Empty;
                tbAreaSide52.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);
        }

        private void tbAreaPlotAcres5_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotAcres5, "Invalid Area Plot Acres was entered."))
            {
                string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode5.Text, tbAreaPlotAcres5.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, tbAreaPlotAcres5.Text);
                if (!string.IsNullOrEmpty(plotRadius))
                    tbAreaPlotRadius5.Text = plotRadius;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup5.Text = blowup;
                tbStripInterval1.Focus();
            }
        }

        private void tbAreaPlotRadius5_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaPlotRadius5, "Invalid Area Plot Radius was entered."))
            {
                string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaPlotRadius5.Text);
                string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                if (!string.IsNullOrEmpty(plotAcres))
                    tbAreaPlotAcres5.Text = plotAcres;
                if (!string.IsNullOrEmpty(blowup))
                    tbAreaBlowup5.Text = blowup;
                tbStripInterval1.Focus();
            }
        }

        private void tbAreaSide51_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide51, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide52, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaSide51.Text, tbAreaSide52.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres5.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup5.Text = blowup;
                    tbAreaSide52.Focus();
                }
            }
        }

        private void tbAreaSide52_Validated(object sender, EventArgs e)
        {
            if (!ValidateDouble(false, tbAreaSide51, "Invalid Area Side1 was entered."))
            {
                if (!ValidateDouble(false, tbAreaSide52, "Invalid Area Side2 was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaSide51.Text, tbAreaSide52.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres5.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup5.Text = blowup;
                    tbStripInterval1.Focus();
                }
            }
        }

        private void tbAreaCode1_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode1.Text))
            {
                e.Cancel = false;
                this.dxErrorProvider1.SetError(tbAreaCode1, string.Empty);
            }
            else
            {
                e.Cancel = true;
                this.dxErrorProvider1.SetError(tbAreaCode1, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode2_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode2.Text))
            {
                e.Cancel = false;
                this.dxErrorProvider1.SetError(tbAreaCode2, string.Empty);
            }
            else
            {
                e.Cancel = true;
                this.dxErrorProvider1.SetError(tbAreaCode2, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode3_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode3.Text))
            {
                e.Cancel = false;
                this.dxErrorProvider1.SetError(tbAreaCode3, string.Empty);
            }
            else
            {
                e.Cancel = true;
                this.dxErrorProvider1.SetError(tbAreaCode3, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode4_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode4.Text))
            {
                e.Cancel = false;
                this.dxErrorProvider1.SetError(tbAreaCode4, string.Empty);
            }
            else
            {
                e.Cancel = true;
                this.dxErrorProvider1.SetError(tbAreaCode4, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode5_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode5.Text))
            {
                e.Cancel = false;
                this.dxErrorProvider1.SetError(tbAreaCode5, string.Empty);
            }
            else
            {
                e.Cancel = true;
                this.dxErrorProvider1.SetError(tbAreaCode5, "Invalid Area Code was entered.");
            }
        }

        private void tbStripExpansion1_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion1.Text))
            {
                tbStripInterval1.Text = string.Empty;
                tbStripWidth1.Text = string.Empty;
            }
        }

        private void tbStripExpansion2_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion2.Text))
            {
                tbStripInterval2.Text = string.Empty;
                tbStripWidth2.Text = string.Empty;
            }
        }

        private void tbStripExpansion3_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion3.Text))
            {
                tbStripInterval3.Text = string.Empty;
                tbStripWidth3.Text = string.Empty;
            }
        }

        private void tbStripExpansion4_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion4.Text))
            {
                tbStripInterval4.Text = string.Empty;
                tbStripWidth4.Text = string.Empty;
            }
        }

        private void tbStripExpansion5_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion5.Text))
            {
                tbStripInterval5.Text = string.Empty;
                tbStripWidth5.Text = string.Empty;
            }
        }

        private void tbTract_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9-\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    if (rtb.Text.Length > 12)
                    {
                        e.Cancel = true;
                        dxErrorProvider1.SetError(rtb, "Tract Name Max Length is 12 chacters");
                    }
                    else
                    {
                        e.Cancel = false;
                        dxErrorProvider1.SetError(rtb, string.Empty);
                    }
                }
            }
        }

        private void tbSpecies_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;

            if (string.IsNullOrEmpty(rtb.Text))
            {
                e.Cancel = false;
                dxErrorProvider1.SetError(rtb, string.Empty);
                return;
            }

            bool exist = projectContext.Species.Any(s => s.TableName == currentProject.SpeciesTableName && s.Abbreviation == rtb.Text);

            if (exist)
            {
                e.Cancel = false;
                dxErrorProvider1.SetError(rtb, string.Empty);
            }
            else
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Species does not exist");
            }
        }

        private void cbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            countyBindingSource.Clear();
            countyBindingSource.DataSource = projectLocationContext.StateCounties.OrderBy(sc => sc.CountyAbbr).Where(sc => sc.StateAbbr == cbState.Text).ToList();
        }

        private void tbStand_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                }
            }
        }

        private void tbTownship_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 3)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbRange_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 3)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbBaf_Validated(object sender, EventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            ValidateDouble(false, rtb, "Invalid characters were entered.");
        }

        private void tbStrip_Validated(object sender, EventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            ValidateDouble(false, rtb, "Invalid characters were entered.");
        }

        private void tbAge_Validated(object sender, EventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            ValidateInt(false, rtb, "Invalid characters were entered.");
        }

        private void StandAddForm_Shown(object sender, EventArgs e)
        {
            cbState.Focus();
        }

        private void tbNetAcres_Validated(object sender, EventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            ValidateDouble(true, rtb, "Invalid characters were entered.");
        }

        private void cbState_TextChanged(object sender, EventArgs e)
        {
            countyBindingSource.Clear();
            countyBindingSource.DataSource = projectLocationContext.StateCounties.OrderBy(sc => sc.CountyAbbr).Where(sc => sc.StateAbbr == cbState.Text).ToList();
        }

        private void tbSection_Validating(object sender, CancelEventArgs e)
        {
            TextEdit rtb = sender as TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 2)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbAreaCode1_SelectedValueChanged(object sender, EventArgs e)
        {
            // A1
            if (!string.IsNullOrEmpty(tbAreaCode1.Text))
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaPlotRadius1.Text = string.Empty;
                tbAreaSide11.Text = string.Empty;
                tbAreaSide12.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
        }

        private void tbAreaCode2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode2.Text))
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaPlotRadius2.Text = string.Empty;
                tbAreaSide21.Text = string.Empty;
                tbAreaSide22.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
        }

        private void tbAreaCode3_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode3.Text))
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaPlotRadius3.Text = string.Empty;
                tbAreaSide31.Text = string.Empty;
                tbAreaSide32.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
        }

        private void tbAreaCode4_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode4.Text))
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaPlotRadius4.Text = string.Empty;
                tbAreaSide41.Text = string.Empty;
                tbAreaSide42.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
        }

        private void tbAreaCode5_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaCode5.Text))
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaPlotRadius5.Text = string.Empty;
                tbAreaSide51.Text = string.Empty;
                tbAreaSide52.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);
        }

        private void tbTract_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 12)
                tbStand.Focus();
        }

        private void tbStand_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 4)
                tbTownship.Focus();
        }

        private void tbTownship_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 3)
                tbRange.Focus();
        }

        private void tbRange_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 3)
                tbSection.Focus();
        }

        private void tbSection_Validated(object sender, EventArgs e)
        {

        }

        private void tbSection_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit)sender).Text.Length == 2)
                tbNetAcres.Focus();
        }

        private void gridView4_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Yarding System
            switch (gridView4.FocusedColumn.FieldName)
            {
                case "Description":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        string costTableName = string.Empty;
                        if (!string.IsNullOrEmpty(newStand.CostTableName))
                            costTableName = newStand.CostTableName;
                        else
                            costTableName = currentProject.CostTableName;
                        Cost costRec = projectContext.Costs.FirstOrDefault(c => c.TableName == costTableName && c.CostType == "YARDING" && c.CostDescription == e.Value.ToString());
                        if (costRec != null)
                        {
                            gridView4.SetRowCellValue(gridView4.FocusedRowHandle, gridView4.Columns["CostUnit"], costRec.Units);
                        }
                        else
                        {
                            XtraMessageBox.Show("Problem find Yarding Cost");
                            e.Valid = false;
                        }
                    }
                    break;
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView4.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteYardingSystemRecord();
                    }
                    break;
            }
        }

        private void AddYardingSystemRecord()
        {
            StandYardingSystem newRec = standYardingSystemBindingSource.Current as StandYardingSystem;
            if (newRec != null)
            {
                StandYardingSystem rec = new StandYardingSystem();
                rec.StandsId = newStand.StandsId;
                rec.Code = string.Empty;
                rec.Description = newRec.Description;
                rec.CostUnit = newRec.CostUnit;
                rec.Percent = newRec.Percent;
                projectContext.StandYardingSystems.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteYardingSystemRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandYardingSystem rec = standYardingSystemBindingSource.Current as StandYardingSystem;
            projectContext.StandYardingSystems.Remove(rec);
            projectContext.SaveChanges();
            LoadStandYardingSystemDataSource();
        }

        private void LoadStandYardingSystemDataSource()
        {
            if (standYardingSystemBindingSource.Count > 0)
                standYardingSystemBindingSource.Clear();
            standYardingSystemBindingSource.DataSource = projectContext.StandYardingSystems.Where(y => y.StandsId == newStand.StandsId).OrderBy(y => y.Description).ToList();
        }

        private void LoadStandAgeDataSource()
        {
            if (standAgeBindingSource.Count > 0)
                standAgeBindingSource.Clear();
            standAgeBindingSource.DataSource = projectContext.StandAges.Where(y => y.StandsId == newStand.StandsId).OrderBy(y => y.AgeCode).ToList();
        }

        private void LoadStandPermPlotDataSource()
        {
            if (standPermPlotBindingSource.Count > 0)
                standPermPlotBindingSource.Clear();
            standPermPlotBindingSource.DataSource = projectContext.StandPermPlots.Where(y => y.StandsId == newStand.StandsId).OrderBy(y => y.PermPlotOccasion).ToList();
        }

        private bool CheckStandYardingSystem()
        {
            float total = 0;

            if (standYardingSystemBindingSource.Count == 0)
                return true;

            for (int i = 0; i < gridView4.DataRowCount; i++)
            {
                total += Utils.ConvertToFloat(gridView4.GetRowCellValue(i, "Percent").ToString());
            }
            if (total != 100)
                return false;
            else
                return true;
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView2.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandAgeRecord();
                    }

                    break;
                //case "Append":
                //    e.Handled = true;
                //    AddStandAgeRecord();
                //    break;
            }
        }

        private void AddStandAgeRecord()
        {
            StandAge newRec = standAgeBindingSource.Current as StandAge;
            if (newRec != null)
            {
                StandAge rec = new StandAge();
                rec.StandsId = newStand.StandsId;
                rec.Age = newRec.Age;
                rec.AgeCode = newRec.AgeCode;
                projectContext.StandAges.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteStandAgeRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandAge rec = standAgeBindingSource.Current as StandAge;
            projectContext.StandAges.Remove(rec);
            projectContext.SaveChanges();
            LoadStandAgeDataSource();
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView3.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandPermPlotRecord();
                    }
                    break;
            }
        }

        private void AddStandPermPlotRecord()
        {
            StandPermPlot newRec = standPermPlotBindingSource.Current as StandPermPlot;
            if (newRec != null)
            {
                StandPermPlot rec = new StandPermPlot();
                rec.StandsId = newStand.StandsId;
                rec.PermPlotDate = newRec.PermPlotDate; // DateTime.Now;
                rec.PermPlotInterval = newRec.PermPlotInterval;
                rec.PermPlotOccasion = newRec.PermPlotOccasion; // (short)(gridView3.DataRowCount + 1);
                projectContext.StandPermPlots.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void DeleteStandPermPlotRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandPermPlot rec = standPermPlotBindingSource.Current as StandPermPlot;
            projectContext.StandPermPlots.Remove(rec);
            projectContext.SaveChanges();
            LoadStandPermPlotDataSource();
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //if (e.RowHandle == GridControl.NewItemRowHandle)
            //    AddStandHaulingRecord();
            //else
            //    projectContext.SaveChanges();
            if (e.RowHandle == GridControl.NewItemRowHandle)
                XtraMessageBox.Show("Trees must be entered before adding Stand Hauling information.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {

        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            switch (gridView1.FocusedColumn.FieldName)
            {
                case "AvgLoadSize":
                    float avgLoadSize = Utils.ConvertToFloat(e.Value.ToString());
                    Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, currentProject.SpeciesTableName, "DF");
                    float avgLoadCcf = (float)Math.Round(avgLoadSize * 2000 / (float)spcRec.Lbs, 1);
                    float avgLoadMbf = (float)Math.Round(avgLoadCcf / (float)2.155, 1);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadCcf"], avgLoadCcf);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"], avgLoadMbf);

                    float calcCostMbf0 = 0;
                    float costPerHour0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf0 = costPerHour0 * ((minutes0 / 60) + hours0 + loadUnloadHours0) / avgLoadMbf0;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf0);
                    break;
                case "CostPerHour":
                    float calcCostMbf1 = 0;
                    float costPerHour1 = Utils.ConvertToFloat(e.Value.ToString());
                    float minutes1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf1 = costPerHour1 * ((minutes1 / 60) + hours1 + loadUnloadHours1) / avgLoadMbf1;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf1);
                    break;
                case "RoundTripMiles":
                    //float calcCostMbf2 = 0;
                    //float costPerHour2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    //float minutes2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    //float hours2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    //float loadUnloadHours2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    //float avgLoadMbf2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    //calcCostMbf2 = costPerHour2 * ((minutes2 / 60) + hours2 + loadUnloadHours2) / avgLoadMbf2;
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf2);
                    break;
                case "Hours":
                    float calcCostMbf3 = 0;
                    float costPerHour3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours3 = Utils.ConvertToFloat(e.Value.ToString());
                    float loadUnloadHours3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf3 = costPerHour3 * ((minutes3 / 60) + hours3 + loadUnloadHours3) / avgLoadMbf3;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf3);
                    break;
                case "Minutes":
                    float calcCostMbf4 = 0;
                    float costPerHour4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes4 = Utils.ConvertToFloat(e.Value.ToString());
                    float hours4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf4 = costPerHour4 * ((minutes4 / 60) + hours4 + loadUnloadHours4) / avgLoadMbf4;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf4);
                    break;
                case "LoadUnloadHours":
                    float calcCostMbf5 = 0;
                    float costPerHour5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours5 = Utils.ConvertToFloat(e.Value.ToString());
                    float avgLoadMbf5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf5 = costPerHour5 * ((minutes5 / 60) + hours5 + loadUnloadHours5) / avgLoadMbf5;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf5);
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            // Hauling
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView1.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandHaulingRecord();
                    }
                    break;
                case "Append":
                    if (!projectContext.Trees.Any(s => s.StandsId == newStand.StandsId))
                    {
                        XtraMessageBox.Show("Trees must be entered before adding Stand Hauling information.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void DeleteStandHaulingRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandHauling rec = standHaulingBindingSource.Current as StandHauling;
            projectContext.StandHaulings.Remove(rec);
            projectContext.SaveChanges();
            LoadStandHaulingSource();
        }

        private void LoadStandHaulingSource()
        {
            if (standHaulingBindingSource.Count > 0)
                standHaulingBindingSource.Clear();
            List<StandHauling> hauling = projectContext.StandHaulings.OrderBy(h => h.Destination).Where(h => h.TableName == "GENERAL" && h.StandsId == newStand.StandsId).ToList();
            standHaulingBindingSource.DataSource = hauling;
        }

        private void AddStandHaulingRecord()
        {
            StandHauling newRec = standHaulingBindingSource.Current as StandHauling;
            if (newRec != null)
            {
                StandHauling rec = new StandHauling();
                rec.StandsId = newStand.StandsId;
                rec.AvgLoadCcf = newRec.AvgLoadCcf;
                rec.AvgLoadMbf = newRec.AvgLoadMbf;
                rec.AvgLoadSize = newRec.AvgLoadSize;
                rec.CalcCostDollarPerMbf = newRec.CalcCostDollarPerMbf;
                rec.CostPerHour = newRec.CostPerHour;
                rec.Destination = newRec.Destination;
                rec.Hours = newRec.Hours;
                rec.LoadUnloadHours = newRec.LoadUnloadHours;
                rec.Minutes = newRec.Minutes;
                rec.RoundTripMiles = newRec.RoundTripMiles;
                projectContext.StandHaulings.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void gridView2_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddStandAgeRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridView2_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            byte i = (byte)(gridView2.DataRowCount + 1);
            gridView2.SetRowCellValue(e.RowHandle, gridView2.Columns["AgeCode"], i);
            gridView2.FocusedColumn = gridView2.Columns["Age"];
        }

        private void gridView2_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            //XtraMessageBox.Show("BeforeLeaveRow");
        }

        private void gridControl2_Leave(object sender, EventArgs e)
        {
            if (!(gridView2.PostEditor() && gridView2.UpdateCurrentRow()))
            {
                standAgeBindingSource.EndEdit();
            }
        }

        private void gridView3_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddStandPermPlotRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridView4_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddYardingSystemRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridView4_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {

        }

        private void gridView4_ValidatingEditor_1(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Yarding System
            switch (gridView4.FocusedColumn.FieldName)
            {
                case "Description":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        string costTableName = string.Empty;
                        if (!string.IsNullOrEmpty(newStand.CostTableName))
                            costTableName = newStand.CostTableName;
                        else
                            costTableName = currentProject.CostTableName;
                        Cost costRec = projectContext.Costs.FirstOrDefault(c => c.TableName == costTableName && c.CostType == "YARDING" && c.CostDescription == e.Value.ToString());
                        if (costRec != null)
                        {
                            gridView4.SetRowCellValue(gridView4.FocusedRowHandle, gridView4.Columns["CostUnit"], costRec.Units);
                        }
                        else
                        {
                            XtraMessageBox.Show("Problem finding Yarding Cost");
                            e.Valid = false;
                        }
                    }
                    break;
            }
        }

        private void gridView3_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            byte i = (byte)(gridView3.DataRowCount + 1);
            gridView3.SetRowCellValue(e.RowHandle, gridView3.Columns["PermPlotOccasion"], i);
            gridView3.FocusedColumn = gridView3.Columns["PermPlotDate"];
        }

        private void gridView3_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            //switch (gridView3.FocusedColumn.FieldName)
            //{
            //    case "PermPlotDate":
            //        if (!string.IsNullOrEmpty(e.Value.ToString()))
            //        {
            //            int occasion = Utils.ConvertToInt(gridView3.GetRowCellValue(gridView3.FocusedRowHandle, gridView4.Columns["PermPlotOccasion"]).ToString());
            //            if (occasion == 1)
            //            {
            //                gridView3.SetRowCellValue(gridView3.FocusedRowHandle, gridView4.Columns["PermPlotInterval"], 0);
            //            }
            //            else
            //            {
            //                DateTime prevDate = Convert.ToDateTime(gridView3.GetRowCellValue(gridView3.FocusedRowHandle-1, gridView4.Columns["PermPlotDate"]));
            //                DateTime currDate = Convert.ToDateTime(e.Value);
            //                int diff = currDate.Year - prevDate.Year;
            //                gridView3.SetRowCellValue(gridView3.FocusedRowHandle, gridView4.Columns["PermPlotInterval"], diff);
            //            }
                        
            //        }
            //        break;
            //}
        }

        private void gridView3_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            int occasion = Utils.ConvertToInt(gridView3.GetRowCellValue(e.RowHandle, "PermPlotOccasion").ToString());
            if (occasion == 1)
            {
                gridView3.SetRowCellValue(e.RowHandle, "PermPlotInterval", 0);
            }
            else
            {
                StandPermPlot rec = projectContext.StandPermPlots.FirstOrDefault(s => s.StandsId == newStand.StandsId && s.PermPlotOccasion == occasion - 1);
                DateTime prevDate = rec.PermPlotDate; // Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle - 1, "PermPlotDate"));
                DateTime currDate = Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle, "PermPlotDate"));
                int diff = currDate.Year - prevDate.Year;
                gridView3.SetRowCellValue(e.RowHandle, "PermPlotInterval", diff);
            }
        }

        private void gridView4_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            switch (e.Column.FieldName)
            {
                case "Description":
                    gridView4.ShowEditor();
                    (gridView4.ActiveEditor as ComboBoxEdit).ShowPopup();
                    break;
            }
        }
    }
}