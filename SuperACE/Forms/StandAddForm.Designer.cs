﻿namespace SuperACE
{
    partial class StandAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.standHaulingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHaulingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDestination = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinutes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoundTripMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadUnloadHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcCostDollarPerMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.haulingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.tbSiteIndex4 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex3 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex2 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.tbSpecies11 = new DevExpress.XtraEditors.LookUpEdit();
            this.speciesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbSpecies12 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies13 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies21 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies22 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies23 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies31 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies32 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies33 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies41 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies42 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies43 = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.tbStripExpansion5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.tbStripWidth5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth1 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.tbAreaBlowup5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide52 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide42 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide32 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide22 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup1 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide12 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide51 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide41 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide31 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide21 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide11 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.tbAreaPlotAcres5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotAcres4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotAcres3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotAcres2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotAcres1 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaCode2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaCode3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaCode4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaCode5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.tbBafLimit5 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf5 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit4 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf4 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit3 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf3 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit2 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf2 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit1 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf1 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.tbAutoSegmentLength = new DevExpress.XtraEditors.TextEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.cbGrowthModel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.cbCostTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.cbPriceTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.cbGradeTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.cbSortTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.cbSpeciesTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbNonStockedTable = new DevExpress.XtraEditors.LookUpEdit();
            this.nonStockedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbNonTimberedTable = new DevExpress.XtraEditors.LookUpEdit();
            this.nonTimberedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbHarvestTable = new DevExpress.XtraEditors.LookUpEdit();
            this.harvestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btnGrow = new DevExpress.XtraEditors.SimpleButton();
            this.tbGrownToDate = new DevExpress.XtraEditors.TextEdit();
            this.tbInputDate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.tbNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.tbSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.tbRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.tbTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.tbStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.tbTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.tbProject = new DevExpress.XtraEditors.TextEdit();
            this.cbState = new DevExpress.XtraEditors.LookUpEdit();
            this.stateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbCounty = new DevExpress.XtraEditors.LookUpEdit();
            this.countyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.standYardingSystemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.standPermPlotBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.standAgeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBoxYarding = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditHarvest = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotOccasion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standHistoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHaulingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAutoSegmentLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrowthModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonStockedTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonStockedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonTimberedTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonTimberedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHarvestTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.harvestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGrownToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCounty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standYardingSystemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standPermPlotBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standAgeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxYarding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditHarvest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHistoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "New";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Save";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Cancel";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1148, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 608);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1148, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 584);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1148, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 584);
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.gridControl1);
            this.groupControl6.Location = new System.Drawing.Point(255, 416);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(518, 179);
            this.groupControl6.TabIndex = 22;
            this.groupControl6.Text = "Hauling Calculations";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.standHaulingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl1.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(2, 21);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(514, 156);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // standHaulingBindingSource
            // 
            this.standHaulingBindingSource.DataSource = typeof(Project.DAL.StandHauling);
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 45;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHaulingID,
            this.colDestination,
            this.colAvgLoadSize,
            this.colMinutes,
            this.colRoundTripMiles,
            this.colHours,
            this.colCostPerHour,
            this.colTableName,
            this.colAvgLoadCcf,
            this.colAvgLoadMbf,
            this.colLoadUnloadHours,
            this.colCalcCostDollarPerMbf});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowRowSizing = true;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colHaulingID
            // 
            this.colHaulingID.AppearanceHeader.Options.UseTextOptions = true;
            this.colHaulingID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHaulingID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHaulingID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHaulingID.FieldName = "HaulingID";
            this.colHaulingID.Name = "colHaulingID";
            // 
            // colDestination
            // 
            this.colDestination.AppearanceHeader.Options.UseTextOptions = true;
            this.colDestination.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDestination.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDestination.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDestination.FieldName = "Destination";
            this.colDestination.Name = "colDestination";
            this.colDestination.Visible = true;
            this.colDestination.VisibleIndex = 0;
            this.colDestination.Width = 91;
            // 
            // colAvgLoadSize
            // 
            this.colAvgLoadSize.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadSize.Caption = "Avg Load Tons";
            this.colAvgLoadSize.DisplayFormat.FormatString = "{0:0.0}";
            this.colAvgLoadSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadSize.FieldName = "AvgLoadSize";
            this.colAvgLoadSize.Name = "colAvgLoadSize";
            this.colAvgLoadSize.Visible = true;
            this.colAvgLoadSize.VisibleIndex = 1;
            this.colAvgLoadSize.Width = 35;
            // 
            // colMinutes
            // 
            this.colMinutes.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinutes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinutes.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinutes.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinutes.Caption = "Min";
            this.colMinutes.DisplayFormat.FormatString = "{0:0}";
            this.colMinutes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinutes.FieldName = "Minutes";
            this.colMinutes.Name = "colMinutes";
            this.colMinutes.Visible = true;
            this.colMinutes.VisibleIndex = 6;
            this.colMinutes.Width = 35;
            // 
            // colRoundTripMiles
            // 
            this.colRoundTripMiles.AppearanceHeader.Options.UseTextOptions = true;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRoundTripMiles.Caption = "Round Trip Miles";
            this.colRoundTripMiles.DisplayFormat.FormatString = "{0:0}";
            this.colRoundTripMiles.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRoundTripMiles.FieldName = "RoundTripMiles";
            this.colRoundTripMiles.Name = "colRoundTripMiles";
            this.colRoundTripMiles.Visible = true;
            this.colRoundTripMiles.VisibleIndex = 4;
            this.colRoundTripMiles.Width = 45;
            // 
            // colHours
            // 
            this.colHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHours.Caption = "Hours";
            this.colHours.DisplayFormat.FormatString = "{0:0}";
            this.colHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 5;
            this.colHours.Width = 45;
            // 
            // colCostPerHour
            // 
            this.colCostPerHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostPerHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostPerHour.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostPerHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostPerHour.Caption = "Truck Cost per Hour $";
            this.colCostPerHour.DisplayFormat.FormatString = "{0:0.00}";
            this.colCostPerHour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCostPerHour.FieldName = "CostPerHour";
            this.colCostPerHour.Name = "colCostPerHour";
            this.colCostPerHour.Visible = true;
            this.colCostPerHour.VisibleIndex = 8;
            this.colCostPerHour.Width = 50;
            // 
            // colTableName
            // 
            this.colTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colAvgLoadCcf
            // 
            this.colAvgLoadCcf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadCcf.Caption = "Avg Load Ccf";
            this.colAvgLoadCcf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadCcf.FieldName = "AvgLoadCcf";
            this.colAvgLoadCcf.Name = "colAvgLoadCcf";
            this.colAvgLoadCcf.Visible = true;
            this.colAvgLoadCcf.VisibleIndex = 2;
            this.colAvgLoadCcf.Width = 35;
            // 
            // colAvgLoadMbf
            // 
            this.colAvgLoadMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadMbf.Caption = "Avg Load Mbf";
            this.colAvgLoadMbf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadMbf.FieldName = "AvgLoadMbf";
            this.colAvgLoadMbf.Name = "colAvgLoadMbf";
            this.colAvgLoadMbf.Visible = true;
            this.colAvgLoadMbf.VisibleIndex = 3;
            this.colAvgLoadMbf.Width = 35;
            // 
            // colLoadUnloadHours
            // 
            this.colLoadUnloadHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoadUnloadHours.Caption = "Load /Unload Hours";
            this.colLoadUnloadHours.DisplayFormat.FormatString = "n0";
            this.colLoadUnloadHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLoadUnloadHours.FieldName = "LoadUnloadHours";
            this.colLoadUnloadHours.Name = "colLoadUnloadHours";
            this.colLoadUnloadHours.Visible = true;
            this.colLoadUnloadHours.VisibleIndex = 7;
            this.colLoadUnloadHours.Width = 50;
            // 
            // colCalcCostDollarPerMbf
            // 
            this.colCalcCostDollarPerMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcCostDollarPerMbf.Caption = "Calc Cost $/Mbf";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatString = "n0";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcCostDollarPerMbf.FieldName = "CalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.Name = "colCalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.Visible = true;
            this.colCalcCostDollarPerMbf.VisibleIndex = 9;
            this.colCalcCostDollarPerMbf.Width = 38;
            // 
            // haulingBindingSource
            // 
            this.haulingBindingSource.DataSource = typeof(Project.DAL.Hauling);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.labelControl110);
            this.groupControl5.Controls.Add(this.tbSiteIndex4);
            this.groupControl5.Controls.Add(this.tbSiteIndex3);
            this.groupControl5.Controls.Add(this.tbSiteIndex2);
            this.groupControl5.Controls.Add(this.tbSiteIndex1);
            this.groupControl5.Controls.Add(this.labelControl102);
            this.groupControl5.Controls.Add(this.labelControl103);
            this.groupControl5.Controls.Add(this.labelControl104);
            this.groupControl5.Controls.Add(this.tbSpecies11);
            this.groupControl5.Controls.Add(this.tbSpecies12);
            this.groupControl5.Controls.Add(this.tbSpecies13);
            this.groupControl5.Controls.Add(this.tbSpecies21);
            this.groupControl5.Controls.Add(this.tbSpecies22);
            this.groupControl5.Controls.Add(this.tbSpecies23);
            this.groupControl5.Controls.Add(this.tbSpecies31);
            this.groupControl5.Controls.Add(this.tbSpecies32);
            this.groupControl5.Controls.Add(this.tbSpecies33);
            this.groupControl5.Controls.Add(this.tbSpecies41);
            this.groupControl5.Controls.Add(this.tbSpecies42);
            this.groupControl5.Controls.Add(this.tbSpecies43);
            this.groupControl5.Location = new System.Drawing.Point(0, 415);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(249, 180);
            this.groupControl5.TabIndex = 21;
            this.groupControl5.Text = "Site Index by Species";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Options.UseTextOptions = true;
            this.labelControl110.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl110.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl110.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl110.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl110.Location = new System.Drawing.Point(195, 29);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(28, 26);
            this.labelControl110.TabIndex = 62;
            this.labelControl110.Text = "Site Index";
            // 
            // tbSiteIndex4
            // 
            this.tbSiteIndex4.EnterMoveNextControl = true;
            this.tbSiteIndex4.Location = new System.Drawing.Point(187, 145);
            this.tbSiteIndex4.MenuManager = this.barManager1;
            this.tbSiteIndex4.Name = "tbSiteIndex4";
            this.tbSiteIndex4.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex4.Properties.Mask.EditMask = "n0";
            this.tbSiteIndex4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbSiteIndex4.Size = new System.Drawing.Size(50, 20);
            this.tbSiteIndex4.TabIndex = 15;
            this.tbSiteIndex4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex3
            // 
            this.tbSiteIndex3.EnterMoveNextControl = true;
            this.tbSiteIndex3.Location = new System.Drawing.Point(187, 119);
            this.tbSiteIndex3.MenuManager = this.barManager1;
            this.tbSiteIndex3.Name = "tbSiteIndex3";
            this.tbSiteIndex3.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex3.Properties.Mask.EditMask = "n0";
            this.tbSiteIndex3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbSiteIndex3.Size = new System.Drawing.Size(50, 20);
            this.tbSiteIndex3.TabIndex = 11;
            this.tbSiteIndex3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex2
            // 
            this.tbSiteIndex2.EnterMoveNextControl = true;
            this.tbSiteIndex2.Location = new System.Drawing.Point(187, 93);
            this.tbSiteIndex2.MenuManager = this.barManager1;
            this.tbSiteIndex2.Name = "tbSiteIndex2";
            this.tbSiteIndex2.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex2.Properties.Mask.EditMask = "n0";
            this.tbSiteIndex2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbSiteIndex2.Size = new System.Drawing.Size(50, 20);
            this.tbSiteIndex2.TabIndex = 7;
            this.tbSiteIndex2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex1
            // 
            this.tbSiteIndex1.EnterMoveNextControl = true;
            this.tbSiteIndex1.Location = new System.Drawing.Point(187, 67);
            this.tbSiteIndex1.MenuManager = this.barManager1;
            this.tbSiteIndex1.Name = "tbSiteIndex1";
            this.tbSiteIndex1.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex1.Properties.Mask.EditMask = "n0";
            this.tbSiteIndex1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbSiteIndex1.Size = new System.Drawing.Size(50, 20);
            this.tbSiteIndex1.TabIndex = 3;
            this.tbSiteIndex1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl102
            // 
            this.labelControl102.Appearance.Options.UseTextOptions = true;
            this.labelControl102.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl102.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl102.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl102.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl102.Location = new System.Drawing.Point(135, 42);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(40, 13);
            this.labelControl102.TabIndex = 56;
            this.labelControl102.Text = "Species";
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Options.UseTextOptions = true;
            this.labelControl103.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl103.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl103.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl103.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl103.Location = new System.Drawing.Point(67, 42);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(58, 13);
            this.labelControl103.TabIndex = 25;
            this.labelControl103.Text = "Species";
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Options.UseTextOptions = true;
            this.labelControl104.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl104.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl104.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl104.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl104.Location = new System.Drawing.Point(5, 42);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(56, 13);
            this.labelControl104.TabIndex = 24;
            this.labelControl104.Text = "Species";
            // 
            // tbSpecies11
            // 
            this.tbSpecies11.EnterMoveNextControl = true;
            this.tbSpecies11.Location = new System.Drawing.Point(10, 67);
            this.tbSpecies11.MenuManager = this.barManager1;
            this.tbSpecies11.Name = "tbSpecies11";
            this.tbSpecies11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies11.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies11.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies11.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies11.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies11.Properties.NullText = "";
            this.tbSpecies11.Properties.ValueMember = "Abbreviation";
            this.tbSpecies11.Size = new System.Drawing.Size(48, 20);
            this.tbSpecies11.TabIndex = 0;
            this.tbSpecies11.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // speciesBindingSource
            // 
            this.speciesBindingSource.DataSource = typeof(Project.DAL.Species);
            // 
            // tbSpecies12
            // 
            this.tbSpecies12.EnterMoveNextControl = true;
            this.tbSpecies12.Location = new System.Drawing.Point(69, 67);
            this.tbSpecies12.MenuManager = this.barManager1;
            this.tbSpecies12.Name = "tbSpecies12";
            this.tbSpecies12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies12.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies12.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies12.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies12.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies12.Properties.NullText = "";
            this.tbSpecies12.Properties.ValueMember = "Abbreviation";
            this.tbSpecies12.Size = new System.Drawing.Size(51, 20);
            this.tbSpecies12.TabIndex = 1;
            this.tbSpecies12.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies13
            // 
            this.tbSpecies13.EnterMoveNextControl = true;
            this.tbSpecies13.Location = new System.Drawing.Point(131, 67);
            this.tbSpecies13.MenuManager = this.barManager1;
            this.tbSpecies13.Name = "tbSpecies13";
            this.tbSpecies13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies13.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies13.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies13.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies13.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies13.Properties.NullText = "";
            this.tbSpecies13.Properties.ValueMember = "Abbreviation";
            this.tbSpecies13.Size = new System.Drawing.Size(50, 20);
            this.tbSpecies13.TabIndex = 2;
            this.tbSpecies13.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies21
            // 
            this.tbSpecies21.EnterMoveNextControl = true;
            this.tbSpecies21.Location = new System.Drawing.Point(10, 93);
            this.tbSpecies21.MenuManager = this.barManager1;
            this.tbSpecies21.Name = "tbSpecies21";
            this.tbSpecies21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies21.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies21.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies21.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies21.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies21.Properties.NullText = "";
            this.tbSpecies21.Properties.ValueMember = "Abbreviation";
            this.tbSpecies21.Size = new System.Drawing.Size(48, 20);
            this.tbSpecies21.TabIndex = 4;
            this.tbSpecies21.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies22
            // 
            this.tbSpecies22.EnterMoveNextControl = true;
            this.tbSpecies22.Location = new System.Drawing.Point(69, 93);
            this.tbSpecies22.MenuManager = this.barManager1;
            this.tbSpecies22.Name = "tbSpecies22";
            this.tbSpecies22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies22.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies22.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies22.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies22.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies22.Properties.NullText = "";
            this.tbSpecies22.Properties.ValueMember = "Abbreviation";
            this.tbSpecies22.Size = new System.Drawing.Size(51, 20);
            this.tbSpecies22.TabIndex = 5;
            this.tbSpecies22.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies23
            // 
            this.tbSpecies23.EnterMoveNextControl = true;
            this.tbSpecies23.Location = new System.Drawing.Point(131, 93);
            this.tbSpecies23.MenuManager = this.barManager1;
            this.tbSpecies23.Name = "tbSpecies23";
            this.tbSpecies23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies23.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies23.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies23.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies23.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies23.Properties.NullText = "";
            this.tbSpecies23.Properties.ValueMember = "Abbreviation";
            this.tbSpecies23.Size = new System.Drawing.Size(50, 20);
            this.tbSpecies23.TabIndex = 6;
            this.tbSpecies23.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies31
            // 
            this.tbSpecies31.EnterMoveNextControl = true;
            this.tbSpecies31.Location = new System.Drawing.Point(10, 119);
            this.tbSpecies31.MenuManager = this.barManager1;
            this.tbSpecies31.Name = "tbSpecies31";
            this.tbSpecies31.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies31.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies31.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies31.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies31.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies31.Properties.NullText = "";
            this.tbSpecies31.Properties.ValueMember = "Abbreviation";
            this.tbSpecies31.Size = new System.Drawing.Size(48, 20);
            this.tbSpecies31.TabIndex = 8;
            this.tbSpecies31.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies32
            // 
            this.tbSpecies32.EnterMoveNextControl = true;
            this.tbSpecies32.Location = new System.Drawing.Point(69, 119);
            this.tbSpecies32.MenuManager = this.barManager1;
            this.tbSpecies32.Name = "tbSpecies32";
            this.tbSpecies32.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies32.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies32.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies32.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies32.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies32.Properties.NullText = "";
            this.tbSpecies32.Properties.ValueMember = "Abbreviation";
            this.tbSpecies32.Size = new System.Drawing.Size(51, 20);
            this.tbSpecies32.TabIndex = 9;
            this.tbSpecies32.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies33
            // 
            this.tbSpecies33.EnterMoveNextControl = true;
            this.tbSpecies33.Location = new System.Drawing.Point(131, 119);
            this.tbSpecies33.MenuManager = this.barManager1;
            this.tbSpecies33.Name = "tbSpecies33";
            this.tbSpecies33.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies33.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies33.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies33.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies33.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies33.Properties.NullText = "";
            this.tbSpecies33.Properties.ValueMember = "Abbreviation";
            this.tbSpecies33.Size = new System.Drawing.Size(50, 20);
            this.tbSpecies33.TabIndex = 10;
            this.tbSpecies33.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies41
            // 
            this.tbSpecies41.EnterMoveNextControl = true;
            this.tbSpecies41.Location = new System.Drawing.Point(10, 145);
            this.tbSpecies41.MenuManager = this.barManager1;
            this.tbSpecies41.Name = "tbSpecies41";
            this.tbSpecies41.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies41.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies41.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies41.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies41.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies41.Properties.NullText = "";
            this.tbSpecies41.Properties.ValueMember = "Abbreviation";
            this.tbSpecies41.Size = new System.Drawing.Size(48, 20);
            this.tbSpecies41.TabIndex = 12;
            this.tbSpecies41.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies42
            // 
            this.tbSpecies42.EnterMoveNextControl = true;
            this.tbSpecies42.Location = new System.Drawing.Point(69, 145);
            this.tbSpecies42.MenuManager = this.barManager1;
            this.tbSpecies42.Name = "tbSpecies42";
            this.tbSpecies42.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies42.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies42.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies42.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies42.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies42.Properties.NullText = "";
            this.tbSpecies42.Properties.ValueMember = "Abbreviation";
            this.tbSpecies42.Size = new System.Drawing.Size(51, 20);
            this.tbSpecies42.TabIndex = 13;
            this.tbSpecies42.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies43
            // 
            this.tbSpecies43.EnterMoveNextControl = true;
            this.tbSpecies43.Location = new System.Drawing.Point(131, 145);
            this.tbSpecies43.MenuManager = this.barManager1;
            this.tbSpecies43.Name = "tbSpecies43";
            this.tbSpecies43.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies43.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies43.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies43.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies43.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies43.Properties.NullText = "";
            this.tbSpecies43.Properties.ValueMember = "Abbreviation";
            this.tbSpecies43.Size = new System.Drawing.Size(50, 20);
            this.tbSpecies43.TabIndex = 14;
            this.tbSpecies43.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl101);
            this.groupControl3.Controls.Add(this.tbStripExpansion5);
            this.groupControl3.Controls.Add(this.tbStripExpansion4);
            this.groupControl3.Controls.Add(this.tbStripExpansion3);
            this.groupControl3.Controls.Add(this.tbStripExpansion2);
            this.groupControl3.Controls.Add(this.tbStripExpansion1);
            this.groupControl3.Controls.Add(this.labelControl84);
            this.groupControl3.Controls.Add(this.labelControl85);
            this.groupControl3.Controls.Add(this.labelControl86);
            this.groupControl3.Controls.Add(this.labelControl87);
            this.groupControl3.Controls.Add(this.labelControl88);
            this.groupControl3.Controls.Add(this.labelControl89);
            this.groupControl3.Controls.Add(this.labelControl90);
            this.groupControl3.Controls.Add(this.tbStripWidth5);
            this.groupControl3.Controls.Add(this.tbStripInterval5);
            this.groupControl3.Controls.Add(this.tbStripWidth4);
            this.groupControl3.Controls.Add(this.tbStripInterval4);
            this.groupControl3.Controls.Add(this.tbStripWidth3);
            this.groupControl3.Controls.Add(this.tbStripInterval3);
            this.groupControl3.Controls.Add(this.tbStripWidth2);
            this.groupControl3.Controls.Add(this.tbStripInterval2);
            this.groupControl3.Controls.Add(this.tbStripWidth1);
            this.groupControl3.Controls.Add(this.tbStripInterval1);
            this.groupControl3.Location = new System.Drawing.Point(559, 205);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(214, 205);
            this.groupControl3.TabIndex = 19;
            this.groupControl3.Text = "Strip Cruise";
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Options.UseTextOptions = true;
            this.labelControl101.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl101.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl101.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl101.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl101.Location = new System.Drawing.Point(161, 35);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(28, 26);
            this.labelControl101.TabIndex = 56;
            this.labelControl101.Text = "Blow Up";
            // 
            // tbStripExpansion5
            // 
            this.tbStripExpansion5.EnterMoveNextControl = true;
            this.tbStripExpansion5.Location = new System.Drawing.Point(151, 171);
            this.tbStripExpansion5.MenuManager = this.barManager1;
            this.tbStripExpansion5.Name = "tbStripExpansion5";
            this.tbStripExpansion5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion5.Size = new System.Drawing.Size(50, 20);
            this.tbStripExpansion5.TabIndex = 14;
            this.tbStripExpansion5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion5.Validated += new System.EventHandler(this.tbStripExpansion5_Validated);
            // 
            // tbStripExpansion4
            // 
            this.tbStripExpansion4.EnterMoveNextControl = true;
            this.tbStripExpansion4.Location = new System.Drawing.Point(151, 145);
            this.tbStripExpansion4.MenuManager = this.barManager1;
            this.tbStripExpansion4.Name = "tbStripExpansion4";
            this.tbStripExpansion4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion4.Size = new System.Drawing.Size(50, 20);
            this.tbStripExpansion4.TabIndex = 11;
            this.tbStripExpansion4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion4.Validated += new System.EventHandler(this.tbStripExpansion4_Validated);
            // 
            // tbStripExpansion3
            // 
            this.tbStripExpansion3.EnterMoveNextControl = true;
            this.tbStripExpansion3.Location = new System.Drawing.Point(151, 119);
            this.tbStripExpansion3.MenuManager = this.barManager1;
            this.tbStripExpansion3.Name = "tbStripExpansion3";
            this.tbStripExpansion3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion3.Size = new System.Drawing.Size(50, 20);
            this.tbStripExpansion3.TabIndex = 8;
            this.tbStripExpansion3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion3.Validated += new System.EventHandler(this.tbStripExpansion3_Validated);
            // 
            // tbStripExpansion2
            // 
            this.tbStripExpansion2.EnterMoveNextControl = true;
            this.tbStripExpansion2.Location = new System.Drawing.Point(151, 93);
            this.tbStripExpansion2.MenuManager = this.barManager1;
            this.tbStripExpansion2.Name = "tbStripExpansion2";
            this.tbStripExpansion2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion2.Size = new System.Drawing.Size(50, 20);
            this.tbStripExpansion2.TabIndex = 5;
            this.tbStripExpansion2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion2.Validated += new System.EventHandler(this.tbStripExpansion2_Validated);
            // 
            // tbStripExpansion1
            // 
            this.tbStripExpansion1.EnterMoveNextControl = true;
            this.tbStripExpansion1.Location = new System.Drawing.Point(151, 67);
            this.tbStripExpansion1.MenuManager = this.barManager1;
            this.tbStripExpansion1.Name = "tbStripExpansion1";
            this.tbStripExpansion1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion1.Size = new System.Drawing.Size(50, 20);
            this.tbStripExpansion1.TabIndex = 2;
            this.tbStripExpansion1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion1.Validated += new System.EventHandler(this.tbStripExpansion1_Validated);
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Options.UseTextOptions = true;
            this.labelControl84.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl84.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl84.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl84.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl84.Location = new System.Drawing.Point(87, 35);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(58, 26);
            this.labelControl84.TabIndex = 25;
            this.labelControl84.Text = "Strip Width in Ft";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Options.UseTextOptions = true;
            this.labelControl85.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl85.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl85.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl85.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl85.Location = new System.Drawing.Point(26, 24);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(56, 39);
            this.labelControl85.TabIndex = 24;
            this.labelControl85.Text = "Strip Intverval in Ft";
            // 
            // labelControl86
            // 
            this.labelControl86.Location = new System.Drawing.Point(5, 174);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(16, 13);
            this.labelControl86.TabIndex = 23;
            this.labelControl86.Text = "S5:";
            // 
            // labelControl87
            // 
            this.labelControl87.Location = new System.Drawing.Point(5, 148);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(16, 13);
            this.labelControl87.TabIndex = 22;
            this.labelControl87.Text = "S4:";
            // 
            // labelControl88
            // 
            this.labelControl88.Location = new System.Drawing.Point(5, 122);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(16, 13);
            this.labelControl88.TabIndex = 21;
            this.labelControl88.Text = "S3:";
            // 
            // labelControl89
            // 
            this.labelControl89.Location = new System.Drawing.Point(5, 96);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(16, 13);
            this.labelControl89.TabIndex = 20;
            this.labelControl89.Text = "S2:";
            // 
            // labelControl90
            // 
            this.labelControl90.Location = new System.Drawing.Point(5, 70);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(16, 13);
            this.labelControl90.TabIndex = 19;
            this.labelControl90.Text = "S1:";
            // 
            // tbStripWidth5
            // 
            this.tbStripWidth5.EnterMoveNextControl = true;
            this.tbStripWidth5.Location = new System.Drawing.Point(89, 171);
            this.tbStripWidth5.MenuManager = this.barManager1;
            this.tbStripWidth5.Name = "tbStripWidth5";
            this.tbStripWidth5.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth5.Size = new System.Drawing.Size(51, 20);
            this.tbStripWidth5.TabIndex = 13;
            this.tbStripWidth5.TextChanged += new System.EventHandler(this.tbStripWidth5_TextChanged);
            this.tbStripWidth5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripInterval5
            // 
            this.tbStripInterval5.EnterMoveNextControl = true;
            this.tbStripInterval5.Location = new System.Drawing.Point(30, 171);
            this.tbStripInterval5.MenuManager = this.barManager1;
            this.tbStripInterval5.Name = "tbStripInterval5";
            this.tbStripInterval5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval5.Size = new System.Drawing.Size(48, 20);
            this.tbStripInterval5.TabIndex = 12;
            this.tbStripInterval5.TextChanged += new System.EventHandler(this.tbStripInterval5_TextChanged);
            this.tbStripInterval5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval5.Validated += new System.EventHandler(this.tbStrip_Validated);
            // 
            // tbStripWidth4
            // 
            this.tbStripWidth4.EnterMoveNextControl = true;
            this.tbStripWidth4.Location = new System.Drawing.Point(89, 145);
            this.tbStripWidth4.MenuManager = this.barManager1;
            this.tbStripWidth4.Name = "tbStripWidth4";
            this.tbStripWidth4.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth4.Size = new System.Drawing.Size(51, 20);
            this.tbStripWidth4.TabIndex = 10;
            this.tbStripWidth4.TextChanged += new System.EventHandler(this.tbStripWidth4_TextChanged);
            this.tbStripWidth4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripInterval4
            // 
            this.tbStripInterval4.EnterMoveNextControl = true;
            this.tbStripInterval4.Location = new System.Drawing.Point(30, 145);
            this.tbStripInterval4.MenuManager = this.barManager1;
            this.tbStripInterval4.Name = "tbStripInterval4";
            this.tbStripInterval4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval4.Size = new System.Drawing.Size(48, 20);
            this.tbStripInterval4.TabIndex = 9;
            this.tbStripInterval4.TextChanged += new System.EventHandler(this.tbStripInterval4_TextChanged);
            this.tbStripInterval4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval4.Validated += new System.EventHandler(this.tbStrip_Validated);
            // 
            // tbStripWidth3
            // 
            this.tbStripWidth3.EnterMoveNextControl = true;
            this.tbStripWidth3.Location = new System.Drawing.Point(89, 119);
            this.tbStripWidth3.MenuManager = this.barManager1;
            this.tbStripWidth3.Name = "tbStripWidth3";
            this.tbStripWidth3.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth3.Size = new System.Drawing.Size(51, 20);
            this.tbStripWidth3.TabIndex = 7;
            this.tbStripWidth3.TextChanged += new System.EventHandler(this.tbStripWidth3_TextChanged);
            this.tbStripWidth3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripInterval3
            // 
            this.tbStripInterval3.EnterMoveNextControl = true;
            this.tbStripInterval3.Location = new System.Drawing.Point(30, 119);
            this.tbStripInterval3.MenuManager = this.barManager1;
            this.tbStripInterval3.Name = "tbStripInterval3";
            this.tbStripInterval3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval3.Size = new System.Drawing.Size(48, 20);
            this.tbStripInterval3.TabIndex = 6;
            this.tbStripInterval3.TextChanged += new System.EventHandler(this.tbStripInterval3_TextChanged);
            this.tbStripInterval3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval3.Validated += new System.EventHandler(this.tbStrip_Validated);
            // 
            // tbStripWidth2
            // 
            this.tbStripWidth2.EnterMoveNextControl = true;
            this.tbStripWidth2.Location = new System.Drawing.Point(89, 93);
            this.tbStripWidth2.MenuManager = this.barManager1;
            this.tbStripWidth2.Name = "tbStripWidth2";
            this.tbStripWidth2.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth2.Size = new System.Drawing.Size(51, 20);
            this.tbStripWidth2.TabIndex = 4;
            this.tbStripWidth2.TextChanged += new System.EventHandler(this.tbStripWidth2_TextChanged);
            this.tbStripWidth2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripInterval2
            // 
            this.tbStripInterval2.EnterMoveNextControl = true;
            this.tbStripInterval2.Location = new System.Drawing.Point(30, 93);
            this.tbStripInterval2.MenuManager = this.barManager1;
            this.tbStripInterval2.Name = "tbStripInterval2";
            this.tbStripInterval2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval2.Size = new System.Drawing.Size(48, 20);
            this.tbStripInterval2.TabIndex = 3;
            this.tbStripInterval2.TextChanged += new System.EventHandler(this.tbStripInterval2_TextChanged);
            this.tbStripInterval2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval2.Validated += new System.EventHandler(this.tbStrip_Validated);
            // 
            // tbStripWidth1
            // 
            this.tbStripWidth1.EnterMoveNextControl = true;
            this.tbStripWidth1.Location = new System.Drawing.Point(89, 67);
            this.tbStripWidth1.MenuManager = this.barManager1;
            this.tbStripWidth1.Name = "tbStripWidth1";
            this.tbStripWidth1.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth1.Size = new System.Drawing.Size(51, 20);
            this.tbStripWidth1.TabIndex = 1;
            this.tbStripWidth1.TextChanged += new System.EventHandler(this.tbStripWidth1_TextChanged);
            this.tbStripWidth1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripInterval1
            // 
            this.tbStripInterval1.EnterMoveNextControl = true;
            this.tbStripInterval1.Location = new System.Drawing.Point(30, 67);
            this.tbStripInterval1.MenuManager = this.barManager1;
            this.tbStripInterval1.Name = "tbStripInterval1";
            this.tbStripInterval1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval1.Size = new System.Drawing.Size(48, 20);
            this.tbStripInterval1.TabIndex = 0;
            this.tbStripInterval1.TextChanged += new System.EventHandler(this.tbStripInterval1_TextChanged);
            this.tbStripInterval1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval1.Validated += new System.EventHandler(this.tbStrip_Validated);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl83);
            this.groupControl2.Controls.Add(this.labelControl82);
            this.groupControl2.Controls.Add(this.labelControl81);
            this.groupControl2.Controls.Add(this.labelControl80);
            this.groupControl2.Controls.Add(this.labelControl74);
            this.groupControl2.Controls.Add(this.tbAreaBlowup5);
            this.groupControl2.Controls.Add(this.tbAreaSide52);
            this.groupControl2.Controls.Add(this.tbAreaBlowup4);
            this.groupControl2.Controls.Add(this.tbAreaSide42);
            this.groupControl2.Controls.Add(this.tbAreaBlowup3);
            this.groupControl2.Controls.Add(this.tbAreaSide32);
            this.groupControl2.Controls.Add(this.tbAreaBlowup2);
            this.groupControl2.Controls.Add(this.tbAreaSide22);
            this.groupControl2.Controls.Add(this.tbAreaBlowup1);
            this.groupControl2.Controls.Add(this.tbAreaSide12);
            this.groupControl2.Controls.Add(this.tbAreaSide51);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius5);
            this.groupControl2.Controls.Add(this.tbAreaSide41);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius4);
            this.groupControl2.Controls.Add(this.tbAreaSide31);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius3);
            this.groupControl2.Controls.Add(this.tbAreaSide21);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius2);
            this.groupControl2.Controls.Add(this.tbAreaSide11);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius1);
            this.groupControl2.Controls.Add(this.labelControl73);
            this.groupControl2.Controls.Add(this.labelControl75);
            this.groupControl2.Controls.Add(this.labelControl76);
            this.groupControl2.Controls.Add(this.labelControl77);
            this.groupControl2.Controls.Add(this.labelControl78);
            this.groupControl2.Controls.Add(this.labelControl79);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres5);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres4);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres3);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres2);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres1);
            this.groupControl2.Controls.Add(this.tbAreaCode1);
            this.groupControl2.Controls.Add(this.tbAreaCode2);
            this.groupControl2.Controls.Add(this.tbAreaCode3);
            this.groupControl2.Controls.Add(this.tbAreaCode4);
            this.groupControl2.Controls.Add(this.tbAreaCode5);
            this.groupControl2.Location = new System.Drawing.Point(185, 205);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(368, 205);
            this.groupControl2.TabIndex = 18;
            this.groupControl2.Text = "Area";
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Options.UseTextOptions = true;
            this.labelControl83.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl83.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl83.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl83.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl83.Location = new System.Drawing.Point(311, 35);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(28, 26);
            this.labelControl83.TabIndex = 50;
            this.labelControl83.Text = "Blow Up";
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Options.UseTextOptions = true;
            this.labelControl82.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl82.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl82.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl82.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl82.Location = new System.Drawing.Point(261, 35);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(21, 26);
            this.labelControl82.TabIndex = 49;
            this.labelControl82.Text = "Side 2";
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Options.UseTextOptions = true;
            this.labelControl81.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl81.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl81.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl81.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl81.Location = new System.Drawing.Point(202, 35);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(26, 26);
            this.labelControl81.TabIndex = 48;
            this.labelControl81.Text = "Side 1";
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Options.UseTextOptions = true;
            this.labelControl80.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl80.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl80.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl80.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl80.Location = new System.Drawing.Point(143, 35);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(43, 26);
            this.labelControl80.TabIndex = 47;
            this.labelControl80.Text = "Plot Radius";
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Options.UseTextOptions = true;
            this.labelControl74.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl74.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl74.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl74.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl74.Location = new System.Drawing.Point(30, 35);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(47, 26);
            this.labelControl74.TabIndex = 46;
            this.labelControl74.Text = "Area Code";
            // 
            // tbAreaBlowup5
            // 
            this.tbAreaBlowup5.Enabled = false;
            this.tbAreaBlowup5.EnterMoveNextControl = true;
            this.tbAreaBlowup5.Location = new System.Drawing.Point(301, 171);
            this.tbAreaBlowup5.MenuManager = this.barManager1;
            this.tbAreaBlowup5.Name = "tbAreaBlowup5";
            this.tbAreaBlowup5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup5.Properties.ReadOnly = true;
            this.tbAreaBlowup5.Size = new System.Drawing.Size(50, 20);
            this.tbAreaBlowup5.TabIndex = 29;
            // 
            // tbAreaSide52
            // 
            this.tbAreaSide52.EnterMoveNextControl = true;
            this.tbAreaSide52.Location = new System.Drawing.Point(248, 171);
            this.tbAreaSide52.MenuManager = this.barManager1;
            this.tbAreaSide52.Name = "tbAreaSide52";
            this.tbAreaSide52.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide52.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide52.Size = new System.Drawing.Size(47, 20);
            this.tbAreaSide52.TabIndex = 24;
            this.tbAreaSide52.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide52.Validated += new System.EventHandler(this.tbAreaSide52_Validated);
            // 
            // tbAreaBlowup4
            // 
            this.tbAreaBlowup4.Enabled = false;
            this.tbAreaBlowup4.EnterMoveNextControl = true;
            this.tbAreaBlowup4.Location = new System.Drawing.Point(301, 145);
            this.tbAreaBlowup4.MenuManager = this.barManager1;
            this.tbAreaBlowup4.Name = "tbAreaBlowup4";
            this.tbAreaBlowup4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup4.Properties.ReadOnly = true;
            this.tbAreaBlowup4.Size = new System.Drawing.Size(50, 20);
            this.tbAreaBlowup4.TabIndex = 23;
            // 
            // tbAreaSide42
            // 
            this.tbAreaSide42.EnterMoveNextControl = true;
            this.tbAreaSide42.Location = new System.Drawing.Point(248, 145);
            this.tbAreaSide42.MenuManager = this.barManager1;
            this.tbAreaSide42.Name = "tbAreaSide42";
            this.tbAreaSide42.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide42.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide42.Size = new System.Drawing.Size(47, 20);
            this.tbAreaSide42.TabIndex = 19;
            this.tbAreaSide42.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide42.Validated += new System.EventHandler(this.tbAreaSide42_Validated);
            // 
            // tbAreaBlowup3
            // 
            this.tbAreaBlowup3.Enabled = false;
            this.tbAreaBlowup3.EnterMoveNextControl = true;
            this.tbAreaBlowup3.Location = new System.Drawing.Point(301, 119);
            this.tbAreaBlowup3.MenuManager = this.barManager1;
            this.tbAreaBlowup3.Name = "tbAreaBlowup3";
            this.tbAreaBlowup3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup3.Properties.ReadOnly = true;
            this.tbAreaBlowup3.Size = new System.Drawing.Size(50, 20);
            this.tbAreaBlowup3.TabIndex = 17;
            // 
            // tbAreaSide32
            // 
            this.tbAreaSide32.EnterMoveNextControl = true;
            this.tbAreaSide32.Location = new System.Drawing.Point(248, 119);
            this.tbAreaSide32.MenuManager = this.barManager1;
            this.tbAreaSide32.Name = "tbAreaSide32";
            this.tbAreaSide32.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide32.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide32.Size = new System.Drawing.Size(47, 20);
            this.tbAreaSide32.TabIndex = 14;
            this.tbAreaSide32.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide32.Validated += new System.EventHandler(this.tbAreaSide32_Validated);
            // 
            // tbAreaBlowup2
            // 
            this.tbAreaBlowup2.Enabled = false;
            this.tbAreaBlowup2.EnterMoveNextControl = true;
            this.tbAreaBlowup2.Location = new System.Drawing.Point(301, 93);
            this.tbAreaBlowup2.MenuManager = this.barManager1;
            this.tbAreaBlowup2.Name = "tbAreaBlowup2";
            this.tbAreaBlowup2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup2.Properties.ReadOnly = true;
            this.tbAreaBlowup2.Size = new System.Drawing.Size(50, 20);
            this.tbAreaBlowup2.TabIndex = 11;
            // 
            // tbAreaSide22
            // 
            this.tbAreaSide22.EnterMoveNextControl = true;
            this.tbAreaSide22.Location = new System.Drawing.Point(248, 93);
            this.tbAreaSide22.MenuManager = this.barManager1;
            this.tbAreaSide22.Name = "tbAreaSide22";
            this.tbAreaSide22.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide22.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide22.Size = new System.Drawing.Size(47, 20);
            this.tbAreaSide22.TabIndex = 9;
            this.tbAreaSide22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide22.Validated += new System.EventHandler(this.tbAreaSide22_Validated);
            // 
            // tbAreaBlowup1
            // 
            this.tbAreaBlowup1.Enabled = false;
            this.tbAreaBlowup1.EnterMoveNextControl = true;
            this.tbAreaBlowup1.Location = new System.Drawing.Point(301, 67);
            this.tbAreaBlowup1.MenuManager = this.barManager1;
            this.tbAreaBlowup1.Name = "tbAreaBlowup1";
            this.tbAreaBlowup1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup1.Properties.ReadOnly = true;
            this.tbAreaBlowup1.Size = new System.Drawing.Size(50, 20);
            this.tbAreaBlowup1.TabIndex = 5;
            // 
            // tbAreaSide12
            // 
            this.tbAreaSide12.EnterMoveNextControl = true;
            this.tbAreaSide12.Location = new System.Drawing.Point(248, 67);
            this.tbAreaSide12.MenuManager = this.barManager1;
            this.tbAreaSide12.Name = "tbAreaSide12";
            this.tbAreaSide12.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide12.Size = new System.Drawing.Size(47, 20);
            this.tbAreaSide12.TabIndex = 4;
            this.tbAreaSide12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide12.Validated += new System.EventHandler(this.tbAreaSide12_Validated);
            // 
            // tbAreaSide51
            // 
            this.tbAreaSide51.EnterMoveNextControl = true;
            this.tbAreaSide51.Location = new System.Drawing.Point(192, 171);
            this.tbAreaSide51.MenuManager = this.barManager1;
            this.tbAreaSide51.Name = "tbAreaSide51";
            this.tbAreaSide51.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide51.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide51.Size = new System.Drawing.Size(50, 20);
            this.tbAreaSide51.TabIndex = 23;
            this.tbAreaSide51.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide51.Validated += new System.EventHandler(this.tbAreaSide51_Validated);
            // 
            // tbAreaPlotRadius5
            // 
            this.tbAreaPlotRadius5.EnterMoveNextControl = true;
            this.tbAreaPlotRadius5.Location = new System.Drawing.Point(139, 171);
            this.tbAreaPlotRadius5.MenuManager = this.barManager1;
            this.tbAreaPlotRadius5.Name = "tbAreaPlotRadius5";
            this.tbAreaPlotRadius5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius5.Size = new System.Drawing.Size(47, 20);
            this.tbAreaPlotRadius5.TabIndex = 22;
            this.tbAreaPlotRadius5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius5.Validated += new System.EventHandler(this.tbAreaPlotRadius5_Validated);
            // 
            // tbAreaSide41
            // 
            this.tbAreaSide41.EnterMoveNextControl = true;
            this.tbAreaSide41.Location = new System.Drawing.Point(192, 145);
            this.tbAreaSide41.MenuManager = this.barManager1;
            this.tbAreaSide41.Name = "tbAreaSide41";
            this.tbAreaSide41.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide41.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide41.Size = new System.Drawing.Size(50, 20);
            this.tbAreaSide41.TabIndex = 18;
            this.tbAreaSide41.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide41.Validated += new System.EventHandler(this.tbAreaSide41_Validated);
            // 
            // tbAreaPlotRadius4
            // 
            this.tbAreaPlotRadius4.EnterMoveNextControl = true;
            this.tbAreaPlotRadius4.Location = new System.Drawing.Point(139, 145);
            this.tbAreaPlotRadius4.MenuManager = this.barManager1;
            this.tbAreaPlotRadius4.Name = "tbAreaPlotRadius4";
            this.tbAreaPlotRadius4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius4.Size = new System.Drawing.Size(47, 20);
            this.tbAreaPlotRadius4.TabIndex = 17;
            this.tbAreaPlotRadius4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius4.Validated += new System.EventHandler(this.tbAreaPlotRadius4_Validated);
            // 
            // tbAreaSide31
            // 
            this.tbAreaSide31.EnterMoveNextControl = true;
            this.tbAreaSide31.Location = new System.Drawing.Point(192, 119);
            this.tbAreaSide31.MenuManager = this.barManager1;
            this.tbAreaSide31.Name = "tbAreaSide31";
            this.tbAreaSide31.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide31.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide31.Size = new System.Drawing.Size(50, 20);
            this.tbAreaSide31.TabIndex = 13;
            this.tbAreaSide31.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide31.Validated += new System.EventHandler(this.tbAreaSide31_Validated);
            // 
            // tbAreaPlotRadius3
            // 
            this.tbAreaPlotRadius3.EnterMoveNextControl = true;
            this.tbAreaPlotRadius3.Location = new System.Drawing.Point(139, 119);
            this.tbAreaPlotRadius3.MenuManager = this.barManager1;
            this.tbAreaPlotRadius3.Name = "tbAreaPlotRadius3";
            this.tbAreaPlotRadius3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius3.Size = new System.Drawing.Size(47, 20);
            this.tbAreaPlotRadius3.TabIndex = 12;
            this.tbAreaPlotRadius3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius3.Validated += new System.EventHandler(this.tbAreaPlotRadius3_Validated);
            // 
            // tbAreaSide21
            // 
            this.tbAreaSide21.EnterMoveNextControl = true;
            this.tbAreaSide21.Location = new System.Drawing.Point(192, 93);
            this.tbAreaSide21.MenuManager = this.barManager1;
            this.tbAreaSide21.Name = "tbAreaSide21";
            this.tbAreaSide21.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide21.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide21.Size = new System.Drawing.Size(50, 20);
            this.tbAreaSide21.TabIndex = 8;
            this.tbAreaSide21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide21.Validated += new System.EventHandler(this.tbAreaSide21_Validated);
            // 
            // tbAreaPlotRadius2
            // 
            this.tbAreaPlotRadius2.EnterMoveNextControl = true;
            this.tbAreaPlotRadius2.Location = new System.Drawing.Point(139, 93);
            this.tbAreaPlotRadius2.MenuManager = this.barManager1;
            this.tbAreaPlotRadius2.Name = "tbAreaPlotRadius2";
            this.tbAreaPlotRadius2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius2.Size = new System.Drawing.Size(47, 20);
            this.tbAreaPlotRadius2.TabIndex = 7;
            this.tbAreaPlotRadius2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius2.Validated += new System.EventHandler(this.tbAreaPlotRadius2_Validated);
            // 
            // tbAreaSide11
            // 
            this.tbAreaSide11.EnterMoveNextControl = true;
            this.tbAreaSide11.Location = new System.Drawing.Point(192, 67);
            this.tbAreaSide11.MenuManager = this.barManager1;
            this.tbAreaSide11.Name = "tbAreaSide11";
            this.tbAreaSide11.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide11.Size = new System.Drawing.Size(50, 20);
            this.tbAreaSide11.TabIndex = 3;
            this.tbAreaSide11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide11.Validated += new System.EventHandler(this.tbAreaSide11_Validated);
            // 
            // tbAreaPlotRadius1
            // 
            this.tbAreaPlotRadius1.EnterMoveNextControl = true;
            this.tbAreaPlotRadius1.Location = new System.Drawing.Point(139, 67);
            this.tbAreaPlotRadius1.MenuManager = this.barManager1;
            this.tbAreaPlotRadius1.Name = "tbAreaPlotRadius1";
            this.tbAreaPlotRadius1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius1.Size = new System.Drawing.Size(47, 20);
            this.tbAreaPlotRadius1.TabIndex = 2;
            this.tbAreaPlotRadius1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius1.Validated += new System.EventHandler(this.tbAreaPlotRadius1_Validated);
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Options.UseTextOptions = true;
            this.labelControl73.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl73.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl73.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl73.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl73.Location = new System.Drawing.Point(87, 35);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(43, 26);
            this.labelControl73.TabIndex = 25;
            this.labelControl73.Text = "Plot Acres";
            // 
            // labelControl75
            // 
            this.labelControl75.Location = new System.Drawing.Point(5, 174);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(17, 13);
            this.labelControl75.TabIndex = 23;
            this.labelControl75.Text = "A5:";
            // 
            // labelControl76
            // 
            this.labelControl76.Location = new System.Drawing.Point(5, 148);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(17, 13);
            this.labelControl76.TabIndex = 22;
            this.labelControl76.Text = "A4:";
            // 
            // labelControl77
            // 
            this.labelControl77.Location = new System.Drawing.Point(5, 122);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(17, 13);
            this.labelControl77.TabIndex = 21;
            this.labelControl77.Text = "A3:";
            // 
            // labelControl78
            // 
            this.labelControl78.Location = new System.Drawing.Point(5, 96);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(17, 13);
            this.labelControl78.TabIndex = 20;
            this.labelControl78.Text = "A2:";
            // 
            // labelControl79
            // 
            this.labelControl79.Location = new System.Drawing.Point(5, 70);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(17, 13);
            this.labelControl79.TabIndex = 19;
            this.labelControl79.Text = "A1:";
            // 
            // tbAreaPlotAcres5
            // 
            this.tbAreaPlotAcres5.EnterMoveNextControl = true;
            this.tbAreaPlotAcres5.Location = new System.Drawing.Point(83, 171);
            this.tbAreaPlotAcres5.MenuManager = this.barManager1;
            this.tbAreaPlotAcres5.Name = "tbAreaPlotAcres5";
            this.tbAreaPlotAcres5.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres5.Size = new System.Drawing.Size(50, 20);
            this.tbAreaPlotAcres5.TabIndex = 21;
            this.tbAreaPlotAcres5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres5.Validated += new System.EventHandler(this.tbAreaPlotAcres5_Validated);
            // 
            // tbAreaPlotAcres4
            // 
            this.tbAreaPlotAcres4.EnterMoveNextControl = true;
            this.tbAreaPlotAcres4.Location = new System.Drawing.Point(83, 145);
            this.tbAreaPlotAcres4.MenuManager = this.barManager1;
            this.tbAreaPlotAcres4.Name = "tbAreaPlotAcres4";
            this.tbAreaPlotAcres4.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres4.Size = new System.Drawing.Size(50, 20);
            this.tbAreaPlotAcres4.TabIndex = 16;
            this.tbAreaPlotAcres4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres4.Validated += new System.EventHandler(this.tbAreaPlotAcres4_Validated);
            // 
            // tbAreaPlotAcres3
            // 
            this.tbAreaPlotAcres3.EnterMoveNextControl = true;
            this.tbAreaPlotAcres3.Location = new System.Drawing.Point(83, 119);
            this.tbAreaPlotAcres3.MenuManager = this.barManager1;
            this.tbAreaPlotAcres3.Name = "tbAreaPlotAcres3";
            this.tbAreaPlotAcres3.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres3.Size = new System.Drawing.Size(50, 20);
            this.tbAreaPlotAcres3.TabIndex = 11;
            this.tbAreaPlotAcres3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres3.Validated += new System.EventHandler(this.tbAreaPlotAcres3_Validated);
            // 
            // tbAreaPlotAcres2
            // 
            this.tbAreaPlotAcres2.EnterMoveNextControl = true;
            this.tbAreaPlotAcres2.Location = new System.Drawing.Point(83, 93);
            this.tbAreaPlotAcres2.MenuManager = this.barManager1;
            this.tbAreaPlotAcres2.Name = "tbAreaPlotAcres2";
            this.tbAreaPlotAcres2.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres2.Size = new System.Drawing.Size(50, 20);
            this.tbAreaPlotAcres2.TabIndex = 6;
            this.tbAreaPlotAcres2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres2.Validated += new System.EventHandler(this.tbAreaPlotAcres2_Validated);
            // 
            // tbAreaPlotAcres1
            // 
            this.tbAreaPlotAcres1.EnterMoveNextControl = true;
            this.tbAreaPlotAcres1.Location = new System.Drawing.Point(83, 67);
            this.tbAreaPlotAcres1.MenuManager = this.barManager1;
            this.tbAreaPlotAcres1.Name = "tbAreaPlotAcres1";
            this.tbAreaPlotAcres1.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres1.Size = new System.Drawing.Size(50, 20);
            this.tbAreaPlotAcres1.TabIndex = 1;
            this.tbAreaPlotAcres1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres1.Validated += new System.EventHandler(this.tbAreaPlotAcres1_Validated);
            // 
            // tbAreaCode1
            // 
            this.tbAreaCode1.EnterMoveNextControl = true;
            this.tbAreaCode1.Location = new System.Drawing.Point(30, 67);
            this.tbAreaCode1.MenuManager = this.barManager1;
            this.tbAreaCode1.Name = "tbAreaCode1";
            this.tbAreaCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode1.Properties.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode1.Size = new System.Drawing.Size(47, 20);
            this.tbAreaCode1.TabIndex = 0;
            this.tbAreaCode1.SelectedValueChanged += new System.EventHandler(this.tbAreaCode1_SelectedValueChanged);
            this.tbAreaCode1.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode1_Validating);
            this.tbAreaCode1.Validated += new System.EventHandler(this.tbAreaCode1_Validated);
            // 
            // tbAreaCode2
            // 
            this.tbAreaCode2.EnterMoveNextControl = true;
            this.tbAreaCode2.Location = new System.Drawing.Point(30, 93);
            this.tbAreaCode2.MenuManager = this.barManager1;
            this.tbAreaCode2.Name = "tbAreaCode2";
            this.tbAreaCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode2.Properties.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode2.Size = new System.Drawing.Size(47, 20);
            this.tbAreaCode2.TabIndex = 5;
            this.tbAreaCode2.SelectedValueChanged += new System.EventHandler(this.tbAreaCode2_SelectedValueChanged);
            this.tbAreaCode2.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode2_Validating);
            this.tbAreaCode2.Validated += new System.EventHandler(this.tbAreaCode2_Validated);
            // 
            // tbAreaCode3
            // 
            this.tbAreaCode3.EnterMoveNextControl = true;
            this.tbAreaCode3.Location = new System.Drawing.Point(30, 119);
            this.tbAreaCode3.MenuManager = this.barManager1;
            this.tbAreaCode3.Name = "tbAreaCode3";
            this.tbAreaCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode3.Properties.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode3.Size = new System.Drawing.Size(47, 20);
            this.tbAreaCode3.TabIndex = 10;
            this.tbAreaCode3.SelectedValueChanged += new System.EventHandler(this.tbAreaCode3_SelectedValueChanged);
            this.tbAreaCode3.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode3_Validating);
            this.tbAreaCode3.Validated += new System.EventHandler(this.tbAreaCode3_Validated);
            // 
            // tbAreaCode4
            // 
            this.tbAreaCode4.EnterMoveNextControl = true;
            this.tbAreaCode4.Location = new System.Drawing.Point(30, 145);
            this.tbAreaCode4.MenuManager = this.barManager1;
            this.tbAreaCode4.Name = "tbAreaCode4";
            this.tbAreaCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode4.Properties.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode4.Size = new System.Drawing.Size(47, 20);
            this.tbAreaCode4.TabIndex = 15;
            this.tbAreaCode4.SelectedValueChanged += new System.EventHandler(this.tbAreaCode4_SelectedValueChanged);
            this.tbAreaCode4.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode4_Validating);
            this.tbAreaCode4.Validated += new System.EventHandler(this.tbAreaCode4_Validated);
            // 
            // tbAreaCode5
            // 
            this.tbAreaCode5.EnterMoveNextControl = true;
            this.tbAreaCode5.Location = new System.Drawing.Point(30, 171);
            this.tbAreaCode5.MenuManager = this.barManager1;
            this.tbAreaCode5.Name = "tbAreaCode5";
            this.tbAreaCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode5.Properties.Items.AddRange(new object[] {
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode5.Size = new System.Drawing.Size(47, 20);
            this.tbAreaCode5.TabIndex = 20;
            this.tbAreaCode5.SelectedValueChanged += new System.EventHandler(this.tbAreaCode5_SelectedValueChanged);
            this.tbAreaCode5.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode5_Validating);
            this.tbAreaCode5.Validated += new System.EventHandler(this.tbAreaCode5_Validated);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl72);
            this.groupControl1.Controls.Add(this.labelControl71);
            this.groupControl1.Controls.Add(this.labelControl70);
            this.groupControl1.Controls.Add(this.labelControl69);
            this.groupControl1.Controls.Add(this.labelControl68);
            this.groupControl1.Controls.Add(this.labelControl67);
            this.groupControl1.Controls.Add(this.labelControl66);
            this.groupControl1.Controls.Add(this.tbBafLimit5);
            this.groupControl1.Controls.Add(this.tbBaf5);
            this.groupControl1.Controls.Add(this.tbBafLimit4);
            this.groupControl1.Controls.Add(this.tbBaf4);
            this.groupControl1.Controls.Add(this.tbBafLimit3);
            this.groupControl1.Controls.Add(this.tbBaf3);
            this.groupControl1.Controls.Add(this.tbBafLimit2);
            this.groupControl1.Controls.Add(this.tbBaf2);
            this.groupControl1.Controls.Add(this.tbBafLimit1);
            this.groupControl1.Controls.Add(this.tbBaf1);
            this.groupControl1.Location = new System.Drawing.Point(0, 205);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(179, 205);
            this.groupControl1.TabIndex = 17;
            this.groupControl1.Text = "Proportional Plot (BAF)";
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Options.UseTextOptions = true;
            this.labelControl72.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl72.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl72.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl72.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl72.Location = new System.Drawing.Point(90, 35);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(76, 26);
            this.labelControl72.TabIndex = 25;
            this.labelControl72.Text = "Plot Radius Factor Calc";
            // 
            // labelControl71
            // 
            this.labelControl71.Location = new System.Drawing.Point(35, 48);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(48, 13);
            this.labelControl71.TabIndex = 24;
            this.labelControl71.Text = "Input BAF";
            // 
            // labelControl70
            // 
            this.labelControl70.Location = new System.Drawing.Point(5, 174);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(16, 13);
            this.labelControl70.TabIndex = 23;
            this.labelControl70.Text = "B5:";
            // 
            // labelControl69
            // 
            this.labelControl69.Location = new System.Drawing.Point(5, 148);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(16, 13);
            this.labelControl69.TabIndex = 22;
            this.labelControl69.Text = "B4:";
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(5, 122);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(16, 13);
            this.labelControl68.TabIndex = 21;
            this.labelControl68.Text = "B3:";
            // 
            // labelControl67
            // 
            this.labelControl67.Location = new System.Drawing.Point(5, 96);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(16, 13);
            this.labelControl67.TabIndex = 20;
            this.labelControl67.Text = "B2:";
            // 
            // labelControl66
            // 
            this.labelControl66.Location = new System.Drawing.Point(5, 70);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(16, 13);
            this.labelControl66.TabIndex = 19;
            this.labelControl66.Text = "B1:";
            // 
            // tbBafLimit5
            // 
            this.tbBafLimit5.Enabled = false;
            this.tbBafLimit5.Location = new System.Drawing.Point(97, 171);
            this.tbBafLimit5.MenuManager = this.barManager1;
            this.tbBafLimit5.Name = "tbBafLimit5";
            this.tbBafLimit5.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit5.Properties.ReadOnly = true;
            this.tbBafLimit5.Size = new System.Drawing.Size(61, 20);
            this.tbBafLimit5.TabIndex = 18;
            // 
            // tbBaf5
            // 
            this.tbBaf5.EnterMoveNextControl = true;
            this.tbBaf5.Location = new System.Drawing.Point(30, 171);
            this.tbBaf5.MenuManager = this.barManager1;
            this.tbBaf5.Name = "tbBaf5";
            this.tbBaf5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf5.Size = new System.Drawing.Size(58, 20);
            this.tbBaf5.TabIndex = 4;
            this.tbBaf5.TextChanged += new System.EventHandler(this.tbBaf5_TextChanged);
            this.tbBaf5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf5.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // tbBafLimit4
            // 
            this.tbBafLimit4.Enabled = false;
            this.tbBafLimit4.Location = new System.Drawing.Point(97, 145);
            this.tbBafLimit4.MenuManager = this.barManager1;
            this.tbBafLimit4.Name = "tbBafLimit4";
            this.tbBafLimit4.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit4.Properties.ReadOnly = true;
            this.tbBafLimit4.Size = new System.Drawing.Size(61, 20);
            this.tbBafLimit4.TabIndex = 16;
            // 
            // tbBaf4
            // 
            this.tbBaf4.EnterMoveNextControl = true;
            this.tbBaf4.Location = new System.Drawing.Point(30, 145);
            this.tbBaf4.MenuManager = this.barManager1;
            this.tbBaf4.Name = "tbBaf4";
            this.tbBaf4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf4.Size = new System.Drawing.Size(58, 20);
            this.tbBaf4.TabIndex = 3;
            this.tbBaf4.TextChanged += new System.EventHandler(this.tbBaf4_TextChanged);
            this.tbBaf4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf4.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // tbBafLimit3
            // 
            this.tbBafLimit3.Enabled = false;
            this.tbBafLimit3.Location = new System.Drawing.Point(97, 119);
            this.tbBafLimit3.MenuManager = this.barManager1;
            this.tbBafLimit3.Name = "tbBafLimit3";
            this.tbBafLimit3.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit3.Properties.ReadOnly = true;
            this.tbBafLimit3.Size = new System.Drawing.Size(61, 20);
            this.tbBafLimit3.TabIndex = 14;
            // 
            // tbBaf3
            // 
            this.tbBaf3.EnterMoveNextControl = true;
            this.tbBaf3.Location = new System.Drawing.Point(30, 119);
            this.tbBaf3.MenuManager = this.barManager1;
            this.tbBaf3.Name = "tbBaf3";
            this.tbBaf3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf3.Size = new System.Drawing.Size(58, 20);
            this.tbBaf3.TabIndex = 2;
            this.tbBaf3.TextChanged += new System.EventHandler(this.tbBaf3_TextChanged);
            this.tbBaf3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf3.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // tbBafLimit2
            // 
            this.tbBafLimit2.Enabled = false;
            this.tbBafLimit2.Location = new System.Drawing.Point(97, 93);
            this.tbBafLimit2.MenuManager = this.barManager1;
            this.tbBafLimit2.Name = "tbBafLimit2";
            this.tbBafLimit2.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit2.Properties.ReadOnly = true;
            this.tbBafLimit2.Size = new System.Drawing.Size(61, 20);
            this.tbBafLimit2.TabIndex = 12;
            // 
            // tbBaf2
            // 
            this.tbBaf2.EnterMoveNextControl = true;
            this.tbBaf2.Location = new System.Drawing.Point(30, 93);
            this.tbBaf2.MenuManager = this.barManager1;
            this.tbBaf2.Name = "tbBaf2";
            this.tbBaf2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf2.Size = new System.Drawing.Size(58, 20);
            this.tbBaf2.TabIndex = 1;
            this.tbBaf2.TextChanged += new System.EventHandler(this.tbBaf2_TextChanged);
            this.tbBaf2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf2.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // tbBafLimit1
            // 
            this.tbBafLimit1.Enabled = false;
            this.tbBafLimit1.Location = new System.Drawing.Point(97, 67);
            this.tbBafLimit1.MenuManager = this.barManager1;
            this.tbBafLimit1.Name = "tbBafLimit1";
            this.tbBafLimit1.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit1.Properties.ReadOnly = true;
            this.tbBafLimit1.Size = new System.Drawing.Size(61, 20);
            this.tbBafLimit1.TabIndex = 10;
            // 
            // tbBaf1
            // 
            this.tbBaf1.EnterMoveNextControl = true;
            this.tbBaf1.Location = new System.Drawing.Point(30, 67);
            this.tbBaf1.MenuManager = this.barManager1;
            this.tbBaf1.Name = "tbBaf1";
            this.tbBaf1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf1.Properties.EditFormat.FormatString = "{0:#.##}";
            this.tbBaf1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf1.Size = new System.Drawing.Size(58, 20);
            this.tbBaf1.TabIndex = 0;
            this.tbBaf1.TextChanged += new System.EventHandler(this.tbBaf1_TextChanged);
            this.tbBaf1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf1.Validated += new System.EventHandler(this.tbBaf_Validated);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.tbAutoSegmentLength);
            this.panelControl5.Controls.Add(this.labelControl65);
            this.panelControl5.Controls.Add(this.labelControl64);
            this.panelControl5.Controls.Add(this.cbGrowthModel);
            this.panelControl5.Controls.Add(this.labelControl63);
            this.panelControl5.Controls.Add(this.labelControl62);
            this.panelControl5.Controls.Add(this.labelControl61);
            this.panelControl5.Controls.Add(this.labelControl60);
            this.panelControl5.Controls.Add(this.cbCostTable);
            this.panelControl5.Controls.Add(this.labelControl59);
            this.panelControl5.Controls.Add(this.cbPriceTable);
            this.panelControl5.Controls.Add(this.labelControl58);
            this.panelControl5.Controls.Add(this.cbGradeTable);
            this.panelControl5.Controls.Add(this.labelControl57);
            this.panelControl5.Controls.Add(this.cbSortTable);
            this.panelControl5.Controls.Add(this.labelControl56);
            this.panelControl5.Controls.Add(this.cbSpeciesTable);
            this.panelControl5.Controls.Add(this.cbNonStockedTable);
            this.panelControl5.Controls.Add(this.cbNonTimberedTable);
            this.panelControl5.Controls.Add(this.cbHarvestTable);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 89);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1148, 110);
            this.panelControl5.TabIndex = 16;
            // 
            // tbAutoSegmentLength
            // 
            this.tbAutoSegmentLength.Location = new System.Drawing.Point(912, 24);
            this.tbAutoSegmentLength.MenuManager = this.barManager1;
            this.tbAutoSegmentLength.Name = "tbAutoSegmentLength";
            this.tbAutoSegmentLength.Size = new System.Drawing.Size(32, 20);
            this.tbAutoSegmentLength.TabIndex = 5;
            this.tbAutoSegmentLength.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl65
            // 
            this.labelControl65.Location = new System.Drawing.Point(793, 27);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(108, 13);
            this.labelControl65.TabIndex = 24;
            this.labelControl65.Text = "Auto Segment Length:";
            // 
            // labelControl64
            // 
            this.labelControl64.Location = new System.Drawing.Point(659, 56);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(66, 13);
            this.labelControl64.TabIndex = 23;
            this.labelControl64.Text = "Growth Model";
            // 
            // cbGrowthModel
            // 
            this.cbGrowthModel.Location = new System.Drawing.Point(629, 75);
            this.cbGrowthModel.MenuManager = this.barManager1;
            this.cbGrowthModel.Name = "cbGrowthModel";
            this.cbGrowthModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGrowthModel.Size = new System.Drawing.Size(134, 20);
            this.cbGrowthModel.TabIndex = 8;
            // 
            // labelControl63
            // 
            this.labelControl63.Location = new System.Drawing.Point(504, 56);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(67, 13);
            this.labelControl63.TabIndex = 21;
            this.labelControl63.Text = "Harvest Table";
            this.labelControl63.Visible = false;
            // 
            // labelControl62
            // 
            this.labelControl62.Location = new System.Drawing.Point(171, 56);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(96, 13);
            this.labelControl62.TabIndex = 19;
            this.labelControl62.Text = "Non-Timbered Table";
            // 
            // labelControl61
            // 
            this.labelControl61.Location = new System.Drawing.Point(25, 56);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(90, 13);
            this.labelControl61.TabIndex = 17;
            this.labelControl61.Text = "Non-Stocked Table";
            // 
            // labelControl60
            // 
            this.labelControl60.Location = new System.Drawing.Point(659, 5);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(51, 13);
            this.labelControl60.TabIndex = 15;
            this.labelControl60.Text = "Cost Table";
            // 
            // cbCostTable
            // 
            this.cbCostTable.Location = new System.Drawing.Point(629, 24);
            this.cbCostTable.MenuManager = this.barManager1;
            this.cbCostTable.Name = "cbCostTable";
            this.cbCostTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCostTable.Size = new System.Drawing.Size(134, 20);
            this.cbCostTable.TabIndex = 4;
            // 
            // labelControl59
            // 
            this.labelControl59.Location = new System.Drawing.Point(504, 5);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(52, 13);
            this.labelControl59.TabIndex = 13;
            this.labelControl59.Text = "Price Table";
            // 
            // cbPriceTable
            // 
            this.cbPriceTable.Location = new System.Drawing.Point(474, 24);
            this.cbPriceTable.MenuManager = this.barManager1;
            this.cbPriceTable.Name = "cbPriceTable";
            this.cbPriceTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPriceTable.Size = new System.Drawing.Size(134, 20);
            this.cbPriceTable.TabIndex = 3;
            // 
            // labelControl58
            // 
            this.labelControl58.Location = new System.Drawing.Point(348, 5);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(58, 13);
            this.labelControl58.TabIndex = 11;
            this.labelControl58.Text = "Grade Table";
            // 
            // cbGradeTable
            // 
            this.cbGradeTable.Location = new System.Drawing.Point(318, 24);
            this.cbGradeTable.MenuManager = this.barManager1;
            this.cbGradeTable.Name = "cbGradeTable";
            this.cbGradeTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGradeTable.Size = new System.Drawing.Size(134, 20);
            this.cbGradeTable.TabIndex = 2;
            // 
            // labelControl57
            // 
            this.labelControl57.Location = new System.Drawing.Point(188, 5);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(49, 13);
            this.labelControl57.TabIndex = 9;
            this.labelControl57.Text = "Sort Table";
            // 
            // cbSortTable
            // 
            this.cbSortTable.Location = new System.Drawing.Point(158, 24);
            this.cbSortTable.MenuManager = this.barManager1;
            this.cbSortTable.Name = "cbSortTable";
            this.cbSortTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSortTable.Size = new System.Drawing.Size(134, 20);
            this.cbSortTable.TabIndex = 1;
            // 
            // labelControl56
            // 
            this.labelControl56.Location = new System.Drawing.Point(35, 5);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(65, 13);
            this.labelControl56.TabIndex = 7;
            this.labelControl56.Text = "Species Table";
            // 
            // cbSpeciesTable
            // 
            this.cbSpeciesTable.Location = new System.Drawing.Point(5, 24);
            this.cbSpeciesTable.MenuManager = this.barManager1;
            this.cbSpeciesTable.Name = "cbSpeciesTable";
            this.cbSpeciesTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSpeciesTable.Size = new System.Drawing.Size(134, 20);
            this.cbSpeciesTable.TabIndex = 0;
            // 
            // cbNonStockedTable
            // 
            this.cbNonStockedTable.Location = new System.Drawing.Point(5, 75);
            this.cbNonStockedTable.MenuManager = this.barManager1;
            this.cbNonStockedTable.Name = "cbNonStockedTable";
            this.cbNonStockedTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNonStockedTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbNonStockedTable.Properties.DataSource = this.nonStockedBindingSource;
            this.cbNonStockedTable.Properties.DisplayMember = "TableName";
            this.cbNonStockedTable.Properties.NullText = "";
            this.cbNonStockedTable.Properties.PopupSizeable = false;
            this.cbNonStockedTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbNonStockedTable.Properties.ValueMember = "NonStockedID";
            this.cbNonStockedTable.Size = new System.Drawing.Size(134, 20);
            this.cbNonStockedTable.TabIndex = 5;
            // 
            // nonStockedBindingSource
            // 
            this.nonStockedBindingSource.DataSource = typeof(Project.DAL.Nonstocked);
            // 
            // cbNonTimberedTable
            // 
            this.cbNonTimberedTable.Location = new System.Drawing.Point(158, 75);
            this.cbNonTimberedTable.MenuManager = this.barManager1;
            this.cbNonTimberedTable.Name = "cbNonTimberedTable";
            this.cbNonTimberedTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNonTimberedTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbNonTimberedTable.Properties.DataSource = this.nonTimberedBindingSource;
            this.cbNonTimberedTable.Properties.DisplayMember = "TableName";
            this.cbNonTimberedTable.Properties.NullText = "";
            this.cbNonTimberedTable.Properties.PopupSizeable = false;
            this.cbNonTimberedTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbNonTimberedTable.Size = new System.Drawing.Size(134, 20);
            this.cbNonTimberedTable.TabIndex = 6;
            // 
            // nonTimberedBindingSource
            // 
            this.nonTimberedBindingSource.DataSource = typeof(Project.DAL.Nontimbered);
            // 
            // cbHarvestTable
            // 
            this.cbHarvestTable.Location = new System.Drawing.Point(474, 75);
            this.cbHarvestTable.MenuManager = this.barManager1;
            this.cbHarvestTable.Name = "cbHarvestTable";
            this.cbHarvestTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHarvestTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbHarvestTable.Properties.DataSource = this.harvestBindingSource;
            this.cbHarvestTable.Properties.DisplayMember = "DisplayCode";
            this.cbHarvestTable.Properties.NullText = "";
            this.cbHarvestTable.Properties.PopupSizeable = false;
            this.cbHarvestTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbHarvestTable.Properties.ValueMember = "DisplayCode";
            this.cbHarvestTable.Size = new System.Drawing.Size(134, 20);
            this.cbHarvestTable.TabIndex = 7;
            this.cbHarvestTable.Visible = false;
            // 
            // harvestBindingSource
            // 
            this.harvestBindingSource.DataSource = typeof(Project.DAL.Harvestsystem);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.btnGrow);
            this.panelControl8.Controls.Add(this.tbGrownToDate);
            this.panelControl8.Controls.Add(this.tbInputDate);
            this.panelControl8.Controls.Add(this.labelControl55);
            this.panelControl8.Controls.Add(this.labelControl54);
            this.panelControl8.Controls.Add(this.labelControl50);
            this.panelControl8.Controls.Add(this.tbNetAcres);
            this.panelControl8.Controls.Add(this.labelControl49);
            this.panelControl8.Controls.Add(this.tbSection);
            this.panelControl8.Controls.Add(this.labelControl48);
            this.panelControl8.Controls.Add(this.tbRange);
            this.panelControl8.Controls.Add(this.labelControl47);
            this.panelControl8.Controls.Add(this.tbTownship);
            this.panelControl8.Controls.Add(this.labelControl46);
            this.panelControl8.Controls.Add(this.tbStand);
            this.panelControl8.Controls.Add(this.labelControl45);
            this.panelControl8.Controls.Add(this.tbTract);
            this.panelControl8.Controls.Add(this.labelControl51);
            this.panelControl8.Controls.Add(this.labelControl52);
            this.panelControl8.Controls.Add(this.labelControl53);
            this.panelControl8.Controls.Add(this.tbProject);
            this.panelControl8.Controls.Add(this.cbState);
            this.panelControl8.Controls.Add(this.cbCounty);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 24);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1148, 65);
            this.panelControl8.TabIndex = 15;
            // 
            // btnGrow
            // 
            this.btnGrow.Location = new System.Drawing.Point(958, 27);
            this.btnGrow.Name = "btnGrow";
            this.btnGrow.Size = new System.Drawing.Size(75, 23);
            this.btnGrow.TabIndex = 22;
            this.btnGrow.Text = "Grow";
            // 
            // tbGrownToDate
            // 
            this.tbGrownToDate.Location = new System.Drawing.Point(848, 29);
            this.tbGrownToDate.MenuManager = this.barManager1;
            this.tbGrownToDate.Name = "tbGrownToDate";
            this.tbGrownToDate.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbGrownToDate.Properties.DisplayFormat.FormatString = "d";
            this.tbGrownToDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tbGrownToDate.Size = new System.Drawing.Size(100, 20);
            this.tbGrownToDate.TabIndex = 9;
            this.tbGrownToDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbInputDate
            // 
            this.tbInputDate.Location = new System.Drawing.Point(848, 3);
            this.tbInputDate.MenuManager = this.barManager1;
            this.tbInputDate.Name = "tbInputDate";
            this.tbInputDate.Properties.DisplayFormat.FormatString = "d";
            this.tbInputDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tbInputDate.Size = new System.Drawing.Size(100, 20);
            this.tbInputDate.TabIndex = 8;
            this.tbInputDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl55
            // 
            this.labelControl55.Location = new System.Drawing.Point(761, 32);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(81, 13);
            this.labelControl55.TabIndex = 19;
            this.labelControl55.Text = "Stand Grown To:";
            // 
            // labelControl54
            // 
            this.labelControl54.Location = new System.Drawing.Point(740, 5);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(102, 13);
            this.labelControl54.TabIndex = 18;
            this.labelControl54.Text = "Cruise or Input Date:";
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(660, 5);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(47, 13);
            this.labelControl50.TabIndex = 17;
            this.labelControl50.Text = "Net Acres";
            // 
            // tbNetAcres
            // 
            this.tbNetAcres.EnterMoveNextControl = true;
            this.tbNetAcres.Location = new System.Drawing.Point(646, 29);
            this.tbNetAcres.MenuManager = this.barManager1;
            this.tbNetAcres.Name = "tbNetAcres";
            this.tbNetAcres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbNetAcres.Size = new System.Drawing.Size(71, 20);
            this.tbNetAcres.TabIndex = 7;
            this.tbNetAcres.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbNetAcres.Validated += new System.EventHandler(this.tbNetAcres_Validated);
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(604, 5);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(17, 13);
            this.labelControl49.TabIndex = 15;
            this.labelControl49.Text = "Sec";
            // 
            // tbSection
            // 
            this.tbSection.EnterMoveNextControl = true;
            this.tbSection.Location = new System.Drawing.Point(589, 29);
            this.tbSection.MenuManager = this.barManager1;
            this.tbSection.Name = "tbSection";
            this.tbSection.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSection.Properties.MaxLength = 2;
            this.tbSection.Size = new System.Drawing.Size(46, 20);
            this.tbSection.TabIndex = 6;
            this.tbSection.TextChanged += new System.EventHandler(this.tbSection_TextChanged);
            this.tbSection.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSection.Validating += new System.ComponentModel.CancelEventHandler(this.tbSection_Validating);
            this.tbSection.Validated += new System.EventHandler(this.tbSection_Validated);
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(542, 5);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(19, 13);
            this.labelControl48.TabIndex = 13;
            this.labelControl48.Text = "Rge";
            // 
            // tbRange
            // 
            this.tbRange.EnterMoveNextControl = true;
            this.tbRange.Location = new System.Drawing.Point(529, 29);
            this.tbRange.MenuManager = this.barManager1;
            this.tbRange.Name = "tbRange";
            this.tbRange.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbRange.Properties.MaxLength = 3;
            this.tbRange.Size = new System.Drawing.Size(46, 20);
            this.tbRange.TabIndex = 5;
            this.tbRange.TextChanged += new System.EventHandler(this.tbRange_TextChanged);
            this.tbRange.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbRange.Validating += new System.ComponentModel.CancelEventHandler(this.tbRange_Validating);
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(480, 5);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(20, 13);
            this.labelControl47.TabIndex = 11;
            this.labelControl47.Text = "Twn";
            // 
            // tbTownship
            // 
            this.tbTownship.EnterMoveNextControl = true;
            this.tbTownship.Location = new System.Drawing.Point(468, 29);
            this.tbTownship.MenuManager = this.barManager1;
            this.tbTownship.Name = "tbTownship";
            this.tbTownship.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTownship.Properties.MaxLength = 3;
            this.tbTownship.Size = new System.Drawing.Size(46, 20);
            this.tbTownship.TabIndex = 4;
            this.tbTownship.TextChanged += new System.EventHandler(this.tbTownship_TextChanged);
            this.tbTownship.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTownship.Validating += new System.ComponentModel.CancelEventHandler(this.tbTownship_Validating);
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(408, 5);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(39, 13);
            this.labelControl46.TabIndex = 9;
            this.labelControl46.Text = "Stand #";
            // 
            // tbStand
            // 
            this.tbStand.EnterMoveNextControl = true;
            this.tbStand.Location = new System.Drawing.Point(397, 29);
            this.tbStand.MenuManager = this.barManager1;
            this.tbStand.Name = "tbStand";
            this.tbStand.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbStand.Properties.MaxLength = 4;
            this.tbStand.Size = new System.Drawing.Size(57, 20);
            this.tbStand.TabIndex = 3;
            this.tbStand.TextChanged += new System.EventHandler(this.tbStand_TextChanged);
            this.tbStand.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStand.Validating += new System.ComponentModel.CancelEventHandler(this.tbStand_Validating);
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(311, 5);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(25, 13);
            this.labelControl45.TabIndex = 7;
            this.labelControl45.Text = "Tract";
            // 
            // tbTract
            // 
            this.tbTract.EnterMoveNextControl = true;
            this.tbTract.Location = new System.Drawing.Point(263, 29);
            this.tbTract.MenuManager = this.barManager1;
            this.tbTract.Name = "tbTract";
            this.tbTract.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTract.Properties.MaxLength = 12;
            this.tbTract.Size = new System.Drawing.Size(115, 20);
            this.tbTract.TabIndex = 2;
            this.tbTract.TextChanged += new System.EventHandler(this.tbTract_TextChanged);
            this.tbTract.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTract.Validating += new System.ComponentModel.CancelEventHandler(this.tbTract_Validating);
            // 
            // labelControl51
            // 
            this.labelControl51.Location = new System.Drawing.Point(191, 5);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(35, 13);
            this.labelControl51.TabIndex = 5;
            this.labelControl51.Text = "County";
            // 
            // labelControl52
            // 
            this.labelControl52.Location = new System.Drawing.Point(127, 5);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(26, 13);
            this.labelControl52.TabIndex = 4;
            this.labelControl52.Text = "State";
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(36, 5);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(34, 13);
            this.labelControl53.TabIndex = 3;
            this.labelControl53.Text = "Project";
            // 
            // tbProject
            // 
            this.tbProject.Location = new System.Drawing.Point(7, 29);
            this.tbProject.MenuManager = this.barManager1;
            this.tbProject.Name = "tbProject";
            this.tbProject.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tbProject.Properties.Appearance.Options.UseFont = true;
            this.tbProject.Properties.ReadOnly = true;
            this.tbProject.Size = new System.Drawing.Size(100, 20);
            this.tbProject.TabIndex = 0;
            // 
            // cbState
            // 
            this.cbState.EnterMoveNextControl = true;
            this.cbState.Location = new System.Drawing.Point(115, 29);
            this.cbState.MenuManager = this.barManager1;
            this.cbState.Name = "cbState";
            this.cbState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbState.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbr", "State", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbState.Properties.DataSource = this.stateBindingSource;
            this.cbState.Properties.DisplayMember = "Abbr";
            this.cbState.Properties.NullText = "";
            this.cbState.Properties.PopupSizeable = false;
            this.cbState.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbState.Properties.ValueMember = "Abbr";
            this.cbState.Size = new System.Drawing.Size(53, 20);
            this.cbState.TabIndex = 0;
            this.cbState.TextChanged += new System.EventHandler(this.cbState_TextChanged);
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataSource = typeof(Projects.DAL.State);
            // 
            // cbCounty
            // 
            this.cbCounty.EnterMoveNextControl = true;
            this.cbCounty.Location = new System.Drawing.Point(184, 29);
            this.cbCounty.MenuManager = this.barManager1;
            this.cbCounty.Name = "cbCounty";
            this.cbCounty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCounty.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountyAbbr", "County", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountyName", "County Name", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbCounty.Properties.DataSource = this.countyBindingSource;
            this.cbCounty.Properties.DisplayMember = "CountyAbbr";
            this.cbCounty.Properties.NullText = "";
            this.cbCounty.Properties.PopupSizeable = false;
            this.cbCounty.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbCounty.Properties.ValueMember = "CountyAbbr";
            this.cbCounty.Size = new System.Drawing.Size(53, 20);
            this.cbCounty.TabIndex = 1;
            // 
            // countyBindingSource
            // 
            this.countyBindingSource.DataSource = typeof(Projects.DAL.StateCounty);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // standYardingSystemBindingSource
            // 
            this.standYardingSystemBindingSource.DataSource = typeof(Project.DAL.StandYardingSystem);
            // 
            // standPermPlotBindingSource
            // 
            this.standPermPlotBindingSource.DataSource = typeof(Project.DAL.StandPermPlot);
            // 
            // standAgeBindingSource
            // 
            this.standAgeBindingSource.DataSource = typeof(Project.DAL.StandAge);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gridControl4);
            this.groupControl4.Location = new System.Drawing.Point(779, 415);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(357, 180);
            this.groupControl4.TabIndex = 30;
            this.groupControl4.Text = "Yarding System";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.standYardingSystemBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(2, 21);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditHarvest,
            this.repositoryItemComboBoxYarding});
            this.gridControl4.Size = new System.Drawing.Size(353, 157);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            this.gridControl4.Validating += new System.ComponentModel.CancelEventHandler(this.gridControl4_Validating);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId3,
            this.colStandsId7,
            this.colCode,
            this.colDescription,
            this.colPercent,
            this.colCostUnit});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView4.OptionsCustomization.AllowColumnMoving = false;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsCustomization.AllowGroup = false;
            this.gridView4.OptionsCustomization.AllowSort = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView4_RowCellClick);
            this.gridView4.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView4_ValidateRow);
            this.gridView4.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            this.gridView4.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView4_ValidatingEditor_1);
            // 
            // colId3
            // 
            this.colId3.FieldName = "Id";
            this.colId3.Name = "colId3";
            // 
            // colStandsId7
            // 
            this.colStandsId7.FieldName = "StandsId";
            this.colStandsId7.Name = "colStandsId7";
            // 
            // colCode
            // 
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            // 
            // colDescription
            // 
            this.colDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescription.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescription.Caption = "Description";
            this.colDescription.ColumnEdit = this.repositoryItemComboBoxYarding;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 145;
            // 
            // repositoryItemComboBoxYarding
            // 
            this.repositoryItemComboBoxYarding.AutoHeight = false;
            this.repositoryItemComboBoxYarding.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxYarding.Name = "repositoryItemComboBoxYarding";
            // 
            // colPercent
            // 
            this.colPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercent.DisplayFormat.FormatString = "{0:#}";
            this.colPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercent.FieldName = "Percent";
            this.colPercent.Name = "colPercent";
            this.colPercent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Percent", "{0:#}")});
            this.colPercent.Visible = true;
            this.colPercent.VisibleIndex = 1;
            this.colPercent.Width = 45;
            // 
            // colCostUnit
            // 
            this.colCostUnit.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostUnit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostUnit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostUnit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostUnit.Caption = "Cost Unit";
            this.colCostUnit.FieldName = "CostUnit";
            this.colCostUnit.Name = "colCostUnit";
            this.colCostUnit.OptionsColumn.AllowEdit = false;
            this.colCostUnit.OptionsColumn.AllowFocus = false;
            this.colCostUnit.OptionsColumn.ReadOnly = true;
            this.colCostUnit.Visible = true;
            this.colCostUnit.VisibleIndex = 2;
            this.colCostUnit.Width = 51;
            // 
            // repositoryItemLookUpEditHarvest
            // 
            this.repositoryItemLookUpEditHarvest.AutoHeight = false;
            this.repositoryItemLookUpEditHarvest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditHarvest.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")});
            this.repositoryItemLookUpEditHarvest.DataSource = this.harvestBindingSource;
            this.repositoryItemLookUpEditHarvest.DisplayMember = "Description";
            this.repositoryItemLookUpEditHarvest.Name = "repositoryItemLookUpEditHarvest";
            this.repositoryItemLookUpEditHarvest.NullText = "";
            this.repositoryItemLookUpEditHarvest.ValueMember = "Description";
            // 
            // groupControl11
            // 
            this.groupControl11.Controls.Add(this.gridControl3);
            this.groupControl11.Location = new System.Drawing.Point(912, 205);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(224, 205);
            this.groupControl11.TabIndex = 29;
            this.groupControl11.Text = "Permanent Plot Measurements";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.standPermPlotBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl3.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl3.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(2, 21);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(220, 182);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.ColumnPanelRowHeight = 35;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId2,
            this.colStandsId6,
            this.colPermPlotOccasion,
            this.colPermPlotDate,
            this.colPermPlotInterval});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView3.OptionsCustomization.AllowColumnMoving = false;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowSort = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView3_InitNewRow);
            this.gridView3.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView3_ValidateRow);
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // colId2
            // 
            this.colId2.AppearanceHeader.Options.UseTextOptions = true;
            this.colId2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colId2.FieldName = "Id";
            this.colId2.Name = "colId2";
            // 
            // colStandsId6
            // 
            this.colStandsId6.FieldName = "StandsId";
            this.colStandsId6.Name = "colStandsId6";
            // 
            // colPermPlotOccasion
            // 
            this.colPermPlotOccasion.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotOccasion.Caption = "Occa sion";
            this.colPermPlotOccasion.FieldName = "PermPlotOccasion";
            this.colPermPlotOccasion.Name = "colPermPlotOccasion";
            this.colPermPlotOccasion.Visible = true;
            this.colPermPlotOccasion.VisibleIndex = 0;
            this.colPermPlotOccasion.Width = 51;
            // 
            // colPermPlotDate
            // 
            this.colPermPlotDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotDate.Caption = "Date";
            this.colPermPlotDate.FieldName = "PermPlotDate";
            this.colPermPlotDate.Name = "colPermPlotDate";
            this.colPermPlotDate.Visible = true;
            this.colPermPlotDate.VisibleIndex = 1;
            this.colPermPlotDate.Width = 84;
            // 
            // colPermPlotInterval
            // 
            this.colPermPlotInterval.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotInterval.Caption = "Inter val";
            this.colPermPlotInterval.FieldName = "PermPlotInterval";
            this.colPermPlotInterval.Name = "colPermPlotInterval";
            this.colPermPlotInterval.Visible = true;
            this.colPermPlotInterval.VisibleIndex = 2;
            this.colPermPlotInterval.Width = 44;
            // 
            // groupControl10
            // 
            this.groupControl10.Controls.Add(this.gridControl2);
            this.groupControl10.Location = new System.Drawing.Point(779, 205);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(127, 205);
            this.groupControl10.TabIndex = 28;
            this.groupControl10.Text = "Age";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.standAgeBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl2.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl2.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(2, 21);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(123, 182);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl2.Leave += new System.EventHandler(this.gridControl2_Leave);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId1,
            this.colStandsId5,
            this.colAgeCode1,
            this.colAge2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView2_InitNewRow);
            this.gridView2.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridView2_BeforeLeaveRow);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colId1
            // 
            this.colId1.FieldName = "Id";
            this.colId1.Name = "colId1";
            // 
            // colStandsId5
            // 
            this.colStandsId5.FieldName = "StandsId";
            this.colStandsId5.Name = "colStandsId5";
            // 
            // colAgeCode1
            // 
            this.colAgeCode1.Caption = "Code";
            this.colAgeCode1.FieldName = "AgeCode";
            this.colAgeCode1.Name = "colAgeCode1";
            this.colAgeCode1.Visible = true;
            this.colAgeCode1.VisibleIndex = 0;
            this.colAgeCode1.Width = 41;
            // 
            // colAge2
            // 
            this.colAge2.Caption = "Age";
            this.colAge2.FieldName = "Age";
            this.colAge2.Name = "colAge2";
            this.colAge2.Visible = true;
            this.colAge2.VisibleIndex = 1;
            this.colAge2.Width = 37;
            // 
            // standHistoryBindingSource
            // 
            this.standHistoryBindingSource.DataSource = typeof(Project.DAL.StandHistory);
            // 
            // StandAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 608);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl11);
            this.Controls.Add(this.groupControl10);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl8);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "StandAddForm";
            this.Text = "Add Stand";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StandAddForm_FormClosing);
            this.Load += new System.EventHandler(this.StandAddForm_Load);
            this.Shown += new System.EventHandler(this.StandAddForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHaulingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAutoSegmentLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrowthModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonStockedTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonStockedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonTimberedTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonTimberedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHarvestTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.harvestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGrownToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCounty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standYardingSystemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standPermPlotBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standAgeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxYarding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditHarvest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHistoryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex4;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex3;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex2;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex1;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion5;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion4;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion3;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion2;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion1;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.TextEdit tbStripWidth5;
        private DevExpress.XtraEditors.TextEdit tbStripInterval5;
        private DevExpress.XtraEditors.TextEdit tbStripWidth4;
        private DevExpress.XtraEditors.TextEdit tbStripInterval4;
        private DevExpress.XtraEditors.TextEdit tbStripWidth3;
        private DevExpress.XtraEditors.TextEdit tbStripInterval3;
        private DevExpress.XtraEditors.TextEdit tbStripWidth2;
        private DevExpress.XtraEditors.TextEdit tbStripInterval2;
        private DevExpress.XtraEditors.TextEdit tbStripWidth1;
        private DevExpress.XtraEditors.TextEdit tbStripInterval1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup5;
        private DevExpress.XtraEditors.TextEdit tbAreaSide52;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup4;
        private DevExpress.XtraEditors.TextEdit tbAreaSide42;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup3;
        private DevExpress.XtraEditors.TextEdit tbAreaSide32;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup2;
        private DevExpress.XtraEditors.TextEdit tbAreaSide22;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup1;
        private DevExpress.XtraEditors.TextEdit tbAreaSide12;
        private DevExpress.XtraEditors.TextEdit tbAreaSide51;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius5;
        private DevExpress.XtraEditors.TextEdit tbAreaSide41;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius4;
        private DevExpress.XtraEditors.TextEdit tbAreaSide31;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius3;
        private DevExpress.XtraEditors.TextEdit tbAreaSide21;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius2;
        private DevExpress.XtraEditors.TextEdit tbAreaSide11;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius1;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres5;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres4;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres3;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres2;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.TextEdit tbBafLimit5;
        private DevExpress.XtraEditors.TextEdit tbBaf5;
        private DevExpress.XtraEditors.TextEdit tbBafLimit4;
        private DevExpress.XtraEditors.TextEdit tbBaf4;
        private DevExpress.XtraEditors.TextEdit tbBafLimit3;
        private DevExpress.XtraEditors.TextEdit tbBaf3;
        private DevExpress.XtraEditors.TextEdit tbBafLimit2;
        private DevExpress.XtraEditors.TextEdit tbBaf2;
        private DevExpress.XtraEditors.TextEdit tbBafLimit1;
        private DevExpress.XtraEditors.TextEdit tbBaf1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.TextEdit tbAutoSegmentLength;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.ComboBoxEdit cbGrowthModel;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.ComboBoxEdit cbCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.ComboBoxEdit cbPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.ComboBoxEdit cbGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.ComboBoxEdit cbSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.ComboBoxEdit cbSpeciesTable;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btnGrow;
        private DevExpress.XtraEditors.TextEdit tbGrownToDate;
        private DevExpress.XtraEditors.TextEdit tbInputDate;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.TextEdit tbNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.TextEdit tbSection;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit tbRange;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit tbTownship;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit tbStand;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.TextEdit tbTract;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit tbProject;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies11;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies12;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies13;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies21;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies22;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies23;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies31;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies32;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies33;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies41;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies42;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies43;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode1;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode2;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode3;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode4;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode5;
        private System.Windows.Forms.BindingSource speciesBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbState;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbCounty;
        private System.Windows.Forms.BindingSource countyBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbNonStockedTable;
        private System.Windows.Forms.BindingSource nonStockedBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbNonTimberedTable;
        private System.Windows.Forms.BindingSource nonTimberedBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbHarvestTable;
        private System.Windows.Forms.BindingSource harvestBindingSource;
        private System.Windows.Forms.BindingSource haulingBindingSource;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colHaulingID;
        private DevExpress.XtraGrid.Columns.GridColumn colDestination;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadSize;
        private DevExpress.XtraGrid.Columns.GridColumn colMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colRoundTripMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadUnloadHours;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcCostDollarPerMbf;
        private System.Windows.Forms.BindingSource standYardingSystemBindingSource;
        private System.Windows.Forms.BindingSource standPermPlotBindingSource;
        private System.Windows.Forms.BindingSource standAgeBindingSource;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colId3;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId7;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditHarvest;
        private DevExpress.XtraGrid.Columns.GridColumn colPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnit;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colId2;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId6;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotOccasion;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotInterval;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId1;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId5;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colAge2;
        private System.Windows.Forms.BindingSource standHistoryBindingSource;
        private System.Windows.Forms.BindingSource standHaulingBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxYarding;
    }
}