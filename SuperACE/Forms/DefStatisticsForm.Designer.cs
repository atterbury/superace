﻿namespace SuperACE
{
    partial class DefStatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tbPlotReq3 = new DevExpress.XtraEditors.TextEdit();
            this.tbPlotReq2 = new DevExpress.XtraEditors.TextEdit();
            this.tbPlotReq1 = new DevExpress.XtraEditors.TextEdit();
            this.tbConfLevel = new DevExpress.XtraEditors.TextEdit();
            this.tbStdDev = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbConfLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStdDev.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barLargeButtonItem1,
            this.barLargeButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Save";
            this.barLargeButtonItem1.Id = 0;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Caption = "Cancel";
            this.barLargeButtonItem2.Id = 1;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(404, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 256);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(404, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 232);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(404, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 232);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl7.Location = new System.Drawing.Point(217, 118);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(175, 26);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "times out of 100 the volume will be within the Sampling Error %";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(37, 127);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(86, 13);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Confidence Level:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(22, 92);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(101, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Standard Deviations:";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(22, 28);
            this.radioGroup1.MenuManager = this.barManager1;
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Columns = 2;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Infinite", true, 'Y'),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Finite", true, 'N')});
            this.radioGroup1.Size = new System.Drawing.Size(177, 47);
            this.radioGroup1.TabIndex = 11;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.tbPlotReq3);
            this.groupControl1.Controls.Add(this.tbPlotReq2);
            this.groupControl1.Controls.Add(this.tbPlotReq1);
            this.groupControl1.Location = new System.Drawing.Point(22, 170);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(276, 71);
            this.groupControl1.TabIndex = 17;
            this.groupControl1.Text = "Number of Plots Required for the Sampling Error %";
            // 
            // tbPlotReq3
            // 
            this.tbPlotReq3.EditValue = ((short)(15));
            this.tbPlotReq3.Location = new System.Drawing.Point(221, 36);
            this.tbPlotReq3.MenuManager = this.barManager1;
            this.tbPlotReq3.Name = "tbPlotReq3";
            this.tbPlotReq3.Size = new System.Drawing.Size(36, 20);
            this.tbPlotReq3.TabIndex = 13;
            this.tbPlotReq3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbPlotReq2
            // 
            this.tbPlotReq2.EditValue = ((short)(10));
            this.tbPlotReq2.Location = new System.Drawing.Point(118, 36);
            this.tbPlotReq2.MenuManager = this.barManager1;
            this.tbPlotReq2.Name = "tbPlotReq2";
            this.tbPlotReq2.Size = new System.Drawing.Size(36, 20);
            this.tbPlotReq2.TabIndex = 12;
            this.tbPlotReq2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbPlotReq1
            // 
            this.tbPlotReq1.EditValue = ((short)(5));
            this.tbPlotReq1.Location = new System.Drawing.Point(15, 36);
            this.tbPlotReq1.MenuManager = this.barManager1;
            this.tbPlotReq1.Name = "tbPlotReq1";
            this.tbPlotReq1.Size = new System.Drawing.Size(36, 20);
            this.tbPlotReq1.TabIndex = 11;
            this.tbPlotReq1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbConfLevel
            // 
            this.tbConfLevel.EditValue = "";
            this.tbConfLevel.EnterMoveNextControl = true;
            this.tbConfLevel.Location = new System.Drawing.Point(142, 124);
            this.tbConfLevel.MenuManager = this.barManager1;
            this.tbConfLevel.Name = "tbConfLevel";
            this.tbConfLevel.Size = new System.Drawing.Size(57, 20);
            this.tbConfLevel.TabIndex = 27;
            this.tbConfLevel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbConfLevel.Validating += new System.ComponentModel.CancelEventHandler(this.tbConfLevel_Validating);
            // 
            // tbStdDev
            // 
            this.tbStdDev.EditValue = "";
            this.tbStdDev.EnterMoveNextControl = true;
            this.tbStdDev.Location = new System.Drawing.Point(142, 89);
            this.tbStdDev.MenuManager = this.barManager1;
            this.tbStdDev.Name = "tbStdDev";
            this.tbStdDev.Size = new System.Drawing.Size(57, 20);
            this.tbStdDev.TabIndex = 28;
            this.tbStdDev.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStdDev.Validating += new System.ComponentModel.CancelEventHandler(this.tbStdDev_Validating);
            // 
            // DefStatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 256);
            this.Controls.Add(this.tbStdDev);
            this.Controls.Add(this.tbConfLevel);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.radioGroup1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefStatisticsForm";
            this.Text = "Calculations";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefStatisticsForm_FormClosing);
            this.Load += new System.EventHandler(this.DefStatisticsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotReq1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbConfLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStdDev.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit tbPlotReq3;
        private DevExpress.XtraEditors.TextEdit tbPlotReq2;
        private DevExpress.XtraEditors.TextEdit tbPlotReq1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.TextEdit tbConfLevel;
        private DevExpress.XtraEditors.TextEdit tbStdDev;
    }
}