﻿namespace SuperACE
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::SuperACE.SplashScreen1), true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItemFile = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem80 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem81 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem78 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem28 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem64 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem29 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem43 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem87 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemProject = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem59 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem58 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemStands = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem60 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem61 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem62 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemOwnership = new DevExpress.XtraBars.BarSubItem();
            this.barSubItemReports = new DevExpress.XtraBars.BarSubItem();
            this.barSubItemTables = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItemSpeciesTable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSortTable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGradeTable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPriceTable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCostTable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem39 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem40 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem41 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem42 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem44 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem55 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem56 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem45 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem46 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem47 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem48 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem50 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem51 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem49 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem52 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem53 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem54 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem37 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem38 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem65 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem66 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem30 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem69 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemDefaults = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemHelp = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem73 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem83 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem67 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem75 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem76 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem68 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem77 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem31 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem63 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem74 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem79 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem82 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem84 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem85 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem86 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem88 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem89 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItemFirst = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrev = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemNext = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLast = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem70 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem71 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem72 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem57 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.btnLimitingDistance = new DevExpress.XtraBars.BarButtonItem();
            this.btnTreeMeasurementsRelaskop = new DevExpress.XtraBars.BarButtonItem();
            this.btnTreeMeasurementsRD100 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemFileExit = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItemProjectEditProject = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemProjectNewProject = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemProjectSelectProject = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemFileExit = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem7 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem8 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem9 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.btnTotalHeight = new DevExpress.XtraBars.BarButtonItem();
            this.btnFormFactorWithoutDistance = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPageHome = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlStands = new DevExpress.XtraGrid.GridControl();
            this.standsViewBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewStands = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStandsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjectsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTractGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttachments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTractName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTownship = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetGeographicAcres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOfStandData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrownToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMajorAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHarvest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMajorSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasalAreaPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQmDbh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTonsPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCcfPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMbfPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlots = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabNavigationPageStandMaster = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.standYardingSystemBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBoxYarding = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditHarvest = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.harvestBindingSource = new System.Windows.Forms.BindingSource();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.standPermPlotBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotOccasion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermPlotInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.standAgeBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditAge = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.standHaulingBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHaulingId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDestination = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colAvgLoadSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoundTripMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinutes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgLoadMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadUnloadHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcCostDollarPerMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.haulingBindingSource = new System.Windows.Forms.BindingSource();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.tbSiteIndex4 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex3 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex2 = new DevExpress.XtraEditors.TextEdit();
            this.tbSiteIndex1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.tbSpecies11 = new DevExpress.XtraEditors.LookUpEdit();
            this.speciesBindingSource = new System.Windows.Forms.BindingSource();
            this.tbSpecies12 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies13 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies21 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies22 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies23 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies31 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies32 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies33 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies41 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies42 = new DevExpress.XtraEditors.LookUpEdit();
            this.tbSpecies43 = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.tbStripExpansion5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripExpansion1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.tbStripWidth5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval5 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval4 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval3 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval2 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripWidth1 = new DevExpress.XtraEditors.TextEdit();
            this.tbStripInterval1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.tbAreaBlowup5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide52 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide42 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide32 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide22 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaBlowup1 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide12 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide51 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide41 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide31 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide21 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaSide11 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaPlotRadius1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.tbAreaPlotAcres5 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaPlotAcres4 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaPlotAcres3 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaPlotAcres2 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAreaPlotAcres1 = new DevExpress.XtraEditors.TextEdit();
            this.tbAreaCode1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.tbBafLimit5 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf5 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit4 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf4 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit3 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf3 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit2 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf2 = new DevExpress.XtraEditors.TextEdit();
            this.tbBafLimit1 = new DevExpress.XtraEditors.TextEdit();
            this.tbBaf1 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.cbSource = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.tbAutoSegmentLength = new DevExpress.XtraEditors.TextEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.cbGrowthModel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.cbCostTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.cbPriceTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.cbGradeTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.cbSortTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.cbSpeciesTable = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbNonStockedTable = new DevExpress.XtraEditors.LookUpEdit();
            this.nonStockedBindingSource = new System.Windows.Forms.BindingSource();
            this.cbNonTimberedTable = new DevExpress.XtraEditors.LookUpEdit();
            this.nonTimberedBindingSource = new System.Windows.Forms.BindingSource();
            this.cbHarvestTable = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlots = new DevExpress.XtraEditors.TextEdit();
            this.btnGrow = new DevExpress.XtraEditors.SimpleButton();
            this.tbGrownToDate = new DevExpress.XtraEditors.TextEdit();
            this.tbInputDate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.tbNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.tbSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.tbRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.tbTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.tbStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.tbTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.tbProject = new DevExpress.XtraEditors.TextEdit();
            this.cbState = new DevExpress.XtraEditors.LookUpEdit();
            this.stateBindingSource = new System.Windows.Forms.BindingSource();
            this.cbCounty = new DevExpress.XtraEditors.LookUpEdit();
            this.stateCountyBindingSource = new System.Windows.Forms.BindingSource();
            this.tabNavigationPagePlotLocation = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlPlots = new DevExpress.XtraGrid.GridControl();
            this.plotsBindingSource = new System.Windows.Forms.BindingSource();
            this.advBandedGridViewPlots = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colStandsId1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCruiserID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotsId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAttachments1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUserPlotNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPlotDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCruiser = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBafInAt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbBafInAt = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCruFlag = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAddFlag = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRefFlag = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDWFlag = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSlope = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAspect = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbAspect = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.aspectsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colHabitat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbHabitat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.habitatBindingSource = new System.Windows.Forms.BindingSource();
            this.colSpecies = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbSpecies = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colEnvironmental = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbEnvironmental = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.environmentsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colX = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colZ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colBasalAreaAcre = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreesPerAcre1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetCfAcre = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNetBfAcre = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colNotes = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tbPlotsCostTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsPriceTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsGradeTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsSortTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsSpeciesTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tbPlotsTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.tabNavigationPageTreeInput = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlTrees = new DevExpress.XtraGrid.GridControl();
            this.treesBindingSource = new System.Windows.Forms.BindingSource();
            this.advBandedGridViewTrees = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTreesId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStandsId2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotsId1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemUpper = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTreeNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPlotFactorInput = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGroupPlot = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGroupTree = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAge = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcTotalHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCalcTotalHtUsedYN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPFValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colAgeCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSpeciesAbbreviation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeStatusDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTreeCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDbh = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFormPoint = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colReportedFormFactor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTopDiameterFractionCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBoleHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItem3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalHeight = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCrownPositionDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCrownRatioDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVigorDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDamageDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUserDefinedDisplayCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSort12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrade12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLength12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtLD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtDD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtLD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCuFtDD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colBdFtPD12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPFType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemFormPoint = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemFormFactor = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGrade = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.tbTreesCostTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesPriceTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesGradeTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesSortTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesSpeciesTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.tbTreesTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.tabNavigationPageTreeEdit = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlSegments = new SuperACE.Grid.CustomGridControl();
            this.segmentsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewSegments = new SuperACE.Grid.CustomGridView();
            this.colTreeSegmentsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreesId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupPlotNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupTreeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSegmentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBdFtLD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBdFtDD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCuFtLD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCuFtDD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGradeCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScribnerNetVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCubicNetVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCalcTopDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcButtDia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlotId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTreeBasalArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcAccumulatedLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBdFtPD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlotNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTreeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlotFactorInput1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeStatusDisplayCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDbh1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormPoint1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBoleHeight1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHeight1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVigor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDamage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalcTotalHtUsedYN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPFType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tbSegCostTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegPriceTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegGradeTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegSortTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegSpeciesTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.tbSegTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.tabNavigationPageStandInput = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlStandInputOrig = new DevExpress.XtraGrid.GridControl();
            this.standInputBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewStandInputOrig = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStandsId4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteIndex1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigTotalHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigStocking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigBasalArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigD4H = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigLogsPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigGrossCuFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigNetCuFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigGrossBdFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigNetBdFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigTotalCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.origTotalMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.btnStandInputGrow = new DevExpress.XtraEditors.SimpleButton();
            this.tbStandInputGrownToDate = new DevExpress.XtraEditors.TextEdit();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlStandInputGrown = new DevExpress.XtraGrid.GridControl();
            this.gridViewStandInputGrown = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUpdtId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtStandsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtSiteIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtDbh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtStocking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtTreesPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtLogsPerAcre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtBasalArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtGrossCuFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtNetCuFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtGrossBdFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtNetBdFt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtTotalHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtBirthYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtTotalCcf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdtTotalMbf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.tbStandInputCostTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputPriceTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputGradeTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputSortTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputSpeciesTable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputNetAcres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputSection = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputRange = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputTownship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputStand = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.tbStandInputTract = new DevExpress.XtraEditors.TextEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.sourceBindingSource = new System.Windows.Forms.BindingSource();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            this.imageList1 = new System.Windows.Forms.ImageList();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPageHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standsViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStands)).BeginInit();
            this.tabNavigationPageStandMaster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standYardingSystemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxYarding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditHarvest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.harvestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standPermPlotBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standAgeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHaulingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAutoSegmentLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrowthModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonStockedTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonStockedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonTimberedTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonTimberedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHarvestTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlots.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGrownToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCounty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateCountyBindingSource)).BeginInit();
            this.tabNavigationPagePlotLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlots)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewPlots)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBafInAt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAspect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHabitat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.habitatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpecies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEnvironmental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.environmentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsTract.Properties)).BeginInit();
            this.tabNavigationPageTreeInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesTract.Properties)).BeginInit();
            this.tabNavigationPageTreeEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSegments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.segmentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSegments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegTract.Properties)).BeginInit();
            this.tabNavigationPageStandInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStandInputOrig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standInputBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStandInputOrig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputGrownToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStandInputGrown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStandInputGrown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputCostTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputPriceTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputGradeTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSortTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSpeciesTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputNetAcres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputTownship.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputStand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputTract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // splashScreenManager1
            // 
            splashScreenManager1.ClosingDelay = 500;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItemFile,
            this.barSubItemProject,
            this.barSubItemStands,
            this.barSubItemOwnership,
            this.barSubItemReports,
            this.barSubItemTables,
            this.barSubItemDefaults,
            this.barSubItemHelp,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barSubItemFileExit,
            this.barButtonItemFirst,
            this.barButtonItemPrev,
            this.barButtonItemNext,
            this.barButtonItemLast,
            this.barSubItem1,
            this.barStaticItemProjectEditProject,
            this.barStaticItemProjectNewProject,
            this.barStaticItemProjectSelectProject,
            this.barStaticItemFileExit,
            this.barStaticItem5,
            this.barStaticItem6,
            this.barStaticItem7,
            this.barStaticItem8,
            this.barStaticItem9,
            this.barButtonItem6,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItemSpeciesTable,
            this.barButtonItemSortTable,
            this.barButtonItemGradeTable,
            this.barButtonItemPriceTable,
            this.barButtonItemCostTable,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barButtonItem17,
            this.barButtonItem7,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barSubItem2,
            this.btnLimitingDistance,
            this.btnTreeMeasurementsRelaskop,
            this.btnTreeMeasurementsRD100,
            this.btnTotalHeight,
            this.btnFormFactorWithoutDistance,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barButtonItem27,
            this.barSubItem3,
            this.barButtonItem28,
            this.barButtonItem29,
            this.barSubItem4,
            this.barButtonItem31,
            this.barButtonItem32,
            this.barSubItem5,
            this.barButtonItem33,
            this.barButtonItem34,
            this.barButtonItem36,
            this.barButtonItem37,
            this.barButtonItem38,
            this.barButtonItem39,
            this.barButtonItem40,
            this.barButtonItem41,
            this.barButtonItem42,
            this.barButtonItem43,
            this.barButtonItem44,
            this.barButtonItem45,
            this.barButtonItem46,
            this.barButtonItem47,
            this.barButtonItem48,
            this.barButtonItem49,
            this.barButtonItem50,
            this.barButtonItem51,
            this.barButtonItem52,
            this.barButtonItem53,
            this.barButtonItem54,
            this.barButtonItem55,
            this.barButtonItem56,
            this.barButtonItem57,
            this.barButtonItem58,
            this.barButtonItem59,
            this.barButtonItem60,
            this.barButtonItem61,
            this.barButtonItem62,
            this.barButtonItem63,
            this.barButtonItem65,
            this.barButtonItem66,
            this.barButtonItem67,
            this.barButtonItem64,
            this.barButtonItem70,
            this.barButtonItem71,
            this.barButtonItem72,
            this.barButtonItem75,
            this.barButtonItem76,
            this.barButtonItem77,
            this.barButtonItem68,
            this.barButtonItem30,
            this.barButtonItem69,
            this.barButtonItem73,
            this.barButtonItem74,
            this.barButtonItem79,
            this.barButtonItem80,
            this.barButtonItem78,
            this.barButtonItem81,
            this.barButtonItem82,
            this.barButtonItem83,
            this.barButtonItem84,
            this.barButtonItem85,
            this.barButtonItem86,
            this.barButtonItem87,
            this.barButtonItem88,
            this.barButtonItem35,
            this.barButtonItem89});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 150;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemProject),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemStands),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemOwnership),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemReports),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemTables),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemDefaults),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemHelp),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem4)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItemFile
            // 
            this.barSubItemFile.Caption = "File";
            this.barSubItemFile.Id = 0;
            this.barSubItemFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem80),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem81),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem78),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem87, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8, true)});
            this.barSubItemFile.Name = "barSubItemFile";
            // 
            // barButtonItem80
            // 
            this.barButtonItem80.Caption = "Cruise To Inventory (Project)";
            this.barButtonItem80.Id = 137;
            this.barButtonItem80.Name = "barButtonItem80";
            this.barButtonItem80.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem80_ItemClick);
            // 
            // barButtonItem81
            // 
            this.barButtonItem81.Caption = "Cruise To Inventory (Selected Stands)";
            this.barButtonItem81.Id = 139;
            this.barButtonItem81.Name = "barButtonItem81";
            this.barButtonItem81.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem81_ItemClick);
            // 
            // barButtonItem78
            // 
            this.barButtonItem78.Caption = "Stand Input Errors";
            this.barButtonItem78.Id = 138;
            this.barButtonItem78.Name = "barButtonItem78";
            this.barButtonItem78.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem78_ItemClick_1);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Import";
            this.barSubItem3.Id = 74;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem28),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem64),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem29, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem43)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem28
            // 
            this.barButtonItem28.Caption = "SuperACE 08";
            this.barButtonItem28.Id = 75;
            this.barButtonItem28.Name = "barButtonItem28";
            this.barButtonItem28.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem28_ItemClick);
            // 
            // barButtonItem64
            // 
            this.barButtonItem64.Caption = "SuperACE 08 Db Location";
            this.barButtonItem64.Id = 118;
            this.barButtonItem64.Name = "barButtonItem64";
            this.barButtonItem64.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem64_ItemClick);
            // 
            // barButtonItem29
            // 
            this.barButtonItem29.Caption = "PocketEASY from Device";
            this.barButtonItem29.Id = 76;
            this.barButtonItem29.Name = "barButtonItem29";
            this.barButtonItem29.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem29_ItemClick);
            // 
            // barButtonItem43
            // 
            this.barButtonItem43.Caption = "PocketEASY from PC";
            this.barButtonItem43.Id = 93;
            this.barButtonItem43.Name = "barButtonItem43";
            this.barButtonItem43.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem43_ItemClick);
            // 
            // barButtonItem87
            // 
            this.barButtonItem87.Caption = "Open Excel Worksheet";
            this.barButtonItem87.Id = 145;
            this.barButtonItem87.Name = "barButtonItem87";
            this.barButtonItem87.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem87_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Exit";
            this.barButtonItem8.Id = 38;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barSubItemProject
            // 
            this.barSubItemProject.Caption = "Project";
            this.barSubItemProject.Id = 1;
            this.barSubItemProject.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem59, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem58, true)});
            this.barSubItemProject.Name = "barSubItemProject";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Select Project";
            this.barButtonItem9.Id = 39;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Edit Project";
            this.barButtonItem10.Id = 40;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "New Project";
            this.barButtonItem11.Id = 41;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // barButtonItem59
            // 
            this.barButtonItem59.Caption = "Copy Project";
            this.barButtonItem59.Id = 109;
            this.barButtonItem59.Name = "barButtonItem59";
            this.barButtonItem59.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem59_ItemClick);
            // 
            // barButtonItem58
            // 
            this.barButtonItem58.Caption = "Delete Project";
            this.barButtonItem58.Id = 108;
            this.barButtonItem58.Name = "barButtonItem58";
            this.barButtonItem58.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem58_ItemClick);
            // 
            // barSubItemStands
            // 
            this.barSubItemStands.Caption = "Stands";
            this.barSubItemStands.Id = 2;
            this.barSubItemStands.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem24, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem60, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem61, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem62)});
            this.barSubItemStands.Name = "barSubItemStands";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "New Stand";
            this.barButtonItem23.Id = 69;
            this.barButtonItem23.Name = "barButtonItem23";
            this.barButtonItem23.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem23_ItemClick);
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "Delete Stand";
            this.barButtonItem24.Id = 70;
            this.barButtonItem24.Name = "barButtonItem24";
            this.barButtonItem24.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem24_ItemClick);
            // 
            // barButtonItem60
            // 
            this.barButtonItem60.Caption = "Copy Stand(s)";
            this.barButtonItem60.Id = 110;
            this.barButtonItem60.Name = "barButtonItem60";
            this.barButtonItem60.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem60_ItemClick);
            // 
            // barButtonItem61
            // 
            this.barButtonItem61.Caption = "Copy Plot(s)";
            this.barButtonItem61.Id = 111;
            this.barButtonItem61.Name = "barButtonItem61";
            this.barButtonItem61.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem61_ItemClick);
            // 
            // barButtonItem62
            // 
            this.barButtonItem62.Caption = "Delete Plot(s)";
            this.barButtonItem62.Id = 112;
            this.barButtonItem62.Name = "barButtonItem62";
            this.barButtonItem62.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem62_ItemClick);
            // 
            // barSubItemOwnership
            // 
            this.barSubItemOwnership.Caption = "Ownership";
            this.barSubItemOwnership.Id = 3;
            this.barSubItemOwnership.Name = "barSubItemOwnership";
            // 
            // barSubItemReports
            // 
            this.barSubItemReports.Caption = "Reports";
            this.barSubItemReports.Id = 4;
            this.barSubItemReports.Name = "barSubItemReports";
            this.barSubItemReports.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItemReports_ItemClick);
            // 
            // barSubItemTables
            // 
            this.barSubItemTables.Caption = "Tables";
            this.barSubItemTables.Id = 5;
            this.barSubItemTables.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSpeciesTable),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSortTable),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemGradeTable),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPriceTable),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCostTable),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem34, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem36),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem37),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem38),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem65, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem66),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem30, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem69)});
            this.barSubItemTables.Name = "barSubItemTables";
            // 
            // barButtonItemSpeciesTable
            // 
            this.barButtonItemSpeciesTable.Caption = "Species";
            this.barButtonItemSpeciesTable.Id = 43;
            this.barButtonItemSpeciesTable.Name = "barButtonItemSpeciesTable";
            this.barButtonItemSpeciesTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSpeciesTable_ItemClick);
            // 
            // barButtonItemSortTable
            // 
            this.barButtonItemSortTable.Caption = "Sort";
            this.barButtonItemSortTable.Id = 44;
            this.barButtonItemSortTable.Name = "barButtonItemSortTable";
            this.barButtonItemSortTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSortTable_ItemClick);
            // 
            // barButtonItemGradeTable
            // 
            this.barButtonItemGradeTable.Caption = "Grade";
            this.barButtonItemGradeTable.Id = 45;
            this.barButtonItemGradeTable.Name = "barButtonItemGradeTable";
            this.barButtonItemGradeTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemGradeTable_ItemClick);
            // 
            // barButtonItemPriceTable
            // 
            this.barButtonItemPriceTable.Caption = "Price";
            this.barButtonItemPriceTable.Id = 46;
            this.barButtonItemPriceTable.Name = "barButtonItemPriceTable";
            this.barButtonItemPriceTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPriceTable_ItemClick);
            // 
            // barButtonItemCostTable
            // 
            this.barButtonItemCostTable.Caption = "Cost";
            this.barButtonItemCostTable.Id = 47;
            this.barButtonItemCostTable.Name = "barButtonItemCostTable";
            this.barButtonItemCostTable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCostTable_ItemClick);
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "Yield/Normality";
            this.barButtonItem21.Id = 57;
            this.barButtonItem21.Name = "barButtonItem21";
            this.barButtonItem21.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem21_ItemClick);
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Classifications";
            this.barSubItem5.Id = 81;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem39),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem40),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem41),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem42),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem44),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem35),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem55),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem56),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem45),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem46),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem47),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem48),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem50),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem51),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem49),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem52),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem53),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem54)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem39
            // 
            this.barButtonItem39.Caption = "Aspect";
            this.barButtonItem39.Id = 89;
            this.barButtonItem39.Name = "barButtonItem39";
            this.barButtonItem39.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem39_ItemClick);
            // 
            // barButtonItem40
            // 
            this.barButtonItem40.Caption = "Component";
            this.barButtonItem40.Id = 90;
            this.barButtonItem40.Name = "barButtonItem40";
            this.barButtonItem40.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem40_ItemClick);
            // 
            // barButtonItem41
            // 
            this.barButtonItem41.Caption = "Environment";
            this.barButtonItem41.Id = 91;
            this.barButtonItem41.Name = "barButtonItem41";
            this.barButtonItem41.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem41_ItemClick);
            // 
            // barButtonItem42
            // 
            this.barButtonItem42.Caption = "Form";
            this.barButtonItem42.Id = 92;
            this.barButtonItem42.Name = "barButtonItem42";
            this.barButtonItem42.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem42_ItemClick);
            // 
            // barButtonItem44
            // 
            this.barButtonItem44.Caption = "Harvest";
            this.barButtonItem44.Id = 94;
            this.barButtonItem44.Name = "barButtonItem44";
            this.barButtonItem44.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem44_ItemClick);
            // 
            // barButtonItem35
            // 
            this.barButtonItem35.Caption = "History/Planning";
            this.barButtonItem35.Id = 147;
            this.barButtonItem35.Name = "barButtonItem35";
            this.barButtonItem35.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem35_ItemClick_1);
            // 
            // barButtonItem55
            // 
            this.barButtonItem55.Caption = "Non Stocked";
            this.barButtonItem55.Id = 105;
            this.barButtonItem55.Name = "barButtonItem55";
            this.barButtonItem55.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem55_ItemClick);
            // 
            // barButtonItem56
            // 
            this.barButtonItem56.Caption = "Non Timbered";
            this.barButtonItem56.Id = 106;
            this.barButtonItem56.Name = "barButtonItem56";
            this.barButtonItem56.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem56_ItemClick);
            // 
            // barButtonItem45
            // 
            this.barButtonItem45.Caption = "Roads";
            this.barButtonItem45.Id = 95;
            this.barButtonItem45.Name = "barButtonItem45";
            this.barButtonItem45.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem45_ItemClick);
            // 
            // barButtonItem46
            // 
            this.barButtonItem46.Caption = "Seed Zone";
            this.barButtonItem46.Id = 96;
            this.barButtonItem46.Name = "barButtonItem46";
            this.barButtonItem46.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem46_ItemClick);
            // 
            // barButtonItem47
            // 
            this.barButtonItem47.Caption = "Slash";
            this.barButtonItem47.Id = 97;
            this.barButtonItem47.Name = "barButtonItem47";
            this.barButtonItem47.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem47_ItemClick);
            // 
            // barButtonItem48
            // 
            this.barButtonItem48.Caption = "Soils";
            this.barButtonItem48.Id = 98;
            this.barButtonItem48.Name = "barButtonItem48";
            this.barButtonItem48.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem48_ItemClick);
            // 
            // barButtonItem50
            // 
            this.barButtonItem50.Caption = "Streams";
            this.barButtonItem50.Id = 100;
            this.barButtonItem50.Name = "barButtonItem50";
            this.barButtonItem50.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem50_ItemClick);
            // 
            // barButtonItem51
            // 
            this.barButtonItem51.Caption = "Treatment";
            this.barButtonItem51.Id = 101;
            this.barButtonItem51.Name = "barButtonItem51";
            this.barButtonItem51.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem51_ItemClick);
            // 
            // barButtonItem49
            // 
            this.barButtonItem49.Caption = "Tree Status";
            this.barButtonItem49.Id = 99;
            this.barButtonItem49.Name = "barButtonItem49";
            this.barButtonItem49.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem49_ItemClick);
            // 
            // barButtonItem52
            // 
            this.barButtonItem52.Caption = "Tree Source";
            this.barButtonItem52.Id = 102;
            this.barButtonItem52.Name = "barButtonItem52";
            this.barButtonItem52.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem52_ItemClick);
            // 
            // barButtonItem53
            // 
            this.barButtonItem53.Caption = "Vegetation";
            this.barButtonItem53.Id = 103;
            this.barButtonItem53.Name = "barButtonItem53";
            this.barButtonItem53.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem53_ItemClick);
            // 
            // barButtonItem54
            // 
            this.barButtonItem54.Caption = "Wood Type";
            this.barButtonItem54.Id = 104;
            this.barButtonItem54.Name = "barButtonItem54";
            this.barButtonItem54.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem54_ItemClick);
            // 
            // barButtonItem34
            // 
            this.barButtonItem34.Caption = "Crown Position";
            this.barButtonItem34.Id = 84;
            this.barButtonItem34.Name = "barButtonItem34";
            this.barButtonItem34.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem34_ItemClick);
            // 
            // barButtonItem36
            // 
            this.barButtonItem36.Caption = "Vigor";
            this.barButtonItem36.Id = 86;
            this.barButtonItem36.Name = "barButtonItem36";
            this.barButtonItem36.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem36_ItemClick);
            // 
            // barButtonItem37
            // 
            this.barButtonItem37.Caption = "Damage";
            this.barButtonItem37.Id = 87;
            this.barButtonItem37.Name = "barButtonItem37";
            this.barButtonItem37.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem37_ItemClick);
            // 
            // barButtonItem38
            // 
            this.barButtonItem38.Caption = "User Defined";
            this.barButtonItem38.Id = 88;
            this.barButtonItem38.Name = "barButtonItem38";
            this.barButtonItem38.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem38_ItemClick);
            // 
            // barButtonItem65
            // 
            this.barButtonItem65.Caption = "Poles";
            this.barButtonItem65.Id = 115;
            this.barButtonItem65.Name = "barButtonItem65";
            this.barButtonItem65.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem65_ItemClick);
            // 
            // barButtonItem66
            // 
            this.barButtonItem66.Caption = "Piling";
            this.barButtonItem66.Id = 116;
            this.barButtonItem66.Name = "barButtonItem66";
            this.barButtonItem66.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem66_ItemClick);
            // 
            // barButtonItem30
            // 
            this.barButtonItem30.Caption = "Count With Dbh";
            this.barButtonItem30.Id = 131;
            this.barButtonItem30.Name = "barButtonItem30";
            this.barButtonItem30.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem30_ItemClick_1);
            // 
            // barButtonItem69
            // 
            this.barButtonItem69.Caption = "Count Avgs";
            this.barButtonItem69.Id = 132;
            this.barButtonItem69.Name = "barButtonItem69";
            this.barButtonItem69.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem69_ItemClick_1);
            // 
            // barSubItemDefaults
            // 
            this.barSubItemDefaults.Caption = "Defaults";
            this.barSubItemDefaults.Id = 6;
            this.barSubItemDefaults.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem18, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem20)});
            this.barSubItemDefaults.Name = "barSubItemDefaults";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Tree Input Fields";
            this.barButtonItem7.Id = 53;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "Report Format";
            this.barButtonItem18.Id = 54;
            this.barButtonItem18.Name = "barButtonItem18";
            this.barButtonItem18.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem18_ItemClick);
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "Calculations";
            this.barButtonItem19.Id = 55;
            this.barButtonItem19.Name = "barButtonItem19";
            this.barButtonItem19.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem19_ItemClick);
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "Statistics";
            this.barButtonItem20.Id = 56;
            this.barButtonItem20.Name = "barButtonItem20";
            this.barButtonItem20.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem20_ItemClick);
            // 
            // barSubItemHelp
            // 
            this.barSubItemHelp.Caption = "Help";
            this.barSubItemHelp.Id = 7;
            this.barSubItemHelp.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem22)});
            this.barSubItemHelp.Name = "barSubItemHelp";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Check For Update";
            this.barButtonItem22.Id = 60;
            this.barButtonItem22.Name = "barButtonItem22";
            this.barButtonItem22.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem22_ItemClick);
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Testing";
            this.barSubItem4.Id = 77;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem73),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem83),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem67),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem75, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem76),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem68, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem77),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem31, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem32),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem63, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem74, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem79, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem82),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem84, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem85),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem86),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem88),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem89)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barButtonItem73
            // 
            this.barButtonItem73.Caption = "Init TreeType in TreeSegments";
            this.barButtonItem73.Id = 133;
            this.barButtonItem73.Name = "barButtonItem73";
            this.barButtonItem73.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem73_ItemClick);
            // 
            // barButtonItem83
            // 
            this.barButtonItem83.Caption = "Set PFType for Trees and Segments";
            this.barButtonItem83.Id = 141;
            this.barButtonItem83.Name = "barButtonItem83";
            this.barButtonItem83.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem83_ItemClick);
            // 
            // barButtonItem67
            // 
            this.barButtonItem67.Caption = "Update Calcs For Trees (Select)";
            this.barButtonItem67.Id = 117;
            this.barButtonItem67.Name = "barButtonItem67";
            this.barButtonItem67.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem67_ItemClick);
            // 
            // barButtonItem75
            // 
            this.barButtonItem75.Caption = "Populate CountWithDia";
            this.barButtonItem75.Id = 127;
            this.barButtonItem75.Name = "barButtonItem75";
            this.barButtonItem75.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem75_ItemClick);
            // 
            // barButtonItem76
            // 
            this.barButtonItem76.Caption = "Populate AdjCruisePlots";
            this.barButtonItem76.Id = 128;
            this.barButtonItem76.Name = "barButtonItem76";
            this.barButtonItem76.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem76_ItemClick);
            // 
            // barButtonItem68
            // 
            this.barButtonItem68.Caption = "Reset Count Records";
            this.barButtonItem68.Id = 130;
            this.barButtonItem68.Name = "barButtonItem68";
            this.barButtonItem68.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem68_ItemClick_1);
            // 
            // barButtonItem77
            // 
            this.barButtonItem77.Caption = "Process Count Records";
            this.barButtonItem77.Id = 129;
            this.barButtonItem77.Name = "barButtonItem77";
            this.barButtonItem77.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem77_ItemClick);
            // 
            // barButtonItem31
            // 
            this.barButtonItem31.Caption = "Update Plots for Stand";
            this.barButtonItem31.Id = 79;
            this.barButtonItem31.Name = "barButtonItem31";
            this.barButtonItem31.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem31_ItemClick);
            // 
            // barButtonItem32
            // 
            this.barButtonItem32.Caption = "Update Stand Totals";
            this.barButtonItem32.Id = 80;
            this.barButtonItem32.Name = "barButtonItem32";
            this.barButtonItem32.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem32_ItemClick);
            // 
            // barButtonItem63
            // 
            this.barButtonItem63.Caption = "Existing Project Tables";
            this.barButtonItem63.Id = 113;
            this.barButtonItem63.Name = "barButtonItem63";
            this.barButtonItem63.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem63_ItemClick);
            // 
            // barButtonItem74
            // 
            this.barButtonItem74.Caption = "Trim Species and Status";
            this.barButtonItem74.Id = 134;
            this.barButtonItem74.Name = "barButtonItem74";
            this.barButtonItem74.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem74_ItemClick);
            // 
            // barButtonItem79
            // 
            this.barButtonItem79.Caption = "Interpolation Test";
            this.barButtonItem79.Id = 136;
            this.barButtonItem79.Name = "barButtonItem79";
            this.barButtonItem79.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem79_ItemClick);
            // 
            // barButtonItem82
            // 
            this.barButtonItem82.Caption = "Convert CL";
            this.barButtonItem82.Id = 140;
            this.barButtonItem82.Name = "barButtonItem82";
            this.barButtonItem82.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem82_ItemClick);
            // 
            // barButtonItem84
            // 
            this.barButtonItem84.Caption = "Create Segments for Select Stand(s)";
            this.barButtonItem84.Id = 142;
            this.barButtonItem84.Name = "barButtonItem84";
            this.barButtonItem84.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem84_ItemClick);
            // 
            // barButtonItem85
            // 
            this.barButtonItem85.Caption = "Init Tree Totals if Null";
            this.barButtonItem85.Id = 143;
            this.barButtonItem85.Name = "barButtonItem85";
            this.barButtonItem85.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem85_ItemClick);
            // 
            // barButtonItem86
            // 
            this.barButtonItem86.Caption = "Clean up Plots";
            this.barButtonItem86.Id = 144;
            this.barButtonItem86.Name = "barButtonItem86";
            this.barButtonItem86.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem86_ItemClick);
            // 
            // barButtonItem88
            // 
            this.barButtonItem88.Caption = "Set InAt for Plots";
            this.barButtonItem88.Id = 146;
            this.barButtonItem88.Name = "barButtonItem88";
            this.barButtonItem88.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem88_ItemClick);
            // 
            // barButtonItem89
            // 
            this.barButtonItem89.Caption = "Build Stand Species";
            this.barButtonItem89.Id = 148;
            this.barButtonItem89.Name = "barButtonItem89";
            this.barButtonItem89.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem89_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bar3.BarAppearance.Hovered.Options.UseFont = true;
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 13;
            this.barStaticItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barStaticItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticItem2.Id = 14;
            this.barStaticItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barStaticItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "barStaticItem3";
            this.barStaticItem3.Id = 15;
            this.barStaticItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barStaticItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "barStaticItem4";
            this.barStaticItem4.Id = 16;
            this.barStaticItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barStaticItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 4";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemFirst),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrev),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemNext),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemLast),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem13, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem14, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem70, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem71, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem72, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem15, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem17, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem57, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem25, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem16, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.Text = "Custom 4";
            // 
            // barButtonItemFirst
            // 
            this.barButtonItemFirst.Caption = "First";
            this.barButtonItemFirst.Id = 22;
            this.barButtonItemFirst.Name = "barButtonItemFirst";
            this.barButtonItemFirst.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFirst_ItemClick);
            // 
            // barButtonItemPrev
            // 
            this.barButtonItemPrev.Caption = "Prev";
            this.barButtonItemPrev.Id = 23;
            this.barButtonItemPrev.Name = "barButtonItemPrev";
            this.barButtonItemPrev.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrev_ItemClick);
            // 
            // barButtonItemNext
            // 
            this.barButtonItemNext.Caption = "Next";
            this.barButtonItemNext.Id = 24;
            this.barButtonItemNext.Name = "barButtonItemNext";
            this.barButtonItemNext.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemNext_ItemClick);
            // 
            // barButtonItemLast
            // 
            this.barButtonItemLast.Caption = "Last";
            this.barButtonItemLast.Id = 25;
            this.barButtonItemLast.Name = "barButtonItemLast";
            this.barButtonItemLast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLast_ItemClick);
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "Print";
            this.barButtonItem13.Id = 48;
            this.barButtonItem13.Name = "barButtonItem13";
            this.barButtonItem13.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem13_ItemClick);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Save";
            this.barButtonItem14.Id = 49;
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem14_ItemClick);
            // 
            // barButtonItem70
            // 
            this.barButtonItem70.Caption = "Delete Plot(s)";
            this.barButtonItem70.Id = 121;
            this.barButtonItem70.Name = "barButtonItem70";
            this.barButtonItem70.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem70_ItemClick);
            // 
            // barButtonItem71
            // 
            this.barButtonItem71.Caption = "Delete Tree(s)";
            this.barButtonItem71.Id = 122;
            this.barButtonItem71.Name = "barButtonItem71";
            this.barButtonItem71.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem71_ItemClick);
            // 
            // barButtonItem72
            // 
            this.barButtonItem72.Caption = "Delete Segment(s)";
            this.barButtonItem72.Id = 123;
            this.barButtonItem72.Name = "barButtonItem72";
            this.barButtonItem72.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem72_ItemClick);
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Column Chooser";
            this.barButtonItem15.Id = 50;
            this.barButtonItem15.Name = "barButtonItem15";
            this.barButtonItem15.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem15_ItemClick);
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "Filter";
            this.barButtonItem17.Id = 52;
            this.barButtonItem17.Name = "barButtonItem17";
            this.barButtonItem17.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem17_ItemClick);
            // 
            // barButtonItem57
            // 
            this.barButtonItem57.Caption = "Select All";
            this.barButtonItem57.Id = 107;
            this.barButtonItem57.Name = "barButtonItem57";
            this.barButtonItem57.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem57_ItemClick);
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "Attached Files";
            this.barButtonItem25.Id = 71;
            this.barButtonItem25.Name = "barButtonItem25";
            this.barButtonItem25.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem25_ItemClick);
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "Additional Tree Measurements";
            this.barButtonItem16.Id = 51;
            this.barButtonItem16.Name = "barButtonItem16";
            this.barButtonItem16.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem16_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Measure From Distance";
            this.barSubItem2.Id = 61;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnLimitingDistance),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTreeMeasurementsRelaskop),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTreeMeasurementsRD100),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem26, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem27, true)});
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnLimitingDistance
            // 
            this.btnLimitingDistance.Caption = "Limiting Distance";
            this.btnLimitingDistance.Id = 62;
            this.btnLimitingDistance.Name = "btnLimitingDistance";
            this.btnLimitingDistance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLimitingDistance_ItemClick);
            // 
            // btnTreeMeasurementsRelaskop
            // 
            this.btnTreeMeasurementsRelaskop.Caption = "Tree Measurements Relaskop";
            this.btnTreeMeasurementsRelaskop.Id = 63;
            this.btnTreeMeasurementsRelaskop.Name = "btnTreeMeasurementsRelaskop";
            this.btnTreeMeasurementsRelaskop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTreeMeasurementsRelaskop_ItemClick);
            // 
            // btnTreeMeasurementsRD100
            // 
            this.btnTreeMeasurementsRD100.Caption = "Tree Measurements RD100";
            this.btnTreeMeasurementsRD100.Id = 64;
            this.btnTreeMeasurementsRD100.Name = "btnTreeMeasurementsRD100";
            this.btnTreeMeasurementsRD100.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTreeMeasurementsRD100_ItemClick);
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "Total Height";
            this.barButtonItem26.Id = 72;
            this.barButtonItem26.Name = "barButtonItem26";
            this.barButtonItem26.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem26_ItemClick);
            // 
            // barButtonItem27
            // 
            this.barButtonItem27.Caption = "Form Factor without Distance";
            this.barButtonItem27.Id = 73;
            this.barButtonItem27.Name = "barButtonItem27";
            this.barButtonItem27.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem27_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(1410, 67);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 837);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(1410, 32);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 67);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 770);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1410, 67);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 770);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "First";
            this.barButtonItem1.Id = 8;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Prev";
            this.barButtonItem2.Id = 9;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Next";
            this.barButtonItem3.Id = 10;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Last";
            this.barButtonItem4.Id = 17;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Print";
            this.barButtonItem5.Id = 18;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barSubItemFileExit
            // 
            this.barSubItemFileExit.Caption = "Exit";
            this.barSubItemFileExit.Id = 19;
            this.barSubItemFileExit.Name = "barSubItemFileExit";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Select Project";
            this.barSubItem1.Id = 26;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barStaticItemProjectEditProject
            // 
            this.barStaticItemProjectEditProject.Caption = "Edit Project";
            this.barStaticItemProjectEditProject.Id = 27;
            this.barStaticItemProjectEditProject.Name = "barStaticItemProjectEditProject";
            // 
            // barStaticItemProjectNewProject
            // 
            this.barStaticItemProjectNewProject.Caption = "New Project";
            this.barStaticItemProjectNewProject.Id = 28;
            this.barStaticItemProjectNewProject.Name = "barStaticItemProjectNewProject";
            // 
            // barStaticItemProjectSelectProject
            // 
            this.barStaticItemProjectSelectProject.Caption = "Select Project";
            this.barStaticItemProjectSelectProject.Id = 29;
            this.barStaticItemProjectSelectProject.Name = "barStaticItemProjectSelectProject";
            // 
            // barStaticItemFileExit
            // 
            this.barStaticItemFileExit.Caption = "Exit";
            this.barStaticItemFileExit.Id = 30;
            this.barStaticItemFileExit.Name = "barStaticItemFileExit";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Species";
            this.barStaticItem5.Id = 31;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "Sort";
            this.barStaticItem6.Id = 32;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // barStaticItem7
            // 
            this.barStaticItem7.Caption = "Grade";
            this.barStaticItem7.Id = 33;
            this.barStaticItem7.Name = "barStaticItem7";
            // 
            // barStaticItem8
            // 
            this.barStaticItem8.Caption = "Price";
            this.barStaticItem8.Id = 34;
            this.barStaticItem8.Name = "barStaticItem8";
            // 
            // barStaticItem9
            // 
            this.barStaticItem9.Caption = "Cost";
            this.barStaticItem9.Id = 35;
            this.barStaticItem9.Name = "barStaticItem9";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Test";
            this.barButtonItem6.Id = 36;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Species";
            this.barButtonItem12.Id = 42;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // btnTotalHeight
            // 
            this.btnTotalHeight.Caption = "Total Height";
            this.btnTotalHeight.Id = 66;
            this.btnTotalHeight.Name = "btnTotalHeight";
            this.btnTotalHeight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTotalHeight_ItemClick);
            // 
            // btnFormFactorWithoutDistance
            // 
            this.btnFormFactorWithoutDistance.Caption = "Form Factor without Distance";
            this.btnFormFactorWithoutDistance.Id = 68;
            this.btnFormFactorWithoutDistance.Name = "btnFormFactorWithoutDistance";
            this.btnFormFactorWithoutDistance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFormFactorWithoutDistance_ItemClick);
            // 
            // barButtonItem33
            // 
            this.barButtonItem33.Caption = "Crown Position";
            this.barButtonItem33.Id = 82;
            this.barButtonItem33.Name = "barButtonItem33";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPageHome);
            this.tabPane1.Controls.Add(this.tabNavigationPageStandMaster);
            this.tabPane1.Controls.Add(this.tabNavigationPagePlotLocation);
            this.tabPane1.Controls.Add(this.tabNavigationPageTreeInput);
            this.tabPane1.Controls.Add(this.tabNavigationPageTreeEdit);
            this.tabPane1.Controls.Add(this.tabNavigationPageStandInput);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 67);
            this.tabPane1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPageHome,
            this.tabNavigationPageStandMaster,
            this.tabNavigationPagePlotLocation,
            this.tabNavigationPageTreeInput,
            this.tabNavigationPageTreeEdit,
            this.tabNavigationPageStandInput});
            this.tabPane1.RegularSize = new System.Drawing.Size(1410, 770);
            this.tabPane1.SelectedPage = this.tabNavigationPageStandMaster;
            this.tabPane1.Size = new System.Drawing.Size(1410, 770);
            this.tabPane1.TabIndex = 4;
            this.tabPane1.Text = "tabPane1";
            this.tabPane1.SelectedPageChanged += new DevExpress.XtraBars.Navigation.SelectedPageChangedEventHandler(this.tabPane1_SelectedPageChanged);
            this.tabPane1.SelectedPageChanging += new DevExpress.XtraBars.Navigation.SelectedPageChangingEventHandler(this.tabPane1_SelectedPageChanging);
            // 
            // tabNavigationPageHome
            // 
            this.tabNavigationPageHome.Caption = "Home";
            this.tabNavigationPageHome.Controls.Add(this.gridControlStands);
            this.tabNavigationPageHome.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPageHome.Name = "tabNavigationPageHome";
            this.tabNavigationPageHome.Size = new System.Drawing.Size(1388, 713);
            // 
            // gridControlStands
            // 
            this.gridControlStands.DataSource = this.standsViewBindingSource;
            this.gridControlStands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStands.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStands.Location = new System.Drawing.Point(0, 0);
            this.gridControlStands.MainView = this.gridViewStands;
            this.gridControlStands.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStands.MenuManager = this.barManager1;
            this.gridControlStands.Name = "gridControlStands";
            this.gridControlStands.Size = new System.Drawing.Size(1388, 713);
            this.gridControlStands.TabIndex = 0;
            this.gridControlStands.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStands});
            // 
            // standsViewBindingSource
            // 
            this.standsViewBindingSource.DataSource = typeof(Project.DAL.Stand);
            // 
            // gridViewStands
            // 
            this.gridViewStands.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewStands.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewStands.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.gridViewStands.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewStands.ColumnPanelRowHeight = 50;
            this.gridViewStands.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStandsId,
            this.colProjectsId,
            this.colTractGroup,
            this.colAttachments,
            this.colState,
            this.colCounty,
            this.colTractName,
            this.colStandName,
            this.colTownship,
            this.colRange,
            this.colSection,
            this.colNetGeographicAcres,
            this.colSource,
            this.colDateOfStandData,
            this.colGrownToDate,
            this.colMajorAge,
            this.colHarvest,
            this.colLandClass,
            this.colSiteIndex,
            this.colMajorSpecies,
            this.colTreesPerAcre,
            this.colBasalAreaPerAcre,
            this.colQmDbh,
            this.colTonsPerAcre,
            this.colCcfPerAcre,
            this.colMbfPerAcre,
            this.colTotalCcf,
            this.colTotalMbf,
            this.colPlots});
            this.gridViewStands.GridControl = this.gridControlStands;
            this.gridViewStands.GroupCount = 2;
            this.gridViewStands.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NetGeographicAcres", this.colNetGeographicAcres, "{0:0.00}", "0"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "MajorAge", this.colMajorAge, "{0:0}", ((short)(1))),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCcf", this.colTotalCcf, "{0:#,##0}", "8"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalMbf", this.colTotalMbf, "{0:#,##0}", "9"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "SiteIndex", this.colSiteIndex, "{0:#,##0}", ((short)(2))),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TreesPerAcre", this.colTreesPerAcre, "{0:#,##0.00}", 3F),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "BasalAreaPerAcre", this.colBasalAreaPerAcre, "{0:#,##0.00}", 4F),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TonsPerAcre", this.colTonsPerAcre, "{0:#,##0}", 5),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "CcfPerAcre", this.colCcfPerAcre, "{0:#,##0.00}", 6F),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "MbfPerAcre", this.colMbfPerAcre, "{0:#,##0.000}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Plots", this.colPlots, "{0:#}", "10")});
            this.gridViewStands.Name = "gridViewStands";
            this.gridViewStands.OptionsBehavior.Editable = false;
            this.gridViewStands.OptionsCustomization.AllowFilter = false;
            this.gridViewStands.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewStands.OptionsCustomization.AllowSort = false;
            this.gridViewStands.OptionsSelection.MultiSelect = true;
            this.gridViewStands.OptionsView.ColumnAutoWidth = false;
            this.gridViewStands.OptionsView.ShowFooter = true;
            this.gridViewStands.OptionsView.ShowGroupPanel = false;
            this.gridViewStands.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colProjectsId, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTractGroup, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewStands.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridViewStands_CustomSummaryCalculate);
            this.gridViewStands.Click += new System.EventHandler(this.gridViewStands_Click);
            this.gridViewStands.DoubleClick += new System.EventHandler(this.gridViewStands_DoubleClick);
            // 
            // colStandsId
            // 
            this.colStandsId.Caption = " ";
            this.colStandsId.FieldName = "StandsId";
            this.colStandsId.Name = "colStandsId";
            this.colStandsId.OptionsFilter.AllowFilter = false;
            this.colStandsId.Width = 20;
            // 
            // colProjectsId
            // 
            this.colProjectsId.FieldName = "ProjectsId";
            this.colProjectsId.Name = "colProjectsId";
            this.colProjectsId.OptionsFilter.AllowFilter = false;
            // 
            // colTractGroup
            // 
            this.colTractGroup.Caption = "Tract";
            this.colTractGroup.FieldName = "TractGroup";
            this.colTractGroup.Name = "colTractGroup";
            this.colTractGroup.OptionsFilter.AllowFilter = false;
            this.colTractGroup.Visible = true;
            this.colTractGroup.VisibleIndex = 19;
            // 
            // colAttachments
            // 
            this.colAttachments.Caption = "ATT";
            this.colAttachments.FieldName = "Attachments";
            this.colAttachments.Name = "colAttachments";
            this.colAttachments.Visible = true;
            this.colAttachments.VisibleIndex = 0;
            this.colAttachments.Width = 62;
            // 
            // colState
            // 
            this.colState.Caption = "St";
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.Visible = true;
            this.colState.VisibleIndex = 1;
            this.colState.Width = 30;
            // 
            // colCounty
            // 
            this.colCounty.Caption = "Cty";
            this.colCounty.FieldName = "County";
            this.colCounty.Name = "colCounty";
            this.colCounty.Visible = true;
            this.colCounty.VisibleIndex = 2;
            this.colCounty.Width = 35;
            // 
            // colTractName
            // 
            this.colTractName.Caption = "Tract";
            this.colTractName.FieldName = "TractName";
            this.colTractName.Name = "colTractName";
            this.colTractName.Visible = true;
            this.colTractName.VisibleIndex = 3;
            this.colTractName.Width = 65;
            // 
            // colStandName
            // 
            this.colStandName.Caption = "Stand #";
            this.colStandName.FieldName = "StandName";
            this.colStandName.Name = "colStandName";
            this.colStandName.Visible = true;
            this.colStandName.VisibleIndex = 4;
            this.colStandName.Width = 40;
            // 
            // colTownship
            // 
            this.colTownship.Caption = "Twn";
            this.colTownship.FieldName = "Township";
            this.colTownship.Name = "colTownship";
            this.colTownship.Visible = true;
            this.colTownship.VisibleIndex = 5;
            this.colTownship.Width = 33;
            // 
            // colRange
            // 
            this.colRange.Caption = "Rge";
            this.colRange.FieldName = "Range";
            this.colRange.Name = "colRange";
            this.colRange.Visible = true;
            this.colRange.VisibleIndex = 6;
            this.colRange.Width = 33;
            // 
            // colSection
            // 
            this.colSection.Caption = "Sec";
            this.colSection.FieldName = "Section";
            this.colSection.Name = "colSection";
            this.colSection.Visible = true;
            this.colSection.VisibleIndex = 7;
            this.colSection.Width = 30;
            // 
            // colNetGeographicAcres
            // 
            this.colNetGeographicAcres.AppearanceCell.Options.UseTextOptions = true;
            this.colNetGeographicAcres.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNetGeographicAcres.Caption = "Net Acres";
            this.colNetGeographicAcres.DisplayFormat.FormatString = "{0:0.00}";
            this.colNetGeographicAcres.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetGeographicAcres.FieldName = "NetGeographicAcres";
            this.colNetGeographicAcres.Name = "colNetGeographicAcres";
            this.colNetGeographicAcres.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NetGeographicAcres", "{0:0.00}")});
            this.colNetGeographicAcres.Visible = true;
            this.colNetGeographicAcres.VisibleIndex = 8;
            this.colNetGeographicAcres.Width = 50;
            // 
            // colSource
            // 
            this.colSource.Caption = "Src";
            this.colSource.FieldName = "Source";
            this.colSource.Name = "colSource";
            this.colSource.Visible = true;
            this.colSource.VisibleIndex = 9;
            this.colSource.Width = 35;
            // 
            // colDateOfStandData
            // 
            this.colDateOfStandData.Caption = "Exam Date";
            this.colDateOfStandData.DisplayFormat.FormatString = "d";
            this.colDateOfStandData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateOfStandData.FieldName = "DateOfStandData";
            this.colDateOfStandData.Name = "colDateOfStandData";
            this.colDateOfStandData.Visible = true;
            this.colDateOfStandData.VisibleIndex = 10;
            this.colDateOfStandData.Width = 70;
            // 
            // colGrownToDate
            // 
            this.colGrownToDate.Caption = "Grown Date";
            this.colGrownToDate.DisplayFormat.FormatString = "d";
            this.colGrownToDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colGrownToDate.FieldName = "GrownToDate";
            this.colGrownToDate.Name = "colGrownToDate";
            this.colGrownToDate.Visible = true;
            this.colGrownToDate.VisibleIndex = 11;
            this.colGrownToDate.Width = 70;
            // 
            // colMajorAge
            // 
            this.colMajorAge.AppearanceCell.Options.UseTextOptions = true;
            this.colMajorAge.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMajorAge.Caption = "Maj Age";
            this.colMajorAge.FieldName = "MajorAge";
            this.colMajorAge.Name = "colMajorAge";
            this.colMajorAge.Visible = true;
            this.colMajorAge.VisibleIndex = 12;
            this.colMajorAge.Width = 35;
            // 
            // colHarvest
            // 
            this.colHarvest.Caption = "Hrv";
            this.colHarvest.FieldName = "Harvest";
            this.colHarvest.Name = "colHarvest";
            this.colHarvest.Visible = true;
            this.colHarvest.VisibleIndex = 13;
            this.colHarvest.Width = 33;
            // 
            // colLandClass
            // 
            this.colLandClass.Caption = "Land Class";
            this.colLandClass.FieldName = "LandClass";
            this.colLandClass.Name = "colLandClass";
            this.colLandClass.Visible = true;
            this.colLandClass.VisibleIndex = 14;
            this.colLandClass.Width = 40;
            // 
            // colSiteIndex
            // 
            this.colSiteIndex.AppearanceCell.Options.UseTextOptions = true;
            this.colSiteIndex.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSiteIndex.Caption = "SI";
            this.colSiteIndex.FieldName = "SiteIndex";
            this.colSiteIndex.Name = "colSiteIndex";
            this.colSiteIndex.Visible = true;
            this.colSiteIndex.VisibleIndex = 15;
            this.colSiteIndex.Width = 32;
            // 
            // colMajorSpecies
            // 
            this.colMajorSpecies.Caption = "Maj Spc";
            this.colMajorSpecies.FieldName = "MajorSpecies";
            this.colMajorSpecies.Name = "colMajorSpecies";
            this.colMajorSpecies.Visible = true;
            this.colMajorSpecies.VisibleIndex = 16;
            this.colMajorSpecies.Width = 32;
            // 
            // colTreesPerAcre
            // 
            this.colTreesPerAcre.AppearanceCell.Options.UseTextOptions = true;
            this.colTreesPerAcre.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTreesPerAcre.Caption = "Trees Per Ac";
            this.colTreesPerAcre.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.colTreesPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreesPerAcre.FieldName = "TreesPerAcre";
            this.colTreesPerAcre.Name = "colTreesPerAcre";
            this.colTreesPerAcre.Visible = true;
            this.colTreesPerAcre.VisibleIndex = 17;
            this.colTreesPerAcre.Width = 50;
            // 
            // colBasalAreaPerAcre
            // 
            this.colBasalAreaPerAcre.AppearanceCell.Options.UseTextOptions = true;
            this.colBasalAreaPerAcre.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colBasalAreaPerAcre.Caption = "BA Per Ac";
            this.colBasalAreaPerAcre.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.colBasalAreaPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBasalAreaPerAcre.FieldName = "BasalAreaPerAcre";
            this.colBasalAreaPerAcre.Name = "colBasalAreaPerAcre";
            this.colBasalAreaPerAcre.Visible = true;
            this.colBasalAreaPerAcre.VisibleIndex = 18;
            this.colBasalAreaPerAcre.Width = 50;
            // 
            // colQmDbh
            // 
            this.colQmDbh.AppearanceCell.Options.UseTextOptions = true;
            this.colQmDbh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colQmDbh.Caption = "QM Dbh";
            this.colQmDbh.DisplayFormat.FormatString = "{0:#,###.0}";
            this.colQmDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQmDbh.FieldName = "QmDbh";
            this.colQmDbh.Name = "colQmDbh";
            this.colQmDbh.Visible = true;
            this.colQmDbh.VisibleIndex = 19;
            this.colQmDbh.Width = 40;
            // 
            // colTonsPerAcre
            // 
            this.colTonsPerAcre.AppearanceCell.Options.UseTextOptions = true;
            this.colTonsPerAcre.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTonsPerAcre.Caption = "Tons Per Ac";
            this.colTonsPerAcre.DisplayFormat.FormatString = "{0:#,###}";
            this.colTonsPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTonsPerAcre.FieldName = "TonsPerAcre";
            this.colTonsPerAcre.Name = "colTonsPerAcre";
            this.colTonsPerAcre.Visible = true;
            this.colTonsPerAcre.VisibleIndex = 20;
            this.colTonsPerAcre.Width = 45;
            // 
            // colCcfPerAcre
            // 
            this.colCcfPerAcre.AppearanceCell.Options.UseTextOptions = true;
            this.colCcfPerAcre.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colCcfPerAcre.Caption = "Net Ccf Per Ac";
            this.colCcfPerAcre.DisplayFormat.FormatString = "{0:0.00}";
            this.colCcfPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCcfPerAcre.FieldName = "CcfPerAcre";
            this.colCcfPerAcre.Name = "colCcfPerAcre";
            this.colCcfPerAcre.Visible = true;
            this.colCcfPerAcre.VisibleIndex = 21;
            this.colCcfPerAcre.Width = 45;
            // 
            // colMbfPerAcre
            // 
            this.colMbfPerAcre.AppearanceCell.Options.UseTextOptions = true;
            this.colMbfPerAcre.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMbfPerAcre.Caption = "Net Mbf Per Ac";
            this.colMbfPerAcre.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.colMbfPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMbfPerAcre.FieldName = "MbfPerAcre";
            this.colMbfPerAcre.Name = "colMbfPerAcre";
            this.colMbfPerAcre.Visible = true;
            this.colMbfPerAcre.VisibleIndex = 22;
            this.colMbfPerAcre.Width = 47;
            // 
            // colTotalCcf
            // 
            this.colTotalCcf.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalCcf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalCcf.Caption = "Total Net Ccf";
            this.colTotalCcf.DisplayFormat.FormatString = "{0:#,###}";
            this.colTotalCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalCcf.FieldName = "TotalCcf";
            this.colTotalCcf.Name = "colTotalCcf";
            this.colTotalCcf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCcf", "{0:#,###}")});
            this.colTotalCcf.Visible = true;
            this.colTotalCcf.VisibleIndex = 23;
            this.colTotalCcf.Width = 60;
            // 
            // colTotalMbf
            // 
            this.colTotalMbf.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalMbf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalMbf.Caption = "Total Net Mbf";
            this.colTotalMbf.DisplayFormat.FormatString = "{0:#,###}";
            this.colTotalMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalMbf.FieldName = "TotalMbf";
            this.colTotalMbf.Name = "colTotalMbf";
            this.colTotalMbf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalMbf", "{0:#,###}")});
            this.colTotalMbf.Visible = true;
            this.colTotalMbf.VisibleIndex = 24;
            this.colTotalMbf.Width = 50;
            // 
            // colPlots
            // 
            this.colPlots.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlots.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlots.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlots.Caption = "Plots";
            this.colPlots.DisplayFormat.FormatString = "{0:0}";
            this.colPlots.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPlots.FieldName = "Plots";
            this.colPlots.Name = "colPlots";
            this.colPlots.Visible = true;
            this.colPlots.VisibleIndex = 25;
            this.colPlots.Width = 45;
            // 
            // tabNavigationPageStandMaster
            // 
            this.tabNavigationPageStandMaster.Caption = "Stand Master";
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl4);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl11);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl10);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl6);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl5);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl3);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl2);
            this.tabNavigationPageStandMaster.Controls.Add(this.groupControl1);
            this.tabNavigationPageStandMaster.Controls.Add(this.panelControl5);
            this.tabNavigationPageStandMaster.Controls.Add(this.panelControl8);
            this.tabNavigationPageStandMaster.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPageStandMaster.Name = "tabNavigationPageStandMaster";
            this.tabNavigationPageStandMaster.Size = new System.Drawing.Size(1388, 713);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gridControl4);
            this.groupControl4.Location = new System.Drawing.Point(913, 481);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(416, 222);
            this.groupControl4.TabIndex = 17;
            this.groupControl4.Text = "Yarding System";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.standYardingSystemBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl4.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl4.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(2, 26);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditHarvest,
            this.repositoryItemComboBoxYarding});
            this.gridControl4.Size = new System.Drawing.Size(412, 194);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            this.gridControl4.Validating += new System.ComponentModel.CancelEventHandler(this.gridControl4_Validating);
            // 
            // standYardingSystemBindingSource
            // 
            this.standYardingSystemBindingSource.DataSource = typeof(Project.DAL.StandYardingSystem);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId3,
            this.colStandsId7,
            this.colCode,
            this.colDescription,
            this.colPercent,
            this.colCostUnit});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView4.OptionsCustomization.AllowColumnMoving = false;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsCustomization.AllowGroup = false;
            this.gridView4.OptionsCustomization.AllowSort = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView4_RowCellClick);
            this.gridView4.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView4_ValidateRow);
            this.gridView4.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            this.gridView4.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView4_ValidatingEditor);
            // 
            // colId3
            // 
            this.colId3.FieldName = "Id";
            this.colId3.Name = "colId3";
            // 
            // colStandsId7
            // 
            this.colStandsId7.FieldName = "StandsId";
            this.colStandsId7.Name = "colStandsId7";
            // 
            // colCode
            // 
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            // 
            // colDescription
            // 
            this.colDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescription.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDescription.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescription.Caption = "Description";
            this.colDescription.ColumnEdit = this.repositoryItemComboBoxYarding;
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 145;
            // 
            // repositoryItemComboBoxYarding
            // 
            this.repositoryItemComboBoxYarding.AutoHeight = false;
            this.repositoryItemComboBoxYarding.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxYarding.Name = "repositoryItemComboBoxYarding";
            // 
            // colPercent
            // 
            this.colPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPercent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPercent.DisplayFormat.FormatString = "{0:#}";
            this.colPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercent.FieldName = "Percent";
            this.colPercent.Name = "colPercent";
            this.colPercent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Percent", "{0:#}")});
            this.colPercent.Visible = true;
            this.colPercent.VisibleIndex = 1;
            this.colPercent.Width = 45;
            // 
            // colCostUnit
            // 
            this.colCostUnit.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostUnit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostUnit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostUnit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostUnit.Caption = "Cost Unit";
            this.colCostUnit.FieldName = "CostUnit";
            this.colCostUnit.Name = "colCostUnit";
            this.colCostUnit.OptionsColumn.AllowEdit = false;
            this.colCostUnit.OptionsColumn.AllowFocus = false;
            this.colCostUnit.OptionsColumn.ReadOnly = true;
            this.colCostUnit.Visible = true;
            this.colCostUnit.VisibleIndex = 2;
            this.colCostUnit.Width = 51;
            // 
            // repositoryItemLookUpEditHarvest
            // 
            this.repositoryItemLookUpEditHarvest.AutoHeight = false;
            this.repositoryItemLookUpEditHarvest.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditHarvest.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")});
            this.repositoryItemLookUpEditHarvest.DataSource = this.harvestBindingSource;
            this.repositoryItemLookUpEditHarvest.DisplayMember = "Description";
            this.repositoryItemLookUpEditHarvest.Name = "repositoryItemLookUpEditHarvest";
            this.repositoryItemLookUpEditHarvest.NullText = "";
            this.repositoryItemLookUpEditHarvest.ValueMember = "Description";
            // 
            // harvestBindingSource
            // 
            this.harvestBindingSource.DataSource = typeof(Project.DAL.Harvestsystem);
            // 
            // groupControl11
            // 
            this.groupControl11.Controls.Add(this.gridControl3);
            this.groupControl11.Location = new System.Drawing.Point(1069, 223);
            this.groupControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(261, 252);
            this.groupControl11.TabIndex = 16;
            this.groupControl11.Text = "Permanent Plot Measurements";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.standPermPlotBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl3.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl3.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(2, 26);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(257, 224);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // standPermPlotBindingSource
            // 
            this.standPermPlotBindingSource.DataSource = typeof(Project.DAL.StandPermPlot);
            // 
            // gridView3
            // 
            this.gridView3.ColumnPanelRowHeight = 35;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId2,
            this.colStandsId6,
            this.colPermPlotOccasion,
            this.colPermPlotDate,
            this.colPermPlotInterval});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView3.OptionsCustomization.AllowColumnMoving = false;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowSort = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView3_InitNewRow);
            this.gridView3.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView3_ValidateRow);
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // colId2
            // 
            this.colId2.AppearanceHeader.Options.UseTextOptions = true;
            this.colId2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colId2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colId2.FieldName = "Id";
            this.colId2.Name = "colId2";
            // 
            // colStandsId6
            // 
            this.colStandsId6.FieldName = "StandsId";
            this.colStandsId6.Name = "colStandsId6";
            // 
            // colPermPlotOccasion
            // 
            this.colPermPlotOccasion.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotOccasion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotOccasion.Caption = "Occa sion";
            this.colPermPlotOccasion.FieldName = "PermPlotOccasion";
            this.colPermPlotOccasion.Name = "colPermPlotOccasion";
            this.colPermPlotOccasion.OptionsColumn.ReadOnly = true;
            this.colPermPlotOccasion.Visible = true;
            this.colPermPlotOccasion.VisibleIndex = 0;
            this.colPermPlotOccasion.Width = 51;
            // 
            // colPermPlotDate
            // 
            this.colPermPlotDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotDate.Caption = "Date";
            this.colPermPlotDate.FieldName = "PermPlotDate";
            this.colPermPlotDate.Name = "colPermPlotDate";
            this.colPermPlotDate.Visible = true;
            this.colPermPlotDate.VisibleIndex = 1;
            this.colPermPlotDate.Width = 84;
            // 
            // colPermPlotInterval
            // 
            this.colPermPlotInterval.AppearanceHeader.Options.UseTextOptions = true;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPermPlotInterval.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPermPlotInterval.Caption = "Inter val";
            this.colPermPlotInterval.FieldName = "PermPlotInterval";
            this.colPermPlotInterval.Name = "colPermPlotInterval";
            this.colPermPlotInterval.OptionsColumn.ReadOnly = true;
            this.colPermPlotInterval.Visible = true;
            this.colPermPlotInterval.VisibleIndex = 2;
            this.colPermPlotInterval.Width = 44;
            // 
            // groupControl10
            // 
            this.groupControl10.Controls.Add(this.gridControl2);
            this.groupControl10.Location = new System.Drawing.Point(913, 223);
            this.groupControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(148, 252);
            this.groupControl10.TabIndex = 15;
            this.groupControl10.Text = "Age";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.standAgeBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl2.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl2.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(2, 26);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditAge});
            this.gridControl2.Size = new System.Drawing.Size(144, 224);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // standAgeBindingSource
            // 
            this.standAgeBindingSource.DataSource = typeof(Project.DAL.StandAge);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId1,
            this.colStandsId5,
            this.colAgeCode1,
            this.colAge2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView2_InitNewRow);
            this.gridView2.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView2_RowUpdated);
            // 
            // colId1
            // 
            this.colId1.FieldName = "Id";
            this.colId1.Name = "colId1";
            // 
            // colStandsId5
            // 
            this.colStandsId5.FieldName = "StandsId";
            this.colStandsId5.Name = "colStandsId5";
            // 
            // colAgeCode1
            // 
            this.colAgeCode1.Caption = "Code";
            this.colAgeCode1.FieldName = "AgeCode";
            this.colAgeCode1.Name = "colAgeCode1";
            this.colAgeCode1.Visible = true;
            this.colAgeCode1.VisibleIndex = 0;
            this.colAgeCode1.Width = 41;
            // 
            // colAge2
            // 
            this.colAge2.Caption = "Age";
            this.colAge2.ColumnEdit = this.repositoryItemTextEditAge;
            this.colAge2.FieldName = "Age";
            this.colAge2.Name = "colAge2";
            this.colAge2.Visible = true;
            this.colAge2.VisibleIndex = 1;
            this.colAge2.Width = 37;
            // 
            // repositoryItemTextEditAge
            // 
            this.repositoryItemTextEditAge.AutoHeight = false;
            this.repositoryItemTextEditAge.Name = "repositoryItemTextEditAge";
            this.repositoryItemTextEditAge.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemTextEditAge_EditValueChanging);
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.gridControl1);
            this.groupControl6.Location = new System.Drawing.Point(301, 482);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(604, 220);
            this.groupControl6.TabIndex = 13;
            this.groupControl6.Text = "Hauling Calculations";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.standHaulingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(2, 26);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit4,
            this.repositoryItemComboBox1});
            this.gridControl1.Size = new System.Drawing.Size(600, 192);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // standHaulingBindingSource
            // 
            this.standHaulingBindingSource.DataSource = typeof(Project.DAL.StandHauling);
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 45;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHaulingId,
            this.colDestination,
            this.colAvgLoadSize,
            this.colRoundTripMiles,
            this.colCostPerHour,
            this.colTableName,
            this.colHours,
            this.colMinutes,
            this.colAvgLoadCcf,
            this.colAvgLoadMbf,
            this.colLoadUnloadHours,
            this.colCalcCostDollarPerMbf,
            this.colStandsId8});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowRowSizing = true;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colHaulingId
            // 
            this.colHaulingId.AppearanceHeader.Options.UseTextOptions = true;
            this.colHaulingId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHaulingId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHaulingId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHaulingId.FieldName = "HaulingId";
            this.colHaulingId.Name = "colHaulingId";
            // 
            // colDestination
            // 
            this.colDestination.AppearanceHeader.Options.UseTextOptions = true;
            this.colDestination.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDestination.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDestination.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDestination.Caption = "Destination";
            this.colDestination.ColumnEdit = this.repositoryItemComboBox1;
            this.colDestination.FieldName = "Destination";
            this.colDestination.Name = "colDestination";
            this.colDestination.Visible = true;
            this.colDestination.VisibleIndex = 0;
            this.colDestination.Width = 110;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // colAvgLoadSize
            // 
            this.colAvgLoadSize.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadSize.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadSize.Caption = "Avg Load Tons";
            this.colAvgLoadSize.DisplayFormat.FormatString = "n1";
            this.colAvgLoadSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadSize.FieldName = "AvgLoadSize";
            this.colAvgLoadSize.Name = "colAvgLoadSize";
            this.colAvgLoadSize.Visible = true;
            this.colAvgLoadSize.VisibleIndex = 1;
            this.colAvgLoadSize.Width = 40;
            // 
            // colRoundTripMiles
            // 
            this.colRoundTripMiles.AppearanceHeader.Options.UseTextOptions = true;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colRoundTripMiles.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRoundTripMiles.Caption = "Round Trip Miles";
            this.colRoundTripMiles.DisplayFormat.FormatString = "n0";
            this.colRoundTripMiles.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRoundTripMiles.FieldName = "RoundTripMiles";
            this.colRoundTripMiles.Name = "colRoundTripMiles";
            this.colRoundTripMiles.Visible = true;
            this.colRoundTripMiles.VisibleIndex = 4;
            this.colRoundTripMiles.Width = 40;
            // 
            // colCostPerHour
            // 
            this.colCostPerHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colCostPerHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCostPerHour.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCostPerHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCostPerHour.Caption = "Cost per Hour $";
            this.colCostPerHour.DisplayFormat.FormatString = "n2";
            this.colCostPerHour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCostPerHour.FieldName = "CostPerHour";
            this.colCostPerHour.Name = "colCostPerHour";
            this.colCostPerHour.Visible = true;
            this.colCostPerHour.VisibleIndex = 8;
            this.colCostPerHour.Width = 40;
            // 
            // colTableName
            // 
            this.colTableName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTableName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTableName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            // 
            // colHours
            // 
            this.colHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHours.Caption = "Hours";
            this.colHours.DisplayFormat.FormatString = "n0";
            this.colHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 5;
            this.colHours.Width = 40;
            // 
            // colMinutes
            // 
            this.colMinutes.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinutes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinutes.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colMinutes.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMinutes.Caption = "Min";
            this.colMinutes.DisplayFormat.FormatString = "n0";
            this.colMinutes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinutes.FieldName = "Minutes";
            this.colMinutes.Name = "colMinutes";
            this.colMinutes.Visible = true;
            this.colMinutes.VisibleIndex = 6;
            this.colMinutes.Width = 30;
            // 
            // colAvgLoadCcf
            // 
            this.colAvgLoadCcf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadCcf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadCcf.Caption = "Avg Load Ccf";
            this.colAvgLoadCcf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadCcf.FieldName = "AvgLoadCcf";
            this.colAvgLoadCcf.Name = "colAvgLoadCcf";
            this.colAvgLoadCcf.Visible = true;
            this.colAvgLoadCcf.VisibleIndex = 2;
            this.colAvgLoadCcf.Width = 35;
            // 
            // colAvgLoadMbf
            // 
            this.colAvgLoadMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAvgLoadMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgLoadMbf.Caption = "Avg Load Mbf";
            this.colAvgLoadMbf.DisplayFormat.FormatString = "n1";
            this.colAvgLoadMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAvgLoadMbf.FieldName = "AvgLoadMbf";
            this.colAvgLoadMbf.Name = "colAvgLoadMbf";
            this.colAvgLoadMbf.Visible = true;
            this.colAvgLoadMbf.VisibleIndex = 3;
            this.colAvgLoadMbf.Width = 35;
            // 
            // colLoadUnloadHours
            // 
            this.colLoadUnloadHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLoadUnloadHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoadUnloadHours.Caption = "Load & Unload Hours";
            this.colLoadUnloadHours.DisplayFormat.FormatString = "n2";
            this.colLoadUnloadHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLoadUnloadHours.FieldName = "LoadUnloadHours";
            this.colLoadUnloadHours.Name = "colLoadUnloadHours";
            this.colLoadUnloadHours.Visible = true;
            this.colLoadUnloadHours.VisibleIndex = 7;
            this.colLoadUnloadHours.Width = 45;
            // 
            // colCalcCostDollarPerMbf
            // 
            this.colCalcCostDollarPerMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcCostDollarPerMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcCostDollarPerMbf.Caption = "Calc Cost $/Mbf";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatString = "n0";
            this.colCalcCostDollarPerMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcCostDollarPerMbf.FieldName = "CalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.Name = "colCalcCostDollarPerMbf";
            this.colCalcCostDollarPerMbf.Visible = true;
            this.colCalcCostDollarPerMbf.VisibleIndex = 9;
            this.colCalcCostDollarPerMbf.Width = 45;
            // 
            // colStandsId8
            // 
            this.colStandsId8.AppearanceHeader.Options.UseTextOptions = true;
            this.colStandsId8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStandsId8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStandsId8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStandsId8.FieldName = "StandsId";
            this.colStandsId8.Name = "colStandsId8";
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Destination", "Destination", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit4.DataSource = this.haulingBindingSource;
            this.repositoryItemLookUpEdit4.DisplayMember = "Destination";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.NullText = "";
            this.repositoryItemLookUpEdit4.ValueMember = "Destination";
            // 
            // haulingBindingSource
            // 
            this.haulingBindingSource.DataSource = typeof(Project.DAL.Hauling);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.labelControl110);
            this.groupControl5.Controls.Add(this.tbSiteIndex4);
            this.groupControl5.Controls.Add(this.tbSiteIndex3);
            this.groupControl5.Controls.Add(this.tbSiteIndex2);
            this.groupControl5.Controls.Add(this.tbSiteIndex1);
            this.groupControl5.Controls.Add(this.labelControl102);
            this.groupControl5.Controls.Add(this.labelControl103);
            this.groupControl5.Controls.Add(this.labelControl104);
            this.groupControl5.Controls.Add(this.tbSpecies11);
            this.groupControl5.Controls.Add(this.tbSpecies12);
            this.groupControl5.Controls.Add(this.tbSpecies13);
            this.groupControl5.Controls.Add(this.tbSpecies21);
            this.groupControl5.Controls.Add(this.tbSpecies22);
            this.groupControl5.Controls.Add(this.tbSpecies23);
            this.groupControl5.Controls.Add(this.tbSpecies31);
            this.groupControl5.Controls.Add(this.tbSpecies32);
            this.groupControl5.Controls.Add(this.tbSpecies33);
            this.groupControl5.Controls.Add(this.tbSpecies41);
            this.groupControl5.Controls.Add(this.tbSpecies42);
            this.groupControl5.Controls.Add(this.tbSpecies43);
            this.groupControl5.Location = new System.Drawing.Point(3, 481);
            this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(290, 222);
            this.groupControl5.TabIndex = 12;
            this.groupControl5.Text = "Site Index by Species";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Options.UseTextOptions = true;
            this.labelControl110.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl110.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl110.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl110.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl110.Location = new System.Drawing.Point(227, 36);
            this.labelControl110.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(33, 32);
            this.labelControl110.TabIndex = 62;
            this.labelControl110.Text = "Site Index";
            // 
            // tbSiteIndex4
            // 
            this.tbSiteIndex4.EnterMoveNextControl = true;
            this.tbSiteIndex4.Location = new System.Drawing.Point(218, 178);
            this.tbSiteIndex4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSiteIndex4.MenuManager = this.barManager1;
            this.tbSiteIndex4.Name = "tbSiteIndex4";
            this.tbSiteIndex4.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex4.Size = new System.Drawing.Size(58, 22);
            this.tbSiteIndex4.TabIndex = 87;
            this.tbSiteIndex4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbSiteIndex4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex3
            // 
            this.tbSiteIndex3.EnterMoveNextControl = true;
            this.tbSiteIndex3.Location = new System.Drawing.Point(218, 146);
            this.tbSiteIndex3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSiteIndex3.MenuManager = this.barManager1;
            this.tbSiteIndex3.Name = "tbSiteIndex3";
            this.tbSiteIndex3.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex3.Size = new System.Drawing.Size(58, 22);
            this.tbSiteIndex3.TabIndex = 83;
            this.tbSiteIndex3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbSiteIndex3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex2
            // 
            this.tbSiteIndex2.EnterMoveNextControl = true;
            this.tbSiteIndex2.Location = new System.Drawing.Point(218, 114);
            this.tbSiteIndex2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSiteIndex2.MenuManager = this.barManager1;
            this.tbSiteIndex2.Name = "tbSiteIndex2";
            this.tbSiteIndex2.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex2.Size = new System.Drawing.Size(58, 22);
            this.tbSiteIndex2.TabIndex = 79;
            this.tbSiteIndex2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbSiteIndex2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbSiteIndex1
            // 
            this.tbSiteIndex1.EnterMoveNextControl = true;
            this.tbSiteIndex1.Location = new System.Drawing.Point(218, 82);
            this.tbSiteIndex1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSiteIndex1.MenuManager = this.barManager1;
            this.tbSiteIndex1.Name = "tbSiteIndex1";
            this.tbSiteIndex1.Properties.DisplayFormat.FormatString = "{0:#}";
            this.tbSiteIndex1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbSiteIndex1.Size = new System.Drawing.Size(58, 22);
            this.tbSiteIndex1.TabIndex = 75;
            this.tbSiteIndex1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbSiteIndex1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl102
            // 
            this.labelControl102.Appearance.Options.UseTextOptions = true;
            this.labelControl102.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl102.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl102.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl102.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl102.Location = new System.Drawing.Point(157, 52);
            this.labelControl102.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(47, 16);
            this.labelControl102.TabIndex = 56;
            this.labelControl102.Text = "Species";
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Options.UseTextOptions = true;
            this.labelControl103.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl103.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl103.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl103.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl103.Location = new System.Drawing.Point(78, 52);
            this.labelControl103.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(68, 16);
            this.labelControl103.TabIndex = 25;
            this.labelControl103.Text = "Species";
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Options.UseTextOptions = true;
            this.labelControl104.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl104.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl104.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl104.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl104.Location = new System.Drawing.Point(6, 52);
            this.labelControl104.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(65, 16);
            this.labelControl104.TabIndex = 24;
            this.labelControl104.Text = "Species";
            // 
            // tbSpecies11
            // 
            this.tbSpecies11.EnterMoveNextControl = true;
            this.tbSpecies11.Location = new System.Drawing.Point(12, 82);
            this.tbSpecies11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies11.MenuManager = this.barManager1;
            this.tbSpecies11.Name = "tbSpecies11";
            this.tbSpecies11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies11.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies11.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies11.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies11.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies11.Properties.NullText = "";
            this.tbSpecies11.Properties.PopupSizeable = false;
            this.tbSpecies11.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies11.Properties.ValueMember = "Abbreviation";
            this.tbSpecies11.Size = new System.Drawing.Size(56, 22);
            this.tbSpecies11.TabIndex = 72;
            this.tbSpecies11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies11.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // speciesBindingSource
            // 
            this.speciesBindingSource.DataSource = typeof(Project.DAL.Species);
            // 
            // tbSpecies12
            // 
            this.tbSpecies12.EnterMoveNextControl = true;
            this.tbSpecies12.Location = new System.Drawing.Point(80, 82);
            this.tbSpecies12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies12.MenuManager = this.barManager1;
            this.tbSpecies12.Name = "tbSpecies12";
            this.tbSpecies12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies12.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies12.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies12.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies12.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies12.Properties.NullText = "";
            this.tbSpecies12.Properties.PopupSizeable = false;
            this.tbSpecies12.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies12.Properties.ValueMember = "Abbreviation";
            this.tbSpecies12.Size = new System.Drawing.Size(59, 22);
            this.tbSpecies12.TabIndex = 73;
            this.tbSpecies12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies12.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies13
            // 
            this.tbSpecies13.EnterMoveNextControl = true;
            this.tbSpecies13.Location = new System.Drawing.Point(153, 82);
            this.tbSpecies13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies13.MenuManager = this.barManager1;
            this.tbSpecies13.Name = "tbSpecies13";
            this.tbSpecies13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies13.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies13.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies13.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies13.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies13.Properties.NullText = "";
            this.tbSpecies13.Properties.PopupSizeable = false;
            this.tbSpecies13.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies13.Properties.ValueMember = "Abbreviation";
            this.tbSpecies13.Size = new System.Drawing.Size(58, 22);
            this.tbSpecies13.TabIndex = 74;
            this.tbSpecies13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies13.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies21
            // 
            this.tbSpecies21.EnterMoveNextControl = true;
            this.tbSpecies21.Location = new System.Drawing.Point(12, 114);
            this.tbSpecies21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies21.MenuManager = this.barManager1;
            this.tbSpecies21.Name = "tbSpecies21";
            this.tbSpecies21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies21.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies21.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies21.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies21.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies21.Properties.NullText = "";
            this.tbSpecies21.Properties.PopupSizeable = false;
            this.tbSpecies21.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies21.Properties.ValueMember = "Abbreviation";
            this.tbSpecies21.Size = new System.Drawing.Size(56, 22);
            this.tbSpecies21.TabIndex = 76;
            this.tbSpecies21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies21.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies22
            // 
            this.tbSpecies22.EnterMoveNextControl = true;
            this.tbSpecies22.Location = new System.Drawing.Point(80, 114);
            this.tbSpecies22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies22.MenuManager = this.barManager1;
            this.tbSpecies22.Name = "tbSpecies22";
            this.tbSpecies22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies22.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies22.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies22.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies22.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies22.Properties.NullText = "";
            this.tbSpecies22.Properties.PopupSizeable = false;
            this.tbSpecies22.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies22.Properties.ValueMember = "Abbreviation";
            this.tbSpecies22.Size = new System.Drawing.Size(59, 22);
            this.tbSpecies22.TabIndex = 77;
            this.tbSpecies22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies22.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies23
            // 
            this.tbSpecies23.EnterMoveNextControl = true;
            this.tbSpecies23.Location = new System.Drawing.Point(153, 114);
            this.tbSpecies23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies23.MenuManager = this.barManager1;
            this.tbSpecies23.Name = "tbSpecies23";
            this.tbSpecies23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies23.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies23.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies23.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies23.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies23.Properties.NullText = "";
            this.tbSpecies23.Properties.PopupSizeable = false;
            this.tbSpecies23.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies23.Properties.ValueMember = "Abbreviation";
            this.tbSpecies23.Size = new System.Drawing.Size(58, 22);
            this.tbSpecies23.TabIndex = 78;
            this.tbSpecies23.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies23.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies31
            // 
            this.tbSpecies31.EnterMoveNextControl = true;
            this.tbSpecies31.Location = new System.Drawing.Point(12, 146);
            this.tbSpecies31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies31.MenuManager = this.barManager1;
            this.tbSpecies31.Name = "tbSpecies31";
            this.tbSpecies31.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies31.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies31.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies31.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies31.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies31.Properties.NullText = "";
            this.tbSpecies31.Properties.PopupSizeable = false;
            this.tbSpecies31.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies31.Properties.ValueMember = "Abbreviation";
            this.tbSpecies31.Size = new System.Drawing.Size(56, 22);
            this.tbSpecies31.TabIndex = 80;
            this.tbSpecies31.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies31.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies32
            // 
            this.tbSpecies32.EnterMoveNextControl = true;
            this.tbSpecies32.Location = new System.Drawing.Point(80, 146);
            this.tbSpecies32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies32.MenuManager = this.barManager1;
            this.tbSpecies32.Name = "tbSpecies32";
            this.tbSpecies32.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies32.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies32.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies32.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies32.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies32.Properties.NullText = "";
            this.tbSpecies32.Properties.PopupSizeable = false;
            this.tbSpecies32.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies32.Properties.ValueMember = "Abbreviation";
            this.tbSpecies32.Size = new System.Drawing.Size(59, 22);
            this.tbSpecies32.TabIndex = 81;
            this.tbSpecies32.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies32.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies33
            // 
            this.tbSpecies33.EnterMoveNextControl = true;
            this.tbSpecies33.Location = new System.Drawing.Point(153, 146);
            this.tbSpecies33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies33.MenuManager = this.barManager1;
            this.tbSpecies33.Name = "tbSpecies33";
            this.tbSpecies33.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies33.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies33.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies33.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies33.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies33.Properties.NullText = "";
            this.tbSpecies33.Properties.PopupSizeable = false;
            this.tbSpecies33.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies33.Properties.ValueMember = "Abbreviation";
            this.tbSpecies33.Size = new System.Drawing.Size(58, 22);
            this.tbSpecies33.TabIndex = 82;
            this.tbSpecies33.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies33.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies41
            // 
            this.tbSpecies41.EnterMoveNextControl = true;
            this.tbSpecies41.Location = new System.Drawing.Point(12, 178);
            this.tbSpecies41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies41.MenuManager = this.barManager1;
            this.tbSpecies41.Name = "tbSpecies41";
            this.tbSpecies41.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies41.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies41.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies41.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies41.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies41.Properties.NullText = "";
            this.tbSpecies41.Properties.PopupSizeable = false;
            this.tbSpecies41.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies41.Properties.ValueMember = "Abbreviation";
            this.tbSpecies41.Size = new System.Drawing.Size(56, 22);
            this.tbSpecies41.TabIndex = 84;
            this.tbSpecies41.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies41.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies42
            // 
            this.tbSpecies42.EnterMoveNextControl = true;
            this.tbSpecies42.Location = new System.Drawing.Point(80, 178);
            this.tbSpecies42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies42.MenuManager = this.barManager1;
            this.tbSpecies42.Name = "tbSpecies42";
            this.tbSpecies42.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies42.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies42.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies42.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies42.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies42.Properties.NullText = "";
            this.tbSpecies42.Properties.PopupSizeable = false;
            this.tbSpecies42.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies42.Properties.ValueMember = "Abbreviation";
            this.tbSpecies42.Size = new System.Drawing.Size(59, 22);
            this.tbSpecies42.TabIndex = 85;
            this.tbSpecies42.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies42.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // tbSpecies43
            // 
            this.tbSpecies43.EnterMoveNextControl = true;
            this.tbSpecies43.Location = new System.Drawing.Point(153, 178);
            this.tbSpecies43.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSpecies43.MenuManager = this.barManager1;
            this.tbSpecies43.Name = "tbSpecies43";
            this.tbSpecies43.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbSpecies43.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSpecies43.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.tbSpecies43.Properties.DataSource = this.speciesBindingSource;
            this.tbSpecies43.Properties.DisplayMember = "Abbreviation";
            this.tbSpecies43.Properties.NullText = "";
            this.tbSpecies43.Properties.PopupSizeable = false;
            this.tbSpecies43.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tbSpecies43.Properties.ValueMember = "Abbreviation";
            this.tbSpecies43.Size = new System.Drawing.Size(58, 22);
            this.tbSpecies43.TabIndex = 86;
            this.tbSpecies43.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSpecies43.Validating += new System.ComponentModel.CancelEventHandler(this.tbSpecies_Validating);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl101);
            this.groupControl3.Controls.Add(this.tbStripExpansion5);
            this.groupControl3.Controls.Add(this.tbStripExpansion4);
            this.groupControl3.Controls.Add(this.tbStripExpansion3);
            this.groupControl3.Controls.Add(this.tbStripExpansion2);
            this.groupControl3.Controls.Add(this.tbStripExpansion1);
            this.groupControl3.Controls.Add(this.labelControl84);
            this.groupControl3.Controls.Add(this.labelControl85);
            this.groupControl3.Controls.Add(this.labelControl86);
            this.groupControl3.Controls.Add(this.labelControl87);
            this.groupControl3.Controls.Add(this.labelControl88);
            this.groupControl3.Controls.Add(this.labelControl89);
            this.groupControl3.Controls.Add(this.labelControl90);
            this.groupControl3.Controls.Add(this.tbStripWidth5);
            this.groupControl3.Controls.Add(this.tbStripInterval5);
            this.groupControl3.Controls.Add(this.tbStripWidth4);
            this.groupControl3.Controls.Add(this.tbStripInterval4);
            this.groupControl3.Controls.Add(this.tbStripWidth3);
            this.groupControl3.Controls.Add(this.tbStripInterval3);
            this.groupControl3.Controls.Add(this.tbStripWidth2);
            this.groupControl3.Controls.Add(this.tbStripInterval2);
            this.groupControl3.Controls.Add(this.tbStripWidth1);
            this.groupControl3.Controls.Add(this.tbStripInterval1);
            this.groupControl3.Location = new System.Drawing.Point(656, 223);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(250, 252);
            this.groupControl3.TabIndex = 10;
            this.groupControl3.Text = "Strip Cruise";
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Options.UseTextOptions = true;
            this.labelControl101.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl101.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl101.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl101.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl101.Location = new System.Drawing.Point(188, 43);
            this.labelControl101.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(33, 32);
            this.labelControl101.TabIndex = 56;
            this.labelControl101.Text = "Blow Up";
            // 
            // tbStripExpansion5
            // 
            this.tbStripExpansion5.EnterMoveNextControl = true;
            this.tbStripExpansion5.Location = new System.Drawing.Point(176, 210);
            this.tbStripExpansion5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripExpansion5.MenuManager = this.barManager1;
            this.tbStripExpansion5.Name = "tbStripExpansion5";
            this.tbStripExpansion5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion5.Size = new System.Drawing.Size(58, 22);
            this.tbStripExpansion5.TabIndex = 62;
            this.tbStripExpansion5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion5.Validated += new System.EventHandler(this.textEdit19_Validated);
            // 
            // tbStripExpansion4
            // 
            this.tbStripExpansion4.EnterMoveNextControl = true;
            this.tbStripExpansion4.Location = new System.Drawing.Point(176, 178);
            this.tbStripExpansion4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripExpansion4.MenuManager = this.barManager1;
            this.tbStripExpansion4.Name = "tbStripExpansion4";
            this.tbStripExpansion4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion4.Size = new System.Drawing.Size(58, 22);
            this.tbStripExpansion4.TabIndex = 59;
            this.tbStripExpansion4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion4.Validated += new System.EventHandler(this.textEdit21_Validated);
            // 
            // tbStripExpansion3
            // 
            this.tbStripExpansion3.EnterMoveNextControl = true;
            this.tbStripExpansion3.Location = new System.Drawing.Point(176, 146);
            this.tbStripExpansion3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripExpansion3.MenuManager = this.barManager1;
            this.tbStripExpansion3.Name = "tbStripExpansion3";
            this.tbStripExpansion3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion3.Size = new System.Drawing.Size(58, 22);
            this.tbStripExpansion3.TabIndex = 56;
            this.tbStripExpansion3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion3.Validated += new System.EventHandler(this.textEdit22_Validated);
            // 
            // tbStripExpansion2
            // 
            this.tbStripExpansion2.EnterMoveNextControl = true;
            this.tbStripExpansion2.Location = new System.Drawing.Point(176, 114);
            this.tbStripExpansion2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripExpansion2.MenuManager = this.barManager1;
            this.tbStripExpansion2.Name = "tbStripExpansion2";
            this.tbStripExpansion2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion2.Size = new System.Drawing.Size(58, 22);
            this.tbStripExpansion2.TabIndex = 53;
            this.tbStripExpansion2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion2.Validated += new System.EventHandler(this.textEdit34_Validated);
            // 
            // tbStripExpansion1
            // 
            this.tbStripExpansion1.EnterMoveNextControl = true;
            this.tbStripExpansion1.Location = new System.Drawing.Point(176, 82);
            this.tbStripExpansion1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripExpansion1.MenuManager = this.barManager1;
            this.tbStripExpansion1.Name = "tbStripExpansion1";
            this.tbStripExpansion1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripExpansion1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripExpansion1.Size = new System.Drawing.Size(58, 22);
            this.tbStripExpansion1.TabIndex = 50;
            this.tbStripExpansion1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripExpansion1.Validated += new System.EventHandler(this.textEdit35_Validated);
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Options.UseTextOptions = true;
            this.labelControl84.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl84.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl84.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl84.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl84.Location = new System.Drawing.Point(101, 43);
            this.labelControl84.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(68, 32);
            this.labelControl84.TabIndex = 25;
            this.labelControl84.Text = "Strip Width in Ft";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Options.UseTextOptions = true;
            this.labelControl85.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl85.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl85.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl85.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl85.Location = new System.Drawing.Point(30, 30);
            this.labelControl85.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(65, 48);
            this.labelControl85.TabIndex = 24;
            this.labelControl85.Text = "Strip Intverval in Ft";
            // 
            // labelControl86
            // 
            this.labelControl86.Location = new System.Drawing.Point(6, 214);
            this.labelControl86.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(20, 16);
            this.labelControl86.TabIndex = 23;
            this.labelControl86.Text = "S5:";
            // 
            // labelControl87
            // 
            this.labelControl87.Location = new System.Drawing.Point(6, 182);
            this.labelControl87.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(20, 16);
            this.labelControl87.TabIndex = 22;
            this.labelControl87.Text = "S4:";
            // 
            // labelControl88
            // 
            this.labelControl88.Location = new System.Drawing.Point(6, 150);
            this.labelControl88.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(20, 16);
            this.labelControl88.TabIndex = 21;
            this.labelControl88.Text = "S3:";
            // 
            // labelControl89
            // 
            this.labelControl89.Location = new System.Drawing.Point(6, 118);
            this.labelControl89.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(20, 16);
            this.labelControl89.TabIndex = 20;
            this.labelControl89.Text = "S2:";
            // 
            // labelControl90
            // 
            this.labelControl90.Location = new System.Drawing.Point(6, 86);
            this.labelControl90.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(20, 16);
            this.labelControl90.TabIndex = 19;
            this.labelControl90.Text = "S1:";
            // 
            // tbStripWidth5
            // 
            this.tbStripWidth5.EnterMoveNextControl = true;
            this.tbStripWidth5.Location = new System.Drawing.Point(104, 210);
            this.tbStripWidth5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripWidth5.MenuManager = this.barManager1;
            this.tbStripWidth5.Name = "tbStripWidth5";
            this.tbStripWidth5.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth5.Size = new System.Drawing.Size(59, 22);
            this.tbStripWidth5.TabIndex = 61;
            this.tbStripWidth5.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripWidth5.TextChanged += new System.EventHandler(this.textEdit1_TextChanged);
            this.tbStripWidth5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripWidth5.Validated += new System.EventHandler(this.textEdit1_Validated);
            // 
            // tbStripInterval5
            // 
            this.tbStripInterval5.EnterMoveNextControl = true;
            this.tbStripInterval5.Location = new System.Drawing.Point(35, 210);
            this.tbStripInterval5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripInterval5.MenuManager = this.barManager1;
            this.tbStripInterval5.Name = "tbStripInterval5";
            this.tbStripInterval5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval5.Size = new System.Drawing.Size(56, 22);
            this.tbStripInterval5.TabIndex = 60;
            this.tbStripInterval5.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripInterval5.TextChanged += new System.EventHandler(this.textEdit2_TextChanged);
            this.tbStripInterval5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripWidth4
            // 
            this.tbStripWidth4.EnterMoveNextControl = true;
            this.tbStripWidth4.Location = new System.Drawing.Point(104, 178);
            this.tbStripWidth4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripWidth4.MenuManager = this.barManager1;
            this.tbStripWidth4.Name = "tbStripWidth4";
            this.tbStripWidth4.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth4.Size = new System.Drawing.Size(59, 22);
            this.tbStripWidth4.TabIndex = 58;
            this.tbStripWidth4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripWidth4.TextChanged += new System.EventHandler(this.textEdit3_TextChanged);
            this.tbStripWidth4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripWidth4.Validated += new System.EventHandler(this.textEdit3_Validated);
            // 
            // tbStripInterval4
            // 
            this.tbStripInterval4.EnterMoveNextControl = true;
            this.tbStripInterval4.Location = new System.Drawing.Point(35, 178);
            this.tbStripInterval4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripInterval4.MenuManager = this.barManager1;
            this.tbStripInterval4.Name = "tbStripInterval4";
            this.tbStripInterval4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval4.Size = new System.Drawing.Size(56, 22);
            this.tbStripInterval4.TabIndex = 57;
            this.tbStripInterval4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripInterval4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval4.Validated += new System.EventHandler(this.textEdit4_Validated);
            // 
            // tbStripWidth3
            // 
            this.tbStripWidth3.EnterMoveNextControl = true;
            this.tbStripWidth3.Location = new System.Drawing.Point(104, 146);
            this.tbStripWidth3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripWidth3.MenuManager = this.barManager1;
            this.tbStripWidth3.Name = "tbStripWidth3";
            this.tbStripWidth3.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth3.Size = new System.Drawing.Size(59, 22);
            this.tbStripWidth3.TabIndex = 55;
            this.tbStripWidth3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripWidth3.TextChanged += new System.EventHandler(this.textEdit5_TextChanged);
            this.tbStripWidth3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripWidth3.Validated += new System.EventHandler(this.textEdit5_Validated);
            // 
            // tbStripInterval3
            // 
            this.tbStripInterval3.EnterMoveNextControl = true;
            this.tbStripInterval3.Location = new System.Drawing.Point(35, 146);
            this.tbStripInterval3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripInterval3.MenuManager = this.barManager1;
            this.tbStripInterval3.Name = "tbStripInterval3";
            this.tbStripInterval3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval3.Size = new System.Drawing.Size(56, 22);
            this.tbStripInterval3.TabIndex = 54;
            this.tbStripInterval3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripInterval3.TextChanged += new System.EventHandler(this.textEdit6_TextChanged);
            this.tbStripInterval3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripWidth2
            // 
            this.tbStripWidth2.EnterMoveNextControl = true;
            this.tbStripWidth2.Location = new System.Drawing.Point(104, 114);
            this.tbStripWidth2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripWidth2.MenuManager = this.barManager1;
            this.tbStripWidth2.Name = "tbStripWidth2";
            this.tbStripWidth2.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth2.Size = new System.Drawing.Size(59, 22);
            this.tbStripWidth2.TabIndex = 52;
            this.tbStripWidth2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripWidth2.TextChanged += new System.EventHandler(this.textEdit7_TextChanged);
            this.tbStripWidth2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripWidth2.Validated += new System.EventHandler(this.textEdit7_Validated);
            // 
            // tbStripInterval2
            // 
            this.tbStripInterval2.EnterMoveNextControl = true;
            this.tbStripInterval2.Location = new System.Drawing.Point(35, 114);
            this.tbStripInterval2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripInterval2.MenuManager = this.barManager1;
            this.tbStripInterval2.Name = "tbStripInterval2";
            this.tbStripInterval2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval2.Size = new System.Drawing.Size(56, 22);
            this.tbStripInterval2.TabIndex = 51;
            this.tbStripInterval2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripInterval2.TextChanged += new System.EventHandler(this.textEdit8_TextChanged);
            this.tbStripInterval2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbStripWidth1
            // 
            this.tbStripWidth1.EnterMoveNextControl = true;
            this.tbStripWidth1.Location = new System.Drawing.Point(104, 82);
            this.tbStripWidth1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripWidth1.MenuManager = this.barManager1;
            this.tbStripWidth1.Name = "tbStripWidth1";
            this.tbStripWidth1.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbStripWidth1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripWidth1.Size = new System.Drawing.Size(59, 22);
            this.tbStripWidth1.TabIndex = 49;
            this.tbStripWidth1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripWidth1.TextChanged += new System.EventHandler(this.textEdit9_TextChanged);
            this.tbStripWidth1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripWidth1.Validated += new System.EventHandler(this.textEdit9_Validated);
            // 
            // tbStripInterval1
            // 
            this.tbStripInterval1.EnterMoveNextControl = true;
            this.tbStripInterval1.Location = new System.Drawing.Point(35, 82);
            this.tbStripInterval1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStripInterval1.MenuManager = this.barManager1;
            this.tbStripInterval1.Name = "tbStripInterval1";
            this.tbStripInterval1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbStripInterval1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbStripInterval1.Size = new System.Drawing.Size(56, 22);
            this.tbStripInterval1.TabIndex = 48;
            this.tbStripInterval1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbStripInterval1.TextChanged += new System.EventHandler(this.textEdit10_TextChanged);
            this.tbStripInterval1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStripInterval1.Validated += new System.EventHandler(this.textEdit10_Validated);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl83);
            this.groupControl2.Controls.Add(this.labelControl82);
            this.groupControl2.Controls.Add(this.labelControl81);
            this.groupControl2.Controls.Add(this.labelControl80);
            this.groupControl2.Controls.Add(this.labelControl74);
            this.groupControl2.Controls.Add(this.tbAreaBlowup5);
            this.groupControl2.Controls.Add(this.tbAreaSide52);
            this.groupControl2.Controls.Add(this.tbAreaBlowup4);
            this.groupControl2.Controls.Add(this.tbAreaSide42);
            this.groupControl2.Controls.Add(this.tbAreaBlowup3);
            this.groupControl2.Controls.Add(this.tbAreaSide32);
            this.groupControl2.Controls.Add(this.tbAreaBlowup2);
            this.groupControl2.Controls.Add(this.tbAreaSide22);
            this.groupControl2.Controls.Add(this.tbAreaBlowup1);
            this.groupControl2.Controls.Add(this.tbAreaSide12);
            this.groupControl2.Controls.Add(this.tbAreaSide51);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius5);
            this.groupControl2.Controls.Add(this.tbAreaSide41);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius4);
            this.groupControl2.Controls.Add(this.tbAreaSide31);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius3);
            this.groupControl2.Controls.Add(this.tbAreaSide21);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius2);
            this.groupControl2.Controls.Add(this.tbAreaSide11);
            this.groupControl2.Controls.Add(this.tbAreaPlotRadius1);
            this.groupControl2.Controls.Add(this.labelControl73);
            this.groupControl2.Controls.Add(this.labelControl75);
            this.groupControl2.Controls.Add(this.labelControl76);
            this.groupControl2.Controls.Add(this.labelControl77);
            this.groupControl2.Controls.Add(this.labelControl78);
            this.groupControl2.Controls.Add(this.labelControl79);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres5);
            this.groupControl2.Controls.Add(this.tbAreaCode5);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres4);
            this.groupControl2.Controls.Add(this.tbAreaCode4);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres3);
            this.groupControl2.Controls.Add(this.tbAreaCode3);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres2);
            this.groupControl2.Controls.Add(this.tbAreaCode2);
            this.groupControl2.Controls.Add(this.tbAreaPlotAcres1);
            this.groupControl2.Controls.Add(this.tbAreaCode1);
            this.groupControl2.Location = new System.Drawing.Point(219, 223);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(429, 252);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Fixed Area";
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Options.UseTextOptions = true;
            this.labelControl83.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl83.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl83.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl83.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl83.Location = new System.Drawing.Point(363, 43);
            this.labelControl83.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(33, 32);
            this.labelControl83.TabIndex = 50;
            this.labelControl83.Text = "Blow Up";
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Options.UseTextOptions = true;
            this.labelControl82.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl82.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl82.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl82.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl82.Location = new System.Drawing.Point(304, 43);
            this.labelControl82.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(24, 32);
            this.labelControl82.TabIndex = 49;
            this.labelControl82.Text = "Side 2";
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Options.UseTextOptions = true;
            this.labelControl81.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl81.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl81.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl81.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl81.Location = new System.Drawing.Point(236, 43);
            this.labelControl81.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(30, 32);
            this.labelControl81.TabIndex = 48;
            this.labelControl81.Text = "Side 1";
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Options.UseTextOptions = true;
            this.labelControl80.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl80.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl80.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl80.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl80.Location = new System.Drawing.Point(167, 43);
            this.labelControl80.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(50, 32);
            this.labelControl80.TabIndex = 47;
            this.labelControl80.Text = "Plot Radius";
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Options.UseTextOptions = true;
            this.labelControl74.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl74.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl74.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl74.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl74.Location = new System.Drawing.Point(35, 43);
            this.labelControl74.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(55, 32);
            this.labelControl74.TabIndex = 46;
            this.labelControl74.Text = "Area Code";
            // 
            // tbAreaBlowup5
            // 
            this.tbAreaBlowup5.EnterMoveNextControl = true;
            this.tbAreaBlowup5.Location = new System.Drawing.Point(351, 210);
            this.tbAreaBlowup5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaBlowup5.MenuManager = this.barManager1;
            this.tbAreaBlowup5.Name = "tbAreaBlowup5";
            this.tbAreaBlowup5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup5.Properties.ReadOnly = true;
            this.tbAreaBlowup5.Size = new System.Drawing.Size(58, 22);
            this.tbAreaBlowup5.TabIndex = 29;
            // 
            // tbAreaSide52
            // 
            this.tbAreaSide52.EnterMoveNextControl = true;
            this.tbAreaSide52.Location = new System.Drawing.Point(289, 210);
            this.tbAreaSide52.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide52.MenuManager = this.barManager1;
            this.tbAreaSide52.Name = "tbAreaSide52";
            this.tbAreaSide52.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide52.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide52.Size = new System.Drawing.Size(55, 22);
            this.tbAreaSide52.TabIndex = 47;
            this.tbAreaSide52.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide52.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide52.Validated += new System.EventHandler(this.tbAreaSide52_Validated);
            // 
            // tbAreaBlowup4
            // 
            this.tbAreaBlowup4.EnterMoveNextControl = true;
            this.tbAreaBlowup4.Location = new System.Drawing.Point(351, 178);
            this.tbAreaBlowup4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaBlowup4.MenuManager = this.barManager1;
            this.tbAreaBlowup4.Name = "tbAreaBlowup4";
            this.tbAreaBlowup4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup4.Properties.ReadOnly = true;
            this.tbAreaBlowup4.Size = new System.Drawing.Size(58, 22);
            this.tbAreaBlowup4.TabIndex = 23;
            // 
            // tbAreaSide42
            // 
            this.tbAreaSide42.EnterMoveNextControl = true;
            this.tbAreaSide42.Location = new System.Drawing.Point(289, 178);
            this.tbAreaSide42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide42.MenuManager = this.barManager1;
            this.tbAreaSide42.Name = "tbAreaSide42";
            this.tbAreaSide42.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide42.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide42.Size = new System.Drawing.Size(55, 22);
            this.tbAreaSide42.TabIndex = 42;
            this.tbAreaSide42.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide42.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide42.Validated += new System.EventHandler(this.tbAreaSide42_Validated);
            // 
            // tbAreaBlowup3
            // 
            this.tbAreaBlowup3.EnterMoveNextControl = true;
            this.tbAreaBlowup3.Location = new System.Drawing.Point(351, 146);
            this.tbAreaBlowup3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaBlowup3.MenuManager = this.barManager1;
            this.tbAreaBlowup3.Name = "tbAreaBlowup3";
            this.tbAreaBlowup3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup3.Properties.ReadOnly = true;
            this.tbAreaBlowup3.Size = new System.Drawing.Size(58, 22);
            this.tbAreaBlowup3.TabIndex = 17;
            // 
            // tbAreaSide32
            // 
            this.tbAreaSide32.EnterMoveNextControl = true;
            this.tbAreaSide32.Location = new System.Drawing.Point(289, 146);
            this.tbAreaSide32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide32.MenuManager = this.barManager1;
            this.tbAreaSide32.Name = "tbAreaSide32";
            this.tbAreaSide32.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide32.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide32.Size = new System.Drawing.Size(55, 22);
            this.tbAreaSide32.TabIndex = 37;
            this.tbAreaSide32.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide32.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide32.Validated += new System.EventHandler(this.tbAreaSide32_Validated);
            // 
            // tbAreaBlowup2
            // 
            this.tbAreaBlowup2.EnterMoveNextControl = true;
            this.tbAreaBlowup2.Location = new System.Drawing.Point(351, 114);
            this.tbAreaBlowup2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaBlowup2.MenuManager = this.barManager1;
            this.tbAreaBlowup2.Name = "tbAreaBlowup2";
            this.tbAreaBlowup2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup2.Properties.ReadOnly = true;
            this.tbAreaBlowup2.Size = new System.Drawing.Size(58, 22);
            this.tbAreaBlowup2.TabIndex = 11;
            // 
            // tbAreaSide22
            // 
            this.tbAreaSide22.EnterMoveNextControl = true;
            this.tbAreaSide22.Location = new System.Drawing.Point(289, 114);
            this.tbAreaSide22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide22.MenuManager = this.barManager1;
            this.tbAreaSide22.Name = "tbAreaSide22";
            this.tbAreaSide22.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide22.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide22.Size = new System.Drawing.Size(55, 22);
            this.tbAreaSide22.TabIndex = 32;
            this.tbAreaSide22.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide22.Validated += new System.EventHandler(this.tbAreaSide22_Validated);
            // 
            // tbAreaBlowup1
            // 
            this.tbAreaBlowup1.EnterMoveNextControl = true;
            this.tbAreaBlowup1.Location = new System.Drawing.Point(351, 82);
            this.tbAreaBlowup1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaBlowup1.MenuManager = this.barManager1;
            this.tbAreaBlowup1.Name = "tbAreaBlowup1";
            this.tbAreaBlowup1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaBlowup1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaBlowup1.Properties.ReadOnly = true;
            this.tbAreaBlowup1.Size = new System.Drawing.Size(58, 22);
            this.tbAreaBlowup1.TabIndex = 5;
            // 
            // tbAreaSide12
            // 
            this.tbAreaSide12.EnterMoveNextControl = true;
            this.tbAreaSide12.Location = new System.Drawing.Point(289, 82);
            this.tbAreaSide12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide12.MenuManager = this.barManager1;
            this.tbAreaSide12.Name = "tbAreaSide12";
            this.tbAreaSide12.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide12.Size = new System.Drawing.Size(55, 22);
            this.tbAreaSide12.TabIndex = 27;
            this.tbAreaSide12.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide12.Validated += new System.EventHandler(this.tbAreaSide12_Validated);
            // 
            // tbAreaSide51
            // 
            this.tbAreaSide51.EnterMoveNextControl = true;
            this.tbAreaSide51.Location = new System.Drawing.Point(224, 210);
            this.tbAreaSide51.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide51.MenuManager = this.barManager1;
            this.tbAreaSide51.Name = "tbAreaSide51";
            this.tbAreaSide51.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide51.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide51.Size = new System.Drawing.Size(58, 22);
            this.tbAreaSide51.TabIndex = 46;
            this.tbAreaSide51.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide51.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide51.Validated += new System.EventHandler(this.tbAreaSide51_Validated);
            // 
            // tbAreaPlotRadius5
            // 
            this.tbAreaPlotRadius5.EnterMoveNextControl = true;
            this.tbAreaPlotRadius5.Location = new System.Drawing.Point(162, 210);
            this.tbAreaPlotRadius5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotRadius5.MenuManager = this.barManager1;
            this.tbAreaPlotRadius5.Name = "tbAreaPlotRadius5";
            this.tbAreaPlotRadius5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius5.Size = new System.Drawing.Size(55, 22);
            this.tbAreaPlotRadius5.TabIndex = 45;
            this.tbAreaPlotRadius5.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotRadius5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius5.Validated += new System.EventHandler(this.tbAreaPlotRadius5_Validated);
            // 
            // tbAreaSide41
            // 
            this.tbAreaSide41.EnterMoveNextControl = true;
            this.tbAreaSide41.Location = new System.Drawing.Point(224, 178);
            this.tbAreaSide41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide41.MenuManager = this.barManager1;
            this.tbAreaSide41.Name = "tbAreaSide41";
            this.tbAreaSide41.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide41.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide41.Size = new System.Drawing.Size(58, 22);
            this.tbAreaSide41.TabIndex = 41;
            this.tbAreaSide41.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide41.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide41.Validated += new System.EventHandler(this.tbAreaSide41_Validated);
            // 
            // tbAreaPlotRadius4
            // 
            this.tbAreaPlotRadius4.EnterMoveNextControl = true;
            this.tbAreaPlotRadius4.Location = new System.Drawing.Point(162, 178);
            this.tbAreaPlotRadius4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotRadius4.MenuManager = this.barManager1;
            this.tbAreaPlotRadius4.Name = "tbAreaPlotRadius4";
            this.tbAreaPlotRadius4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius4.Size = new System.Drawing.Size(55, 22);
            this.tbAreaPlotRadius4.TabIndex = 40;
            this.tbAreaPlotRadius4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotRadius4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius4.Validated += new System.EventHandler(this.tbAreaPlotRadius4_Validated);
            // 
            // tbAreaSide31
            // 
            this.tbAreaSide31.EnterMoveNextControl = true;
            this.tbAreaSide31.Location = new System.Drawing.Point(224, 146);
            this.tbAreaSide31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide31.MenuManager = this.barManager1;
            this.tbAreaSide31.Name = "tbAreaSide31";
            this.tbAreaSide31.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide31.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide31.Size = new System.Drawing.Size(58, 22);
            this.tbAreaSide31.TabIndex = 36;
            this.tbAreaSide31.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide31.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide31.Validated += new System.EventHandler(this.tbAreaSide31_Validated);
            // 
            // tbAreaPlotRadius3
            // 
            this.tbAreaPlotRadius3.EnterMoveNextControl = true;
            this.tbAreaPlotRadius3.Location = new System.Drawing.Point(162, 146);
            this.tbAreaPlotRadius3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotRadius3.MenuManager = this.barManager1;
            this.tbAreaPlotRadius3.Name = "tbAreaPlotRadius3";
            this.tbAreaPlotRadius3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius3.Size = new System.Drawing.Size(55, 22);
            this.tbAreaPlotRadius3.TabIndex = 35;
            this.tbAreaPlotRadius3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotRadius3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius3.Validated += new System.EventHandler(this.tbAreaPlotRadius3_Validated);
            // 
            // tbAreaSide21
            // 
            this.tbAreaSide21.EnterMoveNextControl = true;
            this.tbAreaSide21.Location = new System.Drawing.Point(224, 114);
            this.tbAreaSide21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide21.MenuManager = this.barManager1;
            this.tbAreaSide21.Name = "tbAreaSide21";
            this.tbAreaSide21.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide21.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide21.Size = new System.Drawing.Size(58, 22);
            this.tbAreaSide21.TabIndex = 31;
            this.tbAreaSide21.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide21.Validated += new System.EventHandler(this.tbAreaSide21_Validated);
            // 
            // tbAreaPlotRadius2
            // 
            this.tbAreaPlotRadius2.EnterMoveNextControl = true;
            this.tbAreaPlotRadius2.Location = new System.Drawing.Point(162, 114);
            this.tbAreaPlotRadius2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotRadius2.MenuManager = this.barManager1;
            this.tbAreaPlotRadius2.Name = "tbAreaPlotRadius2";
            this.tbAreaPlotRadius2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius2.Size = new System.Drawing.Size(55, 22);
            this.tbAreaPlotRadius2.TabIndex = 30;
            this.tbAreaPlotRadius2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotRadius2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius2.Validated += new System.EventHandler(this.tbAreaPlotRadius2_Validated);
            // 
            // tbAreaSide11
            // 
            this.tbAreaSide11.EnterMoveNextControl = true;
            this.tbAreaSide11.Location = new System.Drawing.Point(224, 82);
            this.tbAreaSide11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaSide11.MenuManager = this.barManager1;
            this.tbAreaSide11.Name = "tbAreaSide11";
            this.tbAreaSide11.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaSide11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaSide11.Size = new System.Drawing.Size(58, 22);
            this.tbAreaSide11.TabIndex = 26;
            this.tbAreaSide11.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaSide11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaSide11.Validated += new System.EventHandler(this.tbAreaSide11_Validated);
            // 
            // tbAreaPlotRadius1
            // 
            this.tbAreaPlotRadius1.EnterMoveNextControl = true;
            this.tbAreaPlotRadius1.Location = new System.Drawing.Point(162, 82);
            this.tbAreaPlotRadius1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotRadius1.MenuManager = this.barManager1;
            this.tbAreaPlotRadius1.Name = "tbAreaPlotRadius1";
            this.tbAreaPlotRadius1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbAreaPlotRadius1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotRadius1.Size = new System.Drawing.Size(55, 22);
            this.tbAreaPlotRadius1.TabIndex = 25;
            this.tbAreaPlotRadius1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotRadius1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotRadius1.Validated += new System.EventHandler(this.tbAreaPlotRadius1_Validated);
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Options.UseTextOptions = true;
            this.labelControl73.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl73.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl73.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl73.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl73.Location = new System.Drawing.Point(101, 43);
            this.labelControl73.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(50, 32);
            this.labelControl73.TabIndex = 25;
            this.labelControl73.Text = "Plot Acres";
            // 
            // labelControl75
            // 
            this.labelControl75.Location = new System.Drawing.Point(6, 214);
            this.labelControl75.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(20, 16);
            this.labelControl75.TabIndex = 23;
            this.labelControl75.Text = "A5:";
            // 
            // labelControl76
            // 
            this.labelControl76.Location = new System.Drawing.Point(6, 182);
            this.labelControl76.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(20, 16);
            this.labelControl76.TabIndex = 22;
            this.labelControl76.Text = "A4:";
            // 
            // labelControl77
            // 
            this.labelControl77.Location = new System.Drawing.Point(6, 150);
            this.labelControl77.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(20, 16);
            this.labelControl77.TabIndex = 21;
            this.labelControl77.Text = "A3:";
            // 
            // labelControl78
            // 
            this.labelControl78.Location = new System.Drawing.Point(6, 118);
            this.labelControl78.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(20, 16);
            this.labelControl78.TabIndex = 20;
            this.labelControl78.Text = "A2:";
            // 
            // labelControl79
            // 
            this.labelControl79.Location = new System.Drawing.Point(6, 86);
            this.labelControl79.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(20, 16);
            this.labelControl79.TabIndex = 19;
            this.labelControl79.Text = "A1:";
            // 
            // tbAreaPlotAcres5
            // 
            this.tbAreaPlotAcres5.EnterMoveNextControl = true;
            this.tbAreaPlotAcres5.Location = new System.Drawing.Point(97, 210);
            this.tbAreaPlotAcres5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotAcres5.MenuManager = this.barManager1;
            this.tbAreaPlotAcres5.Name = "tbAreaPlotAcres5";
            this.tbAreaPlotAcres5.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres5.Size = new System.Drawing.Size(58, 22);
            this.tbAreaPlotAcres5.TabIndex = 44;
            this.tbAreaPlotAcres5.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotAcres5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres5.Validated += new System.EventHandler(this.tbAreaPlotAcres5_Validated);
            // 
            // tbAreaCode5
            // 
            this.tbAreaCode5.EnterMoveNextControl = true;
            this.tbAreaCode5.Location = new System.Drawing.Point(35, 210);
            this.tbAreaCode5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaCode5.MenuManager = this.barManager1;
            this.tbAreaCode5.Name = "tbAreaCode5";
            this.tbAreaCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode5.Properties.Items.AddRange(new object[] {
            "",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode5.Size = new System.Drawing.Size(55, 22);
            this.tbAreaCode5.TabIndex = 43;
            this.tbAreaCode5.SelectedValueChanged += new System.EventHandler(this.tbAreaCode5_SelectedValueChanged);
            this.tbAreaCode5.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode5_Validating);
            this.tbAreaCode5.Validated += new System.EventHandler(this.tbAreaCode5_Validated);
            // 
            // tbAreaPlotAcres4
            // 
            this.tbAreaPlotAcres4.EnterMoveNextControl = true;
            this.tbAreaPlotAcres4.Location = new System.Drawing.Point(97, 178);
            this.tbAreaPlotAcres4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotAcres4.MenuManager = this.barManager1;
            this.tbAreaPlotAcres4.Name = "tbAreaPlotAcres4";
            this.tbAreaPlotAcres4.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres4.Size = new System.Drawing.Size(58, 22);
            this.tbAreaPlotAcres4.TabIndex = 39;
            this.tbAreaPlotAcres4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotAcres4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres4.Validated += new System.EventHandler(this.tbAreaPlotAcres4_Validated);
            // 
            // tbAreaCode4
            // 
            this.tbAreaCode4.EnterMoveNextControl = true;
            this.tbAreaCode4.Location = new System.Drawing.Point(35, 178);
            this.tbAreaCode4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaCode4.MenuManager = this.barManager1;
            this.tbAreaCode4.Name = "tbAreaCode4";
            this.tbAreaCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode4.Properties.Items.AddRange(new object[] {
            "",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode4.Size = new System.Drawing.Size(55, 22);
            this.tbAreaCode4.TabIndex = 38;
            this.tbAreaCode4.SelectedValueChanged += new System.EventHandler(this.tbAreaCode4_SelectedValueChanged);
            this.tbAreaCode4.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode4_Validating);
            this.tbAreaCode4.Validated += new System.EventHandler(this.tbAreaCode4_Validated);
            // 
            // tbAreaPlotAcres3
            // 
            this.tbAreaPlotAcres3.EnterMoveNextControl = true;
            this.tbAreaPlotAcres3.Location = new System.Drawing.Point(97, 146);
            this.tbAreaPlotAcres3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotAcres3.MenuManager = this.barManager1;
            this.tbAreaPlotAcres3.Name = "tbAreaPlotAcres3";
            this.tbAreaPlotAcres3.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres3.Size = new System.Drawing.Size(58, 22);
            this.tbAreaPlotAcres3.TabIndex = 34;
            this.tbAreaPlotAcres3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotAcres3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres3.Validated += new System.EventHandler(this.tbAreaPlotAcres3_Validated);
            // 
            // tbAreaCode3
            // 
            this.tbAreaCode3.EnterMoveNextControl = true;
            this.tbAreaCode3.Location = new System.Drawing.Point(35, 146);
            this.tbAreaCode3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaCode3.MenuManager = this.barManager1;
            this.tbAreaCode3.Name = "tbAreaCode3";
            this.tbAreaCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode3.Properties.Items.AddRange(new object[] {
            "",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode3.Size = new System.Drawing.Size(55, 22);
            this.tbAreaCode3.TabIndex = 33;
            this.tbAreaCode3.SelectedValueChanged += new System.EventHandler(this.tbAreaCode3_SelectedValueChanged);
            this.tbAreaCode3.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode3_Validating);
            this.tbAreaCode3.Validated += new System.EventHandler(this.tbAreaCode3_Validated);
            // 
            // tbAreaPlotAcres2
            // 
            this.tbAreaPlotAcres2.EnterMoveNextControl = true;
            this.tbAreaPlotAcres2.Location = new System.Drawing.Point(97, 114);
            this.tbAreaPlotAcres2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotAcres2.MenuManager = this.barManager1;
            this.tbAreaPlotAcres2.Name = "tbAreaPlotAcres2";
            this.tbAreaPlotAcres2.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres2.Size = new System.Drawing.Size(58, 22);
            this.tbAreaPlotAcres2.TabIndex = 29;
            this.tbAreaPlotAcres2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotAcres2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres2.Validated += new System.EventHandler(this.tbAreaPlotAcres2_Validated);
            // 
            // tbAreaCode2
            // 
            this.tbAreaCode2.EnterMoveNextControl = true;
            this.tbAreaCode2.Location = new System.Drawing.Point(35, 114);
            this.tbAreaCode2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaCode2.MenuManager = this.barManager1;
            this.tbAreaCode2.Name = "tbAreaCode2";
            this.tbAreaCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode2.Properties.Items.AddRange(new object[] {
            "",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode2.Size = new System.Drawing.Size(55, 22);
            this.tbAreaCode2.TabIndex = 28;
            this.tbAreaCode2.SelectedValueChanged += new System.EventHandler(this.tbAreaCode2_SelectedValueChanged);
            this.tbAreaCode2.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode2_Validating);
            this.tbAreaCode2.Validated += new System.EventHandler(this.tbAreaCode2_Validated);
            // 
            // tbAreaPlotAcres1
            // 
            this.tbAreaPlotAcres1.EnterMoveNextControl = true;
            this.tbAreaPlotAcres1.Location = new System.Drawing.Point(97, 82);
            this.tbAreaPlotAcres1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaPlotAcres1.MenuManager = this.barManager1;
            this.tbAreaPlotAcres1.Name = "tbAreaPlotAcres1";
            this.tbAreaPlotAcres1.Properties.DisplayFormat.FormatString = "{0:#.###}";
            this.tbAreaPlotAcres1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbAreaPlotAcres1.Size = new System.Drawing.Size(58, 22);
            this.tbAreaPlotAcres1.TabIndex = 24;
            this.tbAreaPlotAcres1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbAreaPlotAcres1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbAreaPlotAcres1.Validated += new System.EventHandler(this.tbAreaPlotAcres1_Validated);
            // 
            // tbAreaCode1
            // 
            this.tbAreaCode1.EnterMoveNextControl = true;
            this.tbAreaCode1.Location = new System.Drawing.Point(35, 82);
            this.tbAreaCode1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAreaCode1.MenuManager = this.barManager1;
            this.tbAreaCode1.Name = "tbAreaCode1";
            this.tbAreaCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tbAreaCode1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbAreaCode1.Properties.Items.AddRange(new object[] {
            "",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "R1",
            "R2",
            "R3",
            "R4",
            "R5"});
            this.tbAreaCode1.Size = new System.Drawing.Size(55, 22);
            this.tbAreaCode1.TabIndex = 23;
            this.tbAreaCode1.SelectedValueChanged += new System.EventHandler(this.tbAreaCode1_SelectedValueChanged);
            this.tbAreaCode1.Validating += new System.ComponentModel.CancelEventHandler(this.tbAreaCode1_Validating);
            this.tbAreaCode1.Validated += new System.EventHandler(this.tbAreaCode1_Validated);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl72);
            this.groupControl1.Controls.Add(this.labelControl71);
            this.groupControl1.Controls.Add(this.labelControl70);
            this.groupControl1.Controls.Add(this.labelControl69);
            this.groupControl1.Controls.Add(this.labelControl68);
            this.groupControl1.Controls.Add(this.labelControl67);
            this.groupControl1.Controls.Add(this.labelControl66);
            this.groupControl1.Controls.Add(this.tbBafLimit5);
            this.groupControl1.Controls.Add(this.tbBaf5);
            this.groupControl1.Controls.Add(this.tbBafLimit4);
            this.groupControl1.Controls.Add(this.tbBaf4);
            this.groupControl1.Controls.Add(this.tbBafLimit3);
            this.groupControl1.Controls.Add(this.tbBaf3);
            this.groupControl1.Controls.Add(this.tbBafLimit2);
            this.groupControl1.Controls.Add(this.tbBaf2);
            this.groupControl1.Controls.Add(this.tbBafLimit1);
            this.groupControl1.Controls.Add(this.tbBaf1);
            this.groupControl1.Location = new System.Drawing.Point(3, 223);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(209, 252);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Proportional Plot (BAF)";
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Options.UseTextOptions = true;
            this.labelControl72.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl72.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControl72.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl72.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl72.Location = new System.Drawing.Point(105, 43);
            this.labelControl72.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(89, 32);
            this.labelControl72.TabIndex = 25;
            this.labelControl72.Text = "Plot Radius Factor Calc";
            // 
            // labelControl71
            // 
            this.labelControl71.Location = new System.Drawing.Point(41, 59);
            this.labelControl71.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(55, 16);
            this.labelControl71.TabIndex = 24;
            this.labelControl71.Text = "Input BAF";
            // 
            // labelControl70
            // 
            this.labelControl70.Location = new System.Drawing.Point(6, 214);
            this.labelControl70.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(19, 16);
            this.labelControl70.TabIndex = 23;
            this.labelControl70.Text = "B5:";
            // 
            // labelControl69
            // 
            this.labelControl69.Location = new System.Drawing.Point(6, 182);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(19, 16);
            this.labelControl69.TabIndex = 22;
            this.labelControl69.Text = "B4:";
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(6, 150);
            this.labelControl68.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(19, 16);
            this.labelControl68.TabIndex = 21;
            this.labelControl68.Text = "B3:";
            // 
            // labelControl67
            // 
            this.labelControl67.Location = new System.Drawing.Point(6, 118);
            this.labelControl67.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(19, 16);
            this.labelControl67.TabIndex = 20;
            this.labelControl67.Text = "B2:";
            // 
            // labelControl66
            // 
            this.labelControl66.Location = new System.Drawing.Point(6, 86);
            this.labelControl66.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(19, 16);
            this.labelControl66.TabIndex = 19;
            this.labelControl66.Text = "B1:";
            // 
            // tbBafLimit5
            // 
            this.tbBafLimit5.Location = new System.Drawing.Point(113, 210);
            this.tbBafLimit5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBafLimit5.MenuManager = this.barManager1;
            this.tbBafLimit5.Name = "tbBafLimit5";
            this.tbBafLimit5.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit5.Properties.ReadOnly = true;
            this.tbBafLimit5.Size = new System.Drawing.Size(71, 22);
            this.tbBafLimit5.TabIndex = 18;
            // 
            // tbBaf5
            // 
            this.tbBaf5.EnterMoveNextControl = true;
            this.tbBaf5.Location = new System.Drawing.Point(35, 210);
            this.tbBaf5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBaf5.MenuManager = this.barManager1;
            this.tbBaf5.Name = "tbBaf5";
            this.tbBaf5.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf5.Size = new System.Drawing.Size(68, 22);
            this.tbBaf5.TabIndex = 22;
            this.tbBaf5.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbBaf5.TextChanged += new System.EventHandler(this.tbBaf5_TextChanged);
            this.tbBaf5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbBafLimit4
            // 
            this.tbBafLimit4.Location = new System.Drawing.Point(113, 178);
            this.tbBafLimit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBafLimit4.MenuManager = this.barManager1;
            this.tbBafLimit4.Name = "tbBafLimit4";
            this.tbBafLimit4.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit4.Properties.ReadOnly = true;
            this.tbBafLimit4.Size = new System.Drawing.Size(71, 22);
            this.tbBafLimit4.TabIndex = 16;
            // 
            // tbBaf4
            // 
            this.tbBaf4.EnterMoveNextControl = true;
            this.tbBaf4.Location = new System.Drawing.Point(35, 178);
            this.tbBaf4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBaf4.MenuManager = this.barManager1;
            this.tbBaf4.Name = "tbBaf4";
            this.tbBaf4.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf4.Size = new System.Drawing.Size(68, 22);
            this.tbBaf4.TabIndex = 21;
            this.tbBaf4.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbBaf4.TextChanged += new System.EventHandler(this.tbBaf4_TextChanged);
            this.tbBaf4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbBafLimit3
            // 
            this.tbBafLimit3.Location = new System.Drawing.Point(113, 146);
            this.tbBafLimit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBafLimit3.MenuManager = this.barManager1;
            this.tbBafLimit3.Name = "tbBafLimit3";
            this.tbBafLimit3.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit3.Properties.ReadOnly = true;
            this.tbBafLimit3.Size = new System.Drawing.Size(71, 22);
            this.tbBafLimit3.TabIndex = 14;
            // 
            // tbBaf3
            // 
            this.tbBaf3.EnterMoveNextControl = true;
            this.tbBaf3.Location = new System.Drawing.Point(35, 146);
            this.tbBaf3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBaf3.MenuManager = this.barManager1;
            this.tbBaf3.Name = "tbBaf3";
            this.tbBaf3.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf3.Size = new System.Drawing.Size(68, 22);
            this.tbBaf3.TabIndex = 20;
            this.tbBaf3.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbBaf3.TextChanged += new System.EventHandler(this.tbBaf3_TextChanged);
            this.tbBaf3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbBafLimit2
            // 
            this.tbBafLimit2.Location = new System.Drawing.Point(113, 114);
            this.tbBafLimit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBafLimit2.MenuManager = this.barManager1;
            this.tbBafLimit2.Name = "tbBafLimit2";
            this.tbBafLimit2.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit2.Properties.ReadOnly = true;
            this.tbBafLimit2.Size = new System.Drawing.Size(71, 22);
            this.tbBafLimit2.TabIndex = 12;
            // 
            // tbBaf2
            // 
            this.tbBaf2.EnterMoveNextControl = true;
            this.tbBaf2.Location = new System.Drawing.Point(35, 114);
            this.tbBaf2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBaf2.MenuManager = this.barManager1;
            this.tbBaf2.Name = "tbBaf2";
            this.tbBaf2.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf2.Size = new System.Drawing.Size(68, 22);
            this.tbBaf2.TabIndex = 19;
            this.tbBaf2.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbBaf2.TextChanged += new System.EventHandler(this.tbBaf2_TextChanged);
            this.tbBaf2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbBafLimit1
            // 
            this.tbBafLimit1.Location = new System.Drawing.Point(113, 82);
            this.tbBafLimit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBafLimit1.MenuManager = this.barManager1;
            this.tbBafLimit1.Name = "tbBafLimit1";
            this.tbBafLimit1.Properties.DisplayFormat.FormatString = "{0:#.####}";
            this.tbBafLimit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBafLimit1.Properties.ReadOnly = true;
            this.tbBafLimit1.Size = new System.Drawing.Size(71, 22);
            this.tbBafLimit1.TabIndex = 10;
            // 
            // tbBaf1
            // 
            this.tbBaf1.EnterMoveNextControl = true;
            this.tbBaf1.Location = new System.Drawing.Point(35, 82);
            this.tbBaf1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBaf1.MenuManager = this.barManager1;
            this.tbBaf1.Name = "tbBaf1";
            this.tbBaf1.Properties.DisplayFormat.FormatString = "{0:#.##}";
            this.tbBaf1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf1.Properties.EditFormat.FormatString = "{0:#.##}";
            this.tbBaf1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.tbBaf1.Size = new System.Drawing.Size(68, 22);
            this.tbBaf1.TabIndex = 18;
            this.tbBaf1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.tb_EditValueChanging);
            this.tbBaf1.TextChanged += new System.EventHandler(this.tbBaf1_TextChanged);
            this.tbBaf1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbBaf1.Validated += new System.EventHandler(this.tbBaf1_Validated);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.cbSource);
            this.panelControl5.Controls.Add(this.labelControl111);
            this.panelControl5.Controls.Add(this.tbAutoSegmentLength);
            this.panelControl5.Controls.Add(this.labelControl65);
            this.panelControl5.Controls.Add(this.labelControl64);
            this.panelControl5.Controls.Add(this.cbGrowthModel);
            this.panelControl5.Controls.Add(this.labelControl63);
            this.panelControl5.Controls.Add(this.labelControl62);
            this.panelControl5.Controls.Add(this.labelControl61);
            this.panelControl5.Controls.Add(this.labelControl60);
            this.panelControl5.Controls.Add(this.cbCostTable);
            this.panelControl5.Controls.Add(this.labelControl59);
            this.panelControl5.Controls.Add(this.cbPriceTable);
            this.panelControl5.Controls.Add(this.labelControl58);
            this.panelControl5.Controls.Add(this.cbGradeTable);
            this.panelControl5.Controls.Add(this.labelControl57);
            this.panelControl5.Controls.Add(this.cbSortTable);
            this.panelControl5.Controls.Add(this.labelControl56);
            this.panelControl5.Controls.Add(this.cbSpeciesTable);
            this.panelControl5.Controls.Add(this.cbNonStockedTable);
            this.panelControl5.Controls.Add(this.cbNonTimberedTable);
            this.panelControl5.Controls.Add(this.cbHarvestTable);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 80);
            this.panelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1388, 135);
            this.panelControl5.TabIndex = 7;
            // 
            // cbSource
            // 
            this.cbSource.Location = new System.Drawing.Point(1064, 92);
            this.cbSource.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSource.MenuManager = this.barManager1;
            this.cbSource.Name = "cbSource";
            this.cbSource.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSource.Size = new System.Drawing.Size(84, 22);
            this.cbSource.TabIndex = 26;
            // 
            // labelControl111
            // 
            this.labelControl111.Location = new System.Drawing.Point(1008, 96);
            this.labelControl111.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(45, 16);
            this.labelControl111.TabIndex = 25;
            this.labelControl111.Text = "Source:";
            // 
            // tbAutoSegmentLength
            // 
            this.tbAutoSegmentLength.Location = new System.Drawing.Point(1064, 30);
            this.tbAutoSegmentLength.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbAutoSegmentLength.MenuManager = this.barManager1;
            this.tbAutoSegmentLength.Name = "tbAutoSegmentLength";
            this.tbAutoSegmentLength.Size = new System.Drawing.Size(37, 22);
            this.tbAutoSegmentLength.TabIndex = 5;
            this.tbAutoSegmentLength.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl65
            // 
            this.labelControl65.Location = new System.Drawing.Point(925, 33);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(128, 16);
            this.labelControl65.TabIndex = 24;
            this.labelControl65.Text = "Auto Segment Length:";
            // 
            // labelControl64
            // 
            this.labelControl64.Location = new System.Drawing.Point(769, 69);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(79, 16);
            this.labelControl64.TabIndex = 23;
            this.labelControl64.Text = "Growth Model";
            // 
            // cbGrowthModel
            // 
            this.cbGrowthModel.Location = new System.Drawing.Point(734, 92);
            this.cbGrowthModel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbGrowthModel.MenuManager = this.barManager1;
            this.cbGrowthModel.Name = "cbGrowthModel";
            this.cbGrowthModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGrowthModel.Size = new System.Drawing.Size(156, 22);
            this.cbGrowthModel.TabIndex = 17;
            // 
            // labelControl63
            // 
            this.labelControl63.Location = new System.Drawing.Point(588, 69);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(79, 16);
            this.labelControl63.TabIndex = 21;
            this.labelControl63.Text = "Harvest Table";
            this.labelControl63.Visible = false;
            // 
            // labelControl62
            // 
            this.labelControl62.Location = new System.Drawing.Point(199, 69);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(118, 16);
            this.labelControl62.TabIndex = 19;
            this.labelControl62.Text = "Non-Timbered Table";
            // 
            // labelControl61
            // 
            this.labelControl61.Location = new System.Drawing.Point(29, 69);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(108, 16);
            this.labelControl61.TabIndex = 17;
            this.labelControl61.Text = "Non-Stocked Table";
            // 
            // labelControl60
            // 
            this.labelControl60.Location = new System.Drawing.Point(769, 6);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(61, 16);
            this.labelControl60.TabIndex = 15;
            this.labelControl60.Text = "Cost Table";
            // 
            // cbCostTable
            // 
            this.cbCostTable.Location = new System.Drawing.Point(734, 30);
            this.cbCostTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCostTable.MenuManager = this.barManager1;
            this.cbCostTable.Name = "cbCostTable";
            this.cbCostTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCostTable.Size = new System.Drawing.Size(156, 22);
            this.cbCostTable.TabIndex = 13;
            this.cbCostTable.Validating += new System.ComponentModel.CancelEventHandler(this.cbCostTable_Validating);
            // 
            // labelControl59
            // 
            this.labelControl59.Location = new System.Drawing.Point(588, 6);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(64, 16);
            this.labelControl59.TabIndex = 13;
            this.labelControl59.Text = "Price Table";
            // 
            // cbPriceTable
            // 
            this.cbPriceTable.Location = new System.Drawing.Point(553, 30);
            this.cbPriceTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbPriceTable.MenuManager = this.barManager1;
            this.cbPriceTable.Name = "cbPriceTable";
            this.cbPriceTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPriceTable.Size = new System.Drawing.Size(156, 22);
            this.cbPriceTable.TabIndex = 12;
            this.cbPriceTable.Validating += new System.ComponentModel.CancelEventHandler(this.cbPriceTable_Validating);
            // 
            // labelControl58
            // 
            this.labelControl58.Location = new System.Drawing.Point(406, 6);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(70, 16);
            this.labelControl58.TabIndex = 11;
            this.labelControl58.Text = "Grade Table";
            // 
            // cbGradeTable
            // 
            this.cbGradeTable.Location = new System.Drawing.Point(371, 30);
            this.cbGradeTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbGradeTable.MenuManager = this.barManager1;
            this.cbGradeTable.Name = "cbGradeTable";
            this.cbGradeTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGradeTable.Size = new System.Drawing.Size(156, 22);
            this.cbGradeTable.TabIndex = 11;
            this.cbGradeTable.Validating += new System.ComponentModel.CancelEventHandler(this.cbGradeTable_Validating);
            // 
            // labelControl57
            // 
            this.labelControl57.Location = new System.Drawing.Point(219, 6);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(60, 16);
            this.labelControl57.TabIndex = 9;
            this.labelControl57.Text = "Sort Table";
            // 
            // cbSortTable
            // 
            this.cbSortTable.Location = new System.Drawing.Point(184, 30);
            this.cbSortTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSortTable.MenuManager = this.barManager1;
            this.cbSortTable.Name = "cbSortTable";
            this.cbSortTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSortTable.Size = new System.Drawing.Size(156, 22);
            this.cbSortTable.TabIndex = 10;
            this.cbSortTable.Validating += new System.ComponentModel.CancelEventHandler(this.cbSortTable_Validating);
            // 
            // labelControl56
            // 
            this.labelControl56.Location = new System.Drawing.Point(41, 6);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(80, 16);
            this.labelControl56.TabIndex = 7;
            this.labelControl56.Text = "Species Table";
            // 
            // cbSpeciesTable
            // 
            this.cbSpeciesTable.Location = new System.Drawing.Point(6, 30);
            this.cbSpeciesTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSpeciesTable.MenuManager = this.barManager1;
            this.cbSpeciesTable.Name = "cbSpeciesTable";
            this.cbSpeciesTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSpeciesTable.Size = new System.Drawing.Size(156, 22);
            this.cbSpeciesTable.TabIndex = 9;
            this.cbSpeciesTable.Validating += new System.ComponentModel.CancelEventHandler(this.cbSpeciesTable_Validating);
            // 
            // cbNonStockedTable
            // 
            this.cbNonStockedTable.Location = new System.Drawing.Point(6, 92);
            this.cbNonStockedTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbNonStockedTable.MenuManager = this.barManager1;
            this.cbNonStockedTable.Name = "cbNonStockedTable";
            this.cbNonStockedTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNonStockedTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbNonStockedTable.Properties.DataSource = this.nonStockedBindingSource;
            this.cbNonStockedTable.Properties.DisplayMember = "Code";
            this.cbNonStockedTable.Properties.NullText = "";
            this.cbNonStockedTable.Properties.PopupSizeable = false;
            this.cbNonStockedTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbNonStockedTable.Properties.ValueMember = "Code";
            this.cbNonStockedTable.Size = new System.Drawing.Size(156, 22);
            this.cbNonStockedTable.TabIndex = 14;
            // 
            // nonStockedBindingSource
            // 
            this.nonStockedBindingSource.DataSource = typeof(Project.DAL.Nonstocked);
            // 
            // cbNonTimberedTable
            // 
            this.cbNonTimberedTable.Location = new System.Drawing.Point(184, 92);
            this.cbNonTimberedTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbNonTimberedTable.MenuManager = this.barManager1;
            this.cbNonTimberedTable.Name = "cbNonTimberedTable";
            this.cbNonTimberedTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNonTimberedTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbNonTimberedTable.Properties.DataSource = this.nonTimberedBindingSource;
            this.cbNonTimberedTable.Properties.DisplayMember = "Code";
            this.cbNonTimberedTable.Properties.NullText = "";
            this.cbNonTimberedTable.Properties.PopupSizeable = false;
            this.cbNonTimberedTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbNonTimberedTable.Properties.ValueMember = "Code";
            this.cbNonTimberedTable.Size = new System.Drawing.Size(156, 22);
            this.cbNonTimberedTable.TabIndex = 15;
            // 
            // nonTimberedBindingSource
            // 
            this.nonTimberedBindingSource.DataSource = typeof(Project.DAL.Nontimbered);
            // 
            // cbHarvestTable
            // 
            this.cbHarvestTable.Location = new System.Drawing.Point(553, 92);
            this.cbHarvestTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbHarvestTable.MenuManager = this.barManager1;
            this.cbHarvestTable.Name = "cbHarvestTable";
            this.cbHarvestTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHarvestTable.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbHarvestTable.Properties.DataSource = this.harvestBindingSource;
            this.cbHarvestTable.Properties.DisplayMember = "DisplayCode";
            this.cbHarvestTable.Properties.NullText = "";
            this.cbHarvestTable.Properties.PopupSizeable = false;
            this.cbHarvestTable.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbHarvestTable.Properties.ValueMember = "DisplayCode";
            this.cbHarvestTable.Size = new System.Drawing.Size(156, 22);
            this.cbHarvestTable.TabIndex = 16;
            this.cbHarvestTable.Visible = false;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.labelControl107);
            this.panelControl8.Controls.Add(this.tbPlots);
            this.panelControl8.Controls.Add(this.btnGrow);
            this.panelControl8.Controls.Add(this.tbGrownToDate);
            this.panelControl8.Controls.Add(this.tbInputDate);
            this.panelControl8.Controls.Add(this.labelControl55);
            this.panelControl8.Controls.Add(this.labelControl54);
            this.panelControl8.Controls.Add(this.labelControl50);
            this.panelControl8.Controls.Add(this.tbNetAcres);
            this.panelControl8.Controls.Add(this.labelControl49);
            this.panelControl8.Controls.Add(this.tbSection);
            this.panelControl8.Controls.Add(this.labelControl48);
            this.panelControl8.Controls.Add(this.tbRange);
            this.panelControl8.Controls.Add(this.labelControl47);
            this.panelControl8.Controls.Add(this.tbTownship);
            this.panelControl8.Controls.Add(this.labelControl46);
            this.panelControl8.Controls.Add(this.tbStand);
            this.panelControl8.Controls.Add(this.labelControl45);
            this.panelControl8.Controls.Add(this.tbTract);
            this.panelControl8.Controls.Add(this.labelControl51);
            this.panelControl8.Controls.Add(this.labelControl52);
            this.panelControl8.Controls.Add(this.labelControl53);
            this.panelControl8.Controls.Add(this.tbProject);
            this.panelControl8.Controls.Add(this.cbState);
            this.panelControl8.Controls.Add(this.cbCounty);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1388, 80);
            this.panelControl8.TabIndex = 6;
            // 
            // labelControl107
            // 
            this.labelControl107.Location = new System.Drawing.Point(875, 6);
            this.labelControl107.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(27, 16);
            this.labelControl107.TabIndex = 24;
            this.labelControl107.Text = "Plots";
            // 
            // tbPlots
            // 
            this.tbPlots.EnterMoveNextControl = true;
            this.tbPlots.Location = new System.Drawing.Point(862, 36);
            this.tbPlots.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlots.MenuManager = this.barManager1;
            this.tbPlots.Name = "tbPlots";
            this.tbPlots.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbPlots.Size = new System.Drawing.Size(54, 22);
            this.tbPlots.TabIndex = 23;
            this.tbPlots.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // btnGrow
            // 
            this.btnGrow.Location = new System.Drawing.Point(1197, 33);
            this.btnGrow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGrow.Name = "btnGrow";
            this.btnGrow.Size = new System.Drawing.Size(87, 28);
            this.btnGrow.TabIndex = 22;
            this.btnGrow.Text = "Grow";
            // 
            // tbGrownToDate
            // 
            this.tbGrownToDate.Location = new System.Drawing.Point(1069, 36);
            this.tbGrownToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbGrownToDate.MenuManager = this.barManager1;
            this.tbGrownToDate.Name = "tbGrownToDate";
            this.tbGrownToDate.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbGrownToDate.Properties.DisplayFormat.FormatString = "d";
            this.tbGrownToDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tbGrownToDate.Size = new System.Drawing.Size(117, 22);
            this.tbGrownToDate.TabIndex = 8;
            this.tbGrownToDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // tbInputDate
            // 
            this.tbInputDate.Location = new System.Drawing.Point(1069, 4);
            this.tbInputDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbInputDate.MenuManager = this.barManager1;
            this.tbInputDate.Name = "tbInputDate";
            this.tbInputDate.Properties.DisplayFormat.FormatString = "d";
            this.tbInputDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tbInputDate.Size = new System.Drawing.Size(117, 22);
            this.tbInputDate.TabIndex = 7;
            this.tbInputDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            // 
            // labelControl55
            // 
            this.labelControl55.Location = new System.Drawing.Point(967, 39);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(98, 16);
            this.labelControl55.TabIndex = 19;
            this.labelControl55.Text = "Stand Grown To:";
            // 
            // labelControl54
            // 
            this.labelControl54.Location = new System.Drawing.Point(943, 6);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(120, 16);
            this.labelControl54.TabIndex = 18;
            this.labelControl54.Text = "Cruise or Input Date:";
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(784, 6);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(55, 16);
            this.labelControl50.TabIndex = 17;
            this.labelControl50.Text = "Net Acres";
            // 
            // tbNetAcres
            // 
            this.tbNetAcres.EnterMoveNextControl = true;
            this.tbNetAcres.Location = new System.Drawing.Point(757, 36);
            this.tbNetAcres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbNetAcres.MenuManager = this.barManager1;
            this.tbNetAcres.Name = "tbNetAcres";
            this.tbNetAcres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbNetAcres.Size = new System.Drawing.Size(87, 22);
            this.tbNetAcres.TabIndex = 6;
            this.tbNetAcres.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbNetAcres.Validating += new System.ComponentModel.CancelEventHandler(this.tbNetAcres_Validating);
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(703, 6);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(21, 16);
            this.labelControl49.TabIndex = 15;
            this.labelControl49.Text = "Sec";
            // 
            // tbSection
            // 
            this.tbSection.EnterMoveNextControl = true;
            this.tbSection.Location = new System.Drawing.Point(686, 36);
            this.tbSection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSection.MenuManager = this.barManager1;
            this.tbSection.Name = "tbSection";
            this.tbSection.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSection.Properties.MaxLength = 2;
            this.tbSection.Size = new System.Drawing.Size(54, 22);
            this.tbSection.TabIndex = 5;
            this.tbSection.TextChanged += new System.EventHandler(this.tbSection_TextChanged);
            this.tbSection.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbSection.Validating += new System.ComponentModel.CancelEventHandler(this.tbSection_Validating);
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(633, 6);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(22, 16);
            this.labelControl48.TabIndex = 13;
            this.labelControl48.Text = "Rge";
            // 
            // tbRange
            // 
            this.tbRange.EnterMoveNextControl = true;
            this.tbRange.Location = new System.Drawing.Point(617, 36);
            this.tbRange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbRange.MenuManager = this.barManager1;
            this.tbRange.Name = "tbRange";
            this.tbRange.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbRange.Properties.MaxLength = 3;
            this.tbRange.Size = new System.Drawing.Size(54, 22);
            this.tbRange.TabIndex = 4;
            this.tbRange.TextChanged += new System.EventHandler(this.tbRange_TextChanged);
            this.tbRange.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbRange.Validating += new System.ComponentModel.CancelEventHandler(this.tbRange_Validating);
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(560, 6);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(25, 16);
            this.labelControl47.TabIndex = 11;
            this.labelControl47.Text = "Twn";
            // 
            // tbTownship
            // 
            this.tbTownship.EnterMoveNextControl = true;
            this.tbTownship.Location = new System.Drawing.Point(542, 36);
            this.tbTownship.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTownship.MenuManager = this.barManager1;
            this.tbTownship.Name = "tbTownship";
            this.tbTownship.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTownship.Properties.MaxLength = 3;
            this.tbTownship.Size = new System.Drawing.Size(59, 22);
            this.tbTownship.TabIndex = 3;
            this.tbTownship.TextChanged += new System.EventHandler(this.tbTownship_TextChanged);
            this.tbTownship.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTownship.Validating += new System.ComponentModel.CancelEventHandler(this.tbTownship_Validating);
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(470, 6);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(46, 16);
            this.labelControl46.TabIndex = 9;
            this.labelControl46.Text = "Stand #";
            // 
            // tbStand
            // 
            this.tbStand.EnterMoveNextControl = true;
            this.tbStand.Location = new System.Drawing.Point(455, 36);
            this.tbStand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStand.MenuManager = this.barManager1;
            this.tbStand.Name = "tbStand";
            this.tbStand.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbStand.Properties.MaxLength = 4;
            this.tbStand.Size = new System.Drawing.Size(72, 22);
            this.tbStand.TabIndex = 2;
            this.tbStand.TextChanged += new System.EventHandler(this.tbStand_TextChanged);
            this.tbStand.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbStand.Validating += new System.ComponentModel.CancelEventHandler(this.tbStand_Validating);
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(351, 6);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(30, 16);
            this.labelControl45.TabIndex = 7;
            this.labelControl45.Text = "Tract";
            // 
            // tbTract
            // 
            this.tbTract.EnterMoveNextControl = true;
            this.tbTract.Location = new System.Drawing.Point(307, 36);
            this.tbTract.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTract.MenuManager = this.barManager1;
            this.tbTract.Name = "tbTract";
            this.tbTract.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbTract.Properties.MaxLength = 12;
            this.tbTract.Size = new System.Drawing.Size(129, 22);
            this.tbTract.TabIndex = 1;
            this.tbTract.TextChanged += new System.EventHandler(this.tbTract_TextChanged);
            this.tbTract.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Editor_MouseUp);
            this.tbTract.Validating += new System.ComponentModel.CancelEventHandler(this.tbTract_Validating);
            // 
            // labelControl51
            // 
            this.labelControl51.Location = new System.Drawing.Point(223, 6);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(39, 16);
            this.labelControl51.TabIndex = 5;
            this.labelControl51.Text = "County";
            // 
            // labelControl52
            // 
            this.labelControl52.Location = new System.Drawing.Point(148, 6);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(30, 16);
            this.labelControl52.TabIndex = 4;
            this.labelControl52.Text = "State";
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(42, 6);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(40, 16);
            this.labelControl53.TabIndex = 3;
            this.labelControl53.Text = "Project";
            // 
            // tbProject
            // 
            this.tbProject.Location = new System.Drawing.Point(8, 36);
            this.tbProject.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbProject.MenuManager = this.barManager1;
            this.tbProject.Name = "tbProject";
            this.tbProject.Properties.ReadOnly = true;
            this.tbProject.Size = new System.Drawing.Size(117, 22);
            this.tbProject.TabIndex = 0;
            // 
            // cbState
            // 
            this.cbState.EnterMoveNextControl = true;
            this.cbState.Location = new System.Drawing.Point(134, 36);
            this.cbState.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbState.MenuManager = this.barManager1;
            this.cbState.Name = "cbState";
            this.cbState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbState.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbr", "State", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbState.Properties.DataSource = this.stateBindingSource;
            this.cbState.Properties.DisplayMember = "Abbr";
            this.cbState.Properties.NullText = "";
            this.cbState.Properties.PopupSizeable = false;
            this.cbState.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbState.Properties.ValueMember = "Abbr";
            this.cbState.Size = new System.Drawing.Size(62, 22);
            this.cbState.TabIndex = 1;
            this.cbState.TextChanged += new System.EventHandler(this.cbState_TextChanged);
            // 
            // stateBindingSource
            // 
            this.stateBindingSource.DataSource = typeof(Projects.DAL.State);
            // 
            // cbCounty
            // 
            this.cbCounty.EnterMoveNextControl = true;
            this.cbCounty.Location = new System.Drawing.Point(215, 36);
            this.cbCounty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCounty.MenuManager = this.barManager1;
            this.cbCounty.Name = "cbCounty";
            this.cbCounty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCounty.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountyAbbr", "County", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountyName", "Name", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbCounty.Properties.DataSource = this.stateCountyBindingSource;
            this.cbCounty.Properties.DisplayMember = "CountyAbbr";
            this.cbCounty.Properties.NullText = "";
            this.cbCounty.Properties.PopupSizeable = false;
            this.cbCounty.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbCounty.Properties.ValueMember = "CountyAbbr";
            this.cbCounty.Size = new System.Drawing.Size(62, 22);
            this.cbCounty.TabIndex = 2;
            // 
            // stateCountyBindingSource
            // 
            this.stateCountyBindingSource.DataSource = typeof(Projects.DAL.StateCounty);
            // 
            // tabNavigationPagePlotLocation
            // 
            this.tabNavigationPagePlotLocation.Caption = "Plot Location";
            this.tabNavigationPagePlotLocation.Controls.Add(this.gridControlPlots);
            this.tabNavigationPagePlotLocation.Controls.Add(this.panelControl1);
            this.tabNavigationPagePlotLocation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPagePlotLocation.Name = "tabNavigationPagePlotLocation";
            this.tabNavigationPagePlotLocation.Size = new System.Drawing.Size(1624, 921);
            // 
            // gridControlPlots
            // 
            this.gridControlPlots.DataSource = this.plotsBindingSource;
            this.gridControlPlots.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlots.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlPlots.Location = new System.Drawing.Point(0, 84);
            this.gridControlPlots.MainView = this.advBandedGridViewPlots;
            this.gridControlPlots.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlPlots.MenuManager = this.barManager1;
            this.gridControlPlots.Name = "gridControlPlots";
            this.gridControlPlots.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.cbAspect,
            this.cbHabitat,
            this.cbSpecies,
            this.cbEnvironmental,
            this.repositoryItemTextEdit3,
            this.cbBafInAt,
            this.repositoryItemLookUpEdit1});
            this.gridControlPlots.Size = new System.Drawing.Size(1624, 837);
            this.gridControlPlots.TabIndex = 1;
            this.gridControlPlots.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridViewPlots});
            this.gridControlPlots.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlPlots_EditorKeyDown);
            // 
            // plotsBindingSource
            // 
            this.plotsBindingSource.DataSource = typeof(Project.DAL.Plot);
            // 
            // advBandedGridViewPlots
            // 
            this.advBandedGridViewPlots.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9});
            this.advBandedGridViewPlots.ColumnPanelRowHeight = 40;
            this.advBandedGridViewPlots.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPlotsId,
            this.colStandsId1,
            this.colCruiserID,
            this.colAttachments1,
            this.colUserPlotNumber,
            this.colPlotDate,
            this.colCruiser,
            this.colBafInAt,
            this.colCruFlag,
            this.colAddFlag,
            this.colRefFlag,
            this.colDWFlag,
            this.colSlope,
            this.colAspect,
            this.colHabitat,
            this.colSpecies,
            this.colEnvironmental,
            this.colX,
            this.colY,
            this.colZ,
            this.colBasalAreaAcre,
            this.colTreesPerAcre1,
            this.colNetCfAcre,
            this.colNetBfAcre,
            this.colNotes});
            this.advBandedGridViewPlots.GridControl = this.gridControlPlots;
            this.advBandedGridViewPlots.Name = "advBandedGridViewPlots";
            this.advBandedGridViewPlots.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.advBandedGridViewPlots.OptionsCustomization.AllowFilter = false;
            this.advBandedGridViewPlots.OptionsCustomization.AllowSort = false;
            this.advBandedGridViewPlots.OptionsNavigation.AutoFocusNewRow = true;
            this.advBandedGridViewPlots.OptionsNavigation.EnterMoveNextColumn = true;
            this.advBandedGridViewPlots.OptionsView.ShowGroupPanel = false;
            this.advBandedGridViewPlots.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.advBandedGridViewPlots_ShowingEditor);
            this.advBandedGridViewPlots.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.advBandedGridViewPlots_ValidateRow);
            this.advBandedGridViewPlots.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.advBandedGridViewPlots_RowUpdated);
            this.advBandedGridViewPlots.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.advBandedGridViewPlots_PrintInitialize);
            this.advBandedGridViewPlots.KeyDown += new System.Windows.Forms.KeyEventHandler(this.advBandedGridViewPlots_KeyDown);
            this.advBandedGridViewPlots.Click += new System.EventHandler(this.advBandedGridViewPlots_Click);
            this.advBandedGridViewPlots.DoubleClick += new System.EventHandler(this.advBandedGridViewPlots_DoubleClick);
            this.advBandedGridViewPlots.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.advBandedGridViewPlots_ValidatingEditor);
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Root Group";
            this.gridBand2.Columns.Add(this.colStandsId1);
            this.gridBand2.Columns.Add(this.colCruiserID);
            this.gridBand2.Columns.Add(this.colPlotsId);
            this.gridBand2.Columns.Add(this.colAttachments1);
            this.gridBand2.Columns.Add(this.colUserPlotNumber);
            this.gridBand2.Columns.Add(this.colPlotDate);
            this.gridBand2.Columns.Add(this.colCruiser);
            this.gridBand2.Columns.Add(this.colBafInAt);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 271;
            // 
            // colStandsId1
            // 
            this.colStandsId1.AppearanceHeader.Options.UseTextOptions = true;
            this.colStandsId1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStandsId1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStandsId1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStandsId1.FieldName = "StandsId";
            this.colStandsId1.Name = "colStandsId1";
            this.colStandsId1.Width = 20;
            // 
            // colCruiserID
            // 
            this.colCruiserID.AppearanceHeader.Options.UseTextOptions = true;
            this.colCruiserID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCruiserID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCruiserID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCruiserID.FieldName = "CruiserID";
            this.colCruiserID.Name = "colCruiserID";
            this.colCruiserID.Width = 20;
            // 
            // colPlotsId
            // 
            this.colPlotsId.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotsId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotsId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotsId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotsId.Caption = "ATT";
            this.colPlotsId.FieldName = "PlotsId";
            this.colPlotsId.Name = "colPlotsId";
            this.colPlotsId.Width = 30;
            // 
            // colAttachments1
            // 
            this.colAttachments1.AppearanceHeader.Options.UseTextOptions = true;
            this.colAttachments1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAttachments1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAttachments1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAttachments1.Caption = "ATT";
            this.colAttachments1.FieldName = "Attachments";
            this.colAttachments1.Name = "colAttachments1";
            this.colAttachments1.Visible = true;
            this.colAttachments1.Width = 43;
            // 
            // colUserPlotNumber
            // 
            this.colUserPlotNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserPlotNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserPlotNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUserPlotNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserPlotNumber.Caption = "Plot #";
            this.colUserPlotNumber.ColumnEdit = this.repositoryItemTextEdit1;
            this.colUserPlotNumber.FieldName = "UserPlotNumber";
            this.colUserPlotNumber.Name = "colUserPlotNumber";
            this.colUserPlotNumber.Visible = true;
            this.colUserPlotNumber.Width = 38;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colPlotDate
            // 
            this.colPlotDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotDate.Caption = "Plot Date";
            this.colPlotDate.DisplayFormat.FormatString = "d";
            this.colPlotDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPlotDate.FieldName = "PlotDate";
            this.colPlotDate.Name = "colPlotDate";
            this.colPlotDate.Visible = true;
            this.colPlotDate.Width = 70;
            // 
            // colCruiser
            // 
            this.colCruiser.AppearanceHeader.Options.UseTextOptions = true;
            this.colCruiser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCruiser.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCruiser.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCruiser.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCruiser.FieldName = "Cruiser";
            this.colCruiser.Name = "colCruiser";
            this.colCruiser.Visible = true;
            this.colCruiser.Width = 77;
            // 
            // colBafInAt
            // 
            this.colBafInAt.AppearanceHeader.Options.UseTextOptions = true;
            this.colBafInAt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBafInAt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBafInAt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBafInAt.Caption = "In At";
            this.colBafInAt.ColumnEdit = this.cbBafInAt;
            this.colBafInAt.FieldName = "BafInAt";
            this.colBafInAt.Name = "colBafInAt";
            this.colBafInAt.Visible = true;
            this.colBafInAt.Width = 43;
            // 
            // cbBafInAt
            // 
            this.cbBafInAt.AutoHeight = false;
            this.cbBafInAt.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbBafInAt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbBafInAt.Items.AddRange(new object[] {
            "FP",
            "DBH",
            "ALL",
            ""});
            this.cbBafInAt.Name = "cbBafInAt";
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Data Taken";
            this.gridBand3.Columns.Add(this.colCruFlag);
            this.gridBand3.Columns.Add(this.colAddFlag);
            this.gridBand3.Columns.Add(this.colRefFlag);
            this.gridBand3.Columns.Add(this.colDWFlag);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 1;
            this.gridBand3.Width = 125;
            // 
            // colCruFlag
            // 
            this.colCruFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.colCruFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCruFlag.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCruFlag.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCruFlag.Caption = "Cru";
            this.colCruFlag.ColumnEdit = this.repositoryItemTextEdit3;
            this.colCruFlag.FieldName = "CruFlag";
            this.colCruFlag.Name = "colCruFlag";
            this.colCruFlag.OptionsColumn.ReadOnly = true;
            this.colCruFlag.Visible = true;
            this.colCruFlag.Width = 30;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.ReadOnly = true;
            // 
            // colAddFlag
            // 
            this.colAddFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.colAddFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAddFlag.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAddFlag.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAddFlag.Caption = "Add";
            this.colAddFlag.ColumnEdit = this.repositoryItemTextEdit3;
            this.colAddFlag.FieldName = "AddFlag";
            this.colAddFlag.Name = "colAddFlag";
            this.colAddFlag.OptionsColumn.ReadOnly = true;
            this.colAddFlag.Visible = true;
            this.colAddFlag.Width = 30;
            // 
            // colRefFlag
            // 
            this.colRefFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefFlag.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colRefFlag.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRefFlag.Caption = "Ref";
            this.colRefFlag.ColumnEdit = this.repositoryItemTextEdit3;
            this.colRefFlag.FieldName = "RefFlag";
            this.colRefFlag.Name = "colRefFlag";
            this.colRefFlag.OptionsColumn.ReadOnly = true;
            this.colRefFlag.Visible = true;
            this.colRefFlag.Width = 30;
            // 
            // colDWFlag
            // 
            this.colDWFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.colDWFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDWFlag.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDWFlag.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDWFlag.Caption = "D&W";
            this.colDWFlag.ColumnEdit = this.repositoryItemTextEdit3;
            this.colDWFlag.FieldName = "DWFlag";
            this.colDWFlag.Name = "colDWFlag";
            this.colDWFlag.OptionsColumn.ReadOnly = true;
            this.colDWFlag.Visible = true;
            this.colDWFlag.Width = 35;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Load Data";
            this.gridBand4.Columns.Add(this.colSlope);
            this.gridBand4.Columns.Add(this.colAspect);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 100;
            // 
            // colSlope
            // 
            this.colSlope.AppearanceHeader.Options.UseTextOptions = true;
            this.colSlope.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSlope.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSlope.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSlope.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSlope.FieldName = "Slope";
            this.colSlope.Name = "colSlope";
            this.colSlope.Visible = true;
            this.colSlope.Width = 47;
            // 
            // colAspect
            // 
            this.colAspect.AppearanceHeader.Options.UseTextOptions = true;
            this.colAspect.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAspect.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAspect.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAspect.ColumnEdit = this.cbAspect;
            this.colAspect.FieldName = "Aspect";
            this.colAspect.Name = "colAspect";
            this.colAspect.Visible = true;
            this.colAspect.Width = 53;
            // 
            // cbAspect
            // 
            this.cbAspect.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbAspect.AutoHeight = false;
            this.cbAspect.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAspect.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbAspect.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbAspect.DataSource = this.aspectsBindingSource;
            this.cbAspect.DisplayMember = "DisplayCode";
            this.cbAspect.Name = "cbAspect";
            this.cbAspect.NullText = "";
            this.cbAspect.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbAspect.ValueMember = "DisplayCode";
            // 
            // aspectsBindingSource
            // 
            this.aspectsBindingSource.DataSource = typeof(Project.DAL.Aspect);
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Habitat";
            this.gridBand5.Columns.Add(this.colHabitat);
            this.gridBand5.Columns.Add(this.colSpecies);
            this.gridBand5.Columns.Add(this.colEnvironmental);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 200;
            // 
            // colHabitat
            // 
            this.colHabitat.AppearanceHeader.Options.UseTextOptions = true;
            this.colHabitat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHabitat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colHabitat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHabitat.ColumnEdit = this.cbHabitat;
            this.colHabitat.FieldName = "Habitat";
            this.colHabitat.Name = "colHabitat";
            this.colHabitat.Visible = true;
            this.colHabitat.Width = 72;
            // 
            // cbHabitat
            // 
            this.cbHabitat.AutoHeight = false;
            this.cbHabitat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHabitat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbHabitat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbHabitat.DataSource = this.habitatBindingSource;
            this.cbHabitat.DisplayMember = "DisplayCode";
            this.cbHabitat.Name = "cbHabitat";
            this.cbHabitat.NullText = "";
            this.cbHabitat.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbHabitat.ValueMember = "DisplayCode";
            // 
            // habitatBindingSource
            // 
            this.habitatBindingSource.DataSource = typeof(Project.DAL.Habitat);
            // 
            // colSpecies
            // 
            this.colSpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies.Caption = "Spp";
            this.colSpecies.ColumnEdit = this.cbSpecies;
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.Visible = true;
            this.colSpecies.Width = 59;
            // 
            // cbSpecies
            // 
            this.cbSpecies.AutoHeight = false;
            this.cbSpecies.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSpecies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbSpecies.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbSpecies.DataSource = this.speciesBindingSource;
            this.cbSpecies.DisplayMember = "Abbreviation";
            this.cbSpecies.Name = "cbSpecies";
            this.cbSpecies.NullText = "";
            this.cbSpecies.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbSpecies.ValueMember = "Abbreviation";
            // 
            // colEnvironmental
            // 
            this.colEnvironmental.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnvironmental.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnvironmental.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colEnvironmental.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEnvironmental.Caption = "Env";
            this.colEnvironmental.ColumnEdit = this.cbEnvironmental;
            this.colEnvironmental.FieldName = "Environmental";
            this.colEnvironmental.Name = "colEnvironmental";
            this.colEnvironmental.Visible = true;
            this.colEnvironmental.Width = 69;
            // 
            // cbEnvironmental
            // 
            this.cbEnvironmental.AutoHeight = false;
            this.cbEnvironmental.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEnvironmental.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.cbEnvironmental.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayCode", "Code", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cbEnvironmental.DataSource = this.environmentsBindingSource;
            this.cbEnvironmental.DisplayMember = "DisplayCode";
            this.cbEnvironmental.Name = "cbEnvironmental";
            this.cbEnvironmental.NullText = "";
            this.cbEnvironmental.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbEnvironmental.ValueMember = "DisplayCode";
            // 
            // environmentsBindingSource
            // 
            this.environmentsBindingSource.DataSource = typeof(Project.DAL.Environment);
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Coordinates";
            this.gridBand7.Columns.Add(this.colX);
            this.gridBand7.Columns.Add(this.colY);
            this.gridBand7.Columns.Add(this.colZ);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 175;
            // 
            // colX
            // 
            this.colX.AppearanceHeader.Options.UseTextOptions = true;
            this.colX.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colX.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colX.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colX.Caption = "X - North";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.Visible = true;
            this.colX.Width = 60;
            // 
            // colY
            // 
            this.colY.AppearanceHeader.Options.UseTextOptions = true;
            this.colY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colY.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colY.Caption = "Y - East";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.Visible = true;
            this.colY.Width = 55;
            // 
            // colZ
            // 
            this.colZ.AppearanceHeader.Options.UseTextOptions = true;
            this.colZ.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZ.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colZ.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colZ.Caption = "Z - Elev";
            this.colZ.FieldName = "Z";
            this.colZ.Name = "colZ";
            this.colZ.Visible = true;
            this.colZ.Width = 60;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Per Acre by Plot";
            this.gridBand8.Columns.Add(this.colBasalAreaAcre);
            this.gridBand8.Columns.Add(this.colTreesPerAcre1);
            this.gridBand8.Columns.Add(this.colNetCfAcre);
            this.gridBand8.Columns.Add(this.colNetBfAcre);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 5;
            this.gridBand8.Width = 237;
            // 
            // colBasalAreaAcre
            // 
            this.colBasalAreaAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colBasalAreaAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBasalAreaAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBasalAreaAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBasalAreaAcre.Caption = "Basal Area Per Ac";
            this.colBasalAreaAcre.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.colBasalAreaAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBasalAreaAcre.FieldName = "BasalAreaAcre";
            this.colBasalAreaAcre.Name = "colBasalAreaAcre";
            this.colBasalAreaAcre.Visible = true;
            this.colBasalAreaAcre.Width = 60;
            // 
            // colTreesPerAcre1
            // 
            this.colTreesPerAcre1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreesPerAcre1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreesPerAcre1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreesPerAcre1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreesPerAcre1.Caption = "Trees Per Ac";
            this.colTreesPerAcre1.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.colTreesPerAcre1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreesPerAcre1.FieldName = "TreesPerAcre";
            this.colTreesPerAcre1.Name = "colTreesPerAcre1";
            this.colTreesPerAcre1.Visible = true;
            this.colTreesPerAcre1.Width = 55;
            // 
            // colNetCfAcre
            // 
            this.colNetCfAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colNetCfAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNetCfAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colNetCfAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNetCfAcre.Caption = "NetCF Per Ac";
            this.colNetCfAcre.DisplayFormat.FormatString = "{0:#,##0}";
            this.colNetCfAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetCfAcre.FieldName = "NetCfAcre";
            this.colNetCfAcre.Name = "colNetCfAcre";
            this.colNetCfAcre.Visible = true;
            this.colNetCfAcre.Width = 70;
            // 
            // colNetBfAcre
            // 
            this.colNetBfAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colNetBfAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNetBfAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colNetBfAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNetBfAcre.Caption = "NetBF Per Ac";
            this.colNetBfAcre.DisplayFormat.FormatString = "{0:#,##0}";
            this.colNetBfAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetBfAcre.FieldName = "NetBfAcre";
            this.colNetBfAcre.Name = "colNetBfAcre";
            this.colNetBfAcre.Visible = true;
            this.colNetBfAcre.Width = 52;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Columns.Add(this.colNotes);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 6;
            this.gridBand9.Width = 100;
            // 
            // colNotes
            // 
            this.colNotes.AppearanceHeader.Options.UseTextOptions = true;
            this.colNotes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNotes.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colNotes.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.Visible = true;
            this.colNotes.Width = 100;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tbPlotsCostTable);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.tbPlotsNetAcres);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.tbPlotsPriceTable);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.tbPlotsGradeTable);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.tbPlotsSortTable);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.tbPlotsSpeciesTable);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.tbPlotsSection);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.tbPlotsRange);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.tbPlotsTownship);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.tbPlotsStand);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.tbPlotsTract);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1624, 84);
            this.panelControl1.TabIndex = 0;
            // 
            // tbPlotsCostTable
            // 
            this.tbPlotsCostTable.Location = new System.Drawing.Point(1235, 39);
            this.tbPlotsCostTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsCostTable.MenuManager = this.barManager1;
            this.tbPlotsCostTable.Name = "tbPlotsCostTable";
            this.tbPlotsCostTable.Properties.ReadOnly = true;
            this.tbPlotsCostTable.Size = new System.Drawing.Size(142, 22);
            this.tbPlotsCostTable.TabIndex = 27;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(1280, 16);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(61, 16);
            this.labelControl11.TabIndex = 26;
            this.labelControl11.Text = "Cost Table";
            // 
            // tbPlotsNetAcres
            // 
            this.tbPlotsNetAcres.Location = new System.Drawing.Point(399, 39);
            this.tbPlotsNetAcres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsNetAcres.MenuManager = this.barManager1;
            this.tbPlotsNetAcres.Name = "tbPlotsNetAcres";
            this.tbPlotsNetAcres.Properties.ReadOnly = true;
            this.tbPlotsNetAcres.Size = new System.Drawing.Size(72, 22);
            this.tbPlotsNetAcres.TabIndex = 25;
            // 
            // labelControl10
            // 
            this.labelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl10.Location = new System.Drawing.Point(407, 16);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(55, 16);
            this.labelControl10.TabIndex = 24;
            this.labelControl10.Text = "Net Acres";
            // 
            // tbPlotsPriceTable
            // 
            this.tbPlotsPriceTable.Location = new System.Drawing.Point(1050, 39);
            this.tbPlotsPriceTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsPriceTable.MenuManager = this.barManager1;
            this.tbPlotsPriceTable.Name = "tbPlotsPriceTable";
            this.tbPlotsPriceTable.Properties.ReadOnly = true;
            this.tbPlotsPriceTable.Size = new System.Drawing.Size(142, 22);
            this.tbPlotsPriceTable.TabIndex = 23;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(1087, 16);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(64, 16);
            this.labelControl6.TabIndex = 22;
            this.labelControl6.Text = "Price Table";
            // 
            // tbPlotsGradeTable
            // 
            this.tbPlotsGradeTable.Location = new System.Drawing.Point(861, 39);
            this.tbPlotsGradeTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsGradeTable.MenuManager = this.barManager1;
            this.tbPlotsGradeTable.Name = "tbPlotsGradeTable";
            this.tbPlotsGradeTable.Properties.ReadOnly = true;
            this.tbPlotsGradeTable.Size = new System.Drawing.Size(142, 22);
            this.tbPlotsGradeTable.TabIndex = 21;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(899, 16);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(70, 16);
            this.labelControl7.TabIndex = 20;
            this.labelControl7.Text = "Grade Table";
            // 
            // tbPlotsSortTable
            // 
            this.tbPlotsSortTable.Location = new System.Drawing.Point(672, 39);
            this.tbPlotsSortTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsSortTable.MenuManager = this.barManager1;
            this.tbPlotsSortTable.Name = "tbPlotsSortTable";
            this.tbPlotsSortTable.Properties.ReadOnly = true;
            this.tbPlotsSortTable.Size = new System.Drawing.Size(142, 22);
            this.tbPlotsSortTable.TabIndex = 19;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(714, 16);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 16);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Sort Table";
            // 
            // tbPlotsSpeciesTable
            // 
            this.tbPlotsSpeciesTable.Location = new System.Drawing.Point(485, 39);
            this.tbPlotsSpeciesTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsSpeciesTable.MenuManager = this.barManager1;
            this.tbPlotsSpeciesTable.Name = "tbPlotsSpeciesTable";
            this.tbPlotsSpeciesTable.Properties.ReadOnly = true;
            this.tbPlotsSpeciesTable.Size = new System.Drawing.Size(142, 22);
            this.tbPlotsSpeciesTable.TabIndex = 17;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(524, 16);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(80, 16);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Species Table";
            // 
            // tbPlotsSection
            // 
            this.tbPlotsSection.Location = new System.Drawing.Point(345, 39);
            this.tbPlotsSection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsSection.MenuManager = this.barManager1;
            this.tbPlotsSection.Name = "tbPlotsSection";
            this.tbPlotsSection.Properties.ReadOnly = true;
            this.tbPlotsSection.Size = new System.Drawing.Size(34, 22);
            this.tbPlotsSection.TabIndex = 15;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(352, 16);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(21, 16);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Sec";
            // 
            // tbPlotsRange
            // 
            this.tbPlotsRange.Location = new System.Drawing.Point(278, 39);
            this.tbPlotsRange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsRange.MenuManager = this.barManager1;
            this.tbPlotsRange.Name = "tbPlotsRange";
            this.tbPlotsRange.Properties.ReadOnly = true;
            this.tbPlotsRange.Size = new System.Drawing.Size(45, 22);
            this.tbPlotsRange.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(289, 16);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 16);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Rge";
            // 
            // tbPlotsTownship
            // 
            this.tbPlotsTownship.Location = new System.Drawing.Point(212, 39);
            this.tbPlotsTownship.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsTownship.MenuManager = this.barManager1;
            this.tbPlotsTownship.Name = "tbPlotsTownship";
            this.tbPlotsTownship.Properties.ReadOnly = true;
            this.tbPlotsTownship.Size = new System.Drawing.Size(45, 22);
            this.tbPlotsTownship.TabIndex = 11;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(220, 16);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(25, 16);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Twn";
            // 
            // tbPlotsStand
            // 
            this.tbPlotsStand.Location = new System.Drawing.Point(140, 39);
            this.tbPlotsStand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsStand.MenuManager = this.barManager1;
            this.tbPlotsStand.Name = "tbPlotsStand";
            this.tbPlotsStand.Properties.ReadOnly = true;
            this.tbPlotsStand.Size = new System.Drawing.Size(52, 22);
            this.tbPlotsStand.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(143, 16);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(46, 16);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Stand #";
            // 
            // tbPlotsTract
            // 
            this.tbPlotsTract.Location = new System.Drawing.Point(6, 39);
            this.tbPlotsTract.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPlotsTract.MenuManager = this.barManager1;
            this.tbPlotsTract.Name = "tbPlotsTract";
            this.tbPlotsTract.Properties.ReadOnly = true;
            this.tbPlotsTract.Size = new System.Drawing.Size(117, 22);
            this.tbPlotsTract.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(45, 16);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(30, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Tract";
            // 
            // tabNavigationPageTreeInput
            // 
            this.tabNavigationPageTreeInput.Caption = "Tree Input";
            this.tabNavigationPageTreeInput.Controls.Add(this.gridControlTrees);
            this.tabNavigationPageTreeInput.Controls.Add(this.panelControl2);
            this.tabNavigationPageTreeInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPageTreeInput.Name = "tabNavigationPageTreeInput";
            this.tabNavigationPageTreeInput.Size = new System.Drawing.Size(1388, 713);
            // 
            // gridControlTrees
            // 
            this.gridControlTrees.DataSource = this.treesBindingSource;
            this.gridControlTrees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTrees.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlTrees.Location = new System.Drawing.Point(0, 84);
            this.gridControlTrees.MainView = this.advBandedGridViewTrees;
            this.gridControlTrees.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlTrees.MenuManager = this.barManager1;
            this.gridControlTrees.Name = "gridControlTrees";
            this.gridControlTrees.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItem2,
            this.repositoryItemFormPoint,
            this.repositoryItemFormFactor,
            this.repositoryItemGrade,
            this.repositoryItem1,
            this.repositoryItemUpper,
            this.repositoryItem3,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemLookUpEdit3});
            this.gridControlTrees.Size = new System.Drawing.Size(1388, 629);
            this.gridControlTrees.TabIndex = 2;
            this.gridControlTrees.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridViewTrees});
            this.gridControlTrees.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlTrees_ProcessGridKey);
            this.gridControlTrees.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlTrees_EditorKeyDown);
            // 
            // treesBindingSource
            // 
            this.treesBindingSource.DataSource = typeof(Project.DAL.Tree);
            // 
            // advBandedGridViewTrees
            // 
            this.advBandedGridViewTrees.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand6,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23});
            this.advBandedGridViewTrees.ColumnPanelRowHeight = 40;
            this.advBandedGridViewTrees.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colTreesId,
            this.colStandsId2,
            this.colPlotsId1,
            this.colPlotNumber,
            this.colTreeNumber,
            this.colPlotFactorInput,
            this.colAgeCode,
            this.colSpeciesAbbreviation,
            this.colTreeStatusDisplayCode,
            this.colTreeCount,
            this.colDbh,
            this.colFormPoint,
            this.colReportedFormFactor,
            this.colTopDiameterFractionCode,
            this.colBoleHeight,
            this.colTotalHeight,
            this.colCrownPositionDisplayCode,
            this.colCrownRatioDisplayCode,
            this.colVigorDisplayCode,
            this.colDamageDisplayCode,
            this.colUserDefinedDisplayCode,
            this.colSort1,
            this.colGrade1,
            this.colLength1,
            this.colBdFtLD1,
            this.colBdFtDD1,
            this.colCuFtLD1,
            this.colCuFtDD1,
            this.colBdFtPD1,
            this.colSort2,
            this.colGrade2,
            this.colLength2,
            this.colBdFtLD2,
            this.colBdFtDD2,
            this.colCuFtLD2,
            this.colCuFtDD2,
            this.colBdFtPD2,
            this.colSort3,
            this.colGrade3,
            this.colLength3,
            this.colBdFtLD3,
            this.colBdFtDD3,
            this.colCuFtLD3,
            this.colCuFtDD3,
            this.colSort4,
            this.colGrade4,
            this.colLength4,
            this.colBdFtLD4,
            this.colBdFtDD4,
            this.colCuFtLD4,
            this.colCuFtDD4,
            this.colSort5,
            this.colGrade5,
            this.colLength5,
            this.colBdFtLD5,
            this.colBdFtDD5,
            this.colCuFtLD5,
            this.colCuFtDD5,
            this.colSort6,
            this.colGrade6,
            this.colLength6,
            this.colBdFtLD6,
            this.colBdFtDD6,
            this.colCuFtLD6,
            this.colCuFtDD6,
            this.colSort7,
            this.colGrade7,
            this.colLength7,
            this.colBdFtLD7,
            this.colBdFtDD7,
            this.colCuFtLD7,
            this.colCuFtDD7,
            this.colSort8,
            this.colGrade8,
            this.colLength8,
            this.colBdFtLD8,
            this.colBdFtDD8,
            this.colCuFtLD8,
            this.colCuFtDD8,
            this.colSort9,
            this.colGrade9,
            this.colLength9,
            this.colBdFtLD9,
            this.colBdFtDD9,
            this.colCuFtLD9,
            this.colCuFtDD9,
            this.colSort10,
            this.colGrade10,
            this.colLength10,
            this.colBdFtLD10,
            this.colBdFtDD10,
            this.colCuFtLD10,
            this.colCuFtDD10,
            this.colSort11,
            this.colGrade11,
            this.colLength11,
            this.colBdFtLD11,
            this.colBdFtDD11,
            this.colCuFtLD11,
            this.colCuFtDD11,
            this.colSort12,
            this.colGrade12,
            this.colLength12,
            this.colBdFtLD12,
            this.colBdFtDD12,
            this.colCuFtLD12,
            this.colCuFtDD12,
            this.colGroupPlot,
            this.colGroupTree,
            this.colTreeType,
            this.colAge,
            this.colBdFtPD3,
            this.colBdFtPD4,
            this.colBdFtPD5,
            this.colBdFtPD6,
            this.colBdFtPD7,
            this.colBdFtPD8,
            this.colBdFtPD9,
            this.colBdFtPD10,
            this.colBdFtPD11,
            this.colBdFtPD12,
            this.colCalcTotalHeight,
            this.colCalcTotalHtUsedYN,
            this.colPFValue,
            this.colPFType});
            this.advBandedGridViewTrees.GridControl = this.gridControlTrees;
            this.advBandedGridViewTrees.GroupCount = 1;
            this.advBandedGridViewTrees.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "PlotId", this.colTreeNumber, "")});
            this.advBandedGridViewTrees.Name = "advBandedGridViewTrees";
            this.advBandedGridViewTrees.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.advBandedGridViewTrees.OptionsCustomization.AllowFilter = false;
            this.advBandedGridViewTrees.OptionsCustomization.AllowSort = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowColumnMRUFilterList = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowFilterEditor = false;
            this.advBandedGridViewTrees.OptionsFilter.AllowMRUFilterList = false;
            this.advBandedGridViewTrees.OptionsNavigation.EnterMoveNextColumn = true;
            this.advBandedGridViewTrees.OptionsSelection.MultiSelect = true;
            this.advBandedGridViewTrees.OptionsView.ShowGroupPanel = false;
            this.advBandedGridViewTrees.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPlotsId1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.advBandedGridViewTrees.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.advBandedGridViewTrees_RowCellStyle);
            this.advBandedGridViewTrees.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.advBandedGridViewTrees_InvalidRowException);
            this.advBandedGridViewTrees.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.advBandedGridViewTrees_ValidateRow);
            this.advBandedGridViewTrees.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.advBandedGridViewTrees_RowUpdated);
            this.advBandedGridViewTrees.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.advBandedGridViewTrees_PrintInitialize);
            this.advBandedGridViewTrees.KeyDown += new System.Windows.Forms.KeyEventHandler(this.advBandedGridViewTrees_KeyDown);
            this.advBandedGridViewTrees.Click += new System.EventHandler(this.advBandedGridViewTrees_Click);
            this.advBandedGridViewTrees.DoubleClick += new System.EventHandler(this.advBandedGridViewTrees_DoubleClick);
            this.advBandedGridViewTrees.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.advBandedGridViewTrees_ValidatingEditor);
            this.advBandedGridViewTrees.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.advBandedGridViewTrees_InvalidValueException);
            // 
            // gridBand1
            // 
            this.gridBand1.Columns.Add(this.colTreesId);
            this.gridBand1.Columns.Add(this.colStandsId2);
            this.gridBand1.Columns.Add(this.colPlotsId1);
            this.gridBand1.Columns.Add(this.colPlotNumber);
            this.gridBand1.Columns.Add(this.colTreeNumber);
            this.gridBand1.Columns.Add(this.colPlotFactorInput);
            this.gridBand1.Columns.Add(this.colGroupPlot);
            this.gridBand1.Columns.Add(this.colGroupTree);
            this.gridBand1.Columns.Add(this.colTreeType);
            this.gridBand1.Columns.Add(this.colAge);
            this.gridBand1.Columns.Add(this.colCalcTotalHeight);
            this.gridBand1.Columns.Add(this.colCalcTotalHtUsedYN);
            this.gridBand1.Columns.Add(this.colPFValue);
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 108;
            // 
            // colTreesId
            // 
            this.colTreesId.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreesId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreesId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreesId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreesId.FieldName = "TreesId";
            this.colTreesId.Name = "colTreesId";
            // 
            // colStandsId2
            // 
            this.colStandsId2.AppearanceHeader.Options.UseTextOptions = true;
            this.colStandsId2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStandsId2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStandsId2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStandsId2.FieldName = "StandsId";
            this.colStandsId2.Name = "colStandsId2";
            // 
            // colPlotsId1
            // 
            this.colPlotsId1.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotsId1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotsId1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotsId1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotsId1.FieldName = "PlotsId";
            this.colPlotsId1.Name = "colPlotsId1";
            // 
            // colPlotNumber
            // 
            this.colPlotNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotNumber.Caption = "Plot #";
            this.colPlotNumber.ColumnEdit = this.repositoryItemUpper;
            this.colPlotNumber.FieldName = "PlotNumber";
            this.colPlotNumber.Name = "colPlotNumber";
            this.colPlotNumber.Visible = true;
            this.colPlotNumber.Width = 41;
            // 
            // repositoryItemUpper
            // 
            this.repositoryItemUpper.AutoHeight = false;
            this.repositoryItemUpper.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemUpper.Name = "repositoryItemUpper";
            // 
            // colTreeNumber
            // 
            this.colTreeNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeNumber.Caption = "Tree #";
            this.colTreeNumber.DisplayFormat.FormatString = "{0:#}";
            this.colTreeNumber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeNumber.FieldName = "TreeNumber";
            this.colTreeNumber.Name = "colTreeNumber";
            this.colTreeNumber.Visible = true;
            this.colTreeNumber.Width = 41;
            // 
            // colPlotFactorInput
            // 
            this.colPlotFactorInput.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotFactorInput.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotFactorInput.Caption = "PF";
            this.colPlotFactorInput.ColumnEdit = this.repositoryItem2;
            this.colPlotFactorInput.FieldName = "PlotFactorInput";
            this.colPlotFactorInput.Name = "colPlotFactorInput";
            this.colPlotFactorInput.Visible = true;
            this.colPlotFactorInput.Width = 26;
            // 
            // repositoryItem2
            // 
            this.repositoryItem2.AutoHeight = false;
            this.repositoryItem2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem2.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItem2.MaxLength = 2;
            this.repositoryItem2.Name = "repositoryItem2";
            this.repositoryItem2.EditValueChanged += new System.EventHandler(this.repositoryItem2_EditValueChanged);
            // 
            // colGroupPlot
            // 
            this.colGroupPlot.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupPlot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupPlot.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGroupPlot.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGroupPlot.FieldName = "GroupPlot";
            this.colGroupPlot.Name = "colGroupPlot";
            // 
            // colGroupTree
            // 
            this.colGroupTree.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupTree.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupTree.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGroupTree.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGroupTree.FieldName = "GroupTree";
            this.colGroupTree.Name = "colGroupTree";
            // 
            // colTreeType
            // 
            this.colTreeType.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            // 
            // colAge
            // 
            this.colAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAge.FieldName = "Age";
            this.colAge.Name = "colAge";
            // 
            // colCalcTotalHeight
            // 
            this.colCalcTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTotalHeight.FieldName = "CalcTotalHeight";
            this.colCalcTotalHeight.Name = "colCalcTotalHeight";
            // 
            // colCalcTotalHtUsedYN
            // 
            this.colCalcTotalHtUsedYN.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTotalHtUsedYN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTotalHtUsedYN.FieldName = "CalcTotalHtUsedYn";
            this.colCalcTotalHtUsedYN.Name = "colCalcTotalHtUsedYN";
            // 
            // colPFValue
            // 
            this.colPFValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colPFValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPFValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPFValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPFValue.FieldName = "PFValue";
            this.colPFValue.Name = "colPFValue";
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Identification";
            this.gridBand6.Columns.Add(this.colAgeCode);
            this.gridBand6.Columns.Add(this.colSpeciesAbbreviation);
            this.gridBand6.Columns.Add(this.colTreeStatusDisplayCode);
            this.gridBand6.Columns.Add(this.colTreeCount);
            this.gridBand6.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 115;
            // 
            // colAgeCode
            // 
            this.colAgeCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgeCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgeCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAgeCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgeCode.Caption = "Age";
            this.colAgeCode.ColumnEdit = this.repositoryItem1;
            this.colAgeCode.DisplayFormat.FormatString = "{0:#}";
            this.colAgeCode.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAgeCode.FieldName = "AgeCode";
            this.colAgeCode.Name = "colAgeCode";
            this.colAgeCode.Visible = true;
            this.colAgeCode.Width = 29;
            // 
            // repositoryItem1
            // 
            this.repositoryItem1.AutoHeight = false;
            this.repositoryItem1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem1.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItem1.MaxLength = 1;
            this.repositoryItem1.Name = "repositoryItem1";
            this.repositoryItem1.EditValueChanged += new System.EventHandler(this.repositoryItem1_EditValueChanged);
            // 
            // colSpeciesAbbreviation
            // 
            this.colSpeciesAbbreviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpeciesAbbreviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeciesAbbreviation.Caption = "Spp";
            this.colSpeciesAbbreviation.ColumnEdit = this.repositoryItemUpper;
            this.colSpeciesAbbreviation.FieldName = "SpeciesAbbreviation";
            this.colSpeciesAbbreviation.Name = "colSpeciesAbbreviation";
            this.colSpeciesAbbreviation.Visible = true;
            this.colSpeciesAbbreviation.Width = 34;
            // 
            // colTreeStatusDisplayCode
            // 
            this.colTreeStatusDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeStatusDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeStatusDisplayCode.Caption = "St";
            this.colTreeStatusDisplayCode.ColumnEdit = this.repositoryItemUpper;
            this.colTreeStatusDisplayCode.FieldName = "TreeStatusDisplayCode";
            this.colTreeStatusDisplayCode.Name = "colTreeStatusDisplayCode";
            this.colTreeStatusDisplayCode.Visible = true;
            this.colTreeStatusDisplayCode.Width = 24;
            // 
            // colTreeCount
            // 
            this.colTreeCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeCount.Caption = "Ct";
            this.colTreeCount.FieldName = "TreeCount";
            this.colTreeCount.Name = "colTreeCount";
            this.colTreeCount.Visible = true;
            this.colTreeCount.Width = 28;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Measurements";
            this.gridBand10.Columns.Add(this.colDbh);
            this.gridBand10.Columns.Add(this.colFormPoint);
            this.gridBand10.Columns.Add(this.colReportedFormFactor);
            this.gridBand10.Columns.Add(this.colTopDiameterFractionCode);
            this.gridBand10.Columns.Add(this.colBoleHeight);
            this.gridBand10.Columns.Add(this.colTotalHeight);
            this.gridBand10.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 179;
            // 
            // colDbh
            // 
            this.colDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDbh.Caption = "Dbh";
            this.colDbh.DisplayFormat.FormatString = "{0:0.0}";
            this.colDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDbh.FieldName = "Dbh";
            this.colDbh.Name = "colDbh";
            this.colDbh.Visible = true;
            this.colDbh.Width = 36;
            // 
            // colFormPoint
            // 
            this.colFormPoint.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormPoint.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormPoint.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormPoint.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormPoint.Caption = "FP";
            this.colFormPoint.ColumnEdit = this.repositoryItem2;
            this.colFormPoint.DisplayFormat.FormatString = "{0:#}";
            this.colFormPoint.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFormPoint.FieldName = "FormPoint";
            this.colFormPoint.Name = "colFormPoint";
            this.colFormPoint.Visible = true;
            this.colFormPoint.Width = 20;
            // 
            // colReportedFormFactor
            // 
            this.colReportedFormFactor.AppearanceHeader.Options.UseTextOptions = true;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colReportedFormFactor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colReportedFormFactor.Caption = "FF";
            this.colReportedFormFactor.ColumnEdit = this.repositoryItem2;
            this.colReportedFormFactor.DisplayFormat.FormatString = "{0:#}";
            this.colReportedFormFactor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colReportedFormFactor.FieldName = "ReportedFormFactor";
            this.colReportedFormFactor.Name = "colReportedFormFactor";
            this.colReportedFormFactor.Visible = true;
            this.colReportedFormFactor.Width = 20;
            // 
            // colTopDiameterFractionCode
            // 
            this.colTopDiameterFractionCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTopDiameterFractionCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTopDiameterFractionCode.Caption = "TDF";
            this.colTopDiameterFractionCode.ColumnEdit = this.repositoryItem2;
            this.colTopDiameterFractionCode.FieldName = "TopDiameterFractionCode";
            this.colTopDiameterFractionCode.Name = "colTopDiameterFractionCode";
            this.colTopDiameterFractionCode.Visible = true;
            this.colTopDiameterFractionCode.Width = 29;
            // 
            // colBoleHeight
            // 
            this.colBoleHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colBoleHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBoleHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBoleHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBoleHeight.Caption = "Bole Ht";
            this.colBoleHeight.ColumnEdit = this.repositoryItem3;
            this.colBoleHeight.DisplayFormat.FormatString = "{0:#}";
            this.colBoleHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBoleHeight.FieldName = "BoleHeight";
            this.colBoleHeight.Name = "colBoleHeight";
            this.colBoleHeight.Visible = true;
            this.colBoleHeight.Width = 34;
            // 
            // repositoryItem3
            // 
            this.repositoryItem3.AutoHeight = false;
            this.repositoryItem3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItem3.DisplayFormat.FormatString = "{0:#}";
            this.repositoryItem3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItem3.MaxLength = 3;
            this.repositoryItem3.Name = "repositoryItem3";
            this.repositoryItem3.EditValueChanged += new System.EventHandler(this.repositoryItem3_EditValueChanged);
            // 
            // colTotalHeight
            // 
            this.colTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalHeight.Caption = "Total Ht";
            this.colTotalHeight.ColumnEdit = this.repositoryItem3;
            this.colTotalHeight.DisplayFormat.FormatString = "{0:#}";
            this.colTotalHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalHeight.FieldName = "TotalHeight";
            this.colTotalHeight.Name = "colTotalHeight";
            this.colTotalHeight.Visible = true;
            this.colTotalHeight.Width = 40;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Classification";
            this.gridBand11.Columns.Add(this.colCrownPositionDisplayCode);
            this.gridBand11.Columns.Add(this.colCrownRatioDisplayCode);
            this.gridBand11.Columns.Add(this.colVigorDisplayCode);
            this.gridBand11.Columns.Add(this.colDamageDisplayCode);
            this.gridBand11.Columns.Add(this.colUserDefinedDisplayCode);
            this.gridBand11.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 3;
            this.gridBand11.Width = 115;
            // 
            // colCrownPositionDisplayCode
            // 
            this.colCrownPositionDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownPositionDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownPositionDisplayCode.Caption = "PO";
            this.colCrownPositionDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colCrownPositionDisplayCode.FieldName = "CrownPositionDisplayCode";
            this.colCrownPositionDisplayCode.Name = "colCrownPositionDisplayCode";
            this.colCrownPositionDisplayCode.Visible = true;
            this.colCrownPositionDisplayCode.Width = 26;
            // 
            // colCrownRatioDisplayCode
            // 
            this.colCrownRatioDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownRatioDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownRatioDisplayCode.Caption = "CR";
            this.colCrownRatioDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colCrownRatioDisplayCode.FieldName = "CrownRatioDisplayCode";
            this.colCrownRatioDisplayCode.Name = "colCrownRatioDisplayCode";
            this.colCrownRatioDisplayCode.Visible = true;
            this.colCrownRatioDisplayCode.Width = 24;
            // 
            // colVigorDisplayCode
            // 
            this.colVigorDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colVigorDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVigorDisplayCode.Caption = "VI";
            this.colVigorDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colVigorDisplayCode.FieldName = "VigorDisplayCode";
            this.colVigorDisplayCode.Name = "colVigorDisplayCode";
            this.colVigorDisplayCode.Visible = true;
            this.colVigorDisplayCode.Width = 21;
            // 
            // colDamageDisplayCode
            // 
            this.colDamageDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDamageDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDamageDisplayCode.Caption = "DA";
            this.colDamageDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colDamageDisplayCode.FieldName = "DamageDisplayCode";
            this.colDamageDisplayCode.Name = "colDamageDisplayCode";
            this.colDamageDisplayCode.Visible = true;
            this.colDamageDisplayCode.Width = 22;
            // 
            // colUserDefinedDisplayCode
            // 
            this.colUserDefinedDisplayCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUserDefinedDisplayCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserDefinedDisplayCode.Caption = "UD";
            this.colUserDefinedDisplayCode.ColumnEdit = this.repositoryItem2;
            this.colUserDefinedDisplayCode.FieldName = "UserDefinedDisplayCode";
            this.colUserDefinedDisplayCode.Name = "colUserDefinedDisplayCode";
            this.colUserDefinedDisplayCode.Visible = true;
            this.colUserDefinedDisplayCode.Width = 22;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Segment 1";
            this.gridBand12.Columns.Add(this.colSort1);
            this.gridBand12.Columns.Add(this.colGrade1);
            this.gridBand12.Columns.Add(this.colLength1);
            this.gridBand12.Columns.Add(this.colBdFtLD1);
            this.gridBand12.Columns.Add(this.colBdFtDD1);
            this.gridBand12.Columns.Add(this.colCuFtLD1);
            this.gridBand12.Columns.Add(this.colCuFtDD1);
            this.gridBand12.Columns.Add(this.colBdFtPD1);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 4;
            this.gridBand12.Width = 166;
            // 
            // colSort1
            // 
            this.colSort1.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort1.Caption = "S";
            this.colSort1.ColumnEdit = this.repositoryItem1;
            this.colSort1.FieldName = "Sort1";
            this.colSort1.Name = "colSort1";
            this.colSort1.Visible = true;
            this.colSort1.Width = 20;
            // 
            // colGrade1
            // 
            this.colGrade1.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade1.Caption = "G";
            this.colGrade1.ColumnEdit = this.repositoryItem1;
            this.colGrade1.FieldName = "Grade1";
            this.colGrade1.Name = "colGrade1";
            this.colGrade1.Visible = true;
            this.colGrade1.Width = 20;
            // 
            // colLength1
            // 
            this.colLength1.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength1.Caption = "Ln";
            this.colLength1.ColumnEdit = this.repositoryItem2;
            this.colLength1.FieldName = "Length1";
            this.colLength1.Name = "colLength1";
            this.colLength1.Visible = true;
            this.colLength1.Width = 20;
            // 
            // colBdFtLD1
            // 
            this.colBdFtLD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD1.Caption = "B F";
            this.colBdFtLD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD1.FieldName = "BdFtLd1";
            this.colBdFtLD1.Name = "colBdFtLD1";
            this.colBdFtLD1.Visible = true;
            this.colBdFtLD1.Width = 20;
            // 
            // colBdFtDD1
            // 
            this.colBdFtDD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD1.Caption = "B I";
            this.colBdFtDD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD1.FieldName = "BdFtDd1";
            this.colBdFtDD1.Name = "colBdFtDD1";
            this.colBdFtDD1.Visible = true;
            this.colBdFtDD1.Width = 20;
            // 
            // colCuFtLD1
            // 
            this.colCuFtLD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD1.Caption = "C F";
            this.colCuFtLD1.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD1.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD1.FieldName = "CuFtLd1";
            this.colCuFtLD1.Name = "colCuFtLD1";
            this.colCuFtLD1.Visible = true;
            this.colCuFtLD1.Width = 20;
            // 
            // colCuFtDD1
            // 
            this.colCuFtDD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD1.Caption = "C I";
            this.colCuFtDD1.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD1.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD1.FieldName = "CuFtDd1";
            this.colCuFtDD1.Name = "colCuFtDD1";
            this.colCuFtDD1.Visible = true;
            this.colCuFtDD1.Width = 20;
            // 
            // colBdFtPD1
            // 
            this.colBdFtPD1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD1.Caption = "%";
            this.colBdFtPD1.ColumnEdit = this.repositoryItem1;
            this.colBdFtPD1.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD1.FieldName = "BdFtPd1";
            this.colBdFtPD1.Name = "colBdFtPD1";
            this.colBdFtPD1.Visible = true;
            this.colBdFtPD1.Width = 26;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Segment 2";
            this.gridBand13.Columns.Add(this.colSort2);
            this.gridBand13.Columns.Add(this.colGrade2);
            this.gridBand13.Columns.Add(this.colLength2);
            this.gridBand13.Columns.Add(this.colBdFtLD2);
            this.gridBand13.Columns.Add(this.colBdFtDD2);
            this.gridBand13.Columns.Add(this.colCuFtLD2);
            this.gridBand13.Columns.Add(this.colCuFtDD2);
            this.gridBand13.Columns.Add(this.colBdFtPD2);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 5;
            this.gridBand13.Width = 160;
            // 
            // colSort2
            // 
            this.colSort2.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort2.Caption = "S";
            this.colSort2.ColumnEdit = this.repositoryItem1;
            this.colSort2.FieldName = "Sort2";
            this.colSort2.Name = "colSort2";
            this.colSort2.Visible = true;
            this.colSort2.Width = 20;
            // 
            // colGrade2
            // 
            this.colGrade2.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade2.Caption = "G";
            this.colGrade2.ColumnEdit = this.repositoryItem1;
            this.colGrade2.FieldName = "Grade2";
            this.colGrade2.Name = "colGrade2";
            this.colGrade2.Visible = true;
            this.colGrade2.Width = 20;
            // 
            // colLength2
            // 
            this.colLength2.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength2.Caption = "Ln";
            this.colLength2.ColumnEdit = this.repositoryItem2;
            this.colLength2.FieldName = "Length2";
            this.colLength2.Name = "colLength2";
            this.colLength2.Visible = true;
            this.colLength2.Width = 20;
            // 
            // colBdFtLD2
            // 
            this.colBdFtLD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD2.Caption = "B F";
            this.colBdFtLD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD2.FieldName = "BdFtLd2";
            this.colBdFtLD2.Name = "colBdFtLD2";
            this.colBdFtLD2.Visible = true;
            this.colBdFtLD2.Width = 20;
            // 
            // colBdFtDD2
            // 
            this.colBdFtDD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD2.Caption = "B I";
            this.colBdFtDD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD2.FieldName = "BdFtDd2";
            this.colBdFtDD2.Name = "colBdFtDD2";
            this.colBdFtDD2.Visible = true;
            this.colBdFtDD2.Width = 20;
            // 
            // colCuFtLD2
            // 
            this.colCuFtLD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD2.Caption = "C F";
            this.colCuFtLD2.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD2.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD2.FieldName = "CuFtLd2";
            this.colCuFtLD2.Name = "colCuFtLD2";
            this.colCuFtLD2.Visible = true;
            this.colCuFtLD2.Width = 20;
            // 
            // colCuFtDD2
            // 
            this.colCuFtDD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD2.Caption = "C I";
            this.colCuFtDD2.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD2.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD2.FieldName = "CuFtDd2";
            this.colCuFtDD2.Name = "colCuFtDD2";
            this.colCuFtDD2.Visible = true;
            this.colCuFtDD2.Width = 20;
            // 
            // colBdFtPD2
            // 
            this.colBdFtPD2.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD2.Caption = "%";
            this.colBdFtPD2.ColumnEdit = this.repositoryItem1;
            this.colBdFtPD2.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD2.FieldName = "BdFtPd2";
            this.colBdFtPD2.Name = "colBdFtPD2";
            this.colBdFtPD2.Visible = true;
            this.colBdFtPD2.Width = 20;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Segment 3";
            this.gridBand14.Columns.Add(this.colSort3);
            this.gridBand14.Columns.Add(this.colGrade3);
            this.gridBand14.Columns.Add(this.colLength3);
            this.gridBand14.Columns.Add(this.colBdFtLD3);
            this.gridBand14.Columns.Add(this.colBdFtDD3);
            this.gridBand14.Columns.Add(this.colCuFtLD3);
            this.gridBand14.Columns.Add(this.colCuFtDD3);
            this.gridBand14.Columns.Add(this.colBdFtPD3);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 6;
            this.gridBand14.Width = 160;
            // 
            // colSort3
            // 
            this.colSort3.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort3.Caption = "S";
            this.colSort3.ColumnEdit = this.repositoryItem1;
            this.colSort3.FieldName = "Sort3";
            this.colSort3.Name = "colSort3";
            this.colSort3.Visible = true;
            this.colSort3.Width = 20;
            // 
            // colGrade3
            // 
            this.colGrade3.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade3.Caption = "G";
            this.colGrade3.ColumnEdit = this.repositoryItem1;
            this.colGrade3.FieldName = "Grade3";
            this.colGrade3.Name = "colGrade3";
            this.colGrade3.Visible = true;
            this.colGrade3.Width = 20;
            // 
            // colLength3
            // 
            this.colLength3.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength3.Caption = "Ln";
            this.colLength3.ColumnEdit = this.repositoryItem2;
            this.colLength3.FieldName = "Length3";
            this.colLength3.Name = "colLength3";
            this.colLength3.Visible = true;
            this.colLength3.Width = 20;
            // 
            // colBdFtLD3
            // 
            this.colBdFtLD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD3.Caption = "B F";
            this.colBdFtLD3.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD3.FieldName = "BdFtLd3";
            this.colBdFtLD3.Name = "colBdFtLD3";
            this.colBdFtLD3.Visible = true;
            this.colBdFtLD3.Width = 20;
            // 
            // colBdFtDD3
            // 
            this.colBdFtDD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD3.Caption = "B I";
            this.colBdFtDD3.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD3.FieldName = "BdFtDd3";
            this.colBdFtDD3.Name = "colBdFtDD3";
            this.colBdFtDD3.Visible = true;
            this.colBdFtDD3.Width = 20;
            // 
            // colCuFtLD3
            // 
            this.colCuFtLD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD3.Caption = "C F";
            this.colCuFtLD3.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD3.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD3.FieldName = "CuFtLd3";
            this.colCuFtLD3.Name = "colCuFtLD3";
            this.colCuFtLD3.Visible = true;
            this.colCuFtLD3.Width = 20;
            // 
            // colCuFtDD3
            // 
            this.colCuFtDD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD3.Caption = "C I";
            this.colCuFtDD3.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD3.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD3.FieldName = "CuFtDd3";
            this.colCuFtDD3.Name = "colCuFtDD3";
            this.colCuFtDD3.Visible = true;
            this.colCuFtDD3.Width = 20;
            // 
            // colBdFtPD3
            // 
            this.colBdFtPD3.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD3.Caption = "%";
            this.colBdFtPD3.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD3.FieldName = "BdFtPd3";
            this.colBdFtPD3.Name = "colBdFtPD3";
            this.colBdFtPD3.Visible = true;
            this.colBdFtPD3.Width = 20;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Segment 4";
            this.gridBand15.Columns.Add(this.colSort4);
            this.gridBand15.Columns.Add(this.colGrade4);
            this.gridBand15.Columns.Add(this.colLength4);
            this.gridBand15.Columns.Add(this.colBdFtLD4);
            this.gridBand15.Columns.Add(this.colBdFtDD4);
            this.gridBand15.Columns.Add(this.colCuFtLD4);
            this.gridBand15.Columns.Add(this.colCuFtDD4);
            this.gridBand15.Columns.Add(this.colBdFtPD4);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 7;
            this.gridBand15.Width = 160;
            // 
            // colSort4
            // 
            this.colSort4.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort4.Caption = "S";
            this.colSort4.ColumnEdit = this.repositoryItem1;
            this.colSort4.FieldName = "Sort4";
            this.colSort4.Name = "colSort4";
            this.colSort4.Visible = true;
            this.colSort4.Width = 20;
            // 
            // colGrade4
            // 
            this.colGrade4.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade4.Caption = "G";
            this.colGrade4.ColumnEdit = this.repositoryItem1;
            this.colGrade4.FieldName = "Grade4";
            this.colGrade4.Name = "colGrade4";
            this.colGrade4.Visible = true;
            this.colGrade4.Width = 20;
            // 
            // colLength4
            // 
            this.colLength4.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength4.Caption = "Ln";
            this.colLength4.ColumnEdit = this.repositoryItem2;
            this.colLength4.FieldName = "Length4";
            this.colLength4.Name = "colLength4";
            this.colLength4.Visible = true;
            this.colLength4.Width = 20;
            // 
            // colBdFtLD4
            // 
            this.colBdFtLD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD4.Caption = "B F";
            this.colBdFtLD4.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD4.FieldName = "BdFtLd4";
            this.colBdFtLD4.Name = "colBdFtLD4";
            this.colBdFtLD4.Visible = true;
            this.colBdFtLD4.Width = 20;
            // 
            // colBdFtDD4
            // 
            this.colBdFtDD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD4.Caption = "B I";
            this.colBdFtDD4.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD4.FieldName = "BdFtDd4";
            this.colBdFtDD4.Name = "colBdFtDD4";
            this.colBdFtDD4.Visible = true;
            this.colBdFtDD4.Width = 20;
            // 
            // colCuFtLD4
            // 
            this.colCuFtLD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD4.Caption = "C F";
            this.colCuFtLD4.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD4.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD4.FieldName = "CuFtLd4";
            this.colCuFtLD4.Name = "colCuFtLD4";
            this.colCuFtLD4.Visible = true;
            this.colCuFtLD4.Width = 20;
            // 
            // colCuFtDD4
            // 
            this.colCuFtDD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD4.Caption = "C I";
            this.colCuFtDD4.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD4.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD4.FieldName = "CuFtDd4";
            this.colCuFtDD4.Name = "colCuFtDD4";
            this.colCuFtDD4.Visible = true;
            this.colCuFtDD4.Width = 20;
            // 
            // colBdFtPD4
            // 
            this.colBdFtPD4.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD4.Caption = "%";
            this.colBdFtPD4.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD4.FieldName = "BdFtPd4";
            this.colBdFtPD4.Name = "colBdFtPD4";
            this.colBdFtPD4.Visible = true;
            this.colBdFtPD4.Width = 20;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "Segment 5";
            this.gridBand16.Columns.Add(this.colSort5);
            this.gridBand16.Columns.Add(this.colGrade5);
            this.gridBand16.Columns.Add(this.colLength5);
            this.gridBand16.Columns.Add(this.colBdFtLD5);
            this.gridBand16.Columns.Add(this.colBdFtDD5);
            this.gridBand16.Columns.Add(this.colCuFtLD5);
            this.gridBand16.Columns.Add(this.colCuFtDD5);
            this.gridBand16.Columns.Add(this.colBdFtPD5);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 8;
            this.gridBand16.Width = 160;
            // 
            // colSort5
            // 
            this.colSort5.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort5.Caption = "S";
            this.colSort5.ColumnEdit = this.repositoryItem1;
            this.colSort5.FieldName = "Sort5";
            this.colSort5.Name = "colSort5";
            this.colSort5.Visible = true;
            this.colSort5.Width = 20;
            // 
            // colGrade5
            // 
            this.colGrade5.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade5.Caption = "G";
            this.colGrade5.ColumnEdit = this.repositoryItem1;
            this.colGrade5.FieldName = "Grade5";
            this.colGrade5.Name = "colGrade5";
            this.colGrade5.Visible = true;
            this.colGrade5.Width = 20;
            // 
            // colLength5
            // 
            this.colLength5.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength5.Caption = "Ln";
            this.colLength5.ColumnEdit = this.repositoryItem2;
            this.colLength5.FieldName = "Length5";
            this.colLength5.Name = "colLength5";
            this.colLength5.Visible = true;
            this.colLength5.Width = 20;
            // 
            // colBdFtLD5
            // 
            this.colBdFtLD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD5.Caption = "B F";
            this.colBdFtLD5.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD5.FieldName = "BdFtLd5";
            this.colBdFtLD5.Name = "colBdFtLD5";
            this.colBdFtLD5.Visible = true;
            this.colBdFtLD5.Width = 20;
            // 
            // colBdFtDD5
            // 
            this.colBdFtDD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD5.Caption = "B I";
            this.colBdFtDD5.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD5.FieldName = "BdFtDd5";
            this.colBdFtDD5.Name = "colBdFtDD5";
            this.colBdFtDD5.Visible = true;
            this.colBdFtDD5.Width = 20;
            // 
            // colCuFtLD5
            // 
            this.colCuFtLD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD5.Caption = "C F";
            this.colCuFtLD5.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD5.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD5.FieldName = "CuFtLd5";
            this.colCuFtLD5.Name = "colCuFtLD5";
            this.colCuFtLD5.Visible = true;
            this.colCuFtLD5.Width = 20;
            // 
            // colCuFtDD5
            // 
            this.colCuFtDD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD5.Caption = "C I";
            this.colCuFtDD5.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD5.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD5.FieldName = "CuFtDd5";
            this.colCuFtDD5.Name = "colCuFtDD5";
            this.colCuFtDD5.Visible = true;
            this.colCuFtDD5.Width = 20;
            // 
            // colBdFtPD5
            // 
            this.colBdFtPD5.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD5.Caption = "%";
            this.colBdFtPD5.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD5.FieldName = "BdFtPd5";
            this.colBdFtPD5.Name = "colBdFtPD5";
            this.colBdFtPD5.Visible = true;
            this.colBdFtPD5.Width = 20;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "Segment 6";
            this.gridBand17.Columns.Add(this.colSort6);
            this.gridBand17.Columns.Add(this.colGrade6);
            this.gridBand17.Columns.Add(this.colLength6);
            this.gridBand17.Columns.Add(this.colBdFtLD6);
            this.gridBand17.Columns.Add(this.colBdFtDD6);
            this.gridBand17.Columns.Add(this.colCuFtLD6);
            this.gridBand17.Columns.Add(this.colCuFtDD6);
            this.gridBand17.Columns.Add(this.colBdFtPD6);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 9;
            this.gridBand17.Width = 160;
            // 
            // colSort6
            // 
            this.colSort6.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort6.Caption = "S";
            this.colSort6.ColumnEdit = this.repositoryItem1;
            this.colSort6.FieldName = "Sort6";
            this.colSort6.Name = "colSort6";
            this.colSort6.Visible = true;
            this.colSort6.Width = 20;
            // 
            // colGrade6
            // 
            this.colGrade6.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade6.Caption = "G";
            this.colGrade6.ColumnEdit = this.repositoryItem1;
            this.colGrade6.FieldName = "Grade6";
            this.colGrade6.Name = "colGrade6";
            this.colGrade6.Visible = true;
            this.colGrade6.Width = 20;
            // 
            // colLength6
            // 
            this.colLength6.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength6.Caption = "Ln";
            this.colLength6.ColumnEdit = this.repositoryItem2;
            this.colLength6.FieldName = "Length6";
            this.colLength6.Name = "colLength6";
            this.colLength6.Visible = true;
            this.colLength6.Width = 20;
            // 
            // colBdFtLD6
            // 
            this.colBdFtLD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD6.Caption = "B F";
            this.colBdFtLD6.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD6.FieldName = "BdFtLd6";
            this.colBdFtLD6.Name = "colBdFtLD6";
            this.colBdFtLD6.Visible = true;
            this.colBdFtLD6.Width = 20;
            // 
            // colBdFtDD6
            // 
            this.colBdFtDD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD6.Caption = "B I";
            this.colBdFtDD6.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD6.FieldName = "BdFtDd6";
            this.colBdFtDD6.Name = "colBdFtDD6";
            this.colBdFtDD6.Visible = true;
            this.colBdFtDD6.Width = 20;
            // 
            // colCuFtLD6
            // 
            this.colCuFtLD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD6.Caption = "C F";
            this.colCuFtLD6.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD6.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD6.FieldName = "CuFtLd6";
            this.colCuFtLD6.Name = "colCuFtLD6";
            this.colCuFtLD6.Visible = true;
            this.colCuFtLD6.Width = 20;
            // 
            // colCuFtDD6
            // 
            this.colCuFtDD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD6.Caption = "C I";
            this.colCuFtDD6.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD6.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD6.FieldName = "CuFtDd6";
            this.colCuFtDD6.Name = "colCuFtDD6";
            this.colCuFtDD6.Visible = true;
            this.colCuFtDD6.Width = 20;
            // 
            // colBdFtPD6
            // 
            this.colBdFtPD6.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD6.Caption = "%";
            this.colBdFtPD6.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD6.FieldName = "BdFtPd6";
            this.colBdFtPD6.Name = "colBdFtPD6";
            this.colBdFtPD6.Visible = true;
            this.colBdFtPD6.Width = 20;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "Segment 7";
            this.gridBand18.Columns.Add(this.colSort7);
            this.gridBand18.Columns.Add(this.colGrade7);
            this.gridBand18.Columns.Add(this.colLength7);
            this.gridBand18.Columns.Add(this.colBdFtLD7);
            this.gridBand18.Columns.Add(this.colBdFtDD7);
            this.gridBand18.Columns.Add(this.colCuFtLD7);
            this.gridBand18.Columns.Add(this.colCuFtDD7);
            this.gridBand18.Columns.Add(this.colBdFtPD7);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 10;
            this.gridBand18.Width = 160;
            // 
            // colSort7
            // 
            this.colSort7.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort7.Caption = "S";
            this.colSort7.ColumnEdit = this.repositoryItem1;
            this.colSort7.FieldName = "Sort7";
            this.colSort7.Name = "colSort7";
            this.colSort7.Visible = true;
            this.colSort7.Width = 20;
            // 
            // colGrade7
            // 
            this.colGrade7.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade7.Caption = "G";
            this.colGrade7.ColumnEdit = this.repositoryItem1;
            this.colGrade7.FieldName = "Grade7";
            this.colGrade7.Name = "colGrade7";
            this.colGrade7.Visible = true;
            this.colGrade7.Width = 20;
            // 
            // colLength7
            // 
            this.colLength7.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength7.Caption = "Ln";
            this.colLength7.ColumnEdit = this.repositoryItem2;
            this.colLength7.FieldName = "Length7";
            this.colLength7.Name = "colLength7";
            this.colLength7.Visible = true;
            this.colLength7.Width = 20;
            // 
            // colBdFtLD7
            // 
            this.colBdFtLD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD7.Caption = "B F";
            this.colBdFtLD7.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD7.FieldName = "BdFtLd7";
            this.colBdFtLD7.Name = "colBdFtLD7";
            this.colBdFtLD7.Visible = true;
            this.colBdFtLD7.Width = 20;
            // 
            // colBdFtDD7
            // 
            this.colBdFtDD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD7.Caption = "B I";
            this.colBdFtDD7.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD7.FieldName = "BdFtDd7";
            this.colBdFtDD7.Name = "colBdFtDD7";
            this.colBdFtDD7.Visible = true;
            this.colBdFtDD7.Width = 20;
            // 
            // colCuFtLD7
            // 
            this.colCuFtLD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD7.Caption = "C F";
            this.colCuFtLD7.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD7.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD7.FieldName = "CuFtLd7";
            this.colCuFtLD7.Name = "colCuFtLD7";
            this.colCuFtLD7.Visible = true;
            this.colCuFtLD7.Width = 20;
            // 
            // colCuFtDD7
            // 
            this.colCuFtDD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD7.Caption = "C I";
            this.colCuFtDD7.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD7.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD7.FieldName = "CuFtDd7";
            this.colCuFtDD7.Name = "colCuFtDD7";
            this.colCuFtDD7.Visible = true;
            this.colCuFtDD7.Width = 20;
            // 
            // colBdFtPD7
            // 
            this.colBdFtPD7.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD7.Caption = "%";
            this.colBdFtPD7.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD7.FieldName = "BdFtPd7";
            this.colBdFtPD7.Name = "colBdFtPD7";
            this.colBdFtPD7.Visible = true;
            this.colBdFtPD7.Width = 20;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "Segment 8";
            this.gridBand19.Columns.Add(this.colSort8);
            this.gridBand19.Columns.Add(this.colGrade8);
            this.gridBand19.Columns.Add(this.colLength8);
            this.gridBand19.Columns.Add(this.colBdFtLD8);
            this.gridBand19.Columns.Add(this.colBdFtDD8);
            this.gridBand19.Columns.Add(this.colCuFtLD8);
            this.gridBand19.Columns.Add(this.colCuFtDD8);
            this.gridBand19.Columns.Add(this.colBdFtPD8);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 11;
            this.gridBand19.Width = 160;
            // 
            // colSort8
            // 
            this.colSort8.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort8.Caption = "S";
            this.colSort8.ColumnEdit = this.repositoryItem1;
            this.colSort8.FieldName = "Sort8";
            this.colSort8.Name = "colSort8";
            this.colSort8.Visible = true;
            this.colSort8.Width = 20;
            // 
            // colGrade8
            // 
            this.colGrade8.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade8.Caption = "G";
            this.colGrade8.ColumnEdit = this.repositoryItem1;
            this.colGrade8.FieldName = "Grade8";
            this.colGrade8.Name = "colGrade8";
            this.colGrade8.Visible = true;
            this.colGrade8.Width = 20;
            // 
            // colLength8
            // 
            this.colLength8.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength8.Caption = "Ln";
            this.colLength8.ColumnEdit = this.repositoryItem2;
            this.colLength8.FieldName = "Length8";
            this.colLength8.Name = "colLength8";
            this.colLength8.Visible = true;
            this.colLength8.Width = 20;
            // 
            // colBdFtLD8
            // 
            this.colBdFtLD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD8.Caption = "B F";
            this.colBdFtLD8.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD8.FieldName = "BdFtLd8";
            this.colBdFtLD8.Name = "colBdFtLD8";
            this.colBdFtLD8.Visible = true;
            this.colBdFtLD8.Width = 20;
            // 
            // colBdFtDD8
            // 
            this.colBdFtDD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD8.Caption = "B I";
            this.colBdFtDD8.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD8.FieldName = "BdFtDd8";
            this.colBdFtDD8.Name = "colBdFtDD8";
            this.colBdFtDD8.Visible = true;
            this.colBdFtDD8.Width = 20;
            // 
            // colCuFtLD8
            // 
            this.colCuFtLD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD8.Caption = "C F";
            this.colCuFtLD8.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD8.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD8.FieldName = "CuFtLd8";
            this.colCuFtLD8.Name = "colCuFtLD8";
            this.colCuFtLD8.Visible = true;
            this.colCuFtLD8.Width = 20;
            // 
            // colCuFtDD8
            // 
            this.colCuFtDD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD8.Caption = "C I";
            this.colCuFtDD8.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD8.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD8.FieldName = "CuFtDd8";
            this.colCuFtDD8.Name = "colCuFtDD8";
            this.colCuFtDD8.Visible = true;
            this.colCuFtDD8.Width = 20;
            // 
            // colBdFtPD8
            // 
            this.colBdFtPD8.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD8.Caption = "%";
            this.colBdFtPD8.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD8.FieldName = "BdFtPd8";
            this.colBdFtPD8.Name = "colBdFtPD8";
            this.colBdFtPD8.Visible = true;
            this.colBdFtPD8.Width = 20;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "Segment 9";
            this.gridBand20.Columns.Add(this.colSort9);
            this.gridBand20.Columns.Add(this.colGrade9);
            this.gridBand20.Columns.Add(this.colLength9);
            this.gridBand20.Columns.Add(this.colBdFtLD9);
            this.gridBand20.Columns.Add(this.colBdFtDD9);
            this.gridBand20.Columns.Add(this.colCuFtLD9);
            this.gridBand20.Columns.Add(this.colCuFtDD9);
            this.gridBand20.Columns.Add(this.colBdFtPD9);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 12;
            this.gridBand20.Width = 160;
            // 
            // colSort9
            // 
            this.colSort9.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort9.Caption = "S";
            this.colSort9.ColumnEdit = this.repositoryItem1;
            this.colSort9.FieldName = "Sort9";
            this.colSort9.Name = "colSort9";
            this.colSort9.Visible = true;
            this.colSort9.Width = 20;
            // 
            // colGrade9
            // 
            this.colGrade9.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade9.Caption = "G";
            this.colGrade9.ColumnEdit = this.repositoryItem1;
            this.colGrade9.FieldName = "Grade9";
            this.colGrade9.Name = "colGrade9";
            this.colGrade9.Visible = true;
            this.colGrade9.Width = 20;
            // 
            // colLength9
            // 
            this.colLength9.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength9.Caption = "Ln";
            this.colLength9.ColumnEdit = this.repositoryItem2;
            this.colLength9.FieldName = "Length9";
            this.colLength9.Name = "colLength9";
            this.colLength9.Visible = true;
            this.colLength9.Width = 20;
            // 
            // colBdFtLD9
            // 
            this.colBdFtLD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD9.Caption = "B F";
            this.colBdFtLD9.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD9.FieldName = "BdFtLd9";
            this.colBdFtLD9.Name = "colBdFtLD9";
            this.colBdFtLD9.Visible = true;
            this.colBdFtLD9.Width = 20;
            // 
            // colBdFtDD9
            // 
            this.colBdFtDD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD9.Caption = "B I";
            this.colBdFtDD9.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD9.FieldName = "BdFtDd9";
            this.colBdFtDD9.Name = "colBdFtDD9";
            this.colBdFtDD9.Visible = true;
            this.colBdFtDD9.Width = 20;
            // 
            // colCuFtLD9
            // 
            this.colCuFtLD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD9.Caption = "C F";
            this.colCuFtLD9.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD9.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD9.FieldName = "CuFtLd9";
            this.colCuFtLD9.Name = "colCuFtLD9";
            this.colCuFtLD9.Visible = true;
            this.colCuFtLD9.Width = 20;
            // 
            // colCuFtDD9
            // 
            this.colCuFtDD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD9.Caption = "C I";
            this.colCuFtDD9.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD9.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD9.FieldName = "CuFtDd9";
            this.colCuFtDD9.Name = "colCuFtDD9";
            this.colCuFtDD9.Visible = true;
            this.colCuFtDD9.Width = 20;
            // 
            // colBdFtPD9
            // 
            this.colBdFtPD9.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD9.Caption = "%";
            this.colBdFtPD9.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD9.FieldName = "BdFtPd9";
            this.colBdFtPD9.Name = "colBdFtPD9";
            this.colBdFtPD9.Visible = true;
            this.colBdFtPD9.Width = 20;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Segment 10";
            this.gridBand21.Columns.Add(this.colSort10);
            this.gridBand21.Columns.Add(this.colGrade10);
            this.gridBand21.Columns.Add(this.colLength10);
            this.gridBand21.Columns.Add(this.colBdFtLD10);
            this.gridBand21.Columns.Add(this.colBdFtDD10);
            this.gridBand21.Columns.Add(this.colCuFtLD10);
            this.gridBand21.Columns.Add(this.colCuFtDD10);
            this.gridBand21.Columns.Add(this.colBdFtPD10);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 13;
            this.gridBand21.Width = 160;
            // 
            // colSort10
            // 
            this.colSort10.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort10.Caption = "S";
            this.colSort10.ColumnEdit = this.repositoryItem1;
            this.colSort10.FieldName = "Sort10";
            this.colSort10.Name = "colSort10";
            this.colSort10.Visible = true;
            this.colSort10.Width = 20;
            // 
            // colGrade10
            // 
            this.colGrade10.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade10.Caption = "G";
            this.colGrade10.ColumnEdit = this.repositoryItem1;
            this.colGrade10.FieldName = "Grade10";
            this.colGrade10.Name = "colGrade10";
            this.colGrade10.Visible = true;
            this.colGrade10.Width = 20;
            // 
            // colLength10
            // 
            this.colLength10.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength10.Caption = "Ln";
            this.colLength10.ColumnEdit = this.repositoryItem2;
            this.colLength10.FieldName = "Length10";
            this.colLength10.Name = "colLength10";
            this.colLength10.Visible = true;
            this.colLength10.Width = 20;
            // 
            // colBdFtLD10
            // 
            this.colBdFtLD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD10.Caption = "B F";
            this.colBdFtLD10.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD10.FieldName = "BdFtLd10";
            this.colBdFtLD10.Name = "colBdFtLD10";
            this.colBdFtLD10.Visible = true;
            this.colBdFtLD10.Width = 20;
            // 
            // colBdFtDD10
            // 
            this.colBdFtDD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD10.Caption = "B I";
            this.colBdFtDD10.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD10.FieldName = "BdFtDd10";
            this.colBdFtDD10.Name = "colBdFtDD10";
            this.colBdFtDD10.Visible = true;
            this.colBdFtDD10.Width = 20;
            // 
            // colCuFtLD10
            // 
            this.colCuFtLD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD10.Caption = "C F";
            this.colCuFtLD10.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD10.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD10.FieldName = "CuFtLd10";
            this.colCuFtLD10.Name = "colCuFtLD10";
            this.colCuFtLD10.Visible = true;
            this.colCuFtLD10.Width = 20;
            // 
            // colCuFtDD10
            // 
            this.colCuFtDD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD10.Caption = "C I";
            this.colCuFtDD10.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD10.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD10.FieldName = "CuFtDd10";
            this.colCuFtDD10.Name = "colCuFtDD10";
            this.colCuFtDD10.Visible = true;
            this.colCuFtDD10.Width = 20;
            // 
            // colBdFtPD10
            // 
            this.colBdFtPD10.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD10.Caption = "%";
            this.colBdFtPD10.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD10.FieldName = "BdFtPd10";
            this.colBdFtPD10.Name = "colBdFtPD10";
            this.colBdFtPD10.Visible = true;
            this.colBdFtPD10.Width = 20;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "Segment 11";
            this.gridBand22.Columns.Add(this.colSort11);
            this.gridBand22.Columns.Add(this.colGrade11);
            this.gridBand22.Columns.Add(this.colLength11);
            this.gridBand22.Columns.Add(this.colBdFtLD11);
            this.gridBand22.Columns.Add(this.colBdFtDD11);
            this.gridBand22.Columns.Add(this.colCuFtLD11);
            this.gridBand22.Columns.Add(this.colCuFtDD11);
            this.gridBand22.Columns.Add(this.colBdFtPD11);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 14;
            this.gridBand22.Width = 160;
            // 
            // colSort11
            // 
            this.colSort11.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort11.Caption = "S";
            this.colSort11.ColumnEdit = this.repositoryItem1;
            this.colSort11.FieldName = "Sort11";
            this.colSort11.Name = "colSort11";
            this.colSort11.Visible = true;
            this.colSort11.Width = 20;
            // 
            // colGrade11
            // 
            this.colGrade11.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade11.Caption = "G";
            this.colGrade11.ColumnEdit = this.repositoryItem1;
            this.colGrade11.FieldName = "Grade11";
            this.colGrade11.Name = "colGrade11";
            this.colGrade11.Visible = true;
            this.colGrade11.Width = 20;
            // 
            // colLength11
            // 
            this.colLength11.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength11.Caption = "Ln";
            this.colLength11.ColumnEdit = this.repositoryItem2;
            this.colLength11.FieldName = "Length11";
            this.colLength11.Name = "colLength11";
            this.colLength11.Visible = true;
            this.colLength11.Width = 20;
            // 
            // colBdFtLD11
            // 
            this.colBdFtLD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD11.Caption = "B F";
            this.colBdFtLD11.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD11.FieldName = "BdFtLd11";
            this.colBdFtLD11.Name = "colBdFtLD11";
            this.colBdFtLD11.Visible = true;
            this.colBdFtLD11.Width = 20;
            // 
            // colBdFtDD11
            // 
            this.colBdFtDD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD11.Caption = "B I";
            this.colBdFtDD11.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD11.FieldName = "BdFtDd11";
            this.colBdFtDD11.Name = "colBdFtDD11";
            this.colBdFtDD11.Visible = true;
            this.colBdFtDD11.Width = 20;
            // 
            // colCuFtLD11
            // 
            this.colCuFtLD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD11.Caption = "C F";
            this.colCuFtLD11.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD11.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD11.FieldName = "CuFtLd11";
            this.colCuFtLD11.Name = "colCuFtLD11";
            this.colCuFtLD11.Visible = true;
            this.colCuFtLD11.Width = 20;
            // 
            // colCuFtDD11
            // 
            this.colCuFtDD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD11.Caption = "C I";
            this.colCuFtDD11.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD11.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD11.FieldName = "CuFtDd11";
            this.colCuFtDD11.Name = "colCuFtDD11";
            this.colCuFtDD11.Visible = true;
            this.colCuFtDD11.Width = 20;
            // 
            // colBdFtPD11
            // 
            this.colBdFtPD11.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD11.Caption = "%";
            this.colBdFtPD11.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD11.FieldName = "BdFtPd11";
            this.colBdFtPD11.Name = "colBdFtPD11";
            this.colBdFtPD11.Visible = true;
            this.colBdFtPD11.Width = 20;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "Segment 12";
            this.gridBand23.Columns.Add(this.colSort12);
            this.gridBand23.Columns.Add(this.colGrade12);
            this.gridBand23.Columns.Add(this.colLength12);
            this.gridBand23.Columns.Add(this.colBdFtLD12);
            this.gridBand23.Columns.Add(this.colBdFtDD12);
            this.gridBand23.Columns.Add(this.colCuFtLD12);
            this.gridBand23.Columns.Add(this.colCuFtDD12);
            this.gridBand23.Columns.Add(this.colBdFtPD12);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 15;
            this.gridBand23.Width = 160;
            // 
            // colSort12
            // 
            this.colSort12.AppearanceHeader.Options.UseTextOptions = true;
            this.colSort12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSort12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSort12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSort12.Caption = "S";
            this.colSort12.ColumnEdit = this.repositoryItem1;
            this.colSort12.FieldName = "Sort12";
            this.colSort12.Name = "colSort12";
            this.colSort12.Visible = true;
            this.colSort12.Width = 20;
            // 
            // colGrade12
            // 
            this.colGrade12.AppearanceHeader.Options.UseTextOptions = true;
            this.colGrade12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGrade12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGrade12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGrade12.Caption = "G";
            this.colGrade12.ColumnEdit = this.repositoryItem1;
            this.colGrade12.FieldName = "Grade12";
            this.colGrade12.Name = "colGrade12";
            this.colGrade12.Visible = true;
            this.colGrade12.Width = 20;
            // 
            // colLength12
            // 
            this.colLength12.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength12.Caption = "Ln";
            this.colLength12.ColumnEdit = this.repositoryItem2;
            this.colLength12.FieldName = "Length12";
            this.colLength12.Name = "colLength12";
            this.colLength12.Visible = true;
            this.colLength12.Width = 20;
            // 
            // colBdFtLD12
            // 
            this.colBdFtLD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD12.Caption = "B F";
            this.colBdFtLD12.ColumnEdit = this.repositoryItem1;
            this.colBdFtLD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD12.FieldName = "BdFtLd12";
            this.colBdFtLD12.Name = "colBdFtLD12";
            this.colBdFtLD12.Visible = true;
            this.colBdFtLD12.Width = 20;
            // 
            // colBdFtDD12
            // 
            this.colBdFtDD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD12.Caption = "B I";
            this.colBdFtDD12.ColumnEdit = this.repositoryItem1;
            this.colBdFtDD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD12.FieldName = "BdFtDd12";
            this.colBdFtDD12.Name = "colBdFtDD12";
            this.colBdFtDD12.Visible = true;
            this.colBdFtDD12.Width = 20;
            // 
            // colCuFtLD12
            // 
            this.colCuFtLD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD12.Caption = "C F";
            this.colCuFtLD12.ColumnEdit = this.repositoryItem1;
            this.colCuFtLD12.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD12.FieldName = "CuFtLd12";
            this.colCuFtLD12.Name = "colCuFtLD12";
            this.colCuFtLD12.Visible = true;
            this.colCuFtLD12.Width = 20;
            // 
            // colCuFtDD12
            // 
            this.colCuFtDD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD12.Caption = "C I";
            this.colCuFtDD12.ColumnEdit = this.repositoryItem1;
            this.colCuFtDD12.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD12.FieldName = "CuFtDd12";
            this.colCuFtDD12.Name = "colCuFtDD12";
            this.colCuFtDD12.Visible = true;
            this.colCuFtDD12.Width = 20;
            // 
            // colBdFtPD12
            // 
            this.colBdFtPD12.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD12.Caption = "%";
            this.colBdFtPD12.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtPD12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtPD12.FieldName = "BdFtPd12";
            this.colBdFtPD12.Name = "colBdFtPD12";
            this.colBdFtPD12.Visible = true;
            this.colBdFtPD12.Width = 20;
            // 
            // colPFType
            // 
            this.colPFType.Caption = "PFType";
            this.colPFType.FieldName = "PfType";
            this.colPFType.Name = "colPFType";
            // 
            // repositoryItemFormPoint
            // 
            this.repositoryItemFormPoint.AutoHeight = false;
            this.repositoryItemFormPoint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemFormPoint.MaxLength = 2;
            this.repositoryItemFormPoint.Name = "repositoryItemFormPoint";
            // 
            // repositoryItemFormFactor
            // 
            this.repositoryItemFormFactor.AutoHeight = false;
            this.repositoryItemFormFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemFormFactor.MaxLength = 2;
            this.repositoryItemFormFactor.Name = "repositoryItemFormFactor";
            // 
            // repositoryItemGrade
            // 
            this.repositoryItemGrade.AutoHeight = false;
            this.repositoryItemGrade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemGrade.MaxLength = 1;
            this.repositoryItemGrade.Name = "repositoryItemGrade";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Abbreviation", "Code"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit2.DataSource = this.speciesBindingSource;
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.tbTreesCostTable);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.tbTreesPriceTable);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.tbTreesGradeTable);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.tbTreesSortTable);
            this.panelControl2.Controls.Add(this.labelControl16);
            this.panelControl2.Controls.Add(this.tbTreesSpeciesTable);
            this.panelControl2.Controls.Add(this.labelControl17);
            this.panelControl2.Controls.Add(this.tbTreesNetAcres);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.tbTreesSection);
            this.panelControl2.Controls.Add(this.labelControl18);
            this.panelControl2.Controls.Add(this.tbTreesRange);
            this.panelControl2.Controls.Add(this.labelControl19);
            this.panelControl2.Controls.Add(this.tbTreesTownship);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Controls.Add(this.tbTreesStand);
            this.panelControl2.Controls.Add(this.labelControl21);
            this.panelControl2.Controls.Add(this.tbTreesTract);
            this.panelControl2.Controls.Add(this.labelControl22);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1388, 84);
            this.panelControl2.TabIndex = 1;
            // 
            // tbTreesCostTable
            // 
            this.tbTreesCostTable.Location = new System.Drawing.Point(1239, 39);
            this.tbTreesCostTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesCostTable.MenuManager = this.barManager1;
            this.tbTreesCostTable.Name = "tbTreesCostTable";
            this.tbTreesCostTable.Properties.ReadOnly = true;
            this.tbTreesCostTable.Size = new System.Drawing.Size(142, 22);
            this.tbTreesCostTable.TabIndex = 37;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(1283, 16);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(61, 16);
            this.labelControl12.TabIndex = 36;
            this.labelControl12.Text = "Cost Table";
            // 
            // tbTreesPriceTable
            // 
            this.tbTreesPriceTable.Location = new System.Drawing.Point(1053, 39);
            this.tbTreesPriceTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesPriceTable.MenuManager = this.barManager1;
            this.tbTreesPriceTable.Name = "tbTreesPriceTable";
            this.tbTreesPriceTable.Properties.ReadOnly = true;
            this.tbTreesPriceTable.Size = new System.Drawing.Size(142, 22);
            this.tbTreesPriceTable.TabIndex = 35;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(1091, 16);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(64, 16);
            this.labelControl14.TabIndex = 34;
            this.labelControl14.Text = "Price Table";
            // 
            // tbTreesGradeTable
            // 
            this.tbTreesGradeTable.Location = new System.Drawing.Point(864, 39);
            this.tbTreesGradeTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesGradeTable.MenuManager = this.barManager1;
            this.tbTreesGradeTable.Name = "tbTreesGradeTable";
            this.tbTreesGradeTable.Properties.ReadOnly = true;
            this.tbTreesGradeTable.Size = new System.Drawing.Size(142, 22);
            this.tbTreesGradeTable.TabIndex = 33;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(903, 16);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(70, 16);
            this.labelControl15.TabIndex = 32;
            this.labelControl15.Text = "Grade Table";
            // 
            // tbTreesSortTable
            // 
            this.tbTreesSortTable.Location = new System.Drawing.Point(675, 39);
            this.tbTreesSortTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesSortTable.MenuManager = this.barManager1;
            this.tbTreesSortTable.Name = "tbTreesSortTable";
            this.tbTreesSortTable.Properties.ReadOnly = true;
            this.tbTreesSortTable.Size = new System.Drawing.Size(142, 22);
            this.tbTreesSortTable.TabIndex = 31;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(717, 16);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(60, 16);
            this.labelControl16.TabIndex = 30;
            this.labelControl16.Text = "Sort Table";
            // 
            // tbTreesSpeciesTable
            // 
            this.tbTreesSpeciesTable.Location = new System.Drawing.Point(489, 39);
            this.tbTreesSpeciesTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesSpeciesTable.MenuManager = this.barManager1;
            this.tbTreesSpeciesTable.Name = "tbTreesSpeciesTable";
            this.tbTreesSpeciesTable.Properties.ReadOnly = true;
            this.tbTreesSpeciesTable.Size = new System.Drawing.Size(142, 22);
            this.tbTreesSpeciesTable.TabIndex = 29;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(527, 16);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(80, 16);
            this.labelControl17.TabIndex = 28;
            this.labelControl17.Text = "Species Table";
            // 
            // tbTreesNetAcres
            // 
            this.tbTreesNetAcres.Location = new System.Drawing.Point(399, 39);
            this.tbTreesNetAcres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesNetAcres.MenuManager = this.barManager1;
            this.tbTreesNetAcres.Name = "tbTreesNetAcres";
            this.tbTreesNetAcres.Properties.ReadOnly = true;
            this.tbTreesNetAcres.Size = new System.Drawing.Size(72, 22);
            this.tbTreesNetAcres.TabIndex = 25;
            // 
            // labelControl13
            // 
            this.labelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl13.Location = new System.Drawing.Point(407, 16);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(55, 16);
            this.labelControl13.TabIndex = 24;
            this.labelControl13.Text = "Net Acres";
            // 
            // tbTreesSection
            // 
            this.tbTreesSection.Location = new System.Drawing.Point(345, 39);
            this.tbTreesSection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesSection.MenuManager = this.barManager1;
            this.tbTreesSection.Name = "tbTreesSection";
            this.tbTreesSection.Properties.ReadOnly = true;
            this.tbTreesSection.Size = new System.Drawing.Size(34, 22);
            this.tbTreesSection.TabIndex = 15;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(352, 16);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(21, 16);
            this.labelControl18.TabIndex = 14;
            this.labelControl18.Text = "Sec";
            // 
            // tbTreesRange
            // 
            this.tbTreesRange.Location = new System.Drawing.Point(278, 39);
            this.tbTreesRange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesRange.MenuManager = this.barManager1;
            this.tbTreesRange.Name = "tbTreesRange";
            this.tbTreesRange.Properties.ReadOnly = true;
            this.tbTreesRange.Size = new System.Drawing.Size(45, 22);
            this.tbTreesRange.TabIndex = 13;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(289, 16);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(22, 16);
            this.labelControl19.TabIndex = 12;
            this.labelControl19.Text = "Rge";
            // 
            // tbTreesTownship
            // 
            this.tbTreesTownship.Location = new System.Drawing.Point(212, 39);
            this.tbTreesTownship.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesTownship.MenuManager = this.barManager1;
            this.tbTreesTownship.Name = "tbTreesTownship";
            this.tbTreesTownship.Properties.ReadOnly = true;
            this.tbTreesTownship.Size = new System.Drawing.Size(45, 22);
            this.tbTreesTownship.TabIndex = 11;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(220, 16);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(25, 16);
            this.labelControl20.TabIndex = 10;
            this.labelControl20.Text = "Twn";
            // 
            // tbTreesStand
            // 
            this.tbTreesStand.Location = new System.Drawing.Point(140, 39);
            this.tbTreesStand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesStand.MenuManager = this.barManager1;
            this.tbTreesStand.Name = "tbTreesStand";
            this.tbTreesStand.Properties.ReadOnly = true;
            this.tbTreesStand.Size = new System.Drawing.Size(52, 22);
            this.tbTreesStand.TabIndex = 9;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(143, 16);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(46, 16);
            this.labelControl21.TabIndex = 8;
            this.labelControl21.Text = "Stand #";
            // 
            // tbTreesTract
            // 
            this.tbTreesTract.Location = new System.Drawing.Point(6, 39);
            this.tbTreesTract.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTreesTract.MenuManager = this.barManager1;
            this.tbTreesTract.Name = "tbTreesTract";
            this.tbTreesTract.Properties.ReadOnly = true;
            this.tbTreesTract.Size = new System.Drawing.Size(117, 22);
            this.tbTreesTract.TabIndex = 7;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(45, 16);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(30, 16);
            this.labelControl22.TabIndex = 6;
            this.labelControl22.Text = "Tract";
            // 
            // tabNavigationPageTreeEdit
            // 
            this.tabNavigationPageTreeEdit.Caption = "Tree Edit";
            this.tabNavigationPageTreeEdit.Controls.Add(this.gridControlSegments);
            this.tabNavigationPageTreeEdit.Controls.Add(this.panelControl3);
            this.tabNavigationPageTreeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPageTreeEdit.Name = "tabNavigationPageTreeEdit";
            this.tabNavigationPageTreeEdit.Size = new System.Drawing.Size(1388, 713);
            // 
            // gridControlSegments
            // 
            this.gridControlSegments.DataSource = this.segmentsBindingSource;
            this.gridControlSegments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSegments.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlSegments.Location = new System.Drawing.Point(0, 84);
            this.gridControlSegments.MainView = this.gridViewSegments;
            this.gridControlSegments.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlSegments.MenuManager = this.barManager1;
            this.gridControlSegments.Name = "gridControlSegments";
            this.gridControlSegments.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit5,
            this.repositoryItemTextEdit6});
            this.gridControlSegments.Size = new System.Drawing.Size(1388, 629);
            this.gridControlSegments.TabIndex = 2;
            this.gridControlSegments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSegments});
            // 
            // segmentsBindingSource
            // 
            this.segmentsBindingSource.DataSource = typeof(Project.DAL.Treesegment);
            // 
            // gridViewSegments
            // 
            this.gridViewSegments.ColumnPanelRowHeight = 40;
            this.gridViewSegments.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeSegmentsID,
            this.colTreesId1,
            this.colGroupPlotNumber,
            this.colGroupTreeNumber,
            this.colSegmentNumber,
            this.colBdFtLD,
            this.colBdFtDD,
            this.colCuFtLD,
            this.colCuFtDD,
            this.colSortCode,
            this.colGradeCode,
            this.colScribnerNetVolume,
            this.colCubicNetVolume,
            this.colStandsId3,
            this.colLength,
            this.colCalcTopDia,
            this.colCalcButtDia,
            this.colComments,
            this.colPlotId,
            this.colSpecies1,
            this.colTreeBasalArea,
            this.colTreeTreesPerAcre,
            this.colCalcAccumulatedLength,
            this.colBdFtPD,
            this.colPlotNumber1,
            this.colTreeNumber1,
            this.colPlotFactorInput1,
            this.colAge1,
            this.colTreeStatusDisplayCode1,
            this.colTreeCount1,
            this.colDbh1,
            this.colFormPoint1,
            this.colFormFactor,
            this.colTDF,
            this.colBoleHeight1,
            this.colTotalHeight1,
            this.colCrownPosition,
            this.colCrownRatio,
            this.colVigor,
            this.colDamage,
            this.colUserDefined,
            this.colBark,
            this.colLogValue,
            this.colAO,
            this.colCalcTotalHtUsedYN1,
            this.colTreeType1,
            this.colPFType1});
            this.gridViewSegments.GridControl = this.gridControlSegments;
            this.gridViewSegments.GroupCount = 2;
            this.gridViewSegments.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ScribnerNetVolume", this.colScribnerNetVolume, "{0:0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CubicNetVolume", this.colCubicNetVolume, "{0:0}")});
            this.gridViewSegments.Name = "gridViewSegments";
            this.gridViewSegments.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewSegments.OptionsCustomization.AllowFilter = false;
            this.gridViewSegments.OptionsCustomization.AllowGroup = false;
            this.gridViewSegments.OptionsCustomization.AllowSort = false;
            this.gridViewSegments.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewSegments.OptionsView.ColumnAutoWidth = false;
            this.gridViewSegments.OptionsView.ShowFooter = true;
            this.gridViewSegments.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridViewSegments.OptionsView.ShowGroupPanel = false;
            this.gridViewSegments.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupPlotNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupTreeNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewSegments.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewSegments_RowCellStyle);
            this.gridViewSegments.CalcRowHeight += new DevExpress.XtraGrid.Views.Grid.RowHeightEventHandler(this.gridViewSegments_CalcRowHeight);
            this.gridViewSegments.GroupRowCollapsing += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewSegments_GroupRowCollapsing);
            this.gridViewSegments.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewSegments_ShowingEditor);
            this.gridViewSegments.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewSegments_InvalidRowException);
            this.gridViewSegments.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewSegments_ValidateRow);
            this.gridViewSegments.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewSegments_RowUpdated);
            this.gridViewSegments.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewSegments_CustomColumnDisplayText);
            this.gridViewSegments.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridViewSegments_PrintInitialize);
            this.gridViewSegments.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewSegments_KeyDown);
            this.gridViewSegments.Click += new System.EventHandler(this.gridViewSegments_Click);
            this.gridViewSegments.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewSegments_ValidatingEditor);
            this.gridViewSegments.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridViewSegments_InvalidValueException);
            // 
            // colTreeSegmentsID
            // 
            this.colTreeSegmentsID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeSegmentsID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeSegmentsID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeSegmentsID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeSegmentsID.FieldName = "TreeSegmentsID";
            this.colTreeSegmentsID.Name = "colTreeSegmentsID";
            // 
            // colTreesId1
            // 
            this.colTreesId1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreesId1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreesId1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreesId1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreesId1.FieldName = "TreesId";
            this.colTreesId1.Name = "colTreesId1";
            // 
            // colGroupPlotNumber
            // 
            this.colGroupPlotNumber.FieldName = "GroupPlotNumber";
            this.colGroupPlotNumber.Name = "colGroupPlotNumber";
            this.colGroupPlotNumber.Visible = true;
            this.colGroupPlotNumber.VisibleIndex = 2;
            // 
            // colGroupTreeNumber
            // 
            this.colGroupTreeNumber.FieldName = "GroupTreeNumber";
            this.colGroupTreeNumber.Name = "colGroupTreeNumber";
            this.colGroupTreeNumber.Visible = true;
            this.colGroupTreeNumber.VisibleIndex = 4;
            // 
            // colSegmentNumber
            // 
            this.colSegmentNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colSegmentNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSegmentNumber.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSegmentNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSegmentNumber.Caption = "S #";
            this.colSegmentNumber.DisplayFormat.FormatString = "{0:#}";
            this.colSegmentNumber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSegmentNumber.FieldName = "SegmentNumber";
            this.colSegmentNumber.Name = "colSegmentNumber";
            this.colSegmentNumber.Visible = true;
            this.colSegmentNumber.VisibleIndex = 18;
            this.colSegmentNumber.Width = 20;
            // 
            // colBdFtLD
            // 
            this.colBdFtLD.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtLD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtLD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtLD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtLD.Caption = "Bf F";
            this.colBdFtLD.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtLD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtLD.FieldName = "BdFtLd";
            this.colBdFtLD.Name = "colBdFtLD";
            this.colBdFtLD.Visible = true;
            this.colBdFtLD.VisibleIndex = 22;
            this.colBdFtLD.Width = 20;
            // 
            // colBdFtDD
            // 
            this.colBdFtDD.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtDD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtDD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtDD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtDD.Caption = "Bf I";
            this.colBdFtDD.DisplayFormat.FormatString = "{0:#}";
            this.colBdFtDD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBdFtDD.FieldName = "BdFtDd";
            this.colBdFtDD.Name = "colBdFtDD";
            this.colBdFtDD.Visible = true;
            this.colBdFtDD.VisibleIndex = 23;
            this.colBdFtDD.Width = 20;
            // 
            // colCuFtLD
            // 
            this.colCuFtLD.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtLD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtLD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtLD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtLD.Caption = "Cf F";
            this.colCuFtLD.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtLD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtLD.FieldName = "CuFtLd";
            this.colCuFtLD.Name = "colCuFtLD";
            this.colCuFtLD.Visible = true;
            this.colCuFtLD.VisibleIndex = 24;
            this.colCuFtLD.Width = 20;
            // 
            // colCuFtDD
            // 
            this.colCuFtDD.AppearanceHeader.Options.UseTextOptions = true;
            this.colCuFtDD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCuFtDD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCuFtDD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCuFtDD.Caption = "Cf I";
            this.colCuFtDD.DisplayFormat.FormatString = "{0:#}";
            this.colCuFtDD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCuFtDD.FieldName = "CuFtDd";
            this.colCuFtDD.Name = "colCuFtDD";
            this.colCuFtDD.Visible = true;
            this.colCuFtDD.VisibleIndex = 25;
            this.colCuFtDD.Width = 20;
            // 
            // colSortCode
            // 
            this.colSortCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colSortCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSortCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSortCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSortCode.Caption = "S";
            this.colSortCode.ColumnEdit = this.repositoryItemTextEdit2;
            this.colSortCode.FieldName = "SortCode";
            this.colSortCode.Name = "colSortCode";
            this.colSortCode.Visible = true;
            this.colSortCode.VisibleIndex = 19;
            this.colSortCode.Width = 20;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit2.MaxLength = 1;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colGradeCode
            // 
            this.colGradeCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colGradeCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGradeCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colGradeCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGradeCode.Caption = "G";
            this.colGradeCode.ColumnEdit = this.repositoryItemTextEdit2;
            this.colGradeCode.FieldName = "GradeCode";
            this.colGradeCode.Name = "colGradeCode";
            this.colGradeCode.Visible = true;
            this.colGradeCode.VisibleIndex = 20;
            this.colGradeCode.Width = 20;
            // 
            // colScribnerNetVolume
            // 
            this.colScribnerNetVolume.AppearanceHeader.Options.UseTextOptions = true;
            this.colScribnerNetVolume.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colScribnerNetVolume.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colScribnerNetVolume.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colScribnerNetVolume.Caption = "N BdFt";
            this.colScribnerNetVolume.DisplayFormat.FormatString = "{0:0}";
            this.colScribnerNetVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colScribnerNetVolume.FieldName = "ScribnerNetVolume";
            this.colScribnerNetVolume.Name = "colScribnerNetVolume";
            this.colScribnerNetVolume.Visible = true;
            this.colScribnerNetVolume.VisibleIndex = 35;
            this.colScribnerNetVolume.Width = 37;
            // 
            // colCubicNetVolume
            // 
            this.colCubicNetVolume.AppearanceHeader.Options.UseTextOptions = true;
            this.colCubicNetVolume.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCubicNetVolume.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCubicNetVolume.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCubicNetVolume.Caption = "N CuFt";
            this.colCubicNetVolume.DisplayFormat.FormatString = "{0:0}";
            this.colCubicNetVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCubicNetVolume.FieldName = "CubicNetVolume";
            this.colCubicNetVolume.Name = "colCubicNetVolume";
            this.colCubicNetVolume.Visible = true;
            this.colCubicNetVolume.VisibleIndex = 34;
            this.colCubicNetVolume.Width = 34;
            // 
            // colStandsId3
            // 
            this.colStandsId3.AppearanceHeader.Options.UseTextOptions = true;
            this.colStandsId3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStandsId3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStandsId3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStandsId3.FieldName = "StandsId";
            this.colStandsId3.Name = "colStandsId3";
            // 
            // colLength
            // 
            this.colLength.AppearanceHeader.Options.UseTextOptions = true;
            this.colLength.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLength.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLength.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLength.Caption = "LN";
            this.colLength.ColumnEdit = this.repositoryItemTextEdit4;
            this.colLength.DisplayFormat.FormatString = "{0:#}";
            this.colLength.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 21;
            this.colLength.Width = 20;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit4.MaxLength = 2;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colCalcTopDia
            // 
            this.colCalcTopDia.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTopDia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTopDia.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTopDia.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTopDia.Caption = "Top";
            this.colCalcTopDia.DisplayFormat.FormatString = "{0:0.0}";
            this.colCalcTopDia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcTopDia.FieldName = "CalcTopDia";
            this.colCalcTopDia.Name = "colCalcTopDia";
            this.colCalcTopDia.Visible = true;
            this.colCalcTopDia.VisibleIndex = 33;
            this.colCalcTopDia.Width = 31;
            // 
            // colCalcButtDia
            // 
            this.colCalcButtDia.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcButtDia.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcButtDia.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcButtDia.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcButtDia.Caption = "Butt";
            this.colCalcButtDia.DisplayFormat.FormatString = "{0:0.0}";
            this.colCalcButtDia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcButtDia.FieldName = "CalcButtDia";
            this.colCalcButtDia.Name = "colCalcButtDia";
            this.colCalcButtDia.Visible = true;
            this.colCalcButtDia.VisibleIndex = 32;
            this.colCalcButtDia.Width = 32;
            // 
            // colComments
            // 
            this.colComments.AppearanceHeader.Options.UseTextOptions = true;
            this.colComments.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComments.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colComments.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 37;
            // 
            // colPlotId
            // 
            this.colPlotId.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotId.FieldName = "PlotId";
            this.colPlotId.Name = "colPlotId";
            // 
            // colSpecies1
            // 
            this.colSpecies1.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies1.Caption = "Spp";
            this.colSpecies1.ColumnEdit = this.repositoryItemTextEdit5;
            this.colSpecies1.FieldName = "Species";
            this.colSpecies1.Name = "colSpecies1";
            this.colSpecies1.Visible = true;
            this.colSpecies1.VisibleIndex = 4;
            this.colSpecies1.Width = 29;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit5.MaxLength = 3;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // colTreeBasalArea
            // 
            this.colTreeBasalArea.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeBasalArea.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeBasalArea.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeBasalArea.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeBasalArea.Caption = "B/A";
            this.colTreeBasalArea.DisplayFormat.FormatString = "{0:0.00}";
            this.colTreeBasalArea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeBasalArea.FieldName = "TreeBasalArea";
            this.colTreeBasalArea.Name = "colTreeBasalArea";
            this.colTreeBasalArea.Visible = true;
            this.colTreeBasalArea.VisibleIndex = 29;
            this.colTreeBasalArea.Width = 31;
            // 
            // colTreeTreesPerAcre
            // 
            this.colTreeTreesPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeTreesPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeTreesPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeTreesPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeTreesPerAcre.Caption = "T/A";
            this.colTreeTreesPerAcre.DisplayFormat.FormatString = "{0:0.000}";
            this.colTreeTreesPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeTreesPerAcre.FieldName = "TreeTreesPerAcre";
            this.colTreeTreesPerAcre.Name = "colTreeTreesPerAcre";
            this.colTreeTreesPerAcre.Visible = true;
            this.colTreeTreesPerAcre.VisibleIndex = 30;
            this.colTreeTreesPerAcre.Width = 31;
            // 
            // colCalcAccumulatedLength
            // 
            this.colCalcAccumulatedLength.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcAccumulatedLength.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcAccumulatedLength.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcAccumulatedLength.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcAccumulatedLength.Caption = "Len";
            this.colCalcAccumulatedLength.DisplayFormat.FormatString = "{0:0}";
            this.colCalcAccumulatedLength.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalcAccumulatedLength.FieldName = "CalcAccumulatedLength";
            this.colCalcAccumulatedLength.Name = "colCalcAccumulatedLength";
            this.colCalcAccumulatedLength.Visible = true;
            this.colCalcAccumulatedLength.VisibleIndex = 31;
            this.colCalcAccumulatedLength.Width = 27;
            // 
            // colBdFtPD
            // 
            this.colBdFtPD.AppearanceHeader.Options.UseTextOptions = true;
            this.colBdFtPD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBdFtPD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBdFtPD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBdFtPD.Caption = "%";
            this.colBdFtPD.ColumnEdit = this.repositoryItemTextEdit2;
            this.colBdFtPD.FieldName = "BdFtPd";
            this.colBdFtPD.Name = "colBdFtPD";
            this.colBdFtPD.Visible = true;
            this.colBdFtPD.VisibleIndex = 26;
            this.colBdFtPD.Width = 24;
            // 
            // colPlotNumber1
            // 
            this.colPlotNumber1.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotNumber1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotNumber1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotNumber1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotNumber1.Caption = "Plot #";
            this.colPlotNumber1.ColumnEdit = this.repositoryItemTextEdit6;
            this.colPlotNumber1.DisplayFormat.FormatString = "{0:#}";
            this.colPlotNumber1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPlotNumber1.FieldName = "PlotNumber";
            this.colPlotNumber1.Name = "colPlotNumber1";
            this.colPlotNumber1.Visible = true;
            this.colPlotNumber1.VisibleIndex = 0;
            this.colPlotNumber1.Width = 62;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.repositoryItemTextEdit6.MaxLength = 4;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // colTreeNumber1
            // 
            this.colTreeNumber1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeNumber1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeNumber1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeNumber1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeNumber1.Caption = "Tree #";
            this.colTreeNumber1.DisplayFormat.FormatString = "{0:#}";
            this.colTreeNumber1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeNumber1.FieldName = "TreeNumber";
            this.colTreeNumber1.Name = "colTreeNumber1";
            this.colTreeNumber1.Visible = true;
            this.colTreeNumber1.VisibleIndex = 1;
            this.colTreeNumber1.Width = 37;
            // 
            // colPlotFactorInput1
            // 
            this.colPlotFactorInput1.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlotFactorInput1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlotFactorInput1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colPlotFactorInput1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlotFactorInput1.Caption = "PF";
            this.colPlotFactorInput1.FieldName = "PlotFactorInput";
            this.colPlotFactorInput1.Name = "colPlotFactorInput1";
            this.colPlotFactorInput1.Visible = true;
            this.colPlotFactorInput1.VisibleIndex = 2;
            this.colPlotFactorInput1.Width = 24;
            // 
            // colAge1
            // 
            this.colAge1.AppearanceHeader.Options.UseTextOptions = true;
            this.colAge1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAge1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAge1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAge1.Caption = "Age";
            this.colAge1.DisplayFormat.FormatString = "{0:#}";
            this.colAge1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAge1.FieldName = "Age";
            this.colAge1.Name = "colAge1";
            this.colAge1.Visible = true;
            this.colAge1.VisibleIndex = 3;
            this.colAge1.Width = 29;
            // 
            // colTreeStatusDisplayCode1
            // 
            this.colTreeStatusDisplayCode1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeStatusDisplayCode1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeStatusDisplayCode1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeStatusDisplayCode1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeStatusDisplayCode1.Caption = "ST";
            this.colTreeStatusDisplayCode1.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTreeStatusDisplayCode1.FieldName = "TreeStatusDisplayCode";
            this.colTreeStatusDisplayCode1.Name = "colTreeStatusDisplayCode1";
            this.colTreeStatusDisplayCode1.Visible = true;
            this.colTreeStatusDisplayCode1.VisibleIndex = 5;
            this.colTreeStatusDisplayCode1.Width = 21;
            // 
            // colTreeCount1
            // 
            this.colTreeCount1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreeCount1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeCount1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTreeCount1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTreeCount1.Caption = "TC";
            this.colTreeCount1.DisplayFormat.FormatString = "{0:#}";
            this.colTreeCount1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTreeCount1.FieldName = "TreeCount";
            this.colTreeCount1.Name = "colTreeCount1";
            this.colTreeCount1.Visible = true;
            this.colTreeCount1.VisibleIndex = 6;
            this.colTreeCount1.Width = 22;
            // 
            // colDbh1
            // 
            this.colDbh1.AppearanceHeader.Options.UseTextOptions = true;
            this.colDbh1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDbh1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDbh1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDbh1.Caption = "Dbh";
            this.colDbh1.DisplayFormat.FormatString = "{0:0.0}";
            this.colDbh1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDbh1.FieldName = "Dbh";
            this.colDbh1.Name = "colDbh1";
            this.colDbh1.Visible = true;
            this.colDbh1.VisibleIndex = 7;
            this.colDbh1.Width = 35;
            // 
            // colFormPoint1
            // 
            this.colFormPoint1.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormPoint1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormPoint1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormPoint1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormPoint1.Caption = "FP";
            this.colFormPoint1.DisplayFormat.FormatString = "{0:#}";
            this.colFormPoint1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFormPoint1.FieldName = "FormPoint";
            this.colFormPoint1.Name = "colFormPoint1";
            this.colFormPoint1.Visible = true;
            this.colFormPoint1.VisibleIndex = 8;
            this.colFormPoint1.Width = 22;
            // 
            // colFormFactor
            // 
            this.colFormFactor.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormFactor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormFactor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colFormFactor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFormFactor.Caption = "FF";
            this.colFormFactor.DisplayFormat.FormatString = "{0:#}";
            this.colFormFactor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFormFactor.FieldName = "FormFactor";
            this.colFormFactor.Name = "colFormFactor";
            this.colFormFactor.Visible = true;
            this.colFormFactor.VisibleIndex = 9;
            this.colFormFactor.Width = 22;
            // 
            // colTDF
            // 
            this.colTDF.AppearanceHeader.Options.UseTextOptions = true;
            this.colTDF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTDF.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTDF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTDF.ColumnEdit = this.repositoryItemTextEdit4;
            this.colTDF.FieldName = "Tdf";
            this.colTDF.Name = "colTDF";
            this.colTDF.Visible = true;
            this.colTDF.VisibleIndex = 10;
            this.colTDF.Width = 30;
            // 
            // colBoleHeight1
            // 
            this.colBoleHeight1.AppearanceHeader.Options.UseTextOptions = true;
            this.colBoleHeight1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBoleHeight1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBoleHeight1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBoleHeight1.Caption = "Bole Ht";
            this.colBoleHeight1.DisplayFormat.FormatString = "{0:#}";
            this.colBoleHeight1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBoleHeight1.FieldName = "BoleHeight";
            this.colBoleHeight1.Name = "colBoleHeight1";
            this.colBoleHeight1.Visible = true;
            this.colBoleHeight1.VisibleIndex = 11;
            this.colBoleHeight1.Width = 38;
            // 
            // colTotalHeight1
            // 
            this.colTotalHeight1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalHeight1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHeight1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colTotalHeight1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalHeight1.Caption = "Total Ht";
            this.colTotalHeight1.DisplayFormat.FormatString = "{0:#}";
            this.colTotalHeight1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalHeight1.FieldName = "TotalHeight";
            this.colTotalHeight1.Name = "colTotalHeight1";
            this.colTotalHeight1.Visible = true;
            this.colTotalHeight1.VisibleIndex = 12;
            this.colTotalHeight1.Width = 33;
            // 
            // colCrownPosition
            // 
            this.colCrownPosition.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownPosition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownPosition.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownPosition.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownPosition.Caption = "PO";
            this.colCrownPosition.ColumnEdit = this.repositoryItemTextEdit4;
            this.colCrownPosition.FieldName = "CrownPosition";
            this.colCrownPosition.Name = "colCrownPosition";
            this.colCrownPosition.Visible = true;
            this.colCrownPosition.VisibleIndex = 13;
            this.colCrownPosition.Width = 27;
            // 
            // colCrownRatio
            // 
            this.colCrownRatio.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrownRatio.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrownRatio.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCrownRatio.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrownRatio.Caption = "CR";
            this.colCrownRatio.ColumnEdit = this.repositoryItemTextEdit4;
            this.colCrownRatio.FieldName = "CrownRatio";
            this.colCrownRatio.Name = "colCrownRatio";
            this.colCrownRatio.Visible = true;
            this.colCrownRatio.VisibleIndex = 14;
            this.colCrownRatio.Width = 26;
            // 
            // colVigor
            // 
            this.colVigor.AppearanceHeader.Options.UseTextOptions = true;
            this.colVigor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVigor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colVigor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVigor.Caption = "VI";
            this.colVigor.ColumnEdit = this.repositoryItemTextEdit4;
            this.colVigor.FieldName = "Vigor";
            this.colVigor.Name = "colVigor";
            this.colVigor.Visible = true;
            this.colVigor.VisibleIndex = 15;
            this.colVigor.Width = 20;
            // 
            // colDamage
            // 
            this.colDamage.AppearanceHeader.Options.UseTextOptions = true;
            this.colDamage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDamage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colDamage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDamage.Caption = "DA";
            this.colDamage.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDamage.FieldName = "Damage";
            this.colDamage.Name = "colDamage";
            this.colDamage.Visible = true;
            this.colDamage.VisibleIndex = 16;
            this.colDamage.Width = 27;
            // 
            // colUserDefined
            // 
            this.colUserDefined.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserDefined.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserDefined.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUserDefined.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserDefined.Caption = "UD";
            this.colUserDefined.ColumnEdit = this.repositoryItemTextEdit4;
            this.colUserDefined.FieldName = "UserDefined";
            this.colUserDefined.Name = "colUserDefined";
            this.colUserDefined.Visible = true;
            this.colUserDefined.VisibleIndex = 17;
            this.colUserDefined.Width = 28;
            // 
            // colBark
            // 
            this.colBark.AppearanceHeader.Options.UseTextOptions = true;
            this.colBark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBark.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBark.DisplayFormat.FormatString = "{0:0.000}";
            this.colBark.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBark.FieldName = "Bark";
            this.colBark.Name = "colBark";
            this.colBark.Visible = true;
            this.colBark.VisibleIndex = 27;
            this.colBark.Width = 32;
            // 
            // colLogValue
            // 
            this.colLogValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colLogValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLogValue.Caption = "Log Value $";
            this.colLogValue.DisplayFormat.FormatString = "{0:0}";
            this.colLogValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLogValue.FieldName = "LogValue";
            this.colLogValue.Name = "colLogValue";
            this.colLogValue.Visible = true;
            this.colLogValue.VisibleIndex = 36;
            this.colLogValue.Width = 46;
            // 
            // colAO
            // 
            this.colAO.AppearanceHeader.Options.UseTextOptions = true;
            this.colAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colAO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAO.DisplayFormat.FormatString = "{0:#.###}";
            this.colAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAO.FieldName = "Ao";
            this.colAO.Name = "colAO";
            this.colAO.Visible = true;
            this.colAO.VisibleIndex = 28;
            this.colAO.Width = 37;
            // 
            // colCalcTotalHtUsedYN1
            // 
            this.colCalcTotalHtUsedYN1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCalcTotalHtUsedYN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCalcTotalHtUsedYN1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colCalcTotalHtUsedYN1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCalcTotalHtUsedYN1.FieldName = "CalcTotalHtUsedYn";
            this.colCalcTotalHtUsedYN1.Name = "colCalcTotalHtUsedYN1";
            // 
            // colTreeType1
            // 
            this.colTreeType1.Caption = "gridColumn5";
            this.colTreeType1.FieldName = "TreeType";
            this.colTreeType1.Name = "colTreeType1";
            // 
            // colPFType1
            // 
            this.colPFType1.Caption = "PFType";
            this.colPFType1.FieldName = "PfType";
            this.colPFType1.Name = "colPFType1";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.tbSegCostTable);
            this.panelControl3.Controls.Add(this.labelControl23);
            this.panelControl3.Controls.Add(this.tbSegPriceTable);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.tbSegGradeTable);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.tbSegSortTable);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.tbSegSpeciesTable);
            this.panelControl3.Controls.Add(this.labelControl28);
            this.panelControl3.Controls.Add(this.tbSegNetAcres);
            this.panelControl3.Controls.Add(this.labelControl24);
            this.panelControl3.Controls.Add(this.tbSegSection);
            this.panelControl3.Controls.Add(this.labelControl29);
            this.panelControl3.Controls.Add(this.tbSegRange);
            this.panelControl3.Controls.Add(this.labelControl30);
            this.panelControl3.Controls.Add(this.tbSegTownship);
            this.panelControl3.Controls.Add(this.labelControl31);
            this.panelControl3.Controls.Add(this.tbSegStand);
            this.panelControl3.Controls.Add(this.labelControl32);
            this.panelControl3.Controls.Add(this.tbSegTract);
            this.panelControl3.Controls.Add(this.labelControl33);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1388, 84);
            this.panelControl3.TabIndex = 1;
            // 
            // tbSegCostTable
            // 
            this.tbSegCostTable.Location = new System.Drawing.Point(1239, 39);
            this.tbSegCostTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegCostTable.MenuManager = this.barManager1;
            this.tbSegCostTable.Name = "tbSegCostTable";
            this.tbSegCostTable.Properties.ReadOnly = true;
            this.tbSegCostTable.Size = new System.Drawing.Size(142, 22);
            this.tbSegCostTable.TabIndex = 37;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(1283, 16);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(61, 16);
            this.labelControl23.TabIndex = 36;
            this.labelControl23.Text = "Cost Table";
            // 
            // tbSegPriceTable
            // 
            this.tbSegPriceTable.Location = new System.Drawing.Point(1053, 39);
            this.tbSegPriceTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegPriceTable.MenuManager = this.barManager1;
            this.tbSegPriceTable.Name = "tbSegPriceTable";
            this.tbSegPriceTable.Properties.ReadOnly = true;
            this.tbSegPriceTable.Size = new System.Drawing.Size(142, 22);
            this.tbSegPriceTable.TabIndex = 35;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(1091, 16);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(64, 16);
            this.labelControl25.TabIndex = 34;
            this.labelControl25.Text = "Price Table";
            // 
            // tbSegGradeTable
            // 
            this.tbSegGradeTable.Location = new System.Drawing.Point(864, 39);
            this.tbSegGradeTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegGradeTable.MenuManager = this.barManager1;
            this.tbSegGradeTable.Name = "tbSegGradeTable";
            this.tbSegGradeTable.Properties.ReadOnly = true;
            this.tbSegGradeTable.Size = new System.Drawing.Size(142, 22);
            this.tbSegGradeTable.TabIndex = 33;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(903, 16);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(70, 16);
            this.labelControl26.TabIndex = 32;
            this.labelControl26.Text = "Grade Table";
            // 
            // tbSegSortTable
            // 
            this.tbSegSortTable.Location = new System.Drawing.Point(675, 39);
            this.tbSegSortTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegSortTable.MenuManager = this.barManager1;
            this.tbSegSortTable.Name = "tbSegSortTable";
            this.tbSegSortTable.Properties.ReadOnly = true;
            this.tbSegSortTable.Size = new System.Drawing.Size(142, 22);
            this.tbSegSortTable.TabIndex = 31;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(717, 16);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(60, 16);
            this.labelControl27.TabIndex = 30;
            this.labelControl27.Text = "Sort Table";
            // 
            // tbSegSpeciesTable
            // 
            this.tbSegSpeciesTable.Location = new System.Drawing.Point(489, 39);
            this.tbSegSpeciesTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegSpeciesTable.MenuManager = this.barManager1;
            this.tbSegSpeciesTable.Name = "tbSegSpeciesTable";
            this.tbSegSpeciesTable.Properties.ReadOnly = true;
            this.tbSegSpeciesTable.Size = new System.Drawing.Size(142, 22);
            this.tbSegSpeciesTable.TabIndex = 29;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(527, 16);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(80, 16);
            this.labelControl28.TabIndex = 28;
            this.labelControl28.Text = "Species Table";
            // 
            // tbSegNetAcres
            // 
            this.tbSegNetAcres.Location = new System.Drawing.Point(399, 39);
            this.tbSegNetAcres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegNetAcres.MenuManager = this.barManager1;
            this.tbSegNetAcres.Name = "tbSegNetAcres";
            this.tbSegNetAcres.Properties.ReadOnly = true;
            this.tbSegNetAcres.Size = new System.Drawing.Size(72, 22);
            this.tbSegNetAcres.TabIndex = 25;
            // 
            // labelControl24
            // 
            this.labelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl24.Location = new System.Drawing.Point(407, 16);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(55, 16);
            this.labelControl24.TabIndex = 24;
            this.labelControl24.Text = "Net Acres";
            // 
            // tbSegSection
            // 
            this.tbSegSection.Location = new System.Drawing.Point(345, 39);
            this.tbSegSection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegSection.MenuManager = this.barManager1;
            this.tbSegSection.Name = "tbSegSection";
            this.tbSegSection.Properties.ReadOnly = true;
            this.tbSegSection.Size = new System.Drawing.Size(34, 22);
            this.tbSegSection.TabIndex = 15;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(352, 16);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(21, 16);
            this.labelControl29.TabIndex = 14;
            this.labelControl29.Text = "Sec";
            // 
            // tbSegRange
            // 
            this.tbSegRange.Location = new System.Drawing.Point(278, 39);
            this.tbSegRange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegRange.MenuManager = this.barManager1;
            this.tbSegRange.Name = "tbSegRange";
            this.tbSegRange.Properties.ReadOnly = true;
            this.tbSegRange.Size = new System.Drawing.Size(45, 22);
            this.tbSegRange.TabIndex = 13;
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(289, 16);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(22, 16);
            this.labelControl30.TabIndex = 12;
            this.labelControl30.Text = "Rge";
            // 
            // tbSegTownship
            // 
            this.tbSegTownship.Location = new System.Drawing.Point(212, 39);
            this.tbSegTownship.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegTownship.MenuManager = this.barManager1;
            this.tbSegTownship.Name = "tbSegTownship";
            this.tbSegTownship.Properties.ReadOnly = true;
            this.tbSegTownship.Size = new System.Drawing.Size(45, 22);
            this.tbSegTownship.TabIndex = 11;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(220, 16);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(25, 16);
            this.labelControl31.TabIndex = 10;
            this.labelControl31.Text = "Twn";
            // 
            // tbSegStand
            // 
            this.tbSegStand.Location = new System.Drawing.Point(140, 39);
            this.tbSegStand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegStand.MenuManager = this.barManager1;
            this.tbSegStand.Name = "tbSegStand";
            this.tbSegStand.Properties.ReadOnly = true;
            this.tbSegStand.Size = new System.Drawing.Size(52, 22);
            this.tbSegStand.TabIndex = 9;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(143, 16);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(46, 16);
            this.labelControl32.TabIndex = 8;
            this.labelControl32.Text = "Stand #";
            // 
            // tbSegTract
            // 
            this.tbSegTract.Location = new System.Drawing.Point(6, 39);
            this.tbSegTract.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbSegTract.MenuManager = this.barManager1;
            this.tbSegTract.Name = "tbSegTract";
            this.tbSegTract.Properties.ReadOnly = true;
            this.tbSegTract.Size = new System.Drawing.Size(117, 22);
            this.tbSegTract.TabIndex = 7;
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(45, 16);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(30, 16);
            this.labelControl33.TabIndex = 6;
            this.labelControl33.Text = "Tract";
            // 
            // tabNavigationPageStandInput
            // 
            this.tabNavigationPageStandInput.Caption = "Stand Input";
            this.tabNavigationPageStandInput.Controls.Add(this.splitContainerControl1);
            this.tabNavigationPageStandInput.Controls.Add(this.panelControl4);
            this.tabNavigationPageStandInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNavigationPageStandInput.Name = "tabNavigationPageStandInput";
            this.tabNavigationPageStandInput.Size = new System.Drawing.Size(1624, 921);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 84);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl8);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl6);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl9);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1624, 837);
            this.splitContainerControl1.SplitterPosition = 222;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl8
            // 
            this.groupControl8.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl8.AppearanceCaption.Options.UseFont = true;
            this.groupControl8.Controls.Add(this.gridControlStandInputOrig);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl8.Location = new System.Drawing.Point(0, 0);
            this.groupControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(1624, 222);
            this.groupControl8.TabIndex = 1;
            this.groupControl8.Text = "Original";
            // 
            // gridControlStandInputOrig
            // 
            this.gridControlStandInputOrig.DataSource = this.standInputBindingSource;
            this.gridControlStandInputOrig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStandInputOrig.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStandInputOrig.Location = new System.Drawing.Point(2, 26);
            this.gridControlStandInputOrig.MainView = this.gridViewStandInputOrig;
            this.gridControlStandInputOrig.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStandInputOrig.MenuManager = this.barManager1;
            this.gridControlStandInputOrig.Name = "gridControlStandInputOrig";
            this.gridControlStandInputOrig.Size = new System.Drawing.Size(1620, 194);
            this.gridControlStandInputOrig.TabIndex = 0;
            this.gridControlStandInputOrig.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStandInputOrig});
            // 
            // standInputBindingSource
            // 
            this.standInputBindingSource.DataSource = typeof(Project.DAL.StandInput);
            // 
            // gridViewStandInputOrig
            // 
            this.gridViewStandInputOrig.ColumnPanelRowHeight = 50;
            this.gridViewStandInputOrig.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colStandsId4,
            this.colSpecies2,
            this.colStatus,
            this.colOrigAge,
            this.colBirthYear,
            this.colSiteIndex1,
            this.colOrigTotalHeight,
            this.colOrigStocking,
            this.colOrigTreesPerAcre,
            this.colOrigBasalArea,
            this.colOrigD4H,
            this.colOrigLogsPerAcre,
            this.colOrigGrossCuFt,
            this.colOrigNetCuFt,
            this.colOrigGrossBdFt,
            this.colOrigNetBdFt,
            this.colOrigTotalCcf,
            this.origTotalMbf});
            this.gridViewStandInputOrig.GridControl = this.gridControlStandInputOrig;
            this.gridViewStandInputOrig.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCcf", this.colOrigTotalCcf, "n0"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalMbf", this.origTotalMbf, "n0")});
            this.gridViewStandInputOrig.Name = "gridViewStandInputOrig";
            this.gridViewStandInputOrig.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridViewStandInputOrig.OptionsCustomization.AllowFilter = false;
            this.gridViewStandInputOrig.OptionsCustomization.AllowGroup = false;
            this.gridViewStandInputOrig.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewStandInputOrig.OptionsCustomization.AllowSort = false;
            this.gridViewStandInputOrig.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewStandInputOrig.OptionsPrint.AllowMultilineHeaders = true;
            this.gridViewStandInputOrig.OptionsView.ColumnAutoWidth = false;
            this.gridViewStandInputOrig.OptionsView.ShowFooter = true;
            this.gridViewStandInputOrig.OptionsView.ShowGroupPanel = false;
            this.gridViewStandInputOrig.TopRowChanged += new System.EventHandler(this.gridViewStandInputOrig_TopRowChanged);
            this.gridViewStandInputOrig.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridViewStandInputOrig_CustomSummaryCalculate);
            this.gridViewStandInputOrig.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewStandInputOrig_FocusedRowChanged);
            this.gridViewStandInputOrig.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewStandInputOrig_CustomUnboundColumnData);
            this.gridViewStandInputOrig.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridViewStandInputOrig_PrintInitialize);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colStandsId4
            // 
            this.colStandsId4.FieldName = "StandsId";
            this.colStandsId4.Name = "colStandsId4";
            // 
            // colSpecies2
            // 
            this.colSpecies2.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpecies2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpecies2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSpecies2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpecies2.Caption = "Spc";
            this.colSpecies2.FieldName = "Species";
            this.colSpecies2.Name = "colSpecies2";
            this.colSpecies2.Visible = true;
            this.colSpecies2.VisibleIndex = 0;
            this.colSpecies2.Width = 40;
            // 
            // colStatus
            // 
            this.colStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStatus.Caption = "St";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 1;
            this.colStatus.Width = 25;
            // 
            // colOrigAge
            // 
            this.colOrigAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigAge.Caption = "Age";
            this.colOrigAge.FieldName = "OrigAge";
            this.colOrigAge.Name = "colOrigAge";
            this.colOrigAge.Visible = true;
            this.colOrigAge.VisibleIndex = 2;
            this.colOrigAge.Width = 30;
            // 
            // colBirthYear
            // 
            this.colBirthYear.AppearanceHeader.Options.UseTextOptions = true;
            this.colBirthYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBirthYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colBirthYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBirthYear.Caption = "Birth Year";
            this.colBirthYear.FieldName = "BirthYear";
            this.colBirthYear.Name = "colBirthYear";
            this.colBirthYear.Visible = true;
            this.colBirthYear.VisibleIndex = 3;
            this.colBirthYear.Width = 45;
            // 
            // colSiteIndex1
            // 
            this.colSiteIndex1.AppearanceHeader.Options.UseTextOptions = true;
            this.colSiteIndex1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSiteIndex1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colSiteIndex1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSiteIndex1.Caption = "SI";
            this.colSiteIndex1.DisplayFormat.FormatString = "{0:#,###}";
            this.colSiteIndex1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSiteIndex1.FieldName = "SiteIndex";
            this.colSiteIndex1.Name = "colSiteIndex1";
            this.colSiteIndex1.Visible = true;
            this.colSiteIndex1.VisibleIndex = 4;
            this.colSiteIndex1.Width = 30;
            // 
            // colOrigTotalHeight
            // 
            this.colOrigTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigTotalHeight.Caption = "Ht";
            this.colOrigTotalHeight.DisplayFormat.FormatString = "n0";
            this.colOrigTotalHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigTotalHeight.FieldName = "OrigTotalHeight";
            this.colOrigTotalHeight.Name = "colOrigTotalHeight";
            this.colOrigTotalHeight.Visible = true;
            this.colOrigTotalHeight.VisibleIndex = 5;
            this.colOrigTotalHeight.Width = 30;
            // 
            // colOrigStocking
            // 
            this.colOrigStocking.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigStocking.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigStocking.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigStocking.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigStocking.Caption = "Norma- lity";
            this.colOrigStocking.DisplayFormat.FormatString = "{0:#,###}";
            this.colOrigStocking.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigStocking.FieldName = "OrigStocking";
            this.colOrigStocking.Name = "colOrigStocking";
            this.colOrigStocking.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigStocking", "{0:#,###}")});
            this.colOrigStocking.Visible = true;
            this.colOrigStocking.VisibleIndex = 6;
            this.colOrigStocking.Width = 55;
            // 
            // colOrigTreesPerAcre
            // 
            this.colOrigTreesPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigTreesPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigTreesPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigTreesPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigTreesPerAcre.Caption = "Trees  /Ac";
            this.colOrigTreesPerAcre.DisplayFormat.FormatString = "{0:0.000}";
            this.colOrigTreesPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigTreesPerAcre.FieldName = "OrigTreesPerAcre";
            this.colOrigTreesPerAcre.Name = "colOrigTreesPerAcre";
            this.colOrigTreesPerAcre.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigTreesPerAcre", "{0:0.000}")});
            this.colOrigTreesPerAcre.Visible = true;
            this.colOrigTreesPerAcre.VisibleIndex = 8;
            this.colOrigTreesPerAcre.Width = 55;
            // 
            // colOrigBasalArea
            // 
            this.colOrigBasalArea.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigBasalArea.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigBasalArea.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigBasalArea.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigBasalArea.Caption = "Basal Area /Ac";
            this.colOrigBasalArea.DisplayFormat.FormatString = "{0:0.000}";
            this.colOrigBasalArea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigBasalArea.FieldName = "OrigBasalArea";
            this.colOrigBasalArea.Name = "colOrigBasalArea";
            this.colOrigBasalArea.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigBasalArea", "{0:0.000}")});
            this.colOrigBasalArea.Visible = true;
            this.colOrigBasalArea.VisibleIndex = 7;
            this.colOrigBasalArea.Width = 55;
            // 
            // colOrigD4H
            // 
            this.colOrigD4H.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigD4H.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigD4H.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigD4H.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigD4H.Caption = "Dbh";
            this.colOrigD4H.DisplayFormat.FormatString = "{0:0.0}";
            this.colOrigD4H.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigD4H.FieldName = "OrigD4H";
            this.colOrigD4H.Name = "colOrigD4H";
            this.colOrigD4H.Visible = true;
            this.colOrigD4H.VisibleIndex = 9;
            this.colOrigD4H.Width = 35;
            // 
            // colOrigLogsPerAcre
            // 
            this.colOrigLogsPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigLogsPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigLogsPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigLogsPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigLogsPerAcre.Caption = "Logs  /Ac";
            this.colOrigLogsPerAcre.DisplayFormat.FormatString = "{0:0.000}";
            this.colOrigLogsPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigLogsPerAcre.FieldName = "OrigLogsPerAcre";
            this.colOrigLogsPerAcre.Name = "colOrigLogsPerAcre";
            this.colOrigLogsPerAcre.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigLogsPerAcre", "{0:0.000}")});
            this.colOrigLogsPerAcre.Visible = true;
            this.colOrigLogsPerAcre.VisibleIndex = 10;
            this.colOrigLogsPerAcre.Width = 55;
            // 
            // colOrigGrossCuFt
            // 
            this.colOrigGrossCuFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigGrossCuFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigGrossCuFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigGrossCuFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigGrossCuFt.Caption = "Gross CuFt /Ac";
            this.colOrigGrossCuFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colOrigGrossCuFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigGrossCuFt.FieldName = "OrigGrossCuFt";
            this.colOrigGrossCuFt.Name = "colOrigGrossCuFt";
            this.colOrigGrossCuFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigGrossCuFt", "{0:#,###}")});
            this.colOrigGrossCuFt.Visible = true;
            this.colOrigGrossCuFt.VisibleIndex = 11;
            this.colOrigGrossCuFt.Width = 50;
            // 
            // colOrigNetCuFt
            // 
            this.colOrigNetCuFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigNetCuFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigNetCuFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigNetCuFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigNetCuFt.Caption = "Net CuFt /Ac";
            this.colOrigNetCuFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colOrigNetCuFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigNetCuFt.FieldName = "OrigNetCuFt";
            this.colOrigNetCuFt.Name = "colOrigNetCuFt";
            this.colOrigNetCuFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigNetCuFt", "{0:#,###}")});
            this.colOrigNetCuFt.Visible = true;
            this.colOrigNetCuFt.VisibleIndex = 12;
            this.colOrigNetCuFt.Width = 50;
            // 
            // colOrigGrossBdFt
            // 
            this.colOrigGrossBdFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigGrossBdFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigGrossBdFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigGrossBdFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigGrossBdFt.Caption = "Gross BdFt /Ac";
            this.colOrigGrossBdFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colOrigGrossBdFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigGrossBdFt.FieldName = "OrigGrossBdFt";
            this.colOrigGrossBdFt.Name = "colOrigGrossBdFt";
            this.colOrigGrossBdFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigGrossBdFt", "{0:#,###}")});
            this.colOrigGrossBdFt.Visible = true;
            this.colOrigGrossBdFt.VisibleIndex = 13;
            this.colOrigGrossBdFt.Width = 50;
            // 
            // colOrigNetBdFt
            // 
            this.colOrigNetBdFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigNetBdFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigNetBdFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigNetBdFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigNetBdFt.Caption = "Net BdFt /Ac";
            this.colOrigNetBdFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colOrigNetBdFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigNetBdFt.FieldName = "OrigNetBdFt";
            this.colOrigNetBdFt.Name = "colOrigNetBdFt";
            this.colOrigNetBdFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigNetBdFt", "{0:#,###}")});
            this.colOrigNetBdFt.Visible = true;
            this.colOrigNetBdFt.VisibleIndex = 14;
            this.colOrigNetBdFt.Width = 50;
            // 
            // colOrigTotalCcf
            // 
            this.colOrigTotalCcf.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrigTotalCcf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrigTotalCcf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colOrigTotalCcf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrigTotalCcf.Caption = "Total Ccf";
            this.colOrigTotalCcf.DisplayFormat.FormatString = "n0";
            this.colOrigTotalCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOrigTotalCcf.FieldName = "OrigTotalCcf";
            this.colOrigTotalCcf.Name = "colOrigTotalCcf";
            this.colOrigTotalCcf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigTotalCcf", "{0:#,###}")});
            this.colOrigTotalCcf.Visible = true;
            this.colOrigTotalCcf.VisibleIndex = 15;
            this.colOrigTotalCcf.Width = 45;
            // 
            // origTotalMbf
            // 
            this.origTotalMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.origTotalMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.origTotalMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.origTotalMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.origTotalMbf.Caption = "Total Mbf";
            this.origTotalMbf.DisplayFormat.FormatString = "n0";
            this.origTotalMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.origTotalMbf.FieldName = "OrigTotalMbf";
            this.origTotalMbf.Name = "origTotalMbf";
            this.origTotalMbf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrigTotalMbf", "{0:#,###}")});
            this.origTotalMbf.Visible = true;
            this.origTotalMbf.VisibleIndex = 16;
            this.origTotalMbf.Width = 45;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.labelControl109);
            this.panelControl6.Controls.Add(this.btnStandInputGrow);
            this.panelControl6.Controls.Add(this.tbStandInputGrownToDate);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(0, 563);
            this.panelControl6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1624, 44);
            this.panelControl6.TabIndex = 3;
            // 
            // labelControl109
            // 
            this.labelControl109.Location = new System.Drawing.Point(6, 15);
            this.labelControl109.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(98, 16);
            this.labelControl109.TabIndex = 2;
            this.labelControl109.Text = "Stand Grown To:";
            // 
            // btnStandInputGrow
            // 
            this.btnStandInputGrow.Location = new System.Drawing.Point(266, 11);
            this.btnStandInputGrow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStandInputGrow.Name = "btnStandInputGrow";
            this.btnStandInputGrow.Size = new System.Drawing.Size(75, 23);
            this.btnStandInputGrow.TabIndex = 1;
            this.btnStandInputGrow.Text = "Grow";
            this.btnStandInputGrow.Click += new System.EventHandler(this.btnStandInputGrow_Click);
            // 
            // tbStandInputGrownToDate
            // 
            this.tbStandInputGrownToDate.Location = new System.Drawing.Point(120, 12);
            this.tbStandInputGrownToDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbStandInputGrownToDate.MenuManager = this.barManager1;
            this.tbStandInputGrownToDate.Name = "tbStandInputGrownToDate";
            this.tbStandInputGrownToDate.Size = new System.Drawing.Size(125, 22);
            this.tbStandInputGrownToDate.TabIndex = 0;
            this.tbStandInputGrownToDate.Validating += new System.ComponentModel.CancelEventHandler(this.tbStandInputGrownToDate_Validating);
            // 
            // groupControl9
            // 
            this.groupControl9.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl9.AppearanceCaption.Options.UseFont = true;
            this.groupControl9.Controls.Add(this.gridControlStandInputGrown);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl9.Location = new System.Drawing.Point(0, 0);
            this.groupControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(1624, 607);
            this.groupControl9.TabIndex = 2;
            this.groupControl9.Text = "Updated";
            // 
            // gridControlStandInputGrown
            // 
            this.gridControlStandInputGrown.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlStandInputGrown.DataSource = this.standInputBindingSource;
            this.gridControlStandInputGrown.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStandInputGrown.Location = new System.Drawing.Point(0, 52);
            this.gridControlStandInputGrown.MainView = this.gridViewStandInputGrown;
            this.gridControlStandInputGrown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlStandInputGrown.MenuManager = this.barManager1;
            this.gridControlStandInputGrown.Name = "gridControlStandInputGrown";
            this.gridControlStandInputGrown.Size = new System.Drawing.Size(1619, 516);
            this.gridControlStandInputGrown.TabIndex = 1;
            this.gridControlStandInputGrown.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStandInputGrown});
            // 
            // gridViewStandInputGrown
            // 
            this.gridViewStandInputGrown.ColumnPanelRowHeight = 50;
            this.gridViewStandInputGrown.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUpdtId,
            this.colUpdtStandsId,
            this.colUpdtSpecies,
            this.colUpdtStatus,
            this.colUpdtSiteIndex,
            this.colUpdtAge,
            this.colUpdtDbh,
            this.colUpdtStocking,
            this.colUpdtTreesPerAcre,
            this.colUpdtLogsPerAcre,
            this.colUpdtBasalArea,
            this.colUpdtGrossCuFt,
            this.colUpdtNetCuFt,
            this.colUpdtGrossBdFt,
            this.colUpdtNetBdFt,
            this.colUpdtTotalHeight,
            this.colUpdtBirthYear,
            this.colUpdtTotalCcf,
            this.colUpdtTotalMbf});
            this.gridViewStandInputGrown.GridControl = this.gridControlStandInputGrown;
            this.gridViewStandInputGrown.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCcf", this.colUpdtTotalCcf, "n0"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalMbf", this.colUpdtTotalMbf, "n0")});
            this.gridViewStandInputGrown.Name = "gridViewStandInputGrown";
            this.gridViewStandInputGrown.OptionsCustomization.AllowFilter = false;
            this.gridViewStandInputGrown.OptionsCustomization.AllowGroup = false;
            this.gridViewStandInputGrown.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewStandInputGrown.OptionsCustomization.AllowSort = false;
            this.gridViewStandInputGrown.OptionsPrint.AllowMultilineHeaders = true;
            this.gridViewStandInputGrown.OptionsView.ColumnAutoWidth = false;
            this.gridViewStandInputGrown.OptionsView.ShowFooter = true;
            this.gridViewStandInputGrown.OptionsView.ShowGroupPanel = false;
            this.gridViewStandInputGrown.TopRowChanged += new System.EventHandler(this.gridViewStandInputGrown_TopRowChanged);
            this.gridViewStandInputGrown.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewStandInputGrown_FocusedRowChanged);
            this.gridViewStandInputGrown.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewStandInputGrown_CustomUnboundColumnData);
            this.gridViewStandInputGrown.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridViewStandInputGrown_PrintInitialize);
            // 
            // colUpdtId
            // 
            this.colUpdtId.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtId.FieldName = "Id";
            this.colUpdtId.Name = "colUpdtId";
            // 
            // colUpdtStandsId
            // 
            this.colUpdtStandsId.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtStandsId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtStandsId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtStandsId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtStandsId.FieldName = "StandsId";
            this.colUpdtStandsId.Name = "colUpdtStandsId";
            // 
            // colUpdtSpecies
            // 
            this.colUpdtSpecies.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtSpecies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtSpecies.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtSpecies.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtSpecies.Caption = "Spc";
            this.colUpdtSpecies.FieldName = "Species";
            this.colUpdtSpecies.Name = "colUpdtSpecies";
            this.colUpdtSpecies.Visible = true;
            this.colUpdtSpecies.VisibleIndex = 0;
            this.colUpdtSpecies.Width = 40;
            // 
            // colUpdtStatus
            // 
            this.colUpdtStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtStatus.Caption = "St";
            this.colUpdtStatus.FieldName = "Status";
            this.colUpdtStatus.Name = "colUpdtStatus";
            this.colUpdtStatus.Visible = true;
            this.colUpdtStatus.VisibleIndex = 1;
            this.colUpdtStatus.Width = 25;
            // 
            // colUpdtSiteIndex
            // 
            this.colUpdtSiteIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtSiteIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtSiteIndex.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtSiteIndex.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtSiteIndex.Caption = "SI";
            this.colUpdtSiteIndex.DisplayFormat.FormatString = "{0:#}";
            this.colUpdtSiteIndex.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtSiteIndex.FieldName = "SiteIndex";
            this.colUpdtSiteIndex.Name = "colUpdtSiteIndex";
            this.colUpdtSiteIndex.Visible = true;
            this.colUpdtSiteIndex.VisibleIndex = 4;
            this.colUpdtSiteIndex.Width = 30;
            // 
            // colUpdtAge
            // 
            this.colUpdtAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtAge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtAge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtAge.Caption = "Age";
            this.colUpdtAge.DisplayFormat.FormatString = "{0:#}";
            this.colUpdtAge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtAge.FieldName = "Age";
            this.colUpdtAge.Name = "colUpdtAge";
            this.colUpdtAge.Visible = true;
            this.colUpdtAge.VisibleIndex = 2;
            this.colUpdtAge.Width = 30;
            // 
            // colUpdtDbh
            // 
            this.colUpdtDbh.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtDbh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtDbh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtDbh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtDbh.Caption = "Dbh";
            this.colUpdtDbh.DisplayFormat.FormatString = "{0:0.0}";
            this.colUpdtDbh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtDbh.FieldName = "D4H";
            this.colUpdtDbh.Name = "colUpdtDbh";
            this.colUpdtDbh.Visible = true;
            this.colUpdtDbh.VisibleIndex = 9;
            this.colUpdtDbh.Width = 35;
            // 
            // colUpdtStocking
            // 
            this.colUpdtStocking.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtStocking.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtStocking.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtStocking.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtStocking.Caption = "Norma- lity";
            this.colUpdtStocking.DisplayFormat.FormatString = "{0:#}";
            this.colUpdtStocking.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtStocking.FieldName = "Stocking";
            this.colUpdtStocking.Name = "colUpdtStocking";
            this.colUpdtStocking.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Stocking", "{0:#}")});
            this.colUpdtStocking.Visible = true;
            this.colUpdtStocking.VisibleIndex = 6;
            this.colUpdtStocking.Width = 55;
            // 
            // colUpdtTreesPerAcre
            // 
            this.colUpdtTreesPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtTreesPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtTreesPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtTreesPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtTreesPerAcre.Caption = "Trees /Ac";
            this.colUpdtTreesPerAcre.DisplayFormat.FormatString = "{0:0.000}";
            this.colUpdtTreesPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtTreesPerAcre.FieldName = "TreesPerAcre";
            this.colUpdtTreesPerAcre.Name = "colUpdtTreesPerAcre";
            this.colUpdtTreesPerAcre.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TreesPerAcre", "{0:0.000}")});
            this.colUpdtTreesPerAcre.Visible = true;
            this.colUpdtTreesPerAcre.VisibleIndex = 8;
            this.colUpdtTreesPerAcre.Width = 55;
            // 
            // colUpdtLogsPerAcre
            // 
            this.colUpdtLogsPerAcre.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtLogsPerAcre.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtLogsPerAcre.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtLogsPerAcre.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtLogsPerAcre.Caption = "Logs /Ac";
            this.colUpdtLogsPerAcre.DisplayFormat.FormatString = "{0:0.000}";
            this.colUpdtLogsPerAcre.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtLogsPerAcre.FieldName = "LogsPerAcre";
            this.colUpdtLogsPerAcre.Name = "colUpdtLogsPerAcre";
            this.colUpdtLogsPerAcre.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LogsPerAcre", "{0:0.000}")});
            this.colUpdtLogsPerAcre.Visible = true;
            this.colUpdtLogsPerAcre.VisibleIndex = 10;
            this.colUpdtLogsPerAcre.Width = 55;
            // 
            // colUpdtBasalArea
            // 
            this.colUpdtBasalArea.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtBasalArea.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtBasalArea.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtBasalArea.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtBasalArea.Caption = "Basal Area /Ac";
            this.colUpdtBasalArea.DisplayFormat.FormatString = "{0:0.000}";
            this.colUpdtBasalArea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtBasalArea.FieldName = "BasalArea";
            this.colUpdtBasalArea.Name = "colUpdtBasalArea";
            this.colUpdtBasalArea.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BasalArea", "{0:0.000}")});
            this.colUpdtBasalArea.Visible = true;
            this.colUpdtBasalArea.VisibleIndex = 7;
            this.colUpdtBasalArea.Width = 55;
            // 
            // colUpdtGrossCuFt
            // 
            this.colUpdtGrossCuFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtGrossCuFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtGrossCuFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtGrossCuFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtGrossCuFt.Caption = "Gross CuFt /Ac";
            this.colUpdtGrossCuFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colUpdtGrossCuFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtGrossCuFt.FieldName = "GrossCuFt";
            this.colUpdtGrossCuFt.Name = "colUpdtGrossCuFt";
            this.colUpdtGrossCuFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "GrossCuFt", "{0:#,###}")});
            this.colUpdtGrossCuFt.Visible = true;
            this.colUpdtGrossCuFt.VisibleIndex = 11;
            this.colUpdtGrossCuFt.Width = 50;
            // 
            // colUpdtNetCuFt
            // 
            this.colUpdtNetCuFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtNetCuFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtNetCuFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtNetCuFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtNetCuFt.Caption = "Net CuFt /Ac";
            this.colUpdtNetCuFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colUpdtNetCuFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtNetCuFt.FieldName = "NetCuFt";
            this.colUpdtNetCuFt.Name = "colUpdtNetCuFt";
            this.colUpdtNetCuFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NetCuFt", "{0:#,###}")});
            this.colUpdtNetCuFt.Visible = true;
            this.colUpdtNetCuFt.VisibleIndex = 12;
            this.colUpdtNetCuFt.Width = 50;
            // 
            // colUpdtGrossBdFt
            // 
            this.colUpdtGrossBdFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtGrossBdFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtGrossBdFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtGrossBdFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtGrossBdFt.Caption = "Gross BdFt /Ac";
            this.colUpdtGrossBdFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colUpdtGrossBdFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtGrossBdFt.FieldName = "GrossBdFt";
            this.colUpdtGrossBdFt.Name = "colUpdtGrossBdFt";
            this.colUpdtGrossBdFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "GrossBdFt", "{0:#,###}")});
            this.colUpdtGrossBdFt.Visible = true;
            this.colUpdtGrossBdFt.VisibleIndex = 13;
            this.colUpdtGrossBdFt.Width = 50;
            // 
            // colUpdtNetBdFt
            // 
            this.colUpdtNetBdFt.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtNetBdFt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtNetBdFt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtNetBdFt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtNetBdFt.Caption = "Net BdFt /Ac";
            this.colUpdtNetBdFt.DisplayFormat.FormatString = "{0:#,###}";
            this.colUpdtNetBdFt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtNetBdFt.FieldName = "NetBdFt";
            this.colUpdtNetBdFt.Name = "colUpdtNetBdFt";
            this.colUpdtNetBdFt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NetBdFt", "{0:#,###}")});
            this.colUpdtNetBdFt.Visible = true;
            this.colUpdtNetBdFt.VisibleIndex = 14;
            this.colUpdtNetBdFt.Width = 50;
            // 
            // colUpdtTotalHeight
            // 
            this.colUpdtTotalHeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtTotalHeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtTotalHeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtTotalHeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtTotalHeight.Caption = "Ht";
            this.colUpdtTotalHeight.DisplayFormat.FormatString = "{0:#}";
            this.colUpdtTotalHeight.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtTotalHeight.FieldName = "TotalHeight";
            this.colUpdtTotalHeight.Name = "colUpdtTotalHeight";
            this.colUpdtTotalHeight.Visible = true;
            this.colUpdtTotalHeight.VisibleIndex = 5;
            this.colUpdtTotalHeight.Width = 30;
            // 
            // colUpdtBirthYear
            // 
            this.colUpdtBirthYear.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtBirthYear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtBirthYear.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtBirthYear.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtBirthYear.FieldName = "BirthYear";
            this.colUpdtBirthYear.Name = "colUpdtBirthYear";
            this.colUpdtBirthYear.Visible = true;
            this.colUpdtBirthYear.VisibleIndex = 3;
            this.colUpdtBirthYear.Width = 45;
            // 
            // colUpdtTotalCcf
            // 
            this.colUpdtTotalCcf.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtTotalCcf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtTotalCcf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtTotalCcf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtTotalCcf.Caption = "Total Ccf";
            this.colUpdtTotalCcf.DisplayFormat.FormatString = "n0";
            this.colUpdtTotalCcf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtTotalCcf.FieldName = "TotalCcf";
            this.colUpdtTotalCcf.Name = "colUpdtTotalCcf";
            this.colUpdtTotalCcf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCcf", "{0:#,###}")});
            this.colUpdtTotalCcf.Visible = true;
            this.colUpdtTotalCcf.VisibleIndex = 15;
            this.colUpdtTotalCcf.Width = 45;
            // 
            // colUpdtTotalMbf
            // 
            this.colUpdtTotalMbf.AppearanceHeader.Options.UseTextOptions = true;
            this.colUpdtTotalMbf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUpdtTotalMbf.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.colUpdtTotalMbf.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUpdtTotalMbf.Caption = "Total Mbf";
            this.colUpdtTotalMbf.DisplayFormat.FormatString = "n0";
            this.colUpdtTotalMbf.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUpdtTotalMbf.FieldName = "TotalMbf";
            this.colUpdtTotalMbf.Name = "colUpdtTotalMbf";
            this.colUpdtTotalMbf.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalMbf", "{0:#,###}")});
            this.colUpdtTotalMbf.Visible = true;
            this.colUpdtTotalMbf.VisibleIndex = 16;
            this.colUpdtTotalMbf.Width = 45;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.tbStandInputCostTable);
            this.panelControl4.Controls.Add(this.labelControl34);
            this.panelControl4.Controls.Add(this.tbStandInputPriceTable);
            this.panelControl4.Controls.Add(this.labelControl36);
            this.panelControl4.Controls.Add(this.tbStandInputGradeTable);
            this.panelControl4.Controls.Add(this.labelControl37);
            this.panelControl4.Controls.Add(this.tbStandInputSortTable);
            this.panelControl4.Controls.Add(this.labelControl38);
            this.panelControl4.Controls.Add(this.tbStandInputSpeciesTable);
            this.panelControl4.Controls.Add(this.labelControl39);
            this.panelControl4.Controls.Add(this.tbStandInputNetAcres);
            this.panelControl4.Controls.Add(this.labelControl35);
            this.panelControl4.Controls.Add(this.tbStandInputSection);
            this.panelControl4.Controls.Add(this.labelControl40);
            this.panelControl4.Controls.Add(this.tbStandInputRange);
            this.panelControl4.Controls.Add(this.labelControl41);
            this.panelControl4.Controls.Add(this.tbStandInputTownship);
            this.panelControl4.Controls.Add(this.labelControl42);
            this.panelControl4.Controls.Add(this.tbStandInputStand);
            this.panelControl4.Controls.Add(this.labelControl43);
            this.panelControl4.Controls.Add(this.tbStandInputTract);
            this.panelControl4.Controls.Add(this.labelControl44);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1624, 84);
            this.panelControl4.TabIndex = 1;
            // 
            // tbStandInputCostTable
            // 
            this.tbStandInputCostTable.Location = new System.Drawing.Point(1239, 39);
            this.tbStandInputCostTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputCostTable.MenuManager = this.barManager1;
            this.tbStandInputCostTable.Name = "tbStandInputCostTable";
            this.tbStandInputCostTable.Properties.ReadOnly = true;
            this.tbStandInputCostTable.Size = new System.Drawing.Size(142, 22);
            this.tbStandInputCostTable.TabIndex = 37;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(1283, 16);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(61, 16);
            this.labelControl34.TabIndex = 36;
            this.labelControl34.Text = "Cost Table";
            // 
            // tbStandInputPriceTable
            // 
            this.tbStandInputPriceTable.Location = new System.Drawing.Point(1053, 39);
            this.tbStandInputPriceTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputPriceTable.MenuManager = this.barManager1;
            this.tbStandInputPriceTable.Name = "tbStandInputPriceTable";
            this.tbStandInputPriceTable.Properties.ReadOnly = true;
            this.tbStandInputPriceTable.Size = new System.Drawing.Size(142, 22);
            this.tbStandInputPriceTable.TabIndex = 35;
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(1091, 16);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(64, 16);
            this.labelControl36.TabIndex = 34;
            this.labelControl36.Text = "Price Table";
            // 
            // tbStandInputGradeTable
            // 
            this.tbStandInputGradeTable.Location = new System.Drawing.Point(864, 39);
            this.tbStandInputGradeTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputGradeTable.MenuManager = this.barManager1;
            this.tbStandInputGradeTable.Name = "tbStandInputGradeTable";
            this.tbStandInputGradeTable.Properties.ReadOnly = true;
            this.tbStandInputGradeTable.Size = new System.Drawing.Size(142, 22);
            this.tbStandInputGradeTable.TabIndex = 33;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(903, 16);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(70, 16);
            this.labelControl37.TabIndex = 32;
            this.labelControl37.Text = "Grade Table";
            // 
            // tbStandInputSortTable
            // 
            this.tbStandInputSortTable.Location = new System.Drawing.Point(675, 39);
            this.tbStandInputSortTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputSortTable.MenuManager = this.barManager1;
            this.tbStandInputSortTable.Name = "tbStandInputSortTable";
            this.tbStandInputSortTable.Properties.ReadOnly = true;
            this.tbStandInputSortTable.Size = new System.Drawing.Size(142, 22);
            this.tbStandInputSortTable.TabIndex = 31;
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(717, 16);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(60, 16);
            this.labelControl38.TabIndex = 30;
            this.labelControl38.Text = "Sort Table";
            // 
            // tbStandInputSpeciesTable
            // 
            this.tbStandInputSpeciesTable.Location = new System.Drawing.Point(489, 39);
            this.tbStandInputSpeciesTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputSpeciesTable.MenuManager = this.barManager1;
            this.tbStandInputSpeciesTable.Name = "tbStandInputSpeciesTable";
            this.tbStandInputSpeciesTable.Properties.ReadOnly = true;
            this.tbStandInputSpeciesTable.Size = new System.Drawing.Size(142, 22);
            this.tbStandInputSpeciesTable.TabIndex = 29;
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(527, 16);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(80, 16);
            this.labelControl39.TabIndex = 28;
            this.labelControl39.Text = "Species Table";
            // 
            // tbStandInputNetAcres
            // 
            this.tbStandInputNetAcres.Location = new System.Drawing.Point(399, 39);
            this.tbStandInputNetAcres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputNetAcres.MenuManager = this.barManager1;
            this.tbStandInputNetAcres.Name = "tbStandInputNetAcres";
            this.tbStandInputNetAcres.Properties.ReadOnly = true;
            this.tbStandInputNetAcres.Size = new System.Drawing.Size(72, 22);
            this.tbStandInputNetAcres.TabIndex = 25;
            // 
            // labelControl35
            // 
            this.labelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl35.Location = new System.Drawing.Point(407, 16);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(55, 16);
            this.labelControl35.TabIndex = 24;
            this.labelControl35.Text = "Net Acres";
            // 
            // tbStandInputSection
            // 
            this.tbStandInputSection.Location = new System.Drawing.Point(345, 39);
            this.tbStandInputSection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputSection.MenuManager = this.barManager1;
            this.tbStandInputSection.Name = "tbStandInputSection";
            this.tbStandInputSection.Properties.ReadOnly = true;
            this.tbStandInputSection.Size = new System.Drawing.Size(34, 22);
            this.tbStandInputSection.TabIndex = 15;
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(352, 16);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(21, 16);
            this.labelControl40.TabIndex = 14;
            this.labelControl40.Text = "Sec";
            // 
            // tbStandInputRange
            // 
            this.tbStandInputRange.Location = new System.Drawing.Point(278, 39);
            this.tbStandInputRange.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputRange.MenuManager = this.barManager1;
            this.tbStandInputRange.Name = "tbStandInputRange";
            this.tbStandInputRange.Properties.ReadOnly = true;
            this.tbStandInputRange.Size = new System.Drawing.Size(45, 22);
            this.tbStandInputRange.TabIndex = 13;
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(289, 16);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(22, 16);
            this.labelControl41.TabIndex = 12;
            this.labelControl41.Text = "Rge";
            // 
            // tbStandInputTownship
            // 
            this.tbStandInputTownship.Location = new System.Drawing.Point(212, 39);
            this.tbStandInputTownship.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputTownship.MenuManager = this.barManager1;
            this.tbStandInputTownship.Name = "tbStandInputTownship";
            this.tbStandInputTownship.Properties.ReadOnly = true;
            this.tbStandInputTownship.Size = new System.Drawing.Size(45, 22);
            this.tbStandInputTownship.TabIndex = 11;
            // 
            // labelControl42
            // 
            this.labelControl42.Location = new System.Drawing.Point(220, 16);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(25, 16);
            this.labelControl42.TabIndex = 10;
            this.labelControl42.Text = "Twn";
            // 
            // tbStandInputStand
            // 
            this.tbStandInputStand.Location = new System.Drawing.Point(140, 39);
            this.tbStandInputStand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputStand.MenuManager = this.barManager1;
            this.tbStandInputStand.Name = "tbStandInputStand";
            this.tbStandInputStand.Properties.ReadOnly = true;
            this.tbStandInputStand.Size = new System.Drawing.Size(52, 22);
            this.tbStandInputStand.TabIndex = 9;
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(143, 16);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(46, 16);
            this.labelControl43.TabIndex = 8;
            this.labelControl43.Text = "Stand #";
            // 
            // tbStandInputTract
            // 
            this.tbStandInputTract.Location = new System.Drawing.Point(6, 39);
            this.tbStandInputTract.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbStandInputTract.MenuManager = this.barManager1;
            this.tbStandInputTract.Name = "tbStandInputTract";
            this.tbStandInputTract.Properties.ReadOnly = true;
            this.tbStandInputTract.Size = new System.Drawing.Size(117, 22);
            this.tbStandInputTract.TabIndex = 7;
            // 
            // labelControl44
            // 
            this.labelControl44.Location = new System.Drawing.Point(45, 16);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(30, 16);
            this.labelControl44.TabIndex = 6;
            this.labelControl44.Text = "Tract";
            // 
            // sourceBindingSource
            // 
            this.sourceBindingSource.DataSource = typeof(Projects.DAL.Source);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Blue";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "attach.png");
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = global::SuperACE.Properties.Settings.Default.ClientSize;
            this.Controls.Add(this.tabPane1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::SuperACE.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::SuperACE.Properties.Settings.Default, "ClientSize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = global::SuperACE.Properties.Settings.Default.Location;
            this.LookAndFeel.SkinName = "Office 2007 Blue";
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "SuperACE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPageHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standsViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStands)).EndInit();
            this.tabNavigationPageStandMaster.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standYardingSystemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxYarding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditHarvest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.harvestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standPermPlotBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standAgeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standHaulingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.haulingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSiteIndex1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speciesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpecies43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripExpansion1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripWidth1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStripInterval1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaBlowup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaSide11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotRadius1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaPlotAcres1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAreaCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBafLimit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBaf1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAutoSegmentLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrowthModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonStockedTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonStockedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNonTimberedTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nonTimberedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHarvestTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlots.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGrownToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInputDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCounty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateCountyBindingSource)).EndInit();
            this.tabNavigationPagePlotLocation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlots)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewPlots)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBafInAt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAspect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHabitat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.habitatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSpecies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEnvironmental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.environmentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlotsTract.Properties)).EndInit();
            this.tabNavigationPageTreeInput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridViewTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFormFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreesTract.Properties)).EndInit();
            this.tabNavigationPageTreeEdit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSegments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.segmentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSegments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSegTract.Properties)).EndInit();
            this.tabNavigationPageStandInput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStandInputOrig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standInputBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStandInputOrig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputGrownToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStandInputGrown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStandInputGrown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputCostTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputPriceTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputGradeTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSortTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSpeciesTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputNetAcres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputTownship.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputStand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStandInputTract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarSubItem barSubItemFile;
        private DevExpress.XtraBars.BarSubItem barSubItemProject;
        private DevExpress.XtraBars.BarSubItem barSubItemStands;
        private DevExpress.XtraBars.BarSubItem barSubItemOwnership;
        private DevExpress.XtraBars.BarSubItem barSubItemReports;
        private DevExpress.XtraBars.BarSubItem barSubItemTables;
        private DevExpress.XtraBars.BarSubItem barSubItemDefaults;
        private DevExpress.XtraBars.BarSubItem barSubItemHelp;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageHome;
        private DevExpress.XtraGrid.GridControl gridControlStands;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStands;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageStandMaster;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPagePlotLocation;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageTreeInput;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageTreeEdit;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPageStandInput;
        private System.Windows.Forms.BindingSource standsViewBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectsId;
        private DevExpress.XtraGrid.Columns.GridColumn colStandName;
        private DevExpress.XtraGrid.Columns.GridColumn colTractName;
        private DevExpress.XtraGrid.Columns.GridColumn colTownship;
        private DevExpress.XtraGrid.Columns.GridColumn colRange;
        private DevExpress.XtraGrid.Columns.GridColumn colSection;
        private DevExpress.XtraGrid.Columns.GridColumn colNetGeographicAcres;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOfStandData;
        private DevExpress.XtraGrid.Columns.GridColumn colBasalAreaPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTreesPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTonsPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colMbfPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colCcfPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colQmDbh;
        private DevExpress.XtraGrid.Columns.GridColumn colMajorAge;
        private DevExpress.XtraGrid.Columns.GridColumn colTractGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colMajorSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colGrownToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHarvest;
        private DevExpress.XtraGrid.Columns.GridColumn colSource;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraGrid.Columns.GridColumn colCounty;
        private DevExpress.XtraGrid.Columns.GridColumn colLandClass;
        private DevExpress.XtraGrid.Columns.GridColumn colAttachments;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarSubItem barSubItemFileExit;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemFirst;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrev;
        private DevExpress.XtraBars.BarButtonItem barButtonItemNext;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLast;
        private DevExpress.XtraGrid.GridControl gridControlPlots;
        private System.Windows.Forms.BindingSource plotsBindingSource;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit tbPlotsCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit tbPlotsNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit tbPlotsPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit tbPlotsGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit tbPlotsSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit tbPlotsSpeciesTable;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit tbPlotsSection;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit tbPlotsRange;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit tbPlotsTownship;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit tbPlotsStand;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit tbPlotsTract;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit tbTreesNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit tbTreesSection;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit tbTreesRange;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit tbTreesTownship;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit tbTreesStand;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit tbTreesTract;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit tbSegNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit tbSegSection;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.TextEdit tbSegRange;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit tbSegTownship;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit tbSegStand;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit tbSegTract;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit tbStandInputNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit tbStandInputSection;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit tbStandInputRange;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit tbStandInputTownship;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit tbStandInputStand;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit tbStandInputTract;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraGrid.GridControl gridControlTrees;
        private System.Windows.Forms.BindingSource treesBindingSource;
        private DevExpress.XtraEditors.TextEdit tbTreesCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit tbTreesPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit tbTreesGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit tbTreesSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit tbTreesSpeciesTable;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit tbSegCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit tbSegPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit tbSegGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit tbSegSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit tbSegSpeciesTable;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit tbStandInputCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit tbStandInputPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit tbStandInputGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit tbStandInputSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit tbStandInputSpeciesTable;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridViewTrees;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreesId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotsId1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotFactorInput;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgeCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpeciesAbbreviation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeStatusDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDbh;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFormPoint;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colReportedFormFactor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTopDiameterFractionCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBoleHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownPositionDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCrownRatioDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVigorDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDamageDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUserDefinedDisplayCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSort12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrade12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLength12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtLD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtDD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtLD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCuFtDD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGroupPlot;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGroupTree;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreeType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAge;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBdFtPD12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHeight;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCalcTotalHtUsedYN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPFValue;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridViewPlots;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotsId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStandsId1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCruiserID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAttachments1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUserPlotNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPlotDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCruiser;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBafInAt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCruFlag;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAddFlag;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRefFlag;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDWFlag;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSlope;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAspect;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHabitat;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpecies;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEnvironmental;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colX;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colZ;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colBasalAreaAcre;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTreesPerAcre1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetCfAcre;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNetBfAcre;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNotes;
        private Grid.CustomGridControl gridControlSegments;
        private System.Windows.Forms.BindingSource segmentsBindingSource;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemProjectEditProject;
        private DevExpress.XtraBars.BarStaticItem barStaticItemProjectNewProject;
        private DevExpress.XtraBars.BarStaticItem barStaticItemProjectSelectProject;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFileExit;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItem7;
        private DevExpress.XtraBars.BarStaticItem barStaticItem8;
        private DevExpress.XtraBars.BarStaticItem barStaticItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSpeciesTable;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSortTable;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGradeTable;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPriceTable;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCostTable;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.TextEdit tbAutoSegmentLength;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.ComboBoxEdit cbGrowthModel;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.ComboBoxEdit cbCostTable;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.ComboBoxEdit cbPriceTable;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.ComboBoxEdit cbGradeTable;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.ComboBoxEdit cbSortTable;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.ComboBoxEdit cbSpeciesTable;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btnGrow;
        private DevExpress.XtraEditors.TextEdit tbGrownToDate;
        private DevExpress.XtraEditors.TextEdit tbInputDate;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.TextEdit tbNetAcres;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.TextEdit tbSection;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit tbRange;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit tbTownship;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit tbStand;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.TextEdit tbTract;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit tbProject;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup5;
        private DevExpress.XtraEditors.TextEdit tbAreaSide52;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup4;
        private DevExpress.XtraEditors.TextEdit tbAreaSide42;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup3;
        private DevExpress.XtraEditors.TextEdit tbAreaSide32;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup2;
        private DevExpress.XtraEditors.TextEdit tbAreaSide22;
        private DevExpress.XtraEditors.TextEdit tbAreaBlowup1;
        private DevExpress.XtraEditors.TextEdit tbAreaSide12;
        private DevExpress.XtraEditors.TextEdit tbAreaSide51;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius5;
        private DevExpress.XtraEditors.TextEdit tbAreaSide41;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius4;
        private DevExpress.XtraEditors.TextEdit tbAreaSide31;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius3;
        private DevExpress.XtraEditors.TextEdit tbAreaSide21;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius2;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotRadius1;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres5;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode5;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres4;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode4;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres3;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode3;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres2;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode2;
        private DevExpress.XtraEditors.TextEdit tbAreaPlotAcres1;
        private DevExpress.XtraEditors.ComboBoxEdit tbAreaCode1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.TextEdit tbBafLimit5;
        private DevExpress.XtraEditors.TextEdit tbBaf5;
        private DevExpress.XtraEditors.TextEdit tbBafLimit4;
        private DevExpress.XtraEditors.TextEdit tbBaf4;
        private DevExpress.XtraEditors.TextEdit tbBafLimit3;
        private DevExpress.XtraEditors.TextEdit tbBaf3;
        private DevExpress.XtraEditors.TextEdit tbBafLimit2;
        private DevExpress.XtraEditors.TextEdit tbBaf2;
        private DevExpress.XtraEditors.TextEdit tbBafLimit1;
        private DevExpress.XtraEditors.TextEdit tbBaf1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion5;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion4;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion3;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion2;
        private DevExpress.XtraEditors.TextEdit tbStripExpansion1;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.TextEdit tbStripWidth5;
        private DevExpress.XtraEditors.TextEdit tbStripInterval5;
        private DevExpress.XtraEditors.TextEdit tbStripWidth4;
        private DevExpress.XtraEditors.TextEdit tbStripInterval4;
        private DevExpress.XtraEditors.TextEdit tbStripWidth3;
        private DevExpress.XtraEditors.TextEdit tbStripInterval3;
        private DevExpress.XtraEditors.TextEdit tbStripWidth2;
        private DevExpress.XtraEditors.TextEdit tbStripInterval2;
        private DevExpress.XtraEditors.TextEdit tbStripWidth1;
        private DevExpress.XtraEditors.TextEdit tbStripInterval1;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex4;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex3;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex2;
        private DevExpress.XtraEditors.TextEdit tbSiteIndex1;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource haulingBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControlStandInputOrig;
        private System.Windows.Forms.BindingSource standInputBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStandInputOrig;
        private DevExpress.XtraGrid.GridControl gridControlStandInputGrown;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStandInputGrown;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtId;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtStandsId;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtSiteIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtAge;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtDbh;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtStocking;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtTreesPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtLogsPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtBasalArea;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtGrossCuFt;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtNetCuFt;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtGrossBdFt;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtNetBdFt;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtTotalHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtBirthYear;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem btnLimitingDistance;
        private DevExpress.XtraBars.BarButtonItem btnTreeMeasurementsRelaskop;
        private DevExpress.XtraBars.BarButtonItem btnTreeMeasurementsRD100;
        private DevExpress.XtraBars.BarButtonItem btnTotalHeight;
        private DevExpress.XtraBars.BarButtonItem btnFormFactorWithoutDistance;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarButtonItem barButtonItem27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private System.Windows.Forms.BindingSource speciesBindingSource;
        private DevExpress.XtraEditors.TextEdit tbAreaSide11;
        private System.Windows.Forms.ImageList imageList1;
        private Grid.CustomGridView gridViewSegments;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSegmentsID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreesId1;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupPlotNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupTreeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSegmentNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colBdFtLD;
        private DevExpress.XtraGrid.Columns.GridColumn colBdFtDD;
        private DevExpress.XtraGrid.Columns.GridColumn colCuFtLD;
        private DevExpress.XtraGrid.Columns.GridColumn colCuFtDD;
        private DevExpress.XtraGrid.Columns.GridColumn colSortCode;
        private DevExpress.XtraGrid.Columns.GridColumn colGradeCode;
        private DevExpress.XtraGrid.Columns.GridColumn colScribnerNetVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colCubicNetVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId3;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcTopDia;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcButtDia;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colPlotId;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeBasalArea;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTreesPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcAccumulatedLength;
        private DevExpress.XtraGrid.Columns.GridColumn colBdFtPD;
        private DevExpress.XtraGrid.Columns.GridColumn colPlotNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPlotFactorInput1;
        private DevExpress.XtraGrid.Columns.GridColumn colAge1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeStatusDisplayCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colDbh1;
        private DevExpress.XtraGrid.Columns.GridColumn colFormPoint1;
        private DevExpress.XtraGrid.Columns.GridColumn colFormFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colTDF;
        private DevExpress.XtraGrid.Columns.GridColumn colBoleHeight1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHeight1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colVigor;
        private DevExpress.XtraGrid.Columns.GridColumn colDamage;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined;
        private DevExpress.XtraGrid.Columns.GridColumn colBark;
        private DevExpress.XtraGrid.Columns.GridColumn colLogValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAO;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcTotalHtUsedYN1;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem28;
        private DevExpress.XtraBars.BarButtonItem barButtonItem29;
        private DevExpress.XtraEditors.LookUpEdit cbNonStockedTable;
        private System.Windows.Forms.BindingSource nonStockedBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbNonTimberedTable;
        private System.Windows.Forms.BindingSource nonTimberedBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbHarvestTable;
        private System.Windows.Forms.BindingSource harvestBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbState;
        private System.Windows.Forms.BindingSource stateBindingSource;
        private DevExpress.XtraEditors.LookUpEdit cbCounty;
        private System.Windows.Forms.BindingSource stateCountyBindingSource;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies11;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies12;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies13;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies21;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies22;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies23;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies31;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies32;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies33;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies41;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies42;
        private DevExpress.XtraEditors.LookUpEdit tbSpecies43;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cbAspect;
        private System.Windows.Forms.BindingSource aspectsBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cbHabitat;
        private System.Windows.Forms.BindingSource habitatBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cbSpecies;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cbEnvironmental;
        private System.Windows.Forms.BindingSource environmentsBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem31;
        private DevExpress.XtraBars.BarButtonItem barButtonItem32;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemFormPoint;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemFormFactor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemGrade;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemUpper;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem34;
        private DevExpress.XtraBars.BarButtonItem barButtonItem36;
        private DevExpress.XtraBars.BarButtonItem barButtonItem37;
        private DevExpress.XtraBars.BarButtonItem barButtonItem38;
        private DevExpress.XtraBars.BarButtonItem barButtonItem33;
        private DevExpress.XtraBars.BarButtonItem barButtonItem39;
        private DevExpress.XtraBars.BarButtonItem barButtonItem40;
        private DevExpress.XtraBars.BarButtonItem barButtonItem41;
        private DevExpress.XtraBars.BarButtonItem barButtonItem42;
        private DevExpress.XtraBars.BarButtonItem barButtonItem43;
        private DevExpress.XtraBars.BarButtonItem barButtonItem44;
        private DevExpress.XtraBars.BarButtonItem barButtonItem45;
        private DevExpress.XtraBars.BarButtonItem barButtonItem46;
        private DevExpress.XtraBars.BarButtonItem barButtonItem47;
        private DevExpress.XtraBars.BarButtonItem barButtonItem48;
        private DevExpress.XtraBars.BarButtonItem barButtonItem49;
        private DevExpress.XtraBars.BarButtonItem barButtonItem50;
        private DevExpress.XtraBars.BarButtonItem barButtonItem51;
        private DevExpress.XtraBars.BarButtonItem barButtonItem52;
        private DevExpress.XtraBars.BarButtonItem barButtonItem53;
        private DevExpress.XtraBars.BarButtonItem barButtonItem54;
        private DevExpress.XtraBars.BarButtonItem barButtonItem55;
        private DevExpress.XtraBars.BarButtonItem barButtonItem56;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraEditors.TextEdit tbPlots;
        private DevExpress.XtraGrid.Columns.GridColumn colPlots;
        private DevExpress.XtraBars.BarButtonItem barButtonItem57;
        private DevExpress.XtraBars.BarButtonItem barButtonItem58;
        private DevExpress.XtraBars.BarButtonItem barButtonItem59;
        private DevExpress.XtraBars.BarButtonItem barButtonItem60;
        private DevExpress.XtraBars.BarButtonItem barButtonItem61;
        private DevExpress.XtraBars.BarButtonItem barButtonItem62;
        private DevExpress.XtraBars.BarButtonItem barButtonItem63;
        private DevExpress.XtraBars.BarButtonItem barButtonItem65;
        private DevExpress.XtraBars.BarButtonItem barButtonItem66;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId4;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies2;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigAge;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthYear;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteIndex1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigTotalHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigStocking;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigTreesPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigBasalArea;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigD4H;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigLogsPerAcre;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigGrossCuFt;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigNetCuFt;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigGrossBdFt;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigNetBdFt;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbBafInAt;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem67;
        private DevExpress.XtraBars.BarButtonItem barButtonItem64;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem70;
        private DevExpress.XtraBars.BarButtonItem barButtonItem71;
        private DevExpress.XtraBars.BarButtonItem barButtonItem72;
        private DevExpress.XtraBars.BarButtonItem barButtonItem75;
        private DevExpress.XtraBars.BarButtonItem barButtonItem76;
        private DevExpress.XtraBars.BarButtonItem barButtonItem77;
        private DevExpress.XtraBars.BarButtonItem barButtonItem68;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem30;
        private DevExpress.XtraBars.BarButtonItem barButtonItem69;
        private DevExpress.XtraBars.BarButtonItem barButtonItem73;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem74;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigTotalCcf;
        private DevExpress.XtraGrid.Columns.GridColumn origTotalMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtTotalCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdtTotalMbf;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl109;
        private DevExpress.XtraEditors.SimpleButton btnStandInputGrow;
        private DevExpress.XtraEditors.TextEdit tbStandInputGrownToDate;
        private DevExpress.XtraBars.BarButtonItem barButtonItem79;
        private DevExpress.XtraEditors.LabelControl labelControl111;
        private System.Windows.Forms.BindingSource sourceBindingSource;
        private DevExpress.XtraEditors.ComboBoxEdit cbSource;
        private DevExpress.XtraBars.BarButtonItem barButtonItem80;
        private DevExpress.XtraBars.BarButtonItem barButtonItem78;
        private DevExpress.XtraBars.BarButtonItem barButtonItem81;
        private DevExpress.XtraBars.BarButtonItem barButtonItem82;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPFType;
        private DevExpress.XtraGrid.Columns.GridColumn colPFType1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem83;
        private DevExpress.XtraBars.BarButtonItem barButtonItem84;
        private DevExpress.XtraBars.BarButtonItem barButtonItem85;
        private DevExpress.XtraBars.BarButtonItem barButtonItem86;
        private DevExpress.XtraBars.BarButtonItem barButtonItem87;
        private DevExpress.XtraBars.BarButtonItem barButtonItem88;
        private DevExpress.XtraBars.BarButtonItem barButtonItem35;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource standAgeBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId1;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId5;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colAge2;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource standPermPlotBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colId2;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId6;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotOccasion;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPermPlotInterval;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource standYardingSystemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colId3;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId7;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditHarvest;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private System.Windows.Forms.BindingSource standHaulingBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colHaulingId;
        private DevExpress.XtraGrid.Columns.GridColumn colDestination;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadSize;
        private DevExpress.XtraGrid.Columns.GridColumn colRoundTripMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadCcf;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgLoadMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadUnloadHours;
        private DevExpress.XtraGrid.Columns.GridColumn colCalcCostDollarPerMbf;
        private DevExpress.XtraGrid.Columns.GridColumn colStandsId8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem89;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxYarding;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditAge;
    }
}

