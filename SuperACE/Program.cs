﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars.Localization;

namespace SuperACE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // set global variables according to config settings
            SuperACEUtils.Utils.Setting_RptPrepHardWrite = Convert.ToBoolean(Properties.Settings.Default["RptPrepHardWrites"]);


            //BonusSkins.Register();
            SkinManager.EnableFormSkins();
            //BarLocalizer.Active = new SuperACEBarLocalizer();
            f = new MainForm();
            Application.Run(f);
        }

        static MainForm f;

        public static MainForm MForm
        {
            get { return f; }
        }
    }
}
