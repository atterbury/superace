﻿using DevExpress.Data.Filtering;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.LookAndFeel;

using SuperACEForms;
using SuperACE.Reports;
using SuperACEUtils;

using Project.DAL;
using Projects.DAL;
using DevExpress.XtraGrid;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using System.Text.RegularExpressions;
using System.Deployment.Application;
using DevExpress.Skins;
using Temp.DAL;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraEditors;
using DevExpress.Utils.Drawing;
using System.IO;
using DevExpress.XtraGrid.Views.Base;
using Import;
using SuperACECalcs;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraEditors.Controls;
//using SuperACE.Forms;
using SuperACE.Properties;
using System.Data.Entity.Core;
using System.Collections;
using DevExpress.XtraPrinting;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using SuperACE.Excel;

namespace SuperACE
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        const string version = @"Version: 2018.01.23";

        private string homeLayout = @"C:\ACI\SuperACE\HomeLayout.xml";
        private string plotLayout = @"C:\ACI\SuperACE\PlotLayout.xml";
        private string inputLayout = @"C:\ACI\SuperACE\TreeInputLayout.xml";
        private string editLayout = @"C:\ACI\SuperACE\TreeEditLayout.xml";
        private string origLayout = @"C:\ACI\SuperACE\StandInputOrigLayout.xml";
        private string grownLayout = @"C:\ACI\SuperACE\StandInputGrownLayout.xml";

        private List<Stand> stands = null;
        private List<Stand> sColl = new List<Stand>();

        private bool needWrkTrees = true;
        private bool needWrkSegments = true;
        private bool needToUpdatePlotsStands = false;

        private string defPlotPlotNumber = string.Empty;
        private string defPlotSpecies = string.Empty;
        private string defTreePlotNumber = string.Empty;
        private int defTreeTreeNumber = 0;
        private Species defTreeSpecies = null;
        private string defTreePF = string.Empty;
        private byte defTreeAge = 0;
        private byte defTreeFP = 0;
        private string defTreeTDF = string.Empty;

        private bool calcForMaster = false;
        private bool _IsSynced = true;
        private int delta = 0;
        private bool IsError = false;
        private bool siteIndexError = false;

        private float totalAcres = 0;
        private float totalPlots = 0;
        private float totalTrees = 0;

        // static fields
        public static ProjectDbContext projectContext = null;
        public static ProjectsDbContext locContext = null;
        public static string ProjectLocationDataSource { get; set; }
        public static string ProjectDataSource { get; set; }
        public static string TempDataSource { get; set; }
        public static Projects.DAL.AllProject CurrentProjectLocation { get; set; }
        //public static ProjectLocationModel.PROGRAMSETTINGS CurrentProgramSetting { get; set; }
        public static Project.DAL.Project CurrentProject { get; set; }
        public static Stand CurrentStand { get; set; }
        public static Plot CurrentPlot { get; set; }
        public static Tree CurrentTree { get; set; }
        //public static Treesegment CurrentSegment { get; set; }

        public MainForm()
        {
            InitializeComponent();

            //advBandedGridViewTrees.RowCellStyle += advBandedGridViewTrees_RowCellStyle;
            gridViewSegments.GroupRowCollapsing += gridViewSegments_GroupRowCollapsing;
            gridViewSegments.CalcRowHeight += gridViewSegments_CalcRowHeight;
            gridViewSegments.CustomColumnDisplayText += gridViewSegments_CustomColumnDisplayText;
            //gridViewSegments.RowCellStyle += gridViewSegments_RowCellStyle;

            advBandedGridViewTrees.CustomColumnDisplayText += advBandedGridViewTrees_CustomColumnDisplayText;

            defaultLookAndFeel1.LookAndFeel.SkinName =
                SuperACE.Properties.Settings.Default["ApplicationSkinName"].ToString();
            AddSkinsMenu(bar2);

            ProjectLocationDataSource = SuperACE.Properties.Settings.Default["DbLocation"].ToString() + "Projects.sdf";
            TempDataSource = SuperACE.Properties.Settings.Default["DbLocation"].ToString() + "Temp.sdf";

            barStaticItem4.Caption = version;

            gridViewStands.Images = imageList1;
            gridViewStands.Columns["Attachments"].ImageIndex = 0;
            gridViewStands.Columns["Attachments"].ImageAlignment = StringAlignment.Center;
            gridViewStands.Columns["Attachments"].OptionsColumn.ShowCaption = false;

            advBandedGridViewPlots.Images = imageList1;
            advBandedGridViewPlots.Columns["Attachments"].ImageIndex = 0;
            advBandedGridViewPlots.Columns["Attachments"].ImageAlignment = StringAlignment.Center;
            advBandedGridViewPlots.Columns["Attachments"].OptionsColumn.ShowCaption = false;

            if (File.Exists(homeLayout))
                gridControlStands.MainView.RestoreLayoutFromXml(homeLayout);
            if (File.Exists(plotLayout))
                gridControlPlots.MainView.RestoreLayoutFromXml(plotLayout);
            if (File.Exists(inputLayout))
                gridControlTrees.MainView.RestoreLayoutFromXml(inputLayout);
            if (File.Exists(editLayout))
                gridControlSegments.MainView.RestoreLayoutFromXml(editLayout);
            if (File.Exists(origLayout))
                gridControlStandInputOrig.MainView.RestoreLayoutFromXml(origLayout);
            if (File.Exists(grownLayout))
                gridControlStandInputGrown.MainView.RestoreLayoutFromXml(grownLayout);

            splitContainerControl1.SplitterPosition = splitContainerControl1.Height / 2;

#if DEBUG
            barSubItem4.Visibility = BarItemVisibility.Always;
#else
            barSubItem4.Visibility = BarItemVisibility.Never;
            //barButtonItem30.Visibility = BarItemVisibility.Never;
            barButtonItem31.Visibility = BarItemVisibility.Never;
            barButtonItem32.Visibility = BarItemVisibility.Never;
            barButtonItem63.Visibility = BarItemVisibility.Never;
            barButtonItem67.Visibility = BarItemVisibility.Never;
            barButtonItem68.Visibility = BarItemVisibility.Never;
            //barButtonItem73.Visibility = BarItemVisibility.Never;
#endif
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                setWindowSizing();
            }

            if (IsV40Installed())
            {
                LoadData();
            }
            else
            {
                XtraMessageBox.Show(
                    "Sql Server Compact 4.0 is not installed on this device.\nPlease install Sql Server Compact 4.0 to continue.",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            // check against tiny window values which hides the screen
            setWindowSizing();

            //LoadData();
        }

        private void setWindowSizing()
        {
            Size sz = Settings.Default.ClientSize;
            const int MIN_WINDOW_WIDTH = 400;
            const int MIN_WINDOW_HEIGHT = 200;
            if ((sz.IsEmpty) || (sz.Height < MIN_WINDOW_HEIGHT) || (sz.Width < MIN_WINDOW_WIDTH))
                sz = new Size(MIN_WINDOW_WIDTH, MIN_WINDOW_WIDTH);
            this.Size = sz;
            // center the location, if not good values can handle previous maximized state
            Point loc = Settings.Default.Location;
            if (loc.IsEmpty)
                this.StartPosition = FormStartPosition.CenterScreen;
            else
                this.Location = loc;
        }

        private void LoadData()
        {
            try
            {
                if (locContext != null)
                    locContext.Dispose();
                locContext = new ProjectsDbContext("Data Source = " + ProjectLocationDataSource);

                ProjectDataSource = Properties.Settings.Default["CurrentDb"].ToString();
                if (!File.Exists(ProjectDataSource))
                {
                    DialogResult dlgResult =
                        XtraMessageBox.Show(
                            string.Format(
                                "The Current Database does not exist at the specified location ({0}).\nWould you like to select a different Project?",
                                ProjectDataSource),
                            "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (dlgResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        SelectProject(true);
                    }
                    else
                    {
                        this.Close();
                        return;
                    }
                }

                if (projectContext != null)
                    projectContext.Dispose();
                projectContext = new ProjectDbContext("Data Source = " + ProjectDataSource);
                CurrentProject = ProjectBLL.GetProject(ref projectContext, ProjectDataSource);
                if (CurrentProject == null)
                {
                    XtraMessageBox.Show(
                        string.Format("The Current Project does not exist at the specified location ({0}).",
                            ProjectDataSource), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                this.Text = "SuperACE 2017 V1-Test";
                barStaticItem2.Caption = string.Format("[Project: {0}    Location: {1}]",
                    Properties.Settings.Default["CurrentProject"].ToString(),
                    Properties.Settings.Default["CurrentDataDir"].ToString());

                if (!projectContext.TmpStandInputExceptions.Any())
                    barButtonItem78.Visibility = BarItemVisibility.Never;
                tabPane1.SelectedPage = tabNavigationPageHome;
            }
            catch
            {

            }
        }

        public Treesegment CurrentSegment()
        {
            return (segmentsBindingSource.Current as Treesegment);
        }

        private bool IsV40Installed()
        {
            try
            {
                System.Reflection.Assembly.Load(
                    "System.Data.SqlServerCe, Version=4.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91");
            }
            catch (System.IO.FileNotFoundException)
            {
                return false;
            }

            try
            {
                var factory = System.Data.Common.DbProviderFactories.GetFactory("System.Data.SqlServerCe.4.0");
            }
            catch (System.Configuration.ConfigurationException)
            {
                return false;
            }
            catch (System.ArgumentException)
            {
                return false;
            }
            return true;
        }

        void advBandedGridViewTrees_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "TotalHeight")
            {
                string calcTotalHtUsedYN =
                    (string) advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "CalcTotalHtUsedYn");
                if (calcTotalHtUsedYN != null)
                {
                    if (calcTotalHtUsedYN == "Y")
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                }
            }
        }

        void gridViewSegments_CalcRowHeight(object sender, RowHeightEventArgs e)
        {
            if (e.RowHandle < 0)
                e.RowHeight = 0;
            else
                e.RowHeight = 20;
        }

        void gridViewSegments_GroupRowCollapsing(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            e.Allow = false;
        }

        void gridViewSegments_CustomColumnDisplayText(object sender,
            DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            ColumnView view = sender as ColumnView;
            string treeType = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "TreeType").ToString();
            int segment = (int) view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SegmentNumber");

            if (segment > 1) // was != 1
            {
                switch (e.Column.FieldName)
                {
                    case "PlotNumber":
                    case "TreeNumber":
                    case "PlotFactorInput":
                    case "Age":
                    case "Species":
                    case "TreeStatusDisplayCode":
                    case "TreeCount":
                    case "Dbh":
                    case "FormPoint":
                    case "FormFactor":
                    case "Tdf":
                    case "BoleHeight":
                    case "TotalHeight":
                    case "CrownPosition":
                    case "CrownRatio":
                    case "Vigor":
                    case "Damage":
                    case "UserDefined":
                        e.DisplayText = string.Empty;
                        break;
                }
            }
            if (treeType == "C") // was != 1
            {
                switch (e.Column.FieldName)
                {
                    case "CalcTopDia":
                    case "CalcButtDia":
                    case "CalcAccumulatedLength":
                    case "Bark":
                    case "Ao":
                    case "LogValue":
                        e.DisplayText = string.Empty;
                        break;
                }
            }
        }

        void gridViewSegments_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            switch (e.Column.FieldName)
            {
                case "Comments":
                    string comments = (string)gridViewSegments.GetRowCellValue(e.RowHandle, "Comments");
                    if (!string.IsNullOrEmpty(comments))
                        e.Appearance.ForeColor = Color.Red;
                    else
                        e.Appearance.ForeColor = Color.Black;
                    break;
                case "TotalHeight":
                    string calcTotalHtUsedYN = (string)gridViewSegments.GetRowCellValue(e.RowHandle, "CalcTotalHtUsedYn");
                    int seg1 = Utils.ConvertToInt(gridViewSegments.GetRowCellValue(e.RowHandle, "SegmentNumber").ToString());
                    if (seg1 != null)
                    {
                        if (seg1 == 0)
                            e.Appearance.ForeColor = Color.Gray; //.Chocolate;
                        else
                            e.Appearance.ForeColor = Color.Black;
                    }
                    else
                    {
                        if (calcTotalHtUsedYN != null)
                        {
                            if (calcTotalHtUsedYN == "Y")
                                e.Appearance.ForeColor = Color.Red;
                            else
                                e.Appearance.ForeColor = Color.Black;
                        }
                    }
                    break;
                case "TreeBasalArea":
                case "TreeTreesPerAcre":
                case "ScribnerNetVolume":
                case "CubicNetVolume":
                case "FormFactor":
                    int seg2 = Utils.ConvertToInt(gridViewSegments.GetRowCellValue(e.RowHandle, "SegmentNumber").ToString());
                    if (seg2 != null)
                    {
                        if (seg2 == 0)
                            e.Appearance.ForeColor = Color.Gray; //.Chocolate;
                        else
                            e.Appearance.ForeColor = Color.Black;
                    }
                    break;
            }
        }

        //private void BuildTableMenu()
        //{
        //    List<ProjectLocation.DAL.Tables> tables = ProjectLocationDAL.GetTables();
        //    foreach (var table in tables)
        //    {
        //        BarSubItem tableMenuItem = new BarSubItem(barManager1, table.Heading);
        //        //barSubItemTables.Items.Add(tableMenuItem);
        //        barManager1.Items.Add(tableMenuItem);
        //        barSubItemTables.ItemLinks.Add(tableMenuItem);
        //        List<ProjectLocation.DAL.TableItems> tableItems = ProjectLocationDAL.GetTableItems(table.TablesID);
        //        //if (tableItems.Count == 0)
        //        //    tableMenuItem.ItemClick += tableMenuItem_ItemClick;
        //        foreach (var item in tableItems)
        //        {
        //            if (item.Active == "Y")
        //            {
        //                //BarSubItem tableItem = new BarSubItem(barManager1, item.Heading);
        //                //barManager1.Items.Add(tableItem);
        //                //tableItem.ItemLinks.Add(tableItem);
        //                //tableItem.ItemClick +=  tableItem_ItemClick;
        //            }
        //        }
        //        tableItems.Clear();
        //    }
        //    tables.Clear();             
        //}

        private void tableItem_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void tableMenuItem_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        #region Load Stands

        private void LoadStands()
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                ShowHideStandColumns(false);

                //projectDb = new SqlCeConnection(@"Data Source = C:\ACI\SuperACE\Data\DEMO.sdf");
                if (stands != null)
                    stands.Clear();
                stands = ProjectBLL.GetAllStands(ref projectContext);
                standsViewBindingSource.DataSource = stands;
                barStaticItem1.Caption = string.Format("{0} Stands loaded.", stands.Count);

                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                barStaticItem3.Caption = string.Format("Loading Time - {0} ms", ts.Milliseconds);

                gridViewStands.ExpandAllGroups();
                gridViewStandInputGrown.UpdateSummary();

                CheckProjectTableDefaults();
                gridViewStands.MoveFirst();
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckProjectTableDefaults()
        {
            if (CurrentProject != null)
            {
                StringBuilder message = new StringBuilder();
                if (!projectContext.Species.Any(s => s.TableName == CurrentProject.SpeciesTableName))
                    message.Append(string.Format("No Entries were found for Species Table [{0}]\n",
                        CurrentProject.SpeciesTableName));
                if (!projectContext.Sorts.Any(s => s.TableName == CurrentProject.SortsTableName))
                    message.Append(string.Format("No Entries were found for Sort Table [{0}]\n",
                        CurrentProject.SortsTableName));
                if (!projectContext.Grades.Any(s => s.TableName == CurrentProject.GradesTableName))
                    message.Append(string.Format("No Entries were found for Grade Table [{0}]\n",
                        CurrentProject.GradesTableName));
                if (!projectContext.Prices.Any(s => s.TableName == CurrentProject.PriceTableName))
                    message.Append(string.Format("No Entries were found for Price Table [{0}]\n",
                        CurrentProject.PriceTableName));
                if (!projectContext.Costs.Any(s => s.TableName == CurrentProject.CostTableName))
                    message.Append(string.Format("No Entries were found for Cost Table [{0}]\n",
                        CurrentProject.CostTableName));
                if (message.Length > 0)
                {
                    message.Append("\nPlease edit the Project Table Defaults to correct the problem.");
                    XtraMessageBox.Show(message.ToString(), "Status", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        #endregion

        #region Plot Tab

        public void LoadPlots()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (stands.Count == 0)
            {
                XtraMessageBox.Show("There are no Stands in this Project.\nYou need to add Stands?", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CurrentStand = standsViewBindingSource.Current as Stand;

            speciesBindingSource.Clear();
            speciesBindingSource.DataSource =
                projectContext.Species.OrderBy(s => s.Abbreviation)
                    .Where(s => s.TableName == CurrentProject.SpeciesTableName)
                    .ToList();

            aspectsBindingSource.Clear();
            aspectsBindingSource.DataSource =
                projectContext.Aspects.OrderBy(s => s.DisplayCode)
                    .Where(s => s.TableName == CurrentProject.AspectsTableName)
                    .ToList();

            habitatBindingSource.Clear();
            habitatBindingSource.DataSource =
                projectContext.Habitats.OrderBy(s => s.DisplayCode)
                    .Where(s => s.TableName == CurrentProject.HabitatTableName)
                    .ToList();

            environmentsBindingSource.Clear();
            environmentsBindingSource.DataSource =
                projectContext.Environments.OrderBy(s => s.DisplayCode)
                    .Where(s => s.TableName == CurrentProject.EnvironmentsTableName)
                    .ToList();

            tbPlotsTract.Text = CurrentStand.TractName;
            tbPlotsStand.Text = CurrentStand.StandName;
            tbPlotsTownship.Text = CurrentStand.Township;
            tbPlotsRange.Text = CurrentStand.Range;
            tbPlotsSection.Text = CurrentStand.Section;
            tbPlotsNetAcres.Text = CurrentStand.NetGeographicAcres.ToString();

            tbPlotsSpeciesTable.Text = CurrentStand.SpeciesTableName;
            tbPlotsSortTable.Text = CurrentStand.SortTableName;
            tbPlotsGradeTable.Text = CurrentStand.GradeTableName;
            tbPlotsPriceTable.Text = CurrentStand.PriceTableName;
            tbPlotsCostTable.Text = CurrentStand.CostTableName;

            //var atms = projectContext.AdditionalTreeMeasurement.Where(a => a.StandsId == MainForm.CurrentStand.StandsId).Select(a => a.StandsId).ToList();
            //if (atms.Count > 0)
            //{
            //    rlPlotStatus.Text = "Stand has Additional Tree Measureents";
            //    rlPlotStatus.Visible = true;
            //}
            //else
            //{
            //    rlPlotStatus.Text = string.Empty;
            //    rlPlotStatus.Visible = false;
            //}

            ShowHidePlotColumns(false);

            if (CurrentStand != null)
            {
                LoadPlotDataSource();
            }
            else
            {
                MessageBox.Show("No selected Stand.", "Warning", MessageBoxButtons.OK);
            }
            //WaitingForm.CloseForm();

            //if (gridPlots.Rows.Count > 0)
            //    gridPlots.GridNavigator.SelectFirstRow();

            //UpdateMessage(string.Format("Plots in Stand = {0}", plotBindingSource.Count));

            Cursor.Current = Cursors.Default;
        }

        private void LoadPlotDataSource()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            if (plotsBindingSource.Count > 0)
                plotsBindingSource.Clear();
            plotsBindingSource.DataSource =
                projectContext.Plots.OrderBy(p => p.UserPlotNumber).Where(p => p.StandsId == CurrentStand.StandsId).ToList();
            if (plotsBindingSource.Count == 0)
                AddPlot();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            barStaticItem1.Caption = string.Format("{0} Plots loaded.", plotsBindingSource.Count);
            barStaticItem3.Caption = string.Format("Loading Time - {0} ms", ts.Milliseconds);
        }

        private void AddPlot()
        {
            Plot rec = new Plot();
            rec.StandsId = CurrentStand.StandsId;
            rec.AddFlag = string.Empty;
            rec.Aspect = string.Empty;
            rec.Attachments = false;
            rec.BafInAt = string.Empty;
            rec.BasalAreaAcre = 0;
            rec.CruFlag = string.Empty;
            rec.Cruiser = string.Empty;
            rec.DwFlag = string.Empty;
            rec.Environmental = string.Empty;
            rec.Habitat = string.Empty;
            rec.NetBfAcre = 0;
            rec.NetCfAcre = 0;
            rec.Notes = string.Empty;
            rec.PlotDate = DateTime.Now;
            rec.RefFlag = string.Empty;
            rec.Slope = string.Empty;
            rec.Species = string.Empty;
            rec.TreatmentDisplayCode = string.Empty;
            rec.TreesPerAcre = 0;
            rec.UserPlotNumber = string.Empty;
            rec.X = 0;
            rec.Y = 0;
            rec.Z = 0;
            projectContext.Plots.Add(rec);
            projectContext.SaveChanges();
            LoadPlotDataSource();
            advBandedGridViewPlots.MoveLast();
            advBandedGridViewPlots.FocusedColumn = advBandedGridViewPlots.Columns["UserPlotNumber"];
            gridControlPlots.Focus();
        }

        #endregion

        #region Load Trees

        public void LoadTrees()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (stands.Count == 0)
            {
                XtraMessageBox.Show("There are no Stands in this Project.\nYou need to add Stands?", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CurrentStand = standsViewBindingSource.Current as Stand;

            tbTreesTract.Text = CurrentStand.TractName;
            tbTreesStand.Text = CurrentStand.StandName;
            tbTreesTownship.Text = CurrentStand.Township;
            tbTreesRange.Text = CurrentStand.Range;
            tbTreesSection.Text = CurrentStand.Section;
            tbTreesNetAcres.Text = CurrentStand.NetGeographicAcres.ToString();

            tbTreesSpeciesTable.Text = CurrentStand.SpeciesTableName;
            tbTreesSortTable.Text = CurrentStand.SortTableName;
            tbTreesGradeTable.Text = CurrentStand.GradeTableName;
            tbTreesPriceTable.Text = CurrentStand.PriceTableName;
            tbTreesCostTable.Text = CurrentStand.CostTableName;

            //var atms = projectContext.AdditionalTreeMeasurement.Where(a => a.StandsId == MainForm.CurrentStand.StandsId).Select(a => a.StandsId).ToList();
            //if (atms.Count > 0) // atms.Count > 0
            //{
            //    rlTreeInputStatus.Text = "Stand has Additional Tree Measureents";
            //    rlTreeInputStatus.Visible = true;
            //}
            //else
            //{
            //    rlTreeInputStatus.Text = string.Empty;
            //    rlTreeInputStatus.Visible = false;
            //}

            ShowHideTreeColumns(false);

            if (treesBindingSource.Count > 0)
                treesBindingSource.Clear();

            if (CurrentStand != null)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                LoadTreeDataSource();

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                barStaticItem3.Caption = String.Format("Loading Time - {0} ms", ts.Milliseconds);
            }
            else
            {
                MessageBox.Show("No selected Stand.", "Warning", MessageBoxButtons.OK);
            }

            if (CurrentPlot != null)
            {
                int treeRowHandle = advBandedGridViewTrees.LocateByValue("PlotNumber", CurrentPlot.UserPlotNumber);
                if (treeRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                    advBandedGridViewTrees.FocusedRowHandle = treeRowHandle;
            }
            else
            {
                if (advBandedGridViewTrees.DataRowCount > 0)
                    advBandedGridViewTrees.MoveFirst();
            }

            advBandedGridViewTrees.UpdateSummary();

            //if (treesBindingSource.Count > 0)
            //{
            //    gridTrees.Rows[gridTrees.Rows.Count - 1].Cells["SpeciesAbbreviation"].IsSelected = true;
            //    mTree = Utils.ConvertToInt(gridTrees.Rows[gridTrees.Rows.Count - 1].Cells["TreeNumber"].Value.ToString());
            //}

            barStaticItem1.Caption = string.Format("{0} Trees loaded.", treesBindingSource.Count);

            Cursor.Current = Cursors.Default;
        }

        #endregion

        #region Load Segments

        public void LoadSegments(int pRow, string pCol)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (stands.Count == 0)
            {
                XtraMessageBox.Show("There are no Stands in this Project.\nYou need to add Stands?", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CurrentStand = standsViewBindingSource.Current as Stand;

            tbSegTract.Text = CurrentStand.TractName;
            tbSegStand.Text = CurrentStand.StandName;
            tbSegTownship.Text = CurrentStand.Township;
            tbSegRange.Text = CurrentStand.Range;
            tbSegSection.Text = CurrentStand.Section;
            tbSegNetAcres.Text = CurrentStand.NetGeographicAcres.ToString();

            tbSegSpeciesTable.Text = CurrentStand.SpeciesTableName;
            tbSegSortTable.Text = CurrentStand.SortTableName;
            tbSegGradeTable.Text = CurrentStand.GradeTableName;
            tbSegPriceTable.Text = CurrentStand.PriceTableName;
            tbSegCostTable.Text = CurrentStand.CostTableName;

            //var atms = projectContext.AdditionalTreeMeasurement.Where(a => a.StandsId == MainForm.CurrentStand.StandsId).Select(a => a.StandsId).ToList();
            //if (atms.Count > 0) // atms.Count > 0
            //{
            //    rlTreeInputStatus.Text = "Stand has Additional Tree Measureents";
            //    rlTreeInputStatus.Visible = true;
            //}
            //else
            //{
            //    rlTreeInputStatus.Text = string.Empty;
            //    rlTreeInputStatus.Visible = false;
            //}

            ShowHideSegmentColumns(false);

            if (segmentsBindingSource.Count > 0)
                segmentsBindingSource.Clear();

            if (CurrentStand != null)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                segmentsBindingSource.DataSource =
                    projectContext.Treesegments
                        .OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .ThenBy(t => t.SegmentNumber)
                        .Where(t => t.StandsId == CurrentStand.StandsId)
                        .ToList();

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                barStaticItem3.Caption = String.Format("Loading Time - {0} ms", ts.Milliseconds);

            }
            else
            {
                MessageBox.Show("No selected Stand.", "Warning", MessageBoxButtons.OK);
            }

            gridViewSegments.ExpandAllGroups();

            if (pRow == -1 && pCol == null)
            {
                if (CurrentTree != null)
                {
                    for (int i = 0; i < gridViewSegments.DataRowCount; i++)
                    {
                        string plot = gridViewSegments.GetRowCellValue(i, "PlotNumber").ToString();
                        int tree = Utils.ConvertToInt(gridViewSegments.GetRowCellValue(i, "TreeNumber").ToString());
                        if (plot == CurrentTree.PlotNumber && tree == CurrentTree.TreeNumber)
                        {
                            gridViewSegments.FocusedRowHandle = i;
                            break;
                        }
                    }
                }
                else
                {
                    if (gridViewSegments.DataRowCount > 0)
                        gridViewSegments.MoveFirst();
                }
            }

            gridViewSegments.UpdateSummary();

            //if (MainForm.CurrentSegment != null) // this.gridPlots.SelectedRows.Count > 0
            //{
            //    for (int i = 0; i < this.gridTrees.Rows.Count; i++)
            //    {
            //        if (this.gridTrees.Rows[i].Cells["PlotNumber"].Value.ToString() == MainForm.CurrentSegment.GroupPlotNumber &&
            //            Utils.ConvertToInt(this.gridTrees.Rows[i].Cells["TreeNumber"].Value.ToString()) == MainForm.CurrentSegment.GroupTreeNumber)
            //        {
            //            this.gridTrees.Rows[i].IsSelected = true;
            //            GridTableElement te = this.gridTrees.CurrentView as GridTableElement;
            //            if (te != null)
            //                te.ScrollToRow(i);
            //            break;
            //        }
            //    }
            //}
            //else
            //    if (MainForm.CurrentPlot != null) // this.gridPlots.SelectedRows.Count > 0
            //    {
            //        for (int i = 0; i < this.gridTrees.Rows.Count; i++)
            //        {
            //            if (this.gridTrees.Rows[i].Cells["PlotNumber"].Value.ToString() == MainForm.CurrentPlot.UserPlotNumber)
            //            {
            //                this.gridTrees.Rows[i].IsSelected = true;
            //                GridTableElement te = this.gridTrees.CurrentView as GridTableElement;
            //                if (te != null)
            //                    te.ScrollToRow(i);
            //                break;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (gridTrees.Rows.Count > 0)
            //            this.gridTrees.GridNavigator.SelectFirstRow();
            //    }

            //if (treeBindingSource.Count > 0)
            //{
            //    gridTrees.Rows[gridTrees.Rows.Count - 1].Cells["SpeciesAbbreviation"].IsSelected = true;
            //    mTree = Utils.ConvertToInt(gridTrees.Rows[gridTrees.Rows.Count - 1].Cells["TreeNumber"].Value.ToString());
            //}

            barStaticItem1.Caption = string.Format("{0} Segments loaded.", segmentsBindingSource.Count);

            if (pRow != -1)
                gridViewSegments.FocusedRowHandle = pRow;
            if (pCol != null)
                gridViewSegments.FocusedColumn = gridViewSegments.Columns[pCol];

            Cursor.Current = Cursors.Default;
        }

        #endregion

        #region Load Stand Input

        private void LoadStandInput()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (stands.Count == 0)
            {
                XtraMessageBox.Show("There are no Stands in this Project.\nYou need to add Stands?", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CurrentStand = standsViewBindingSource.Current as Stand;

            tbStandInputTract.Text = CurrentStand.TractName;
            tbStandInputStand.Text = CurrentStand.StandName;
            tbStandInputTownship.Text = CurrentStand.Township;
            tbStandInputRange.Text = CurrentStand.Range;
            tbStandInputSection.Text = CurrentStand.Section;
            tbStandInputNetAcres.Text = CurrentStand.NetGeographicAcres.ToString();

            tbStandInputSpeciesTable.Text = CurrentStand.SpeciesTableName;
            tbStandInputSortTable.Text = CurrentStand.SortTableName;
            tbStandInputGradeTable.Text = CurrentStand.GradeTableName;
            tbStandInputPriceTable.Text = CurrentStand.PriceTableName;
            tbStandInputCostTable.Text = CurrentStand.CostTableName;

            if (standInputBindingSource.Count > 0)
                standInputBindingSource.Clear();

            if (CurrentStand != null)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                List<DefReportDomPri> domPri = projectContext.DefReportDomPris.OrderBy(p => p.Priority).ToList();
                switch(domPri[0].Item)
                {
                    case "Trees per Acre":
                        standInputBindingSource.DataSource =
                            projectContext.StandInputs
                                .OrderBy(s => s.Species)
                                .ThenBy(s => s.OrigAge)
                                .ThenBy(s => s.OrigTreesPerAcre)
                                .Where(s => s.StandsId == CurrentStand.StandsId)
                                .ToList();
                        colOrigStocking.Caption = "(Tpa) Norm- ality";
                        colUpdtStocking.Caption = "(Tpa) Norm- ality";
                        break;
                    case "Normality":
                        standInputBindingSource.DataSource =
                            projectContext.StandInputs
                                .OrderBy(s => s.Species)
                                .ThenBy(s => s.OrigAge)
                                .ThenBy(s => s.OrigStocking)
                                .Where(s => s.StandsId == CurrentStand.StandsId)
                                .ToList();
                        colOrigStocking.Caption = "(Nrmty) Norm- ality";
                        colUpdtStocking.Caption = "(Nrmty) Norm- ality";
                        break;
                    case "Net Cubic Feet / Acre":
                        standInputBindingSource.DataSource =
                            projectContext.StandInputs
                            .OrderBy(s => s.Species)
                            .ThenBy(s => s.OrigAge)
                            .ThenBy(s => s.OrigNetCuFt)
                            .Where(s => s.StandsId == CurrentStand.StandsId)
                            .ToList();
                        colOrigStocking.Caption = "(NCuFt) Norm- ality";
                        colUpdtStocking.Caption = "(NCuFt) Norm- ality";
                        break;
                    case "Net Board Feet / Acre":
                        standInputBindingSource.DataSource =
                        projectContext.StandInputs
                            .OrderBy(s => s.Species)
                            .ThenBy(s => s.OrigAge)
                            .ThenBy(s => s.OrigNetBdFt)
                            .Where(s => s.StandsId == CurrentStand.StandsId)
                            .ToList();
                        colOrigStocking.Caption = "(NBdFt) Norm- ality";
                        colUpdtStocking.Caption = "(NBdFt) Norm- ality";
                        break;
                    case "Basal Area / Acre":
                        standInputBindingSource.DataSource =
                        projectContext.StandInputs
                            .OrderBy(s => s.Species)
                            .ThenBy(s => s.OrigAge)
                            .ThenBy(s => s.OrigBasalArea)
                            .Where(s => s.StandsId == CurrentStand.StandsId)
                            .ToList();
                        colOrigStocking.Caption = "(Ba) Norm- ality";
                        colUpdtStocking.Caption = "(Ba) Norm- ality";
                        break;
                    default:
                        standInputBindingSource.DataSource =
                        projectContext.StandInputs
                            .OrderBy(s => s.Species)
                            .ThenBy(s => s.OrigAge)
                            .ThenBy(s => s.OrigNetBdFt)
                            .Where(s => s.StandsId == CurrentStand.StandsId)
                            .ToList();
                        colOrigStocking.Caption = "(NBdFt) Norm- ality";
                        colUpdtStocking.Caption = "(NBdFt) Norm- ality";
                        break;
                }

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                barStaticItem1.Caption = string.Format("{0} Stand Input loaded.", standInputBindingSource.Count);
                barStaticItem3.Caption = string.Format("Loading Time - {0} ms", ts.Milliseconds);
            }
            else
            {
                MessageBox.Show("No selected Stand.", "Warning", MessageBoxButtons.OK);
            }

            DateTime d1 = (DateTime)CurrentStand.DateOfStandData;
            DateTime d2 = (DateTime)CurrentStand.GrownToDate;

            groupControl8.Text = string.Format("Original - {0}", d1.ToShortDateString());
            groupControl9.Text = string.Format("Updated - {0}", d2.ToShortDateString());
            tbStandInputGrownToDate.Text = d2.ToShortDateString();

            Cursor.Current = Cursors.Default;
        }

        #endregion

        #region Load Stand Master

        private void LoadStandMaster()
        {
            if (stands.Count == 0)
            {
                XtraMessageBox.Show("There are no Stands in this Project.\nYou need to add Stands?", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();

            CurrentStand = standsViewBindingSource.Current as Stand;
            calcForMaster = false;

            PopulateStandMasterTables();

            GetStandProperties();

            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);

            panelControl8.Select();
            tbNetAcres.Select();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            barStaticItem3.Caption = String.Format("Loading Time - {0} ms", ts.Milliseconds);
            barStaticItem1.Caption = "Stand Master loaded.";
        }

        private void PopulateStandMasterTables()
        {
            speciesBindingSource.Clear();
            speciesBindingSource.DataSource =
                projectContext.Species.OrderBy(s => s.Abbreviation)
                    .Where(s => s.TableName == CurrentProject.SpeciesTableName)
                    .ToList();

            stateBindingSource.Clear();
            stateBindingSource.DataSource = ProjectsBLL.GetStates(ref locContext); // qSpc;

            stateCountyBindingSource.Clear();
            stateCountyBindingSource.DataSource = ProjectsBLL.GetStateCounty(ref locContext, cbState.Text);

            nonStockedBindingSource.Clear();
            nonStockedBindingSource.DataSource = ProjectBLL.GetNonStocked(ref projectContext,
                CurrentProject.NonStockedTableName);

            nonTimberedBindingSource.Clear();
            nonTimberedBindingSource.DataSource = ProjectBLL.GetNonTimbered(ref projectContext,
                CurrentProject.NonTimberedTableName);

            haulingBindingSource.Clear();
            haulingBindingSource.DataSource = ProjectBLL.GetHauling(ref projectContext, CurrentStand.CostTableName);

            repositoryItemComboBox1.Items.Clear();
            List<Hauling> haulingColl = ProjectBLL.GetHauling(ref projectContext, CurrentStand.CostTableName);
            foreach (var haulingItem in haulingColl)
                repositoryItemComboBox1.Items.Add(haulingItem.Destination);

            harvestBindingSource.Clear();
            harvestBindingSource.DataSource = ProjectBLL.GetHarvestSystems(ref projectContext,
                CurrentProject.HarvestSystemsTableName);

            repositoryItemComboBoxYarding.Items.Clear();
            List<string> yarding = ProjectBLL.GetYardingCosts(ref projectContext, CurrentStand.CostTableName);
            foreach (var yardItem in yarding)
                repositoryItemComboBoxYarding.Items.Add(yardItem);

            cbSpeciesTable.Properties.Items.Clear();
            List<string> species = ProjectBLL.GetSpeciesTableNames(ref projectContext);
            foreach (var spcItem in species)
                cbSpeciesTable.Properties.Items.Add(spcItem);

            cbSortTable.Properties.Items.Clear();
            List<string> sorts = ProjectBLL.GetSortTableNames(ref projectContext);
            foreach (var srtItem in sorts)
                cbSortTable.Properties.Items.Add(srtItem);

            cbGradeTable.Properties.Items.Clear();
            List<string> grades = ProjectBLL.GetGradeTableNames(ref projectContext);
            foreach (var grdItem in grades)
                cbGradeTable.Properties.Items.Add(grdItem);

            cbPriceTable.Properties.Items.Clear();
            List<string> prices = ProjectBLL.GetPriceTableNames(ref projectContext);
            foreach (var priceItem in prices)
                cbPriceTable.Properties.Items.Add(priceItem);

            cbCostTable.Properties.Items.Clear();
            List<string> costs = ProjectBLL.GetCostTableNames(ref projectContext);
            foreach (var costItem in costs)
                cbCostTable.Properties.Items.Add(costItem);

            cbSource.Properties.Items.Clear();
            List<string> sources = ProjectsBLL.GetSources(ref locContext);
            foreach (var sourceItem in sources)
                cbSource.Properties.Items.Add(sourceItem);
        }

        #endregion

        private void tabPane1_SelectedPageChanged(object sender,
            DevExpress.XtraBars.Navigation.SelectedPageChangedEventArgs e)
        {
            barSubItem2.Visibility = BarItemVisibility.Never;

            //if (siteIndexError)
            //{
            //    tabPane1.SelectedPage = tabNavigationPageStandMaster;
            //    siteIndexError = false;
            //    return;
            //}

            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    barButtonItem15.Visibility = BarItemVisibility.Always;
                    barButtonItem17.Visibility = BarItemVisibility.Always;
                    barButtonItem25.Visibility = BarItemVisibility.Always;
                    barButtonItem16.Visibility = BarItemVisibility.Always;
                    barButtonItem57.Visibility = BarItemVisibility.Always;

                    barButtonItem70.Visibility = BarItemVisibility.Never;
                    barButtonItem71.Visibility = BarItemVisibility.Never;
                    barButtonItem72.Visibility = BarItemVisibility.Never;
                    LoadStands();
                    break;
                case "Stand Master":
                    barButtonItem15.Visibility = BarItemVisibility.Never;
                    barButtonItem17.Visibility = BarItemVisibility.Never;
                    barButtonItem57.Visibility = BarItemVisibility.Never;
                    barButtonItem25.Visibility = BarItemVisibility.Always;
                    barButtonItem16.Visibility = BarItemVisibility.Always;

                    barButtonItem70.Visibility = BarItemVisibility.Never;
                    barButtonItem71.Visibility = BarItemVisibility.Never;
                    barButtonItem72.Visibility = BarItemVisibility.Never;
                    LoadStandMaster();
                    break;
                case "Plot Location":
                    barButtonItem15.Visibility = BarItemVisibility.Always;
                    barButtonItem17.Visibility = BarItemVisibility.Never;
                    barButtonItem57.Visibility = BarItemVisibility.Never;
                    barButtonItem25.Visibility = BarItemVisibility.Always;
                    barButtonItem16.Visibility = BarItemVisibility.Always;

                    barButtonItem70.Visibility = BarItemVisibility.Always;
                    barButtonItem71.Visibility = BarItemVisibility.Never;
                    barButtonItem72.Visibility = BarItemVisibility.Never;
                    LoadPlots();
                    break;
                case "Tree Input":
                    barButtonItem15.Visibility = BarItemVisibility.Always;
                    barButtonItem17.Visibility = BarItemVisibility.Never;
                    barButtonItem57.Visibility = BarItemVisibility.Never;
                    barButtonItem25.Visibility = BarItemVisibility.Never;
                    barButtonItem16.Visibility = BarItemVisibility.Always;
                    barSubItem2.Visibility = BarItemVisibility.Always;

                    barButtonItem70.Visibility = BarItemVisibility.Never;
                    barButtonItem71.Visibility = BarItemVisibility.Always;
                    barButtonItem72.Visibility = BarItemVisibility.Never;
                    LoadTrees();
                    break;
                case "Tree Edit":
                    barButtonItem15.Visibility = BarItemVisibility.Always;
                    barButtonItem17.Visibility = BarItemVisibility.Never;
                    barButtonItem57.Visibility = BarItemVisibility.Never;
                    barButtonItem25.Visibility = BarItemVisibility.Never;
                    barButtonItem16.Visibility = BarItemVisibility.Never;

                    barButtonItem70.Visibility = BarItemVisibility.Never;
                    barButtonItem71.Visibility = BarItemVisibility.Never;
                    barButtonItem72.Visibility = BarItemVisibility.Always;
                    LoadSegments(-1, null);
                    break;
                case "Stand Input":
                    barButtonItem15.Visibility = BarItemVisibility.Never;
                    barButtonItem17.Visibility = BarItemVisibility.Never;
                    barButtonItem57.Visibility = BarItemVisibility.Never;
                    barButtonItem25.Visibility = BarItemVisibility.Never;
                    barButtonItem16.Visibility = BarItemVisibility.Never;

                    barButtonItem70.Visibility = BarItemVisibility.Never;
                    barButtonItem71.Visibility = BarItemVisibility.Never;
                    barButtonItem72.Visibility = BarItemVisibility.Never;
                    LoadStandInput();
                    break;
            }
        }

        private void barButtonItemLast_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    standsViewBindingSource.MoveLast();
                    break;
                case "Stand Master":
                    Stand modRec = projectContext.Stands.SingleOrDefault(s => s.StandsId == CurrentStand.StandsId);
                    SetStandProperties(ref modRec);
                    projectContext.SaveChanges();
                    standsViewBindingSource.MoveLast();
                    LoadStandMaster();
                    break;
                case "Plot Location":
                    standsViewBindingSource.MoveLast();
                    LoadPlots();
                    break;
                case "Tree Input":
                    standsViewBindingSource.MoveLast();
                    LoadTrees();
                    break;
                case "Tree Edit":
                    standsViewBindingSource.MoveLast();
                    LoadSegments(-1, null);
                    break;
                case "Stand Input":
                    standsViewBindingSource.MoveLast();
                    LoadStandInput();
                    break;
            }
        }

        private void barButtonItemFirst_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    standsViewBindingSource.MoveFirst();
                    break;
                case "Stand Master":
                    Stand modRec = projectContext.Stands.SingleOrDefault(s => s.StandsId == CurrentStand.StandsId);
                    SetStandProperties(ref modRec);
                    projectContext.SaveChanges();
                    standsViewBindingSource.MoveFirst();
                    LoadStandMaster();
                    break;
                case "Plot Location":
                    standsViewBindingSource.MoveFirst();
                    LoadPlots();
                    break;
                case "Tree Input":
                    standsViewBindingSource.MoveFirst();
                    LoadTrees();
                    break;
                case "Tree Edit":
                    standsViewBindingSource.MoveFirst();
                    LoadSegments(-1, null);
                    break;
                case "Stand Input":
                    standsViewBindingSource.MoveFirst();
                    LoadStandInput();
                    break;
            }
        }

        private void barButtonItemPrev_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    standsViewBindingSource.MovePrevious();
                    break;
                case "Stand Master":
                    Stand modRec = projectContext.Stands.SingleOrDefault(s => s.StandsId == CurrentStand.StandsId);
                    SetStandProperties(ref modRec);
                    projectContext.SaveChanges();
                    standsViewBindingSource.MovePrevious();
                    LoadStandMaster();
                    break;
                case "Plot Location":
                    standsViewBindingSource.MovePrevious();
                    LoadPlots();
                    break;
                case "Tree Input":
                    standsViewBindingSource.MovePrevious();
                    LoadTrees();
                    break;
                case "Tree Edit":
                    standsViewBindingSource.MovePrevious();
                    LoadSegments(-1, null);
                    break;
                case "Stand Input":
                    standsViewBindingSource.MovePrevious();
                    LoadStandInput();
                    break;
            }
        }

        private void barButtonItemNext_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    standsViewBindingSource.MoveNext();
                    break;
                case "Stand Master":
                    Stand modRec = projectContext.Stands.SingleOrDefault(s => s.StandsId == CurrentStand.StandsId);
                    SetStandProperties(ref modRec);
                    projectContext.SaveChanges();
                    standsViewBindingSource.MoveNext();
                    LoadStandMaster();
                    break;
                case "Plot Location":
                    standsViewBindingSource.MoveNext();
                    LoadPlots();
                    break;
                case "Tree Input":
                    standsViewBindingSource.MoveNext();
                    LoadTrees();
                    break;
                case "Tree Edit":
                    standsViewBindingSource.MoveNext();
                    LoadSegments(-1, null);
                    break;
                case "Stand Input":
                    standsViewBindingSource.MoveNext();
                    LoadStandInput();
                    break;
            }
        }

        private void barButtonItemSpeciesTable_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Species
            SpeciesForm frm = new SpeciesForm(ref projectContext, ref locContext, CurrentProject.SpeciesTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Exit
            this.Close();
        }

        private float standCalcAge;
        private float standCalcSite;
        private float standCalcTPA;
        private float standCalcBA;
        private float standCalcTons;
        private float standCalcCcf;
        private float standCalcMbf;
        private float standCalcTotalCcf;
        private float standCalcTotalMbf;
        private float age;
        private float site;
        private float tpa;
        private float ba;
        private float tons;
        private float ccf;
        private float mbf;
        private float acres;
        private float totalCcf;
        private float totalMbf;

        private void gridViewStands_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            GridView view = sender as GridView;

            int SummaryID = Convert.ToInt32((e.Item as GridSummaryItem).Tag);

            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                standCalcAge = 0;
                standCalcSite = 0;
                standCalcTPA = 0;
                standCalcBA = 0;
                standCalcTons = 0;
                standCalcCcf = 0;
                standCalcMbf = 0;
                age = 0;
                acres = 0;
                site = 0;
                tpa = 0;
                ba = 0;
                tons = 0;
                ccf = 0;
                mbf = 0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (SummaryID == 1)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    age += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "MajorAge")) *
                            Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 2)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    site += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "SiteIndex")) *
                             Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 3)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    tpa += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TreesPerAcre")) *
                            Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 4)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    ba += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "BasalAreaPerAcre")) *
                           Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 5)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    tons += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TonsPerAcre")) *
                             Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 6)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    ccf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TotalCcf"));
                }
                if (SummaryID == 7)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    mbf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TotalMbf"));
                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                if (SummaryID == 1)
                {
                    standCalcAge = age / acres;
                    e.TotalValue = standCalcAge;
                }
                if (SummaryID == 2)
                {
                    standCalcSite = site / acres;
                    e.TotalValue = standCalcSite;
                }
                if (SummaryID == 3)
                {
                    standCalcTPA = tpa / acres;
                    e.TotalValue = standCalcTPA;
                }
                if (SummaryID == 4)
                {
                    standCalcBA = ba / acres;
                    e.TotalValue = standCalcBA;
                }
                if (SummaryID == 5)
                {
                    standCalcTons = tons / acres;
                    e.TotalValue = standCalcTons;
                }
                if (SummaryID == 6)
                {
                    standCalcCcf = ccf / acres;
                    e.TotalValue = standCalcCcf;
                }
                if (SummaryID == 7)
                {
                    standCalcMbf = mbf / acres;
                    e.TotalValue = standCalcMbf;
                }
            }
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Print
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Stand Master":
                    break;
                case "Plot Location":
                    gridControlPlots.ShowPrintPreview();
                    break;
                case "Tree Input":
                    gridControlTrees.ShowPrintPreview();
                    break;
                case "Tree Edit":
                    gridControlSegments.ShowPrintPreview();
                    break;
                case "Stand Input":
                    gridControlStandInputOrig.ShowPrintPreview();
                    gridControlStandInputGrown.ShowPrintPreview();
                    break;
            }
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Save
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Stand Master":
                    break;
                case "Plot Location":
                    plotsBindingSource.EndEdit();
                    projectContext.SaveChanges();
                    RemovePlotsThatAreEmpty();
                    break;
                case "Tree Input":
                    treesBindingSource.EndEdit();
                    advBandedGridViewTrees.MoveLast();
                    if (string.IsNullOrEmpty(advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation")))
                    {
                        advBandedGridViewTrees.SelectRow(advBandedGridViewTrees.FocusedRowHandle);
                        Tree xTree = treesBindingSource.Current as Tree;
                        projectContext.Trees.Remove(xTree);
                    }
                    projectContext.SaveChanges();
                    RemoveTreesWithNoSpecies();
                    LoadTreeDataSource();
                    break;
                case "Tree Edit":
                    segmentsBindingSource.EndEdit();
                    projectContext.SaveChanges();
                    break;
                case "Stand Input":
                    break;
            }
        }

        private void RemoveTreesWithNoSpecies()
        {
            List<Tree> coll = projectContext.Trees.Where(t => t.StandsId == CurrentStand.StandsId && t.SpeciesAbbreviation == string.Empty).OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();
            foreach (var item in coll)
            {
                projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(t => t.TreesId == item.TreesId));
                projectContext.Trees.Remove(item);
            }
            projectContext.SaveChanges();
        }

        private void RemovePlotsThatAreEmpty()
        {
            List<Plot> coll = projectContext.Plots.Where(t => t.StandsId == CurrentStand.StandsId && t.UserPlotNumber == string.Empty).ToList();
            foreach (var item in coll)
            {
                projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(t => t.PlotId == item.PlotsId));
                projectContext.Trees.RemoveRange(projectContext.Trees.Where(t => t.PlotId == item.PlotsId));
                projectContext.Plots.Remove(item);
            }
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Column Chooser
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    ColumnChooserForm fHome = new ColumnChooserForm(ref locContext, "ColumnsStand");
                    fHome.StartPosition = FormStartPosition.CenterParent;
                    fHome.ShowDialog();
                    fHome.Dispose();
                    ShowHideStandColumns(true);
                    break;
                case "Plot Location":
                    ColumnChooserForm fPlot = new ColumnChooserForm(ref locContext, "ColumnsPlot");
                    fPlot.StartPosition = FormStartPosition.CenterParent;
                    fPlot.ShowDialog();
                    fPlot.Dispose();
                    ShowHidePlotColumns(true);
                    break;
                case "Tree Input":
                    TreeColumnChooserForm fTree = new TreeColumnChooserForm(ref locContext, "ColumnsTree");
                    fTree.StartPosition = FormStartPosition.CenterParent;
                    fTree.ShowDialog();
                    fTree.Dispose();
                    ShowHideTreeColumns(true);
                    break;
                case "Tree Edit":
                    ColumnChooserForm fSeg = new ColumnChooserForm(ref locContext, "ColumnsSegment");
                    fSeg.StartPosition = FormStartPosition.CenterParent;
                    fSeg.ShowDialog();
                    fSeg.Dispose();
                    ShowHideSegmentColumns(true);
                    break;
                case "Stand Input":
                    break;
            }

        }

        private void ShowHidePlotColumns(bool p)
        {
            List<Projects.DAL.ColumnsPlot> coll = ProjectsBLL.GetPlotColumns(ref locContext);
            foreach (var item in coll)
            {
                try
                {
                    if (item.Show == false)
                        advBandedGridViewPlots.Columns[item.FieldName].Visible = false;
                    else
                        advBandedGridViewPlots.Columns[item.FieldName].Visible = true;
                }
                catch
                {
                }
            }
            if (p)
                advBandedGridViewPlots.RefreshData();
        }

        private void ShowHideTreeColumns(bool p)
        {
            List<Projects.DAL.ColumnsTree> coll = ProjectsBLL.GetTreeColumns(ref locContext);
            foreach (var item in coll)
            {
                try
                {
                    if (item.Show == false)
                        advBandedGridViewTrees.Columns[item.FieldName].Visible = false;
                    else
                        advBandedGridViewTrees.Columns[item.FieldName].Visible = true;
                }
                catch
                {
                }
            }
            if (p)
                advBandedGridViewTrees.RefreshData();
        }

        private void ShowHideSegmentColumns(bool p)
        {
            List<Projects.DAL.ColumnsSegment> coll = ProjectsBLL.GetSegmentColumns(ref locContext);
            foreach (var item in coll)
            {
                try
                {
                    if (item.Show == false)
                        gridViewSegments.Columns[item.FieldName].Visible = false;
                    else
                        gridViewSegments.Columns[item.FieldName].Visible = true;
                }
                catch (Exception ex)
                {
                    //string msg = ex.Message;
                }
            }
            if (p)
                gridViewSegments.RefreshData();
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Aditional Tree Measurements
            if (gridViewStands.SelectedRowsCount == 0)
            {
                XtraMessageBox.Show("You must select a Stand.");
                return;
            }

            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    AdditionalTreeMeasurementForm frmStand = new AdditionalTreeMeasurementForm(ref projectContext, CurrentProject, CurrentStand, null, null);
                    frmStand.StartPosition = FormStartPosition.CenterParent;
                    frmStand.ShowDialog();
                    frmStand.Dispose();
                    break;
                case "Plot Location":
                    AdditionalTreeMeasurementForm frmPlot = new AdditionalTreeMeasurementForm(ref projectContext, CurrentProject, CurrentStand, (Plot)plotsBindingSource.Current, null);
                    frmPlot.StartPosition = FormStartPosition.CenterParent;
                    frmPlot.ShowDialog();
                    frmPlot.Dispose();
                    break;
                case "Tree Input":
                    AdditionalTreeMeasurementForm frmTree = new AdditionalTreeMeasurementForm(ref projectContext, CurrentProject, CurrentStand, null, (Tree)treesBindingSource.Current);
                    frmTree.StartPosition = FormStartPosition.CenterParent;
                    frmTree.ShowDialog();
                    frmTree.Dispose();
                    break;
            }
        }

        private void ShowHideStandColumns(bool p)
        {
            try
            {
                List<Projects.DAL.ColumnsStand> coll = ProjectsBLL.GetStandColumns(ref locContext);
                foreach (var item in coll)
                {
                    if (item.Show == false)
                        gridViewStands.Columns[item.FieldName].Visible = false;
                    else
                        gridViewStands.Columns[item.FieldName].Visible = true;
                }
                if (p)
                    gridViewStands.RefreshData();
            }
            catch
            {

            }
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Filter
            switch (tabPane1.SelectedPage.Caption)
            {
                case "Home":
                    if (barButtonItem17.Caption == "Filter")
                    {
                        gridViewStands.ShowFilterEditor(gridViewStands.FocusedColumn);
                        if (!string.IsNullOrEmpty(gridViewStands.ActiveFilterString))
                            barButtonItem17.Caption = "Clear Filter";
                    }
                    else
                    {
                        barButtonItem17.Caption = "Filter";
                        gridViewStands.ActiveFilterString = string.Empty;
                    }
                    break;
                case "Plot Location":
                    break;
                case "Tree Input":
                    break;
                case "Tree Edit":
                    break;
                case "Stand Input":
                    break;
            }
        }

        private void barSubItemReports_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool bProcess = false;

            if (stands.Count == 0)
            {
                XtraMessageBox.Show(
                    "There are no Stands in this Project.\nYou need to add Stands before running Reports!", "Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if ((gridViewStands.SelectedRowsCount == 0) ||      // this test by itself does not work reliably
                ((gridViewStands.SelectedRowsCount == 1) && (gridViewStands.GetSelectedRows()[0] == -1)))
            {
                DialogResult result = XtraMessageBox.Show("There are no Stands selected.\nWould you like to select all stands?", "Status", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                    gridViewStands.SelectAll();
                else
                    return;
            }

            needWrkSegments = true;
            needWrkTrees = true;

            // Reports
            ReportForm frm = new ReportForm(ref locContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            if (frm.mProcess)
            {
                SplashScreenManager.ShowForm(typeof(WaitForm1));
                SplashScreenManager.Default.SetWaitFormDescription("Processing ...");
                ReportsBLL.DeleteInputTables();
                ReportsBLL.DeleteOutputTables();
                sColl.Clear();
                sColl = ReportsBLL.BuildStandSelection(gridViewStands);
                ReportsBLL.BuildSepStandsSelected(ref projectContext, sColl, Properties.Settings.Default["CurrentProject"].ToString(), ref totalAcres, ref totalPlots, ref totalTrees);
                bool isCombined = frm.bCombined;


                // read the newly built stand in; later try not to write it out
                List<SepStandsSelected> selectedstands = null;
                List<RptCatalogSpeciesVolume> rptCatalog = null;    // later on this is a generic record for all reports
                using (var tdb = new TempDbContext())
                {
                    selectedstands = tdb.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                }


                if (frm.bCombined)
                {
                    if (sColl.Count == 1)
                        isCombined = false;
                }

                /* 
                 * TODO: DominantPriority between stands is TBD. only applicable for combined reports
                 * It will be needed during combined reprots, when generating summary row of combination of multiple stands
                 * currently input data is incorrect.
                 * When sorted out, need to invoke method and extract an inter-segment DomimantFeature
                 * then pass this parameter down to the report as parameter
                 */

                foreach (var item in frm.selectedReports)
                {
                    switch (item)
                    {
                        case "Log Stock - MBF by Species, Sort, Grade, Len, Dia Class":
                        case "Log Stock - CCF by Species, Sort, Grade, Len, Dia Class":
                        case "Log Stock - TON by Species, Sort, Grade, Len, Dia Class":
                            //if (needWrkSegments)
                            //{
                            //    TempBLL.DeleteWrkTreeSegments();
                            TempBLL.DeleteRptLogStockSortGradeMbf();
                            //    ReportsBLL.BuildWrkSegmentsSep(ref projectContext, CurrentProject, sColl, false);
                            //    needWrkSegments = false;
                            //}
                            ReportsBLL.BuildRptLogStockSortGradeMbf(ref projectContext, CurrentProject, sColl, isCombined);
                            ReportsBLL.AddPercentToRptLogStockSortGradeMbf();
                            break;
                        case "Tree Segment Volume":
                            TempBLL.DeleteRptTreeSegmentVolume();
                            ReportsBLL.BuildRptTreeSegmentVolume(ref projectContext, CurrentProject, sColl);
                            break;
                        case "Plot List By Volume":
                        case "Plot Tree List By Volume":
                            TempBLL.DeleteRptPlotTreeListVolume();
                            ReportsBLL.BuildRptPlotTreeListVolume(ref projectContext);
                            break;
                        case "Plot Tree List Details":
                            TempBLL.DeleteRptPlotTreeList();
                            ReportsBLL.BuildRptPlotTreeList(ref projectContext);
                            break;
                        case "Species Summary - Trees, Logs, Volumes, Percents":
                            TempBLL.DeleteRptSpeciesSummaryLogsVolumes();
                            if (isCombined)
                            {
                                //if (needWrkTrees)
                                //{
                                    ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                    needWrkTrees = false;
                                //}
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumesCombined(ref projectContext, CurrentProject, sColl, totalAcres, totalPlots, totalTrees);
                                ReportsBLL.AddPercentToRptSpeciesSummaryLogVolumesCombined();
                            }
                            else
                            {
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumes(ref projectContext, CurrentProject, sColl);
                                ReportsBLL.AddPercentToRptSpeciesSummaryLogVolumes();
                            }
                            break;
                        case "Species Summary - Trees, BA, QMD, Volumes":
                            TempBLL.DeleteRptSpeciesSummaryLogsVolumes();
                            if (isCombined)
                            {
                                //if (needWrkTrees)
                                //{
                                    ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                    needWrkTrees = false;
                                //}
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumesCombined(ref projectContext, CurrentProject, sColl, totalAcres, totalPlots, totalTrees);
                            }
                            else
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumes(ref projectContext, CurrentProject, sColl);
                            break;
                        case "Pole and Piling":
                            TempBLL.DeleteRptPole();
                            TempBLL.DeleteRptPiling();
                            TempBLL.DeleteWrkPolePiling();
                            bProcess = ReportsBLL.BuildWrkPolePiling(ref projectContext, CurrentProject, sColl, true);
                            if (bProcess)
                                ReportsBLL.BuildRptPolesPiling(ref projectContext, CurrentProject);
                            else
                                XtraMessageBox.Show("No Poles/Piling to process.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case "Timber Value Analysis":
                            TempBLL.DeleteRptSpeciesSortTimberEvaluation();
                            TempBLL.DeleteRptCostByLineItem();
                            TempBLL.DeleteRptTransportationReport();
                            TempBLL.DeleteRptPole();
                            TempBLL.DeleteRptPiling();
                            //if (needWrkSegments)
                            //{
                                TempBLL.DeleteWrkTreeSegments();
                                ReportsBLL.BuildWrkSegmentsSep(ref projectContext, CurrentProject, sColl, true);
                            //    needWrkSegments = false;
                            //}
                            ReportsBLL.BuildRptSpeciesSortTimberEvaluation(ref projectContext, CurrentProject, sColl);
                            ReportsBLL.AddTotalsForCountInRptSpeciesSortTimberEvaluation(ref projectContext);
                            ReportsBLL.BuildRptCostByLineItem(ref projectContext, CurrentProject, sColl);
                            ReportsBLL.ProcessReportAsItems(ref projectContext, CurrentProject, sColl);
                            ReportsBLL.BuildRptTransportation(ref projectContext, CurrentProject, sColl);
                            //ReportsBLL.BuildRptPolesPiling(ref projectContext, CurrentProject);
                            break;
                        case "Catalog - Species by Volume":
                            if (Utils.Setting_RptPrepHardWrite)
                                TempBLL.DeleteRptCatalogSpeciesVolume();
                            rptCatalog = ReportsBLL.BuildRptCatalogSpeciesVolumes(ref projectContext, CurrentProject, sColl);
                            break;
                        case "Stand Summary Report":
                            TempBLL.DeleteRptStandTable();
                            //if (needWrkTrees)
                            //{
                            //    ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                            //    needWrkTrees = false;
                            //}
                            if (isCombined)
                            {
                                ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                needWrkTrees = false;
                                ReportsBLL.BuildRptStandSummaryCombined(ref projectContext, CurrentProject, totalAcres, totalPlots, totalTrees);
                            }
                            else
                                ReportsBLL.BuildRptStandSummary(ref projectContext, CurrentProject, sColl);
                            break;
                        case "Statistics of the Sampled Population":
                            TempBLL.DeleteRptSpeciesSummaryLogsVolumes();
                            TempBLL.DeleteRptStatistics();
                            if (isCombined)
                            {
                                //if (needWrkTrees)
                                //{
                                    ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                    needWrkTrees = false;
                                //}
                                TempBLL.DeleteTempPlotSpecies();
                                ReportsBLL.BuildSpeciesByPlotCombined(ref projectContext, CurrentProject, sColl, totalAcres, totalPlots, totalTrees);
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumesCombined(ref projectContext, CurrentProject, sColl, totalAcres, totalPlots, totalTrees);
                                ReportsBLL.BuildRptStatisticsCombinded(ref projectContext, CurrentProject, totalAcres, totalPlots, totalTrees);
                            }
                            else
                            {
                                //if (needWrkTrees)
                                //{
                                    ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                    needWrkTrees = false;
                                //}
                                TempBLL.DeleteTempPlotSpecies();
                                ReportsBLL.BuildSpeciesByPlot(ref projectContext, CurrentProject, sColl);
                                ReportsBLL.BuildRptSpeciesSummaryLogVolumes(ref projectContext, CurrentProject, sColl);
                                //ReportsBLL.BuildRptStatistics(ref projectContext, CurrentProject, (Stand)standsViewBindingSource.Current);
                                ReportsBLL.BuildRptStatistics(ref projectContext, CurrentProject);
                            }
                            break;
                        case "Relationships by Species":
                            TempBLL.DeleteRptStandTable();
                            TempBLL.DeleteRptSpeciesSummaryLogsVolumes();
                            TempBLL.DeleteRptRelationshipsBySpecies1();
                            TempBLL.DeleteRptRelationshipsBySpecies2();
                            TempBLL.DeleteRptRelationshipsBySpecies3();
                            TempBLL.DeleteRptRelationshipsBySpecies4();
                            TempBLL.DeleteRptRelationshipsBySpecies5();
                            TempBLL.DeleteRptRelationshipsBySpecies6();
                            TempBLL.DeleteRptRelationshipsBySpecies7();
                            TempBLL.DeleteRptRelationshipsBySpecies8();
                            TempBLL.DeleteRptRelationshipsBySpecies9();
                            TempBLL.DeleteRptRelationshipsBySpecies10();
                            if (needWrkTrees)
                            {
                                ReportsBLL.BuildWrkTrees(ref projectContext, CurrentProject);
                                needWrkTrees = false;
                            }
                            ReportsBLL.BuildRptStandSummary(ref projectContext, CurrentProject, sColl);
                            ReportsBLL.BuildRptSpeciesSummaryLogVolumes(ref projectContext, CurrentProject, sColl);
                            ReportsBLL.BuildRptRelationshipsBySpecies1(ref projectContext, CurrentProject);
                            ReportsBLL.BuildRptRelationshipsBySpecies2(ref projectContext, CurrentProject);
                            ReportsBLL.BuildRptRelationshipsBySpecies3(ref projectContext, CurrentProject);
                            ReportsBLL.BuildRptRelationshipsBySpecies4(ref projectContext, CurrentProject);
                            break;

                    }
                }
                SplashScreenManager.CloseForm();

                foreach (var item in frm.selectedReports)
                {
                    switch (item)
                    {
                        case "Log Stock - MBF by Species, Sort, Grade, Len, Dia Class":
                            LogStockSortGradeMbfReport rptLogStockSortGradeMbf = new LogStockSortGradeMbfReport();
                            rptLogStockSortGradeMbf.ShowPrintMarginsWarning = false;
                            rptLogStockSortGradeMbf.CreateDocument();
                            rptLogStockSortGradeMbf.ShowPreview();
                            break;
                        case "Log Stock - CCF by Species, Sort, Grade, Len, Dia Class":
                            LogStockSortGradeCcfReport rptLogStockSortGradeCcf = new LogStockSortGradeCcfReport();
                            rptLogStockSortGradeCcf.ShowPrintMarginsWarning = false;
                            rptLogStockSortGradeCcf.CreateDocument();
                            rptLogStockSortGradeCcf.ShowPreview();
                            break;
                        case "Log Stock - TON by Species, Sort, Grade, Len, Dia Class":
                            LogStockSortGradeTonReport rptLogStockSortGradeTon = new LogStockSortGradeTonReport();
                            rptLogStockSortGradeTon.ShowPrintMarginsWarning = false;
                            rptLogStockSortGradeTon.CreateDocument();
                            rptLogStockSortGradeTon.ShowPreview();
                            break;
                        case "Tree Segment Volume":
                            TreeSegmentVolumeReport rptTreeSegmentVolume = new TreeSegmentVolumeReport();
                            rptTreeSegmentVolume.ShowPrintMarginsWarning = false;
                            rptTreeSegmentVolume.CreateDocument();
                            rptTreeSegmentVolume.ShowPreview();
                            //using (ReportPrintTool printTool = new ReportPrintTool(rptTreeSegmentVolume))
                            //{
                            //    printTool.ShowPreviewDialog();
                            //}
                            break;
                        case "Pole and Piling":
                            if (bProcess)
                            {
                                PolePilingReport rptPolePiling = new PolePilingReport();
                                rptPolePiling.ShowPrintMarginsWarning = false;
                                rptPolePiling.CreateDocument();
                                rptPolePiling.ShowPreview();
                            }
                            break;
                        case "Timber Value Analysis":
                            TimberValueAnalysisReport rptTimberValue = new TimberValueAnalysisReport();
                            rptTimberValue.ShowPrintMarginsWarning = false;
                            rptTimberValue.CreateDocument();
                            rptTimberValue.ShowPreview();
                            break;
                        case "Plot Tree List By Volume":
                            PlotTreeListVolumeReport rptPlotTreeListVolume = new PlotTreeListVolumeReport();
                            rptPlotTreeListVolume.ShowPrintMarginsWarning = false;
                            rptPlotTreeListVolume.CreateDocument();
                            rptPlotTreeListVolume.ShowPreview();
                            break;
                        case "Plot List By Volume":
                            PlotListVolumeReport rptPlotListVolume = new PlotListVolumeReport();

                            // tack on DomPriority parameter.
                            // only applicable for combined reports; and edit reports are no longer needed to be combined
                            // rptPlotListVolume.Parameters["DomPriority"].Value = DomPriority;  

                            rptPlotListVolume.ShowPrintMarginsWarning = false;
                            rptPlotListVolume.CreateDocument();
                            rptPlotListVolume.ShowPreview();
                            break;
                        case "Plot Tree List Details":
                            PlotTreeListDetailsReport rptPlotTreeListDetails = new PlotTreeListDetailsReport();
                            rptPlotTreeListDetails.ShowPrintMarginsWarning = false;
                            rptPlotTreeListDetails.CreateDocument();
                            rptPlotTreeListDetails.ShowPreview();
                            break;
                        case "Species Summary - Trees, Logs, Volumes, Percents":
                            if (isCombined)
                            {
                                SpeciesSummaryLogVolumeCombinedReport rptSpeciesSummaryLogVolumeCombined =
                                    new SpeciesSummaryLogVolumeCombinedReport(Properties.Settings.Default["CurrentProject"].ToString(), totalAcres, totalPlots, totalTrees);
                                rptSpeciesSummaryLogVolumeCombined.ShowPrintMarginsWarning = false;
                                rptSpeciesSummaryLogVolumeCombined.CreateDocument();
                                rptSpeciesSummaryLogVolumeCombined.ShowPreview();
                            }
                            else
                            {
                                SpeciesSummaryLogVolumeReport1 rptSpeciesSummaryLogVolume =
                                    new SpeciesSummaryLogVolumeReport1();
                                rptSpeciesSummaryLogVolume.ShowPrintMarginsWarning = false;
                                rptSpeciesSummaryLogVolume.CreateDocument();
                                rptSpeciesSummaryLogVolume.ShowPreview();
                            }
                            break;
                        case "Species Summary - Trees, BA, QMD, Volumes":
                            if (isCombined)
                            {
                                SpeciesSummaryTreesBAQMDVolumeCombinedReport rptSpeciesSummaryTreesBAQMDVolumeCombined =
                                    new SpeciesSummaryTreesBAQMDVolumeCombinedReport(Properties.Settings.Default["CurrentProject"].ToString(), totalAcres, totalPlots, totalTrees);
                                rptSpeciesSummaryTreesBAQMDVolumeCombined.ShowPrintMarginsWarning = false;
                                rptSpeciesSummaryTreesBAQMDVolumeCombined.CreateDocument();
                                rptSpeciesSummaryTreesBAQMDVolumeCombined.ShowPreview();
                            }
                            else
                            {
                                SpeciesSummaryTreesBAQMDVolumeReport rptSpeciesSummaryTreesBAQMDVolume =
                                    new SpeciesSummaryTreesBAQMDVolumeReport();
                                rptSpeciesSummaryTreesBAQMDVolume.ShowPrintMarginsWarning = false;
                                rptSpeciesSummaryTreesBAQMDVolume.CreateDocument();
                                rptSpeciesSummaryTreesBAQMDVolume.ShowPreview();
                            }
                            break;
                        case "Catalog - Species by Volume":
                            CatalogSpeciesByVolumeReport rptCatalogSpeciesByVolume = new CatalogSpeciesByVolumeReport();
                            rptCatalogSpeciesByVolume.StandsData = selectedstands;
                            rptCatalogSpeciesByVolume.DetailsData = rptCatalog;
                            if (sColl.Count > 1)
                                rptCatalogSpeciesByVolume.Parameters["hasOneStand"].Value = false;
                            else
                                rptCatalogSpeciesByVolume.Parameters["hasOneStand"].Value = true;
                            rptCatalogSpeciesByVolume.ShowPrintMarginsWarning = false;
                            rptCatalogSpeciesByVolume.CreateDocument();
                            rptCatalogSpeciesByVolume.ShowPreview();
                            break;
                        case "Stand List Report":
                            StandListReport rptStandList = new StandListReport();
                            rptStandList.ShowPrintMarginsWarning = false;
                            rptStandList.CreateDocument();
                            rptStandList.ShowPreview();
                            break;
                        case "Stand Summary Report":
                            if (isCombined)
                            {
                                StandSummaryCombinedReport rptStandSummaryCombined = new StandSummaryCombinedReport(Properties.Settings.Default["CurrentProject"].ToString(), totalAcres, totalPlots, totalTrees);
                                rptStandSummaryCombined.ShowPrintMarginsWarning = false;
                                rptStandSummaryCombined.CreateDocument();
                                rptStandSummaryCombined.ShowPreview();
                            }
                            else
                            {
                                StandSummaryReport rptStandSummary = new StandSummaryReport();
                                rptStandSummary.ShowPrintMarginsWarning = false;
                                rptStandSummary.CreateDocument();
                                rptStandSummary.ShowPreview();
                            }
                            break;
                        case "Statistics of the Sampled Population":
                            if (isCombined)
                            {
                                StatisticsCombinedReport rptStatisticsCombined = new StatisticsCombinedReport(Properties.Settings.Default["CurrentProject"].ToString(), 
                                    totalAcres, totalPlots, totalTrees);
                                rptStatisticsCombined.ShowPrintMarginsWarning = false;
                                rptStatisticsCombined.CreateDocument();
                                rptStatisticsCombined.ShowPreview();
                            }
                            else
                            {
                                StatisticsReport rptStatistics = new StatisticsReport();
                                rptStatistics.ShowPrintMarginsWarning = false;
                                rptStatistics.CreateDocument();
                                rptStatistics.ShowPreview();
                            }
                            break;
                        case "Relationships by Species":
                            //XtraMessageBox.Show("Relationships by Species is being updated.", "Status", MessageBoxButtons.OK);
                            RelationshipsBySpeciesReport rptRelationshipsBySpecies = new RelationshipsBySpeciesReport();
                            rptRelationshipsBySpecies.ShowPrintMarginsWarning = false;
                            rptRelationshipsBySpecies.CreateDocument();
                            rptRelationshipsBySpecies.ShowPreview();
                            break;
                    }
                }

                if (isCombined)
                {
                    StandListReport rptStandList = new StandListReport();
                    rptStandList.ShowPrintMarginsWarning = false;
                    rptStandList.CreateDocument();
                    rptStandList.ShowPreview();
                }
                //TreeSegmentVolumeSubReport rpt2 = new TreeSegmentVolumeSubReport();
                //rpt2.CreateDocument();

                //rpt1.Pages.AddRange(rpt2.Pages);
                //rpt1.PrintingSystem.ContinuousPageNumbering = true;

                //ReportPrintTool printTool = new ReportPrintTool(rpt1);
                //printTool.ShowPreviewDialog();
            }
            frm.Dispose();
        }

        private void tbBaf1_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit1.Text = Utils.CalculateLimitingDistanceFactor(tbBaf1.Text);
        }

        private void tbBaf2_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit2.Text = Utils.CalculateLimitingDistanceFactor(tbBaf2.Text);
        }

        private void tbBaf3_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit3.Text = Utils.CalculateLimitingDistanceFactor(tbBaf3.Text);
        }

        private void tbBaf4_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit4.Text = Utils.CalculateLimitingDistanceFactor(tbBaf4.Text);
        }

        private void tbBaf5_TextChanged(object sender, EventArgs e)
        {
            tbBafLimit5.Text = Utils.CalculateLimitingDistanceFactor(tbBaf5.Text);
        }

        private void textEdit10_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text,
                tbStripExpansion1.Text);
            if (string.IsNullOrEmpty(tbStripInterval1.Text))
            {
                tbStripWidth1.Text = string.Empty;
                tbStripExpansion1.Text = string.Empty;
            }
        }

        private void textEdit9_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text,
                tbStripExpansion1.Text);
            if (string.IsNullOrEmpty(tbStripInterval1.Text))
            {
                tbStripWidth1.Text = string.Empty;
                tbStripExpansion1.Text = string.Empty;
            }
        }

        private void textEdit8_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text,
                tbStripExpansion2.Text);
            if (string.IsNullOrEmpty(tbStripInterval2.Text))
            {
                tbStripWidth2.Text = string.Empty;
                tbStripExpansion2.Text = string.Empty;
            }
        }

        private void textEdit7_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text,
                tbStripExpansion2.Text);
            if (string.IsNullOrEmpty(tbStripInterval2.Text))
            {
                tbStripWidth2.Text = string.Empty;
                tbStripExpansion2.Text = string.Empty;
            }
        }

        private void textEdit6_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text,
                tbStripExpansion3.Text);
            if (string.IsNullOrEmpty(tbStripInterval3.Text))
            {
                tbStripWidth3.Text = string.Empty;
                tbStripExpansion3.Text = string.Empty;
            }
        }

        private void textEdit5_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text,
                tbStripExpansion3.Text);
            if (string.IsNullOrEmpty(tbStripInterval3.Text))
            {
                tbStripWidth3.Text = string.Empty;
                tbStripExpansion3.Text = string.Empty;
            }
        }

        private void textEdit4_Validated(object sender, EventArgs e)
        {
            tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text,
                tbStripExpansion4.Text);
            if (string.IsNullOrEmpty(tbStripInterval4.Text))
            {
                tbStripWidth4.Text = string.Empty;
                tbStripExpansion4.Text = string.Empty;
            }
        }

        private void textEdit3_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text,
                tbStripExpansion4.Text);
            if (string.IsNullOrEmpty(tbStripInterval4.Text))
            {
                tbStripWidth4.Text = string.Empty;
                tbStripExpansion4.Text = string.Empty;
            }
        }

        private void textEdit2_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text,
                tbStripExpansion5.Text);
            if (string.IsNullOrEmpty(tbStripInterval5.Text))
            {
                tbStripWidth5.Text = string.Empty;
                tbStripExpansion5.Text = string.Empty;
            }
        }

        private void textEdit1_TextChanged(object sender, EventArgs e)
        {
            tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text,
                tbStripExpansion5.Text);
            if (string.IsNullOrEmpty(tbStripInterval5.Text))
            {
                tbStripWidth5.Text = string.Empty;
                tbStripExpansion5.Text = string.Empty;
            }
        }

        private void tbAreaCode1_Validated(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
        }

        private void tbAreaPlotAcres1_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotAcres1.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotAcres1, "Invalid Area Plot Acres was entered."))
                {
                    string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode1.Text, tbAreaPlotAcres1.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, tbAreaPlotAcres1.Text);
                    if (!string.IsNullOrEmpty(plotRadius))
                        tbAreaPlotRadius1.Text = plotRadius;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup1.Text = blowup;
                    tbAreaCode2.Focus();
                }
            }
            else
            {
                tbAreaPlotRadius1.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
        }

        private void tbAreaPlotAcres2_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotAcres2.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotAcres2, "Invalid Area Plot Acres was entered."))
                {
                    string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode2.Text, tbAreaPlotAcres2.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, tbAreaPlotAcres2.Text);
                    if (!string.IsNullOrEmpty(plotRadius))
                        tbAreaPlotRadius2.Text = plotRadius;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup2.Text = blowup;
                    tbAreaCode3.Focus();
                }
            }
            else
            {
                tbAreaPlotRadius2.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
        }

        private void tbAreaSide11_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide11.Text))
            {
                if (!ValidateDouble(false, tbAreaSide11, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide12, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaSide11.Text,
                            tbAreaSide12.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres1.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup1.Text = blowup;
                        tbAreaSide12.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
        }

        private void tbAreaSide12_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide12.Text))
            {
                if (!ValidateDouble(false, tbAreaSide11, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide12, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaSide11.Text,
                            tbAreaSide12.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres1.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup1.Text = blowup;
                        tbAreaCode2.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
        }

        private void tbAreaCode2_Validated(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
        }

        private void tbAreaPlotRadius1_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotRadius1.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotRadius1, "Invalid Area Plot Radius was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode1.Text, tbAreaPlotRadius1.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode1.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres1.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup1.Text = blowup;
                    tbAreaCode2.Focus();
                }
            }
            else
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
        }

        private void tbAreaPlotRadius2_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotRadius2.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotRadius2, "Invalid Area Plot Radius was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaPlotRadius2.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres2.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup2.Text = blowup;
                    tbAreaCode3.Focus();
                }
            }
            else
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
        }

        private void tbAreaSide21_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide21.Text))
            {
                if (!ValidateDouble(false, tbAreaSide21, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide22, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaSide21.Text,
                            tbAreaSide22.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres2.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup2.Text = blowup;
                        tbAreaSide22.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
        }

        private void tbAreaSide22_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide22.Text))
            {
                if (!ValidateDouble(false, tbAreaSide21, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide22, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode2.Text, tbAreaSide21.Text,
                            tbAreaSide22.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode2.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres2.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup2.Text = blowup;
                        tbAreaCode3.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
        }

        private void tbAreaCode3_Validated(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
        }

        private void tbAreaPlotAcres3_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotAcres3.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotAcres3, "Invalid Area Plot Acres was entered."))
                {
                    string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode3.Text, tbAreaPlotAcres3.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, tbAreaPlotAcres3.Text);
                    if (!string.IsNullOrEmpty(plotRadius))
                        tbAreaPlotRadius3.Text = plotRadius;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup3.Text = blowup;
                    tbAreaCode4.Focus();
                }
            }
            else
            {
                tbAreaPlotRadius3.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
        }

        private void tbAreaPlotRadius3_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotRadius3.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotRadius3, "Invalid Area Plot Radius was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaPlotRadius3.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres3.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup3.Text = blowup;
                    tbAreaCode4.Focus();
                }
            }
            else
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
        }

        private void tbAreaSide31_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide31.Text))
            {
                if (!ValidateDouble(false, tbAreaSide31, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide32, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaSide31.Text,
                            tbAreaSide32.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres3.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup3.Text = blowup;
                        tbAreaSide32.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
        }

        private void tbAreaSide32_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide32.Text))
            {
                if (!ValidateDouble(false, tbAreaSide31, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide32, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode3.Text, tbAreaSide31.Text,
                            tbAreaSide32.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode3.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres3.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup3.Text = blowup;
                        tbAreaCode4.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
        }

        private void tbAreaCode4_Validated(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
        }

        private void tbAreaPlotAcres4_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotAcres4.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotAcres4, "Invalid Area Plot Acres was entered."))
                {
                    string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode4.Text, tbAreaPlotAcres4.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, tbAreaPlotAcres4.Text);
                    if (!string.IsNullOrEmpty(plotRadius))
                        tbAreaPlotRadius4.Text = plotRadius;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup4.Text = blowup;
                    tbAreaCode5.Focus();
                }
            }
            else
            {
                tbAreaPlotRadius4.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
        }

        private void tbAreaPlotRadius4_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotRadius4.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotRadius4, "Invalid Area Plot Radius was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaPlotRadius4.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres4.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup4.Text = blowup;
                    tbAreaCode5.Focus();
                }
            }
            else
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
        }

        private void tbAreaSide41_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide41.Text))
            {
                if (!ValidateDouble(false, tbAreaSide41, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide42, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaSide41.Text,
                            tbAreaSide42.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres4.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup4.Text = blowup;
                        tbAreaSide42.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
        }

        private void tbAreaSide42_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide42.Text))
            {
                if (!ValidateDouble(false, tbAreaSide41, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide42, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode4.Text, tbAreaSide41.Text,
                            tbAreaSide42.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode4.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres4.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup4.Text = blowup;
                        tbAreaCode5.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
        }

        private void tbAreaCode5_Validated(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);
        }

        private void tbAreaPlotAcres5_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotAcres5.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotAcres5, "Invalid Area Plot Acres was entered."))
                {
                    string plotRadius = Utils.CalculateAreaPlotRadius(tbAreaCode5.Text, tbAreaPlotAcres5.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, tbAreaPlotAcres5.Text);
                    if (!string.IsNullOrEmpty(plotRadius))
                        tbAreaPlotRadius5.Text = plotRadius;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup5.Text = blowup;
                    tbStripInterval1.Focus();
                }
            }
            else
            {
                tbAreaPlotRadius5.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
        }

        private void tbAreaPlotRadius5_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaPlotRadius5.Text))
            {
                if (!ValidateDouble(false, tbAreaPlotRadius5, "Invalid Area Plot Radius was entered."))
                {
                    string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaPlotRadius5.Text);
                    string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                    if (!string.IsNullOrEmpty(plotAcres))
                        tbAreaPlotAcres5.Text = plotAcres;
                    if (!string.IsNullOrEmpty(blowup))
                        tbAreaBlowup5.Text = blowup;
                    tbStripInterval1.Focus();
                }
            }
            else
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
        }

        private void tbAreaSide51_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide51.Text))
            {
                if (!ValidateDouble(false, tbAreaSide51, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide52, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaSide51.Text,
                            tbAreaSide52.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres5.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup5.Text = blowup;
                        tbAreaSide52.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
        }

        private void tbAreaSide52_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbAreaSide52.Text))
            {
                if (!ValidateDouble(false, tbAreaSide51, "Invalid Area Side1 was entered."))
                {
                    if (!ValidateDouble(false, tbAreaSide52, "Invalid Area Side2 was entered."))
                    {
                        string plotAcres = Utils.CalculateAreaPlotAcres(tbAreaCode5.Text, tbAreaSide51.Text,
                            tbAreaSide52.Text);
                        string blowup = Utils.CalculateAreaBlowup(tbAreaCode5.Text, plotAcres);
                        if (!string.IsNullOrEmpty(plotAcres))
                            tbAreaPlotAcres5.Text = plotAcres;
                        if (!string.IsNullOrEmpty(blowup))
                            tbAreaBlowup5.Text = blowup;
                        tbStripInterval1.Focus();
                    }
                }
            }
            else
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
        }

        private void tbAreaCode1_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode1.Text))
            {
                e.Cancel = false;
                //this.errorProvider1.SetError(tbAreaCode1, string.Empty);
            }
            else
            {
                e.Cancel = true;
                //this.errorProvider1.SetError(tbAreaCode1, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode2_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode2.Text))
            {
                e.Cancel = false;
                //this.errorProvider1.SetError(tbAreaCode2, string.Empty);
            }
            else
            {
                e.Cancel = true;
                //this.errorProvider1.SetError(tbAreaCode2, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode3_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode3.Text))
            {
                e.Cancel = false;
                //this.errorProvider1.SetError(tbAreaCode3, string.Empty);
            }
            else
            {
                e.Cancel = true;
                //this.errorProvider1.SetError(tbAreaCode3, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode4_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode4.Text))
            {
                e.Cancel = false;
                //this.errorProvider1.SetError(tbAreaCode4, string.Empty);
            }
            else
            {
                e.Cancel = true;
                //this.errorProvider1.SetError(tbAreaCode4, "Invalid Area Code was entered.");
            }
        }

        private void tbAreaCode5_Validating(object sender, CancelEventArgs e)
        {
            if (CheckAreaCode(tbAreaCode5.Text))
            {
                e.Cancel = false;
                //this.errorProvider1.SetError(tbAreaCode5, string.Empty);
            }
            else
            {
                e.Cancel = true;
                //this.errorProvider1.SetError(tbAreaCode5, "Invalid Area Code was entered.");
            }
        }

        private void textEdit35_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion1.Text))
            {
                //string s = tbStripExpansion1.Text;
                tbStripInterval1.Text = string.Empty;
                tbStripWidth1.Text = string.Empty;
                //tbStripExpansion1.Text = s;
            }
            tbStripInterval2.Focus();
        }

        private void textEdit34_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion2.Text))
            {
                //string s = tbStripExpansion2.Text;
                tbStripInterval2.Text = string.Empty;
                tbStripWidth2.Text = string.Empty;
                //tbStripExpansion2.Text = s;
            }
            tbStripInterval3.Focus();
        }

        private void textEdit22_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion3.Text))
            {
                //string s = tbStripExpansion3.Text;
                tbStripInterval3.Text = string.Empty;
                tbStripWidth3.Text = string.Empty;
                //tbStripExpansion3.Text = s;
            }
            tbStripInterval4.Focus();
        }

        private void textEdit21_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion4.Text))
            {
                //string s = tbStripExpansion4.Text;
                tbStripInterval4.Text = string.Empty;
                tbStripWidth4.Text = string.Empty;
                //tbStripExpansion4.Text = s;
            }
            tbStripInterval5.Focus();
        }

        private void textEdit19_Validated(object sender, EventArgs e)
        {
            if (!Utils.IsValidFloat(tbStripExpansion5.Text))
            {
                //string s = tbStripExpansion5.Text;
                tbStripInterval5.Text = string.Empty;
                tbStripWidth5.Text = string.Empty;
                //tbStripExpansion5.Text = s;
            }
            //tbAge1.Focus();
        }

        private void tbSpecies_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;

            if (string.IsNullOrEmpty(rtb.Text))
            {
                e.Cancel = false;
                //errorProvider1.SetError(rtb, string.Empty);
            }
            else
            {
                bool exist = ProjectBLL.DoesSpeciesExist(ref MainForm.projectContext,
                        MainForm.CurrentProject.SpeciesTableName, rtb.Text);
                    // projectContext.Species.Any(s => s.TableName == MainForm.CurrentProject.SpeciesTableName && s.Abbreviation == rtb.Text);

                if (exist)
                {
                    e.Cancel = false;
                    calcForMaster = true;
                    //errorProvider1.SetError(rtb, string.Empty);
                }
                else
                {
                    e.Cancel = true;
                    //errorProvider1.SetError(rtb, "Species does not exist");
                }
            }
        }

        private void textEdit9_Validated(object sender, EventArgs e)
        {
            tbStripInterval2.Focus();
        }

        private void textEdit7_Validated(object sender, EventArgs e)
        {
            tbStripInterval3.Focus();
        }

        private void textEdit5_Validated(object sender, EventArgs e)
        {
            tbStripInterval4.Focus();
        }

        private void textEdit3_Validated(object sender, EventArgs e)
        {
            tbStripInterval5.Focus();
        }

        private void textEdit1_Validated(object sender, EventArgs e)
        {
            //tbAge1.Focus();
        }

        private void tbTract_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                }
            }
        }

        private void tbStand_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                }
            }
        }

        private void tbTownship_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 3)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbRange_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 3)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbSection_Validating(object sender, CancelEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(rtb.Text))
            {
                e.Cancel = true;
                dxErrorProvider1.SetError(rtb, "Invalid character was entered");
            }
            else
            {
                if (ValidateString(rtb, "Field is required"))
                    e.Cancel = true;
                else
                {
                    e.Cancel = false;
                    dxErrorProvider1.SetError(rtb, string.Empty);
                    if (Char.IsDigit(rtb.Text[0]) && rtb.Text.Length < 2)
                        rtb.Text = string.Format("0{0}", rtb.Text);
                }
            }
        }

        private void tbBaf1_Validated(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            ValidateDouble(false, rtb, "Invalid characters were entered.");
        }

        private void textEdit10_Validated(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            ValidateDouble(false, rtb, "Invalid characters were entered.");
        }

        private void tbAge1_Validated(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit rtb = sender as DevExpress.XtraEditors.TextEdit;
            ValidateInt(false, rtb, "Invalid characters were entered.");
        }

        private void SetReadOnly(ref DevExpress.XtraEditors.ComboBoxEdit pAreaCode,
            ref DevExpress.XtraEditors.TextEdit pAreaPlotAcres, ref DevExpress.XtraEditors.TextEdit pAreaPlotRadius,
            ref DevExpress.XtraEditors.TextEdit pAreaSide1,
            ref DevExpress.XtraEditors.TextEdit pAreaSide2, ref DevExpress.XtraEditors.TextEdit pAreaBlowup, int p)
        {
            if (string.IsNullOrEmpty(pAreaCode.Text.Trim()))
            {
                pAreaPlotAcres.Properties.ReadOnly = true;
                pAreaPlotRadius.Properties.ReadOnly = true;
                pAreaSide1.Properties.ReadOnly = true;
                pAreaSide2.Properties.ReadOnly = true;
                pAreaBlowup.Properties.ReadOnly = true;
                return;
            }

            switch (pAreaCode.Text[0])
            {
                case 'R':
                    pAreaPlotAcres.Properties.ReadOnly = true;
                    //pAreaPlotAcres.Enabled = false;
                    pAreaPlotRadius.Properties.ReadOnly = false;
                    //pAreaPlotRadius.Enabled = true;
                    //pAreaSide1.Enabled = false;
                    //pAreaSide2.Enabled = false;
                    pAreaSide1.Properties.ReadOnly = true;
                    pAreaSide2.Properties.ReadOnly = true;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaPlotRadius.Focus();
                    break;
                case 'A':
                    pAreaPlotAcres.Properties.ReadOnly = true;
                    //pAreaPlotAcres.Enabled = false;
                    pAreaPlotRadius.Properties.ReadOnly = true;
                    //pAreaPlotRadius.Enabled = false;
                    //pAreaSide1.Enabled = true;
                    //pAreaSide2.Enabled = true;
                    pAreaSide1.Properties.ReadOnly = false;
                    pAreaSide2.Properties.ReadOnly = false;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaSide1.Focus();
                    break;
                case 'F':
                    pAreaPlotAcres.Properties.ReadOnly = false;
                    //pAreaPlotAcres.Enabled = true;
                    pAreaPlotRadius.Properties.ReadOnly = true;
                    //pAreaPlotRadius.Enabled = false;
                    //pAreaSide1.Enabled = false;
                    //pAreaSide2.Enabled = false;
                    pAreaSide1.Properties.ReadOnly = true;
                    pAreaSide2.Properties.ReadOnly = true;
                    pAreaBlowup.Properties.ReadOnly = true;
                    //pAreaBlowup.Enabled = false;
                    pAreaPlotAcres.Focus();
                    break;
                default:
                    break;
            }
        }

        private bool ValidateString(DevExpress.XtraEditors.TextEdit p, string pMsg)
        {
            if (string.IsNullOrEmpty(p.Text))
            {
                //errorProvider1.SetError(p, pMsg);
                p.Focus();
                return true;
            }
            //errorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateDate(DevExpress.XtraEditors.TextEdit p, string pMsg)
        {
            DateTime temp;
            if (DateTime.TryParse(p.Text, out temp))
            {
                //errorProvider1.SetError(p, string.Empty);
                return false;
            }
            else
            {
                //errorProvider1.SetError(p, pMsg);
                p.Focus();
                return true;
            }
        }

        private bool ValidateDouble(bool pRequired, DevExpress.XtraEditors.TextEdit p, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                if (!Utils.IsValidFloat(p.Text))
                {
                    //errorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
                else
                {
                    //errorProvider1.SetError(p, string.Empty);
                    return false;
                }
            }
            else
            {
                if (pRequired)
                {
                    //errorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
            }
            //errorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateInt(bool pRequired, DevExpress.XtraEditors.TextEdit p, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                if (!Utils.IsValidFloat(p.Text))
                {
                    //errorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
                else
                {
                    //errorProvider1.SetError(p, string.Empty);
                    return false;
                }
            }
            else
            {
                if (pRequired)
                {
                    //errorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return true;
                }
            }
            //errorProvider1.SetError(p, string.Empty);
            return false;
        }

        private bool ValidateSpecies(DevExpress.XtraEditors.TextEdit p, string pTable, string pMsg)
        {
            if (!string.IsNullOrEmpty(p.Text))
            {
                bool exist = ProjectBLL.DoesSpeciesExist(ref projectContext, pTable, p.Text.Trim());
                if (!exist)
                {
                    //errorProvider1.SetError(p, pMsg);
                    p.Focus();
                    return false;
                }
                else
                {
                    //errorProvider1.SetError(p, string.Empty);
                    return true;
                }
            }
            return false;
        }

        private bool CheckAreaCode(string p)
        {
            if (string.IsNullOrEmpty(p))
                return true;

            switch (p)
            {
                case "A1":
                case "A2":
                case "A3":
                case "A4":
                case "A5":
                case "F1":
                case "F2":
                case "F3":
                case "F4":
                case "F5":
                case "R1":
                case "R2":
                case "R3":
                case "R4":
                case "R5":
                    return true;
                default:
                    return false;
            }
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Default Tree Input
            TreeColumnChooserForm frm = new TreeColumnChooserForm(ref locContext, "ColumnsTree");
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();

            ShowHideTreeColumns(true);
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Default Report Format
            DefReportForm frm = new DefReportForm(ref projectContext, CurrentProject, CurrentStand);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Default Calculations
            DefCalculationsForm frm = new DefCalculationsForm(ref projectContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem20_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Defaults Statistics
            DefStatisticsForm frm = new DefStatisticsForm(ref locContext, ref projectContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem21_ItemClick(object sender, ItemClickEventArgs e)
        {
            YieldForm frm = new YieldForm(ref projectContext, ProjectDataSource, CurrentProject.YieldTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        private void barButtonItemCostTable_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Cost Table
            CostForm frm = new CostForm(ref projectContext, CurrentProject.CostTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        private void barButtonItemSortTable_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Sort Table
            SortForm frm = new SortForm(ref projectContext, CurrentProject.SortsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        private void barButtonItemGradeTable_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Grade Table
            GradeForm frm = new GradeForm(ref projectContext, CurrentProject.GradesTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        public void AddSkinsMenu(Bar bar)
        {
            bar.Manager.ForceInitialize();
            BarSubItem skinsmenu = new BarSubItem(bar.Manager, "Skins");
            SkinContainerCollection skins = SkinManager.Default.Skins;
            foreach (SkinContainer s in skins)
            {
                BarCheckItem chitem = new BarCheckItem();
                chitem.Caption = s.SkinName;
                chitem.ItemClick += new ItemClickEventHandler(skin_ItemClick);
                chitem.GroupIndex = 1;
                //if (s.SkinName == DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName)
                if (s.SkinName == defaultLookAndFeel1.LookAndFeel.SkinName)
                {
                    chitem.Checked = true;
                }
                skinsmenu.AddItem(chitem);
            }

            //BarSubItem stylesmenu = new BarSubItem(bar.Manager, "Style");
            //string[] styles = System.Enum.GetNames(typeof(DevExpress.LookAndFeel.LookAndFeelStyle));
            //foreach (string s in styles)
            //{
            //    BarCheckItem chitem = new BarCheckItem();
            //    chitem.Caption = s;
            //    chitem.ItemClick += new ItemClickEventHandler(style_ItemClick);
            //    chitem.GroupIndex = 1;
            //    if (s == System.Enum.GetName(typeof(DevExpress.LookAndFeel.LookAndFeelStyle), DevExpress.LookAndFeel.UserLookAndFeel.Default.Style))
            //    {
            //        chitem.Checked = true;
            //    }
            //    stylesmenu.AddItem(chitem);
            //}

            bar.AddItem(skinsmenu);
            //bar.AddItem(stylesmenu);

            BarCheckItem xsitem = new BarCheckItem();
            xsitem.Caption = "Skin SuperACE 20017";
            xsitem.ItemClick += new ItemClickEventHandler(xsitem_ItemClick);
            xsitem.Checked = false;
            skinsmenu.AddItem(xsitem).BeginGroup = true;
        }

        //void style_ItemClick(object sender, ItemClickEventArgs e)
        //{
        //    LookAndFeelStyle style = (LookAndFeelStyle)System.Enum.Parse(typeof(DevExpress.LookAndFeel.LookAndFeelStyle), e.Item.Caption);
        //    DevExpress.LookAndFeel.UserLookAndFeel.Default.SetStyle(style, false, false);
        //    ((BarCheckItem)e.Item).Checked = true;
        //}

        void skin_ItemClick(object sender, ItemClickEventArgs e)
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(e.Item.Caption);
            ((BarCheckItem) e.Item).Checked = true;
        }

        void xsitem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DevExpress.Skins.SkinManager.AllowFormSkins)
            {
                DevExpress.Skins.SkinManager.DisableFormSkins();
            }
            else
            {
                DevExpress.Skins.SkinManager.EnableFormSkins();
            }
            ((BarCheckItem) e.Item).Checked = DevExpress.Skins.SkinManager.AllowFormSkins;
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
        }

        private void barButtonItem22_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateCheckInfo info = null;

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

                try
                {
                    info = ad.CheckForDetailedUpdate();

                }
                catch (DeploymentDownloadException dde)
                {
                    MessageBox.Show(
                        "The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: " +
                        dde.Message);
                    return;
                }
                catch (InvalidDeploymentException ide)
                {
                    MessageBox.Show(
                        "Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: " +
                        ide.Message);
                    return;
                }
                catch (InvalidOperationException ioe)
                {
                    MessageBox.Show(
                        "This application cannot be updated. It is likely not a ClickOnce application. Error: " +
                        ioe.Message);
                    return;
                }

                if (info.UpdateAvailable)
                {
                    Boolean doUpdate = true;

                    if (!info.IsUpdateRequired)
                    {
                        DialogResult dr =
                            MessageBox.Show("An update is available. Would you like to update the application now?",
                                "Update Available", MessageBoxButtons.OKCancel);
                        if (!(DialogResult.OK == dr))
                        {
                            doUpdate = false;
                        }
                    }
                    else
                    {
                        // Display a message that the app MUST reboot. Display the minimum required version.
                        MessageBox.Show("This application has detected a mandatory update from your current " +
                                        "version to version " + info.MinimumRequiredVersion.ToString() +
                                        ". The application will now install the update and restart.",
                            "Update Available", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }

                    if (doUpdate)
                    {
                        try
                        {
                            ad.Update();
                            MessageBox.Show("The application has been upgraded, and will now restart.");
                            Application.Restart();
                        }
                        catch (DeploymentDownloadException dde)
                        {
                            MessageBox.Show(
                                "Cannot install the latest version of the application. \n\nPlease check your network connection, or try again later. Error: " +
                                dde);
                            return;
                        }
                    }
                }
            }
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select Project
            if (tabPane1.SelectedPage.Caption != "Home")
                tabPane1.SelectedPage = tabNavigationPageHome;
            SelectProject(false);
        }

        private void SelectProject(bool IsLoadData)
        {
            ProjectSelectForm frm = new ProjectSelectForm(ref locContext, true);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bool bSelected = frm.bSelected;
            frm.Dispose();
            if (bSelected)
            {
                ///////
                if (projectContext != null)
                    projectContext.Dispose();

                //CurrentProjectLocation = ProjectsBLL.GetLastProject(ref locContext);
                ProjectDataSource = Properties.Settings.Default["CurrentDb"].ToString();
                    // CurrentProjectLocation.ProjectLocation.Trim() + CurrentProjectLocation.ProjectName + ".sdf";
                projectContext = new ProjectDbContext("Data Source = " + ProjectDataSource);
                CurrentProject = ProjectBLL.GetProject(ref projectContext, ProjectDataSource);
                barStaticItem2.Caption = string.Format("[Project: {0}    Location: {1}]",
                    Properties.Settings.Default["CurrentProject"].ToString(),
                    Properties.Settings.Default["CurrentDataDir"].ToString());
                //////
                if (!IsLoadData)
                    LoadStands();
            }
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Edit Project
            ProjectEditForm frm = new ProjectEditForm(ref projectContext, ref locContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowInTaskbar = false;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            // New Project
            ProjectAddForm frm = new ProjectAddForm(ref projectContext, ref locContext);
            //ProjectAddForm frm = new ProjectAddForm(ref ProjectDbContext projectContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowInTaskbar = false;
            frm.ShowDialog();
            frm.Dispose();

            LoadData();
        }

        private void btnLimitingDistance_ItemClick(object sender, ItemClickEventArgs e)
        {
            CalcLDForm frm = new CalcLDForm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.mUpdate)
            {
            }

            frm.Dispose();
        }

        private void btnTreeMeasurementsRelaskop_ItemClick(object sender, ItemClickEventArgs e)
        {
            CalcRDForm frm = new CalcRDForm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.mUpdate)
            {
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["Dbh"], frm.mDbh);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["FormPoint"], frm.mFP);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["ReportedFormFactor"], frm.mFF);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["TopDiameterFractionCode"], frm.mTDF);

                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["BoleHeight"], frm.mBole);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["TotalHeight"], frm.mTotalHeight);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["CrownRatioDisplayCode"], frm.mCrownRatio);

            }
            frm.Dispose();
        }

        private void btnTreeMeasurementsRD100_ItemClick(object sender, ItemClickEventArgs e)
        {
            CalcRD1000Form frm = new CalcRD1000Form();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.mUpdate)
            {
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["Dbh"], frm.mDbh);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["FormPoint"], frm.mFP);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["ReportedFormFactor"], frm.mFF);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["TopDiameterFractionCode"], frm.mTDF);
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["CrownRatioDisplayCode"], frm.mCrownRatio);
            }

            frm.Dispose();
        }

        private void btnTotalHeight_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void btnFormFactorWithoutDistance_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem23_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Add New Stand
            AddNewStand();
        }

        private void AddNewStand()
        {
            StandAddForm frm = new StandAddForm(ref projectContext, ref locContext, CurrentProject);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowInTaskbar = false;
            frm.ShowDialog();
            frm.Dispose();

            LoadStands();
        }

        private void barButtonItem24_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Stand
            sColl.Clear();
            if (tabPane1.SelectedPage.Caption == "Home")
                sColl = sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            else
                return;

            if (sColl.Count == 0)
            {
                XtraMessageBox.Show("You must first select a Stand in order to delete it!", "Warning",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                DialogResult deleteConfirmation =
                    XtraMessageBox.Show(
                        string.Format("You are about to delete {0} Stands. Are you sure?", sColl.Count),
                        "Confirm delete",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (deleteConfirmation != DialogResult.Yes)
                    return;

                if (sColl.Count > 0)
                {
                    foreach (var stand in sColl)
                    {
                        ProjectBLL.DeleteStand(ref projectContext, stand);
                    }
                    LoadStands();
                }
            }
        }

        private void barButtonItem25_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Attached Files
            Stand stand = standsViewBindingSource.Current as Stand; // MainForm.CurrentStand;
            AttachmentsForm frm = new AttachmentsForm(ref projectContext, ProjectDataSource, stand, null);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem26_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Total Height
            CalcTHForm thFrm = new CalcTHForm();
            thFrm.StartPosition = FormStartPosition.CenterParent;
            thFrm.ShowDialog();

            if (thFrm.mUpdate)
            {
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["TotalHeight"], thFrm.mHeight);
            }
            thFrm.Dispose();
        }

        private void barButtonItem27_ItemClick(object sender, ItemClickEventArgs e)
        {
            // FF
            CalcFFForm ffFrm = new CalcFFForm();
            ffFrm.StartPosition = FormStartPosition.CenterParent;
            ffFrm.ShowDialog();

            if (ffFrm.mUpdate)
                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                    advBandedGridViewTrees.Columns["ReportedFormFactor"], ffFrm.mFF);
            ffFrm.Dispose();
        }

        //private string saveTreeInputPlotNumber = string.Empty;
        //private void advBandedGridViewTrees_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        //{
        //    //if (e.Column.FieldName == "PlotNumber")
        //    //{
        //    //    if (string.IsNullOrEmpty(saveTreeInputPlotNumber))
        //    //        saveTreeInputPlotNumber = e.CellValue.ToString();
        //    //    if (e.CellValue.ToString() != saveTreeInputPlotNumber)
        //    //    {
        //    //        DrawLine(e.Cache, e.Bounds, "top", 1);
        //    //        //e.Graphics.DrawLine(Pens.Red, e.Bounds.Right, e.Bounds.Top, e.Bounds.Right, e.Bounds.Bottom);
        //    //        //e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
        //    //        //e.Handled = true;
        //    //        saveTreeInputPlotNumber = e.CellValue.ToString();
        //    //    }
        //    //}
        //    ////DrawLine(e.Cache, e.Bounds, e.RowHandle % 2 == 0 ? "top" : "bottom", 1);
        //}

        //void DrawLine(GraphicsCache cache, Rectangle bounds, string position, int height)
        //{
        //    Rectangle rect = Rectangle.Empty;
        //    if (position == "top")
        //    {
        //        rect.X = bounds.X;
        //        rect.Y = bounds.Y;
        //        rect.Width = bounds.Width * 14;
        //        rect.Height = height;
        //    }
        //    else if (position == "bottom")
        //    {
        //        rect.X = bounds.X;
        //        rect.Y = bounds.Bottom - height;
        //        rect.Width = bounds.Width;
        //        rect.Height = height;
        //    }
        //    cache.FillRectangle(Brushes.Black, rect);
        //}

        //private string saveTreeEditPlotNumber = string.Empty;
        //private void advBandedGridViewSegments_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        //{
        //    //if (e.Column.FieldName == "PlotNumber")
        //    //{
        //    //    if (string.IsNullOrEmpty(saveTreeEditPlotNumber))
        //    //        saveTreeEditPlotNumber = e.CellValue.ToString();
        //    //    if (e.CellValue.ToString() != saveTreeEditPlotNumber)
        //    //    {
        //    //        DrawLine(e.Cache, e.Bounds, "top", 1);
        //    //        //e.Graphics.DrawLine(Pens.Red, e.Bounds.Right, e.Bounds.Top, e.Bounds.Right, e.Bounds.Bottom);
        //    //        //e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
        //    //        //e.Handled = true;
        //    //        saveTreeEditPlotNumber = e.CellValue.ToString();
        //    //    }
        //    //}
        //}

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            gridControlStands.MainView.SaveLayoutToXml(homeLayout);
            gridControlPlots.MainView.SaveLayoutToXml(plotLayout);
            gridControlTrees.MainView.SaveLayoutToXml(inputLayout);
            gridControlSegments.MainView.SaveLayoutToXml(editLayout);
            gridControlStandInputOrig.MainView.SaveLayoutToXml(origLayout);
            gridControlStandInputGrown.MainView.SaveLayoutToXml(grownLayout);

            Settings.Default.ClientSize = this.Size;
            Settings.Default.Location = this.Location;
            Settings.Default["ApplicationSkinName"] = defaultLookAndFeel1.LookAndFeel.SkinName;
            Settings.Default.Save();

            if (projectContext != null)
            {
                projectContext.Dispose();
                projectContext = null;
            }

            if (locContext != null)
            {
                locContext.Dispose();
                locContext = null;
            }
        }

        //private void advBandedGridViewSegments_RowCellStyle(object sender, RowCellStyleEventArgs e)
        //{
        //    //try
        //    //{
        //    //    if (e.Column.FieldName == "SegmentNumber")
        //    //    {
        //    //        var s = (string)advBandedGridViewSegments.GetRowCellValue(e.RowHandle, advBandedGridViewSegments.Columns["SegmentNumber"]).ToString();
        //    //        if (s != "1")
        //    //        {
        //    //            advBandedGridViewSegments.SetRowCellValue(e.RowHandle, "PlotNumber", "");
        //    //            advBandedGridViewSegments.SetRowCellValue(e.RowHandle, "TreeNumber", "");
        //    //            advBandedGridViewSegments.SetRowCellValue(e.RowHandle, "Dbh", "");
        //    //        }
        //    //    }
        //    //}
        //    //catch
        //    //{

        //    //}
        //}

        private void dateEdit1_TextChanged(object sender, EventArgs e)
        {
            if (tabPane1.SelectedPage.Caption != "Stand Master")
                return;

            //DateEdit dp = sender as DateEdit;

            //DateTime d2;
            //DateTime d1;

            //switch (dp.Name)
            //{
            //    case "dateEdit1":
            //        tbOccasion1.Text = "1";
            //        tbInterval1.Text = "0";
            //        break;
            //    case "dateEdit2":
            //        tbOccasion2.Text = "2";
            //        d2 = Convert.ToDateTime(dateEdit2.Text);
            //        d1 = Convert.ToDateTime(dateEdit1.Text);
            //        tbInterval2.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit3":
            //        tbOccasion3.Text = "3";
            //        d2 = Convert.ToDateTime(dateEdit3.Text);
            //        d1 = Convert.ToDateTime(dateEdit2.Text);
            //        tbInterval3.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit4":
            //        tbOccasion4.Text = "4";
            //        d2 = Convert.ToDateTime(dateEdit4.Text);
            //        d1 = Convert.ToDateTime(dateEdit3.Text);
            //        tbInterval4.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit5":
            //        tbOccasion5.Text = "5";
            //        d2 = Convert.ToDateTime(dateEdit5.Text);
            //        d1 = Convert.ToDateTime(dateEdit4.Text);
            //        tbInterval5.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit6":
            //        tbOccasion6.Text = "6";
            //        d2 = Convert.ToDateTime(dateEdit6.Text);
            //        d1 = Convert.ToDateTime(dateEdit5.Text);
            //        tbInterval6.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit7":
            //        tbOccasion7.Text = "7";
            //        d2 = Convert.ToDateTime(dateEdit7.Text);
            //        d1 = Convert.ToDateTime(dateEdit6.Text);
            //        tbInterval7.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit8":
            //        tbOccasion8.Text = "8";
            //        d2 = Convert.ToDateTime(dateEdit8.Text);
            //        d1 = Convert.ToDateTime(dateEdit7.Text);
            //        tbInterval8.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit9":
            //        tbOccasion9.Text = "9";
            //        d2 = Convert.ToDateTime(dateEdit9.Text);
            //        d1 = Convert.ToDateTime(dateEdit8.Text);
            //        tbInterval9.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //    case "dateEdit10":
            //        tbOccasion10.Text = "10";
            //        d2 = Convert.ToDateTime(dateEdit10.Text);
            //        d1 = Convert.ToDateTime(dateEdit9.Text);
            //        tbInterval10.Text = string.Format("{0}", (d2.Year - d1.Year));
            //        break;
            //}
        }

        private void tabPane1_SelectedPageChanging(object sender,
            DevExpress.XtraBars.Navigation.SelectedPageChangingEventArgs e)
        {
            if (CurrentStand != null)
            {
                switch (e.OldPage.Caption)
                {
                    case "Stand Master":
                        if (!CheckStandYardingSystem())
                        {
                            tabPane1.SelectedPage = tabNavigationPageStandMaster;
                            XtraMessageBox.Show("Percent must Total 100 %.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                            gridView4.FocusedRowHandle = 0;
                            gridView4.FocusedColumn = gridView4.Columns["Percent"];
                            gridControl4.Focus();
                        }
                        else
                        {
                            Stand modRec = projectContext.Stands.SingleOrDefault(s => s.StandsId == CurrentStand.StandsId);
                            SetStandProperties(ref modRec);
                            projectContext.SaveChanges();

                            if (calcForMaster)
                            {
                                //SplashScreenManager.ShowForm(typeof(WaitForm1));
                                //SplashScreenManager.Default.SetWaitFormCaption("Processing");
                                //SplashScreenManager.Default.SetWaitFormDescription("Updating Trees for Stand");
                                //Stand smStand = CurrentStand;
                                //CalcTreesForStand(smStand);
                                //SplashScreenManager.Default.SetWaitFormDescription("Updating Plots from Trees");
                                //Utils.UpdateCountPlotsForStand(ref projectContext, CurrentProject, smStand);
                                //Utils.UpdatePlotsForStand(ref projectContext, CurrentProject, smStand);
                                //SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Totals from Trees");
                                //Utils.UpdateTotalsForStand(ref projectContext, CurrentProject, smStand, false);
                                //SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Stand Input from Tree Input");
                                //bool smResult = ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject, smStand, true);
                                //SplashScreenManager.CloseForm();

                                //if (!smResult)
                                //{
                                //    string errors = string.Empty;
                                //    List<string> varList = new List<string>();
                                //    var result = (from t in projectContext.TmpStandInputExceptions
                                //                  where t.StandsId == smStand.StandsId
                                //                  group t by t.Species into myGroup
                                //                  orderby myGroup.Key
                                //                  select new { Species = myGroup.Key }).ToList();
                                //    foreach (var item in result)
                                //        errors += string.Format("{0},", item.Species);

                                //    errors = errors.TrimEnd(',');
                                //    XtraMessageBox.Show(string.Format("Site Index is not defined for the following species ({0}) in Stand Master.", errors), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //    siteIndexError = true;

                                //    e.Cancel = true;
                                //}
                            }
                        }
                        break;
                    case "Tree Input":
                        if (e.Page.Caption == "Stand Master" && siteIndexError)
                        {
                            siteIndexError = false;
                        }
                        else
                        {
                            if (needToUpdatePlotsStands)
                            {
                                RemoveTreesWithNoSpecies();
                                SplashScreenManager.ShowForm(typeof(WaitForm1));
                                SplashScreenManager.Default.SetWaitFormCaption("Processing");
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Plots from Trees");
                                //float tAcres = 0;
                                Stand tiStand = CurrentStand;
                                Utils.UpdateCountPlotsForStand(ref projectContext, CurrentProject, tiStand);
                                Utils.UpdatePlotsForStand(ref projectContext, CurrentProject, tiStand);
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Totals from Trees");
                                Utils.UpdateTotalsForStand(ref projectContext, CurrentProject, tiStand, false);
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Stand Input from Tree Input");
                                bool tiResult = ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject, tiStand, true);
                                SplashScreenManager.CloseForm();

                                if (!tiResult)
                                {
                                    string errors = string.Empty;
                                    List<string> varList = new List<string>();
                                    var result = (from t in projectContext.TmpStandInputExceptions
                                                  group t by t.Species into myGroup
                                                  orderby myGroup.Key
                                                  select new { Species = myGroup.Key }).ToList();
                                    foreach (var item in result)
                                        errors += string.Format("{0},", item.Species);

                                    errors = errors.TrimEnd(',');
                                    XtraMessageBox.Show(string.Format("Site Index is not defined for the following species ({0}) in Stand Master.", errors), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    siteIndexError = true;

                                    e.Cancel = true;
                                }
                                needToUpdatePlotsStands = false;
                            }
                        }
                        break;
                    case "Tree Edit":
                        if (e.Page.Caption == "Stand Master" && siteIndexError)
                        {
                            siteIndexError = false;
                        }
                        else
                        {
                            if (needToUpdatePlotsStands)
                            {
                                RemoveTreesWithNoSpecies();
                                SplashScreenManager.ShowForm(typeof(WaitForm1));
                                SplashScreenManager.Default.SetWaitFormCaption("Processing");
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Plots from Trees");
                                //float tAcres = 0;
                                Stand teStand = CurrentStand;
                                Utils.UpdateCountPlotsForStand(ref projectContext, CurrentProject, teStand);
                                Utils.UpdatePlotsForStand(ref projectContext, CurrentProject, teStand);
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Totals from Trees");
                                Utils.UpdateTotalsForStand(ref projectContext, CurrentProject, teStand, false);
                                SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Stand Input from Tree Edit");
                                bool teResult = ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject, teStand, true);
                                SplashScreenManager.CloseForm();

                                if (!teResult)
                                {
                                    string errors = string.Empty;
                                    List<string> varList = new List<string>();
                                    var result = (from t in projectContext.TmpStandInputExceptions
                                                  group t by t.Species into myGroup
                                                  orderby myGroup.Key
                                                  select new { Species = myGroup.Key }).ToList();
                                    foreach (var item in result)
                                        errors += string.Format("{0},", item.Species);

                                    errors = errors.TrimEnd(',');
                                    XtraMessageBox.Show(string.Format("Site Index is not defined for the following species ({0}) in Stand Master.", errors), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    siteIndexError = true;
                                    e.Cancel = true;
                                }
                                needToUpdatePlotsStands = false;
                            }
                        }
                        break;
                }
                if (e.OldPage.Caption == "Stand Master")
                {
                    SplashScreenManager.ShowForm(typeof(WaitForm1));
                    SplashScreenManager.Default.SetWaitFormCaption("Processing");
                    //SplashScreenManager.Default.SetWaitFormDescription("Updating Plots from Trees");
                    Stand teStand = CurrentStand;
                    //Utils.UpdateCountPlotsForStand(ref projectContext, CurrentProject, teStand);
                    //Utils.UpdatePlotsForStand(ref projectContext, CurrentProject, teStand);
                    //SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Totals from Trees");
                    //Utils.UpdateTotalsForStand(ref projectContext, CurrentProject, teStand, false);
                    SplashScreenManager.Default.SetWaitFormDescription("Updating Stand Stand Input from Tree Edit");
                    bool teResult = ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject, teStand, true);
                    SplashScreenManager.CloseForm();

                    if (!teResult)
                    {
                        string errors = string.Empty;
                        List<string> varList = new List<string>();
                        var result = (from t in projectContext.TmpStandInputExceptions
                                      group t by t.Species into myGroup
                                      orderby myGroup.Key
                                      select new { Species = myGroup.Key }).ToList();
                        foreach (var item in result)
                            errors += string.Format("{0},", item.Species);

                        errors = errors.TrimEnd(',');
                        XtraMessageBox.Show(string.Format("Site Index is not defined for the following species ({0}) in Stand Master.", errors), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //tabPane1.SelectedPage = tabNavigationPageStandMaster;
                        e.Cancel = true;
                    }
                }
            }
        }

        private void CalcTreesForStand(Stand pStand)
        {
            Stand standItem = pStand;
            Project.DAL.Project projectItem = CurrentProject;

            Calc.SetCalcContextProjectStand(ref projectContext, MainForm.CurrentProject, ref standItem);
            Calc.projectDataSource = ProjectDataSource;

            List<Tree> tColl =
                projectContext.Trees.OrderBy(t => t.PlotNumber)
                    .ThenBy(t => t.TreeNumber)
                    .Where(t => t.StandsId == standItem.StandsId)
                    .ToList();
            foreach (var treeItem in tColl)
                DoCalcForTree(true, treeItem, false);
        }

        private void GetStandProperties()
        {
            tbProject.Text = Properties.Settings.Default["CurrentProject"].ToString();
                // CurrentStand.Project.ProjectName;
            cbState.Text = CurrentStand.State;
            cbCounty.Text = CurrentStand.County;
            tbTract.Text = CurrentStand.TractName;
            tbStand.Text = CurrentStand.StandName;
            tbTownship.Text = CurrentStand.Township;
            tbRange.Text = CurrentStand.Range;
            tbSection.Text = CurrentStand.Section;
            tbNetAcres.Text = CurrentStand.NetGeographicAcres.ToString();
            tbPlots.Text = CurrentStand.Plots.ToString();
            cbSource.Text = CurrentStand.Source;

            LoadStandHaulingSource();
            LoadStandAgeDataSource();
            LoadStandPermPlotDataSource();
            LoadStandYardingSystemDataSource();

            DateTime d1 = (DateTime) CurrentStand.DateOfStandData;
            tbInputDate.Text = d1.ToShortDateString();
            DateTime d2 = (DateTime) CurrentStand.GrownToDate;
            tbGrownToDate.Text = d2.ToShortDateString();

            if (CurrentStand.SpeciesTableName != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.SpeciesTableName))
                    cbSpeciesTable.Text = CurrentStand.SpeciesTableName;
            }
            else
                cbSpeciesTable.Text = CurrentProject.SpeciesTableName; // CurrentStand.Project.SpeciesTableName;
            if (CurrentStand.SortTableName != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.SortTableName))
                    cbSortTable.Text = CurrentStand.SortTableName;
            }
            else
                cbSortTable.Text = CurrentProject.SortsTableName;
            if (CurrentStand.GradeTableName != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.GradeTableName))
                    cbGradeTable.Text = CurrentStand.GradeTableName;
            }
            else
                cbGradeTable.Text = CurrentProject.GradesTableName;
            if (CurrentStand.PriceTableName != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.PriceTableName))
                    cbPriceTable.Text = CurrentStand.PriceTableName;
            }
            else
                cbPriceTable.Text = CurrentProject.PriceTableName;
            if (CurrentStand.CostTableName != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.CostTableName))
                    cbCostTable.Text = CurrentStand.CostTableName;
            }
            else
                cbCostTable.Text = CurrentProject.CostTableName;

            if (CurrentStand.NonStocked != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.NonStocked))
                    cbNonStockedTable.Text = CurrentStand.NonStocked;
            }
            if (CurrentStand.NonTimbered != null)
            {
                if (!string.IsNullOrEmpty(CurrentStand.NonTimbered))
                    cbNonTimberedTable.Text = CurrentStand.NonTimbered;
            }
            if (CurrentStand.Harvest != null)
            {
                cbHarvestTable.Text = CurrentStand.Harvest;
            }
            else
            {
                cbHarvestTable.Text = string.Empty;
            }
            if (CurrentStand.GrowthModelTableName != null)
            {
                cbGrowthModel.Text = CurrentStand.GrowthModelTableName;
            }
            else
            {
                cbGrowthModel.Text = string.Empty;
            }

            tbAutoSegmentLength.Text = CurrentStand.AutoSegmentLength.ToString();

            tbBaf1.Text = CurrentStand.Baf1.ToString();
            tbBaf2.Text = CurrentStand.Baf2.ToString();
            tbBaf3.Text = CurrentStand.Baf3.ToString();
            tbBaf4.Text = CurrentStand.Baf4.ToString();
            tbBaf5.Text = CurrentStand.Baf5.ToString();

            tbBafLimit1.Text = CurrentStand.BafPlotRadius1.ToString();
            tbBafLimit2.Text = CurrentStand.BafPlotRadius2.ToString();
            tbBafLimit3.Text = CurrentStand.BafPlotRadius3.ToString();
            tbBafLimit4.Text = CurrentStand.BafPlotRadius4.ToString();
            tbBafLimit5.Text = CurrentStand.BafPlotRadius5.ToString();
            tbBafLimit1.Text = Utils.CalculateLimitingDistanceFactor(tbBaf1.Text);
            tbBafLimit2.Text = Utils.CalculateLimitingDistanceFactor(tbBaf2.Text);
            tbBafLimit3.Text = Utils.CalculateLimitingDistanceFactor(tbBaf3.Text);
            tbBafLimit4.Text = Utils.CalculateLimitingDistanceFactor(tbBaf4.Text);
            tbBafLimit5.Text = Utils.CalculateLimitingDistanceFactor(tbBaf5.Text);

            tbSpecies11.Text = CurrentStand.Species11;
            tbSpecies12.Text = CurrentStand.Species12;
            tbSpecies13.Text = CurrentStand.Species13;
            tbSiteIndex1.Text = CurrentStand.SiteIndex1.ToString();
            tbSpecies21.Text = CurrentStand.Species21;
            tbSpecies22.Text = CurrentStand.Species22;
            tbSpecies23.Text = CurrentStand.Species23;
            tbSiteIndex2.Text = CurrentStand.SiteIndex2.ToString();
            tbSpecies31.Text = CurrentStand.Species31;
            tbSpecies32.Text = CurrentStand.Species32;
            tbSpecies33.Text = CurrentStand.Species33;
            tbSiteIndex3.Text = CurrentStand.SiteIndex3.ToString();
            tbSpecies41.Text = CurrentStand.Species41;
            tbSpecies42.Text = CurrentStand.Species42;
            tbSpecies43.Text = CurrentStand.Species43;
            tbSiteIndex4.Text = CurrentStand.SiteIndex4.ToString();

            tbAreaCode1.Text = CurrentStand.AreaCode1;
            tbAreaPlotAcres1.Text = string.Format("{0:0.000}", CurrentStand.AreaPlotAcres1);
            tbAreaPlotRadius1.Text = string.Format("{0:0.00}", CurrentStand.AreaPlotRadius1);
            tbAreaSide11.Text = string.Format("{0:0.00}", CurrentStand.AreaSide11);
            tbAreaSide12.Text = string.Format("{0:0.00}", CurrentStand.AreaSide12);
            tbAreaBlowup1.Text = string.Format("{0:0.00}", CurrentStand.AreaBlowup1);

            tbAreaCode2.Text = CurrentStand.AreaCode2;
            tbAreaPlotAcres2.Text = string.Format("{0:0.000}", CurrentStand.AreaPlotAcres2);
            tbAreaPlotRadius2.Text = string.Format("{0:0.00}", CurrentStand.AreaPlotRadius2);
            tbAreaSide21.Text = string.Format("{0:0.00}", CurrentStand.AreaSide21);
            tbAreaSide22.Text = string.Format("{0:0.00}", CurrentStand.AreaSide22);
            tbAreaBlowup2.Text = string.Format("{0:0.00}", CurrentStand.AreaBlowup2);

            tbAreaCode3.Text = CurrentStand.AreaCode3;
            tbAreaPlotAcres3.Text = string.Format("{0:0.000}", CurrentStand.AreaPlotAcres3);
            tbAreaPlotRadius3.Text = string.Format("{0:0.00}", CurrentStand.AreaPlotRadius3);
            tbAreaSide31.Text = string.Format("{0:0.00}", CurrentStand.AreaSide31);
            tbAreaSide32.Text = string.Format("{0:0.00}", CurrentStand.AreaSide32);
            tbAreaBlowup3.Text = string.Format("{0:0.00}", CurrentStand.AreaBlowup3);

            tbAreaCode4.Text = CurrentStand.AreaCode4;
            tbAreaPlotAcres4.Text = string.Format("{0:0.000}", CurrentStand.AreaPlotAcres4);
            tbAreaPlotRadius4.Text = string.Format("{0:0.00}", CurrentStand.AreaPlotRadius4);
            tbAreaSide41.Text = string.Format("{0:0.00}", CurrentStand.AreaSide41);
            tbAreaSide42.Text = string.Format("{0:0.00}", CurrentStand.AreaSide42);
            tbAreaBlowup4.Text = string.Format("{0:0.00}", CurrentStand.AreaBlowup4);

            tbAreaCode5.Text = CurrentStand.AreaCode5;
            tbAreaPlotAcres5.Text = string.Format("{0:0.000}", CurrentStand.AreaPlotAcres5);
            tbAreaPlotRadius5.Text = string.Format("{0:0.00}", CurrentStand.AreaPlotRadius5);
            tbAreaSide51.Text = string.Format("{0:0.00}", CurrentStand.AreaSide51);
            tbAreaSide52.Text = string.Format("{0:0.00}", CurrentStand.AreaSide52);
            tbAreaBlowup5.Text = string.Format("{0:0.00}", CurrentStand.AreaBlowup5);

            tbStripInterval1.Text = CurrentStand.Strip1.ToString();
            tbStripWidth1.Text = CurrentStand.StripWidth1.ToString();
            tbStripExpansion1.Text = CurrentStand.StripBlowup1.ToString();

            tbStripInterval2.Text = CurrentStand.Strip2.ToString();
            tbStripWidth2.Text = CurrentStand.StripWidth2.ToString();
            tbStripExpansion2.Text = CurrentStand.StripBlowup2.ToString();

            tbStripInterval3.Text = CurrentStand.Strip3.ToString();
            tbStripWidth3.Text = CurrentStand.StripWidth3.ToString();
            tbStripExpansion3.Text = CurrentStand.StripBlowup3.ToString();

            tbStripInterval4.Text = CurrentStand.Strip4.ToString();
            tbStripWidth4.Text = CurrentStand.StripWidth4.ToString();
            tbStripExpansion4.Text = CurrentStand.StripBlowup4.ToString();

            tbStripInterval5.Text = CurrentStand.Strip5.ToString();
            tbStripWidth5.Text = CurrentStand.StripWidth5.ToString();
            tbStripExpansion5.Text = CurrentStand.StripBlowup5.ToString();

            tbStripExpansion1.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval1.Text, tbStripWidth1.Text,
                tbStripExpansion1.Text);
            tbStripExpansion2.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval2.Text, tbStripWidth2.Text,
                tbStripExpansion2.Text);
            tbStripExpansion3.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval3.Text, tbStripWidth3.Text,
                tbStripExpansion3.Text);
            tbStripExpansion4.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval4.Text, tbStripWidth4.Text,
                tbStripExpansion4.Text);
            tbStripExpansion5.Text = Utils.CalculateStripCruiseExpansion(tbStripInterval5.Text, tbStripWidth5.Text,
                tbStripExpansion5.Text);

            //tbAge1.Text = CurrentStand.Age1.ToString();
            //tbAge2.Text = CurrentStand.Age2.ToString();
            //tbAge3.Text = CurrentStand.Age3.ToString();
            //tbAge4.Text = CurrentStand.Age4.ToString();
            //tbAge5.Text = CurrentStand.Age5.ToString();
            //tbAge6.Text = CurrentStand.Age6.ToString();
            //tbAge7.Text = CurrentStand.Age7.ToString();
            //tbAge8.Text = CurrentStand.Age8.ToString();
            //tbAge9.Text = CurrentStand.Age9.ToString();

            //if (CurrentStand.PermPlotOccasion1 != null)
            //    tbOccasion1.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion1);
            //if (CurrentStand.PermPlotOccasion2 != null)
            //    tbOccasion2.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion2);
            //if (CurrentStand.PermPlotOccasion3 != null)
            //    tbOccasion3.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion3);
            //if (CurrentStand.PermPlotOccasion4 != null)
            //    tbOccasion4.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion4);
            //if (CurrentStand.PermPlotOccasion5 != null)
            //    tbOccasion5.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion5);
            //if (CurrentStand.PermPlotOccasion6 != null)
            //    tbOccasion6.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion6);
            //if (CurrentStand.PermPlotOccasion7 != null)
            //    tbOccasion7.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion7);
            //if (CurrentStand.PermPlotOccasion8 != null)
            //    tbOccasion8.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion8);
            //if (CurrentStand.PermPlotOccasion9 != null)
            //    tbOccasion9.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion9);
            //if (CurrentStand.PermPlotOccasion10 != null)
            //    tbOccasion10.Text = string.Format("{0:#}", CurrentStand.PermPlotOccasion10);

            //if (CurrentStand.PermPlotInterval1 != null)
            //    tbInterval1.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval1);
            //if (CurrentStand.PermPlotInterval2 != null)
            //    tbInterval2.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval2);
            //if (CurrentStand.PermPlotInterval3 != null)
            //    tbInterval3.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval3);
            //if (CurrentStand.PermPlotInterval4 != null)
            //    tbInterval4.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval4);
            //if (CurrentStand.PermPlotInterval5 != null)
            //    tbInterval5.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval5);
            //if (CurrentStand.PermPlotInterval6 != null)
            //    tbInterval6.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval6);
            //if (CurrentStand.PermPlotInterval7 != null)
            //    tbInterval7.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval7);
            //if (CurrentStand.PermPlotInterval8 != null)
            //    tbInterval8.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval8);
            //if (CurrentStand.PermPlotInterval9 != null)
            //    tbInterval9.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval9);
            //if (CurrentStand.PermPlotInterval10 != null)
            //    tbInterval10.Text = string.Format("{0:#}", CurrentStand.PermPlotInterval10);

            //if (CurrentStand.PermPlotDate1 != null)
            //    dateEdit1.Text = ((DateTime) CurrentStand.PermPlotDate1).ToShortDateString();
            //if (CurrentStand.PermPlotDate2 != null)
            //    dateEdit2.Text = ((DateTime) CurrentStand.PermPlotDate2).ToShortDateString();
            //if (CurrentStand.PermPlotDate3 != null)
            //    dateEdit3.Text = ((DateTime) CurrentStand.PermPlotDate3).ToShortDateString();
            //if (CurrentStand.PermPlotDate4 != null)
            //    dateEdit4.Text = ((DateTime) CurrentStand.PermPlotDate4).ToShortDateString();
            //if (CurrentStand.PermPlotDate5 != null)
            //    dateEdit5.Text = ((DateTime) CurrentStand.PermPlotDate5).ToShortDateString();
            //if (CurrentStand.PermPlotDate6 != null)
            //    dateEdit6.Text = ((DateTime) CurrentStand.PermPlotDate6).ToShortDateString();
            //if (CurrentStand.PermPlotDate7 != null)
            //    dateEdit7.Text = ((DateTime) CurrentStand.PermPlotDate7).ToShortDateString();
            //if (CurrentStand.PermPlotDate8 != null)
            //    dateEdit8.Text = ((DateTime) CurrentStand.PermPlotDate8).ToShortDateString();
            //if (CurrentStand.PermPlotDate9 != null)
            //    dateEdit9.Text = ((DateTime) CurrentStand.PermPlotDate9).ToShortDateString();
            //if (CurrentStand.PermPlotDate10 != null)
            //    dateEdit10.Text = ((DateTime) CurrentStand.PermPlotDate10).ToShortDateString();
        }

        private void SetStandProperties(ref Stand modRec)
        {
            gridView1.PostEditor();
            gridView1.UpdateCurrentRow();
            haulingBindingSource.EndEdit();
            gridView2.PostEditor();
            gridView2.UpdateCurrentRow();
            standAgeBindingSource.EndEdit();
            gridView3.PostEditor();
            gridView3.UpdateCurrentRow();
            standPermPlotBindingSource.EndEdit();
            gridView4.PostEditor();
            gridView4.UpdateCurrentRow();
            standYardingSystemBindingSource.EndEdit();

            modRec.State = string.IsNullOrEmpty(this.cbState.Text.Trim()) ? string.Empty : this.cbState.Text.Trim();
            modRec.County = string.IsNullOrEmpty(this.cbCounty.Text.Trim()) ? string.Empty : this.cbCounty.Text.Trim();
            modRec.TractName = string.IsNullOrEmpty(this.tbTract.Text.Trim()) ? string.Empty : this.tbTract.Text.Trim();
            modRec.TractGroup = string.IsNullOrEmpty(this.tbTract.Text.Trim()) ? string.Empty : this.tbTract.Text.Trim();
            modRec.StandName = string.IsNullOrEmpty(this.tbStand.Text.Trim()) ? string.Empty : this.tbStand.Text.Trim();
            modRec.Township = string.IsNullOrEmpty(this.tbTownship.Text.Trim())
                ? string.Empty
                : this.tbTownship.Text.Trim();
            modRec.Range = string.IsNullOrEmpty(this.tbRange.Text.Trim()) ? string.Empty : this.tbRange.Text.Trim();
            modRec.Section = string.IsNullOrEmpty(this.tbSection.Text.Trim())
                ? string.Empty
                : this.tbSection.Text.Trim();
            modRec.NetGeographicAcres = Utils.ConvertToDouble(this.tbNetAcres.Text.Trim());
            modRec.Plots = Utils.ConvertToInt(this.tbPlots.Text.Trim());
            modRec.Source = string.IsNullOrEmpty(this.cbSource.Text.Trim()) ? string.Empty : this.cbSource.Text.Trim();
            modRec.TractGroup = modRec.TractName;

            modRec.DateOfStandData = Convert.ToDateTime(tbInputDate.Text);
            modRec.GrownToDate = Convert.ToDateTime(tbGrownToDate.Text);
            //this.stand.GrownToDate = Convert.ToDateTime(tbGrownToMonth.Text);
            //modRec.GrownToMonth = Utils.ConvertToInt(radTextBoxMasterGrownToDate.Text);
            //modRec.GrownToYear = Utils.ConvertToInt(radTextBoxMasterGrownToYear.Text);

            modRec.AutoSegmentLength = (short) Utils.ConvertToInt(tbAutoSegmentLength.Text);

            modRec.Harvest = string.IsNullOrEmpty(this.cbHarvestTable.Text.Trim())
                ? string.Empty
                : this.cbHarvestTable.Text.Trim();
            modRec.GrowthModelTableName = string.IsNullOrEmpty(this.cbGrowthModel.Text.Trim())
                ? string.Empty
                : this.cbGrowthModel.Text.Trim();

            modRec.Species11 = string.IsNullOrEmpty(this.tbSpecies11.Text.Trim())
                ? string.Empty
                : this.tbSpecies11.Text.Trim();
            modRec.Species12 = string.IsNullOrEmpty(this.tbSpecies12.Text.Trim())
                ? string.Empty
                : this.tbSpecies12.Text.Trim();
            modRec.Species13 = string.IsNullOrEmpty(this.tbSpecies13.Text.Trim())
                ? string.Empty
                : this.tbSpecies13.Text.Trim();
            modRec.SiteIndex1 = (short) Utils.ConvertToInt(tbSiteIndex1.Text);

            modRec.Species21 = string.IsNullOrEmpty(this.tbSpecies21.Text.Trim())
                ? string.Empty
                : this.tbSpecies21.Text.Trim();
            modRec.Species22 = string.IsNullOrEmpty(this.tbSpecies22.Text.Trim())
                ? string.Empty
                : this.tbSpecies22.Text.Trim();
            modRec.Species23 = string.IsNullOrEmpty(this.tbSpecies23.Text.Trim())
                ? string.Empty
                : this.tbSpecies23.Text.Trim();
            modRec.SiteIndex2 = (short) Utils.ConvertToInt(tbSiteIndex2.Text);

            modRec.Species31 = string.IsNullOrEmpty(this.tbSpecies31.Text.Trim())
                ? string.Empty
                : this.tbSpecies31.Text.Trim();
            modRec.Species32 = string.IsNullOrEmpty(this.tbSpecies32.Text.Trim())
                ? string.Empty
                : this.tbSpecies32.Text.Trim();
            modRec.Species33 = string.IsNullOrEmpty(this.tbSpecies33.Text.Trim())
                ? string.Empty
                : this.tbSpecies33.Text.Trim();
            modRec.SiteIndex3 = (short) Utils.ConvertToInt(tbSiteIndex3.Text);

            modRec.Species41 = string.IsNullOrEmpty(this.tbSpecies41.Text.Trim())
                ? string.Empty
                : this.tbSpecies41.Text.Trim();
            modRec.Species42 = string.IsNullOrEmpty(this.tbSpecies42.Text.Trim())
                ? string.Empty
                : this.tbSpecies42.Text.Trim();
            modRec.Species43 = string.IsNullOrEmpty(this.tbSpecies43.Text.Trim())
                ? string.Empty
                : this.tbSpecies43.Text.Trim();
            modRec.SiteIndex4 = (short) Utils.ConvertToInt(tbSiteIndex4.Text);

            //modRec.Age1 = (short) Utils.ConvertToInt(this.tbAge1.Text);
            //modRec.Age2 = (short) Utils.ConvertToInt(this.tbAge2.Text);
            //modRec.Age3 = (short) Utils.ConvertToInt(this.tbAge3.Text);
            //modRec.Age4 = (short) Utils.ConvertToInt(this.tbAge4.Text);
            //modRec.Age5 = (short) Utils.ConvertToInt(this.tbAge5.Text);
            //modRec.Age6 = (short) Utils.ConvertToInt(this.tbAge6.Text);
            //modRec.Age7 = (short) Utils.ConvertToInt(this.tbAge7.Text);
            //modRec.Age8 = (short) Utils.ConvertToInt(this.tbAge8.Text);
            //modRec.Age9 = (short) Utils.ConvertToInt(this.tbAge9.Text);

            modRec.Baf1 = Utils.ConvertToDouble(this.tbBaf1.Text);
            modRec.Baf2 = Utils.ConvertToDouble(this.tbBaf2.Text);
            modRec.Baf3 = Utils.ConvertToDouble(this.tbBaf3.Text);
            modRec.Baf4 = Utils.ConvertToDouble(this.tbBaf4.Text);
            modRec.Baf5 = Utils.ConvertToDouble(this.tbBaf5.Text);

            modRec.BafPlotRadius1 = Utils.ConvertToDouble(this.tbBafLimit1.Text);
            modRec.BafPlotRadius2 = Utils.ConvertToDouble(this.tbBafLimit2.Text);
            modRec.BafPlotRadius3 = Utils.ConvertToDouble(this.tbBafLimit3.Text);
            modRec.BafPlotRadius4 = Utils.ConvertToDouble(this.tbBafLimit4.Text);
            modRec.BafPlotRadius5 = Utils.ConvertToDouble(this.tbBafLimit5.Text);

            modRec.AreaCode1 = this.tbAreaCode1.Text;
            modRec.AreaPlotAcres1 = Utils.ConvertToFloat(this.tbAreaPlotAcres1.Text);
            modRec.AreaPlotRadius1 = Utils.ConvertToFloat(this.tbAreaPlotRadius1.Text);
            modRec.AreaSide11 = Utils.ConvertToFloat(this.tbAreaSide11.Text);
            modRec.AreaSide12 = Utils.ConvertToFloat(this.tbAreaSide12.Text);
            modRec.AreaBlowup1 = Utils.ConvertToFloat(this.tbAreaBlowup1.Text);

            modRec.AreaCode2 = this.tbAreaCode2.Text;
            modRec.AreaPlotAcres2 = Utils.ConvertToFloat(this.tbAreaPlotAcres2.Text);
            modRec.AreaPlotRadius2 = Utils.ConvertToFloat(this.tbAreaPlotRadius2.Text);
            modRec.AreaSide21 = Utils.ConvertToFloat(this.tbAreaSide21.Text);
            modRec.AreaSide22 = Utils.ConvertToFloat(this.tbAreaSide22.Text);
            modRec.AreaBlowup2 = Utils.ConvertToFloat(this.tbAreaBlowup2.Text);

            modRec.AreaCode3 = tbAreaCode3.Text;
            modRec.AreaPlotAcres3 = Utils.ConvertToFloat(this.tbAreaPlotAcres3.Text);
            modRec.AreaPlotRadius3 = Utils.ConvertToFloat(this.tbAreaPlotRadius3.Text);
            modRec.AreaSide31 = Utils.ConvertToFloat(this.tbAreaSide31.Text);
            modRec.AreaSide32 = Utils.ConvertToFloat(this.tbAreaSide32.Text);
            modRec.AreaBlowup3 = Utils.ConvertToFloat(this.tbAreaBlowup3.Text);

            modRec.AreaCode4 = this.tbAreaCode4.Text;
            modRec.AreaPlotAcres4 = Utils.ConvertToFloat(this.tbAreaPlotAcres4.Text);
            modRec.AreaPlotRadius4 = Utils.ConvertToFloat(tbAreaPlotRadius4.Text);
            modRec.AreaSide41 = Utils.ConvertToFloat(this.tbAreaSide41.Text);
            modRec.AreaSide42 = Utils.ConvertToFloat(this.tbAreaSide42.Text);
            modRec.AreaBlowup4 = Utils.ConvertToFloat(this.tbAreaBlowup4.Text);

            modRec.AreaCode5 = this.tbAreaCode5.Text;
            modRec.AreaPlotAcres5 = Utils.ConvertToFloat(this.tbAreaPlotAcres5.Text);
            modRec.AreaPlotRadius5 = Utils.ConvertToFloat(this.tbAreaPlotRadius5.Text);
            modRec.AreaSide51 = Utils.ConvertToFloat(this.tbAreaSide51.Text);
            modRec.AreaSide52 = Utils.ConvertToFloat(this.tbAreaSide52.Text);
            modRec.AreaBlowup5 = Utils.ConvertToFloat(this.tbAreaBlowup5.Text);

            modRec.Strip1 = Utils.ConvertToDouble(tbStripInterval1.Text);
            modRec.StripWidth1 = Utils.ConvertToDouble(tbStripWidth1.Text);
            modRec.StripBlowup1 = Utils.ConvertToDouble(tbStripExpansion1.Text);
            modRec.Strip2 = Utils.ConvertToDouble(tbStripInterval2.Text);
            modRec.StripWidth2 = Utils.ConvertToDouble(tbStripWidth2.Text);
            modRec.StripBlowup2 = Utils.ConvertToDouble(tbStripExpansion2.Text);
            modRec.Strip3 = Utils.ConvertToDouble(tbStripInterval3.Text);
            modRec.StripWidth3 = Utils.ConvertToDouble(tbStripWidth3.Text);
            modRec.StripBlowup3 = Utils.ConvertToDouble(tbStripExpansion3.Text);
            modRec.Strip4 = Utils.ConvertToDouble(tbStripInterval4.Text);
            modRec.StripWidth4 = Utils.ConvertToDouble(tbStripWidth4.Text);
            modRec.StripBlowup4 = Utils.ConvertToDouble(tbStripExpansion4.Text);
            modRec.Strip5 = Utils.ConvertToDouble(tbStripInterval5.Text);
            modRec.StripWidth5 = Utils.ConvertToDouble(tbStripWidth5.Text);
            modRec.StripBlowup5 = Utils.ConvertToDouble(tbStripExpansion5.Text);

            modRec.SpeciesTableName = cbSpeciesTable.Text;
            modRec.SortTableName = cbSortTable.Text;
            modRec.GradeTableName = cbGradeTable.Text;
            modRec.PriceTableName = cbPriceTable.Text;
            modRec.CostTableName = cbCostTable.Text;
            modRec.NonStocked = cbNonStockedTable.Text;
            modRec.NonTimbered = cbNonTimberedTable.Text;

            //if (Utils.ConvertToInt(tbInterval1.Text) > 0)
            //    modRec.PermPlotInterval1 = (short) Utils.ConvertToInt(tbInterval1.Text);
            //if (Utils.ConvertToInt(tbInterval2.Text) > 0)
            //    modRec.PermPlotInterval2 = (short) Utils.ConvertToInt(tbInterval2.Text);
            //if (Utils.ConvertToInt(tbInterval3.Text) > 0)
            //    modRec.PermPlotInterval3 = (short) Utils.ConvertToInt(tbInterval3.Text);
            //if (Utils.ConvertToInt(tbInterval4.Text) > 0)
            //    modRec.PermPlotInterval4 = (short) Utils.ConvertToInt(tbInterval4.Text);
            //if (Utils.ConvertToInt(tbInterval5.Text) > 0)
            //    modRec.PermPlotInterval5 = (short) Utils.ConvertToInt(tbInterval5.Text);
            //if (Utils.ConvertToInt(tbInterval6.Text) > 0)
            //    modRec.PermPlotInterval6 = (short) Utils.ConvertToInt(tbInterval6.Text);
            //if (Utils.ConvertToInt(tbInterval7.Text) > 0)
            //    modRec.PermPlotInterval7 = (short) Utils.ConvertToInt(tbInterval7.Text);
            //if (Utils.ConvertToInt(tbInterval8.Text) > 0)
            //    modRec.PermPlotInterval8 = (short) Utils.ConvertToInt(tbInterval8.Text);
            //if (Utils.ConvertToInt(tbInterval9.Text) > 0)
            //    modRec.PermPlotInterval9 = (short) Utils.ConvertToInt(tbInterval9.Text);
            //if (Utils.ConvertToInt(tbInterval10.Text) > 0)
            //    modRec.PermPlotInterval10 = (short) Utils.ConvertToInt(tbInterval10.Text);

            //if (Utils.ConvertToInt(tbOccasion1.Text) > 0)
            //    modRec.PermPlotOccasion1 = (short) Utils.ConvertToInt(tbOccasion1.Text);
            //if (Utils.ConvertToInt(tbOccasion2.Text) > 0)
            //    modRec.PermPlotOccasion2 = (short) Utils.ConvertToInt(tbOccasion2.Text);
            //if (Utils.ConvertToInt(tbOccasion3.Text) > 0)
            //    modRec.PermPlotOccasion3 = (short) Utils.ConvertToInt(tbOccasion3.Text);
            //if (Utils.ConvertToInt(tbOccasion4.Text) > 0)
            //    modRec.PermPlotOccasion4 = (short) Utils.ConvertToInt(tbOccasion4.Text);
            //if (Utils.ConvertToInt(tbOccasion5.Text) > 0)
            //    modRec.PermPlotOccasion5 = (short) Utils.ConvertToInt(tbOccasion5.Text);
            //if (Utils.ConvertToInt(tbOccasion6.Text) > 0)
            //    modRec.PermPlotOccasion6 = (short) Utils.ConvertToInt(tbOccasion6.Text);
            //if (Utils.ConvertToInt(tbOccasion7.Text) > 0)
            //    modRec.PermPlotOccasion7 = (short) Utils.ConvertToInt(tbOccasion7.Text);
            //if (Utils.ConvertToInt(tbOccasion8.Text) > 0)
            //    modRec.PermPlotOccasion8 = (short) Utils.ConvertToInt(tbOccasion8.Text);
            //if (Utils.ConvertToInt(tbOccasion9.Text) > 0)
            //    modRec.PermPlotOccasion9 = (short) Utils.ConvertToInt(tbOccasion9.Text);
            //if (Utils.ConvertToInt(tbOccasion10.Text) > 0)
            //    modRec.PermPlotOccasion10 = (short) Utils.ConvertToInt(tbOccasion10.Text);

            //if (!string.IsNullOrEmpty(dateEdit1.Text))
            //    modRec.PermPlotDate1 = Convert.ToDateTime(dateEdit1.Text);
            //if (!string.IsNullOrEmpty(dateEdit2.Text))
            //    modRec.PermPlotDate2 = Convert.ToDateTime(dateEdit2.Text);
            //if (!string.IsNullOrEmpty(dateEdit3.Text))
            //    modRec.PermPlotDate3 = Convert.ToDateTime(dateEdit3.Text);
            //if (!string.IsNullOrEmpty(dateEdit4.Text))
            //    modRec.PermPlotDate4 = Convert.ToDateTime(dateEdit4.Text);
            //if (!string.IsNullOrEmpty(dateEdit5.Text))
            //    modRec.PermPlotDate5 = Convert.ToDateTime(dateEdit5.Text);
            //if (!string.IsNullOrEmpty(dateEdit6.Text))
            //    modRec.PermPlotDate6 = Convert.ToDateTime(dateEdit6.Text);
            //if (!string.IsNullOrEmpty(dateEdit7.Text))
            //    modRec.PermPlotDate7 = Convert.ToDateTime(dateEdit7.Text);
            //if (!string.IsNullOrEmpty(dateEdit8.Text))
            //    modRec.PermPlotDate8 = Convert.ToDateTime(dateEdit8.Text);
            //if (!string.IsNullOrEmpty(dateEdit9.Text))
            //    modRec.PermPlotDate9 = Convert.ToDateTime(dateEdit9.Text);
            //if (!string.IsNullOrEmpty(dateEdit10.Text))
            //    modRec.PermPlotDate10 = Convert.ToDateTime(dateEdit10.Text);

            if (string.IsNullOrEmpty(modRec.NonStocked) && string.IsNullOrEmpty(modRec.NonTimbered))
                modRec.LandClass = "TIM";
            else if (!string.IsNullOrEmpty(modRec.NonStocked))
                modRec.LandClass = "NS";
            else if (!string.IsNullOrEmpty(modRec.NonTimbered))
                modRec.LandClass = "NT";

            standHaulingBindingSource.EndEdit();
            standAgeBindingSource.EndEdit();
            standPermPlotBindingSource.EndEdit();
            standYardingSystemBindingSource.EndEdit();
        }

        private void barButtonItem28_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Import SuperACE 2008

            ImportSA2008MultiProjectForm frm = new ImportSA2008MultiProjectForm(ref projectContext, ref locContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.bProcess)
            {
                TempDbContext tempDb = new TempDbContext("Data Source = " + TempDataSource);
                Import.SuperACE2008Import aci = new Import.SuperACE2008Import();

                Stopwatch sw = new Stopwatch();
                sw.Start();

                List<Temp.DAL.ImportProject> projectsToImport = tempDb.ImportProjects.ToList();
                foreach (var projectToImport in projectsToImport)
                {
                    string newProject = projectToImport.NewProjectName;
                    string newProjectLocation = projectToImport.NewProjectLocation;
                    string flipsDb = frm.oldFlipsFileName;
                    string defaultsDb = frm.oldDefaultsFileName;
                    string projectDb = projectToImport.OldProject + ".mdb";
                    string oldProject = Path.GetFileNameWithoutExtension(frm.oldProjectFileName);

                    if (projectToImport.SelectProject == true)
                    {
                        // if new project exists
                        if (
                            File.Exists(string.Format("{0}{1}.sdf", projectToImport.NewProjectLocation,
                                projectToImport.NewProjectName)))
                        {
                            // User wants to replace the data
                            if (projectToImport.ReplaceProject == true)
                            {
                                //ProjectDataSource = string.Format("{0}{1}.sdf", projectToImport.NewProjectLocation, projectToImport.NewProjectName);
                                try
                                {
                                    ProjectsBLL.DeleteProjectLocation(ref locContext, projectToImport.NewProjectName);
                                }
                                catch
                                {

                                }

                                try
                                {
                                    File.Delete(string.Format("{0}{1}.sdf", projectToImport.NewProjectLocation,
                                        projectToImport.NewProjectName));
                                }
                                catch
                                {

                                }
                            }
                        }


                        try
                        {
                            // Create the new project database from New.sdf
                            File.Copy(
                                string.Format("{0}Empty.sdf",
                                    SuperACE.Properties.Settings.Default["DbLocation"].ToString()),
                                string.Format("{0}{1}.sdf", projectToImport.NewProjectLocation,
                                    projectToImport.NewProjectName));
                            ProjectDataSource = string.Format("{0}{1}.sdf", projectToImport.NewProjectLocation,
                                projectToImport.NewProjectName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (ArgumentException ex)
                        {
                            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (DirectoryNotFoundException ex)
                        {
                            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (FileNotFoundException ex)
                        {
                            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch
                        {
                            MessageBox.Show(
                                "Problem with the Project Location\nCopy " +
                                string.Format("{0}Empty.sdf", Properties.Settings.Default["DbLocation"].ToString()) +
                                " " + string.Format("{0}\\{1}.sdf", newProjectLocation, newProject), "Error");
                            return;
                        }

                        // add the new project to Project Locations database
                        Projects.DAL.AllProject newPrjLocation = new Projects.DAL.AllProject();
                        newPrjLocation.ProjectName = projectToImport.NewProjectName;
                        newPrjLocation.ProjectLocation = projectToImport.NewProjectLocation;
                        //prjLocContext.Project.Add(newPrjLocation);

                        try
                        {
                            //projectContext.SaveChanges();
                            ProjectsBLL.SaveProjectLocation(ProjectLocationDataSource, newPrjLocation);
                                // prjLocContext.SaveChanges();
                            //this.Close();
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(
                                "Saving operation failed! Internal error has occured. Exception message: \n\r" + ex,
                                "Error", MessageBoxButtons.OK);
                            this.DialogResult = DialogResult.None;
                            return;
                        }

                        ProjectDbContext ctx = new ProjectDbContext("Data Source = " + ProjectDataSource);
                        ctx.Configuration.AutoDetectChangesEnabled = false;
                        Project.DAL.Project rec = ProjectBLL.GetProject(ref ctx);
                        rec.ProjectName = projectToImport.NewProjectName;
                        ctx.SaveChanges();

                        // import the SuperACE2008 project
                        aci.ProcessProject(ref ctx, flipsDb, "TProject", oldProject, newProject);
                        // import the SuperACE2008 tables for the project
                        if ((bool) projectToImport.UseProjectTables)
                        {
                            aci.CheckForExistingProjectTable(ref ctx, ref tempDb, flipsDb, newProject);
                            ExistingProjectTablesForm exFrm = new ExistingProjectTablesForm(ref tempDb);
                            exFrm.StartPosition = FormStartPosition.CenterParent;
                            exFrm.ShowDialog();
                            if (exFrm.mProcess)
                            {
                                List<ExistingProjectTable> exTables =
                                    tempDb.ExistingProjectTables.OrderBy(t => t.TableType)
                                        .ThenBy(t => t.TableName)
                                        .ToList();
                                aci.ProcessProjectTables(ref ctx, flipsDb, exTables, newProject, ProjectLocationDataSource);
                            }
                            exFrm.Dispose();
                        }

                        // import the SuperACE2008 tables for the project
                        if ((bool) projectToImport.UseDefaultTables)
                            aci.ProcessProjectDefaultTables(ref ctx, defaultsDb, "TDefaults", newProject);
                        // import the SuperACE2008 stand
                        aci.ProcessStand(ref ctx, projectDb, "TMASTER", newProject);
                        aci.ProcessStandInput(ref ctx, projectDb, "TSTNDSPC", newProject);
                        aci.ProcessPlots(ref ctx, projectDb, "TPLOTLOC", newProject);
                        aci.ProcessTrees(ref ctx, projectDb, "TCRUISE", newProject);

                        tempDb.Dispose();
                        tempDb = null;

                        SplashScreenManager.ShowForm(typeof(WaitForm1));
                        SplashScreenManager.Default.SetWaitFormCaption("Processing");
                        SplashScreenManager.Default.SetWaitFormDescription("Updating Plot and Stands Totals");

                        try
                        {
                            ctx = new ProjectDbContext("Data Source = " + ProjectDataSource);
                            ProjectBLL.UpdateCountPlotsForStands(ref ctx);
                            ProjectBLL.UpdatePlotsForStands(ref ctx);
                            List<Stand> tStands = ProjectBLL.GetAllStands(ref ctx);
                            foreach (var item in tStands)
                            {
                                Stand standItem = item;
                                Project.DAL.Project projectItem = CurrentProject;
                                ProjectBLL.UpdateTotalsForStand(ref ctx, ref projectItem, ref standItem);
                            }
                            ctx.SaveChanges();
                        }
                        catch
                        {

                        }
                        SplashScreenManager.CloseForm();
                    }
                }
                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                barStaticItem3.Caption = string.Format("Import SuperACE 2008 - {0} min", ts.Minutes);

                if (tempDb != null)
                    tempDb.Dispose();
            }
            frm.Dispose();
        }

        private void barButtonItem29_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Import PocketEASY
            GetPocketEASYData frm = new GetPocketEASYData();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItemPriceTable_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Price Form
            PriceForm frm = new PriceForm(ref projectContext, CurrentProject.PriceTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.wasProjectChanged)
                CurrentProject = ProjectBLL.GetProject(ref projectContext);

            frm.Dispose();
        }

        private void cbState_TextChanged(object sender, EventArgs e)
        {
            stateCountyBindingSource.Clear();
            stateCountyBindingSource.DataSource =
                locContext.StateCounties.OrderBy(sc => sc.CountyAbbr).Where(sc => sc.StateAbbr == cbState.Text).ToList();
        }

        private void tbNetAcres_Validating(object sender, CancelEventArgs e)
        {

        }

        void advBandedGridViewTrees_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            //ColumnView view = sender as ColumnView;
            //int tree = (int)view.GetListSourceRowCellValue(e.ListSourceRowIndex, "TreeNumber");

            //if (tree == 0)
            //    return; 

            //if (tree != 1)
            //{
            //    switch (e.Column.FieldName)
            //    {
            //        case "PlotNumber":
            //            e.DisplayText = string.Empty;
            //            break;
            //    }
            //}
        }

        private void gridControlPlots_EditorKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    if (plotsBindingSource.Position == plotsBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        AddPlot();
                    }
                    break;
            }
        }

        private void advBandedGridViewPlots_RowUpdated(object sender, RowObjectEventArgs e)
        {
            projectContext.SaveChanges();
        }

        private void advBandedGridViewPlots_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = (GridView) sender;

            switch (e.KeyCode)
            {
                //case Keys.Tab:
                //case Keys.Right:
                //case Keys.Enter:
                //    if (!view.IsDataRow(view.FocusedRowHandle))
                //        return;
                //    GridColumn nextColumn = view.FocusedColumn;
                //    int nextRowHandle = view.FocusedRowHandle;
                //    while (nextColumn == view.FocusedColumn || nextColumn.ReadOnly)
                //    {
                //        if (nextColumn.VisibleIndex + 1 >= view.VisibleColumns.Count)
                //        {
                //            nextColumn = view.VisibleColumns[0];
                //            if (view.IsDataRow(view.FocusedRowHandle + 1))
                //                nextRowHandle = view.FocusedRowHandle + 1;
                //        }
                //        else
                //            nextColumn = view.VisibleColumns[nextColumn.VisibleIndex + 1];
                //    }

                //    view.FocusedColumn = nextColumn;
                //    view.FocusedRowHandle = nextRowHandle;
                //    view.ShowEditor();
                //    e.Handled = true;
                //    break;
                case Keys.Left:
                    if (!view.IsDataRow(view.FocusedRowHandle))
                        return;
                    GridColumn prevColumn = view.FocusedColumn;
                    int prevRowHandle = view.FocusedRowHandle;
                    while (prevColumn == view.FocusedColumn || prevColumn.ReadOnly)
                    {
                        if (prevColumn.VisibleIndex - 1 >= view.VisibleColumns.Count)
                        {
                            prevColumn = view.VisibleColumns[0];
                            if (view.IsDataRow(view.FocusedRowHandle - 1))
                                prevRowHandle = view.FocusedRowHandle - 1;
                        }
                        else
                            prevColumn = view.VisibleColumns[prevColumn.VisibleIndex - 1];
                    }

                    view.FocusedColumn = prevColumn;
                    view.FocusedRowHandle = prevRowHandle;
                    view.ShowEditor();
                    e.Handled = true;
                    break;
                case Keys.Down:
                    if (plotsBindingSource.Position == plotsBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        AddPlot();
                    }
                    break;
            }
        }

        private void advBandedGridViewTrees_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    advBandedGridViewTrees.UpdateCurrentRow();
                    if (treesBindingSource.Position == treesBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        AddTree();
                    }
                    else
                    {
                        CurrentTree = treesBindingSource.Current as Tree;
                        if (defTreeSpecies.Abbreviation != CurrentTree.SpeciesAbbreviation)
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
                    }
                    break;
            }
        }

        private void DeleteTrees()
        {
            if (XtraMessageBox.Show("Delete the Selected Tree(s)?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Tree current = treesBindingSource.Current as Tree;
                List<string> plots = new List<string>();
                int[] delRows = advBandedGridViewTrees.GetSelectedRows();
                for (int i = 0; i < delRows.Length; i++)
                {
                    treesBindingSource.Position = delRows[i];
                    Tree rec = treesBindingSource.Current as Tree;
                    if (plots.Any(p => p == rec.PlotNumber) == false)
                        plots.Add(rec.PlotNumber);
                    projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(t => t.TreesId == rec.TreesId));
                    projectContext.Trees.Remove(rec);
                }
                projectContext.SaveChanges();

                if (XtraMessageBox.Show("Would you like to renumber the trees in the plot?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    for (int z = 0; z < plots.Count; z++)
                    {
                        List<Tree> treesInPlot = projectContext.Trees.OrderBy(t => t.PlotNumber)
                            .ThenBy(t => t.TreeNumber)
                            .Where(t => t.StandsId == current.StandsId && t.PlotNumber == current.PlotNumber)
                            .ToList();
                        int x = 1;
                        foreach (var tree in treesInPlot)
                        {
                            RenumberTreeSegment(tree, x);
                            tree.TreeNumber = x;
                            x++;
                        }
                    }
                    projectContext.SaveChanges();
                }

                LoadTrees();
            }
        }

        private void RenumberTreeSegment(Tree tree, int x)
        {
            List<Treesegment> reNumberSegments =
                projectContext.Treesegments.OrderBy(s => s.SegmentNumber).Where(s => s.TreesId == tree.TreesId).ToList();
            foreach (var item in reNumberSegments)
            {
                item.TreeNumber = (short)x;
                item.GroupTreeNumber = x;
            }
        }

        private void DeletePlots()
        {
            if (XtraMessageBox.Show("Delete the Selected Plot(s)?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int[] delRows = advBandedGridViewPlots.GetSelectedRows();
                for (int i = 0; i < delRows.Length; i++)
                {
                    plotsBindingSource.Position = delRows[i];
                    Plot rec = plotsBindingSource.Current as Plot;
                    projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(t => t.PlotId == rec.PlotsId));
                    projectContext.Trees.RemoveRange(projectContext.Trees.Where(t => t.PlotId == rec.PlotsId));
                    projectContext.Plots.Remove(rec);
                }
                projectContext.SaveChanges();

                LoadPlots();
            }
        }

        private void gridControlTrees_EditorKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    //advBandedGridViewTrees.ValidateEditor();
                    //SendKeys.Send("{ENTER}");
                    advBandedGridViewTrees.UpdateCurrentRow();
                    if (treesBindingSource.Position == treesBindingSource.Count - 1)
                    {
                        e.Handled = true;
                        //DoCalcForTree(true, null);
                        AddTree();
                    }
                    else
                    {
                        CurrentTree = treesBindingSource.Current as Tree;
                        if (defTreeSpecies != null)
                        {
                            if (defTreeSpecies.Abbreviation != CurrentTree.SpeciesAbbreviation)
                                defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
                        }
                        else
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
                    }
                    break;
                case Keys.Enter:
                case Keys.Right:
                    CheckMoveToNextColumn();
                    break;
            }
        }

        private void CheckMoveToNextColumn()
        {
            //string tmp = advBandedGridViewTrees.GetFocusedRowCellDisplayText(advBandedGridViewTrees.FocusedColumn.FieldName);
            if (advBandedGridViewTrees.ActiveEditor == null)
                return;

            string tmp = advBandedGridViewTrees.ActiveEditor.Text;

            switch (advBandedGridViewTrees.FocusedColumn.FieldName)
            {
                case "Sort1":
                case "Sort2":
                case "Sort3":
                case "Sort4":
                case "Sort5":
                case "Sort6":
                case "Sort7":
                case "Sort8":
                case "Sort9":
                case "Sort10":
                case "Sort11":
                case "Sort12":
                    if (!string.IsNullOrEmpty(tmp))
                        return;
                    break;
                case "BdFtLd1":
                case "BdFtLd2":
                case "BdFtLd3":
                case "BdFtLd4":
                case "BdFtLd5":
                case "BdFtLd6":
                case "BdFtLd7":
                case "BdFtLd8":
                case "BdFtLd9":
                case "BdFtLd10":
                case "BdFtLd11":
                case "BdFtLd12":
                    if (Utils.ConvertToTiny(tmp) != 0)
                        return;
                    break;
            }

            switch (advBandedGridViewTrees.FocusedColumn.FieldName)
            {
                case "Sort1":
                case "Sort2":
                case "Sort3":
                case "Sort4":
                case "Sort5":
                case "Sort6":
                case "Sort7":
                case "Sort8":
                case "Sort9":
                case "Sort10":
                case "Sort11":
                case "Sort12":
                    advBandedGridViewTrees.UpdateCurrentRow();
                    if (treesBindingSource.Position == treesBindingSource.Count - 1)
                    {
                        //DoCalcForTree(true, null);
                        AddTree();
                    }
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
                    break;
                case "BdFtLd1":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort2"];
                    break;
                case "BdFtLd2":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort3"];
                    break;
                case "BdFtLd3":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort4"];
                    break;
                case "BdFtLd4":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort5"];
                    break;
                case "BdFtLd5":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort6"];
                    break;
                case "BdFtLd6":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort7"];
                    break;
                case "BdFtLd7":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort8"];
                    break;
                case "BdFtLd8":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort9"];
                    break;
                case "BdFtLd9":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort10"];
                    break;
                case "BdFtLd10":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort11"];
                    break;
                case "BdFtLd11":
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort12"];
                    break;
                case "BdFtLd12":
                    if (treesBindingSource.Position == treesBindingSource.Count - 1)
                    {
                        //DoCalcForTree(true, null);
                        AddTree();
                    }
                    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
                    break;
            }
        }

        private void AddTree()
        {
            Tree rec = new Tree();
            rec.StandsId = CurrentStand.StandsId;
            rec.Age = 0;
            if (defTreeAge != 0)
                rec.AgeCode = defTreeAge;
            else
                rec.AgeCode = 0;
            rec.Ao = 0;
            rec.Bark = 0;
            rec.BasalArea = 0;

            rec.Sort1 = string.Empty;
            rec.Grade1 = string.Empty;
            rec.Length1 = string.Empty;
            rec.BdFtDd1 = 0;
            rec.BdFtLd1 = 0;
            rec.BdFtPd1 = string.Empty;
            rec.CuFtDd1 = 0;
            rec.CuFtLd1 = 0;

            rec.Sort2 = string.Empty;
            rec.Grade2 = string.Empty;
            rec.Length2 = string.Empty;
            rec.BdFtDd2 = 0;
            rec.BdFtLd2 = 0;
            rec.BdFtPd2 = string.Empty;
            rec.CuFtDd2 = 0;
            rec.CuFtLd2 = 0;

            rec.Sort3 = string.Empty;
            rec.Grade3 = string.Empty;
            rec.Length3 = string.Empty;
            rec.BdFtDd3 = 0;
            rec.BdFtLd3 = 0;
            rec.BdFtPd3 = string.Empty;
            rec.CuFtDd3 = 0;
            rec.CuFtLd3 = 0;

            rec.Sort4 = string.Empty;
            rec.Grade4 = string.Empty;
            rec.Length4 = string.Empty;
            rec.BdFtDd4 = 0;
            rec.BdFtLd4 = 0;
            rec.BdFtPd4 = string.Empty;
            rec.CuFtDd4 = 0;
            rec.CuFtLd4 = 0;

            rec.Sort5 = string.Empty;
            rec.Grade5 = string.Empty;
            rec.Length5 = string.Empty;
            rec.BdFtDd5 = 0;
            rec.BdFtLd5 = 0;
            rec.BdFtPd5 = string.Empty;
            rec.CuFtDd5 = 0;
            rec.CuFtLd5 = 0;

            rec.Sort6 = string.Empty;
            rec.Grade6 = string.Empty;
            rec.Length6 = string.Empty;
            rec.BdFtDd6 = 0;
            rec.BdFtLd6 = 0;
            rec.BdFtPd6 = string.Empty;
            rec.CuFtDd6 = 0;
            rec.CuFtLd6 = 0;

            rec.Sort7 = string.Empty;
            rec.Grade7 = string.Empty;
            rec.Length7 = string.Empty;
            rec.BdFtDd7 = 0;
            rec.BdFtLd7 = 0;
            rec.BdFtPd7 = string.Empty;
            rec.CuFtDd7 = 0;
            rec.CuFtLd7 = 0;

            rec.Sort8 = string.Empty;
            rec.Grade8 = string.Empty;
            rec.Length8 = string.Empty;
            rec.BdFtDd8 = 0;
            rec.BdFtLd8 = 0;
            rec.BdFtPd8 = string.Empty;
            rec.CuFtDd8 = 0;
            rec.CuFtLd8 = 0;

            rec.Sort9 = string.Empty;
            rec.Grade9 = string.Empty;
            rec.Length9 = string.Empty;
            rec.BdFtDd9 = 0;
            rec.BdFtLd9 = 0;
            rec.BdFtPd9 = string.Empty;
            rec.CuFtDd9 = 0;
            rec.CuFtLd9 = 0;

            rec.Sort10 = string.Empty;
            rec.Grade10 = string.Empty;
            rec.Length10 = string.Empty;
            rec.BdFtDd10 = 0;
            rec.BdFtLd10 = 0;
            rec.BdFtPd10 = string.Empty;
            rec.CuFtDd10 = 0;
            rec.CuFtLd10 = 0;

            rec.Sort11 = string.Empty;
            rec.Grade11 = string.Empty;
            rec.Length11 = string.Empty;
            rec.BdFtDd11 = 0;
            rec.BdFtLd11 = 0;
            rec.BdFtPd11 = string.Empty;
            rec.CuFtDd11 = 0;
            rec.CuFtLd11 = 0;

            rec.Sort12 = string.Empty;
            rec.Grade12 = string.Empty;
            rec.Length12 = string.Empty;
            rec.BdFtDd12 = 0;
            rec.BdFtLd12 = 0;
            rec.BdFtPd12 = string.Empty;
            rec.CuFtDd12 = 0;
            rec.CuFtLd12 = 0;

            rec.BoleHeight = 0;
            rec.CalcTotalHeight = 0;
            rec.CalcTotalHtUsedYn = "N";
            rec.CrownPositionDisplayCode = string.Empty;
            rec.CrownRatioDisplayCode = string.Empty;
            rec.UserDefinedDisplayCode = string.Empty;
            rec.VigorDisplayCode = string.Empty;
            rec.DamageDisplayCode = string.Empty;
            rec.Dbh = 0;
            if (defTreeFP != 0)
                rec.FormPoint = defTreeFP;
            else
                rec.FormPoint = 0;
            rec.GroupPlot = string.Empty;
            rec.GroupTree = 0;
            rec.LogsPerAcre = 0;
            rec.PfValue = 0;
            rec.PfType = "B";
            if (!string.IsNullOrEmpty(defTreePF))
                rec.PlotFactorInput = defTreePF;
            else
                rec.PlotFactorInput = string.Empty;
            if (!string.IsNullOrEmpty(defTreePlotNumber))
                rec.PlotNumber = defTreePlotNumber;
            else
                rec.PlotNumber = string.Empty;
            rec.ReportedFormFactor = 0;
            rec.SpeciesAbbreviation = string.Empty;
            //if (defTreeSpecies != null)
            //{
            //    if (!string.IsNullOrEmpty(defTreeSpecies.Abbreviation))
            //        rec.SpeciesAbbreviation = defTreeSpecies.Abbreviation;
            //    else
            //        rec.SpeciesAbbreviation = string.Empty;
            //}
            rec.SpeciesAbbreviation = string.Empty;
            if (!string.IsNullOrEmpty(defTreeTDF))
                rec.TopDiameterFractionCode = defTreeTDF;
            else
                rec.TopDiameterFractionCode = string.Empty;
            rec.TotalBdFtGrossVolume = 0;
            rec.TotalBdFtNetVolume = 0;
            rec.TotalCcf = 0;
            rec.TotalCuFtGrossVolume = 0;
            rec.TotalCuFtNetVolume = 0;
            rec.TotalGrossCubicPerAcre = 0;
            rec.TotalGrossScribnerPerAcre = 0;
            rec.TotalHeight = 0;
            rec.TotalMbf = 0;
            rec.TotalNetCubicPerAcre = 0;
            rec.TotalNetScribnerPerAcre = 0;
            rec.TotalTonsPerAcre = 0;
            rec.TreeCount = 1;
            if (defTreeTreeNumber != 0)
                rec.TreeNumber = ++defTreeTreeNumber;
            else
                rec.TreeNumber = 1;
            rec.TreesPerAcre = 0;
            rec.TreeStatusDisplayCode = string.Empty;
            rec.TreeType = "D";
            if (CurrentPlot != null)
                rec.PlotId = CurrentPlot.PlotsId;

            projectContext.Trees.Add(rec);
            projectContext.SaveChanges();

            needToUpdatePlotsStands = true;

            LoadTreeDataSource();
            advBandedGridViewTrees.MoveLast(); //.FocusedRowHandle = advBandedGridViewPlots.FocusedRowHandle + 1;
            if (string.IsNullOrEmpty(defTreePlotNumber))
                advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["PlotNumber"];
            else
                advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
            gridControlTrees.Focus();
        }

        private void LoadTreeDataSource()
        {
            treesBindingSource.Clear();
            //treesBindingSource.DataSource = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == CurrentStand.StandsId).ToList();
            treesBindingSource.DataSource =
                projectContext.Trees.OrderBy(t => t.PlotNumber)
                    .ThenBy(t => t.TreeNumber)
                    .Where(t => t.StandsId == CurrentStand.StandsId)
                    .ToList();
            if (treesBindingSource.Count == 0)
            {
                defTreeAge = 0;
                defTreeFP = 0;
                defTreePF = string.Empty;
                defTreePlotNumber = string.Empty;
                defTreeTreeNumber = 0;
                defTreeTDF = string.Empty;
                AddTree();
            }
            else
            {
                // Set defaults for tree
                advBandedGridViewTrees.MoveLast();
                defTreePlotNumber = advBandedGridViewTrees.GetFocusedRowCellDisplayText("PlotNumber");
                defTreeTreeNumber = Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TreeNumber"));
                defTreePF = advBandedGridViewTrees.GetFocusedRowCellDisplayText("PlotFactorInput");
                defTreeAge = Utils.ConvertToTiny(advBandedGridViewTrees.GetFocusedRowCellDisplayText("AgeCode"));
                defTreeFP = Utils.ConvertToTiny(advBandedGridViewTrees.GetFocusedRowCellDisplayText("FormPoint"));
                defTreeTDF = advBandedGridViewTrees.GetFocusedRowCellDisplayText("TopDiameterFractionCode");
            }
        }

        private void advBandedGridViewTrees_ValidatingEditor(object sender,
            DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            switch (advBandedGridViewTrees.FocusedColumn.FieldName)
            {
                case "PlotNumber":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0)
                        {
                            e.Value = e.Value.ToString().PadLeft(4, '0');
                        }
                        Plot plot =
                            projectContext.Plots.FirstOrDefault(
                                p => p.StandsId == CurrentStand.StandsId && p.UserPlotNumber == e.Value.ToString());
                        if (plot != null)
                        {
                            CurrentPlot = plot;
                            advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                                advBandedGridViewTrees.Columns["PlotsId"], plot.PlotsId);
                        }
                        else
                        {
                            // Add plot
                            AddPlot(e.Value.ToString());
                            //PlotAddForm frm = new PlotAddForm(ref projectContext, CurrentProject, CurrentStand, e.Value.ToString());
                            //frm.StartPosition = FormStartPosition.CenterParent;
                            //frm.ShowDialog();
                            //frm.Dispose();

                            plot =
                                projectContext.Plots.FirstOrDefault(
                                    p => p.StandsId == CurrentStand.StandsId && p.UserPlotNumber == e.Value.ToString());
                            if (plot != null)
                            {
                                CurrentPlot = plot;
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                                    advBandedGridViewTrees.Columns["PlotsId"], plot.PlotsId);
                            }
                        }
                        if (defTreePlotNumber != e.Value.ToString().Trim())
                        {
                            advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                                advBandedGridViewTrees.Columns["TreeNumber"], 1);
                            defTreeTreeNumber = 1;
                        }
                        defTreePlotNumber = e.Value.ToString().Trim();
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Plot Number.";
                        return;
                    }
                    break;
                case "TreeNumber":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        defTreeTreeNumber = Utils.ConvertToInt(e.Value.ToString());
                    }
                    break;
                case "PlotFactorInput":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        switch (e.Value.ToString())
                        {
                            case "B1":
                                if (CurrentStand.Baf1 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B2":
                                if (CurrentStand.Baf2 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B3":
                                if (CurrentStand.Baf3 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Basal Factor in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B4":
                                if (CurrentStand.Baf4 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B5":
                                if (CurrentStand.Baf5 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "A1":
                            case "A2":
                            case "A3":
                            case "A4":
                            case "A5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = true;
                                    e.ErrorText = "No value was entered for Area Code in the Stand Master.";
                                    return;
                                }
                                break;
                            case "R1":
                            case "R2":
                            case "R3":
                            case "R4":
                            case "R5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Plot Radius in the Stand Master.";
                                    return;
                                }
                                break;
                            case "F1":
                            case "F2":
                            case "F3":
                            case "F4":
                            case "F5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = true;
                                    e.ErrorText = "No value was entered for Fixed Area Plot in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S1":
                                if (CurrentStand.Strip1 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S2":
                                if (CurrentStand.Strip2 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S3":
                                if (CurrentStand.Strip3 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S4":
                                if (CurrentStand.Strip4 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S5":
                                if (CurrentStand.Strip5 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            default:
                                e.ErrorText = string.Empty;
                                if (Utils.ConvertToInt(e.Value.ToString()) < 5)
                                {
                                    if (e.Value.ToString() != "00")
                                    {
                                        e.Valid = false;
                                        e.ErrorText = "Invalid Plot Factor entered.";
                                        return;
                                    }
                                }
                                else
                                {
                                    e.Valid = true;
                                    e.ErrorText = string.Empty;
                                }
                                break;
                        }
                        if (e.Value.ToString().StartsWith("R") && e.Valid)
                            advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                                advBandedGridViewTrees.Columns["FormPoint"], 0);
                        defTreePF = e.Value.ToString().Trim();
                        switch (e.Value.ToString().Substring(0, 1))
                        {
                            case "R":
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle, advBandedGridViewTrees.Columns["PFType"], "R");
                                break;
                            case "S":
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle, advBandedGridViewTrees.Columns["PFType"], "S");
                                break;
                            case "F":
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle, advBandedGridViewTrees.Columns["PFType"], "F");
                                break;
                            case "A":
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle, advBandedGridViewTrees.Columns["PFType"], "A");
                                break;
                            default:
                                advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle, advBandedGridViewTrees.Columns["PFType"], "B");
                                break;
                        }
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Plot Factor.";
                        return;
                    }
                    break;
                case "AgeCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        byte a = Utils.ConvertToTiny(e.Value.ToString());
                        if (!projectContext.StandAges.Any(s => s.StandsId == CurrentStand.StandsId && s.AgeCode == 1))
                        {
                            e.Valid = false;
                            e.ErrorText = string.Format("No value was entered for Age{0} in the Stand Master.", e.Value.ToString());
                            return;
                        }
                        //switch (e.Value.ToString())
                        //{
                        //    case "1":
                        //        if (CurrentStand.Age1 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age1 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "2":
                        //        if (CurrentStand.Age2 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age2 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "3":
                        //        if (CurrentStand.Age3 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age3 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "4":
                        //        if (CurrentStand.Age4 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age4 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "5":
                        //        if (CurrentStand.Age5 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age5 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "6":
                        //        if (CurrentStand.Age6 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age6 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "7":
                        //        if (CurrentStand.Age7 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age7 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "8":
                        //        if (CurrentStand.Age8 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age8 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "9":
                        //        if (CurrentStand.Age9 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age9 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    default:
                        //        e.Valid = false;
                        //        e.ErrorText = "Age Code is invalid.";
                        //        break;
                        //}
                        defTreeAge = Utils.ConvertToTiny(e.Value.ToString());
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Age.";
                        return;
                    }
                    break;
                case "SpeciesAbbreviation":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 2)
                        {
                            e.Valid = false;
                            e.ErrorText = "Value is too large.";
                            return;
                        }
                        // Code was entered
                        if (Utils.IsValidInt(e.Value.ToString()))
                        {
                            Species spcRec1 = ProjectBLL.GetSpecieByCode(ref projectContext,
                                CurrentProject.SpeciesTableName, Utils.ConvertToInt(e.Value.ToString()));
                            if (spcRec1 == null)
                            {
                                //e.Valid = false;
                                //e.ErrorText = "Species is invalid.";
                                //return;
                                PopupSpeciesForm spcForm = new PopupSpeciesForm(ref projectContext,
                                    CurrentProject.SpeciesTableName);
                                spcForm.StartPosition = FormStartPosition.CenterParent;
                                spcForm.ShowDialog();
                                if (!string.IsNullOrEmpty(spcForm.code))
                                {
                                    e.Value = spcForm.code;
                                    spcRec1 = ProjectBLL.GetSpecieByCode(ref projectContext, CurrentProject.SpeciesTableName, Utils.ConvertToInt(e.Value.ToString()));
                                    defTreeSpecies = spcRec1;
                                }
                                else
                                {
                                    e.Valid = false;
                                    e.ErrorText = "Species is invalid.";
                                    return;
                                }
                                spcForm.Dispose();
                            }
                            else
                            {
                                e.Value = spcRec1.Abbreviation;
                                //defTreeSpecies = spcRec1;
                            }
                        }
                        else
                        {
                            // Abrv was entered
                            Species spcRec2 = ProjectBLL.GetSpecieByAbbrev(ref projectContext,
                                CurrentProject.SpeciesTableName, e.Value.ToString());
                            if (spcRec2 == null)
                            {
                                //e.Valid = false;
                                //e.ErrorText = "Species is invalid.";
                                //return;
                                PopupSpeciesForm spcForm = new PopupSpeciesForm(ref projectContext,
                                    CurrentProject.SpeciesTableName);
                                spcForm.StartPosition = FormStartPosition.CenterParent;
                                spcForm.ShowDialog();
                                if (!string.IsNullOrEmpty(spcForm.code))
                                {
                                    e.Value = spcForm.code;
                                    spcRec2 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, e.Value.ToString());
                                    defTreeSpecies = spcRec2;
                                }
                                else
                                {
                                    e.Valid = false;
                                    e.ErrorText = "Species is invalid.";
                                    return;
                                }
                                spcForm.Dispose();
                            }
                            //else
                            //    defTreeSpecies = spcRec2;
                        }
                    }
                    break;
                case "TreeStatusDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 1)
                        {
                            e.Valid = false;
                            e.ErrorText = "Value is too large.";
                            return;
                        }
                        if (!ProjectBLL.DoesStatusExist(ref projectContext, CurrentProject.TreeStatusTableName,
                                e.Value.ToString()))
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Tree Status is invalid.";
                            //return;
                            PopupTreeStatusForm tsForm = new PopupTreeStatusForm(ref projectContext,
                                    CurrentProject.TreeStatusTableName);
                            tsForm.StartPosition = FormStartPosition.CenterParent;
                            tsForm.ShowDialog();
                            if (!string.IsNullOrEmpty(tsForm.code))
                                e.Value = tsForm.code;
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Tree Status is invalid.";
                                return;
                            }
                            tsForm.Dispose();
                        }
                    }
                    break;
                case "TreeCount":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) < 0 || Utils.ConvertToInt(e.Value.ToString()) > 999)
                        {
                            e.Valid = false;
                            e.ErrorText = "Tree Count MUST be 0 - 999.";
                            return;
                        }
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "Value must be 1 - 999.";
                        return;
                    }
                    break;
                case "Dbh":
                    int nPos = e.Value.ToString().IndexOf(".");
                    if (nPos != -1)
                    {
                        if (e.Value.ToString().Length - 1 > nPos + 1)
                        {
                            e.Valid = false;
                            e.ErrorText = "Too many characters to the right of the decimal.";
                            return;
                        }
                    }
                    float MaxDia = 0;
                    if (defTreeSpecies == null)
                    {
                        GetTreeSpecies(advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                        if (defTreeSpecies == null)
                            MaxDia = 0;
                        else
                            MaxDia = (float)defTreeSpecies.MaxDia;
                    }
                    else
                        MaxDia = (float)defTreeSpecies.MaxDia;
                    if (MaxDia == 0)
                        MaxDia = (float)299.99;
                    if (Utils.ConvertToFloat(e.Value.ToString()) < 0.0 ||
                        Utils.ConvertToFloat(e.Value.ToString()) > MaxDia)
                    {
                        e.Valid = false;
                        e.ErrorText = string.Format("Dbh MUST be 0.0 - {0:0.00}.", MaxDia);
                        return;
                    }

                    string csTmp2 = string.Empty;
                    if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                    {
                        Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext, defTreeSpecies.FormFactorsTableName,
                            defTreeSpecies.FormFactorsDisplayCode);
                        float cA = 0.0f;
                        float cB = 0.0f;
                        if (
                            Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("ReportedFormFactor")) ==
                            0 && ff != null)
                        {
                            cA = (float)ff.Avalue;
                            cB = (float)ff.Bvalue;
                            if (Utils.ConvertToFloat(e.Value.ToString()) > 0 &&
                                Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                if (
                                    Utils.ConvertToFloat(
                                        advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                {
                                    float sqValue =
                                        (float)
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")));
                                    float fCalcFF = (float)Utils.ConvertToFloat(e.Value.ToString()) *
                                                    (cA + cB * sqValue);
                                    fCalcFF = (float)fCalcFF / Utils.ConvertToFloat(e.Value.ToString());
                                    if (fCalcFF > 0.99)
                                        fCalcFF = (float).99;
                                    csTmp2 = string.Format("%2.0f", fCalcFF * 100);
                                    advBandedGridViewTrees.SetFocusedRowCellValue("ReportFormFactor", csTmp2);
                                }
                            }
                        }
                    }
                    break;
                case "FormPoint":
                    if (Utils.ConvertToInt(e.Value.ToString()) < 4 || Utils.ConvertToInt(e.Value.ToString()) > 50)
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) != 0)
                        {
                            e.Valid = false;
                            e.ErrorText = "Form Point MUST be 4 - 50.";
                            return;
                        }
                    }
                    else
                    {
                        defTreeFP = Utils.ConvertToTiny(e.Value.ToString());
                    }
                    break;
                case "ReportedFormFactor":
                    if (Utils.ConvertToInt(e.Value.ToString()) < 25 && Utils.ConvertToInt(e.Value.ToString()) > 99)
                    {
                        e.Valid = false;
                        e.ErrorText = "Form Factor MUST be 25 - 99.";
                        return;
                    }
                    break;
                case "TopDiameterFractionCode":
                    if (Utils.IsValidInt(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length == 1)
                            e.Value = e.Value.ToString() + "0";
                    }
                    if (e.Value.ToString().StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) > 0 &&
                            Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) !=
                                Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")))
                            {
                                e.Valid = false;
                                e.ErrorText =
                                    "When TDF is 0 (Total Height), Bole Height and Total Height MUST be equal.";
                            }
                        }
                        else if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) > 0)
                        {
                            advBandedGridViewTrees.SetFocusedRowCellValue("TotalHeight",
                                advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight"));
                        }
                        else if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            advBandedGridViewTrees.SetFocusedRowCellValue("BoleHeight",
                                advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight"));
                        }
                    }
                    defTreeTDF = e.Value.ToString();
                    ;
                    break;
                case "BoleHeight":
                    float MaxHeight = 0;
                    if (defTreeSpecies == null)
                        GetTreeSpecies(advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                    else
                        MaxHeight = (float)defTreeSpecies.MaxHeight;
                    if (MaxHeight == 0)
                        MaxHeight = 351;
                    if (Utils.ConvertToFloat(e.Value.ToString()) <=
                        Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("FormPoint")) ||
                        Utils.ConvertToFloat(e.Value.ToString()) > MaxHeight)
                    {
                        e.Valid = false;
                        e.ErrorText = string.Format(
                            "Bole Height MUST be greater than Form Point and less than {0:0.0}", MaxHeight);
                        return;
                    }
                    if (advBandedGridViewTrees.GetFocusedRowCellDisplayText("TopDiameterFractionCode").StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0 &&
                            Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(e.Value.ToString()) > 0 !=
                                Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                advBandedGridViewTrees.SetFocusedRowCellValue("TotalHeight", e.Value.ToString());
                            }
                        }
                        else
                        {
                            advBandedGridViewTrees.SetFocusedRowCellValue("TotalHeight", e.Value.ToString());
                            if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                            {
                                Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext,
                                    defTreeSpecies.FormFactorsTableName, defTreeSpecies.FormFactorsDisplayCode);
                                float cA = 0.0f;
                                float cB = 0.0f;
                                if (
                                    Utils.ConvertToInt(
                                        advBandedGridViewTrees.GetFocusedRowCellDisplayText("ReportedFormFactor")) == 0 &&
                                    ff != null)
                                {
                                    cA = (float)ff.Avalue;
                                    cB = (float)ff.Bvalue;
                                    if (
                                        Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh")) >
                                        0 &&
                                        Utils.ConvertToFloat(
                                            advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                                    {
                                        if (
                                            Utils.ConvertToFloat(
                                                advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                        {
                                            float sqValue =
                                                (float)
                                                ((float)17.3 /
                                                 (float)
                                                 Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                                ((float)17.3 /
                                                 (float)
                                                 Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")));
                                            float fCalcFF =
                                                (float)Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh")) * (cA + cB * sqValue);
                                            fCalcFF = (float)fCalcFF / Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh"));
                                            if (fCalcFF > 0.99)
                                                fCalcFF = (float).99;
                                            csTmp2 = string.Format("{0:0.0}", fCalcFF * 100);
                                            advBandedGridViewTrees.SetFocusedRowCellValue("TotalHeight", csTmp2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 0 &&
                             advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh") != "+")
                    {
                        if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) <
                            Utils.ConvertToInt(e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Bole Height MUST be less than Total Height.";
                            return;
                        }
                    }
                    break;
                case "TotalHeight":
                    if (advBandedGridViewTrees.GetFocusedRowCellDisplayText("TopDiameterFractionCode").StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0 &&
                            Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(e.Value.ToString()) > 0 !=
                                Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) >
                                0)
                            {
                                advBandedGridViewTrees.SetFocusedRowCellValue("BoleHeight", e.Value.ToString());
                            }
                        }
                        else
                        {
                            advBandedGridViewTrees.SetFocusedRowCellValue("BoleHeight", e.Value.ToString());
                        }
                    }
                    else if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) > 0 && Utils.ConvertToInt(e.Value.ToString()) > 0)
                    {
                        if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) > Utils.ConvertToInt(e.Value.ToString()))
                        {
                            e.Valid = false;
                            ;
                            e.ErrorText = "Total Height MUST be greater than Bole Height.";
                            return;
                        }
                    }
                    else if (Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("BoleHeight")) == 0 && Utils.ConvertToInt(e.Value.ToString()) > 0)
                    {
                        e.Valid = false;
                        e.ErrorText = "MUST enter Bole Height if Total Height entered.";
                        return;
                    }
                    if (defTreeSpecies == null)
                        GetTreeSpecies(advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                    if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                    {
                        Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext, defTreeSpecies.FormFactorsTableName,
                            defTreeSpecies.FormFactorsDisplayCode);
                        float cA = 0.0f;
                        float cB = 0.0f;
                        if (
                            Utils.ConvertToInt(advBandedGridViewTrees.GetFocusedRowCellDisplayText("ReportedFormFactor")) ==
                            0 && ff != null)
                        {
                            if (Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh")) > 0 &&
                                Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                if (
                                    Utils.ConvertToFloat(
                                        advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                {
                                    cA = (float)ff.Avalue;
                                    cB = (float)ff.Bvalue;
                                    float sqValue =
                                        (float)
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             advBandedGridViewTrees.GetFocusedRowCellDisplayText("TotalHeight")));
                                    float fCalcFF =
                                        (float)
                                        Utils.ConvertToFloat(advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh")) *
                                        (cA + cB * sqValue);
                                    fCalcFF = (float)fCalcFF /
                                              Utils.ConvertToFloat(
                                                  advBandedGridViewTrees.GetFocusedRowCellDisplayText("Dbh"));
                                    if (fCalcFF > 0.99)
                                        fCalcFF = (float).99;
                                    csTmp2 = string.Format("{0:0.0}", fCalcFF * 100);
                                    advBandedGridViewTrees.SetFocusedRowCellValue("ReportFormFactor", csTmp2);
                                }
                            }
                        }
                    }
                    break;
                case "CrownPositionDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesCrownPositionExist(ref projectContext, CurrentProject.CrownPositionTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "CP is invalid.";
                            return;
                        }
                    }
                    break;
                case "CrownRatioDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) < 1 || Utils.ConvertToInt(e.Value.ToString()) > 99)
                        {
                            e.Valid = false;
                            e.ErrorText = "CR must be 1 - 99.";
                            return;
                        }
                    }
                    else
                    {
                        e.Valid = true;
                        e.ErrorText = "";
                    }
                    break;
                case "VigorDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesVigorExist(ref projectContext, CurrentProject.VigorTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Vigor is invalid.";
                            return;
                        }
                    }
                    break;
                case "DamageDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesDamageExist(ref projectContext, CurrentProject.DamageTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Damage is invalid.";
                            return;
                        }
                    }
                    break;
                case "UserDefinedDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesT5Exist(ref projectContext, CurrentProject.UserDefinedTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Tree Status is invalid.";
                            return;
                        }
                    }
                    break;
                case "Sort1":
                case "Sort2":
                case "Sort3":
                case "Sort4":
                case "Sort5":
                case "Sort6":
                case "Sort7":
                case "Sort8":
                case "Sort9":
                case "Sort10":
                case "Sort11":
                case "Sort12":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        advBandedGridViewTrees.FocusedRowHandle = advBandedGridViewTrees.FocusedRowHandle + 1;
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
                    }
                    else
                    {
                        if (defTreeSpecies == null)
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                        Sort sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                        if (sRec == null)
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Sort is invalid.";
                            //return;
                            PopupSortForm sortForm = new PopupSortForm(ref projectContext, CurrentProject.SortsTableName, defTreeSpecies.SpeciesGroup);
                            sortForm.StartPosition = FormStartPosition.CenterParent;
                            sortForm.ShowDialog();
                            if (!string.IsNullOrEmpty(sortForm.code))
                            {
                                e.Value = sortForm.code;
                                sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                            }
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Sort is invalid.";
                                return;
                            }
                            sortForm.Dispose();
                        }
                    }
                    break;
                case "Grade1":
                case "Grade2":
                case "Grade3":
                case "Grade4":
                case "Grade5":
                case "Grade6":
                case "Grade7":
                case "Grade8":
                case "Grade9":
                case "Grade10":
                case "Grade11":
                case "Grade12":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (defTreeSpecies == null)
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                        Grade gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                        if (gRec == null)
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Grade is invalid.";
                            //return;
                            PopupGradeForm gradeForm = new PopupGradeForm(ref projectContext, CurrentProject.GradesTableName, defTreeSpecies.SpeciesGroup);
                            gradeForm.StartPosition = FormStartPosition.CenterParent;
                            gradeForm.ShowDialog();
                            if (!string.IsNullOrEmpty(gradeForm.code))
                            {
                                e.Value = gradeForm.code;
                                gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                            }
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Grade is invalid.";
                                return;
                            }
                            gradeForm.Dispose();
                        }
                    }
                    break;
                case "Length1":
                case "Length2":
                case "Length3":
                case "Length4":
                case "Length5":
                case "Length6":
                case "Length7":
                case "Length8":
                case "Length9":
                case "Length10":
                case "Length11":
                case "Length12":
                    break;
                case "BdFtLd1":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort2"];
                    break;
                case "BdFtLd2":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort3"];
                    break;
                case "BdFtLd3":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort4"];
                    break;
                case "BdFtLd4":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort5"];
                    break;
                case "BdFtLd5":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort6"];
                    break;
                case "BdFtLd6":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort7"];
                    break;
                case "BdFtLd7":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort8"];
                    break;
                case "BdFtLd8":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort9"];
                    break;
                case "BdFtLd9":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort10"];
                    break;
                case "BdFtLd10":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort11"];
                    break;
                case "BdFtLd11":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["Sort12"];
                    break;
                case "BdFtLd12":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        advBandedGridViewTrees.FocusedRowHandle = advBandedGridViewTrees.FocusedRowHandle + 1;
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["SpeciesAbbreviation"];
                    }
                    break;
                case "BdFtPd1":
                case "BdFtPd2":
                case "BdFtPd3":
                case "BdFtPd4":
                case "BdFtPd5":
                case "BdFtPd6":
                case "BdFtPd7":
                case "BdFtPd8":
                case "BdFtPd9":
                case "BdFtPd10":
                case "BdFtPd11":
                case "BdFtPd12":
                    break;
            }
        }

        private bool CheckMinSortDiaError(Sort pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Sort", ""));
            if (Calc.seg[i - 1].tdia > 0)
            {
                if (Calc.seg[i - 1].tdia < pRec.MinimumDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMinSortDiaError(int segment, Sort pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].tdia > 0)
            {
                if (Calc.seg[segment - 1].tdia < pRec.MinimumDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMaxSortDiaError(Sort pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Sort", ""));
            if (Calc.seg[i - 1].bdia > 0)
            {
                if (Calc.seg[i - 1].bdia > pRec.MaximumButtDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMaxSortDiaError(int segment, Sort pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].bdia > 0)
            {
                if (Calc.seg[segment - 1].bdia > pRec.MaximumButtDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMinSortLengthError(Sort pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Sort", ""));
            if (Calc.seg[i - 1].len > 0)
            {
                if (Calc.seg[i - 1].len < pRec.MinimumLength)
                    return true;
            }
            return false;
        }

        private bool CheckMinSortLengthError(int segment, Sort pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].len > 0)
            {
                if (Calc.seg[segment - 1].len < pRec.MinimumLength)
                    return true;
            }
            return false;
        }

        private bool CheckMinSortVolumeError(Sort pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Sort", ""));
            if (Calc.seg[i - 1].scribner_net > 0)
            {
                if (Calc.seg[i - 1].scribner_net < pRec.MinimumBdftPerLog)
                    return true;
            }
            return false;
        }

        private bool CheckMinSortVolumeError(int segment, Sort pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].scribner_net > 0)
            {
                if (Calc.seg[segment - 1].scribner_net < pRec.MinimumBdftPerLog)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeDiaError(Grade pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Grade", ""));
            if (Calc.seg[i - 1].tdia > 0)
            {
                if (Calc.seg[i - 1].tdia < pRec.MinimumDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeDiaError(int segment, Grade pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].tdia > 0)
            {
                if (Calc.seg[segment - 1].tdia < pRec.MinimumDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMaxGradeDiaError(Grade pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Grade", ""));
            if (Calc.seg[i - 1].bdia > 0)
            {
                if (Calc.seg[i - 1].bdia > pRec.MaximumButtDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMaxGradeDiaError(int segment, Grade pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].bdia > 0)
            {
                if (Calc.seg[segment - 1].bdia > pRec.MaximumButtDiameter)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeLengthError(Grade pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Grade", ""));
            if (Calc.seg[i - 1].len > 0)
            {
                if (Calc.seg[i - 1].len < pRec.MinimumLength)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeLengthError(int segment, Grade pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].len > 0)
            {
                if (Calc.seg[segment - 1].len < pRec.MinimumLength)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeVolumeError(Grade pRec, string fieldName)
        {
            int i = Utils.ConvertToInt(fieldName.Replace("Grade", ""));
            if (Calc.seg[i - 1].scribner_net > 0)
            {
                if (Calc.seg[i - 1].scribner_net < pRec.MinimumBdftPerLog)
                    return true;
            }
            return false;
        }

        private bool CheckMinGradeVolumeError(int segment, Grade pRec, string fieldName)
        {
            if (Calc.seg[segment - 1].scribner_net > 0)
            {
                if (Calc.seg[segment - 1].scribner_net < pRec.MinimumBdftPerLog)
                    return true;
            }
            return false;
        }

        private void AddPlot(string pPlot)
        {
            Plot rec = new Plot();
            rec.StandsId = CurrentStand.StandsId;
            rec.AddFlag = string.Empty;
            rec.Aspect = string.Empty;
            rec.Attachments = false;
            rec.BafInAt = string.Empty;
            rec.BasalAreaAcre = 0;
            rec.CruFlag = "Y";
            rec.Cruiser = string.Empty;
            rec.DwFlag = string.Empty;
            rec.Environmental = string.Empty;
            rec.Habitat = string.Empty;
            rec.NetBfAcre = 0;
            rec.NetCfAcre = 0;
            rec.Notes = string.Empty;
            rec.PlotDate = DateTime.Now;
            rec.RefFlag = string.Empty;
            rec.Slope = string.Empty;
            rec.Species = string.Empty;
            rec.TreatmentDisplayCode = string.Empty;
            rec.TreesPerAcre = 0;
            rec.UserPlotNumber = pPlot;
            rec.X = 0;
            rec.Y = 0;
            rec.Z = 0;
            projectContext.Plots.Add(rec);
            projectContext.SaveChanges();
        }

        private void GetTreeSpecies(string pSpecies)
        {
            if (!string.IsNullOrEmpty(pSpecies))
            {
                if (Utils.IsValidInt(pSpecies))
                {
                    defTreeSpecies = ProjectBLL.GetSpecieByCode(ref projectContext, CurrentProject.SpeciesTableName,
                        Utils.ConvertToInt(pSpecies));
                }
                else
                {
                    defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName,
                        pSpecies);
                }
            }
        }

        private void advBandedGridViewTrees_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            string sort1 = string.Empty;
            string sort2 = string.Empty;
            string sort3 = string.Empty;
            string sort4 = string.Empty;
            string sort5 = string.Empty;
            string sort6 = string.Empty;
            string sort7 = string.Empty;
            string sort8 = string.Empty;
            string sort9 = string.Empty;
            string sort10 = string.Empty;
            string sort11 = string.Empty;
            string sort12 = string.Empty;

            string grade1 = string.Empty;
            string grade2 = string.Empty;
            string grade3 = string.Empty;
            string grade4 = string.Empty;
            string grade5 = string.Empty;
            string grade6 = string.Empty;
            string grade7 = string.Empty;
            string grade8 = string.Empty;
            string grade9 = string.Empty;
            string grade10 = string.Empty;
            string grade11 = string.Empty;
            string grade12 = string.Empty;

            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort1") != null)
                sort1 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort1").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort2") != null)
                sort2 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort2").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort3") != null)
                sort3 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort3").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort4") != null)
                sort4 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort4").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort5") != null)
                sort5 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort5").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort6") != null)
                sort6 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort6").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort7") != null)
                sort7 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort7").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort8") != null)
                sort8 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort8").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort9") != null)
                sort9 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort9").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort10") != null)
                sort10 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort10").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort11") != null)
                sort11 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort11").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort12") != null)
                sort12 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Sort12").ToString();

            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade1") != null)
                grade1 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade1").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade2") != null)
                grade2 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade2").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade3") != null)
                grade3 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade3").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade4") != null)
                grade4 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade4").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade5") != null)
                grade5 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade5").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade6") != null)
                grade6 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade6").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade7") != null)
                grade7 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade7").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade8") != null)
                grade8 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade8").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade9") != null)
                grade9 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade9").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade10") != null)
                grade10 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade10").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade11") != null)
                grade11 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade11").ToString();
            if (advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade12") != null)
                grade12 = advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "Grade12").ToString();

            Tree selectedTree = treesBindingSource.Current as Tree;

            DoCalcForTree(true, selectedTree, false);
            
            if (selectedTree.TreeType != "C")
            {
                //Utils.DeleteSegmentsForTree(ref projectContext, selectedTree);
                //Utils.tPrices = ProjectBLL.GetPrices(ref projectContext, CurrentProject.PriceTableName);
                //Utils.CreateSegmentsFromTree(ref projectContext, selectedTree);

                ProjectBLL.DeleteCountWithDiaTable(ref projectContext, (int)selectedTree.StandsId);
                ProjectBLL.BuildCountWithDiaTable(ref projectContext, (int)selectedTree.StandsId);
            }

            //if (defTreeSpecies == null)
            //    defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, advBandedGridViewTrees.GetRowCellValue(e.RowHandle, "SpeciesAbbreviation").ToString());
            //ProcessSortValidation(e, "Sort1", sort1, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort2", sort2, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort3", sort3, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort4", sort4, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort5", sort5, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort6", sort6, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort7", sort7, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort8", sort8, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort9", sort9, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort10", sort10, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort11", sort11, selectedTree.TreesId);
            //ProcessSortValidation(e, "Sort12", sort12, selectedTree.TreesId);

            //ProcessGradeValidation(e, "Grade1", grade1, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade2", grade2, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade3", grade3, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade4", grade4, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade5", grade5, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade6", grade6, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade7", grade7, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade8", grade8, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade9", grade9, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade10", grade10, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade11", grade11, selectedTree.TreesId);
            //ProcessGradeValidation(e, "Grade12", grade12, selectedTree.TreesId);
        }

        private void ProcessGradeValidation(ValidateRowEventArgs e, string pColumn, string pField, int pId)
        {
            int seg = Convert.ToInt16(pColumn.Replace("Grade", ""));
            if (!string.IsNullOrEmpty(pField))
            {
                Grade gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, pField, defTreeSpecies.SpeciesGroup);
                if (gRec.MinimumDiameter != null && gRec.MinimumDiameter > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                    if (CheckMinGradeDiaError(gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Dia Error for Grade ({0})", gRec.MinimumDiameter);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Dia Error for Grade"))
                                    segRec.Comments += string.Format("; Min Dia Error for Grade ({0})", gRec.MinimumDiameter);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Diameter Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Dia Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Dia Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MaximumButtDiameter != null && gRec.MaximumButtDiameter > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                    if (CheckMaxGradeDiaError(gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Max Butt Dia Error for Grade ({0})", gRec.MaximumButtDiameter);
                            else
                            {
                                if (!segRec.Comments.Contains("Max Butt Dia Error for Grade"))
                                    segRec.Comments += string.Format("; Max Butt Dia Error for Grade ({0})", gRec.MaximumButtDiameter);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Maximum Butt Diameter Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Max Butt Dia Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Max Butt Dia Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MinimumLength != null && gRec.MinimumLength > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                    if (CheckMinGradeLengthError(gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Len Error for Grade ({0})", gRec.MinimumLength);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Len Error for Grade"))
                                    segRec.Comments += string.Format("; Min Len Error for Grade ({0})", gRec.MinimumLength);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Length Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Len Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Len Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MinimumBdftPerLog != null && gRec.MinimumBdftPerLog > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                    if (CheckMinGradeVolumeError(gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Vol Error for Grade ({0})", gRec.MinimumBdftPerLog);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Vol Error for Grade"))
                                    segRec.Comments += string.Format("; Min Vol Error for Grade ({0})", gRec.MinimumBdftPerLog);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Volume Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Vol Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Vol Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProcessGradeValidation(int pSegmentNumber, string pColumn, string pField, Tree pTree)
        {
            if (!string.IsNullOrEmpty(pField))
            {
                Grade gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, pField, defTreeSpecies.SpeciesGroup);
                if (gRec.MinimumDiameter != null && gRec.MinimumDiameter > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                    if (CheckMinGradeDiaError(pSegmentNumber, gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Dia Error for Grade ({0})", gRec.MinimumDiameter);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Dia Error for Grade"))
                                    segRec.Comments += string.Format("; Min Dia Error for Grade ({0})", gRec.MinimumDiameter);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Diameter Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Dia Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Dia Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MaximumButtDiameter != null && gRec.MaximumButtDiameter > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                    if (CheckMaxGradeDiaError(pSegmentNumber, gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Max Butt Dia Error for Grade ({0})", gRec.MaximumButtDiameter);
                            else
                            {
                                if (!segRec.Comments.Contains("Max Butt Dia Error for Grade"))
                                    segRec.Comments += string.Format("; Max Butt Dia Error for Grade ({0})", gRec.MaximumButtDiameter);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Maximum Butt Diameter Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Max Butt Dia Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Max Butt Dia Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MinimumLength != null && gRec.MinimumLength > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                    if (CheckMinGradeLengthError(pSegmentNumber, gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Len Error for Grade ({0})", gRec.MinimumLength);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Len Error for Grade"))
                                    segRec.Comments += string.Format("; Min Len Error for Grade ({0})", gRec.MinimumLength);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Length Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Len Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Len Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                if (gRec.MinimumBdftPerLog != null && gRec.MinimumBdftPerLog > 0)
                {
                    Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                    if (CheckMinGradeVolumeError(pSegmentNumber, gRec, pColumn))
                    {
                        if (segRec != null)
                        {
                            if (string.IsNullOrEmpty(segRec.Comments))
                                segRec.Comments = string.Format("Min Vol Error for Grade ({0})", gRec.MinimumBdftPerLog);
                            else
                            {
                                if (!segRec.Comments.Contains("Min Vol Error for Grade"))
                                    segRec.Comments += string.Format("; Min Vol Error for Grade ({0})", gRec.MinimumBdftPerLog);
                            }
                            projectContext.SaveChanges();
                        }
                        //e.Valid = false;
                        //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn], "Minimum Volume Error for Grade.");
                        return;
                    }
                    else
                    {
                        if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                        {
                            if (segRec.Comments.Contains("Min Vol Error for Grade"))
                            {
                                int found1 = segRec.Comments.IndexOf("Min Vol Error for Grade");
                                int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                if (found1 != found2 && found1 >= 0)
                                {
                                    segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                    projectContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProcessSortValidation(ValidateRowEventArgs e, string pColumn, string pField, int pId)
        {
            int seg = Convert.ToInt16(pColumn.Replace("Sort", ""));
            if (!string.IsNullOrEmpty(pField))
            {
                if (pField == "P" || pField == "G")
                {
                    if (pField == "P")
                    {
                        Pole poleRec = ProjectBLL.GetPoleRecord(ref projectContext, CurrentProject.PolesTableName,
                            advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                    }
                    else
                    {
                        Piling pilingRec = ProjectBLL.GetPilingRecord(ref projectContext, CurrentProject.PilingTableName,
                            advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation"));
                    }
                }
                else
                {
                    Sort sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, pField,
                        defTreeSpecies.SpeciesGroup);
                    if (sRec.MinimumDiameter != null && sRec.MinimumDiameter > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                        if (CheckMinSortDiaError(sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Min Dia Error for Sort ({0})", sRec.MinimumDiameter);
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Dia Error for Sort"))
                                        segRec.Comments += string.Format("; Min Dia Error for Sort ({0})", sRec.MinimumDiameter);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Diameter Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Dia Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Dia Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MaximumButtDiameter != null && sRec.MaximumButtDiameter > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                        if (CheckMaxSortDiaError(sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Max Butt Dia Error for Sort ({0})", sRec.MaximumButtDiameter);
                                else
                                {
                                    if (!segRec.Comments.Contains("Max Butt Dia Error for Sort"))
                                        segRec.Comments += string.Format("; Max Butt Dia Error for Sort ({0})", sRec.MaximumButtDiameter);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Maximum Butt Diameter Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Max Butt Dia Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Max Butt Dia Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MinimumLength != null && sRec.MinimumLength > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                        if (CheckMinSortLengthError(sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Min Len Error for Sort ({0})", sRec.MinimumLength);
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Len Error for Sort"))
                                        segRec.Comments += string.Format("; Min Len Error for Sort ({0})", sRec.MinimumLength);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Length Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Len Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Len Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MinimumBdftPerLog != null && sRec.MinimumBdftPerLog > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pId && s.SegmentNumber == seg);
                        if (CheckMinSortVolumeError(sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Min Vol Error for Sort ({0})", sRec.MinimumBdftPerLog);
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Vol Error for Sort"))
                                        segRec.Comments += string.Format("; Min Vol Error for Sort ({0})", sRec.MinimumBdftPerLog);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Volume Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Vol Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Vol Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProcessSortValidation(int pSegmentNumber, string pColumn, string pField, Tree pTree)
        {
            if (!string.IsNullOrEmpty(pField))
            {
                if (pField == "P" || pField == "G")
                {
                    if (pField == "P")
                    {
                        Pole poleRec = ProjectBLL.GetPoleRecord(ref projectContext, CurrentProject.PolesTableName, pTree.SpeciesAbbreviation);
                    }
                    else
                    {
                        Piling pilingRec = ProjectBLL.GetPilingRecord(ref projectContext, CurrentProject.PilingTableName, pTree.SpeciesAbbreviation);
                    }
                }
                else
                {
                    Sort sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, pField, defTreeSpecies.SpeciesGroup);
                    if (sRec.MinimumDiameter != null && sRec.MinimumDiameter > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                        if (CheckMinSortDiaError(pSegmentNumber, sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                {
                                    segRec.Comments = string.Format("Min Dia Error for Sort ({0})", sRec.MinimumDiameter);
                                    gridViewSegments.SetFocusedRowCellValue("SortCode", segRec.Comments);
                                }
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Dia Error for Sort"))
                                    {
                                        segRec.Comments += string.Format("; Min Dia Error for Sort ({0})", sRec.MinimumDiameter);
                                        gridViewSegments.SetFocusedRowCellValue("SortCode", segRec.Comments);
                                    }
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Diameter Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Dia Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Dia Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        gridViewSegments.SetFocusedRowCellValue("SortCode", segRec.Comments);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MaximumButtDiameter != null && sRec.MaximumButtDiameter > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                        if (CheckMaxSortDiaError(pSegmentNumber, sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Max Butt Dia Error for Sort ({0})", sRec.MaximumButtDiameter);
                                else
                                {
                                    if (!segRec.Comments.Contains("Max Butt Dia Error for Sort"))
                                        segRec.Comments += string.Format("; Max Butt Dia Error for Sort ({0})", sRec.MaximumButtDiameter);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Maximum Butt Diameter Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Max Butt Dia Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Max Butt Dia Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MinimumLength != null && sRec.MinimumLength > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                        if (CheckMinSortLengthError(pSegmentNumber, sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Min Len Error for Sort ({0})", sRec.MinimumLength);
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Len Error for Sort"))
                                        segRec.Comments += string.Format("; Min Len Error for Sort ({0})", sRec.MinimumLength);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Length Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Len Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Len Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (sRec.MinimumBdftPerLog != null && sRec.MinimumBdftPerLog > 0)
                    {
                        Treesegment segRec = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == pTree.TreesId && s.SegmentNumber == pSegmentNumber);
                        if (CheckMinSortVolumeError(pSegmentNumber, sRec, pColumn))
                        {
                            if (sRec != null)
                            {
                                if (string.IsNullOrEmpty(segRec.Comments))
                                    segRec.Comments = string.Format("Min Vol Error for Sort ({0})", sRec.MinimumBdftPerLog);
                                else
                                {
                                    if (!segRec.Comments.Contains("Min Vol Error for Sort"))
                                        segRec.Comments += string.Format("; Min Vol Error for Sort ({0})", sRec.MinimumBdftPerLog);
                                }
                                projectContext.SaveChanges();
                            }
                            //e.Valid = false;
                            //advBandedGridViewTrees.SetColumnError(advBandedGridViewTrees.Columns[pColumn],
                            //    "Minimum Volume Error for Sort.");
                            return;
                        }
                        else
                        {
                            if (segRec.Comments != null && !string.IsNullOrEmpty(segRec.Comments))
                            {
                                if (segRec.Comments.Contains("Min Vol Error for Sort"))
                                {
                                    int found1 = segRec.Comments.IndexOf("Min Vol Error for Sort");
                                    int found2 = segRec.Comments.IndexOf(")", found1 + 1);
                                    if (found1 != found2 && found1 >= 0)
                                    {
                                        segRec.Comments = segRec.Comments.Remove(found1 + 1, found2 - found1);
                                        projectContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void advBandedGridViewTrees_InvalidValueException(object sender,
            DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            XtraMessageBox.Show(this, e.ErrorText, "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            IsError = true;
        }

        private void advBandedGridViewTrees_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            if (string.IsNullOrEmpty(e.ErrorText))
                e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void advBandedGridViewPlots_ValidatingEditor(object sender,
            DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            switch (advBandedGridViewPlots.FocusedColumn.FieldName)
            {
                case "UserPlotNumber":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        e.Valid = false;
                        e.ErrorText = "Plot Number is a required";
                    }
                    else
                    {
                        if (Utils.IsValidInt(e.Value.ToString()))
                            e.Value = e.Value.ToString().PadLeft(4, '0');
                        defPlotPlotNumber = e.Value.ToString();
                    }
                    break;
                case "Slope":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 150)
                        {
                            e.Valid = false;
                            e.ErrorText = "Slope is invalid";
                        }
                    }
                    break;
                case "Aspect":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesAspectExist(ref projectContext, CurrentProject.AspectsTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Aspect is invalid";
                        }
                    }
                    break;
                case "Species":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesSpeciesExist(ref projectContext, CurrentProject.SpeciesTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Species is invalid";
                        }
                    }
                    break;
            }
        }

        private void advBandedGridViewPlots_ValidateRow(object sender, ValidateRowEventArgs e)
        {

        }

        private void barButtonItem30_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Update Count Plots for Stand
        }

        private void barButtonItem31_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Update Plots for Stand
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Update Plot Totals");
            ProjectBLL.UpdatePlotsForStands(ref projectContext);
            //List<Stand> tStands = ProjectBLL.GetAllStands(ref projectContext, null);
            //foreach (var item in tStands)
            //{
            //    List<Plot> tPlots = projectContext.Plot.OrderBy(p => p.StandsId).ThenBy(p => p.UserPlotNumber).Where(p => p.StandsId == item.StandsId).ToList();

            //    foreach (Plot itemPlot in tPlots)
            //    {
            //        List<Tree> tTrees = projectContext.Trees.Where(t => t.PlotId == itemPlot.PlotsId).ToList();
            //        float ba = 0;
            //        float tpa = 0;
            //        float netcfpa = 0;
            //        float netbfpa = 0;
            //        foreach (var itemTree in tTrees)
            //        {
            //            ba += (float)itemTree.BasalArea;
            //            tpa += (float)itemTree.TreesPerAcre;
            //            netcfpa += (float)itemTree.TotalNetCubicPerAcre;
            //            netbfpa += (float)itemTree.TotalNetScribnerPerAcre;
            //        }
            //        itemPlot.BasalAreaAcre = ba;
            //        itemPlot.TreesPerAcre = tpa;
            //        itemPlot.NetCfAcre = netcfpa;
            //        itemPlot.NetBfAcre = netbfpa;
            //        if (tTrees.Count > 0)
            //        {
            //            if (tTrees[0].Dbh == 4.0)
            //                itemPlot.BafInAt = "DBH";
            //            else
            //                itemPlot.BafInAt = "FP";
            //        }
            //    }
            //}
            //projectContext.SaveChanges();
            SplashScreenManager.CloseForm();
        }

        private void barButtonItem32_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Update Stand Totals
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Update Stand Totals");

            gridViewStands.SelectAll();
            try
            {
                
                sColl = ReportsBLL.BuildStandSelection(gridViewStands);
                foreach (var item in sColl)
                {
                    Stand standItem = item;
                    Project.DAL.Project projectItem = CurrentProject;
                    ProjectBLL.UpdateTotalsForStand(ref projectContext, ref projectItem, ref standItem);
                }
                projectContext.SaveChanges();
            }
            catch
            {

            }
            gridViewStands.ClearSelection();
            SplashScreenManager.CloseForm();
        }

        private void gridViewStandInputOrig_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            gridViewStandInputGrown.FocusedRowHandle = e.FocusedRowHandle;
        }

        private void gridViewStandInputGrown_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            gridViewStandInputOrig.FocusedRowHandle = e.FocusedRowHandle;
        }

        private void gridViewStandInputOrig_TopRowChanged(object sender, EventArgs e)
        {
            SynchronizeTopRowIndex(sender as GridView);
        }

        private void gridViewStandInputGrown_TopRowChanged(object sender, EventArgs e)
        {
            SynchronizeTopRowIndex(sender as GridView);
        }

        public bool IsSynchronized
        {
            get { return _IsSynced; }
            set
            {
                _IsSynced = value;
                delta = gridViewStandInputOrig.TopRowIndex - gridViewStandInputGrown.TopRowIndex;
            }
        }

        private void SynchronizeTopRowIndex(GridView view)
        {
            if (!_IsSynced)
                return;
            if (view == gridViewStandInputOrig)
                gridViewStandInputGrown.TopRowIndex = gridViewStandInputOrig.TopRowIndex - delta;
            else
                gridViewStandInputOrig.TopRowIndex = gridViewStandInputGrown.TopRowIndex + delta;
        }

        //private void advBandedGridViewTrees_CellValueChanging(object sender, CellValueChangedEventArgs e)
        //{
        //GridView view = (GridView)sender;

        //switch (e.Column.FieldName)
        //{
        //    case "PlotFactorInput":
        //    case "TopDiameterFractionCode":
        //    case "FormPoint":
        //    case "ReportedFormFactor":
        //    case "CrownPositionDisplayCode":
        //    case "CrownRatioDisplayCode":
        //    case "VigorDisplayCode":
        //    case "DamageDisplayCode":
        //    case "UserDefinedDisplayCode":
        //        if (e.Value.ToString().Length == repositoryItem2.MaxLength)
        //        {
        //            GridColumn nextColumn = view.FocusedColumn;
        //            nextColumn = view.VisibleColumns[nextColumn.VisibleIndex + 1];
        //            view.FocusedColumn = nextColumn;
        //            view.ShowEditor();
        //        }
        //        break;
        //    case "AgeCode":
        //    case "Sort1":
        //    case "Sort2":
        //    case "Sort3":
        //    case "Sort4":
        //    case "Sort5":
        //    case "Sort6":
        //    case "Sort7":
        //    case "Sort8":
        //    case "Sort9":
        //    case "Sort10":
        //    case "Sort11":
        //    case "Sort12":
        //    case "Grade1":
        //    case "Grade2":
        //    case "Grade3":
        //    case "Grade4":
        //    case "Grade5":
        //    case "Grade6":
        //    case "Grade7":
        //    case "Grade8":
        //    case "Grade9":
        //    case "Grade10":
        //    case "Grade11":
        //    case "Grade12":
        //    case "BdFtLD1":
        //    case "BdFtLD2":
        //    case "BdFtLD3":
        //    case "BdFtLD4":
        //    case "BdFtLD5":
        //    case "BdFtLD6":
        //    case "BdFtLD7":
        //    case "BdFtLD8":
        //    case "BdFtLD9":
        //    case "BdFtLD10":
        //    case "BdFtLD11":
        //    case "BdFtLD12":
        //    case "BdFtDD1":
        //    case "BdFtDD2":
        //    case "BdFtDD3":
        //    case "BdFtDD4":
        //    case "BdFtDD5":
        //    case "BdFtDD6":
        //    case "BdFtDD7":
        //    case "BdFtDD8":
        //    case "BdFtDD9":
        //    case "BdFtDD10":
        //    case "BdFtDD11":
        //    case "BdFtDD12":
        //    case "BdFtPD1":
        //    case "BdFtPD2":
        //    case "BdFtPD3":
        //    case "BdFtPD4":
        //    case "BdFtPD5":
        //    case "BdFtPD6":
        //    case "BdFtPD7":
        //    case "BdFtPD8":
        //    case "BdFtPD9":
        //    case "BdFtPD10":
        //    case "BdFtPD11":
        //    case "BdFtPD12":
        //    case "CuFtLD1":
        //    case "CuFtLD2":
        //    case "CuFtLD3":
        //    case "CuFtLD4":
        //    case "CuFtLD5":
        //    case "CuFtLD6":
        //    case "CuFtLD7":
        //    case "CuFtLD8":
        //    case "CuFtLD9":
        //    case "CuFtLD10":
        //    case "CuFtLD11":
        //    case "CuFtLD12":
        //    case "CuFtDD1":
        //    case "CuFtDD2":
        //    case "CuFtDD3":
        //    case "CuFtDD4":
        //    case "CuFtDD5":
        //    case "CuFtDD6":
        //    case "CuFtDD7":
        //    case "CuFtDD8":
        //    case "CuFtDD9":
        //    case "CuFtDD10":
        //    case "CuFtDD11":
        //    case "CuFtDD12":
        //        if (e.Value.ToString().Length == repositoryItem1.MaxLength)
        //        {
        //            GridColumn nextColumn = view.FocusedColumn;
        //            nextColumn = view.VisibleColumns[nextColumn.VisibleIndex + 1];
        //            view.FocusedColumn = nextColumn;
        //            view.ShowEditor();
        //        }
        //        break;
        //    case "BoleHeight":
        //    case "TotalHeight":
        //        if (e.Value.ToString().Length == repositoryItem3.MaxLength)
        //        {
        //            GridColumn nextColumn = view.FocusedColumn;
        //            nextColumn = view.VisibleColumns[nextColumn.VisibleIndex + 1];
        //            view.FocusedColumn = nextColumn;
        //            view.ShowEditor();
        //        }
        //        break;
        //}
        //}

        private void repositoryItem2_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;

            //if (IsError)
            //{
            //    IsError = false;
            //    return;
            //}

            if (edit.Text.Length == edit.Properties.MaxLength)
            {
                //advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.VisibleColumns[advBandedGridViewTrees.FocusedColumn.VisibleIndex + 1];
                SendKeys.Send("{ENTER}");
            }
        }

        private void repositoryItem1_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;

            if (IsError)
            {
                IsError = false;
                return;
            }

            if (edit.Text.Length == edit.Properties.MaxLength)
            {
                //advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.VisibleColumns[advBandedGridViewTrees.FocusedColumn.VisibleIndex + 1];
                SendKeys.Send("{ENTER}");
            }
        }

        private void repositoryItem3_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;

            if (IsError)
            {
                IsError = false;
                return;
            }

            if (edit.Text.Length == edit.Properties.MaxLength)
            {
                //advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.VisibleColumns[advBandedGridViewTrees.FocusedColumn.VisibleIndex + 1];
                SendKeys.Send("{ENTER}");
            }
        }

        private void advBandedGridViewTrees_RowUpdated(object sender, RowObjectEventArgs e)
        {
            needToUpdatePlotsStands = false;
            if (string.IsNullOrEmpty(advBandedGridViewTrees.GetFocusedRowCellDisplayText("SpeciesAbbreviation")))
            {
                advBandedGridViewTrees.SelectRow(advBandedGridViewTrees.FocusedRowHandle);
                Tree xTree = treesBindingSource.Current as Tree;
                projectContext.Trees.Remove(xTree);
                projectContext.SaveChanges();
                LoadTreeDataSource();
            }
            else
            {
                //DoCalcForTree(true, null);
                needToUpdatePlotsStands = true;
            }
        }

        private void DoCalcForTree(bool pHandleErrors, Tree pTree, bool UpdateSegmentGrid)
        {
            this.treesBindingSource.EndEdit();

            int origNumberOfSegments = 0;
            Stand st = MainForm.CurrentStand;

            Tree SelectedTree = pTree;

            try
            {
                origNumberOfSegments = projectContext.Treesegments.Where(s => s.TreesId == SelectedTree.TreesId).Count();
            }
            catch
            {
                return;
            }

            if (CurrentPlot == null)
            {
                CurrentPlot =
                    projectContext.Plots.FirstOrDefault(
                        p => p.StandsId == SelectedTree.StandsId && p.UserPlotNumber == SelectedTree.PlotNumber);
            }
            if (SelectedTree.PlotId == null)
            {
                SelectedTree.PlotId = CurrentPlot.PlotsId;
                SelectedTree.GroupPlot = CurrentPlot.UserPlotNumber;
            }
            if (SelectedTree.GroupTree == 0)
                SelectedTree.GroupTree = SelectedTree.TreeNumber;

            Calc.Init();
            if (string.IsNullOrEmpty(SelectedTree.SpeciesAbbreviation))
                return;
            if (string.IsNullOrEmpty(SelectedTree.PlotFactorInput))
                return;
            Calc.SetCalcContextProjectStand(ref projectContext, MainForm.CurrentProject, ref st);
            Calc.projectDataSource = ProjectDataSource;
            Calc.tree = SelectedTree;
            ProjectBLL.SetTreeType(ref SelectedTree);
            if (SelectedTree.TreeType == "C")
            {
                Treesegment seg =
                    projectContext.Treesegments.FirstOrDefault(
                        s => s.StandsId == st.StandsId && s.TreesId == SelectedTree.TreesId);
                if (seg == null)
                {
                    Treesegment newSegment = new Treesegment();
                    newSegment.StandsId = st.StandsId;
                    newSegment.TreesId = SelectedTree.TreesId;
                    newSegment.PlotId = SelectedTree.PlotId;
                    newSegment.Age = SelectedTree.Age;
                    newSegment.Ao = 0;
                    newSegment.Bark = 0;
                    newSegment.BdFtDd = 0;
                    newSegment.BdFtLd = 0;
                    newSegment.BdFtPd = string.Empty;
                    newSegment.BoleHeight = 0;
                    newSegment.CalcAccumulatedLength = 0;
                    newSegment.CalcButtDia = 0;
                    newSegment.CalcLen = 0;
                    newSegment.CalcTopDia = 0;
                    newSegment.CalcTotalHtUsedYn = "N";
                    newSegment.Ccf = 0;
                    newSegment.Comments = string.Empty;
                    newSegment.CrownPosition = string.Empty;
                    newSegment.CrownRatio = string.Empty;
                    newSegment.CubicGrossVolume = 0;
                    newSegment.CubicGrossVolumePerAce = 0;
                    newSegment.CubicNetVolume = 0;
                    newSegment.CubicNetVolumePerAce = 0;
                    newSegment.CuFtDd = 0;
                    newSegment.CuFtLd = 0;
                    newSegment.Damage = string.Empty;
                    if (SelectedTree.Dbh != null && SelectedTree.Dbh > 0)
                        newSegment.Dbh = SelectedTree.Dbh;
                    newSegment.FormFactor = 0;
                    newSegment.FormPoint = SelectedTree.FormPoint;
                    newSegment.GroupPlotNumber = SelectedTree.PlotNumber;
                    newSegment.GroupTreeNumber = SelectedTree.TreeNumber;
                    newSegment.GradeCode = string.Empty;
                    newSegment.Length = string.Empty;
                    newSegment.LogValue = 0;
                    newSegment.Mbf = 0;
                    newSegment.PfType = SelectedTree.PfType;
                    newSegment.PlotFactorInput = SelectedTree.PlotFactorInput;
                    newSegment.PlotNumber = SelectedTree.PlotNumber;
                    newSegment.ScribnerGrossVolume = 0;
                    newSegment.ScribnerGrossVolumePerAce = 0;
                    newSegment.ScribnerNetVolume = 0;
                    newSegment.ScribnerNetVolumePerAce = 0;
                    newSegment.SegmentNumber = 0;
                    newSegment.SortCode = string.Empty;
                    newSegment.TotalHeight = 0;
                    newSegment.TreeBasalArea = 0;
                    newSegment.TreeStatusDisplayCode = string.Empty;
                    newSegment.TreeTreesPerAcre = 0;
                    newSegment.UserDefined = string.Empty;
                    newSegment.Vigor = string.Empty;
                    newSegment.Species = SelectedTree.SpeciesAbbreviation;
                    newSegment.Tdf = SelectedTree.TopDiameterFractionCode;
                    newSegment.TreeCount = SelectedTree.TreeCount;
                    newSegment.TreeNumber = (short)SelectedTree.TreeNumber;
                    newSegment.TreeType = SelectedTree.TreeType;
                    projectContext.Treesegments.Add(newSegment);
                    projectContext.SaveChanges();
                    seg = projectContext.Treesegments.FirstOrDefault(s => s.StandsId == st.StandsId && s.TreesId == SelectedTree.TreesId);
                }
                CountWithDbh cwd = null;
                AdjCruisePlot acp = null;
                if (SelectedTree.Dbh != null && SelectedTree.Dbh > 0)
                    cwd = ProjectBLL.GetCountWithDbh(ref projectContext, SelectedTree);
                else
                    acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == SelectedTree.StandsId && t.Species == SelectedTree.SpeciesAbbreviation);
                int defaultFP = 0;
                int fp = 0;
                float pf = 0;

                if (Utils.IsValidFloat(SelectedTree.PlotFactorInput))
                    pf = Utils.ConvertToFloat(SelectedTree.PlotFactorInput);
                else
                    pf = Utils.ConvertToFloat(ProjectBLL.GetPlotFactor(st, SelectedTree));
                if (pf.ToString().Contains("."))
                    pf = (float) Math.Round(pf,
                        2); //.PlotFactorInput; // ProjectBLL.GetPlotFactor(MainForm.CurrentStand, treeItem);
                else
                    pf = (float) Math.Round(pf, 0);

                if (SelectedTree.FormPoint == null || SelectedTree.FormPoint == 0)
                    fp = defaultFP;
                else
                    fp = (int) SelectedTree.FormPoint;
                if (cwd != null)
                {
                    //SelectedTree.BasalArea = pf * SelectedTree.TreeCount / ((cwd.CalcFf / 100) * (cwd.CalcFf / 100));
                    //SelectedTree.TreesPerAcre = SelectedTree.BasalArea / (0.005454154 * (SelectedTree.Dbh * SelectedTree.Dbh));

                    ///////////////
                    if (fp == 4)
                        SelectedTree.BasalArea  = pf * SelectedTree.TreeCount;
                    else
                        SelectedTree.BasalArea = pf * SelectedTree.TreeCount / ((cwd.CalcFf / 100) * (cwd.CalcFf / 100));
                    SelectedTree.TreesPerAcre = SelectedTree.BasalArea / (0.005454154 * (SelectedTree.Dbh * SelectedTree.Dbh));
                    SelectedTree.TotalHeight = (short)0; // cwd.CalcTotalHt;
                    SelectedTree.ReportedFormFactor = (short)0; // cwd.CalcFf;
                    SelectedTree.TotalCuFtGrossVolume = Math.Round(cwd.GrossCalcCfTree, 0);
                    SelectedTree.TotalCuFtNetVolume = Math.Round(cwd.NetCalcCfTree, 0);
                    SelectedTree.TotalBdFtGrossVolume = Math.Round((double)cwd.GrossCalcBfTree, 0);
                    SelectedTree.TotalBdFtNetVolume = Math.Round((double)cwd.NetCalcBfTree, 0);
                    SelectedTree.TotalGrossCubicPerAcre = SelectedTree.TotalCuFtGrossVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalNetCubicPerAcre = SelectedTree.TotalCuFtNetVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalGrossScribnerPerAcre = SelectedTree.TotalBdFtGrossVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalNetScribnerPerAcre = SelectedTree.TotalBdFtNetVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalCcf = (SelectedTree.TotalNetCubicPerAcre * st.NetGeographicAcres) / 100;
                    SelectedTree.TotalMbf = (SelectedTree.TotalNetScribnerPerAcre * st.NetGeographicAcres) / 1000;
                    //////////////
                }
                else
                {
                    //SelectedTree.BasalArea = pf * SelectedTree.TreeCount / ((acp.Ff) * (acp.Ff));
                    //SelectedTree.TreesPerAcre = 0;

                    ///////
                    SelectedTree.ReportedFormFactor = (short)0; // (acp.Ff * 100);
                    SelectedTree.TotalHeight = (short)0; // acp.TotalHeight;
                    if (acp.BasalArea != null)
                        SelectedTree.BasalArea = acp.BasalArea; // pf * SelectedTree.TreeCount / (acp.Ff * acp.Ff); //(pf * rec1.TreeCount)/(ss.Ff * ss.Ff);
                    else
                        SelectedTree.BasalArea = 0;
                    if (acp.TreesPerAcre != null)
                        SelectedTree.TreesPerAcre = acp.TreesPerAcre; // SelectedTree.BasalArea / (0.005454154 * (acp.QmDbh * acp.QmDbh)); //rec1.BasalArea/(0.005454154*(rec1.Dbh * rec1.Dbh)); //segItem.LogsPerAcre = acp.LogsPerAcre / standMaster.Plots;
                    else
                        SelectedTree.TreesPerAcre = 0;
                    if (acp.GrossCuFtLogs != null)
                        SelectedTree.TotalCuFtGrossVolume = acp.GrossCuFtLogs;
                    else
                        SelectedTree.TotalCuFtGrossVolume = 0;
                    if (acp.NetCuFtLogs != null)
                        SelectedTree.TotalCuFtNetVolume = acp.NetCuFtLogs;
                    else
                        SelectedTree.TotalCuFtNetVolume = 0;
                    if (acp.GrossBdFtLogs != null)
                        SelectedTree.TotalBdFtGrossVolume = acp.GrossBdFtLogs;
                    else
                        SelectedTree.TotalBdFtGrossVolume = 0;
                    if (acp.NetBdFtLogs != null)
                        SelectedTree.TotalBdFtNetVolume = acp.NetBdFtLogs;
                    else
                        SelectedTree.TotalBdFtNetVolume = 0;

                    SelectedTree.TotalGrossCubicPerAcre = SelectedTree.TotalCuFtGrossVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalNetCubicPerAcre = SelectedTree.TotalCuFtNetVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalGrossScribnerPerAcre = SelectedTree.TotalBdFtGrossVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalNetScribnerPerAcre = SelectedTree.TotalBdFtNetVolume * SelectedTree.TreesPerAcre;
                    SelectedTree.TotalCcf = (SelectedTree.TotalNetCubicPerAcre * st.NetGeographicAcres) / 100;
                    SelectedTree.TotalMbf = (SelectedTree.TotalNetScribnerPerAcre * st.NetGeographicAcres) / 1000;
                    /////////
                }
                
                //SelectedTree.TreesPerAcre = SelectedTree.BasalArea / (0.005454154 * (SelectedTree.Dbh * SelectedTree.Dbh));
                //SelectedTree.TotalHeight = 0; // was (short) cwd.CalcTotalHt;
                //SelectedTree.ReportedFormFactor = 0; // was (short) cwd.CalcFf;
                //SelectedTree.TotalCuFtGrossVolume = Math.Round(cwd.GrossCalcCfTree, 0);
                //SelectedTree.TotalCuFtNetVolume = Math.Round(cwd.NetCalcCfTree, 0);
                //SelectedTree.TotalBdFtGrossVolume = Math.Round((double) cwd.GrossCalcBfTree, 0);
                //SelectedTree.TotalBdFtNetVolume = Math.Round((double) cwd.NetCalcBfTree, 0);
                //SelectedTree.TotalGrossCubicPerAcre = SelectedTree.TotalCuFtGrossVolume * SelectedTree.TreesPerAcre;
                //SelectedTree.TotalNetCubicPerAcre = SelectedTree.TotalCuFtNetVolume * SelectedTree.TreesPerAcre;
                //SelectedTree.TotalGrossScribnerPerAcre = SelectedTree.TotalBdFtGrossVolume * SelectedTree.TreesPerAcre;
                //SelectedTree.TotalNetScribnerPerAcre = SelectedTree.TotalBdFtNetVolume * SelectedTree.TreesPerAcre;
                //SelectedTree.TotalCcf = (SelectedTree.TotalNetScribnerPerAcre * st.NetGeographicAcres) / 100;
                //SelectedTree.TotalMbf = (SelectedTree.TotalNetCubicPerAcre * st.NetGeographicAcres) / 1000;

                if (SelectedTree.PlotFactorInput != seg.PlotFactorInput)
                    seg.PlotFactorInput = SelectedTree.PlotFactorInput;
                if (SelectedTree.SpeciesAbbreviation != seg.Species)
                    seg.Species = SelectedTree.SpeciesAbbreviation;
                if (SelectedTree.TreeStatusDisplayCode != seg.TreeStatusDisplayCode)
                    seg.TreeStatusDisplayCode = SelectedTree.TreeStatusDisplayCode;
                if (SelectedTree.TreeCount != seg.TreeCount)
                    seg.TreeCount = SelectedTree.TreeCount;
                if (SelectedTree.Dbh != seg.Dbh)
                    seg.Dbh = SelectedTree.Dbh;
                if (SelectedTree.FormPoint != seg.FormPoint)
                    seg.FormPoint = SelectedTree.FormPoint;
                seg.TreeBasalArea = SelectedTree.BasalArea;
                seg.TreeTreesPerAcre = SelectedTree.TreesPerAcre;
                seg.TotalHeight = SelectedTree.TotalHeight;
                seg.FormFactor = SelectedTree.ReportedFormFactor;
                seg.CubicGrossVolume = SelectedTree.TotalCuFtGrossVolume;
                seg.CubicNetVolume = SelectedTree.TotalCuFtNetVolume;
                seg.ScribnerGrossVolume = SelectedTree.TotalBdFtGrossVolume;
                seg.ScribnerNetVolume = SelectedTree.TotalBdFtNetVolume;
                seg.CubicGrossVolumePerAce = SelectedTree.TotalGrossCubicPerAcre;
                seg.CubicNetVolumePerAce = SelectedTree.TotalNetCubicPerAcre;
                seg.ScribnerGrossVolumePerAce = SelectedTree.TotalGrossScribnerPerAcre;
                seg.ScribnerNetVolumePerAce = SelectedTree.TotalNetScribnerPerAcre;
                seg.Ccf = SelectedTree.TotalCcf;
                seg.Mbf = SelectedTree.TotalMbf;
                seg.SegmentNumber = 0;
                seg.TreeType = SelectedTree.TreeType;

                if (UpdateSegmentGrid)
                {
                    gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", seg.TreeBasalArea);
                    gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", seg.TreeTreesPerAcre);
                    gridViewSegments.SetFocusedRowCellValue("TotalHeight", seg.TotalHeight);
                    gridViewSegments.SetFocusedRowCellValue("FormPoint", seg.FormPoint);
                    gridViewSegments.SetFocusedRowCellValue("FormFactor", seg.FormFactor);
                    gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", seg.CubicNetVolume);
                    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", seg.ScribnerNetVolume);
                    gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", seg.CubicNetVolumePerAce);
                    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", seg.ScribnerNetVolumePerAce);
                    gridViewSegments.SetFocusedRowCellValue("Ccf", seg.Ccf);
                    gridViewSegments.SetFocusedRowCellValue("Mbf", seg.Mbf);
                    gridViewSegments.SetFocusedRowCellValue("Comments", seg.Comments);
                }
            }
            else
            {
                Calc.CalcVolume(pHandleErrors);
                SelectedTree.Age = Utils.GetAge(ref projectContext, st, SelectedTree.AgeCode.ToString());
                SelectedTree.CalcTotalHeight = (short) Calc.total_tree_height;
                SelectedTree.BasalArea = Calc.basal_area;
                SelectedTree.TreesPerAcre = Calc.trees_per_acre;
                SelectedTree.LogsPerAcre = Calc.logs_pa;
                SelectedTree.Ao = Calc.ao;
                SelectedTree.Bark = Calc.bark;

                SelectedTree.TotalBdFtGrossVolume = Calc.tree_scribner_gross;
                SelectedTree.TotalBdFtNetVolume = Calc.tree_scribner_net;
                SelectedTree.TotalMbf = Calc.tree_mbf;

                SelectedTree.TotalCuFtGrossVolume = Calc.tree_cubic_gross;
                SelectedTree.TotalCuFtNetVolume = Calc.tree_cubic_net;
                SelectedTree.TotalCcf = Calc.tree_ccf;

                SelectedTree.TotalGrossCubicPerAcre = Calc.tree_cubic_gross_pa;
                SelectedTree.TotalNetCubicPerAcre = Calc.tree_cubic_net_pa;
                SelectedTree.TotalGrossScribnerPerAcre = Calc.tree_scribner_gross_pa;
                SelectedTree.TotalNetScribnerPerAcre = Calc.tree_scribner_net_pa;

                SelectedTree.CalcTotalHtUsedYn = "N";
                if (SelectedTree.TotalHeight == null || SelectedTree.TotalHeight == 0)
                {
                    SelectedTree.TotalHeight = SelectedTree.CalcTotalHeight;
                    SelectedTree.CalcTotalHtUsedYn = "Y";
                }
                ProjectBLL.SetTreeType(ref SelectedTree);
                float weight =
                    (float) ProjectBLL.GetSpecieByAbbrev(ProjectDataSource, MainForm.CurrentProject.SpeciesTableName,
                        SelectedTree.SpeciesAbbreviation).Weight;
                SelectedTree.TotalTonsPerAcre = ((SelectedTree.TotalCcf * weight) / 2000) /
                                                MainForm.CurrentStand.NetGeographicAcres;
                //if (UpdateSegmentGrid)
                //{
                //    if (origNumberOfSegments == Calc.number_segments)
                //        DisplaySegmentsInTreeEdit(SelectedTree);
                //}
            }
            if (SelectedTree.PlotId == null)
            {
                Plot xPlot = ProjectBLL.GetPlotByStandIdPlot(ref projectContext, (int) SelectedTree.StandsId,
                    SelectedTree.PlotNumber);
                SelectedTree.PlotId = xPlot.PlotsId;
            }
            needToUpdatePlotsStands = true;

            try
            {
                projectContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null)
                    Debug.WriteLine(ex.InnerException.InnerException.Message);
                else
                    Debug.WriteLine(ex.Message);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                Debug.WriteLine(exceptionMessage);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Debug.WriteLine(ex.InnerException.Message);
                else
                    Debug.WriteLine(ex.Message);
            }

            if (SelectedTree.TreeType != "C")
            {
                Utils.DeleteSegmentsForTree(ref projectContext, SelectedTree);
                Utils.tPrices = ProjectBLL.GetPrices(ref projectContext, CurrentProject.PriceTableName);
                Utils.CreateSegmentsFromTree(ref projectContext, SelectedTree);
                //ProjectBLL.DeleteCountWithDiaTable(ref projectContext, st);
                //ProjectBLL.BuildCountWithDiaTable(ref projectContext, st);
            }
        }

        private void DisplaySegmentsInTreeEdit(Tree pTree)
        {
            // Find First tree in segment
            int segment = Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber"));
            int row = gridViewSegments.FocusedRowHandle;

            for (int s = segment; s <= Calc.number_segments; s--)
            {
                if (segment == 1)
                    break;
                gridViewSegments.FocusedRowHandle--;
                segment = Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber"));
            }
            // Loop through segments
            for (int i = 0; i < Calc.number_segments; i++)
            {
                switch (i)
                {
                    case 0:
                        //gridViewSegments.SetFocusedRowCellValue("PlotNumber", Calc.tree.PlotNumber);
                        //gridViewSegments.SetFocusedRowCellValue("TreeNumber", Calc.tree.TreeNumber);
                        //gridViewSegments.SetFocusedRowCellValue("PlotFactorInput", Calc.tree.PlotFactorInput);
                        //gridViewSegments.SetFocusedRowCellValue("Age", Calc.tree.Age);
                        //gridViewSegments.SetFocusedRowCellValue("Species", Calc.tree.SpeciesAbbreviation);
                        //gridViewSegments.SetFocusedRowCellValue("TreeStatusDisplayCode", Calc.tree.TreeStatusDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("TreeCount", Calc.tree.TreeCount);
                        //gridViewSegments.SetFocusedRowCellValue("Dbh", Calc.tree.Dbh);
                        //gridViewSegments.SetFocusedRowCellValue("FormPoint", Calc.tree.FormPoint);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", Calc.tree.ReportedFormFactor);
                        //gridViewSegments.SetFocusedRowCellValue("TopDiameterFractionCode", Calc.tree.TopDiameterFractionCode);
                        //gridViewSegments.SetFocusedRowCellValue("BoleHeight", Calc.tree.BoleHeight);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("CrownPosition", Calc.tree.CrownPositionDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("CrownRatio", Calc.tree.CrownRatioDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("Vigor", Calc.tree.VigorDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("Damage", Calc.tree.DamageDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("UserDefined", Calc.tree.UserDefinedDisplayCode);
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i+1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort1);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade1);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length1);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd1);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd1);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd1);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd1);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd1);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == 1)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 1:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i+1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort2);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade2);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length2);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd2);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd2);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd2);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd2);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd2);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 2:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort3);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade3);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length3);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd3);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd3);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd3);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd3);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd3);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 3:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort4);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade4);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length4);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd4);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd4);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd4);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd4);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd4);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 4:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort5);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade5);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length5);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd5);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd5);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd5);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd5);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd5);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 5:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort6);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade6);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length6);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd6);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd6);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd6);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd6);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd6);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 6:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort7);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade7);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length7);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd7);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd7);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd7);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd7);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd7);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 7:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort8);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade8);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length8);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd8);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd8);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd8);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd8);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd8);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 8:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort9);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade9);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length9);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd9);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd9);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd9);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd9);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd9);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 9:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort10);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade10);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length10);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd10);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd10);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd10);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd10);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd10);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 10:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort11);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade11);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length11);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd11);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd11);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd11);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd11);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd11);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                    case 11:
                        //gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i + 1);
                        gridViewSegments.SetFocusedRowCellValue("SortCode", pTree.Sort12);
                        gridViewSegments.SetFocusedRowCellValue("GradeCode", pTree.Grade12);
                        gridViewSegments.SetFocusedRowCellValue("Length", pTree.Length12);
                        gridViewSegments.SetFocusedRowCellValue("BdFtLD", pTree.BdFtLd12);
                        gridViewSegments.SetFocusedRowCellValue("BdFtDD", pTree.BdFtDd12);
                        gridViewSegments.SetFocusedRowCellValue("CuFtLD", pTree.CuFtLd12);
                        gridViewSegments.SetFocusedRowCellValue("CuFtDD", pTree.CuFtDd12);
                        gridViewSegments.SetFocusedRowCellValue("BdFtPD", pTree.BdFtPd12);
                        gridViewSegments.SetFocusedRowCellValue("Bark", pTree.Bark);
                        gridViewSegments.SetFocusedRowCellValue("Ao", pTree.Ao);

                        gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                        gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                        //gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                        //gridViewSegments.SetFocusedRowCellValue("FormFactor", pTree.ReportedFormFactor);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                        gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                        gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                        gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                        if (Calc.teList.Count > 0)
                        {
                            foreach (var teItem in Calc.teList)
                            {
                                if (teItem.segment == i)
                                    gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                            }
                        }
                        else
                            gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                        break;
                }
                //if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")) == 1)
                //{
                //    gridViewSegments.SetFocusedRowCellValue("PlotNumber", Calc.tree.PlotNumber);
                //    gridViewSegments.SetFocusedRowCellValue("TreeNumber", Calc.tree.TreeNumber);
                //    gridViewSegments.SetFocusedRowCellValue("PlotFactorInput", Calc.tree.PlotFactorInput);
                //    gridViewSegments.SetFocusedRowCellValue("Age", Calc.tree.Age);
                //    gridViewSegments.SetFocusedRowCellValue("Species", Calc.tree.SpeciesAbbreviation);
                //    gridViewSegments.SetFocusedRowCellValue("TreeStatusDisplayCode", Calc.tree.TreeStatusDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("TreeCount", Calc.tree.TreeCount);
                //    gridViewSegments.SetFocusedRowCellValue("Dbh", Calc.tree.Dbh);
                //    gridViewSegments.SetFocusedRowCellValue("FormPoint", Calc.tree.FormPoint);
                //    gridViewSegments.SetFocusedRowCellValue("FormFactor", Calc.tree.ReportedFormFactor);
                //    gridViewSegments.SetFocusedRowCellValue("TopDiameterFractionCode", Calc.tree.TopDiameterFractionCode);
                //    gridViewSegments.SetFocusedRowCellValue("BoleHeight", Calc.tree.BoleHeight);
                //    gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                //    gridViewSegments.SetFocusedRowCellValue("CrownPosition", Calc.tree.CrownPositionDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("CrownRatio", Calc.tree.CrownRatioDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("Vigor", Calc.tree.VigorDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("Damage", Calc.tree.DamageDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("UserDefined", Calc.tree.UserDefinedDisplayCode);
                //    gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i);
                //    gridViewSegments.SetFocusedRowCellValue("SortCode", Calc.seg[i].sort);
                //    gridViewSegments.SetFocusedRowCellValue("GradeCode", Calc.seg[i].grade);
                //    gridViewSegments.SetFocusedRowCellValue("Length", Calc.seg[i].len);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtLD", Calc.seg[i].scribner_lended);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtDD", Calc.seg[i].scribner_diaded);
                //    gridViewSegments.SetFocusedRowCellValue("CuFtLD", Calc.seg[i].cubic_lended);
                //    gridViewSegments.SetFocusedRowCellValue("CuFtDD", Calc.seg[i].cubic_diaded);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtPD", Calc.seg[i].percent);
                //    gridViewSegments.SetFocusedRowCellValue("Bark", Calc.tree.Bark);
                //    gridViewSegments.SetFocusedRowCellValue("Ao", Calc.tree.Ao);

                //    gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                //    gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                //    gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                //    gridViewSegments.SetFocusedRowCellValue("FormFactor", Calc.tree.ReportedFormFactor);
                //    gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                //    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                //    gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                //    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                //    gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                //    gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                //    gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                //    foreach (var teItem in Calc.teList)
                //    {
                //        if (teItem.segment == 1)
                //            gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                //    }
                //}
                //else
                //{
                //    gridViewSegments.SetFocusedRowCellValue("SegmentNumber", i);
                //    gridViewSegments.SetFocusedRowCellValue("SortCode", Calc.seg[i].sort);
                //    gridViewSegments.SetFocusedRowCellValue("GradeCode", Calc.seg[i].grade);
                //    gridViewSegments.SetFocusedRowCellValue("Length", Calc.seg[i].len);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtLD", Calc.seg[i].scribner_lended);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtDD", Calc.seg[i].scribner_diaded);
                //    gridViewSegments.SetFocusedRowCellValue("CuFtLD", Calc.seg[i].cubic_lended);
                //    gridViewSegments.SetFocusedRowCellValue("CuFtDD", Calc.seg[i].cubic_diaded);
                //    gridViewSegments.SetFocusedRowCellValue("BdFtPD", Calc.seg[i].percent);
                //    gridViewSegments.SetFocusedRowCellValue("Bark", Calc.tree.Bark);
                //    gridViewSegments.SetFocusedRowCellValue("Ao", Calc.tree.Ao);

                //    gridViewSegments.SetFocusedRowCellValue("TreeBasalArea", Calc.basal_area);
                //    gridViewSegments.SetFocusedRowCellValue("TreeTreesPerAcre", Calc.trees_pa);
                //    gridViewSegments.SetFocusedRowCellValue("TotalHeight", Calc.total_tree_height);
                //    gridViewSegments.SetFocusedRowCellValue("FormFactor", Calc.tree.ReportedFormFactor);
                //    gridViewSegments.SetFocusedRowCellValue("CubicNetVolume", Calc.seg[i].cubic_net);
                //    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolume", Calc.seg[i].scribner_net);
                //    gridViewSegments.SetFocusedRowCellValue("CubicNetVolumePerAce", Calc.seg[i].cubic_net_pa);
                //    gridViewSegments.SetFocusedRowCellValue("ScribnerNetVolumePerAce", Calc.seg[i].scribner_net_pa);
                //    gridViewSegments.SetFocusedRowCellValue("Ccf", Calc.seg[i].ccf);
                //    gridViewSegments.SetFocusedRowCellValue("Mbf", Calc.seg[i].mbf);
                //    gridViewSegments.SetFocusedRowCellValue("Comments", string.Empty);
                //    foreach (var teItem in Calc.teList)
                //    {
                //        if (teItem.segment == i)
                //            gridViewSegments.SetFocusedRowCellValue("Coments", teItem);
                //    }
                //}
            }
        }

        private void barButtonItem34_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Crown Position
            CrownPositionForm frm = new CrownPositionForm(ref projectContext, CurrentProject.CrownPositionTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem35_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Crown Ratio
            CrownPositionForm frm = new CrownPositionForm(ref projectContext, CurrentProject.CrownPositionTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem36_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Vigor
            VigorForm frm = new VigorForm(ref projectContext, CurrentProject.VigorTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem37_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Damage
            DamageForm frm = new DamageForm(ref projectContext, CurrentProject.DamageTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem38_ItemClick(object sender, ItemClickEventArgs e)
        {
            // User Defined
            UserDefinedForm frm = new UserDefinedForm(ref projectContext, CurrentProject.UserDefinedTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem39_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Aspect
            AspectForm frm = new AspectForm(ref projectContext, CurrentProject.AspectsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem40_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Component
            ComponentForm frm = new ComponentForm(ref projectContext, CurrentProject.ComponentsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem41_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Environment
            EnvironmentForm frm = new EnvironmentForm(ref projectContext, CurrentProject.EnvironmentsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem42_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Form
            FormForm frm = new FormForm(ref projectContext, CurrentProject.FormTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem43_ItemClick(object sender, ItemClickEventArgs e)
        {
            // PocketEASY from PC
        }

        private void barButtonItem44_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Harvest
            HarvestForm frm = new HarvestForm(ref projectContext, CurrentProject.HarvestSystemsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem45_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Roads
            RoadsForm frm = new RoadsForm(ref projectContext, CurrentProject.RoadsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem46_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Seed Zone
            SeedZoneForm frm = new SeedZoneForm(ref projectContext, CurrentProject.SeedZonesTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem47_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Slash
            SlashForm frm = new SlashForm(ref projectContext, CurrentProject.SlashDistributionsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem48_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Soils
            SoilsForm frm = new SoilsForm(ref projectContext, CurrentProject.SoilsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem49_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Tree Status
            TreeStatusForm frm = new TreeStatusForm(ref projectContext, CurrentProject.TreeStatusTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem50_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Streams
            StreamsForm frm = new StreamsForm(ref projectContext, CurrentProject.StreamsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem51_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Treatment
            TreatmentForm frm = new TreatmentForm(ref projectContext, CurrentProject.TreatmentsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem52_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Tree Source
            TreeSourceForm frm = new TreeSourceForm(ref projectContext, CurrentProject.TreeSourcesTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem54_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Wood Type
            WoodTypeForm frm = new WoodTypeForm(ref projectContext, CurrentProject.WoodTypeTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem53_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Vegetation
            VegetationForm frm = new VegetationForm(ref projectContext, CurrentProject.VegetationsTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem55_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Non Stocked
            NonStockedForm frm = new NonStockedForm(ref projectContext, CurrentProject.NonStockedTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem56_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Non Timbere
            NonTimberedForm frm = new NonTimberedForm(ref projectContext, CurrentProject.NonTimberedTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem57_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select All
            if (barButtonItem57.Caption == "Select All")
            {
                gridViewStands.SelectAll();
                barButtonItem57.Caption = "Clear All";
            }
            else
            {
                gridViewStands.ClearSelection();
                barButtonItem57.Caption = "Select All";
            }
        }

        private void gridViewStandInputOrig_CustomSummaryCalculate(object sender,
            DevExpress.Data.CustomSummaryEventArgs e)
        {
            GridView view = sender as GridView;

            int SummaryID = Convert.ToInt32((e.Item as GridSummaryItem).Tag);

            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                standCalcAge = 0;
                standCalcSite = 0;
                standCalcTPA = 0;
                standCalcBA = 0;
                standCalcTons = 0;
                standCalcCcf = 0;
                standCalcMbf = 0;
                standCalcTotalCcf = 0;
                standCalcTotalMbf = 0;
                age = 0;
                acres = 0;
                site = 0;
                tpa = 0;
                ba = 0;
                tons = 0;
                ccf = 0;
                mbf = 0;
                totalCcf = 0;
                totalMbf = 0;
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if (SummaryID == 1)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    age += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "MajorAge")) *
                            Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 2)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    site += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "SiteIndex")) *
                             Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 3)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    tpa += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TreesPerAcre")) *
                            Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 4)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    ba += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "BasalAreaPerAcre")) *
                           Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 5)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    tons += (Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TonsPerAcre")) *
                             Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres")));
                }
                if (SummaryID == 6)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    ccf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "CcfPerAcre"));
                }
                if (SummaryID == 7)
                {
                    acres += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "NetGeographicAcres"));
                    mbf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "MbfPerAcre"));
                }
                if (SummaryID == 8)
                {
                    totalCcf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TotalCcf"));
                }
                if (SummaryID == 9)
                {
                    totalMbf += Convert.ToSingle(view.GetRowCellValue(e.RowHandle, "TotalMbf"));
                }
            }
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                if (SummaryID == 1)
                {
                    standCalcAge = age / acres;
                    e.TotalValue = standCalcAge;
                }
                if (SummaryID == 2)
                {
                    standCalcSite = site / acres;
                    e.TotalValue = standCalcSite;
                }
                if (SummaryID == 3)
                {
                    standCalcTPA = tpa / acres;
                    e.TotalValue = standCalcTPA;
                }
                if (SummaryID == 4)
                {
                    standCalcBA = ba / acres;
                    e.TotalValue = standCalcBA;
                }
                if (SummaryID == 5)
                {
                    standCalcTons = tons / acres;
                    e.TotalValue = standCalcTons;
                }
                if (SummaryID == 6)
                {
                    standCalcCcf = ccf / acres;
                    e.TotalValue = standCalcCcf;
                }
                if (SummaryID == 7)
                {
                    standCalcMbf = mbf / acres;
                    e.TotalValue = standCalcMbf;
                }
                if (SummaryID == 8)
                {
                    e.TotalValue = totalCcf;
                }
                if (SummaryID == 9)
                {
                    e.TotalValue = totalMbf;
                }
            }
        }

        private void barButtonItem58_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Project
            ProjectSelectForm frm = new ProjectSelectForm(ref locContext, false);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            bool bSelected = frm.bSelected;
            frm.Dispose();
            //if (bSelected)
            //{
            //    ///////
            //    projectContext.Dispose();

            //    //CurrentProjectLocation = ProjectsBLL.GetLastProject(ref locContext);
            //    ProjectDataSource = Properties.Settings.Default["CurrentDb"].ToString(); // CurrentProjectLocation.ProjectLocation.Trim() + CurrentProjectLocation.ProjectName + ".sdf";
            //    projectContext = new ProjectDbContext(ProjectBLL.GetConnectionString(ProjectDataSource));
            //    CurrentProject = ProjectBLL.GetProject(ref projectContext, ProjectDataSource);
            //    barStaticItem2.Caption = string.Format("[Project: {0}    Location: {1}]", Properties.Settings.Default["CurrentProject"].ToString(), Properties.Settings.Default["CurrentDataDir"].ToString());
            //    //////
            //    LoadStands();
            //}
        }

        private void barButtonItem59_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Copy Project
            ProjectCopyForm frm = new ProjectCopyForm(ref locContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem60_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Copy Stand
            sColl.Clear();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            if (sColl.Count == 0)
            {
                XtraMessageBox.Show("No Stand(s) were selected to copy.", "Error", MessageBoxButtons.OK);
                return;
            }
            using (TempDbContext tempDb = new TempDbContext("Data Source = " + TempDataSource))
            {
                tempDb.TempCopyStands.RemoveRange(tempDb.TempCopyStands);
                tempDb.SaveChanges();

                foreach (var item in sColl)
                {
                    TempCopyStand rec = new TempCopyStand();
                    rec.OrigTractName = item.TractName;
                    rec.NewTractName = item.TractName;
                    rec.OrigStandName = item.StandName;
                    rec.NewStandName = item.StandName;
                    rec.OrigTownship = item.Township;
                    rec.NewTownship = item.Township;
                    rec.OrigRange = item.Range;
                    rec.NewRange = item.Range;
                    rec.OrigSection = item.Section;
                    rec.NewSection = item.Section;
                    tempDb.TempCopyStands.Add(rec);
                }
                tempDb.SaveChanges();
            }

            StandCopyForm frm = new StandCopyForm(ref locContext, ref projectContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();

            LoadStands();
        }

        private void barButtonItem61_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Copy Plot
            sColl.Clear();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            TempDbContext tempDb = new TempDbContext();
            tempDb.TempCopyPlots.RemoveRange(tempDb.TempCopyPlots);
            tempDb.SaveChanges();

            if (sColl.Count > 0)
            {
                List<Plot> plots = ProjectBLL.GetPlotsByStandId(ref projectContext, sColl[0].StandsId);
                int plotColCount = 0;
                int plotCount = 0;
                int totalPlots = plots.Count;
                TempCopyPlot plotRec = new TempCopyPlot();
                TempBLL.InitTempCopyPlot(ref tempDb, ref plotRec);
                foreach (var plotItem in plots)
                {
                    plotColCount++;
                    plotCount++;
                    switch (plotColCount)
                    {
                        case 1:
                            plotRec.Plot1 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot1 = plotItem.UserPlotNumber;
                            break;
                        case 2:
                            plotRec.Plot2 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot2 = plotItem.UserPlotNumber;
                            break;
                        case 3:
                            plotRec.Plot3 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot3 = plotItem.UserPlotNumber;
                            break;
                        case 4:
                            plotRec.Plot4 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot4 = plotItem.UserPlotNumber;
                            break;
                        case 5:
                            plotRec.Plot5 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot5 = plotItem.UserPlotNumber;
                            break;
                        case 6:
                            plotRec.Plot6 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot6 = plotItem.UserPlotNumber;
                            break;
                        case 7:
                            plotRec.Plot7 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot7 = plotItem.UserPlotNumber;
                            break;
                        case 8:
                            plotRec.Plot8 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot8 = plotItem.UserPlotNumber;
                            break;
                        case 9:
                            plotRec.Plot9 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot9 = plotItem.UserPlotNumber;
                            break;
                        case 10:
                            plotRec.Plot10 = plotItem.UserPlotNumber;
                            plotRec.OrigPlot10 = plotItem.UserPlotNumber;
                            TempBLL.WriteTempCopyPlot(ref tempDb, ref plotRec);
                            TempBLL.InitTempCopyPlot(ref tempDb, ref plotRec);
                            plotColCount = 0;
                            break;
                    }
                    if (plotCount == totalPlots)
                    {
                        if (plotColCount > 0)
                            TempBLL.WriteTempCopyPlot(ref tempDb, ref plotRec);
                    }
                }
            }
            tempDb.SaveChanges();
            tempDb.Dispose();

            PlotCopyForm frm = new PlotCopyForm(ref locContext, ref projectContext, sColl[0]);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();

            LoadStands();
        }

        private void barButtonItem62_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Plots
            XtraMessageBox.Show("Under Development", "Status");
        }

        private void barButtonItem63_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Existing Project.DAL.Project Tables
            TempDbContext tempDb = new TempDbContext("Data Source = " + TempDataSource);
            ExistingProjectTablesForm frm = new ExistingProjectTablesForm(ref tempDb);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();

            tempDb.Dispose();
        }

        private void tbAreaCode1_SelectedValueChanged(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode1, ref tbAreaPlotAcres1, ref tbAreaPlotRadius1, ref tbAreaSide11,
                ref tbAreaSide12, ref tbAreaBlowup1, 1);
            if (string.IsNullOrEmpty(tbAreaCode1.Text))
            {
                tbAreaPlotAcres1.Text = string.Empty;
                tbAreaPlotRadius1.Text = string.Empty;
                tbAreaSide11.Text = string.Empty;
                tbAreaSide12.Text = string.Empty;
                tbAreaBlowup1.Text = string.Empty;
            }
        }

        private void tbAreaCode2_SelectedValueChanged(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode2, ref tbAreaPlotAcres2, ref tbAreaPlotRadius2, ref tbAreaSide21,
                ref tbAreaSide22, ref tbAreaBlowup2, 2);
            if (string.IsNullOrEmpty(tbAreaCode2.Text))
            {
                tbAreaPlotAcres2.Text = string.Empty;
                tbAreaPlotRadius2.Text = string.Empty;
                tbAreaSide21.Text = string.Empty;
                tbAreaSide22.Text = string.Empty;
                tbAreaBlowup2.Text = string.Empty;
            }
        }

        private void tbAreaCode3_SelectedValueChanged(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode3, ref tbAreaPlotAcres3, ref tbAreaPlotRadius3, ref tbAreaSide31,
                ref tbAreaSide32, ref tbAreaBlowup3, 3);
            if (string.IsNullOrEmpty(tbAreaCode3.Text))
            {
                tbAreaPlotAcres3.Text = string.Empty;
                tbAreaPlotRadius3.Text = string.Empty;
                tbAreaSide31.Text = string.Empty;
                tbAreaSide32.Text = string.Empty;
                tbAreaBlowup3.Text = string.Empty;
            }
        }

        private void tbAreaCode4_SelectedValueChanged(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode4, ref tbAreaPlotAcres4, ref tbAreaPlotRadius4, ref tbAreaSide41,
                ref tbAreaSide42, ref tbAreaBlowup4, 4);
            if (string.IsNullOrEmpty(tbAreaCode4.Text))
            {
                tbAreaPlotAcres4.Text = string.Empty;
                tbAreaPlotRadius4.Text = string.Empty;
                tbAreaSide41.Text = string.Empty;
                tbAreaSide42.Text = string.Empty;
                tbAreaBlowup4.Text = string.Empty;
            }
        }

        private void tbAreaCode5_SelectedValueChanged(object sender, EventArgs e)
        {
            SetReadOnly(ref tbAreaCode5, ref tbAreaPlotAcres5, ref tbAreaPlotRadius5, ref tbAreaSide51,
                ref tbAreaSide52, ref tbAreaBlowup5, 5);
            if (string.IsNullOrEmpty(tbAreaCode5.Text))
            {
                tbAreaPlotAcres5.Text = string.Empty;
                tbAreaPlotRadius5.Text = string.Empty;
                tbAreaSide51.Text = string.Empty;
                tbAreaSide52.Text = string.Empty;
                tbAreaBlowup5.Text = string.Empty;
            }
        }

        private void barButtonItem65_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Poles
            PoleForm frm = new PoleForm(ref projectContext, CurrentProject.PolesTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem66_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Piling
            PilingForm frm = new PilingForm(ref projectContext, CurrentProject.PilingTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void advBandedGridViewPlots_Click(object sender, EventArgs e)
        {
            CurrentPlot = plotsBindingSource.Current as Plot;
        }

        private void advBandedGridViewTrees_Click(object sender, EventArgs e)
        {
            CurrentTree = treesBindingSource.Current as Tree;
            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, CurrentTree.SpeciesAbbreviation);
        }

        private void gridViewStands_Click(object sender, EventArgs e)
        {
            CurrentStand = standsViewBindingSource.Current as Stand;
        }

        private void gridViewSegments_Click(object sender, EventArgs e)
        {
            //CurrentSegment = segmentsBindingSource.Current as Treesegment;
        }

        private void tbStand_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit) sender).Text.Length == 4)
                tbTownship.Focus();
        }

        private void tbTract_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit) sender).Text.Length == 12)
                tbStand.Focus();
        }

        private void tbTownship_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit) sender).Text.Length == 3)
                tbRange.Focus();
        }

        private void tbRange_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit) sender).Text.Length == 3)
                tbSection.Focus();
        }

        private void tbSection_TextChanged(object sender, EventArgs e)
        {
            if (((TextEdit) sender).Text.Length == 2)
                tbNetAcres.Focus();
        }

        private void barButtonItem67_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Update Calcs For Trees
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Update Calcs For Trees");

            //sColl.Clear();
            //gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            Treesegment segRec = null;

            foreach (var item in sColl)
            {
                Stand standItem = item;
                Project.DAL.Project projectItem = CurrentProject;

                //Calc calc = new Calc(ref projectContext, MainForm.CurrentProject, ref standItem);
                Calc.SetCalcContextProjectStand(ref projectContext, MainForm.CurrentProject, ref standItem);
                Calc.projectDataSource = ProjectDataSource;

                List<Tree> tColl =
                    projectContext.Trees.OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .Where(t => t.StandsId == standItem.StandsId && t.TreeType == "D")
                        .ToList();
                foreach (var treeItem in tColl)
                {
                    //DoCalcForTree(false, treeItem, false);
                    Calc.tree = treeItem;
                    Calc.CalcVolume(false);

                    if (Calc.teList.Count > 0)
                    {
                        foreach (var teItem in Calc.teList)
                        {
                            segRec = projectContext.Treesegments.FirstOrDefault(m => m.TreesId == treeItem.TreesId && m.SegmentNumber == teItem.segment);
                            if (segRec != null)
                            {
                                segRec.Comments = string.Empty;
                                if (!string.IsNullOrEmpty(teItem.ErrorMsg))
                                {
                                    if (!string.IsNullOrEmpty(segRec.Comments))
                                        segRec.Comments += string.Format("; {0}", teItem.ErrorMsg);
                                    else
                                        segRec.Comments = string.Format("{0}", teItem.ErrorMsg);
                                }
                            }
                            else
                                Debug.WriteLine(string.Format("{0}-{1}", treeItem.PlotNumber, treeItem.TreeNumber));
                        }
                    }
                }
            }
            projectContext.SaveChanges();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem64_ItemClick(object sender, ItemClickEventArgs e)
        {
            // SuperACE 08 Db Location
            FolderBrowserDialog frm = new FolderBrowserDialog();
            frm.SelectedPath = Properties.Settings.Default["SA08FlipsLocation"].ToString();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(frm.SelectedPath))
                {
                    if (File.Exists(string.Format("{0}\\Flips.mdb", frm.SelectedPath)))
                        Properties.Settings.Default["SA08FlipsLocation"] = frm.SelectedPath + "\\";
                    else
                    {
                        XtraMessageBox.Show("SuperACE 08 Files do not exist in selected folder.", "Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            frm.Dispose();
        }

        private void barButtonItem70_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Plot(s)
            DeletePlots();
        }

        private void barButtonItem71_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Tree(s)
            DeleteTrees();
        }

        private void barButtonItem72_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Delete Segment(s)
        }

        private void gridViewSegments_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            if (string.IsNullOrEmpty(e.ErrorText))
                e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void gridViewSegments_InvalidValueException(object sender, InvalidValueExceptionEventArgs e)
        {
            XtraMessageBox.Show(this, e.ErrorText, "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            IsError = true;
        }

        private void gridViewSegments_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    break;
            }
        }

        private void gridViewSegments_RowUpdated(object sender, RowObjectEventArgs e)
        {

        }

        private void gridViewSegments_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            // Update Tree 
            Tree selectedTree = ProjectBLL.GetTree(ref projectContext, CurrentStand, (int)CurrentSegment().TreesId);
            if (selectedTree.TreeType != "C")
            {
                //Utils.DeleteSegmentsForTree(ref projectContext, selectedTree);
                //Utils.tPrices = ProjectBLL.GetPrices(ref projectContext, CurrentProject.PriceTableName);
                //Utils.CreateSegmentsFromTree(ref projectContext, selectedTree);

                ProjectBLL.DeleteCountWithDiaTable(ref projectContext, (int)selectedTree.StandsId);
                ProjectBLL.BuildCountWithDiaTable(ref projectContext, (int)selectedTree.StandsId);
            }
            //LoadTreeFromTreeEdit();
        }

        private void LoadTreeFromTreeEdit()
        {
            Tree rec = ProjectBLL.GetTree(ref projectContext, CurrentStand, (int)CurrentSegment().TreesId);
            switch (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")))
            {
                case 1:
                    rec.PlotFactorInput = gridViewSegments.GetFocusedRowCellDisplayText("PlotFactorInput");
                    rec.Age = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("Age"));
                    rec.AgeCode = ProjectBLL.GetAgeCode(ref projectContext, CurrentStand, (short)rec.Age);
                    rec.SpeciesAbbreviation = gridViewSegments.GetFocusedRowCellDisplayText("Species");
                    rec.TreeStatusDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("TreeStatusDisplayCode");
                    rec.TreeCount = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("TreeCount"));
                    rec.Dbh = Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("Dbh"));
                    rec.FormPoint = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("FormPoint"));
                    rec.ReportedFormFactor = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("FormFactor"));
                    rec.TopDiameterFractionCode = gridViewSegments.GetFocusedRowCellDisplayText("Tdf");
                    rec.BoleHeight = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight"));
                    rec.TotalHeight = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight"));
                    rec.CrownPositionDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("CrownPosition");
                    rec.CrownRatioDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("CrownRatio");
                    rec.VigorDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("Vigor");
                    rec.DamageDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("Damage");
                    rec.UserDefinedDisplayCode = gridViewSegments.GetFocusedRowCellDisplayText("UserDefined");
                    rec.Sort1 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade1 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length1 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd1 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd1 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd1 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd1 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd1 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 2:
                    rec.Sort2 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade2 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length2 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd2 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd2 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd2 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd2 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd2 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 3:
                    rec.Sort3 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade3 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length3 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd3 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd3 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd3 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd3 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd3 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 4:
                    rec.Sort4 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade4 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length4 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd4 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd4 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd4 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd4 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd4 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 5:
                    rec.Sort5 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade5 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length5 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd5 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd5 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd5 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd5 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd5 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 6:
                    rec.Sort6 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade6 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length6 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd6 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd6 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd6 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd6 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd6 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 7:
                    rec.Sort7 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade7 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length7 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd7 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd7 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd7 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd7 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd7 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 8:
                    rec.Sort8 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade8 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length8 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd8 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd8 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd8 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd8 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd8 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 9:
                    rec.Sort9 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade9 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length9 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd9 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd9 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd9 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd9 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd9 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 10:
                    rec.Sort10 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade10 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length10 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd10 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLd"));
                    rec.BdFtDd10 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDd"));
                    rec.CuFtLd10 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLd"));
                    rec.CuFtDd10 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDd"));
                    rec.BdFtPd10 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 11:
                    rec.Sort11 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade11 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length11 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd11 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd11 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd11 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd11 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd11 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
                case 12:
                    rec.Sort12 = gridViewSegments.GetFocusedRowCellDisplayText("SortCode");
                    rec.Grade12 = gridViewSegments.GetFocusedRowCellDisplayText("GradeCode");
                    rec.Length12 = gridViewSegments.GetFocusedRowCellDisplayText("Length");
                    rec.BdFtLd12 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtLD"));
                    rec.BdFtDd12 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("BdFtDD"));
                    rec.CuFtLd12 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtLD"));
                    rec.CuFtDd12 = Utils.ConvertToTiny(gridViewSegments.GetFocusedRowCellDisplayText("CuFtDD"));
                    rec.BdFtPd12 = gridViewSegments.GetFocusedRowCellDisplayText("BdFtPD");
                    break;
            }
            //DoCalcForTree(true, rec);
            LoadSegments(-1, null);
        }

        private void gridViewSegments_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            Tree treeRec = ProjectBLL.GetTree(ref projectContext, CurrentStand, (int)CurrentSegment().TreesId);
            int focusedRow = gridViewSegments.FocusedRowHandle;
            string focusedCol = gridViewSegments.FocusedColumn.FieldName;

            if (Calc.tree == null)
                Calc.tree = treeRec;

            switch (gridViewSegments.FocusedColumn.FieldName)
            {
                case "PlotNumber":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0)
                        {
                            e.Value = e.Value.ToString().PadLeft(4, '0');
                        }
                        Plot plot = projectContext.Plots.FirstOrDefault(p => p.StandsId == CurrentStand.StandsId && p.UserPlotNumber == e.Value.ToString());
                        if (plot != null)
                        {
                            CurrentPlot = plot;
                            gridViewSegments.SetRowCellValue(gridViewSegments.FocusedRowHandle,
                                gridViewSegments.Columns["PlotID"], plot.PlotsId);
                        }
                        //else
                        //{
                        //    // Add plot
                        //    AddPlot(e.Value.ToString());

                        //    plot =
                        //        projectContext.Plots.FirstOrDefault(
                        //            p => p.StandsId == CurrentStand.StandsId && p.UserPlotNumber == e.Value.ToString());
                        //    if (plot != null)
                        //    {
                        //        CurrentPlot = plot;
                        //        advBandedGridViewTrees.SetRowCellValue(advBandedGridViewTrees.FocusedRowHandle,
                        //            advBandedGridViewTrees.Columns["PlotsId"], plot.PlotsId);
                        //    }
                        //}
                        if (defTreePlotNumber != e.Value.ToString().Trim())
                        {
                            gridViewSegments.SetRowCellValue(gridViewSegments.FocusedRowHandle,
                                gridViewSegments.Columns["TreeNumber"], 1);
                            defTreeTreeNumber = 1;
                        }
                        defTreePlotNumber = e.Value.ToString().Trim();
                        treeRec.PlotNumber = e.Value.ToString().Trim();
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Plot Number.";
                        return;
                    }
                    break;
                case "TreeNumber":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        defTreeTreeNumber = Utils.ConvertToInt(e.Value.ToString());
                    }
                    break;
                case "PlotFactorInput":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        switch (e.Value.ToString())
                        {
                            case "B1":
                                if (CurrentStand.Baf1 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B2":
                                if (CurrentStand.Baf2 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B3":
                                if (CurrentStand.Baf3 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Basal Factor in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B4":
                                if (CurrentStand.Baf4 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "B5":
                                if (CurrentStand.Baf5 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for BAF in the Stand Master.";
                                    return;
                                }
                                break;
                            case "A1":
                            case "A2":
                            case "A3":
                            case "A4":
                            case "A5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = true;
                                    e.ErrorText = "No value was entered for Area Code in the Stand Master.";
                                    return;
                                }
                                break;
                            case "R1":
                            case "R2":
                            case "R3":
                            case "R4":
                            case "R5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Plot Radius in the Stand Master.";
                                    return;
                                }
                                break;
                            case "F1":
                            case "F2":
                            case "F3":
                            case "F4":
                            case "F5":
                                if (Utils.ConvertToFloat(ProjectBLL.GetAreaValue(CurrentStand, e.Value.ToString())) == 0)
                                {
                                    e.Valid = true;
                                    e.ErrorText = "No value was entered for Fixed Area Plot in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S1":
                                if (CurrentStand.Strip1 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S2":
                                if (CurrentStand.Strip2 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S3":
                                if (CurrentStand.Strip3 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S4":
                                if (CurrentStand.Strip4 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            case "S5":
                                if (CurrentStand.Strip5 == 0)
                                {
                                    e.Valid = false;
                                    e.ErrorText = "No value was entered for Strip Cruise Blowup in the Stand Master.";
                                    return;
                                }
                                break;
                            default:
                                e.ErrorText = string.Empty;
                                if (Utils.ConvertToInt(e.Value.ToString()) < 5)
                                {
                                    if (e.Value.ToString() != "00")
                                    {
                                        e.Valid = false;
                                        e.ErrorText = "Invalid Plot Factor entered.";
                                        return;
                                    }
                                }
                                else
                                {
                                    e.Valid = true;
                                    e.ErrorText = string.Empty;
                                }
                                break;
                        }
                        if (e.Value.ToString().StartsWith("R") && e.Valid)
                            gridViewSegments.SetRowCellValue(gridViewSegments.FocusedRowHandle,
                                gridViewSegments.Columns["FormPoint"], 0);
                        defTreePF = e.Value.ToString().Trim();
                        treeRec.PlotFactorInput = e.Value.ToString().Trim();
                        projectContext.SaveChanges();
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Plot Factor.";
                        return;
                    }
                    break;
                case "Age": 
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (!projectContext.StandAges.Any(s => s.StandsId == CurrentStand.StandsId && s.Age == Utils.ConvertToTiny(e.Value.ToString())))
                        {
                            e.Valid = false;
                            e.ErrorText = "No value was entered for Age in the Stand Master.";
                            return;
                        }
                        //    switch (e.Value.ToString())
                        //{
                        //    case "1":
                        //        if (CurrentStand.Age1 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age1 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "2":
                        //        if (CurrentStand.Age2 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age2 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "3":
                        //        if (CurrentStand.Age3 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age3 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "4":
                        //        if (CurrentStand.Age4 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age4 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "5":
                        //        if (CurrentStand.Age5 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age5 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "6":
                        //        if (CurrentStand.Age6 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age6 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "7":
                        //        if (CurrentStand.Age7 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age7 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "8":
                        //        if (CurrentStand.Age8 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age8 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    case "9":
                        //        if (CurrentStand.Age9 == 0)
                        //        {
                        //            e.Valid = false;
                        //            e.ErrorText = "No value was entered for Age9 in the Stand Master.";
                        //            return;
                        //        }
                        //        break;
                        //    default:
                        //        e.Valid = false;
                        //        e.ErrorText = "Age Code is invalid.";
                        //        break;
                        //}
                        defTreeAge = Utils.ConvertToTiny(e.Value.ToString());
                        treeRec.AgeCode = Utils.ConvertToTiny(e.Value.ToString());
                        Calc.tree.AgeCode = treeRec.AgeCode;
                        DoCalcForTree(true, treeRec, true);
                        projectContext.SaveChanges();
                        if (treeRec.TreeType != "C")
                        {
                            LoadSegments(focusedRow, focusedCol);
                            gridViewSegments.RefreshData();
                        }
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "No value was entered for Age.";
                        return;
                    }
                    break;
                case "Species":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 2)
                        {
                            e.Valid = false;
                            e.ErrorText = "Value is too large.";
                            return;
                        }
                        // Code was entered
                        if (Utils.IsValidInt(e.Value.ToString()))
                        {
                            Species spcRec1 = ProjectBLL.GetSpecieByCode(ref projectContext,
                                CurrentProject.SpeciesTableName, Utils.ConvertToInt(e.Value.ToString()));
                            if (spcRec1 == null)
                            {
                                //e.Valid = false;
                                //e.ErrorText = "Species is invalid.";
                                //return;
                                PopupSpeciesForm spcForm = new PopupSpeciesForm(ref projectContext,
                                    CurrentProject.SpeciesTableName);
                                spcForm.StartPosition = FormStartPosition.CenterParent;
                                spcForm.ShowDialog();
                                if (!string.IsNullOrEmpty(spcForm.code))
                                {
                                    e.Value = spcForm.code;
                                    spcRec1 = ProjectBLL.GetSpecieByCode(ref projectContext, CurrentProject.SpeciesTableName, Utils.ConvertToInt(e.Value.ToString()));
                                    defTreeSpecies = spcRec1;
                                }
                                else
                                {
                                    e.Valid = false;
                                    e.ErrorText = "Species is invalid.";
                                    return;
                                }
                                spcForm.Dispose();
                            }
                            else
                            {
                                e.Value = spcRec1.Abbreviation;
                                defTreeSpecies = spcRec1;
                            }
                        }
                        else
                        {
                            // Abrv was entered
                            Species spcRec2 = ProjectBLL.GetSpecieByAbbrev(ref projectContext,
                                CurrentProject.SpeciesTableName, e.Value.ToString());
                            if (spcRec2 == null)
                            {
                                //e.Valid = false;
                                //e.ErrorText = "Species is invalid.";
                                //return;
                                PopupSpeciesForm spcForm = new PopupSpeciesForm(ref projectContext,
                                    CurrentProject.SpeciesTableName);
                                spcForm.StartPosition = FormStartPosition.CenterParent;
                                spcForm.ShowDialog();
                                if (!string.IsNullOrEmpty(spcForm.code))
                                {
                                    e.Value = spcForm.code;
                                    spcRec2 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, e.Value.ToString());
                                    defTreeSpecies = spcRec2;
                                }
                                else
                                {
                                    e.Valid = false;
                                    e.ErrorText = "Species is invalid.";
                                    return;
                                }
                                spcForm.Dispose();
                            }
                            else
                                defTreeSpecies = spcRec2;
                        }
                    }
                    treeRec.SpeciesAbbreviation = e.Value.ToString();
                    Calc.tree.SpeciesAbbreviation = treeRec.SpeciesAbbreviation;
                    projectContext.SaveChanges();
                    break;
                case "TreeStatusDisplayCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length > 1)
                        {
                            e.Valid = false;
                            e.ErrorText = "Value is too large.";
                            return;
                        }
                        if (!ProjectBLL.DoesStatusExist(ref projectContext, CurrentProject.TreeStatusTableName,
                                e.Value.ToString()))
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Tree Status is invalid.";
                            //return;
                            PopupTreeStatusForm tsForm = new PopupTreeStatusForm(ref projectContext,
                                    CurrentProject.TreeStatusTableName);
                            tsForm.StartPosition = FormStartPosition.CenterParent;
                            tsForm.ShowDialog();
                            if (!string.IsNullOrEmpty(tsForm.code))
                                e.Value = tsForm.code;
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Tree Status is invalid.";
                                return;
                            }
                            tsForm.Dispose();
                        }
                    }
                    treeRec.TreeStatusDisplayCode = e.Value.ToString();
                    Calc.tree.TreeStatusDisplayCode = treeRec.TreeStatusDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "TreeCount":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) < 0 || Utils.ConvertToInt(e.Value.ToString()) > 999)
                        {
                            e.Valid = false;
                            e.ErrorText = "Tree Count MUST be 0 - 999.";
                            return;
                        }
                        treeRec.TreeCount = Utils.ConvertToTiny(e.Value.ToString());
                        DoCalcForTree(true, treeRec, true);
                        if (treeRec.TreeType != "C")
                            LoadSegments(focusedRow, focusedCol);
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "Value must be 1 - 999.";
                        return;
                    }
                    treeRec.TreeCount = Utils.ConvertToTiny(e.Value.ToString());
                    Calc.tree.TreeCount = treeRec.TreeCount;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    LoadSegments(focusedRow, focusedCol);
                    break;
                case "Dbh":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        e.Value = 0.0;
                    int nPos = e.Value.ToString().IndexOf(".");
                    if (nPos != -1)
                    {
                        if (e.Value.ToString().Length - 1 > nPos + 1)
                        {
                            e.Valid = false;
                            e.ErrorText = "Too many characters to the right of the decimal.";
                            return;
                        }
                    }
                    float MaxDia = 0;
                    if (defTreeSpecies == null)
                    {
                        GetTreeSpecies(gridViewSegments.GetFocusedRowCellDisplayText("Species"));
                        if (defTreeSpecies == null)
                            MaxDia = 0;
                        else
                            MaxDia = (float)defTreeSpecies.MaxDia;
                    }
                    else
                        MaxDia = (float)defTreeSpecies.MaxDia;
                    if (MaxDia == 0)
                        MaxDia = (float)299.99;
                    if (Utils.ConvertToFloat(e.Value.ToString()) < 0.0 ||
                        Utils.ConvertToFloat(e.Value.ToString()) > MaxDia)
                    {
                        e.Valid = false;
                        e.ErrorText = string.Format("Dbh MUST be 0.0 - {0:0.00}.", MaxDia);
                        return;
                    }

                    string csTmp2 = string.Empty;
                    if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                    {
                        Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext, defTreeSpecies.FormFactorsTableName,
                            defTreeSpecies.FormFactorsDisplayCode);
                        float cA = 0.0f;
                        float cB = 0.0f;
                        if (
                            Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("FormFactor")) ==
                            0 && ff != null)
                        {
                            cA = (float)ff.Avalue;
                            cB = (float)ff.Bvalue;
                            if (Utils.ConvertToFloat(e.Value.ToString()) > 0 &&
                                Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                if (
                                    Utils.ConvertToFloat(
                                        gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                {
                                    float sqValue =
                                        (float)
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")));
                                    float fCalcFF = (float)Utils.ConvertToFloat(e.Value.ToString()) *
                                                    (cA + cB * sqValue);
                                    fCalcFF = (float)fCalcFF / Utils.ConvertToFloat(e.Value.ToString());
                                    if (fCalcFF > 0.99)
                                        fCalcFF = (float).99;
                                    csTmp2 = string.Format("%2.0f", fCalcFF * 100);
                                    gridViewSegments.SetFocusedRowCellValue("FormFactor", csTmp2);
                                }
                            }
                        }
                    }
                    treeRec.Dbh = Utils.ConvertToFloat(e.Value.ToString());
                    Calc.tree.Dbh = treeRec.Dbh;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    break;
                case "FormPoint":
                    if (Utils.ConvertToInt(e.Value.ToString()) < 4 || Utils.ConvertToInt(e.Value.ToString()) > 50)
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) != 0)
                        {
                            e.Valid = false;
                            e.ErrorText = "Form Point MUST be 4 - 50.";
                            return;
                        }
                    }
                    else
                    {
                        defTreeFP = Utils.ConvertToTiny(e.Value.ToString());
                    }
                    treeRec.FormPoint = (short)Utils.ConvertToInt(e.Value.ToString());
                    Calc.tree.FormPoint = treeRec.FormPoint;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    break;
                case "FormFactor":
                    if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")) == 1)
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) < 25 && Utils.ConvertToInt(e.Value.ToString()) > 99)
                        {
                            e.Valid = false;
                            e.ErrorText = "Form Factor MUST be 25 - 99.";
                            return;
                        }
                    }
                    else
                    {
                        e.Valid = false;
                        e.ErrorText = "Modify Form Factor on Segment 1 Line.";
                        return;
                    }
                    treeRec.ReportedFormFactor = (short)Utils.ConvertToInt(e.Value.ToString());
                    Calc.tree.ReportedFormFactor = treeRec.ReportedFormFactor;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    break;
                case "Tdf":
                    if (Utils.IsValidInt(e.Value.ToString()))
                    {
                        if (e.Value.ToString().Length == 1)
                            e.Value = e.Value.ToString() + "0";
                    }
                    if (e.Value.ToString().StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) > 0 &&
                            Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) !=
                                Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")))
                            {
                                e.Valid = false;
                                e.ErrorText =
                                    "When TDF is 0 (Total Height), Bole Height and Total Height MUST be equal.";
                            }
                        }
                        else if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) > 0)
                        {
                            gridViewSegments.SetFocusedRowCellValue("TotalHeight",
                                gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight"));
                        }
                        else if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            gridViewSegments.SetFocusedRowCellValue("BoleHeight",
                                gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight"));
                        }
                    }
                    defTreeTDF = e.Value.ToString();
                    treeRec.TreeStatusDisplayCode = e.Value.ToString();
                    Calc.tree.TreeStatusDisplayCode = treeRec.TreeStatusDisplayCode;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    //LoadTreeFromTreeEdit();
                    break;
                case "BoleHeight":
                    float MaxHeight = 0;
                    if (defTreeSpecies == null)
                        GetTreeSpecies(gridViewSegments.GetFocusedRowCellDisplayText("Species"));
                    else
                        MaxHeight = (float)defTreeSpecies.MaxHeight;
                    if (MaxHeight == 0)
                        MaxHeight = 351;
                    if (Utils.ConvertToFloat(e.Value.ToString()) <=
                        Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("FormPoint")) ||
                        Utils.ConvertToFloat(e.Value.ToString()) > MaxHeight)
                    {
                        e.Valid = false;
                        e.ErrorText = string.Format(
                            "Bole Height MUST be greater than Form Point and less than {0:0.0}", MaxHeight);
                        return;
                    }
                    if (gridViewSegments.GetFocusedRowCellDisplayText("Tdf").StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0 &&
                            Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(e.Value.ToString()) > 0 !=
                                Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                gridViewSegments.SetFocusedRowCellValue("TotalHeight", e.Value.ToString());
                            }
                        }
                        else
                        {
                            gridViewSegments.SetFocusedRowCellValue("TotalHeight", e.Value.ToString());
                            if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                            {
                                Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext,
                                    defTreeSpecies.FormFactorsTableName, defTreeSpecies.FormFactorsDisplayCode);
                                float cA = 0.0f;
                                float cB = 0.0f;
                                if (
                                    Utils.ConvertToInt(
                                        gridViewSegments.GetFocusedRowCellDisplayText("FormFactor")) == 0 &&
                                    ff != null)
                                {
                                    cA = (float)ff.Avalue;
                                    cB = (float)ff.Bvalue;
                                    if (
                                        Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("Dbh")) >
                                        0 &&
                                        Utils.ConvertToFloat(
                                            gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 0)
                                    {
                                        if (
                                            Utils.ConvertToFloat(
                                                gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                        {
                                            float sqValue =
                                                (float)
                                                ((float)17.3 /
                                                 (float)
                                                 Utils.ConvertToFloat(
                                                     gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                                ((float)17.3 /
                                                 (float)
                                                 Utils.ConvertToFloat(
                                                     gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")));
                                            float fCalcFF =
                                                (float)
                                                Utils.ConvertToFloat(
                                                    gridViewSegments.GetFocusedRowCellDisplayText("Dbh")) *
                                                (cA + cB * sqValue);
                                            fCalcFF = (float)fCalcFF /
                                                      Utils.ConvertToFloat(
                                                          gridViewSegments.GetFocusedRowCellDisplayText("Dbh"));
                                            if (fCalcFF > 0.99)
                                                fCalcFF = (float).99;
                                            csTmp2 = string.Format("{0:0.0}", fCalcFF * 100);
                                            gridViewSegments.SetFocusedRowCellValue("TotalHeight", csTmp2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 0 &&
                             gridViewSegments.GetFocusedRowCellDisplayText("Dbh") != "+")
                    {
                        if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) <
                            Utils.ConvertToInt(e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Bole Height MUST be less than Total Height.";
                            return;
                        }
                    }
                    treeRec.BoleHeight = (short)Utils.ConvertToInt(e.Value.ToString());
                    Calc.tree.BoleHeight = treeRec.BoleHeight;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    //LoadTreeFromTreeEdit();
                    break;
                case "TotalHeight":
                    if (gridViewSegments.GetFocusedRowCellDisplayText("Tdf").StartsWith("0"))
                    {
                        if (Utils.ConvertToInt(e.Value.ToString()) > 0 &&
                            Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) > 0)
                        {
                            if (Utils.ConvertToInt(e.Value.ToString()) > 0 !=
                                Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) >
                                0)
                            {
                                gridViewSegments.SetFocusedRowCellValue("BoleHeight", e.Value.ToString());
                            }
                        }
                        else
                        {
                            gridViewSegments.SetFocusedRowCellValue("BoleHeight", e.Value.ToString());
                        }
                    }
                    else if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) > 0 && Utils.ConvertToInt(e.Value.ToString()) > 0)
                    {
                        if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) > Utils.ConvertToInt(e.Value.ToString()))
                        {
                            e.Valid = false;
                            ;
                            e.ErrorText = "Total Height MUST be greater than Bole Height.";
                            return;
                        }
                    }
                    else if (Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("BoleHeight")) == 0 && Utils.ConvertToInt(e.Value.ToString()) > 0)
                    {
                        e.Valid = false;
                        e.ErrorText = "MUST enter Bole Height if Total Height entered.";
                        return;
                    }
                    if (defTreeSpecies == null)
                        GetTreeSpecies(gridViewSegments.GetFocusedRowCellDisplayText("Species"));
                    if (Utils.ConvertToFloat(defTreeSpecies.FormFactorsDisplayCode) == 0)
                    {
                        Formfactor ff = ProjectBLL.GetFormFactor(ref projectContext, defTreeSpecies.FormFactorsTableName,
                            defTreeSpecies.FormFactorsDisplayCode);
                        float cA = 0.0f;
                        float cB = 0.0f;
                        if (
                            Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("FormFactor")) ==
                            0 && ff != null)
                        {
                            if (Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("Dbh")) > 0 &&
                                Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) >
                                0)
                            {
                                if (
                                    Utils.ConvertToFloat(
                                        gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")) > 20)
                                {
                                    cA = (float)ff.Avalue;
                                    cB = (float)ff.Bvalue;
                                    float sqValue =
                                        (float)
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight"))) *
                                        ((float)17.3 /
                                         (float)
                                         Utils.ConvertToFloat(
                                             gridViewSegments.GetFocusedRowCellDisplayText("TotalHeight")));
                                    float fCalcFF =
                                        (float)
                                        Utils.ConvertToFloat(gridViewSegments.GetFocusedRowCellDisplayText("Dbh")) *
                                        (cA + cB * sqValue);
                                    fCalcFF = (float)fCalcFF /
                                              Utils.ConvertToFloat(
                                                  gridViewSegments.GetFocusedRowCellDisplayText("Dbh"));
                                    if (fCalcFF > 0.99)
                                        fCalcFF = (float).99;
                                    csTmp2 = string.Format("{0:0.0}", fCalcFF * 100);
                                    gridViewSegments.SetFocusedRowCellValue("FormFactor", csTmp2);
                                }
                            }
                        }
                    }
                    treeRec.TotalHeight = (short)Utils.ConvertToInt(e.Value.ToString());
                    Calc.tree.TotalHeight = treeRec.TotalHeight;
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                        LoadSegments(focusedRow, focusedCol);
                    //LoadTreeFromTreeEdit();
                    break;
                case "CrownPosition":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesCrownPositionExist(ref projectContext, CurrentProject.CrownPositionTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "CP is invalid.";
                            return;
                        }
                    }
                    treeRec.CrownPositionDisplayCode = e.Value.ToString();
                    Calc.tree.CrownPositionDisplayCode = treeRec.CrownPositionDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "CrownRatio":
                    treeRec.CrownRatioDisplayCode = e.Value.ToString();
                    Calc.tree.CrownRatioDisplayCode = treeRec.CrownRatioDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "Vigor":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesVigorExist(ref projectContext, CurrentProject.VigorTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Vigor is invalid.";
                            return;
                        }
                    }
                    treeRec.VigorDisplayCode = e.Value.ToString();
                    Calc.tree.VigorDisplayCode = treeRec.VigorDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "Damage":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesDamageExist(ref projectContext, CurrentProject.DamageTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "Damage is invalid.";
                            return;
                        }
                    }
                    treeRec.DamageDisplayCode = e.Value.ToString();
                    Calc.tree.DamageDisplayCode = treeRec.DamageDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "UserDefined":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (
                            !ProjectBLL.DoesT5Exist(ref projectContext, CurrentProject.UserDefinedTableName,
                                e.Value.ToString()))
                        {
                            e.Valid = false;
                            e.ErrorText = "User Defined is invalid.";
                            return;
                        }
                    }
                    treeRec.UserDefinedDisplayCode = e.Value.ToString();
                    Calc.tree.UserDefinedDisplayCode = treeRec.UserDefinedDisplayCode;
                    projectContext.SaveChanges();
                    break;
                case "SortCode":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        gridViewSegments.FocusedRowHandle = gridViewSegments.FocusedRowHandle + 1;
                        gridViewSegments.FocusedColumn = gridViewSegments.Columns["Species"];
                    }
                    else
                    {
                        if (defTreeSpecies == null)
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, gridViewSegments.GetFocusedRowCellDisplayText("Species"));
                        Sort sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                        if (sRec == null)
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Sort is invalid.";
                            //return;
                            PopupSortForm sortForm = new PopupSortForm(ref projectContext, CurrentProject.SortsTableName, defTreeSpecies.SpeciesGroup);
                            sortForm.StartPosition = FormStartPosition.CenterParent;
                            sortForm.ShowDialog();
                            if (!string.IsNullOrEmpty(sortForm.code))
                            {
                                e.Value = sortForm.code;
                                sRec = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                            }
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Sort is invalid.";
                                return;
                            }
                            sortForm.Dispose();
                        }
                    }
                    LoadSortFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    //ProcessSortValidation(Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), "SortCode", e.Value.ToString(), treeRec);
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "GradeCode":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        if (defTreeSpecies == null)
                            defTreeSpecies = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, gridViewSegments.GetFocusedRowCellDisplayText("Species"));
                        Grade gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                        if (gRec == null)
                        {
                            //e.Valid = false;
                            //e.ErrorText = "Grade is invalid.";
                            //return;
                            PopupGradeForm gradeForm = new PopupGradeForm(ref projectContext, CurrentProject.GradesTableName, defTreeSpecies.SpeciesGroup);
                            gradeForm.StartPosition = FormStartPosition.CenterParent;
                            gradeForm.ShowDialog();
                            if (!string.IsNullOrEmpty(gradeForm.code))
                            {
                                e.Value = gradeForm.code;
                                gRec = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, e.Value.ToString(), defTreeSpecies.SpeciesGroup);
                            }
                            else
                            {
                                e.Valid = false;
                                e.ErrorText = "Grade is invalid.";
                                return;
                            }
                            gradeForm.Dispose();
                        }
                        LoadGradeFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                        DoCalcForTree(true, treeRec, true);
                        projectContext.SaveChanges();
                        //ProcessGradeValidation(Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), "GradeCode", e.Value.ToString(), treeRec);
                        if (treeRec.TreeType != "C")
                        {
                            LoadSegments(focusedRow, focusedCol);
                            gridViewSegments.RefreshData();
                        }
                        //LoadTreeFromTreeEdit();
                    }
                    break;
                case "Length":
                    LoadLengthFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "BdFtLd":
                    if (string.IsNullOrEmpty(e.Value.ToString()))
                        gridViewSegments.FocusedColumn = gridViewSegments.Columns["SortCode"];
                    LoadBdFtLDFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "BdFtDd":
                    LoadBdFtDDFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "CuFtLd":
                    LoadCuFtLDFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "CuFtDd":
                    LoadCuFtDDFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
                case "BdFtPd":
                    LoadBdFtPDFromTreeEdit(ref treeRec, Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber")), e.Value.ToString());
                    DoCalcForTree(true, treeRec, true);
                    projectContext.SaveChanges();
                    if (treeRec.TreeType != "C")
                    {
                        LoadSegments(focusedRow, focusedCol);
                        gridViewSegments.RefreshData();
                    }
                    //LoadTreeFromTreeEdit();
                    break;
            }
        }

        private void LoadSortFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.Sort1 = pField;
                    Calc.tree.Sort1 = rec.Sort1;
                    break;
                case 2:
                    rec.Sort2 = pField;
                    Calc.tree.Sort2 = rec.Sort2;
                    break;
                case 3:
                    rec.Sort3 = pField;
                    Calc.tree.Sort3 = rec.Sort3;
                    break;
                case 4:
                    rec.Sort4 = pField;
                    Calc.tree.Sort4 = rec.Sort4;
                    break;
                case 5:
                    rec.Sort5 = pField;
                    Calc.tree.Sort5 = rec.Sort5;
                    break;
                case 6:
                    rec.Sort6 = pField;
                    Calc.tree.Sort6 = rec.Sort6;
                    break;
                case 7:
                    rec.Sort7 = pField;
                    Calc.tree.Sort7 = rec.Sort7;
                    break;
                case 8:
                    rec.Sort8 = pField;
                    Calc.tree.Sort8 = rec.Sort8;
                    break;
                case 9:
                    rec.Sort9 = pField;
                    Calc.tree.Sort9 = rec.Sort9;
                    break;
                case 10:
                    rec.Sort10 = pField;
                    Calc.tree.Sort10 = rec.Sort10;
                    break;
                case 11:
                    rec.Sort11 = pField;
                    Calc.tree.Sort11 = rec.Sort11;
                    break;
                case 12:
                    rec.Sort12 = pField;
                    Calc.tree.Sort12 = rec.Sort12;
                    break;
            }
        }

        private void LoadGradeFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.Grade1 = pField;
                    Calc.tree.Grade1 = rec.Grade1;
                    break;
                case 2:
                    rec.Grade2 = pField;
                    Calc.tree.Grade2 = rec.Grade2;
                    break;
                case 3:
                    rec.Grade3 = pField;
                    Calc.tree.Grade3 = rec.Grade3;
                    break;
                case 4:
                    rec.Grade4 = pField;
                    Calc.tree.Grade4 = rec.Grade4;
                    break;
                case 5:
                    rec.Grade5 = pField;
                    Calc.tree.Grade5 = rec.Grade5;
                    break;
                case 6:
                    rec.Grade6 = pField;
                    Calc.tree.Grade6 = rec.Grade6;
                    break;
                case 7:
                    rec.Grade7 = pField;
                    Calc.tree.Grade7 = rec.Grade7;
                    break;
                case 8:
                    rec.Grade8 = pField;
                    Calc.tree.Grade8 = rec.Grade8;
                    break;
                case 9:
                    rec.Grade9 = pField;
                    Calc.tree.Grade9 = rec.Grade9;
                    break;
                case 10:
                    rec.Grade10 = pField;
                    Calc.tree.Grade10 = rec.Grade10;
                    break;
                case 11:
                    rec.Grade11 = pField;
                    Calc.tree.Grade11 = rec.Grade11;
                    break;
                case 12:
                    rec.Grade12 = pField;
                    Calc.tree.Grade12 = rec.Grade12;
                    break;
            }
        }

        private void LoadLengthFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.Length1 = pField;
                    Calc.tree.Length1 = rec.Length1;
                    break;
                case 2:
                    rec.Length2 = pField;
                    Calc.tree.Length2 = rec.Length2;
                    break;
                case 3:
                    rec.Length3 = pField;
                    Calc.tree.Length3 = rec.Length3;
                    break;
                case 4:
                    rec.Length4 = pField;
                    Calc.tree.Length4 = rec.Length4;
                    break;
                case 5:
                    rec.Length5 = pField;
                    Calc.tree.Length5 = rec.Length5;
                    break;
                case 6:
                    rec.Length6 = pField;
                    Calc.tree.Length6 = rec.Length6;
                    break;
                case 7:
                    rec.Length7 = pField;
                    Calc.tree.Length7 = rec.Length7;
                    break;
                case 8:
                    rec.Length8 = pField;
                    Calc.tree.Length8 = rec.Length8;
                    break;
                case 9:
                    rec.Length9 = pField;
                    Calc.tree.Length9 = rec.Length9;
                    break;
                case 10:
                    rec.Length10 = pField;
                    Calc.tree.Length10 = rec.Length10;
                    break;
                case 11:
                    rec.Length11 = pField;
                    Calc.tree.Length11 = rec.Length11;
                    break;
                case 12:
                    rec.Length12 = pField;
                    Calc.tree.Length12 = rec.Length12;
                    break;
            }
        }

        private void LoadBdFtLDFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.BdFtLd1 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd1 = rec.BdFtLd1;
                    break;
                case 2:
                    rec.BdFtLd2 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd2 = rec.BdFtLd2;
                    break;
                case 3:
                    rec.BdFtLd3 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd3 = rec.BdFtLd3;
                    break;
                case 4:
                    rec.BdFtLd4 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd4 = rec.BdFtLd4;
                    break;
                case 5:
                    rec.BdFtLd5 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd5 = rec.BdFtLd5;
                    break;
                case 6:
                    rec.BdFtLd6 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd6 = rec.BdFtLd6;
                    break;
                case 7:
                    rec.BdFtLd7 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd7 = rec.BdFtLd7;
                    break;
                case 8:
                    rec.BdFtLd8 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd8 = rec.BdFtLd8;
                    break;
                case 9:
                    rec.BdFtLd9 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd9 = rec.BdFtLd9;
                    break;
                case 10:
                    rec.BdFtLd10 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd10 = rec.BdFtLd10;
                    break;
                case 11:
                    rec.BdFtLd11 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd11 = rec.BdFtLd11;
                    break;
                case 12:
                    rec.BdFtLd12 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtLd12 = rec.BdFtLd12;
                    break;
            }
        }

        private void LoadBdFtDDFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.BdFtDd1 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd1 = rec.BdFtDd1;
                    break;
                case 2:
                    rec.BdFtDd2 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd2 = rec.BdFtDd2;
                    break;
                case 3:
                    rec.BdFtDd3 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd3 = rec.BdFtDd3;
                    break;
                case 4:
                    rec.BdFtDd4 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd4 = rec.BdFtDd4;
                    break;
                case 5:
                    rec.BdFtDd5 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd5 = rec.BdFtDd5;
                    break;
                case 6:
                    rec.BdFtDd6 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd6 = rec.BdFtDd6;
                    break;
                case 7:
                    rec.BdFtDd7 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd7 = rec.BdFtDd7;
                    break;
                case 8:
                    rec.BdFtDd8 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd8 = rec.BdFtDd8;
                    break;
                case 9:
                    rec.BdFtDd9 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd9 = rec.BdFtDd9;
                    break;
                case 10:
                    rec.BdFtDd10 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd10 = rec.BdFtDd10;
                    break;
                case 11:
                    rec.BdFtDd11 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd11 = rec.BdFtDd11;
                    break;
                case 12:
                    rec.BdFtDd12 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.BdFtDd12 = rec.BdFtDd12;
                    break;
            }
        }

        private void LoadCuFtLDFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.CuFtLd1 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd1 = rec.CuFtLd1;
                    break;
                case 2:
                    rec.CuFtLd2 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd2 = rec.CuFtLd2;
                    break;
                case 3:
                    rec.CuFtLd3 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd3 = rec.CuFtLd3;
                    break;
                case 4:
                    rec.CuFtLd4 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd4 = rec.CuFtLd4;
                    break;
                case 5:
                    rec.CuFtLd5 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd5 = rec.CuFtLd5;
                    break;
                case 6:
                    rec.CuFtLd6 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd6 = rec.CuFtLd6;
                    break;
                case 7:
                    rec.CuFtLd7 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd7 = rec.CuFtLd7;
                    break;
                case 8:
                    rec.CuFtLd8 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd8 = rec.CuFtLd8;
                    break;
                case 9:
                    rec.CuFtLd9 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd9 = rec.CuFtLd9;
                    break;
                case 10:
                    rec.CuFtLd10 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd10 = rec.CuFtLd10;
                    break;
                case 11:
                    rec.CuFtLd11 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd11 = rec.CuFtLd11;
                    break;
                case 12:
                    rec.CuFtLd12 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtLd12 = rec.CuFtLd12;
                    break;
            }
        }

        private void LoadCuFtDDFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.CuFtDd1 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd1 = rec.CuFtDd1;
                    break;
                case 2:
                    rec.CuFtDd2 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd2 = rec.CuFtDd2;
                    break;
                case 3:
                    rec.CuFtDd3 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd3 = rec.CuFtDd3;
                    break;
                case 4:
                    rec.CuFtDd4 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd4 = rec.CuFtDd4;
                    break;
                case 5:
                    rec.CuFtDd5 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd5 = rec.CuFtDd5;
                    break;
                case 6:
                    rec.CuFtDd6 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd6 = rec.CuFtDd6;
                    break;
                case 7:
                    rec.CuFtDd7 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd7 = rec.CuFtDd7;
                    break;
                case 8:
                    rec.CuFtDd8 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd8 = rec.CuFtDd8;
                    break;
                case 9:
                    rec.CuFtDd9 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd9 = rec.CuFtDd9;
                    break;
                case 10:
                    rec.CuFtDd10 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd10 = rec.CuFtDd10;
                    break;
                case 11:
                    rec.CuFtDd11 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd11 = rec.CuFtDd11;
                    break;
                case 12:
                    rec.CuFtDd12 = (short)Utils.ConvertToInt(pField);
                    Calc.tree.CuFtDd12 = rec.CuFtDd12;
                    break;
            }
        }

        private void LoadBdFtPDFromTreeEdit(ref Tree rec, int segment, string pField)
        {
            switch (segment)
            {
                case 1:
                    rec.BdFtPd1 = pField;
                    Calc.tree.BdFtPd1 = rec.BdFtPd1;
                    break;
                case 2:
                    rec.BdFtPd2 = pField;
                    Calc.tree.BdFtPd2 = rec.BdFtPd2;
                    break;
                case 3:
                    rec.BdFtPd3 = pField;
                    Calc.tree.BdFtPd3 = rec.BdFtPd3;
                    break;
                case 4:
                    rec.BdFtPd4 = pField;
                    Calc.tree.BdFtPd4 = rec.BdFtPd4;
                    break;
                case 5:
                    rec.BdFtPd5 = pField;
                    Calc.tree.BdFtPd5 = rec.BdFtPd5;
                    break;
                case 6:
                    rec.BdFtPd6 = pField;
                    Calc.tree.BdFtPd6 = rec.BdFtPd6;
                    break;
                case 7:
                    rec.BdFtPd7 = pField;
                    Calc.tree.BdFtPd7 = rec.BdFtPd7;
                    break;
                case 8:
                    rec.BdFtPd8 = pField;
                    Calc.tree.BdFtPd8 = rec.BdFtPd8;
                    break;
                case 9:
                    rec.BdFtPd9 = pField;
                    Calc.tree.BdFtPd9 = rec.BdFtPd9;
                    break;
                case 10:
                    rec.BdFtPd10 = pField;
                    Calc.tree.BdFtPd10 = rec.BdFtPd10;
                    break;
                case 11:
                    rec.BdFtPd11 = pField;
                    Calc.tree.BdFtPd11 = rec.BdFtPd11;
                    break;
                case 12:
                    rec.BdFtPd12 = pField;
                    Calc.tree.BdFtPd12 = rec.BdFtPd12;
                    break;
            }
        }

        private void barButtonItem75_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Populate CountWithDia
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Build Count With Dbh Table...");

            ProjectBLL.DeleteCountWithDiaTable(ref projectContext);
            sColl.Clear();
            gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            ProjectBLL.BuildCountWithDiaTable(ref projectContext, sColl);
            gridViewStands.ClearSelection();
            SplashScreenManager.CloseForm();
        }

        private void barButtonItem76_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Populate AdjCruisePlots
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            SplashScreenManager.Default.SetWaitFormDescription("Build Adj Cruise Plots Table...");

            ProjectBLL.DeleteAdjCruisePlots(ref projectContext);
            sColl.Clear();
            gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            ProjectBLL.BuildAdjCruisePlots(ref projectContext, sColl);
            gridViewStands.ClearSelection();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem77_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Process Count Segments
            ProjectBLL.ProcessCountRecords(ref projectContext);
        }

        private void barButtonItem68_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            // Reset Count
            ProjectBLL.ResetCountRecords(ref projectContext);
        }

        private bool IsCountTree(GridView view, int row)
        {
            GridColumn col = view.Columns["SegmentNumber"];
            int seg = Utils.ConvertToInt(view.GetRowCellValue(row, col).ToString());
            if (seg == 0)
                return true;
            return false;
        }

        private void gridViewSegments_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            int segment = -1;

            if (view.FocusedRowHandle == -1)
                return;

            if (view.FocusedColumn.FieldName == "SegmentNumber")
                e.Cancel = true;
            segment = Utils.ConvertToInt(gridViewSegments.GetFocusedRowCellDisplayText("SegmentNumber").ToString());
            if (segment > 1)
            {
                switch (view.FocusedColumn.FieldName)
                {
                    case "PlotNumber":
                    case "TreeNumber":
                    case "PlotFactorInput":
                    case "Age":
                    case "Species":
                    case "TreeStatusDisplayCode":
                    case "TreeCount":
                    case "Dbh":
                    case "FormPoint":
                    case "FormFactor":
                    case "Tdf":
                    case "BoleHeight":
                    case "TotalHeight":
                    case "CrownPosition":
                    case "CrownRatio":
                    case "Vigor":
                    case "Damage":
                    case "UserDefined":
                    case "TreeBasalArea":
                    case "TreeTreesPerAcre":
                    case "CubicNetVolume":
                    case "ScribnerNetVolume":
                    case "CalcTopDia":
                    case "CalcButtDia":
                    case "CalcAccumulatedLength":
                    case "Ao":
                    case "Bark":
                    case "LogValue":
                        e.Cancel = true;
                        break;
                }
            }
            else
            {
                if (IsCountTree(view, view.FocusedRowHandle))
                {
                    switch (view.FocusedColumn.FieldName)
                    {
                        case "TreeBasalArea":
                        case "TreeTreesPerAcre":
                        case "CubicNetVolume":
                        case "ScribnerNetVolume":
                        case "BoleHeight":
                        case "TotalHeight":
                        case "SortCode":
                        case "GradeCode":
                        case "BdFtLD":
                        case "BdFtDD":
                        case "CuFtLD":
                        case "CuFtDD":
                        case "BdFtPD":
                        case "Length":
                        case "CalcTopDia":
                        case "CalcButtDia":
                        case "CalcAccumulatedLength":
                        case "Tdf":
                        //case "FormPoint":
                        case "FormFactor":
                        case "Ao":
                        case "Bark":
                        case "CrownPosition":
                        case "CrownRatio":
                        case "Vigor":
                        case "Damage":
                        case "UserDefined":
                        case "LogValue":
                            e.Cancel = true;
                            break;
                    }
                }
                else
                {
                    switch (view.FocusedColumn.FieldName)
                    {
                        case "TreeBasalArea":
                        case "TreeTreesPerAcre":
                        case "CubicNetVolume":
                        case "ScribnerNetVolume":
                        case "CalcTopDia":
                        case "CalcButtDia":
                        case "CalcAccumulatedLength":
                        case "Ao":
                        case "Bark":
                        case "LogValue":
                            e.Cancel = true;
                            break;
                    }
                }
            }
        }

        private void advBandedGridViewPlots_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;

            switch (view.FocusedColumn.FieldName)
            {
                case "BasalAreaAcre":
                case "TreesPerAcre":
                case "NetCfAcre":
                case "NetBfAcre":
                    e.Cancel = true;
                    break;
            }
        }

        private void barButtonItem30_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            // Count With Dbh
            if (CurrentStand == null)
            {
                XtraMessageBox.Show("A Stand must be selected.", "Status");
                return;
            }
            CountWithDbhForm frm = new CountWithDbhForm(ref projectContext, CurrentStand);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem69_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            // Count Avgs
            if (CurrentStand == null)
            {
                XtraMessageBox.Show("A Stand must be selected.", "Status");
                return;
            }
            CountAvgForm frm = new CountAvgForm(ref projectContext, CurrentStand);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void barButtonItem73_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Init TreeType in TreeSegments
            var trees = projectContext.Trees
                .OrderBy(t => t.PlotNumber)
                .ThenBy(t => t.TreeNumber)
                .Select(t => new { t.TreesId, t.TreeType}).ToList();
            //var trees = from t in projectContext.Trees
            //    orderby t.PlotNumber, t.TreeNumber
            //    select new
            //    {
            //        TreesId = t.TreesId,
            //        TreeType = t.TreeType
            //    };
            foreach (var treeItem in trees)
            {
                var segments = projectContext.Treesegments
                .OrderBy(s => s.SegmentNumber)
                .Where(s => s.TreesId == treeItem.TreesId)
                .ToList();
                //var segments = from s in projectContext.Treesegments
                //    orderby s.PlotNumber, s.TreeNumber, s.SegmentNumber
                //    select new
                //    {
                //        TreeSegmentsId = s.TreeSegmentsId,
                //        TreeType = s.TreeType
                //    };
                foreach (var segmentItem in segments)
                {
                    segmentItem.TreeType = treeItem.TreeType;
                }
            }
            projectContext.SaveChanges();
        }

        private void barButtonItem74_ItemClick(object sender, ItemClickEventArgs e)
        {
            List<Tree> trees = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();
            foreach (Tree treeItem in trees)
            {
                List<Treesegment> segments = projectContext.Treesegments.OrderBy(s => s.SegmentNumber).Where(s => s.TreesId == treeItem.TreesId).ToList();
                foreach (Treesegment segmentItem in segments)
                {
                    segmentItem.Species = segmentItem.Species.Trim();
                    segmentItem.TreeStatusDisplayCode = segmentItem.TreeStatusDisplayCode.Trim();
                }
                treeItem.SpeciesAbbreviation = treeItem.SpeciesAbbreviation.Trim();
                treeItem.TreeStatusDisplayCode = treeItem.TreeStatusDisplayCode.Trim();
            }
            projectContext.SaveChanges();
        }

        private void gridViewStandInputOrig_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            //int rowIndex = e.ListSourceRowIndex;
            //float acres = Convert.ToSingle(tbStandInputNetAcres.Text);
            //float bdft = Convert.ToSingle(gridViewStandInputOrig.GetListSourceRowCellValue(rowIndex, "OrigNetBdFt"));
            //float cuft = Convert.ToSingle(gridViewStandInputOrig.GetListSourceRowCellValue(rowIndex, "OrigNetCuFt"));
            //switch (e.Column.FieldName)
            //{
            //    case "TotalCcf":
            //        if (e.IsGetData)
            //            e.Value = cuft / 100 * acres;
            //        break;
            //    case "TotalMbf":
            //        if (e.IsGetData)
            //            e.Value = bdft / 1000 * acres;
            //        break;
            //}
        }

        private void gridViewStandInputGrown_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            //int rowIndex = e.ListSourceRowIndex;
            //float acres = Convert.ToSingle(tbStandInputNetAcres.Text);
            //float bdft = Convert.ToSingle(gridViewStandInputGrown.GetListSourceRowCellValue(rowIndex, "NetBdFt"));
            //float cuft = Convert.ToSingle(gridViewStandInputGrown.GetListSourceRowCellValue(rowIndex, "NetCuFt"));
            //float totalCuFt = 0;
            //float totalBdFt = 0;
            //switch (e.Column.FieldName)
            //{
            //    case "TotalCcf":
            //        if (e.IsGetData)
            //            e.Value = cuft / 100 * acres;
            //        break;
            //    case "TotalMbf":
            //        if (e.IsGetData)
            //            e.Value = bdft / 1000 * acres;
            //        break;
            //}
        }

        private void btnStandInputGrow_Click(object sender, EventArgs e)
        {
            // Grow Button
            CurrentStand.GrownToDate = Convert.ToDateTime(tbStandInputGrownToDate.Text);
            //ProjectBLL.GrowTheStand();
        }

        private void tbStandInputGrownToDate_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                string[] dateParts = tbStandInputGrownToDate.Text.Split('/');
                DateTime testDate = new DateTime(Convert.ToInt32(dateParts[2]),
                    Convert.ToInt32(dateParts[0]),
                    Convert.ToInt32(dateParts[1]));
                e.Cancel = false;
            }
            catch
            {
                e.Cancel = true;
            }
        }

        private void barButtonItem78_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Cruise To Inventory
            ProjectBLL.DeleteTmpStandInput(ref projectContext);
            ProjectBLL.DeleteStandInput(ref projectContext);
            ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject);
        }

        private void barButtonItem79_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Interpolation
            //float x =interpolation(9759, 120, 10914, 125, 122);
            float x = interpolation(122, 120, 125, 9759, 10914);
            Debug.WriteLine("Value = {0}", x);
        }

        private float interpolation(float si, float siLow, float siHigh, float dataLow, float dataHigh)
        {
            return dataLow+((si-siLow)/(siHigh - siLow)) * (dataHigh - dataLow);

        }

        private void barButtonItem80_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Cruise To Inventory Project
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormDescription("Processing ...");
            ProjectBLL.DeleteTmpStandInput(ref projectContext);
            ProjectBLL.DeleteStandInput(ref projectContext);
            ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject);

            if (projectContext.TmpStandInputExceptions.Any())
                ProcessStandInputErrors();
            SplashScreenManager.CloseForm();
        }

        private void barButtonItem78_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            if (projectContext.TmpStandInputExceptions.Any())
                ProcessStandInputErrors();
        }

        private void ProcessStandInputErrors()
        {
            StandInputErrorsForm frm = new StandInputErrorsForm(ref projectContext);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            if (frm.Process)
            {
                tabPane1.SelectedPage = tabNavigationPageHome;
                // find the stand
                IList source = (IList)ListBindingHelper.GetList(gridControlStands.DataSource, gridControlStands.DataMember);
                PropertyDescriptorCollection coll = ListBindingHelper.GetListItemProperties(gridControlStands.DataSource);
                PropertyDescriptor descTract = coll["TractName"];
                PropertyDescriptor descStand = coll["StandName"];
                foreach (object row in source)
                {
                    object valTract = descTract.GetValue(row);
                    object valStand = descStand.GetValue(row);
                    if (valTract.Equals(frm.siRec.TractName) && valStand.Equals(frm.siRec.StandName))
                    {
                        gridViewStands.FocusedRowHandle = gridViewStands.GetRowHandle(source.IndexOf(row));
                        break;
                    }
                }
                // select the master
                tabPane1.SelectedPage = tabNavigationPageStandMaster;
                ProjectBLL.DeleteTmpStandInput(ref projectContext, frm.siRec.StandsId);
                projectContext.SaveChanges();

                if (!projectContext.TmpStandInputExceptions.Any())
                    barButtonItem78.Visibility = BarItemVisibility.Never;
            }
            frm.Dispose();
        }

        private void barButtonItem81_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Cruise to Inventory Selected Stands
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormDescription("Processing ...");
            List<Stand> tStands = ReportsBLL.BuildStandSelection(gridViewStands);
            foreach (var item in tStands)
            {
                ProjectBLL.DeleteTmpStandInput(ref projectContext, item);
                ProjectBLL.DeleteStandInput(ref projectContext);
                ProjectBLL.ProcessCruiseToForestInventory(ref projectContext, CurrentProject, item, false);
            }
            
            projectContext.SaveChanges();
            SplashScreenManager.CloseForm();
            if (projectContext.TmpStandInputExceptions.Any())
                ProcessStandInputErrors();
        }

        private void barButtonItem82_ItemClick(object sender, ItemClickEventArgs e)
        {
            List<SdCl> colls = locContext.SdCls.ToList();
            foreach (var item in colls)
            {
                item.Cl = (float)item.Cl*100;
            }
            locContext.SaveChanges();
        }

        private void gridViewStandInputOrig_PrintInitialize(object sender, PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }

        private void gridViewStandInputGrown_PrintInitialize(object sender, PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }

        private void gridViewSegments_PrintInitialize(object sender, PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }

        private void advBandedGridViewPlots_PrintInitialize(object sender, PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }

        private void advBandedGridViewTrees_PrintInitialize(object sender, PrintInitializeEventArgs e)
        {
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;
            pb.PageSettings.TopMargin = 10;
            pb.PageSettings.BottomMargin = 10;
            pb.PageSettings.LeftMargin = 5;
            pb.PageSettings.RightMargin = 5;
        }

        private void barButtonItem83_ItemClick(object sender, ItemClickEventArgs e)
        {
            List<Tree> tTrees = projectContext.Trees.ToList();
            foreach (var treeItem in tTrees)
            {
                string pfType = string.Empty;
                switch (treeItem.PlotFactorInput.Substring(0, 1))
                {
                    case "R":
                        pfType = "R";
                        break;
                    case "S":
                        pfType = "S";
                        break;
                    case "F":
                        pfType = "F";
                        break;
                    case "A":
                        pfType = "A";
                        break;
                    default:
                        pfType = "B";
                        break;
                }
                List<Treesegment> tSegments = projectContext.Treesegments.Where(s => s.TreesId == treeItem.TreesId).ToList();
                foreach (var segItem in tSegments)
                {
                    segItem.PfType = pfType;
                }
                treeItem.PfType = pfType;
            }
            projectContext.SaveChanges();
        }

        private void barButtonItem84_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Create Segments For Selected Stands
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");
            //SplashScreenManager.Default.SetWaitFormDescription("Create Segments for Stand(s)");

            sColl = ReportsBLL.BuildStandSelection(gridViewStands);
            Treesegment segRec = null;

            foreach (var item in sColl)
            {
                Stand standItem = item;
                projectContext.Treesegments.RemoveRange(projectContext.Treesegments.Where(s => s.StandsId == standItem.StandsId));
            }
            projectContext.SaveChanges();

            foreach (var item in sColl)
            {
                Stand standItem = item;
                Project.DAL.Project projectItem = CurrentProject;
                Calc.SetCalcContextProjectStand(ref projectContext, MainForm.CurrentProject, ref standItem);
                Calc.projectDataSource = ProjectDataSource;
                SplashScreenManager.Default.SetWaitFormDescription(string.Format("Create Segments for Stand: {0}-{1}", standItem.TractName, standItem.StandName));
                List<Tree> tColl =
                    projectContext.Trees.OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .Where(t => t.StandsId == standItem.StandsId && t.TreeType == "D")
                        .ToList();
                foreach (var treeItem in tColl)
                {
                    DoCalcForTree(false, treeItem, false);
                }
            }
            projectContext.SaveChanges();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem85_ItemClick(object sender, ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");

            gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);

            foreach (var item in sColl)
            {
                Stand standItem = item;
                SplashScreenManager.Default.SetWaitFormDescription(string.Format("Processing Stand: {0}-{1}", standItem.TractName, standItem.StandName));
                List<Tree> tColl =
                    projectContext.Trees.OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .Where(t => t.StandsId == standItem.StandsId)
                        .ToList();
                foreach (var treeItem in tColl)
                {
                    if (treeItem.TotalBdFtGrossVolume == null)
                        treeItem.TotalBdFtGrossVolume = 0;
                    if (treeItem.TotalBdFtNetVolume == null)
                        treeItem.TotalBdFtNetVolume = 0;
                    if (treeItem.TotalCcf == null)
                        treeItem.TotalCcf = 0;
                    if (treeItem.TotalCuFtGrossVolume == null)
                        treeItem.TotalCuFtGrossVolume = 0;
                    if (treeItem.TotalCuFtNetVolume == null)
                        treeItem.TotalCuFtNetVolume = 0;
                    if (treeItem.TotalGrossCubicPerAcre == null)
                        treeItem.TotalGrossCubicPerAcre = 0;
                    if (treeItem.TotalGrossScribnerPerAcre == null)
                        treeItem.TotalGrossScribnerPerAcre = 0;
                    if (treeItem.TotalMbf == null)
                        treeItem.TotalMbf = 0;
                    if (treeItem.TotalNetCubicPerAcre == null)
                        treeItem.TotalNetCubicPerAcre = 0;
                    if (treeItem.TotalNetScribnerPerAcre == null)
                        treeItem.TotalNetScribnerPerAcre = 0;
                    if (treeItem.TotalTonsPerAcre == null)
                        treeItem.TotalTonsPerAcre = 0;
                }
            }
            projectContext.SaveChanges();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem86_ItemClick(object sender, ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");

            gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);

            foreach (var item in sColl)
            {
                Stand standItem = item;
                SplashScreenManager.Default.SetWaitFormDescription(string.Format("Processing Stand: {0}-{1}", standItem.TractName, standItem.StandName));
                List<Plot> pColl = projectContext.Plots.Where(p => p.StandsId == item.StandsId).OrderBy(p => p.UserPlotNumber).ToList();
                foreach (var plotItem in pColl)
                {
                    Plot plotRec = plotItem;
                    if (projectContext.Trees.Any(t => t.PlotId == plotRec.PlotsId) == false)
                    {
                        projectContext.Plots.Remove(plotRec);
                    }
                }
            }
            projectContext.SaveChanges();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem87_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowExcelForm frm = new ShowExcelForm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }

        private void barButtonItem88_ItemClick(object sender, ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            SplashScreenManager.Default.SetWaitFormCaption("Processing");

            gridViewStands.SelectAll();
            sColl = ReportsBLL.BuildStandSelection(gridViewStands);

            foreach (var item in sColl)
            {
                Stand standItem = item;
                SplashScreenManager.Default.SetWaitFormDescription(string.Format("Processing Stand: {0}-{1}", standItem.TractName, standItem.StandName));
                List<Plot> pColl = projectContext.Plots.Where(p => p.StandsId == item.StandsId).OrderBy(p => p.UserPlotNumber).ToList();
                foreach (var plotItem in pColl)
                {
                    bool hasDbh = false;
                    bool hasFp = false;
                    var q2 = (from t in projectContext.Trees
                              where t.StandsId == item.StandsId && t.PlotId == plotItem.PlotsId
                              group t by t.FormPoint into g
                              select new
                              {
                                  tFP = g.Key
                              });
                    foreach (var fp in q2)
                    {
                        switch (fp.tFP)
                        {
                            case 4:
                                hasDbh = true;
                                break;
                            default:
                                hasFp = true;
                                break;
                        }
                    }
                    //Plot plotRec = projectContext.Plots.FirstOrDefault(p => p.StandsId == plotItem.StandsId && p.PlotsId == plotItem.PlotsId);
                    plotItem.BafInAt = "FP";
                    if (hasDbh && hasFp)
                        plotItem.BafInAt = "ALL";
                    else
                        if (hasDbh)
                        plotItem.BafInAt = "DBH";
                    else
                        if (hasDbh == false && hasFp)
                        plotItem.BafInAt = "";
                }
            }
            projectContext.SaveChanges();

            SplashScreenManager.CloseForm();
        }

        private void barButtonItem35_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            // History and Planning
            HistoryPlanningForm frm = new HistoryPlanningForm(ref projectContext, CurrentProject.HistoryPlanningTableName);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void gridView4_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Yarding System
            switch (gridView4.FocusedColumn.FieldName)
            {
                case "Description":
                    if (!string.IsNullOrEmpty(e.Value.ToString()))
                    {
                        string costTableName = string.Empty;
                        if (!string.IsNullOrEmpty(CurrentStand.CostTableName))
                            costTableName = CurrentStand.CostTableName;
                        else
                            costTableName = CurrentProject.CostTableName;
                        Cost costRec = projectContext.Costs.FirstOrDefault(c => c.TableName == costTableName && c.CostType == "YARDING" && c.CostDescription == e.Value.ToString());
                        if (costRec != null)
                        {
                            gridView4.SetRowCellValue(gridView4.FocusedRowHandle, gridView4.Columns["CostUnit"], costRec.Units);
                        }
                        else
                        {
                            XtraMessageBox.Show("Problem finding Yarding Cost");
                            e.Valid = false;
                        }
                    }
                    break;
            }
        }

        private void gridView4_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            // Yarding System
            
        }

        private void gridView4_RowUpdated(object sender, RowObjectEventArgs e)
        {
            // Yarding System
            //projectContext.SaveChanges();
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddYardingSystemRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView4.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteYardingSystemRecord();
                    }
                    break;
            }
        }

        private void AddYardingSystemRecord()
        {
            StandYardingSystem newRec = standYardingSystemBindingSource.Current as StandYardingSystem;
            if (newRec != null)
            {
                StandYardingSystem rec = new StandYardingSystem();
                rec.StandsId = CurrentStand.StandsId;
                rec.Code = string.Empty;
                rec.Description = newRec.Description;
                rec.CostUnit = newRec.CostUnit;
                rec.Percent = newRec.Percent;
                projectContext.StandYardingSystems.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteYardingSystemRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandYardingSystem rec = standYardingSystemBindingSource.Current as StandYardingSystem;
            projectContext.StandYardingSystems.Remove(rec);
            projectContext.SaveChanges();
            LoadStandYardingSystemDataSource();
        }

        private void LoadStandYardingSystemDataSource()
        {
            if (standYardingSystemBindingSource.Count > 0)
                standYardingSystemBindingSource.Clear();
            standYardingSystemBindingSource.DataSource = projectContext.StandYardingSystems.Where(y => y.StandsId == CurrentStand.StandsId).OrderBy(y => y.Description).ToList();
        }

        private bool CheckStandYardingSystem()
        {
            float total = 0;
            for (int i = 0; i < gridView4.DataRowCount; i++)
            {
                total += Utils.ConvertToFloat(gridView4.GetRowCellValue(i, "Percent").ToString());
            }
            if (total > 0)
            {
                if (total != 100)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView2.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandAgeRecord();
                    }
                    break;
            }
        }

        private void gridView2_RowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddStandAgeRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridView2_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            byte i = (byte)(gridView2.DataRowCount+1);
            gridView2.SetRowCellValue(e.RowHandle, gridView2.Columns["AgeCode"], i);
            gridView2.FocusedColumn = gridView2.Columns["Age"];
        }

        private void LoadStandAgeDataSource()
        {
            if (standAgeBindingSource.Count > 0)
                standAgeBindingSource.Clear();
            standAgeBindingSource.DataSource = projectContext.StandAges.Where(y => y.StandsId == CurrentStand.StandsId).OrderBy(y => y.AgeCode).ToList();
        }

        private void AddStandAgeRecord()
        {
            StandAge newRec = standAgeBindingSource.Current as StandAge;
            if (newRec != null)
            {
                StandAge rec = new StandAge();
                rec.StandsId = CurrentStand.StandsId;
                rec.Age = newRec.Age;
                rec.AgeCode = newRec.AgeCode;
                projectContext.StandAges.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteStandAgeRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandAge rec = standAgeBindingSource.Current as StandAge;
            projectContext.StandAges.Remove(rec);
            projectContext.SaveChanges();
            LoadStandAgeDataSource();
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView3.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandPermPlotRecord();
                    }
                    break;
            }
        }

        private void LoadStandPermPlotDataSource()
        {
            if (standPermPlotBindingSource.Count > 0)
                standPermPlotBindingSource.Clear();
            standPermPlotBindingSource.DataSource = projectContext.StandPermPlots.Where(y => y.StandsId == CurrentStand.StandsId).OrderBy(y => y.PermPlotOccasion).ToList();
        }

        private void AddStandPermPlotRecord()
        {
            StandPermPlot newRec = standPermPlotBindingSource.Current as StandPermPlot;
            if (newRec != null)
            {
                StandPermPlot rec = new StandPermPlot();
                rec.StandsId = CurrentStand.StandsId;
                rec.PermPlotDate = newRec.PermPlotDate; // DateTime.Now;
                rec.PermPlotInterval = newRec.PermPlotInterval;
                rec.PermPlotOccasion = newRec.PermPlotOccasion; // (short)(gridView3.DataRowCount + 1);
                projectContext.StandPermPlots.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteStandPermPlotRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandPermPlot rec = standPermPlotBindingSource.Current as StandPermPlot;
            projectContext.StandPermPlots.Remove(rec);
            projectContext.SaveChanges();
            LoadStandPermPlotDataSource();
        }

        private void gridView3_RowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddStandPermPlotRecord();
            else
                projectContext.SaveChanges();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            // Hauling
            switch (e.Button.ButtonType.ToString())
            {
                case "Remove":
                    if (gridView1.SelectedRowsCount > 0)
                    {
                        e.Handled = true;
                        DeleteStandHaulingRecord();
                    }
                    break;
                case "Append":
                    if (!projectContext.Trees.Any(s => s.StandsId == CurrentStand.StandsId))
                    {
                        XtraMessageBox.Show("Trees must be entered before adding Stand Hauling information.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void gridView1_RowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.RowHandle == GridControl.NewItemRowHandle)
                AddStandHaulingRecord();
            else
                projectContext.SaveChanges();
        }

        private void AddStandHaulingRecord()
        {
            StandHauling newRec = standHaulingBindingSource.Current as StandHauling;
            if (newRec != null)
            {
                StandHauling rec = new StandHauling();
                rec.StandsId = CurrentStand.StandsId;
                rec.AvgLoadCcf = newRec.AvgLoadCcf;
                rec.AvgLoadMbf = newRec.AvgLoadMbf;
                rec.AvgLoadSize = newRec.AvgLoadSize;
                rec.CalcCostDollarPerMbf = newRec.CalcCostDollarPerMbf;
                rec.CostPerHour = newRec.CostPerHour;
                rec.Destination = newRec.Destination;
                rec.Hours = newRec.Hours;
                rec.LoadUnloadHours = newRec.LoadUnloadHours;
                rec.Minutes = newRec.Minutes;
                rec.RoundTripMiles = newRec.RoundTripMiles;
                rec.TableName = "GENERAL";
                projectContext.StandHaulings.Add(rec);
                projectContext.SaveChanges();
            }
        }

        private void DeleteStandHaulingRecord()
        {
            if (XtraMessageBox.Show("Delete the Current Row?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            StandHauling rec = standHaulingBindingSource.Current as StandHauling;
            projectContext.StandHaulings.Remove(rec);
            projectContext.SaveChanges();
            LoadStandHaulingSource();
        }

        private void LoadStandHaulingSource()
        {
            if (standHaulingBindingSource.Count > 0)
                standHaulingBindingSource.Clear();
            List<StandHauling> hauling = projectContext.StandHaulings.OrderBy(h => h.Destination).Where(h => h.TableName == "GENERAL" && h.StandsId == CurrentStand.StandsId).ToList();
            standHaulingBindingSource.DataSource = hauling;
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            switch (gridView1.FocusedColumn.FieldName)
            {
                case "AvgLoadSize":
                    float avgLoadSize = Utils.ConvertToFloat(e.Value.ToString());
                    Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentStand.SpeciesTableName, CurrentStand.MajorSpecies);
                    float avgLoadCcf = (float)Math.Round(avgLoadSize * 2000 / (float)spcRec.Lbs, 1);
                    float avgLoadMbf = (float)Math.Round(avgLoadCcf / (float)2.155, 1);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadCcf"], avgLoadCcf);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"], avgLoadMbf);

                    float calcCostMbf0 = 0;
                    float costPerHour0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf0 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf0 = costPerHour0 * ((minutes0 / 60) + hours0 + loadUnloadHours0) / avgLoadMbf0;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf0);
                    break;
                case "CostPerHour":
                    float calcCostMbf1 = 0;
                    float costPerHour1 = Utils.ConvertToFloat(e.Value.ToString());
                    float minutes1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf1 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf1 = costPerHour1 * ((minutes1 / 60) + hours1 + loadUnloadHours1) / avgLoadMbf1;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf1);
                    break;
                case "RoundTripMiles":
                    //float calcCostMbf2 = 0;
                    //float costPerHour2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    //float minutes2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    //float hours2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    //float loadUnloadHours2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    //float avgLoadMbf2 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    //calcCostMbf2 = costPerHour2 * ((minutes2 / 60) + hours2 + loadUnloadHours2) / avgLoadMbf2;
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf2);
                    break;
                case "Hours":
                    float calcCostMbf3 = 0;
                    float costPerHour3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours3 = Utils.ConvertToFloat(e.Value.ToString());
                    float loadUnloadHours3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf3 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf3 = costPerHour3 * ((minutes3 / 60) + hours3 + loadUnloadHours3) / avgLoadMbf3;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf3);
                    break;
                case "Minutes":
                    float calcCostMbf4 = 0;
                    float costPerHour4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes4 = Utils.ConvertToFloat(e.Value.ToString());
                    float hours4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["LoadUnloadHours"]).ToString());
                    float avgLoadMbf4 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf4 = costPerHour4 * ((minutes4 / 60) + hours4 + loadUnloadHours4) / avgLoadMbf4;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf4);
                    break;
                case "LoadUnloadHours":
                    float calcCostMbf5 = 0;
                    float costPerHour5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CostPerHour"]).ToString());
                    float minutes5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Minutes"]).ToString());
                    float hours5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Hours"]).ToString());
                    float loadUnloadHours5 = Utils.ConvertToFloat(e.Value.ToString());
                    float avgLoadMbf5 = Utils.ConvertToFloat(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["AvgLoadMbf"]).ToString());

                    calcCostMbf5 = costPerHour5 * ((minutes5 / 60) + hours5 + loadUnloadHours5) / avgLoadMbf5;
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["CalcCostDollarPerMbf"], calcCostMbf5);
                    break;
            }
        }

        private void gridView1_ValidateRow(object sender, ValidateRowEventArgs e)
        {

        }

        private void barButtonItem89_ItemClick(object sender, ItemClickEventArgs e)
        {
            //// Build Stand Species
            ////gridViewStands.SelectAll();
            //sColl.Clear();
            //sColl = ReportsBLL.BuildStandSelection(gridViewStands);

            //foreach (var standItem in sColl)
            //{
            //    projectContext.StandSpecies.RemoveRange(projectContext.StandSpecies.Where(s => s.StandsId == standItem.StandsId));
            //    projectContext.StandSpeciesTotals.RemoveRange(projectContext.StandSpeciesTotals.Where(s => s.StandsId == standItem.StandsId));
            //}
            //projectContext.SaveChanges();

            //TempBLL.DeleteWrkTreeSegments();
            //ReportsBLL.BuildWrkSegmentsSep(ref projectContext, CurrentProject, sColl, true);
            //BuildStandSpecies(CurrentProject, sColl);
            //AddTotalsForCountInStandSpecies(sColl);
            ////BuildStandSpeciesTotals(ref projectContext, sColl);
        }

        public static void BuildStandSpecies(Project.DAL.Project pProject, List<Stand> pStands)
        {
            //string saveKey = string.Empty;
            //string currentKey = string.Empty;
            //int saveId = -1;
            //long saveStandsSelected = -1;
            //string tSpecies = string.Empty;
            //string tProductName = string.Empty;
            //string tSort = string.Empty;
            //string tStatus = string.Empty;
            //string tTreeType = string.Empty;
            //float tTreesAcre = 0, tLogsAcre = 0;
            //float tTotalNetBdFt = 0;
            //float tDia = 0;
            //float tLen = 0;
            //float tLogs = 0;
            //float tLogCount = 0;
            //float tCountTrees = 0;
            //float tMeasuredTrees = 0;
            //short tSeq = 1;
            //int? price = -1;
            //float tNetBdFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            //string tTractName = string.Empty, tStandName = string.Empty;

            //using (TempDbContext ctx = new TempDbContext())
            //{
            //    foreach (var standItem in pStands)
            //    {
            //        List<WrkTreeSegment> wColl = ctx.WrkTreeSegments.Where(w => w.StandsId == standItem.StandsId).OrderBy(w => w.Species).ThenBy(w => w.ProductName).ToList();
            //        saveKey = string.Empty;
            //        currentKey = string.Empty;
            //        saveId = -1;
            //        saveStandsSelected = -1;
            //        tSpecies = string.Empty;
            //        tProductName = string.Empty;
            //        tSort = string.Empty;
            //        tStatus = string.Empty;
            //        tTreeType = string.Empty;
            //        tTreesAcre = 0;
            //        tLogsAcre = 0;
            //        tTotalNetBdFt = 0;
            //        tDia = 0;
            //        tLen = 0;
            //        tLogs = 0;
            //        tLogCount = 0;
            //        tCountTrees = 0;
            //        tMeasuredTrees = 0;
            //        tSeq = 1;
            //        price = -1;
            //        tNetBdFtVolume = 0;
            //        tNetCuFtVolume = 0;
            //        tTons = 0;
            //        tTractName = string.Empty;
            //        tStandName = string.Empty;
            //        foreach (var treeItem in wColl)
            //        {
            //            if (string.IsNullOrEmpty(tSpecies))
            //            {
            //                tTractName = treeItem.TractName;
            //                tStandName = treeItem.StandName;
            //                tSpecies = treeItem.Species;
            //                tStatus = treeItem.TreeStatus;
            //                tTreeType = treeItem.TreeType;
            //                tSort = treeItem.SortCode;
            //                tProductName = treeItem.ProductName;
            //                saveId = (int)treeItem.StandsId;
            //                saveStandsSelected = (long)treeItem.StandsSelectedId;
            //                //saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
            //                saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
            //            }
            //            //currentKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
            //            currentKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
            //            if (saveKey != currentKey)
            //            {
            //                // write record
            //                StandSpecy rec = new StandSpecy();
            //                //Stand stand = ProjectBLL.GetStand(ref projectContext, saveId);
            //                rec.StandsId = standItem.StandsId;
            //                rec.StandsSelectedId = saveStandsSelected;
            //                rec.TractName = tTractName;
            //                rec.StandName = tStandName;
            //                rec.LbsCcf = Utils.ConvertToDouble(ProjectBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
            //                rec.TotalCunits = (((tNetCuFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 100);
            //                rec.TotalMbf = (((tNetBdFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 1000);
            //                rec.TotalTons = ((rec.TotalCunits * rec.LbsCcf) / 2000); // (int)tTons; // ((float)rec.TotalCunits * (float)rec.LbsCcf) / 2000;
            //                rec.TotalLogs = (tLogCount * (float)standItem.NetGeographicAcres);
            //                rec.LogAveLen = tLen / tLogCount;
            //                rec.LogAvgDia = tDia / tLogCount;
            //                rec.MeasuredTrees = tMeasuredTrees;
            //                rec.CountTrees = tCountTrees;
            //                rec.SpeciesCode = tSpecies;
            //                rec.SortCode = tSort;
            //                price = ProjectBLL.GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec.LogAveLen, rec.LogAvgDia);
            //                if (price != -1 && tSort != "0")
            //                {
            //                    tSeq++;
            //                    rec.Seq = (byte)tSeq;
            //                    rec.SortDescription = tProductName;
            //                    rec.DollarsPerMbf = price;
            //                    if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
            //                        rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
            //                    else
            //                        rec.TotalDollars = 0;
            //                    if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
            //                        rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
            //                    else
            //                        rec.DollarsPerLog = 0;
            //                    if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                        rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
            //                    else
            //                        rec.DollarsPerAcre = 0;
            //                    if (rec.TotalDollars > 0 && rec.TotalTons > 0)
            //                        rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
            //                    else
            //                        rec.DollarsPerTon = 0;
            //                    if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
            //                        rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
            //                    else
            //                        rec.DollarsPerCcf = 0;
            //                }
            //                else
            //                {
            //                    if (tProductName == "COUNT") // was tSort
            //                    {
            //                        rec.Seq = 1;
            //                        rec.SortDescription = tProductName;
            //                        rec.LogAveLen = 0;
            //                        rec.LogAvgDia = 0;
            //                        rec.DollarsPerMbf = 0;
            //                        if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
            //                            rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
            //                        else
            //                            rec.TotalDollars = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
            //                            rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
            //                        else
            //                            rec.DollarsPerLog = 0;
            //                        if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                            rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
            //                        else
            //                            rec.DollarsPerAcre = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalTons > 0)
            //                            rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
            //                        else
            //                            rec.DollarsPerTon = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
            //                            rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
            //                        else
            //                            rec.DollarsPerCcf = 0;
            //                    }
            //                    else
            //                    if (tProductName == "UTILITY") // was tSort
            //                    {
            //                        rec.Seq = 98;
            //                        rec.SortDescription = tProductName;
            //                        rec.LogAveLen = 0;
            //                        rec.LogAvgDia = 0;
            //                        rec.DollarsPerMbf = 0;
            //                        if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
            //                            rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
            //                        else
            //                            rec.TotalDollars = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
            //                            rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
            //                        else
            //                            rec.DollarsPerLog = 0;
            //                        if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                            rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
            //                        else
            //                            rec.DollarsPerAcre = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalTons > 0)
            //                            rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
            //                        else
            //                            rec.DollarsPerTon = 0;
            //                        if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
            //                            rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
            //                        else
            //                            rec.DollarsPerCcf = 0;
            //                    }
            //                    else
            //                    {
            //                        if (tProductName == "CULL")
            //                        {
            //                            rec.Seq = 99;
            //                            rec.SortDescription = "CULL";
            //                            rec.TotalDollars = 0;
            //                            rec.DollarsPerLog = 0;
            //                            rec.DollarsPerAcre = 0;
            //                            rec.DollarsPerTon = 0;
            //                            rec.DollarsPerCcf = 0;
            //                            rec.DollarsPerMbf = 0;
            //                            rec.LogAveLen = 0;
            //                            rec.LogAvgDia = 0;
            //                        }
            //                    }
            //                }

            //                Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
            //                rec.SpeciesDescription = spc.Description;

            //                projectContext.StandSpecies.Add(rec);

            //                // move fields
            //                tSeq = 1;
            //                tLogs = 0;
            //                tCountTrees = 0;
            //                tMeasuredTrees = 0;
            //                tLogCount = 0;
            //                tLen = 0;
            //                tDia = 0;
            //                tNetBdFtVolume = 0;
            //                tNetCuFtVolume = 0;
            //                tTons = 0;
            //                tTractName = treeItem.TractName;
            //                tStandName = treeItem.StandName;
            //                tSpecies = treeItem.Species;
            //                tSort = treeItem.SortCode;
            //                tStatus = treeItem.TreeStatus;
            //                tTreeType = treeItem.TreeType;
            //                tProductName = treeItem.ProductName;
            //                saveId = (int)treeItem.StandsId;
            //                saveStandsSelected = (long)treeItem.StandsSelectedId;
            //                //saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
            //                saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
            //                if (treeItem.TreeType != "C")
            //                {
            //                    tLogCount++;
            //                    tMeasuredTrees++;
            //                    //tLogs = (float)treeItem.TreeCount * (float)standItem.NetGeographicAcres;
            //                    tLen = (float)treeItem.CalcLen; // was treeItem.CalcLen
            //                    tDia = (float)treeItem.CalcTopDia; // was treeItem.CalcTopDia
            //                    //tNetBdFtVolume = (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
            //                    //tNetCuFtVolume = (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
            //                    //tTons = (float)treeItem.TotalTons;
            //                }
            //                else
            //                {
            //                    tLogs = 0;
            //                    tLen = 0;
            //                    tDia = 0;
            //                    tCountTrees++;
            //                }
            //                tNetBdFtVolume = (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
            //                tNetCuFtVolume = (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
            //                tTons = (float)treeItem.TotalTons;
            //                if (rec.SortDescription == "POLES")
            //                    rec.TotalLogs--;
            //            }
            //            else
            //            {
            //                if (treeItem.TreeType != "C")
            //                {
            //                    //tLogs++;
            //                    tLogCount++;
            //                    tMeasuredTrees++;
            //                    Stand stand = ProjectBLL.GetStand(ref projectContext, (int)treeItem.StandsId);
            //                    //tLogs += (float)treeItem.TreeCount * (float)stand.NetGeographicAcres;
            //                    tLen += (float)treeItem.CalcLen; // was treeItem.CalcLen
            //                    tDia += (float)treeItem.CalcTopDia; // was treeItem.CalcTopDia
            //                    //tNetBdFtVolume += (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
            //                    //tNetCuFtVolume += (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
            //                    //tTons = (float)treeItem.TotalTons;
            //                }
            //                else
            //                    tCountTrees++;
            //                tNetBdFtVolume += (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
            //                tNetCuFtVolume += (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
            //                tTons = (float)treeItem.TotalTons;
            //            }
            //        }
            //        if (wColl.Count > 0)
            //        {
            //            StandSpecy rec1 = new StandSpecy();
            //            //Stand stand1 = ProjectBLL.GetStand(ref projectContext, saveId);
            //            rec1.StandsId = standItem.StandsId;
            //            rec1.StandsSelectedId = saveStandsSelected;
            //            rec1.TractName = tTractName;
            //            rec1.StandName = tStandName;
            //            rec1.LbsCcf = Utils.ConvertToDouble(ProjectBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
            //            rec1.TotalCunits = (((tNetCuFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 100);
            //            rec1.TotalMbf = (((tNetBdFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 1000);
            //            rec1.TotalTons = ((rec1.TotalCunits * rec1.LbsCcf) / 2000); // (int)tTons; // ((float)rec.TotalCunits * (float)rec.LbsCcf) / 2000;
            //            rec1.TotalLogs = (tLogCount * (float)standItem.NetGeographicAcres);
            //            rec1.LogAveLen = tLen / tLogCount;
            //            rec1.LogAvgDia = tDia / tLogCount;
            //            rec1.MeasuredTrees = tMeasuredTrees;
            //            rec1.CountTrees = tCountTrees;
            //            rec1.SpeciesCode = tSpecies;
            //            rec1.SortCode = tSort;
            //            //int? price1 = ProjectBLL.GetPriceByCatagory(ProjectDataSource, CurrentProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
            //            int? price1 = ProjectBLL.GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
            //            if (price1 != -1 && tSort != "0")
            //            {
            //                tSeq++;
            //                rec1.Seq = (byte)tSeq;
            //                rec1.SortDescription = tProductName;
            //                rec1.DollarsPerMbf = price;
            //                if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
            //                    rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
            //                else
            //                    rec1.TotalDollars = 0;
            //                if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
            //                    rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
            //                else
            //                    rec1.DollarsPerLog = 0;
            //                if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                    rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
            //                else
            //                    rec1.DollarsPerAcre = 0;
            //                if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
            //                    rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
            //                else
            //                    rec1.DollarsPerTon = 0;
            //                if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
            //                    rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
            //                else
            //                    rec1.DollarsPerCcf = 0;
            //            }
            //            else
            //            {
            //                if (tProductName == "COUNT") // was tSort
            //                {
            //                    rec1.Seq = 1;
            //                    rec1.SortDescription = tProductName;
            //                    rec1.LogAveLen = 0;
            //                    rec1.LogAvgDia = 0;
            //                    rec1.DollarsPerMbf = 0;
            //                    if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
            //                        rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
            //                    else
            //                        rec1.TotalDollars = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
            //                        rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
            //                    else
            //                        rec1.DollarsPerLog = 0;
            //                    if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                        rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
            //                    else
            //                        rec1.DollarsPerAcre = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
            //                        rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
            //                    else
            //                        rec1.DollarsPerTon = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
            //                        rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
            //                    else
            //                        rec1.DollarsPerCcf = 0;
            //                }
            //                else
            //                if (tProductName == "UTILITY") // was tSort
            //                {
            //                    rec1.Seq = 98;
            //                    rec1.SortDescription = tProductName;
            //                    rec1.LogAveLen = 0;
            //                    rec1.LogAvgDia = 0;
            //                    rec1.DollarsPerMbf = 0;
            //                    if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
            //                        rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
            //                    else
            //                        rec1.TotalDollars = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
            //                        rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
            //                    else
            //                        rec1.DollarsPerLog = 0;
            //                    if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
            //                        rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
            //                    else
            //                        rec1.DollarsPerAcre = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
            //                        rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
            //                    else
            //                        rec1.DollarsPerTon = 0;
            //                    if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
            //                        rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
            //                    else
            //                        rec1.DollarsPerCcf = 0;
            //                }
            //                else
            //                {
            //                    if (tProductName == "CULL")
            //                    {
            //                        rec1.Seq = 99;
            //                        rec1.SortDescription = "CULL";
            //                        rec1.TotalDollars = 0;
            //                        rec1.DollarsPerLog = 0;
            //                        rec1.DollarsPerAcre = 0;
            //                        rec1.DollarsPerTon = 0;
            //                        rec1.DollarsPerCcf = 0;
            //                        rec1.DollarsPerMbf = 0;
            //                        rec1.LogAveLen = 0;
            //                        rec1.LogAvgDia = 0;
            //                    }
            //                }
            //            }
            //            Species spc1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
            //            rec1.SpeciesDescription = spc1.Description;
            //            if (rec1.SortDescription == "POLES")
            //                rec1.TotalLogs--;
            //            projectContext.StandSpecies.Add(rec1);
            //        }
            //    }
            //    projectContext.SaveChanges();
            //}
        }

        public static void AddTotalsForCountInStandSpecies(List<Stand> pStands)
        {
            //float dollarsPerLog = 0;
            //float dollarsPerAcre = 0;
            //float dollarsPerTon = 0;
            //float dollarsPerCcf = 0;
            //float dollarsPerMbf = 0;
            //float totalDollars = 0;
            //float totalMbf = 0;
            //float measuredTotalDollars = 0;
            //float measuredTotalMbf = 0;
            //float measuredTotalCcf = 0;
            //float measuredTotalTon = 0;
            //float measuredTotalLogs = 0;
            //string prevKey = string.Empty;
            //string currentKey = string.Empty;
            //long saveId = -1;
            //string saveSpecies = string.Empty;
            //string saveSort = string.Empty;
            //string saveSpeciesCode = string.Empty;
            //string saveSortCode = string.Empty;

            //float speciesTotalsTotalLogs = 0;
            //float speciesTotalsTotalDollars = 0;
            //float speciesTotalsTotalTons = 0;
            //float speciesTotalsTotalCcf = 0;
            //float speciesTotalsTotalMbf = 0;
            //float speciesTotalsAvgLogDia = 0;
            //float speciesTotalsAvgLogLen = 0;
            //float speciesTotalsDollarsLog = 0;
            //float speciesTotalsDollarsAcre = 0;
            //float speciesTotalsDollarsTon = 0;
            //float speciesTotalsDollarsCcf = 0;
            //float speciesTotalsDollarsMbf = 0;

            //foreach (var item in pStands)
            //{
            //    List<StandSpecy> rColl = projectContext.StandSpecies.Where(r => r.StandsId == item.StandsId)
            //        .OrderByDescending(r => r.SpeciesDescription)
            //        .ThenBy(r => r.Seq)
            //        .ThenBy(r => r.SortDescription).ToList();
            //    dollarsPerLog = 0;
            //    dollarsPerAcre = 0;
            //    dollarsPerTon = 0;
            //    dollarsPerCcf = 0;
            //    dollarsPerMbf = 0;
            //    totalDollars = 0;
            //    totalMbf = 0;
            //    measuredTotalMbf = 0;
            //    measuredTotalCcf = 0;
            //    measuredTotalTon = 0;
            //    measuredTotalDollars = 0;
            //    measuredTotalLogs = 0;
            //    prevKey = string.Empty;
            //    currentKey = string.Empty;
            //    saveSpeciesCode = string.Empty;
            //    saveSortCode = string.Empty;

            //    foreach (var dataItem in rColl)
            //    {
            //        if (string.IsNullOrEmpty(prevKey))
            //        {
            //            prevKey = string.Format("{0}{1}", dataItem.StandsId, dataItem.SpeciesDescription);
            //            saveId = (long)dataItem.StandsId;
            //            saveSpecies = dataItem.SpeciesDescription;
            //            saveSpeciesCode = dataItem.SpeciesCode;
            //            saveSortCode = dataItem.SortCode;
            //            //saveSort = dataItem.Sort;
            //        }
            //        currentKey = string.Format("{0}{1}", dataItem.StandsId, dataItem.SpeciesDescription);
            //        if (currentKey != prevKey)
            //        {
            //            dollarsPerMbf = measuredTotalDollars / measuredTotalMbf;
            //            dollarsPerCcf = measuredTotalDollars / measuredTotalCcf;
            //            dollarsPerTon = measuredTotalDollars / measuredTotalTon;
            //            dollarsPerAcre = measuredTotalDollars / (float)item.NetGeographicAcres;
            //            dollarsPerLog = measuredTotalDollars / measuredTotalLogs;
            //            StandSpecy rec = projectContext.StandSpecies.FirstOrDefault(t => t.StandsId == saveId &&
            //                t.SpeciesDescription == saveSpecies &&
            //                t.SortDescription == "COUNT");
            //            if (rec != null)
            //            {
            //                if (ProjectBLL.GetMeasuredTreesForSpecies(ref projectContext, saveSpecies) == 1)
            //                {
            //                    AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == item.StandsId && t.Species == saveSpeciesCode);
            //                    if (acp != null)
            //                    {
            //                        int cntTrees = ProjectBLL.GetCountTreesForSpecies(ref projectContext, saveSpecies);
            //                        rec.TotalMbf = Math.Round((double)((cntTrees * acp.NetBdFtAcre) * item.NetGeographicAcres) / 1000, 0);
            //                        rec.TotalCunits = Math.Round((double)((cntTrees * acp.NetCuFtAcre) * item.NetGeographicAcres) / 100, 0);
            //                        rec.TotalTons = Math.Round((double)((rec.TotalCunits * rec.LbsCcf) / 2000), 0);
            //                    }
            //                }
            //                rec.DollarsPerAcre = Math.Round((double)dollarsPerAcre, 0);
            //                rec.DollarsPerCcf = Math.Round((double)dollarsPerCcf, 0);
            //                rec.DollarsPerLog = Math.Round((double)dollarsPerLog, 0);
            //                rec.DollarsPerMbf = Math.Round((double)dollarsPerMbf, 0);
            //                rec.DollarsPerTon = Math.Round((double)dollarsPerTon, 0);
            //                rec.TotalDollars = Math.Round((double)(rec.DollarsPerMbf * Math.Round((double)rec.TotalMbf, 0)), 0);
            //            }

            //            // move
            //            measuredTotalMbf = 0;
            //            measuredTotalCcf = 0;
            //            measuredTotalTon = 0;
            //            measuredTotalDollars = 0;
            //            measuredTotalLogs = 0;

            //            saveId = (long)dataItem.StandsId;
            //            saveSpecies = dataItem.SpeciesDescription;
            //            saveSpeciesCode = dataItem.SpeciesCode;
            //            saveSortCode = dataItem.SortCode;
            //            prevKey = currentKey;
            //        }
            //        else
            //        {
            //            if (dataItem.SortDescription != "COUNT")
            //            {
            //                measuredTotalMbf += (float)dataItem.TotalMbf;
            //                measuredTotalCcf += (float)dataItem.TotalCunits;
            //                measuredTotalTon += (float)dataItem.TotalTons;
            //                measuredTotalDollars += (float)dataItem.TotalDollars;
            //                measuredTotalLogs += (float)dataItem.TotalLogs;
            //            }
            //        }
            //    }
            //    dollarsPerMbf = measuredTotalDollars / measuredTotalMbf;
            //    dollarsPerCcf = measuredTotalDollars / measuredTotalCcf;
            //    dollarsPerTon = measuredTotalDollars / measuredTotalTon;
            //    dollarsPerAcre = measuredTotalDollars / (float)item.NetGeographicAcres;
            //    dollarsPerLog = measuredTotalDollars / measuredTotalLogs;
            //    StandSpecy rec1 = projectContext.StandSpecies.FirstOrDefault(t => t.StandsId == saveId &&
            //        t.SpeciesDescription == saveSpecies &&
            //        t.SortDescription == "COUNT");
            //    if (rec1 != null)
            //    {
            //        if (ProjectBLL.GetMeasuredTreesForSpecies(ref projectContext, saveSpecies) == 1)
            //        {
            //            AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == item.StandsId && t.Species == saveSpeciesCode);
            //            if (acp != null)
            //            {
            //                int cntTrees = ProjectBLL.GetCountTreesForSpecies(ref projectContext, saveSpecies);
            //                rec1.TotalMbf = Math.Round((double)((cntTrees * acp.NetBdFtAcre) * item.NetGeographicAcres) / 1000, 0);
            //                rec1.TotalCunits = Math.Round((double)((cntTrees * acp.NetCuFtAcre) * item.NetGeographicAcres) / 100, 0);
            //                rec1.TotalTons = Math.Round((double)((rec1.TotalCunits * rec1.LbsCcf) / 2000), 0);
            //            }
            //        }
            //        rec1.DollarsPerAcre = Math.Round((double)dollarsPerAcre, 0);
            //        rec1.DollarsPerCcf = Math.Round((double)dollarsPerCcf, 0);
            //        rec1.DollarsPerLog = Math.Round((double)dollarsPerLog, 0);
            //        rec1.DollarsPerMbf = Math.Round((double)dollarsPerMbf, 0);
            //        rec1.DollarsPerTon = Math.Round((double)dollarsPerTon, 0);
            //        rec1.TotalDollars = Math.Round((double)(rec1.DollarsPerMbf * Math.Round((double)rec1.TotalMbf, 0)), 0);
            //    }
            //}

            //projectContext.SaveChanges();

            ////////////////
            //projectContext.StandSpeciesTotals.RemoveRange(projectContext.StandSpeciesTotals);
            //projectContext.SaveChanges();

            //foreach (var standItem in pStands)
            //{
            //    List<StandSpecy> tColl = projectContext.StandSpecies.Where(r => r.StandsId == standItem.StandsId)
            //        .OrderByDescending(r => r.SpeciesDescription)
            //        .ThenBy(r => r.Seq)
            //        .ThenBy(r => r.SortDescription).ToList();
            //    foreach (var evalItem in tColl)
            //    {
            //        if (evalItem.LogAvgDia > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsAvgLogDia += (float)(evalItem.LogAvgDia * evalItem.TotalLogs);
            //        }
            //        if (evalItem.LogAveLen > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsAvgLogLen += (float)(evalItem.LogAveLen * evalItem.TotalLogs);
            //        }
            //        if (evalItem.DollarsPerLog > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsDollarsLog += (float)(evalItem.DollarsPerLog * evalItem.TotalLogs);
            //        }
            //        if (evalItem.DollarsPerAcre > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsDollarsAcre += (float)(evalItem.DollarsPerAcre * evalItem.TotalLogs);
            //        }
            //        if (evalItem.DollarsPerTon > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsDollarsTon += (float)(evalItem.DollarsPerTon * evalItem.TotalLogs);
            //        }
            //        if (evalItem.DollarsPerCcf > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsDollarsCcf += (float)(evalItem.DollarsPerCcf * evalItem.TotalLogs);
            //        }
            //        if (evalItem.DollarsPerMbf > 0 && evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsDollarsMbf += (float)(evalItem.DollarsPerMbf * evalItem.TotalLogs);
            //        }
            //        if (evalItem.TotalLogs > 0)
            //        {
            //            speciesTotalsTotalLogs += (float)evalItem.TotalLogs;
            //        }
            //        if (evalItem.TotalTons > 0)
            //        {
            //            speciesTotalsTotalTons += (float)evalItem.TotalTons;
            //        }
            //        if (evalItem.TotalCunits > 0)
            //        {
            //            speciesTotalsTotalCcf += (float)evalItem.TotalCunits;
            //        }
            //        if (evalItem.TotalMbf > 0)
            //        {
            //            speciesTotalsTotalMbf += (float)evalItem.TotalMbf;
            //        }
            //        if (evalItem.TotalDollars > 0)
            //        {
            //            speciesTotalsTotalDollars += (float)evalItem.TotalDollars;
            //        }
            //    }
            //    StandSpeciesTotal totRec = new StandSpeciesTotal();
            //    totRec.CountTrees = 0;
            //    totRec.DollarsPerAcre = Math.Round((double)(speciesTotalsTotalDollars / standItem.NetGeographicAcres), 0);
            //    totRec.DollarsPerCcf = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalCcf), 0);
            //    totRec.DollarsPerLog = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalLogs), 0);
            //    totRec.DollarsPerMbf = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalMbf), 0);
            //    totRec.DollarsPerTon = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalTons), 0);
            //    //totRec.LbsCcf = 0;
            //    totRec.LogAveLen = speciesTotalsAvgLogLen;
            //    totRec.LogAvgDia = speciesTotalsAvgLogDia;
            //    totRec.MeasuredTrees = 0;
            //    //totRec.Seq = 0;
            //    //totRec.Sort = string.Empty;
            //    //totRec.SortCode = string.Empty;
            //    //totRec.Species = string.Empty;
            //    totRec.SpeciesCode = string.Empty;
            //    totRec.StandName = standItem.StandName;
            //    totRec.StandsId = standItem.StandsId;
            //    //totRec.StandsSelectedId = standItem.StandsSelectedId;
            //    totRec.TotalCunits = speciesTotalsTotalCcf;
            //    totRec.TotalDollars = speciesTotalsTotalDollars;
            //    totRec.TotalLogs = speciesTotalsTotalLogs;
            //    totRec.TotalMbf = speciesTotalsTotalMbf;
            //    totRec.TotalTons = speciesTotalsTotalTons;
            //    totRec.TractName = standItem.TractName;
            //    projectContext.StandSpeciesTotals.Add(totRec);
            //}

            //projectContext.SaveChanges();
        }

        private void gridControl4_Validating(object sender, CancelEventArgs e)
        {
            int p = 0;
            for (int i = 0; i < gridView4.RowCount; i++)
                p += Convert.ToInt32(gridView4.GetRowCellValue(i, "Percent"));
            if (p != 100)
            {
                XtraMessageBox.Show("Percent must Total 100 %.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
                e.Cancel = false;
        }

        private void Editor_MouseUp(object sender, MouseEventArgs e)
        {
            TextEdit editor = sender as TextEdit;
            if (editor != null)
                editor.SelectAll();
        }

        private void gridViewStands_DoubleClick(object sender, EventArgs e)
        {
            tabPane1.SelectedPage = tabNavigationPagePlotLocation;
            LoadPlots();
        }

        private void advBandedGridViewPlots_DoubleClick(object sender, EventArgs e)
        {
            tabPane1.SelectedPage = tabNavigationPageTreeInput;
            LoadTrees();
        }

        private void advBandedGridViewTrees_DoubleClick(object sender, EventArgs e)
        {
            tabPane1.SelectedPage = tabNavigationPageTreeEdit;
            LoadSegments(-1, null);
        }

        private void gridView3_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            byte i = (byte)(gridView3.DataRowCount + 1);
            gridView3.SetRowCellValue(e.RowHandle, gridView3.Columns["PermPlotOccasion"], i);
            gridView3.FocusedColumn = gridView3.Columns["PermPlotDate"];
        }

        private void gridView3_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            //switch (gridView3.FocusedColumn.FieldName)
            //{
            //    case "PermPlotDate":
            //        if (!string.IsNullOrEmpty(e.Value.ToString()))
            //        {
            //            int occasion = Utils.ConvertToInt(gridView3.GetFocusedRowCellDisplayText("PermPlotOccasion"));
            //            if (occasion == 1)
            //            {
            //                gridView3.SetFocusedRowCellValue("PermPlotInterval", 0);
            //            }
            //            else
            //            {
            //                DateTime prevDate = Convert.ToDateTime(gridView3.GetRowCellValue(gridView3.FocusedRowHandle - 1, gridView4.Columns["PermPlotDate"]));
            //                DateTime currDate = Convert.ToDateTime(e.Value);
            //                int diff = currDate.Year - prevDate.Year;
            //                gridView3.SetFocusedRowCellValue("PermPlotInterval", diff);
            //            }

            //        }
            //        break;
            //}
        }

        private void gridView3_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            int occasion = Utils.ConvertToInt(gridView3.GetRowCellValue(e.RowHandle, "PermPlotOccasion").ToString());
            if (occasion == 1)
            {
                gridView3.SetRowCellValue(e.RowHandle, "PermPlotInterval", 0);
            }
            else
            {
                StandPermPlot rec = projectContext.StandPermPlots.FirstOrDefault(s => s.StandsId == CurrentStand.StandsId && s.PermPlotOccasion == occasion - 1);
                DateTime prevDate = rec.PermPlotDate; // Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle - 1, "PermPlotDate"));
                DateTime currDate = Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle, "PermPlotDate"));
                int diff = currDate.Year - prevDate.Year;
                gridView3.SetRowCellValue(e.RowHandle, "PermPlotInterval", diff);
            }
        }

        private void gridView4_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            switch (e.Column.FieldName)
            {
                case "Description":
                    gridView4.ShowEditor();
                    (gridView4.ActiveEditor as ComboBoxEdit).ShowPopup();
                    break;
            }
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            switch (e.Column.FieldName)
            {
                case "Destination":
                    gridView1.ShowEditor();
                    (gridView1.ActiveEditor as ComboBoxEdit).ShowPopup();
                    break;
            }
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["AvgLoadSize"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["RoundTripMiles"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["CostPerHour"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["Hours"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["Minutes"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["AvgLoadCcf"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["AvgLoadMbf"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["LoadUnloadHours"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["CalcCostDollarPerMbf"], 0);
        }

        private void gridControlTrees_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (advBandedGridViewTrees.FocusedColumn.FieldName == "SpeciesAbbreviation")
                {
                    //if (string.IsNullOrEmpty(advBandedGridViewTrees.GetFocusedRowCellValue("SpeciesAbbreviation").ToString()))
                    //    advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["PlotNumber"];
                    if (string.IsNullOrEmpty(advBandedGridViewTrees.ActiveEditor.Text))
                        advBandedGridViewTrees.FocusedColumn = advBandedGridViewTrees.Columns["PlotNumber"];
                }
            }
        }

        private void cbSpeciesTable_Validating(object sender, CancelEventArgs e)
        {
            calcForMaster = true;
        }

        private void cbSortTable_Validating(object sender, CancelEventArgs e)
        {
            calcForMaster = true;
        }

        private void cbGradeTable_Validating(object sender, CancelEventArgs e)
        {
            calcForMaster = true;
        }

        private void cbPriceTable_Validating(object sender, CancelEventArgs e)
        {
            calcForMaster = true;
        }

        private void cbCostTable_Validating(object sender, CancelEventArgs e)
        {
            calcForMaster = true;
        }

        private void repositoryItemTextEditAge_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.OldValue == null)
                return;
            if (e.NewValue == null)
                return;

            int oldValue = Utils.ConvertToInt(e.OldValue.ToString());
            int newValue = Utils.ConvertToInt(e.NewValue.ToString());
            if (oldValue > 0 && newValue > 0)
            {
                if (oldValue != newValue)
                    calcForMaster = true;
            }
        }

        private void tb_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.OldValue == null)
                return;
            if (e.NewValue == null)
                return;

            int oldValue = Utils.ConvertToInt(e.OldValue.ToString());
            int newValue = Utils.ConvertToInt(e.NewValue.ToString());
            if (oldValue > 0 && newValue > 0)
            {
                if (oldValue != newValue)
                    calcForMaster = true;
            }
        }
    }
}
