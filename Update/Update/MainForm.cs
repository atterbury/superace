﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Project.DAL;
using System.Diagnostics;
using DevExpress.XtraEditors;

namespace Update
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        private string projectFileName = "DEMO.sdf";
        private string dbConnectionString = string.Empty;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            barEditItemProject.EditValue = projectFileName;
            dbConnectionString = string.Format(@"Data Source=C:\ACI\SuperACE\Database\{0}", projectFileName);
        }

        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Update Sorts
            var ctx = new ProjectDbContext(dbConnectionString);

            AddSort(ref ctx, "", "2", "HI EXPORT", 16, 26, 0, 0, 6, 75, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "3", "J 12", 12, 26, 0, 0, 6, 0, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "4", "JP", 8, 34, 0, 0, 5, 0, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "5", "CH 12", 12, 20, 0, 0, 0, 0, "", 0, 3, "China Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "6", "K 8", 8, 26, 0, 0, 0, 0, "", 0, 0, "Korea Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "7", "K 6", 6, 12, 0, 0, 0, 0, "", 0, 0, "Korea Export", 0, "", "NW SORTS", "SS", 0);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 12, 0, 0, 0, 0, "", 0, 0, "Domestic", 0, "", "NW SORTS", "SS", 28);

            AddSort(ref ctx, "", "2", "HI EXPORT", 16, 26, 0, 0, 6, 75, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "3", "J 12", 12, 26, 0, 0, 6, 0, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "4", "JP", 8, 34, 0, 0, 5, 0, "", 0, 3, "Japan Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "5", "CH 12", 12, 20, 0, 0, 0, 0, "", 0, 3, "China Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "6", "K 8", 8, 26, 0, 0, 0, 0, "", 0, 0, "Korea Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "7", "K 6", 6, 12, 0, 0, 0, 0, "", 0, 0, "Korea Export", 0, "", "NW SORTS", "WH", 0);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 12, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "WH", 28);

            AddSort(ref ctx, "", "8", "DOM SAWLOG", 8, 34, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "RC", 28);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 12, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "YC", 28);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 8, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "PP", 28);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 8, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "WP", 28);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 8, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "CW", 28);
            AddSort(ref ctx, "", "8", "DOM SAWLOG", 5, 8, 0, 0, 0, 0, "", 0, 0, "Domestic Sawlog", 0, "", "NW SORTS", "RA", 28);
            ctx.SaveChanges();
            ctx.Dispose();
        }

        private void AddSort(ref ProjectDbContext ctx, string Abrv, string InputCode, string Description, 
            int MinDia, int MinLen, int MaxDia, int MinimumBdFtPerLog, byte MinimumRingsPerInch, byte PercentSurfaceClear, 
            string MaximumKnotSizePerInch, byte KnotsPerFoot, double SlopeOfGrain, string ProductRecoveryMinimum, 
            int WeightInPounds, string VolumeUnits, string TableName, string PrimarySpecies, int MaximumButtDiameter)
        {
            Sort add = new Sort();
            try
            {
                add.Abbreviation = Abrv;
                add.InputCode = InputCode;
                add.Description = Description;
                add.MarketingCategory = string.Empty;
                add.MinimumDiameter = MinDia;
                add.MinimumLength = MinLen;
                add.MaximumDiameter = MaxDia;
                add.MinimumBdftPerLog = MinimumBdFtPerLog;
                add.MinimumRingsPerInch = MinimumRingsPerInch;
                add.PercentSurfaceClear = PercentSurfaceClear;
                add.MaximumKnotSizePerInch = MaximumKnotSizePerInch;
                add.KnotsPerFoot = KnotsPerFoot;
                add.SlopeOfGrain = SlopeOfGrain;
                add.ProductRecoveryMinimum = ProductRecoveryMinimum;
                add.WeightInPounds = WeightInPounds;
                add.VolumeUnits = VolumeUnits;
                add.TableName = TableName;
                add.Species = string.Empty;
                add.PrimarySpecies = PrimarySpecies;
                add.MaximumButtDiameter = MaximumButtDiameter;
                ctx.Sorts.Add(add);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Update Grades
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Project
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Sql Compact|*.sdf", ValidateNames = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    projectFileName = ofd.FileName;
                    barEditItemProject.EditValue = projectFileName;
                    dbConnectionString = string.Format(@"Data Source={0}", projectFileName);
                }
            }
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Init Hauling new fields
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                List<Hauling> colls = context.Haulings.ToList();
                foreach (var item in colls)
                {
                    item.AvgLoadCcf = 0;
                    item.AvgLoadMbf = 0;
                    item.CalcCostDollarPerMbf = 0;
                    item.LoadUnloadHours = 0;
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Populate StandHauling
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                List<Hauling> colls = context.Haulings.ToList();
                List<Stand> stands = context.Stands.ToList();
                foreach (var standItem in stands)
                {
                    foreach (var item in colls)
                    {
                        StandHauling rec = new StandHauling();
                        rec.AvgLoadCcf = item.AvgLoadCcf;
                        rec.AvgLoadMbf = item.AvgLoadMbf;
                        rec.AvgLoadSize = item.AvgLoadSize;
                        rec.CalcCostDollarPerMbf = item.CalcCostDollarPerMbf;
                        rec.CostPerHour = item.CostPerHour;
                        rec.Destination = item.Destination;
                        rec.Hours = item.Hours;
                        rec.LoadUnloadHours = item.LoadUnloadHours;
                        rec.Minutes = item.Minutes;
                        rec.RoundTripMiles = item.RoundTripMiles;
                        rec.StandsId = standItem.StandsId;
                        rec.TableName = item.TableName;
                        context.StandHaulings.Add(rec);
                    }
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Missing Prices
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                List<Stand> stands = context.Stands.ToList();
                foreach (var standItem in stands)
                {
                    var results = (from segs in context.Treesegments
                                  where segs.StandsId == standItem.StandsId
                                  group segs by new { segs.Species, segs.SortCode, segs.CalcLen, segs.CalcTopDia } into myGroup
                                  orderby myGroup.Key.Species, myGroup.Key.SortCode, myGroup.Key.CalcLen, myGroup.Key.CalcTopDia
                                  select new { Species = myGroup.Key.Species, SortCode = myGroup.Key.SortCode, CalcLen = myGroup.Key.CalcLen, CalcTopDia = myGroup.Key.CalcTopDia }).ToList();
                    foreach (var item in results)
                    {
                        if (item.SortCode != "0" && !string.IsNullOrEmpty(item.SortCode))
                        {
                            Price priceRec = ProjectBLL.GetPrice(context, standItem.PriceTableName, item.Species, item.SortCode, item.CalcLen, item.CalcTopDia);
                            if (priceRec == null)
                            {
                                Debug.WriteLine(string.Format("Stand: {0} Table:{1} Species:{2} Sort:{3} Len:{4} Dia:{5}", 
                                    standItem.StandsId, standItem.PriceTableName, item.Species, item.SortCode, item.CalcLen, item.CalcTopDia));
                            }
                        }
                    }
                    
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // populate tables for stands
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                List<Stand> stands = context.Stands.ToList();
                Project.DAL.Project projectRec = context.Projects.First();
                foreach (var standItem in stands)
                {
                    if (string.IsNullOrEmpty(standItem.SpeciesTableName))
                        standItem.SpeciesTableName = projectRec.SpeciesTableName;
                    if (string.IsNullOrEmpty(standItem.SortTableName))
                        standItem.SortTableName = projectRec.SortsTableName;
                    if (string.IsNullOrEmpty(standItem.GradeTableName))
                        standItem.GradeTableName = projectRec.GradesTableName;
                    if (string.IsNullOrEmpty(standItem.CostTableName))
                        standItem.CostTableName = projectRec.CostTableName;
                    if (string.IsNullOrEmpty(standItem.PriceTableName))
                        standItem.PriceTableName = projectRec.PriceTableName;
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Populate Stand Age
            //using (var context = new ProjectDbContext(dbConnectionString))
            //{
            //    context.StandAges.RemoveRange(context.StandAges);
            //    context.SaveChanges();

            //    List<Stand> stands = context.Stands.ToList();
            //    foreach (var standItem in stands)
            //    {
            //        if (standItem.Age1 != 0)
            //        {
            //            StandAge age1 = new StandAge();
            //            age1.StandsId = standItem.StandsId;
            //            age1.AgeCode = 1;
            //            age1.Age = (short)standItem.Age1;
            //            context.StandAges.Add(age1);
            //        }
            //        if (standItem.Age2 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 2;
            //            age.Age = (short)standItem.Age2;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age3 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 3;
            //            age.Age = (short)standItem.Age3;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age4 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 4;
            //            age.Age = (short)standItem.Age4;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age5 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 5;
            //            age.Age = (short)standItem.Age5;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age6 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 6;
            //            age.Age = (short)standItem.Age6;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age7 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 7;
            //            age.Age = (short)standItem.Age7;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age8 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 8;
            //            age.Age = (short)standItem.Age8;
            //            context.StandAges.Add(age);
            //        }
            //        if (standItem.Age9 != 0)
            //        {
            //            StandAge age = new StandAge();
            //            age.StandsId = standItem.StandsId;
            //            age.AgeCode = 9;
            //            age.Age = (short)standItem.Age9;
            //            context.StandAges.Add(age);
            //        }
            //    }
            //    context.SaveChanges();

            //    XtraMessageBox.Show("Completed!");
            //}
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // populate yarding system
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                context.StandYardingSystems.RemoveRange(context.StandYardingSystems);
                context.SaveChanges();

                Project.DAL.Project projectRec = context.Projects.First();

                List<Cost> cColl = context.Costs.Where(t => t.TableName == projectRec.CostTableName && t.CostType == "YARDING").ToList();

                List<Stand> stands = context.Stands.ToList();
                foreach (var standItem in stands)
                {
                    foreach (var costItem in cColl)
                    {
                        StandYardingSystem rec = new StandYardingSystem();
                        rec.StandsId = standItem.StandsId;
                        rec.Code = string.Empty;
                        rec.Description = costItem.CostDescription;
                        rec.CostUnit = costItem.Units;
                        rec.Percent = 0;
                        context.StandYardingSystems.Add(rec);
                    }
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // populate cost with harvest
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                context.Costs.RemoveRange(context.Costs.Where(c => c.CostType == "YARDING"));
                context.SaveChanges();

                Project.DAL.Project projectRec = context.Projects.First();
                List<Harvestsystem> hColl = context.Harvestsystems.Where(t => t.TableName == projectRec.HarvestSystemsTableName).ToList();

                foreach (var harvestItem in hColl)
                {
                    Cost rec = new Cost();
                    rec.TableName = projectRec.CostTableName;
                    rec.CostAmount = 0;
                    rec.CostDate = DateTime.Now;
                    rec.CostDescription = harvestItem.Description.ToUpper();
                    rec.CostType = "YARDING";
                    rec.CostTypeGroup = "YARDING";
                    rec.Percent = 0;
                    rec.ReportPrintOrder = 0;
                    rec.Species = string.Empty;
                    rec.TreatmentAcresPerYear = 0;
                    rec.Units = "MBF";
                    rec.YearsApplied = "ALL";
                    context.Costs.Add(rec);
                }
                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem12_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Purge Stand Hauling Calcs
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                Project.DAL.Project projectRec = context.Projects.First();
                List<Stand> stands = context.Stands.ToList();
                foreach (var standItem in stands)
                {
                    List<StandHauling> hColl = context.StandHaulings.Where(h => h.StandsId == standItem.StandsId).OrderBy(h => h.Destination).ToList();
                    foreach (var haulingItem in hColl)
                    {
                        bool priceFound = context.Prices.Any(p => p.TableName == standItem.PriceTableName && p.Destination == haulingItem.Destination);
                        if (!priceFound)
                        {
                            StandHauling delRec = haulingItem;
                            context.StandHaulings.Remove(delRec);
                        }
                        else
                        {
                            bool delStandHauling = true;
                            List<Price> priceColl = context.Prices.Where(p => p.TableName == standItem.PriceTableName && p.Destination == haulingItem.Destination).ToList();
                            foreach (var priceItem in priceColl)
                            {
                                if (priceItem != null)
                                {
                                    if (priceItem.Species == "*")
                                        delStandHauling = false;
                                    else
                                    {
                                        if (PriceSpeciesFound(priceItem))
                                            delStandHauling = false;
                                    }
                                }
                            }
                            if (delStandHauling)
                            {
                                Debug.WriteLine(string.Format("Delete {0} of StandsId {1}", haulingItem.Destination, standItem.StandsId));
                                StandHauling delRec = haulingItem;
                                context.StandHaulings.Remove(delRec);
                            }
                        }
                    }
                }

                context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private bool PriceSpeciesFound(Price priceRec)
        {
            bool result = false;
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                string[] species = priceRec.Species.Split(' ');
                for (int i = 0; i < species.Length; i++)
                {
                    string specieCode = Convert.ToString(species[i]);
                    bool specieFound = context.Treesegments.Any(p => p.Species == specieCode);
                    if (specieFound)
                        result = true;
                }
            }
            return result;
        }

        private bool PriceSortGradeFound(Price priceRec)
        {
            bool result = false;
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                for (int i = 0; i < priceRec.Sorts.Length; i++)
                {
                    string sortCode = Convert.ToString(priceRec.Sorts[i]);
                    bool sortFound = context.Treesegments.Any(p => p.SortCode == sortCode);
                    if (sortFound)
                    {
                        for (int x = 0; x < priceRec.Grades.Length; x++)
                        {
                            string gradeCode = Convert.ToString(priceRec.Grades[i]);
                            bool gradeFound = context.Treesegments.Any(p => p.GradeCode == gradeCode);
                            if (gradeFound)
                                result = true;
                        }
                    }
                }
            }
            return result;
        }

        private void barButtonItem13_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Calc Stand Hauling
        }

        private void barButtonItem14_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var context = new ProjectDbContext(dbConnectionString))
            {
                Project.DAL.Project projectRec = context.Projects.First();
                List<Stand> stands = context.Stands.ToList();
                foreach (var standItem in stands)
                {
                    List<Price> pColl = context.Prices.Where(p => p.TableName == standItem.PriceTableName).ToList();
                    foreach (var priceItem in pColl)
                    {
                        bool haulingFound = context.Haulings.Any(h => h.TableName == standItem.CostTableName && h.Destination == priceItem.Destination);
                        if (!haulingFound)
                        {
                            Debug.WriteLine(priceItem.Destination);
                        }
                    }
                }

                //context.SaveChanges();

                XtraMessageBox.Show("Completed!");
            }
        }

        private void barButtonItem15_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Build Stand Species
            ProjectDbContext context = new ProjectDbContext(dbConnectionString);

            Project.DAL.Project projectRec = context.Projects.First();
            List<Stand> stands = context.Stands.ToList();
            BuildStandSpecies(ref context, projectRec, stands);

            context.Dispose();

                XtraMessageBox.Show("Completed!");
            }

        private void BuildStandSpecies(ref ProjectDbContext context, Project.DAL.Project projectRec, List<Stand> stands)
        {
            string saveKey = string.Empty;
            string currentKey = string.Empty;
            int saveId = -1;
            long saveStandsSelected = -1;
            string tSpecies = string.Empty;
            string tProductName = string.Empty;
            string tSort = string.Empty;
            string tGrade = string.Empty;
            string tStatus = string.Empty;
            string tTreeType = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tLogCount = 0;
            float tCountTrees = 0;
            float tMeasuredTrees = 0;
            short tSeq = 1;
            int? price = -1;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            string tTractName = string.Empty, tStandName = string.Empty;

            foreach (var standItem in stands)
            {
                List<Treesegment> wColl = context.Treesegments.Where(w => w.StandsId == standItem.StandsId).OrderBy(w => w.Species).ThenBy(w => w.SortCode).ThenBy(w => w.GradeCode).ToList();
                saveKey = string.Empty;
                currentKey = string.Empty;
                saveId = -1;
                tSpecies = string.Empty;
                tGrade = string.Empty;
                tProductName = string.Empty;
                tSort = string.Empty;
                tStatus = string.Empty;
                tTreeType = string.Empty;
                tTreesAcre = 0;
                tLogsAcre = 0;
                tTotalNetBdFt = 0;
                tDia = 0;
                tLen = 0;
                tLogs = 0;
                tLogCount = 0;
                tCountTrees = 0;
                tMeasuredTrees = 0;
                tSeq = 1;
                price = -1;
                tNetBdFtVolume = 0;
                tNetCuFtVolume = 0;
                tTons = 0;
                tTractName = string.Empty;
                tStandName = string.Empty;
                foreach (var treeItem in wColl)
                {
                    if (string.IsNullOrEmpty(tSpecies))
                    {
                        //tTractName = treeItem.TractName;
                        //tStandName = treeItem.StandName;
                        tSpecies = treeItem.Species;
                        tStatus = treeItem.TreeStatusDisplayCode;
                        tTreeType = treeItem.TreeType;
                        tSort = treeItem.SortCode;
                        //tProductName = treeItem.ProductName;
                        saveId = (int)treeItem.StandsId;
                        //saveStandsSelected = (long)treeItem.StandsSelectedId;
                        saveKey = treeItem.Species.Trim() + treeItem.TreeStatusDisplayCode.Trim() + treeItem.SortCode.Trim() + treeItem.GradeCode.Trim();
                    }
                    //currentKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
                    currentKey = treeItem.Species.Trim() + treeItem.TreeStatusDisplayCode.Trim() + treeItem.SortCode.Trim() + treeItem.GradeCode.Trim();
                    if (saveKey != currentKey)
                    {
                        // write record
                        StandSpecy rec = new StandSpecy();
                        //Stand stand = ProjectBLL.GetStand(ref projectContext, saveId);
                        rec.StandsId = standItem.StandsId;
                        rec.StandsSelectedId = saveStandsSelected;
                        rec.TractName = tTractName;
                        rec.Stand = tStandName;
                        rec.LbsCcf = Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
                        rec.TotalCunits = (((tNetCuFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 100);
                        rec.TotalMbf = (((tNetBdFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 1000);
                        rec.TotalTons = ((rec.TotalCunits * rec.LbsCcf) / 2000); // (int)tTons; // ((float)rec.TotalCunits * (float)rec.LbsCcf) / 2000;
                        rec.TotalLogs = (tLogCount * (float)standItem.NetGeographicAcres);
                        rec.LogAveLen = tLen / tLogCount;
                        rec.LogAvgDia = tDia / tLogCount;
                        rec.MeasuredTrees = tMeasuredTrees;
                        rec.CountTrees = tCountTrees;
                        rec.SpeciesCode = tSpecies;
                        rec..SortCode = tSort;
                        price = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec.LogAveLen, rec.LogAvgDia);
                        if (price != -1 && tSort != "0")
                        {
                            tSeq++;
                            rec.Seq = (byte)tSeq;
                            rec.Sort = tProductName;
                            rec.DollarsPerMbf = price;
                            if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                            else
                                rec.TotalDollars = 0;
                            if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                            else
                                rec.DollarsPerLog = 0;
                            if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                            else
                                rec.DollarsPerAcre = 0;
                            if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                            else
                                rec.DollarsPerTon = 0;
                            if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                            else
                                rec.DollarsPerCcf = 0;
                        }
                        else
                        {
                            if (tProductName == "COUNT") // was tSort
                            {
                                rec.Seq = 1;
                                rec.Sort = tProductName;
                                rec.LogAveLen = 0;
                                rec.LogAvgDia = 0;
                                rec.DollarsPerMbf = 0;
                                if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                    rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                                else
                                    rec.TotalDollars = 0;
                                if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                    rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                                else
                                    rec.DollarsPerLog = 0;
                                if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                    rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                                else
                                    rec.DollarsPerAcre = 0;
                                if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                    rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                                else
                                    rec.DollarsPerTon = 0;
                                if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                    rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                                else
                                    rec.DollarsPerCcf = 0;
                            }
                            else
                            if (tProductName == "UTILITY") // was tSort
                            {
                                rec.Seq = 98;
                                rec.Sort = tProductName;
                                rec.LogAveLen = 0;
                                rec.LogAvgDia = 0;
                                rec.DollarsPerMbf = 0;
                                if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                    rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                                else
                                    rec.TotalDollars = 0;
                                if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                    rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                                else
                                    rec.DollarsPerLog = 0;
                                if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                    rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                                else
                                    rec.DollarsPerAcre = 0;
                                if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                    rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                                else
                                    rec.DollarsPerTon = 0;
                                if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                    rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                                else
                                    rec.DollarsPerCcf = 0;
                            }
                            else
                            {
                                if (tProductName == "CULL")
                                {
                                    rec.Seq = 99;
                                    rec.Sort = "CULL";
                                    rec.TotalDollars = 0;
                                    rec.DollarsPerLog = 0;
                                    rec.DollarsPerAcre = 0;
                                    rec.DollarsPerTon = 0;
                                    rec.DollarsPerCcf = 0;
                                    rec.DollarsPerMbf = 0;
                                    rec.LogAveLen = 0;
                                    rec.LogAvgDia = 0;
                                }
                            }
                        }

                        Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
                        rec.Species = spc.Description;

                        ctx.RptSpeciesSortTimberEvaluations.Add(rec);

                        // move fields
                        tSeq = 1;
                        tLogs = 0;
                        tCountTrees = 0;
                        tMeasuredTrees = 0;
                        tLogCount = 0;
                        tLen = 0;
                        tDia = 0;
                        tNetBdFtVolume = 0;
                        tNetCuFtVolume = 0;
                        tTons = 0;
                        tTractName = treeItem.TractName;
                        tStandName = treeItem.StandName;
                        tSpecies = treeItem.Species;
                        tSort = treeItem.SortCode;
                        tStatus = treeItem.TreeStatus;
                        tTreeType = treeItem.TreeType;
                        tProductName = treeItem.ProductName;
                        saveId = (int)treeItem.StandsId;
                        saveStandsSelected = (long)treeItem.StandsSelectedId;
                        //saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
                        saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
                        if (treeItem.TreeType != "C")
                        {
                            tLogCount++;
                            tMeasuredTrees++;
                            //tLogs = (float)treeItem.TreeCount * (float)standItem.NetGeographicAcres;
                            tLen = (float)treeItem.CalcLen; // was treeItem.CalcLen
                            tDia = (float)treeItem.CalcTopDia; // was treeItem.CalcTopDia
                                                               //tNetBdFtVolume = (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
                                                               //tNetCuFtVolume = (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
                                                               //tTons = (float)treeItem.TotalTons;
                        }
                        else
                        {
                            tLogs = 0;
                            tLen = 0;
                            tDia = 0;
                            tCountTrees++;
                        }
                        tNetBdFtVolume = (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
                        tNetCuFtVolume = (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
                        tTons = (float)treeItem.TotalTons;
                        if (rec.Sort == "POLES")
                            rec.TotalLogs--;
                    }
                    else
                    {
                        if (treeItem.TreeType != "C")
                        {
                            //tLogs++;
                            tLogCount++;
                            tMeasuredTrees++;
                            Stand stand = ProjectBLL.GetStand(ref projectContext, (int)treeItem.StandsId);
                            //tLogs += (float)treeItem.TreeCount * (float)stand.NetGeographicAcres;
                            tLen += (float)treeItem.CalcLen; // was treeItem.CalcLen
                            tDia += (float)treeItem.CalcTopDia; // was treeItem.CalcTopDia
                                                                //tNetBdFtVolume += (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
                                                                //tNetCuFtVolume += (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
                                                                //tTons = (float)treeItem.TotalTons;
                        }
                        else
                            tCountTrees++;
                        tNetBdFtVolume += (float)treeItem.ScribnerNetVolume * (float)treeItem.TreesPerAcre;
                        tNetCuFtVolume += (float)treeItem.CubicNetVolume * (float)treeItem.TreesPerAcre;
                        tTons = (float)treeItem.TotalTons;
                    }
                }
                if (wColl.Count > 0)
                {
                    RptSpeciesSortTimberEvaluation rec1 = new RptSpeciesSortTimberEvaluation();
                    //Stand stand1 = ProjectBLL.GetStand(ref projectContext, saveId);
                    rec1.StandsId = standItem.StandsId;
                    rec1.StandsSelectedId = saveStandsSelected;
                    rec1.TractName = tTractName;
                    rec1.StandName = tStandName;
                    rec1.LbsCcf = Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
                    rec1.TotalCunits = (((tNetCuFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 100);
                    rec1.TotalMbf = (((tNetBdFtVolume / (float)standItem.Plots) * standItem.NetGeographicAcres) / 1000);
                    rec1.TotalTons = ((rec1.TotalCunits * rec1.LbsCcf) / 2000); // (int)tTons; // ((float)rec.TotalCunits * (float)rec.LbsCcf) / 2000;
                    rec1.TotalLogs = (tLogCount * (float)standItem.NetGeographicAcres);
                    rec1.LogAveLen = tLen / tLogCount;
                    rec1.LogAvgDia = tDia / tLogCount;
                    rec1.MeasuredTrees = tMeasuredTrees;
                    rec1.CountTrees = tCountTrees;
                    rec1.SpeciesCode = tSpecies;
                    rec1.SortCode = tSort;
                    //int? price1 = ProjectBLL.GetPriceByCatagory(ProjectDataSource, CurrentProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
                    int? price1 = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
                    if (price1 != -1 && tSort != "0")
                    {
                        tSeq++;
                        rec1.Seq = (byte)tSeq;
                        rec1.Sort = tProductName;
                        rec1.DollarsPerMbf = price;
                        if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                            rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                        else
                            rec1.TotalDollars = 0;
                        if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                            rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                        else
                            rec1.DollarsPerLog = 0;
                        if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                            rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                        else
                            rec1.DollarsPerAcre = 0;
                        if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                            rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                        else
                            rec1.DollarsPerTon = 0;
                        if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                            rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                        else
                            rec1.DollarsPerCcf = 0;
                    }
                    else
                    {
                        if (tProductName == "COUNT") // was tSort
                        {
                            rec1.Seq = 1;
                            rec1.Sort = tProductName;
                            rec1.LogAveLen = 0;
                            rec1.LogAvgDia = 0;
                            rec1.DollarsPerMbf = 0;
                            if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                                rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                            else
                                rec1.TotalDollars = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                                rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                            else
                                rec1.DollarsPerLog = 0;
                            if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                            else
                                rec1.DollarsPerAcre = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                                rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                            else
                                rec1.DollarsPerTon = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                                rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                            else
                                rec1.DollarsPerCcf = 0;
                        }
                        else
                        if (tProductName == "UTILITY") // was tSort
                        {
                            rec1.Seq = 98;
                            rec1.Sort = tProductName;
                            rec1.LogAveLen = 0;
                            rec1.LogAvgDia = 0;
                            rec1.DollarsPerMbf = 0;
                            if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                                rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                            else
                                rec1.TotalDollars = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                                rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                            else
                                rec1.DollarsPerLog = 0;
                            if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                            else
                                rec1.DollarsPerAcre = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                                rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                            else
                                rec1.DollarsPerTon = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                                rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                            else
                                rec1.DollarsPerCcf = 0;
                        }
                        else
                        {
                            if (tProductName == "CULL")
                            {
                                rec1.Seq = 99;
                                rec1.Sort = "CULL";
                                rec1.TotalDollars = 0;
                                rec1.DollarsPerLog = 0;
                                rec1.DollarsPerAcre = 0;
                                rec1.DollarsPerTon = 0;
                                rec1.DollarsPerCcf = 0;
                                rec1.DollarsPerMbf = 0;
                                rec1.LogAveLen = 0;
                                rec1.LogAvgDia = 0;
                            }
                        }
                    }
                    Species spc1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
                    rec1.Species = spc1.Description;
                    if (rec1.Sort == "POLES")
                        rec1.TotalLogs--;
                    ctx.RptSpeciesSortTimberEvaluations.Add(rec1);
                }
            }
        }
    }
}
