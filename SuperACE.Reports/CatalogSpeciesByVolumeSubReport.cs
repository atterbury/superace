﻿using System;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using SuperACEUtils;
using Temp.DAL;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace SuperACE.Reports
{
    public partial class CatalogSpeciesByVolumeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        private bool isFirstLine = true;

        private float ageTotalMbf = 0;
        private float ageTotalCcf = 0;
        private float ageTotalHeight = 0;
        private float ageFF = 0;
        private float ageTreesPerAcre = 0;
        private float ageMeasTreesPerAcre = 0;
        private float ageBasalAreaPerAcre = 0;
        private float ageLogsPerAcre = 0;
        private float ageNetBdFtAcre = 0;
        private float ageNetCuFtAcre = 0;

        private float standTotalHeight = 0;
        private float standFF = 0;
        private float standTreesPerAcre = 0;
        private float standMeasTreesPerAcre = 0;
        private float standBasalAreaPerAcre = 0;
        private float standLogsPerAcre = 0;
        private float standNetBdFtAcre = 0;
        private float standNetCuFtAcre = 0;

        public CatalogSpeciesByVolumeSubReport()
        {
            InitializeComponent();
        }

        public List<RptCatalogSpeciesVolume> DetailsData { get; set; } = null;

        private void CatalogSpeciesByVolumeSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            // filter data for this stand (RC: changes for combined report version)
            long id = Convert.ToInt64(this.Parameters["StandsSelectedId"].Value);
            // are we using hard writes out to the database
            if (Utils.Setting_RptPrepHardWrite)
            {
                objectDataSource1.DataSource = TempBLL.GetRptCatalogSpeciesVolume(id);
                return;
            }
            // using in-memory collection; conduct safety check that developer has set the data
            if (DetailsData == null)
            {
                XtraMessageBox.Show(string.Format(
                    "Invalid data sent to report {0}.", this.GetType().Name),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            objectDataSource1.DataSource = DetailsData.Where(s => s.StandsSelectedId == id).ToList();
        }


        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Age
            //xrLabel16.Text = string.Format("{0:0}", ageAvgBdFt / ageTreesPerAcre);
            if (ageLogsPerAcre > 0) {
                xrlAgeAvgBdFtPerLog.Text = string.Format("{0:0}", ageNetBdFtAcre / ageLogsPerAcre);
                xrlAgeAvgCuFtPerLog.Text = string.Format("{0:0}", ageNetCuFtAcre / ageLogsPerAcre);
            } else {
                xrlAgeAvgBdFtPerLog.Text = string.Format("{0:0}", 0);
                xrlAgeAvgCuFtPerLog.Text = string.Format("{0:0}", 0);
            }

            if (ageMeasTreesPerAcre > 0) {
                xrLabel21.Text = string.Format("{0:0}", ageTotalHeight / ageMeasTreesPerAcre);
                xrLabel22.Text = string.Format("{0:0}", ageFF / ageMeasTreesPerAcre);
            } else {
                xrLabel21.Text = string.Format("{0:0}", 0);
                xrLabel22.Text = string.Format("{0:0}", 0);
            }

            xrLabel23.Text = (ageBasalAreaPerAcre > 0 && ageTreesPerAcre > 0) ?
                string.Format("{0:0.0}", Math.Sqrt(ageBasalAreaPerAcre / (ageTreesPerAcre * 0.005454154))) :
                string.Format("{0:0.0}", 0);

        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            // Age
            ageTotalMbf = 0;
            ageTotalCcf = 0;
            ageTotalHeight = 0;
            ageFF = 0;
            ageTreesPerAcre = 0;
            ageMeasTreesPerAcre = 0;
            ageBasalAreaPerAcre = 0;
            ageLogsPerAcre = 0;
            ageNetBdFtAcre = 0;
            ageNetCuFtAcre = 0;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            float bdft = ReportUtils.GetFloatFromColumn(this, "NetBdFtPerAcre");
            float cuft = ReportUtils.GetFloatFromColumn(this, "NetCuFtPerAcre");
            float tpa = ReportUtils.GetFloatFromColumn(this, "TreesPerAcre");
            float meastpa = ReportUtils.GetFloatFromColumn(this, "MeasuredTreesPerAcre");
            float bapa = ReportUtils.GetFloatFromColumn(this, "BasalAreaPerAcre");
            float lpa = ReportUtils.GetFloatFromColumn(this, "LogsPerAcre");
            float ht = ReportUtils.GetFloatFromColumn(this, "TotalHeight");
            float ff = ReportUtils.GetFloatFromColumn(this, "Ff");

            ageNetBdFtAcre += bdft;
            ageNetCuFtAcre += cuft;
            ageTreesPerAcre += tpa;
            ageMeasTreesPerAcre += meastpa;
            ageBasalAreaPerAcre += bapa;
            ageLogsPerAcre += lpa;
            ageTotalHeight += ht * meastpa;
            ageFF += ff * meastpa;

            standNetBdFtAcre += bdft;
            standNetCuFtAcre += cuft;
            standTreesPerAcre += tpa;
            standMeasTreesPerAcre += meastpa;
            standBasalAreaPerAcre += bapa;
            standLogsPerAcre += lpa;
            standTotalHeight += ht * meastpa;
            standFF += ff * meastpa;
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Stand
            xrLabel33.Text = string.Format("{0:0}", (standMeasTreesPerAcre > 0) ? standTotalHeight / standMeasTreesPerAcre : 0);
            xrLabel34.Text = string.Format("{0:0}", (standMeasTreesPerAcre > 0) ? standFF / standMeasTreesPerAcre : 0);
            xrlStandAvgBdFtPerLog.Text = string.Format("{0:0}", (standLogsPerAcre > 0) ? standNetBdFtAcre / standLogsPerAcre : 0);
            xrlStandAvgCuFtPerLog.Text = string.Format("{0:0}", (standLogsPerAcre > 0) ? standNetCuFtAcre / standLogsPerAcre : 0);
            xrLabel35.Text = string.Format("{0:0.0}", Math.Sqrt(standBasalAreaPerAcre / (standTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            // Stand
            standTotalHeight = 0;
            standFF = 0;
            standTreesPerAcre = 0;
            standBasalAreaPerAcre = 0;
            standMeasTreesPerAcre = 0;
            standLogsPerAcre = 0;
            standNetBdFtAcre = 0;
            standNetCuFtAcre = 0;
        }

        private void xrLabel96_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // stand
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel95_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Township
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel94_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Range
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Section
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Acres
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Source
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Date
            if (!isFirstLine)
                ((XRLabel)sender).Text = string.Empty;
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            isFirstLine = false;            
        }

        private void GroupFooter2_AfterPrint_1(object sender, EventArgs e)
        {
            // Stand
            isFirstLine = true;
        }

        private void GroupHeader2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = Convert.ToString(GetCurrentColumnValue("State"));
            string county = Convert.ToString(GetCurrentColumnValue("County"));
            if (!string.IsNullOrEmpty(state))
                xrLabel77.Text = state;
            else
                xrLabel77.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel79.Text = state;
            else
                xrLabel79.Text = "Blank";
        }
    }
}
