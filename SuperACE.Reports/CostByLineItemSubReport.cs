﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;
using SuperACEUtils;

namespace SuperACE.Reports
{
    public partial class CostByLineItemSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        float perLog = 0;
        float perAcre = 0;
        float perTon = 0;
        float perCcf = 0;
        float perMbf = 0;
        float totalDollars = 0;

        public CostByLineItemSubReport()
        {
            InitializeComponent();
        }

        private void CostByLineItemSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptCostByLineItem();
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            RptSpeciesSortTimberEvalTotal rec = TempBLL.GetRevenueBySpeciesTotals(1);
            if (rec != null)
            {
                if (perLog > 0)
                    xrLabel27.Text = string.Format("{0:n2}", rec.DollarsPerLog - perLog);
                if (perAcre > 0)
                    xrLabel28.Text = string.Format("{0:n2}", rec.DollarsPerAcre - perAcre);
                if (perTon > 0)
                    xrLabel29.Text = string.Format("{0:n2}", rec.DollarsPerTon - perTon);
                if (perCcf > 0)
                    xrLabel32.Text = string.Format("{0:n2}", rec.DollarsPerCcf - perCcf);
                if (perMbf > 0)
                    xrLabel30.Text = string.Format("{0:n2}", rec.DollarsPerMbf - perMbf);
                if (totalDollars > 0)
                    xrLabel31.Text = string.Format("{0:n0}", rec.TotalDollars - totalDollars);
            }

            //xrLabel27.Text = string.Format("{0:0.00}", 0);
            //xrLabel28.Text = string.Format("{0:0.00}", 0);
            //xrLabel29.Text = string.Format("{0:0.00}", 0);
            //xrLabel32.Text = string.Format("{0:0.00}", 0);
            //xrLabel30.Text = string.Format("{0:0.00}", 0);
            //xrLabel31.Text = string.Format("{0:0}", 0);
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("PerLog").ToString()) > 0)
                perLog  += Utils.ConvertToFloat(this.GetCurrentColumnValue("PerLog").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("PerAcre").ToString()) > 0)
                perAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("PerAcre").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("PerTon").ToString()) > 0)
                perTon += Utils.ConvertToFloat(this.GetCurrentColumnValue("PerTon").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("PerCcf").ToString()) > 0)
                perCcf += Utils.ConvertToFloat(this.GetCurrentColumnValue("PerCcf").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("PerMbf").ToString()) > 0)
                perMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("PerMbf").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalDollars").ToString()) > 0)
                totalDollars += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalDollars").ToString());
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {

        }
    }
}
