﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using SuperACEUtils;
using Temp.DAL;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace SuperACE.Reports
{
    public partial class CatalogSpeciesByVolumeReport : DevExpress.XtraReports.UI.XtraReport
    {
        //public bool hasOneStand = true;
        private bool isFirstLine = true;
        private float ageTotalHeight = 0;
        private float ageFF = 0;
        private float ageTreesPerAcre = 0;
        private float ageBasalAreaPerAcre = 0;

        private float standTotalHeight = 0;
        private float standFF = 0;
        private float standTreesPerAcre = 0;
        private float standBasalAreaPerAcre = 0;

        public List<SepStandsSelected> StandsData { get; set; } = null;
        public List<RptCatalogSpeciesVolume> DetailsData { get; set; } = null;

        public CatalogSpeciesByVolumeReport()
        {
            InitializeComponent();

        }

        private void CatalogSpeciesByVolumeReport_DataSourceDemanded(object sender, EventArgs e)
        {
            // are we using hard writes out to the database
            if (Utils.Setting_RptPrepHardWrite)
            {
                objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
                return;
            }
            // using in-memory collection; conduct safety check that developer has set the data
            if (StandsData == null)
            {
                XtraMessageBox.Show(string.Format(
                    "Invalid data sent to report {0}.", this.GetType().Name),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            objectDataSource1.DataSource = StandsData;
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // pass detail data source on to sub-report
            XRSubreport sub = (XRSubreport)sender;
            ((CatalogSpeciesByVolumeSubReport)sub.ReportSource).DetailsData = this.DetailsData;
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel43.Text = string.Format("{0:0}", ageTotalHeight / ageTreesPerAcre);
            //xrLabel44.Text = string.Format("{0:0}", ageFF / ageTreesPerAcre);
            //xrLabel45.Text = string.Format("{0:0.0}", Math.Sqrt(ageBasalAreaPerAcre / (ageTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            ageTotalHeight = 0;
            ageFF = 0;
            ageTreesPerAcre = 0;
            ageBasalAreaPerAcre = 0;
        }

        private void Detail1_AfterPrint(object sender, EventArgs e)
        {
            //isFirstLine = false;
            ////= Sum(Fields.TotalHeight * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sum(Fields.FF * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sqrt(Sum(Fields.BasalAreaPerAcre)/(Sum(Fields.TreesPerAcre) * 0.005454154))
            //ageTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //ageBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    ageTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    ageFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}

            //standTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //standBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    standTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    standFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
        }

        private void GroupFooter3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel57.Text = string.Format("{0:0}", standTotalHeight / standTreesPerAcre);
            //xrLabel58.Text = string.Format("{0:0}", standFF / standTreesPerAcre);
            //xrLabel59.Text = string.Format("{0:0.0}", Math.Sqrt(standBasalAreaPerAcre / (standTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter3_AfterPrint(object sender, EventArgs e)
        {
            standTotalHeight = 0;
            standFF = 0;
            standTreesPerAcre = 0;
            standBasalAreaPerAcre = 0;
            isFirstLine = true;
        }

        private void xrLabel8_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Stand
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Township
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Range
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel25_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Section
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel26_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Acres
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel27_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Source
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void xrLabel28_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Date
            //if (!isFirstLine)
            //    ((XRLabel)sender).Text = string.Empty;
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            bool one = Convert.ToBoolean(this.Parameters["hasOneStand"].Value);
            string state = Convert.ToString(GetCurrentColumnValue("State"));
            string county = Convert.ToString(GetCurrentColumnValue("County"));
            if (!string.IsNullOrEmpty(state))
                xrLabel110.Text = state;
            else
                xrLabel110.Text = "Blank";

            if (!string.IsNullOrEmpty(county))
                xrLabel109.Text = state;
            else
                xrLabel109.Text = "Blank";
            if (!one)
            {
                xrLabel112.Text = "*";
                xrLabel113.Text = "*";
                xrLabel114.Text = "*";
            }
            else
            {
                xrLabel112.Text = Convert.ToString(GetCurrentColumnValue("TractName"));
                xrLabel113.Text = Convert.ToString(GetCurrentColumnValue("StandName"));
                xrLabel114.Text = Convert.ToString(GetCurrentColumnValue("NetGeographicAcres"));
            }
            //xrLabel112.Text = GetCurrentColumnValue("TractName").ToString();
            //xrLabel113.Text = GetCurrentColumnValue("StandName").ToString();
            //xrLabel114.Text = GetCurrentColumnValue("NetGeographicAcres").ToString();
        }

    }
}
