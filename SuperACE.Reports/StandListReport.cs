﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class StandListReport : DevExpress.XtraReports.UI.XtraReport
    {
        float standMbfPerAcre = 0;
        float standNetGeographicAcres = 0;
        float standCcfPerAcre = 0;
        float standTonsPerAcre = 0;
        float standBasalAreaPerAcre = 0;
        float standTreesPerAcre = 0;
        float standSiteIndex = 0;
        float standMajorAge = 0;

        float projectMbfPerAcre = 0;
        float projectNetGeographicAcres = 0;
        float projectCcfPerAcre = 0;
        float projectTonsPerAcre = 0;
        float projectBasalAreaPerAcre = 0;
        float projectTreesPerAcre = 0;
        float projectSiteIndex = 0;
        float projectMajorAge = 0;
        float projectTotalMbf = 0;
        float projectTotalCcf = 0;

        public StandListReport()
        {
            InitializeComponent();

            objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = GetCurrentColumnValue("State").ToString();
            string county = GetCurrentColumnValue("County").ToString();
            if (!string.IsNullOrEmpty(state))
                xrLabel8.Text = state;
            else
                xrLabel8.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel9.Text = state;
            else
                xrLabel9.Text = "Blank";
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            //= Sum(Fields.MbfPerAcre) / Sum(Fields.NetGeographicAcres)
            //= Sum(Fields.CcfPerAcre) / Sum(Fields.NetGeographicAcres)
            //= Sum(Fields.TonsPerAcre * Fields.NetGeographicAcres) / Sum(Fields.NetGeographicAcres)
            //= Sqrt((Sum(Fields.BasalAreaPerAcre*Fields.NetGeographicAcres)/Sum(Fields.NetGeographicAcres))/((Sum(Fields.TreesPerAcre*Fields.NetGeographicAcres)/Sum(Fields.NetGeographicAcres))*0.005454154))
            //= Sum(Fields.BasalAreaPerAcre * Fields.NetGeographicAcres) / Sum(Fields.NetGeographicAcres)
            //= Sum(Fields.TreesPerAcre * Fields.NetGeographicAcres) / Sum(Fields.NetGeographicAcres)
            //= Sum(Fields.SiteIndex * Fields.NetGeographicAcres) / Sum(Fields.NetGeographicAcres)
            //= Sum(Fields.MajorAge * Fields.NetGeographicAcres) / Sum(Fields.NetGeographicAcres)
            //= Fields.Sum(NetGeographicAcres)
            Utils.ConvertToFloat(xrLabel24.Text);
            Utils.ConvertToFloat(xrLabel23.Text);

            standMbfPerAcre += Utils.ConvertToFloat(xrLabel22.Text);
            standCcfPerAcre += Utils.ConvertToFloat(xrLabel21.Text);
            standTonsPerAcre += Utils.ConvertToFloat(xrLabel25.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            standBasalAreaPerAcre += Utils.ConvertToFloat(xrLabel18.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            standTreesPerAcre += Utils.ConvertToFloat(xrLabel19.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            standSiteIndex += Utils.ConvertToFloat(xrLabel28.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            standMajorAge += Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            standNetGeographicAcres += Utils.ConvertToFloat(xrLabel15.Text);

            projectTotalMbf += Utils.ConvertToFloat(xrLabel24.Text);
            projectTotalCcf += Utils.ConvertToFloat(xrLabel23.Text);
            projectMbfPerAcre += Utils.ConvertToFloat(xrLabel22.Text);
            projectCcfPerAcre += Utils.ConvertToFloat(xrLabel21.Text);
            projectTonsPerAcre += Utils.ConvertToFloat(xrLabel25.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            projectBasalAreaPerAcre += Utils.ConvertToFloat(xrLabel18.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            projectTreesPerAcre += Utils.ConvertToFloat(xrLabel19.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            projectSiteIndex += Utils.ConvertToFloat(xrLabel28.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            projectMajorAge += Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel15.Text);
            projectNetGeographicAcres += Utils.ConvertToFloat(xrLabel15.Text);
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel43.Text = string.Format("{0:0.00}", standNetGeographicAcres);
            xrLabel42.Text = string.Format("{0:0}", standMajorAge / standNetGeographicAcres);
            xrLabel41.Text = string.Format("{0:0}", standSiteIndex / standNetGeographicAcres);
            xrLabel40.Text = string.Format("{0:0.00}", standTreesPerAcre / standNetGeographicAcres);
            xrLabel39.Text = string.Format("{0:0.00}", standBasalAreaPerAcre / standNetGeographicAcres);
            xrLabel38.Text = string.Format("{0:0.0}", Math.Sqrt(standBasalAreaPerAcre / (standTreesPerAcre * 0.005454154)));
            xrLabel37.Text = string.Format("{0:0}", standTonsPerAcre / standNetGeographicAcres);
            xrLabel36.Text = string.Format("{0:0.00}", standCcfPerAcre); //  / standNetGeographicAcres
            xrLabel35.Text = string.Format("{0:0.000}", standMbfPerAcre); // / standNetGeographicAcres
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            standMbfPerAcre = 0;
            standNetGeographicAcres = 0;
            standCcfPerAcre = 0;
            standTonsPerAcre = 0;
            standBasalAreaPerAcre = 0;
            standTreesPerAcre = 0;
            standSiteIndex = 0;
            standMajorAge = 0;
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel45.Text = string.Format("{0:0.00}", projectNetGeographicAcres);
            xrLabel52.Text = string.Format("{0:0}", projectMajorAge / projectNetGeographicAcres);
            xrLabel51.Text = string.Format("{0:0}", projectSiteIndex / projectNetGeographicAcres);
            xrLabel50.Text = string.Format("{0:0.00}", projectTreesPerAcre / projectNetGeographicAcres);
            xrLabel49.Text = string.Format("{0:0.00}", projectBasalAreaPerAcre / projectNetGeographicAcres);
            xrLabel48.Text = string.Format("{0:0.0}", Math.Sqrt(projectBasalAreaPerAcre / (projectTreesPerAcre * 0.005454154)));
            xrLabel47.Text = string.Format("{0:0}", projectTonsPerAcre / projectNetGeographicAcres);
            xrLabel46.Text = string.Format("{0:0.00}", projectCcfPerAcre); // / projectNetGeographicAcres
            xrLabel44.Text = string.Format("{0:0.000}", projectMbfPerAcre); // / projectNetGeographicAcres

            xrLabel53.Text = string.Format("{0:0}", projectTotalCcf);
            xrLabel54.Text = string.Format("{0:0}", projectTotalMbf);
        }

    }
}
