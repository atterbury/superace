﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Projects.DAL;
//using System.Windows.Forms;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using Temp.DAL;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;
using System.Data.Entity.Infrastructure;
using System.Windows.Forms;
using Project.DAL;
using System.Data.Entity.Validation;
using DevExpress.XtraRichEdit.Fields;
using SuperACEUtils;
//using Project = Project.DAL.Project;

namespace SuperACE.Reports
{
    public class ReportsBLL
    {
        public static List<string> speciesColl = null;
        public static List<string> plotColl = null;

        public static void DeleteInputTables()
        {
            TempBLL.DeleteWrkStands();
            TempBLL.DeleteWrkTrees();
            TempBLL.DeleteWrkTreeSegments();
            TempBLL.DeleteSepSelectedStand();
            TempBLL.DeleteComSelectedStand();
        }

        public static void DeleteOutputTables()
        {
            
        }

        public static List<Stand> BuildStandSelection(GridView pView)
        {
            List<Stand> tColl = new List<Stand>();
            int[] rowHandles = pView.GetSelectedRows();
            for (int i = 0; i < rowHandles.Length; i++)
			{
                if (rowHandles[i] >= 0)
			        tColl.Add(pView.GetRow(rowHandles[i]) as Stand); 
			}
            return tColl;
        }

        public static void BuildSepStandsSelected(ref ProjectDbContext projectContext, List<Stand> sColl, string pProject, ref float pAcres, ref float pPlots, ref float pTrees)
        {
            pAcres = 0;
            pPlots = 0;
            pTrees = 0;
            using (var ctx = new TempDbContext())
            {
                foreach (Stand item in sColl)
                {
                    //mTotalAcres += Convert.ToSingle(item.NetGeographicAcres;
                    //mPlots += Convert.ToSingle(item.Plots;
                    //mTrees += Convert.ToSingle(item.Trees.Count;

                    SepStandsSelected wt = new SepStandsSelected(); // wColl.A.AddNew();
                    // RC: retrieve DominantFeatuse for each Stand Selected
                    wt.DominantFeature = Dominance.GetDomPrioritySelectorForStand(ref projectContext, item.StandsId);   
                    wt.BasalAreaPerAcre = item.BasalAreaPerAcre;
                    wt.CcfPerAcre = item.CcfPerAcre;
                    wt.CostTable = item.CostTableName;
                    wt.DateOfStandData = item.DateOfStandData;
                    wt.GradeTable = item.GradeTableName;
                    wt.GrownToDate = item.GrownToDate;
                    wt.LogsPerAcre = item.LogsPerAcre;
                    wt.MajorAge = item.MajorAge;
                    wt.Harvest = item.Harvest;
                    wt.LandClass = item.LandClass;
                    wt.MajorSpecies = item.MajorSpecies;
                    wt.MbfPerAcre = item.MbfPerAcre;
                    wt.NetGeographicAcres = item.NetGeographicAcres;
                    pAcres += Convert.ToSingle(item.NetGeographicAcres);
                    wt.Plots = item.Plots;
                    pPlots += Convert.ToSingle(item.Plots);
                    wt.PriceTable = item.PriceTableName;
                    wt.Project = pProject;
                    wt.ProjectsId = item.ProjectsId;
                    wt.QmDbh = item.QmDbh;
                    wt.SiteIndex = item.SiteIndex;
                    wt.SortTable = item.SortTableName;
                    wt.Source = item.Source;
                    wt.SpeciesTable = item.SpeciesTableName;
                    wt.StandType = string.Empty;
                    wt.TonsPerAcre = item.TonsPerAcre;
                    wt.TotalBdFtGross = item.TotalBdFtGross;
                    wt.TotalBdFtNet = item.TotalBdFtNet;
                    wt.TotalCcf = item.TotalCcf;
                    wt.TotalCuFtGross = item.TotalCuFtGross;
                    wt.TotalCuFtMerch = item.TotalCuFtMerch;
                    wt.TotalCuFtNet = item.TotalCuFtNet;
                    wt.TotalMbf = item.TotalMbf;
                    wt.TotalTons = item.TotalTons;
                    wt.Trees = ProjectBLL.GetTreeCount(ref projectContext, item.StandsId); // item.Trees.Count;
                    pTrees += Convert.ToSingle(wt.Trees);
                    wt.TreesPerAcre = item.TreesPerAcre;
                    wt.County = item.County;
                    wt.Section = item.Section;
                    wt.Range = item.Range;
                    wt.StandName = item.StandName;
                    wt.StandsId = item.StandsId;
                    wt.RealStandsId = item.StandsId;
                    wt.State = item.State;
                    wt.Township = item.Township;
                    wt.TractGroup = item.TractGroup;
                    wt.TractName = item.TractName;
                    ctx.SepStandsSelecteds.Add(wt);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildWrkSegmentsSep(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> sColl, bool pLogValue)
        {
            float tpa = 0;
            float ba = 0;
            string rDestination = string.Empty;
            string rProduct = string.Empty;

            using (var tempCtx = new TempDbContext())
            {
                List<SepStandsSelected> tColl = tempCtx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                foreach (SepStandsSelected item in tColl) // Stand item in sColl
                {
                    //mTotalAcres += Convert.ToSingle(item.NetGeographicAcres;
                    //List<Tree> tColl = projectCtx.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == item.StandsId).ToList();
                    //foreach (var treeItem in item.Tree) // was tColl
                    //{
                        List < Treesegment > gColl = projectContext.Treesegments
                            .OrderBy(t => t.PlotNumber)
                            .ThenBy(t => t.TreeNumber)
                            .ThenBy(t => t.SegmentNumber)
                            .Where(t => t.StandsId == item.StandsId)
                            .ToList();
                        foreach (Treesegment segItem in gColl) // was gColl
                        {
                            if (segItem.TreeCount > 0)
                            {
                                WrkTreeSegment wt = new WrkTreeSegment();
                                wt.StandsSelectedId = item.StandsSelectedId;
                                wt.TreesId = segItem.TreesId;
                                wt.StandsId = segItem.StandsId;
                                wt.RealStandsId = segItem.StandsId;
                                wt.TreeSegmentsId = segItem.TreeSegmentsId;
                                wt.TractName = item.TractName;
                                wt.StandName = item.StandName;
                                wt.TreeType = segItem.TreeType; // treeItem.TreeType;
                                wt.TreeNumber = (short)segItem.TreeNumber;
                                wt.SegmentNumber = segItem.SegmentNumber;
                                if (segItem.TreeType == "C")
                                {
                                    wt.Ao = 0;
                                    wt.Bark = 0;
                                }
                                else
                                {
                                    wt.Ao = segItem.Ao; // treeItem.Ao;
                                    wt.Bark = segItem.Bark; // treeItem.Bark;
                                }
                                wt.PlotNumber = segItem.PlotNumber;
                                //if (segItem.SegmentNumber == 1)
                                //{
                                wt.Species = segItem.Species;
                                wt.TreeCount = (byte)segItem.TreeCount;
                                wt.AgeCode = (byte)segItem.Age;
                                wt.BoleHeight = segItem.BoleHeight;
                                if (segItem.TotalHeight != 0)
                                    wt.TotalHeight = segItem.TotalHeight;
                                else
                                    wt.TotalHeight = 0; // segItem.CalcTotalHeight;
                                wt.CalcTotalHtUsedYn = segItem.CalcTotalHtUsedYn;
                                wt.CrownPosition = segItem.CrownPosition;
                                wt.CrownRatio = segItem.CrownRatio;
                                wt.Damage = segItem.Damage;
                                wt.Dbh = segItem.Dbh;
                                wt.FormFactor = (byte)segItem.FormFactor;
                                wt.FormPoint = (byte)segItem.FormPoint;
                                wt.PlotFactor = segItem.PlotFactorInput;
                                wt.Tdf = segItem.Tdf;
                                wt.TreeStatus = segItem.TreeStatusDisplayCode;
                                wt.UserDefined = segItem.UserDefined;
                                wt.Vigor = segItem.Vigor;
                                //}
                                //else
                                //{
                                //    wt.Species = string.Empty;
                                //    wt.TreeCount = 0;
                                //    wt.Ao = 0;
                                //    wt.Bark = 0;
                                //    wt.AgeCode = 0;
                                //    wt.BoleHeight = 0;
                                //    wt.TotalHeight = 0;
                                //    wt.CrownPosition = string.Empty;
                                //    wt.CrownRatio = string.Empty;
                                //    wt.Damage = string.Empty;
                                //    wt.Dbh = 0;
                                //    wt.FormFactor = 0;
                                //    wt.FormPoint = 0;
                                //    wt.PlotFactor = string.Empty;
                                //    wt.Tdf = string.Empty;
                                //    wt.TreeStatus = string.Empty;
                                //    wt.UserDefined = string.Empty;
                                //    wt.Vigor = string.Empty;
                                //}

                                if (segItem.SegmentNumber == 1 || segItem.SegmentNumber == 0)
                                {
                                    ba = Convert.ToSingle(segItem.TreeBasalArea);
                                    tpa = Convert.ToSingle(segItem.TreeTreesPerAcre);
                                }
                                wt.TreeBasalArea = ba; // segItem.TreeBasalArea;
                                wt.TreeTreesPerAcre = tpa; // segItem.TreeTreesPerAcre;
                                wt.TreesPerAcre = tpa;
                                wt.CalcTopDia = segItem.CalcTopDia;
                                wt.CalcLen = segItem.CalcLen;
                                if (segItem.Ccf != null)
                                    wt.TotalCcf = (int)segItem.Ccf;
                                else
                                    wt.TotalCcf = 0;
                                if (segItem.Mbf != null)
                                    wt.TotalMbf = (int)segItem.Mbf;
                                else
                                    wt.TotalMbf = 0;

                                if (pLogValue)
                                {
                                    double lbsCcf = Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, segItem.Species, ""));
                                    wt.TotalTons = (int)(Convert.ToSingle(segItem.Ccf) * Convert.ToSingle(lbsCcf) / 2000);
                                    wt.LogValue = segItem.LogValue;
                                }
                                else
                                {
                                    wt.TotalTons = 0;
                                    wt.LogValue = 0;
                                }
                                if (segItem.ScribnerGrossVolumePerAce != null)
                                    wt.ScribnerGrossVolumePerAce = segItem.ScribnerGrossVolumePerAce;
                                else
                                    wt.ScribnerGrossVolumePerAce = segItem.ScribnerGrossVolume * segItem.TreeTreesPerAcre;
                                if (segItem.ScribnerNetVolumePerAce != null)
                                    wt.ScribnerNetVolumePerAce = segItem.ScribnerNetVolumePerAce;
                                else
                                    wt.ScribnerNetVolumePerAce = segItem.ScribnerNetVolume * segItem.TreeTreesPerAcre;
                                if (segItem.CubicGrossVolumePerAce != null)
                                    wt.CubicGrossVolumePerAce = segItem.CubicGrossVolumePerAce;
                                else
                                    wt.CubicGrossVolumePerAce = segItem.CubicGrossVolume * segItem.TreeTreesPerAcre;
                                if (segItem.CubicNetVolumePerAce != null)
                                    wt.CubicNetVolumePerAce = segItem.CubicNetVolumePerAce;
                                else
                                    wt.CubicNetVolumePerAce = segItem.CubicNetVolume * segItem.TreeTreesPerAcre;
                                wt.CubicGrossVolume = segItem.CubicGrossVolume;
                                wt.CubicNetVolume = segItem.CubicNetVolume;
                                wt.ScribnerGrossVolume = segItem.ScribnerGrossVolume;
                                wt.ScribnerNetVolume = segItem.ScribnerNetVolume;
                                wt.CalcButtDia = segItem.CalcButtDia;
                                wt.CalcAccumulatedLength = segItem.CalcAccumulatedLength;
                                //wt.TreeBasalArea = segItem.TreeBasalArea;
                                //wt.TreeTreesPerAcre = segItem.TreeTreesPerAcre;
                                //wt.TreesPerAcre = tpa;
                                wt.LogsPerAcre = ProjectBLL.GetLogsPerAcreFromTree(ref projectContext, segItem.TreesId); // segItem.Trees.LogsPerAcre;
                                wt.BdFtLd = (byte)segItem.BdFtLd;
                                wt.BdFtDd = (byte)segItem.BdFtDd;
                                wt.CuFtLd = (byte)segItem.CuFtLd;
                                wt.CuFtDd = (byte)segItem.CuFtDd;
                                wt.BdFtPd = segItem.BdFtPd;
                                wt.GradeCode = segItem.GradeCode;
                                wt.Length = segItem.CalcLen.ToString(); //.Length;
                                //wt.TreeSegmentsId = segItem.TreeSegmentsId;
                                wt.SortCode = segItem.SortCode;
                                if (pLogValue) // was  && segItem.SegmentNumber == 1
                                {
                                    Price priceRec = ProjectBLL.GetPrice(projectContext, pProject.PriceTableName, segItem.Species, segItem.SortCode, segItem.CalcLen, segItem.CalcTopDia);
                                    if (priceRec != null)
                                    {
                                        rDestination = priceRec.Destination;
                                        rProduct = priceRec.ProductName;
                                    }
                                    else
                                    {
                                        rDestination = string.Empty;
                                        rProduct = string.Empty;
                                        //Debug.WriteLine(string.Format("GetPrice - Species:{0} Sort:{1} Len:{2:0} Dia:{3:0}", pSpecies, pSort, pLen, pDia));
                                    }
                                    wt.Destination = rDestination;
                                }
                                else
                                {
                                    wt.Destination = string.Empty;
                                }

                            if (segItem.SortCode == "0")
                                wt.ProductName = "CULL";
                            else
                            {
                                if (!string.IsNullOrEmpty(rProduct))
                                    wt.ProductName = rProduct;
                                else
                                    if (segItem.SegmentNumber == 0)
                                        wt.ProductName = "COUNT";
                                else
                                    wt.ProductName = string.Format("{0}-{1:0}-{2:0}", segItem.SortCode, segItem.CalcLen, segItem.CalcTopDia); // was "?"
                            }
                                if (wt.SortCode == "P")
                                    wt.ProductName = "POLES";
                                if (wt.SortCode == "G")
                                    wt.ProductName = "PILINGS";
                                tempCtx.WrkTreeSegments.Add(wt);
                            }
                        } // segment
                    //} // tree
                } // stand

                try
                {
                    tempCtx.SaveChanges();

                    //bWrkSegments = true;
                }
                catch (DbUpdateException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    XtraMessageBox.Show("Error when saving changes to Temp database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // Temp ctx
        }

        public static bool BuildWrkPolePiling(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> sColl, bool pLogValue)
        {
            float tpa = 0;
            float ba = 0;
            string rDestination = string.Empty;
            string rProduct = string.Empty;
            long saveTreesId = -1;
            PolePiling rec = new PolePiling();
            int recsProcessed = 0;
            bool bResult = false;

            bool bCombine = false;

            using (var tempCtx = new TempDbContext())
            {
                List<SepStandsSelected> tColl = tempCtx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                foreach (SepStandsSelected item in tColl) // Stand item in sColl
                {
                    List<Treesegment> gColl = projectContext.Treesegments
                        .OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .ThenBy(t => t.SegmentNumber)
                        .Where(t => t.StandsId == item.StandsId && t.SortCode == "P" || t.SortCode == "G").ToList();
                    if (gColl.Count > 0)
                    { 
                        foreach (Treesegment segItem in gColl) // was gColl
                        {
                            //if (segItem.TreeCount > 0)
                            //{
                                if (saveTreesId == -1)
                                    saveTreesId = (long)segItem.TreesId;
                                if (segItem.BdFtPd == "-")
                                    bCombine = true;
                            if (saveTreesId == segItem.TreesId)
                            {
                                rec.StandsSelectedId = item.StandsSelectedId;
                                rec.StandsId = (long)segItem.StandsId;
                                rec.RealStandsId = (long)segItem.StandsId;
                                rec.TreesId = (long)segItem.TreesId;
                                rec.TreeSegmentsId = segItem.TreeSegmentsId;
                                rec.TractName = item.TractName;
                                rec.StandName = item.StandName;
                                rec.Sort = segItem.SortCode;
                                rec.Species = segItem.Species;
                                if (segItem.SegmentNumber == 1)
                                    rec.TreesPerAcre += Convert.ToSingle(segItem.TreeTreesPerAcre);
                                if (bCombine)
                                {
                                    rec.PerLogLineal += Convert.ToSingle(segItem.CalcLen);
                                    rec.PerLogNetCubic += Convert.ToSingle(segItem.CubicNetVolume);
                                    rec.PerLogNetScribner += Convert.ToSingle(segItem.ScribnerNetVolume);
                                    //rec.PerAcreLineal += rec.PerLogLineal * rec.TreesPerAcre;
                                    rec.PerAcreNetCubic += rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume);
                                    rec.PerAcreNetScribner += rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume);
                                    rec.TotalNetCubic += (rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 100;
                                    rec.TotalNetScribner += (rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 1000;
                                    rec.TotalTons += rec.TotalNetCubic * GetLbsPerCcfFloat(ref projectContext, pProject, rec.Species) / 2000;
                                    //rec.TotalLineal += rec.PerAcreLineal * Convert.ToSingle(item.NetGeographicAcres;
                                    if (segItem.BdFtPd == "-")
                                        rec.ButtDia = Convert.ToSingle(segItem.CalcButtDia);
                                    else
                                    {

                                        rec.TopDia = Convert.ToSingle(segItem.CalcTopDia);
                                    }
                                }
                                else
                                {
                                    rec.PerLogLineal = Convert.ToSingle(segItem.CalcLen);
                                    rec.PerLogNetCubic = Convert.ToSingle(segItem.CubicNetVolume);
                                    rec.PerLogNetScribner = Convert.ToSingle(segItem.ScribnerNetVolume);
                                    //rec.PerAcreLineal = rec.PerLogLineal * rec.TreesPerAcre;
                                    rec.PerAcreNetCubic = rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume);
                                    rec.PerAcreNetScribner = rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume);
                                    rec.TotalNetCubic = (rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 100;
                                    rec.TotalNetScribner = (rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 1000;
                                    rec.TotalTons = rec.TotalNetCubic * GetLbsPerCcfFloat(ref projectContext, pProject, rec.Species) / 2000;
                                    //rec.TotalLineal = rec.PerAcreLineal * Convert.ToSingle(item.NetGeographicAcres;
                                    rec.ButtDia = Convert.ToSingle(segItem.CalcButtDia);
                                    rec.TopDia = Convert.ToSingle(segItem.CalcTopDia);
                                }
                                if (segItem.BdFtPd == "-")
                                    bCombine = true;
                                else
                                {
                                    bCombine = false;
                                }
                            }
                            else
                            {
                                recsProcessed++;
                                //if (recsProcessed == 1)
                                //{
                                WrkPolePiling wt1 = new WrkPolePiling();
                                rec.PerAcreLineal = rec.PerLogLineal * rec.TreesPerAcre;
                                rec.TotalLineal = rec.PerAcreLineal * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots) ;
                                MovePolePiling(ref projectContext, ref wt1, ref rec);
                                tempCtx.WrkPolePilings.Add(wt1);
                                bResult = true;
                                //}

                                rec.Init();
                                saveTreesId = (long)segItem.TreesId;
                                rec.StandsSelectedId = item.StandsSelectedId;
                                rec.StandsId = (long)segItem.StandsId;
                                rec.RealStandsId = (long)segItem.StandsId;
                                rec.TreesId = (long)segItem.TreesId;
                                rec.TreeSegmentsId = segItem.TreeSegmentsId;
                                rec.TractName = item.TractName;
                                rec.StandName = item.StandName;
                                rec.Sort = segItem.SortCode;
                                rec.Species = segItem.Species;
                                if (segItem.SegmentNumber == 1)
                                    rec.TreesPerAcre += Convert.ToSingle(segItem.TreeTreesPerAcre);
                                rec.PerLogLineal = Convert.ToSingle(segItem.CalcLen);
                                rec.PerLogNetCubic = Convert.ToSingle(segItem.CubicNetVolume);
                                rec.PerLogNetScribner = Convert.ToSingle(segItem.ScribnerNetVolume);
                                //rec.PerAcreLineal = rec.PerLogLineal * rec.TreesPerAcre;
                                rec.PerAcreNetCubic = rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume);
                                rec.PerAcreNetScribner = rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume);
                                rec.TotalNetCubic = (rec.TreesPerAcre * Convert.ToSingle(segItem.CubicNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 100;
                                rec.TotalNetScribner = (rec.TreesPerAcre * Convert.ToSingle(segItem.ScribnerNetVolume) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots)) / 1000;
                                rec.TotalTons = rec.TotalNetCubic * GetLbsPerCcfFloat(ref projectContext, pProject, rec.Species) / 2000;
                                //rec.TotalLineal = rec.PerAcreLineal * Convert.ToSingle(item.NetGeographicAcres;
                                rec.ButtDia = Convert.ToSingle(segItem.CalcButtDia);
                                rec.TopDia = Convert.ToSingle(segItem.CalcTopDia);
                                //bCombine = false;
                            }
                            //}
                        } // segment
                        WrkPolePiling wt2 = new WrkPolePiling();
                        rec.PerAcreLineal = rec.PerLogLineal * rec.TreesPerAcre;
                        rec.TotalLineal = rec.PerAcreLineal * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots);
                        MovePolePiling(ref projectContext, ref wt2, ref rec);
                        tempCtx.WrkPolePilings.Add(wt2);
                        bResult = true;
                    }
                } // stand

                if (bResult)
                {
                    try
                    {
                        tempCtx.SaveChanges();

                        //bWrkSegments = true;
                    }
                    catch (DbUpdateException ex)
                    {
                        XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (DbEntityValidationException ex)
                    {
                        // Retrieve the error messages as a list of strings.
                        var errorMessages = ex.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);

                        // Join the list to a single string.
                        var fullErrorMessage = string.Join("; ", errorMessages);

                        // Combine the original exception message with the new one.
                        var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                        XtraMessageBox.Show(exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        // Throw a new DbEntityValidationException with the improved exception message.
                        //throw new DbEntityValidation, Exception(exceptionMessage, ex.EntityValidationErrors);
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            XtraMessageBox.Show(ex.InnerException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                            XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //XtraMessageBox.Show("Error when saving changes to Temp database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            } // Temp ctx
            return bResult;
        }

        private static void MovePolePiling(ref ProjectDbContext projectContext, ref WrkPolePiling wt, ref PolePiling rec)
        {
            wt.StandsSelectedId = rec.StandsSelectedId;
            wt.TreesId = rec.TreesId;
            wt.StandsId = rec.StandsId;
            wt.RealStandsId = rec.StandsId;
            wt.TreeSegmentsId = rec.TreeSegmentsId;
            wt.TractName = rec.TractName;
            wt.StandName = rec.StandName;

            wt.TreeCount = 0;
            wt.TreeStatus = string.Empty;
            wt.TreeType = string.Empty;

            wt.Species = rec.Species;
            wt.SortCode = rec.Sort;
            wt.CalcTopDia = rec.TopDia;
            wt.CalcButtDia = rec.ButtDia; //segItem.CalcButtDia;
            wt.CalcLen = rec.PerLogLineal;

            wt.PerLogCubicNetVolume = rec.PerLogNetCubic; //segItem.CubicNetVolume;
            wt.PerLogScribnerNetVolume = rec.PerLogNetScribner; // segItem.ScribnerNetVolume;
            wt.PerLogLineal = string.Format("{0}", rec.PerLogLineal); // segItem.CalcLen.ToString(); //.Length;

            wt.PerAcreLineal = rec.PerAcreLineal;
            wt.PerAcreNetCuFt = rec.PerAcreNetCubic;
            wt.PerAcreNetBdFt = rec.PerAcreNetScribner;

            wt.TotalLineal = (double)rec.TotalLineal;
            wt.TotalTons = (int)rec.TotalTons;
            wt.TotalMbf = (int)rec.TotalNetScribner;
            wt.TotalCcf = (int)rec.TotalNetCubic;

            wt.LogsPerAcre = ProjectBLL.GetLogsPerAcreFromTree(ref projectContext, (int)rec.TreesId);
            wt.TreesPerAcre = ProjectBLL.GetTreesPerAcreFromTree(ref projectContext, (int) rec.TreesId);
            
            if (wt.SortCode == "P")
                wt.ProductName = "POLES";
            if (wt.SortCode == "G")
                wt.ProductName = "PILINGS";
        }

        public static void BuildRptPlotTreeListVolume(ref ProjectDbContext projectContext)
        {
            float ff = 0;
            try
            {
                using (var ctx = new TempDbContext())
                {
                    List<SepStandsSelected> sColl = ctx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                    foreach (SepStandsSelected item in sColl)
                    {
                        Stand standMaster = projectContext.Stands.FirstOrDefault(t => t.StandsId == item.StandsId);
                        List<Tree> tColl = ProjectBLL.GetTreesForStand(ref projectContext, (int)item.StandsId);
                        foreach (Tree treeItem in tColl)
                        {
                            RptPlotTreeListVolume rec1 = new RptPlotTreeListVolume();
                            // copy over baseline
                            rec1.copyFrom(item);
                            rec1.copyFrom(treeItem);
                            // assign conditionals
                            float pf = 0;
                            Single.TryParse(treeItem.PlotFactorInput, out pf);  // is tree PlotFactorInput a numeric?
                            if (pf == 0)                                        // if not get from stand project table
                                Convert.ToSingle(ProjectBLL.GetPlotFactor(standMaster, treeItem));
                            int preciaion = (pf.ToString().Contains(".")) ? 2 : 0;  // round to either 0 or 2 places of precision
                            rec1.PlotFactor = Math.Round(pf, preciaion);
                            // conditional on either count or measured (specced in tree type)
                            rec1.TreeCount = (byte)((treeItem.TreeType == "C") ? treeItem.TreeCount : 0);
                            if (treeItem.TreeType != "C")
                            {
                                rec1.TreeMeasured = (byte)treeItem.TreeCount;
                                rec1.LogsPerAcre = treeItem.LogsPerAcre;            // only take LogsPerAcre if its measured tree
                            }
                            // computed values
                            float acres = Convert.ToSingle(item.NetGeographicAcres);
                            rec1.TotalCcf = (rec1.NetCcfPerAcre * acres / 100) / item.Plots;
                            rec1.TotalMbf = (rec1.NetMbfPerAcre * acres / 1000) / item.Plots;
                            // add to collection
                            ctx.RptPlotTreeListVolumes.Add(rec1);
                        }
                    }
                    ctx.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static float GetSegmentsInTree(WrkTree treeItem)
        {
            float segments = 0;

            if (!string.IsNullOrEmpty(treeItem.Sort1) && !string.IsNullOrEmpty(treeItem.Grade1))
            {
                if (treeItem.Sort1 != "0" && treeItem.Grade1 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort2) && !string.IsNullOrEmpty(treeItem.Grade2))
            {
                if (treeItem.Sort2 != "0" && treeItem.Grade2 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort3) && !string.IsNullOrEmpty(treeItem.Grade3))
            {
                if (treeItem.Sort3 != "0" && treeItem.Grade3 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort4) && !string.IsNullOrEmpty(treeItem.Grade4))
            {
                if (treeItem.Sort4 != "0" && treeItem.Grade4 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort5) && !string.IsNullOrEmpty(treeItem.Grade5))
            {
                if (treeItem.Sort5 != "0" && treeItem.Grade5 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort6) && !string.IsNullOrEmpty(treeItem.Grade6))
            {
                if (treeItem.Sort6 != "0" && treeItem.Grade6 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort7) && !string.IsNullOrEmpty(treeItem.Grade7))
            {
                if (treeItem.Sort7 != "0" && treeItem.Grade7 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort8) && !string.IsNullOrEmpty(treeItem.Grade8))
            {
                if (treeItem.Sort8 != "0" && treeItem.Grade8 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort9) && !string.IsNullOrEmpty(treeItem.Grade9))
            {
                if (treeItem.Sort9 != "0" && treeItem.Grade9 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort10) && !string.IsNullOrEmpty(treeItem.Grade10))
            {
                if (treeItem.Sort10 != "0" && treeItem.Grade10 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort11) && !string.IsNullOrEmpty(treeItem.Grade11))
            {
                if (treeItem.Sort11 != "0" && treeItem.Grade11 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort12) && !string.IsNullOrEmpty(treeItem.Grade12))
            {
                if (treeItem.Sort12 != "0" && treeItem.Grade12 != "0")
                    segments++;
            }
            return segments;
        }

        public static float GetSegmentsInTree(Tree treeItem)
        {
            float segments = 0;

            if (!string.IsNullOrEmpty(treeItem.Sort1) && !string.IsNullOrEmpty(treeItem.Grade1))
            {
                if (treeItem.Sort1 != "0" && treeItem.Grade1 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort2) && !string.IsNullOrEmpty(treeItem.Grade2))
            {
                if (treeItem.Sort2 != "0" && treeItem.Grade2 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort3) && !string.IsNullOrEmpty(treeItem.Grade3))
            {
                if (treeItem.Sort3 != "0" && treeItem.Grade3 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort4) && !string.IsNullOrEmpty(treeItem.Grade4))
            {
                if (treeItem.Sort4 != "0" && treeItem.Grade4 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort5) && !string.IsNullOrEmpty(treeItem.Grade5))
            {
                if (treeItem.Sort5 != "0" && treeItem.Grade5 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort6) && !string.IsNullOrEmpty(treeItem.Grade6))
            {
                if (treeItem.Sort6 != "0" && treeItem.Grade6 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort7) && !string.IsNullOrEmpty(treeItem.Grade7))
            {
                if (treeItem.Sort7 != "0" && treeItem.Grade7 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort8) && !string.IsNullOrEmpty(treeItem.Grade8))
            {
                if (treeItem.Sort8 != "0" && treeItem.Grade8 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort9) && !string.IsNullOrEmpty(treeItem.Grade9))
            {
                if (treeItem.Sort9 != "0" && treeItem.Grade9 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort10) && !string.IsNullOrEmpty(treeItem.Grade10))
            {
                if (treeItem.Sort10 != "0" && treeItem.Grade10 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort11) && !string.IsNullOrEmpty(treeItem.Grade11))
            {
                if (treeItem.Sort11 != "0" && treeItem.Grade11 != "0")
                    segments++;
            }
            if (!string.IsNullOrEmpty(treeItem.Sort12) && !string.IsNullOrEmpty(treeItem.Grade12))
            {
                if (treeItem.Sort12 != "0" && treeItem.Grade12 != "0")
                    segments++;
            }
            return segments;
        }

        public static void BuildWrkTrees(ref ProjectDbContext projectContext, Project.DAL.Project pProject)
        {
            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> wrkStands = ctx.SepStandsSelecteds.OrderBy(w => w.TractName).ThenBy(w => w.StandName).ToList();
                foreach (var item in wrkStands)
                {
                    List<Tree> tColl = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == item.StandsId).ToList();
                    foreach (Tree treeItem in tColl)
                    {
                        //if (treeItem.TreeCount > 0)
                        //{
                            WrkTree wt = new WrkTree(); // wColl.AddNew();
                            wt.StandsSelectedId = item.StandsSelectedId;
                            wt.StandsId = item.StandsId;
                            wt.RealStandsId = item.RealStandsId;
                            wt.TractName = item.TractName;
                            wt.StandName = item.StandName;
                            wt.Township = item.Township;
                            wt.Range = item.Range;
                            wt.Section = item.Section;

                            wt.PlotId = treeItem.PlotId;
                            wt.TreesId = treeItem.TreesId;
                            wt.Age = (byte)treeItem.Age;
                            wt.AgeCode = (byte)treeItem.AgeCode;
                            wt.Ao = treeItem.Ao;
                            wt.Bark = treeItem.Bark;
                            wt.BasalArea = treeItem.BasalArea;
                            wt.BoleHeight = treeItem.BoleHeight;
                            wt.CrownPositionDisplayCode = treeItem.CrownPositionDisplayCode;
                            wt.CrownRatioDisplayCode = treeItem.CrownRatioDisplayCode;
                            wt.DamageDisplayCode = treeItem.DamageDisplayCode;
                            if (treeItem.Dbh != null)
                                wt.Dbh = treeItem.Dbh;
                            else
                                wt.Dbh = 0;
                            if (treeItem.FormPoint != null)
                                wt.FormPoint = (byte)treeItem.FormPoint;
                            else
                            {
                                wt.FormPoint = 0;
                            }
                            wt.GroupPlot = treeItem.GroupPlot;
                            wt.GroupTree = treeItem.GroupTree;
                            wt.LogsPerAcre = treeItem.LogsPerAcre;
                            wt.PlotFactorInput = treeItem.PlotFactorInput;
                            wt.PfValue = treeItem.PfValue;

                            wt.PlotNumber = treeItem.PlotNumber;
                            if (treeItem.TreeType == "C")
                            {
                                Treesegment seg = ProjectBLL.GetSegmentForCountTree(ref projectContext, treeItem.TreesId);
                                wt.ReportedFormFactor = (byte)seg.FormFactor;
                                wt.TotalHeight = seg.TotalHeight;
                            }
                            else
                            {
                                wt.ReportedFormFactor = (byte)treeItem.ReportedFormFactor;
                                wt.TotalHeight = treeItem.TotalHeight;
                            }
                            wt.SpeciesAbbreviation = treeItem.SpeciesAbbreviation;
                            wt.TreeStatusDisplayCode = treeItem.TreeStatusDisplayCode;

                            wt.TopDiameterFractionCode = treeItem.TopDiameterFractionCode;
                            wt.TotalBdFtGrossVolume = treeItem.TotalBdFtGrossVolume;
                            wt.TotalBdFtNetVolume = treeItem.TotalBdFtNetVolume;
                            wt.TotalCcf = treeItem.TotalCcf;
                            wt.TotalCuFtGrossVolume = treeItem.TotalCuFtGrossVolume;
                            wt.TotalCuFtNetVolume = treeItem.TotalCuFtNetVolume;
                            wt.TotalGrossCubicPerAcre = treeItem.TotalGrossCubicPerAcre;
                            wt.TotalGrossScribnerPerAcre = treeItem.TotalGrossScribnerPerAcre;
                            wt.TotalMbf = treeItem.TotalMbf;
                            wt.TotalNetCubicPerAcre = treeItem.TotalNetCubicPerAcre;
                            wt.TotalNetScribnerPerAcre = treeItem.TotalNetScribnerPerAcre;

                            float weight = (float)ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, treeItem.SpeciesAbbreviation).Lbs;
                            wt.TotalTonsPerAcre = ((wt.TotalCcf * weight) / 2000) / item.NetGeographicAcres;
                            
                            wt.CalcTotalHeight = treeItem.CalcTotalHeight;
                            wt.CalcTotalHtUsedYn = treeItem.CalcTotalHtUsedYn;
                            wt.TreeCount = (byte)treeItem.TreeCount;
                            wt.TreeNumber = treeItem.TreeNumber;
                            wt.TreesPerAcre = treeItem.TreesPerAcre;
                            wt.TreeType = treeItem.TreeType;

                            wt.Sort1 = treeItem.Sort1;
                            wt.Sort2 = treeItem.Sort2;
                            wt.Sort3 = treeItem.Sort3;
                            wt.Sort4 = treeItem.Sort4;
                            wt.Sort5 = treeItem.Sort5;
                            wt.Sort6 = treeItem.Sort6;
                            wt.Sort7 = treeItem.Sort7;
                            wt.Sort8 = treeItem.Sort8;
                            wt.Sort9 = treeItem.Sort9;
                            wt.Sort10 = treeItem.Sort10;
                            wt.Sort11 = treeItem.Sort11;
                            wt.Sort12 = treeItem.Sort12;

                            wt.Grade1 = treeItem.Grade1;
                            wt.Grade2 = treeItem.Grade2;
                            wt.Grade3 = treeItem.Grade3;
                            wt.Grade4 = treeItem.Grade4;
                            wt.Grade5 = treeItem.Grade5;
                            wt.Grade6 = treeItem.Grade6;
                            wt.Grade7 = treeItem.Grade7;
                            wt.Grade8 = treeItem.Grade8;
                            wt.Grade9 = treeItem.Grade9;
                            wt.Grade10 = treeItem.Grade10;
                            wt.Grade11 = treeItem.Grade11;
                            wt.Grade12 = treeItem.Grade12;

                            wt.Length1 = treeItem.Length1;
                            wt.Length2 = treeItem.Length2;
                            wt.Length3 = treeItem.Length3;
                            wt.Length4 = treeItem.Length4;
                            wt.Length5 = treeItem.Length5;
                            wt.Length6 = treeItem.Length6;
                            wt.Length7 = treeItem.Length7;
                            wt.Length8 = treeItem.Length8;
                            wt.Length9 = treeItem.Length9;
                            wt.Length10 = treeItem.Length10;
                            wt.Length11 = treeItem.Length11;
                            wt.Length12 = treeItem.Length12;

                            if (treeItem.BdFtLd1 != null)
                                wt.BdFtLd1 = (byte)treeItem.BdFtLd1;
                            if (treeItem.BdFtLd2 != null)
                                wt.BdFtLd2 = (byte)treeItem.BdFtLd2;
                            if (treeItem.BdFtLd3 != null)
                                wt.BdFtLd3 = (byte)treeItem.BdFtLd3;
                            if (treeItem.BdFtLd4 != null)
                                wt.BdFtLd4 = (byte)treeItem.BdFtLd4;
                            if (treeItem.BdFtLd5 != null)
                                wt.BdFtLd5 = (byte)treeItem.BdFtLd5;
                            if (treeItem.BdFtLd6 != null)
                                wt.BdFtLd6 = (byte)treeItem.BdFtLd6;
                            if (treeItem.BdFtLd7 != null)
                                wt.BdFtLd7 = (byte)treeItem.BdFtLd7;
                            if (treeItem.BdFtLd8 != null)
                                wt.BdFtLd8 = (byte)treeItem.BdFtLd8;
                            if (treeItem.BdFtLd9 != null)
                                wt.BdFtLd9 = (byte)treeItem.BdFtLd9;
                            if (treeItem.BdFtLd10 != null)
                                wt.BdFtLd10 = (byte)treeItem.BdFtLd10;
                            if (treeItem.BdFtLd11 != null)
                                wt.BdFtLd11 = (byte)treeItem.BdFtLd11;
                            if (treeItem.BdFtLd12 != null)
                                wt.BdFtLd12 = (byte)treeItem.BdFtLd12;

                            if (treeItem.BdFtDd1 != null)
                                wt.BdFtDd1 = (byte)treeItem.BdFtDd1;
                            if (treeItem.BdFtDd2 != null)
                                wt.BdFtDd2 = (byte)treeItem.BdFtDd2;
                            if (treeItem.BdFtDd3 != null)
                                wt.BdFtDd3 = (byte)treeItem.BdFtDd3;
                            if (treeItem.BdFtDd4 != null)
                                wt.BdFtDd4 = (byte)treeItem.BdFtDd4;
                            if (treeItem.BdFtDd5 != null)
                                wt.BdFtDd5 = (byte)treeItem.BdFtDd5;
                            if (treeItem.BdFtDd6 != null)
                                wt.BdFtDd6 = (byte)treeItem.BdFtDd6;
                            if (treeItem.BdFtDd7 != null)
                                wt.BdFtDd7 = (byte)treeItem.BdFtDd7;
                            if (treeItem.BdFtDd8 != null)
                                wt.BdFtDd8 = (byte)treeItem.BdFtDd8;
                            if (treeItem.BdFtDd9 != null)
                                wt.BdFtDd9 = (byte)treeItem.BdFtDd9;
                            if (treeItem.BdFtDd10 != null)
                                wt.BdFtDd10 = (byte)treeItem.BdFtDd10;
                            if (treeItem.BdFtDd11 != null)
                                wt.BdFtDd11 = (byte)treeItem.BdFtDd11;
                            if (treeItem.BdFtDd12 != null)
                                wt.BdFtDd12 = (byte)treeItem.BdFtDd12;

                            if (treeItem.BdFtPd1 != null)
                                wt.BdFtPd1 = treeItem.BdFtPd1;
                            if (treeItem.BdFtPd2 != null)
                                wt.BdFtPd2 = treeItem.BdFtPd2;
                            if (treeItem.BdFtPd3 != null)
                                wt.BdFtPd3 = treeItem.BdFtPd3;
                            if (treeItem.BdFtPd4 != null)
                                wt.BdFtPd4 = treeItem.BdFtPd4;
                            if (treeItem.BdFtPd5 != null)
                                wt.BdFtPd5 = treeItem.BdFtPd5;
                            if (treeItem.BdFtPd6 != null)
                                wt.BdFtPd6 = treeItem.BdFtPd6;
                            if (treeItem.BdFtPd7 != null)
                                wt.BdFtPd7 = treeItem.BdFtPd7;
                            if (treeItem.BdFtPd8 != null)
                                wt.BdFtPd8 = treeItem.BdFtPd8;
                            if (treeItem.BdFtPd9 != null)
                                wt.BdFtPd9 = treeItem.BdFtPd9;
                            if (treeItem.BdFtPd10 != null)
                                wt.BdFtPd10 = treeItem.BdFtPd10;
                            if (treeItem.BdFtPd11 != null)
                                wt.BdFtPd11 = treeItem.BdFtPd11;
                            if (treeItem.BdFtPd12 != null)
                                wt.BdFtPd12 = treeItem.BdFtPd12;

                        ctx.WrkTrees.Add(wt);
                        //}
                    }
                    //wColl.Save();
                }
                ctx.SaveChanges();
                //bWrkTrees = true;
            }
        }

        public static void AddTotalsForCountInRptSpeciesSortTimberEvaluation(ref ProjectDbContext projectContext)
        {
            using (var ctx = new TempDbContext())
            {
                float dollarsPerLog = 0;
                float dollarsPerAcre = 0;
                float dollarsPerTon = 0;
                float dollarsPerCcf = 0;
                float dollarsPerMbf = 0;
                float totalDollars = 0;
                float totalMbf = 0;
                float measuredTotalDollars = 0;
                float measuredTotalMbf = 0;
                float measuredTotalCcf = 0;
                float measuredTotalTon = 0;
                float measuredTotalLogs = 0;
                string prevKey = string.Empty;
                string currentKey = string.Empty;
                long saveId = -1;
                string saveSpecies = string.Empty;
                string saveSort = string.Empty;
                string saveSpeciesCode = string.Empty;
                string saveSortCode = string.Empty;

                float speciesTotalsTotalLogs = 0;
                float speciesTotalsTotalDollars = 0;
                float speciesTotalsTotalTons = 0;
                float speciesTotalsTotalCcf = 0;
                float speciesTotalsTotalMbf = 0;
                float speciesTotalsAvgLogDia = 0;
                float speciesTotalsAvgLogLen = 0;
                float speciesTotalsDollarsLog = 0;
                float speciesTotalsDollarsAcre = 0;
                float speciesTotalsDollarsTon = 0;
                float speciesTotalsDollarsCcf = 0;
                float speciesTotalsDollarsMbf = 0;

                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var item in coll)
                {
                    List<RptSpeciesSortTimberEvaluation> rColl = ctx.RptSpeciesSortTimberEvaluations.Where(r => r.StandsSelectedId == item.StandsSelectedId)
                        .OrderByDescending(r => r.Species)
                        .ThenBy(r => r.Seq)
                        .ThenBy(r => r.Sort).ToList();
                    dollarsPerLog = 0;
                    dollarsPerAcre = 0;
                    dollarsPerTon = 0;
                    dollarsPerCcf = 0;
                    dollarsPerMbf = 0;
                    totalDollars = 0;
                    totalMbf = 0;
                    measuredTotalMbf = 0;
                    measuredTotalCcf = 0;
                    measuredTotalTon = 0;
                    measuredTotalDollars = 0;
                    measuredTotalLogs = 0;
                    prevKey = string.Empty;
                    currentKey = string.Empty;
                    saveSpeciesCode = string.Empty;
                    saveSortCode = string.Empty;

                    foreach (var dataItem in rColl)
                    {
                        if (string.IsNullOrEmpty(prevKey))
                        {
                            prevKey = string.Format("{0}{1}", dataItem.StandsSelectedId, dataItem.Species);
                            saveId = (long)dataItem.StandsSelectedId;
                            saveSpecies = dataItem.Species;
                            saveSpeciesCode = dataItem.SpeciesCode;
                            saveSortCode = dataItem.SortCode;
                            //saveSort = dataItem.Sort;
                        }
                        currentKey = string.Format("{0}{1}", dataItem.StandsSelectedId, dataItem.Species);
                        if (currentKey != prevKey)
                        {
                            dollarsPerMbf = measuredTotalDollars / measuredTotalMbf;
                            dollarsPerCcf = measuredTotalDollars / measuredTotalCcf;
                            dollarsPerTon = measuredTotalDollars / measuredTotalTon;
                            dollarsPerAcre = measuredTotalDollars / Convert.ToSingle(item.NetGeographicAcres);
                            dollarsPerLog = measuredTotalDollars / measuredTotalLogs;
                            RptSpeciesSortTimberEvaluation rec = ctx.RptSpeciesSortTimberEvaluations.FirstOrDefault(t => t.StandsSelectedId == saveId && 
                                t.Species == saveSpecies && 
                                t.Sort == "COUNT");
                            if (rec != null)
                            {
                                if (GetMeasuredTreesForSpecies(saveSpecies) == 1)
                                {
                                    AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == item.StandsId && t.Species == saveSpeciesCode);
                                    if (acp != null)
                                    {
                                        int cntTrees = GetCountTreesForSpecies(saveSpecies);
                                        rec.TotalMbf = Math.Round((double)cntTrees * Convert.ToDouble(acp.NetBdFtAcre) * Convert.ToDouble(item.NetGeographicAcres) / 1000, 0);
                                        rec.TotalCunits = Math.Round((double)cntTrees * Convert.ToDouble(acp.NetCuFtAcre) * Convert.ToDouble(item.NetGeographicAcres) / 100, 0);
                                        rec.TotalTons = Math.Round((double)((rec.TotalCunits * rec.LbsCcf) / 2000), 0);
                                    }
                                }
                                rec.DollarsPerAcre = Math.Round((double)dollarsPerAcre, 0);
                                rec.DollarsPerCcf = Math.Round((double)dollarsPerCcf, 0);
                                rec.DollarsPerLog = Math.Round((double)dollarsPerLog, 0);
                                rec.DollarsPerMbf = Math.Round((double)dollarsPerMbf, 0);
                                rec.DollarsPerTon = Math.Round((double)dollarsPerTon, 0);
                                rec.TotalDollars = Math.Round((double)(rec.DollarsPerMbf * Math.Round((double)rec.TotalMbf, 0)), 0);
                            }

                            // move
                            measuredTotalMbf = 0;
                            measuredTotalCcf = 0;
                            measuredTotalTon = 0;
                            measuredTotalDollars = 0;
                            measuredTotalLogs = 0;

                            saveId = (long)dataItem.StandsSelectedId;
                            saveSpecies = dataItem.Species;
                            saveSpeciesCode = dataItem.SpeciesCode;
                            saveSortCode = dataItem.SortCode;
                            prevKey = currentKey;
                        }
                        else
                        {
                            if (dataItem.Sort != "COUNT")
                            {
                                measuredTotalMbf += Convert.ToSingle(dataItem.TotalMbf);
                                measuredTotalCcf += Convert.ToSingle(dataItem.TotalCunits);
                                measuredTotalTon += Convert.ToSingle(dataItem.TotalTons);
                                measuredTotalDollars += Convert.ToSingle(dataItem.TotalDollars);
                                measuredTotalLogs += Convert.ToSingle(dataItem.TotalLogs);
                            }
                        }
                    }
                    dollarsPerMbf = measuredTotalDollars / measuredTotalMbf;
                    dollarsPerCcf = measuredTotalDollars / measuredTotalCcf;
                    dollarsPerTon = measuredTotalDollars / measuredTotalTon;
                    dollarsPerAcre = measuredTotalDollars / Convert.ToSingle(item.NetGeographicAcres);
                    dollarsPerLog = measuredTotalDollars / measuredTotalLogs;
                    RptSpeciesSortTimberEvaluation rec1 = ctx.RptSpeciesSortTimberEvaluations.FirstOrDefault(t => t.StandsSelectedId == saveId &&
                        t.Species == saveSpecies &&
                        t.Sort == "COUNT");
                    if (rec1 != null)
                    {
                        if (GetMeasuredTreesForSpecies(saveSpecies) == 1)
                        {
                            AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == item.StandsId && t.Species == saveSpeciesCode);
                            if (acp != null)
                            {
                                int cntTrees = GetCountTreesForSpecies(saveSpecies);
                                rec1.TotalMbf = Math.Round((double)cntTrees * Convert.ToDouble(acp.NetBdFtAcre) * Convert.ToDouble(item.NetGeographicAcres) / 1000, 0);
                                rec1.TotalCunits = Math.Round((double)cntTrees * Convert.ToDouble(acp.NetCuFtAcre) * Convert.ToDouble(item.NetGeographicAcres) / 100, 0);
                                rec1.TotalTons = Math.Round((double)((rec1.TotalCunits * rec1.LbsCcf) / 2000), 0);
                            }
                        }
                        rec1.DollarsPerAcre = Math.Round((double)dollarsPerAcre, 0);
                        rec1.DollarsPerCcf = Math.Round((double)dollarsPerCcf, 0);
                        rec1.DollarsPerLog = Math.Round((double)dollarsPerLog, 0);
                        rec1.DollarsPerMbf = Math.Round((double)dollarsPerMbf, 0);
                        rec1.DollarsPerTon = Math.Round((double)dollarsPerTon, 0);
                        rec1.TotalDollars = Math.Round((double)(rec1.DollarsPerMbf * Math.Round((double)rec1.TotalMbf, 0)), 0);
                    }
                }

                ctx.SaveChanges();

                //////////////
                ctx.RptSpeciesSortTimberEvalTotals.RemoveRange(ctx.RptSpeciesSortTimberEvalTotals);
                ctx.SaveChanges();

                foreach (var standItem in coll)
                {
                    List<RptSpeciesSortTimberEvaluation> tColl = ctx.RptSpeciesSortTimberEvaluations.Where(r => r.StandsId == standItem.StandsId)
                        .OrderByDescending(r => r.Species)
                        .ThenBy(r => r.Seq)
                        .ThenBy(r => r.Sort).ToList();
                    foreach (var evalItem in tColl)
                    {
                        if (evalItem.LogAvgDia > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsAvgLogDia += Convert.ToSingle((evalItem.LogAvgDia * evalItem.TotalLogs));
                        }
                        if (evalItem.LogAveLen > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsAvgLogLen += Convert.ToSingle((evalItem.LogAveLen * evalItem.TotalLogs));
                        }
                        if (evalItem.DollarsPerLog > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsDollarsLog += Convert.ToSingle((evalItem.DollarsPerLog * evalItem.TotalLogs));
                        }
                        if (evalItem.DollarsPerAcre > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsDollarsAcre += Convert.ToSingle((evalItem.DollarsPerAcre * evalItem.TotalLogs));
                        }
                        if (evalItem.DollarsPerTon > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsDollarsTon += Convert.ToSingle((evalItem.DollarsPerTon * evalItem.TotalLogs));
                        }
                        if (evalItem.DollarsPerCcf > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsDollarsCcf += Convert.ToSingle((evalItem.DollarsPerCcf * evalItem.TotalLogs));
                        }
                        if (evalItem.DollarsPerMbf > 0 && evalItem.TotalLogs > 0)
                        {
                            speciesTotalsDollarsMbf += Convert.ToSingle((evalItem.DollarsPerMbf * evalItem.TotalLogs));
                        }
                        if (evalItem.TotalLogs > 0)
                        {
                            speciesTotalsTotalLogs += Convert.ToSingle(evalItem.TotalLogs);
                        }
                        if (evalItem.TotalTons > 0)
                        {
                            speciesTotalsTotalTons += Convert.ToSingle(evalItem.TotalTons);
                        }
                        if (evalItem.TotalCunits > 0)
                        {
                            speciesTotalsTotalCcf += Convert.ToSingle(evalItem.TotalCunits);
                        }
                        if (evalItem.TotalMbf > 0)
                        {
                            speciesTotalsTotalMbf += Convert.ToSingle(evalItem.TotalMbf);
                        }
                        if (evalItem.TotalDollars > 0)
                        {
                            speciesTotalsTotalDollars += Convert.ToSingle(evalItem.TotalDollars);
                        }
                    }
                    RptSpeciesSortTimberEvalTotal totRec = new RptSpeciesSortTimberEvalTotal();
                    totRec.CountTrees = 0;
                    totRec.DollarsPerAcre = Math.Round((double)(speciesTotalsTotalDollars / standItem.NetGeographicAcres), 0);
                    totRec.DollarsPerCcf = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalCcf), 0);
                    totRec.DollarsPerLog = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalLogs), 0);
                    totRec.DollarsPerMbf = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalMbf), 0);
                    totRec.DollarsPerTon = Math.Round((double)(speciesTotalsTotalDollars / speciesTotalsTotalTons), 0);
                    totRec.LbsCcf = 0;
                    totRec.LogAveLen = speciesTotalsAvgLogLen;
                    totRec.LogAvgDia = speciesTotalsAvgLogDia;
                    totRec.MeasuredTrees = 0;
                    totRec.Seq = 0;
                    totRec.Sort = string.Empty;
                    totRec.SortCode = string.Empty;
                    totRec.Species = string.Empty;
                    totRec.SpeciesCode = string.Empty;
                    totRec.StandName = standItem.StandName;
                    totRec.StandsId = standItem.StandsId;
                    totRec.StandsSelectedId = standItem.StandsSelectedId;
                    totRec.TotalCunits = speciesTotalsTotalCcf;
                    totRec.TotalDollars = speciesTotalsTotalDollars;
                    totRec.TotalLogs = speciesTotalsTotalLogs;
                    totRec.TotalMbf = speciesTotalsTotalMbf;
                    totRec.TotalTons = speciesTotalsTotalTons;
                    totRec.TractName = standItem.TractName;
                    ctx.RptSpeciesSortTimberEvalTotals.Add(totRec);
                }

                ctx.SaveChanges();
            }
        }

        private static int GetMeasuredTreesForSpecies(string saveSpecies)
        {
            int result = 0;
            using (var ctx = new TempDbContext())
            {
                List<RptSpeciesSortTimberEvaluation> rColl = ctx.RptSpeciesSortTimberEvaluations.Where(t => t.Species == saveSpecies).ToList();
                foreach (var item in rColl)
                    result += (int)item.MeasuredTrees;
            }
            return result;
        }

        private static int GetCountTreesForSpecies(string saveSpecies)
        {
            int result = 0;
            using (var ctx = new TempDbContext())
            {
                List<RptSpeciesSortTimberEvaluation> rColl = ctx.RptSpeciesSortTimberEvaluations.Where(t => t.Species == saveSpecies).ToList();
                foreach (var item in rColl)
                    result += (int)item.CountTrees;
            }
            return result;
        }

        public static void ProcessReportAsItems(ref ProjectDbContext projectContext, Project.DAL.Project currentProject, List<Stand> sColl)
        {
            string costName = string.Empty;
            string costType = string.Empty;
            string species = string.Empty;
            float perAcre = 0;
            float perCcf = 0;
            float perLog = 0;
            float perMbf = 0;
            float perTon = 0;
            float totalDollars = 0;

            using (TempDbContext ctx = new TempDbContext())
            {
                List<DefReportA> rptAsFixedItems = projectContext.DefReportAs.Where(r => r.CostType == "FIXED").OrderBy(r => r.ReportAs).ToList();
                foreach (var rptAsFixedItem in rptAsFixedItems)
                {
                    costName = rptAsFixedItem.ReportAs;
                    costType = rptAsFixedItem.CostType;
                    species = string.Empty;
                    perAcre = 0;
                    perCcf = 0;
                    perLog = 0;
                    perMbf = 0;
                    perTon = 0;
                    totalDollars = 0;

                    List<DefReportAsDtl> rptAsFixedItemDtls = projectContext.DefReportAsDtls.Where(d => d.DefReportAsId == rptAsFixedItem.Id).ToList();
                    short fixedItems = (short)(99 - (short)rptAsFixedItemDtls.Count);
                    foreach (var rptAsFixedItemDtl in rptAsFixedItemDtls)
                    {
                        List<RptCostByLineItem> currentCostByLineItem = ctx.RptCostByLineItems.Where(c => c.CostName == rptAsFixedItemDtl.Item).ToList();
                        if (currentCostByLineItem != null)
                        {
                            foreach (RptCostByLineItem item in currentCostByLineItem)
                            {
                                //costType = item.CostType;
                                perAcre += Convert.ToSingle(item.PerAcre);
                                perCcf += Convert.ToSingle(item.PerCcf);
                                perLog += Convert.ToSingle(item.PerLog);
                                perMbf += Convert.ToSingle(item.PerMbf);
                                perTon += Convert.ToSingle(item.PerTon);
                                totalDollars += Convert.ToSingle(item.TotalDollars);
                                ctx.RptCostByLineItems.Remove(item);
                            }
                        }
                    }
                    RptCostByLineItem newFixedCostByLineItem = new RptCostByLineItem();
                    newFixedCostByLineItem.CostName = costName;
                    newFixedCostByLineItem.CostType = costType;
                    newFixedCostByLineItem.CostTypeOrder = 1;
                    newFixedCostByLineItem.PerAcre = perAcre;
                    newFixedCostByLineItem.PerCcf = perCcf;
                    newFixedCostByLineItem.PerLog = perLog;
                    newFixedCostByLineItem.PerMbf = perMbf;
                    newFixedCostByLineItem.PerTon = perTon;
                    newFixedCostByLineItem.Species = string.Empty;
                    newFixedCostByLineItem.TotalDollars = totalDollars;
                    newFixedCostByLineItem.PrintOrder = fixedItems++;
                    ctx.RptCostByLineItems.Add(newFixedCostByLineItem);

                    //if (costName.StartsWith("HAULING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 98;
                    //}
                    //if (costName.StartsWith("YARDING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 99;
                    //}
                    
                }

                List<DefReportA> rptAsVariableItems = projectContext.DefReportAs.Where(r => r.CostType == "VARIABLE").OrderBy(r => r.ReportAs).ToList();
                foreach (var rptAsVariableItem in rptAsVariableItems)
                {
                    costName = rptAsVariableItem.ReportAs;
                    costType = rptAsVariableItem.CostType;
                    species = string.Empty;
                    perAcre = 0;
                    perCcf = 0;
                    perLog = 0;
                    perMbf = 0;
                    perTon = 0;
                    totalDollars = 0;

                    List<DefReportAsDtl> rptAsVariableItemDtls = projectContext.DefReportAsDtls.Where(d => d.DefReportAsId == rptAsVariableItem.Id).ToList();
                    short variableItems = (short)(99 - (short)rptAsVariableItemDtls.Count);
                    foreach (var rptAsFixedItemDtl in rptAsVariableItemDtls)
                    {
                        List<RptCostByLineItem> currentCostByLineItem = ctx.RptCostByLineItems.Where(c => c.CostName == rptAsFixedItemDtl.Item).ToList();
                        if (currentCostByLineItem != null)
                        {
                            foreach (RptCostByLineItem item in currentCostByLineItem)
                            {
                                //costType = item.CostType;
                                perAcre += Convert.ToSingle(item.PerAcre);
                                perCcf += Convert.ToSingle(item.PerCcf);
                                perLog += Convert.ToSingle(item.PerLog);
                                perMbf += Convert.ToSingle(item.PerMbf);
                                perTon += Convert.ToSingle(item.PerTon);
                                totalDollars += Convert.ToSingle(item.TotalDollars);
                                ctx.RptCostByLineItems.Remove(item);
                            }
                        }
                    }
                    RptCostByLineItem newVariableCostByLineItem = new RptCostByLineItem();
                    newVariableCostByLineItem.CostName = costName;
                    newVariableCostByLineItem.CostType = costType;
                    newVariableCostByLineItem.CostTypeOrder = 2;
                    newVariableCostByLineItem.PerAcre = perAcre;
                    newVariableCostByLineItem.PerCcf = perCcf;
                    newVariableCostByLineItem.PerLog = perLog;
                    newVariableCostByLineItem.PerMbf = perMbf;
                    newVariableCostByLineItem.PerTon = perTon;
                    newVariableCostByLineItem.Species = string.Empty;
                    newVariableCostByLineItem.TotalDollars = totalDollars;
                    newVariableCostByLineItem.PrintOrder = variableItems++;
                    ctx.RptCostByLineItems.Add(newVariableCostByLineItem);

                    //if (costName.StartsWith("HAULING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 98;
                    //}
                    //if (costName.StartsWith("YARDING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 99;
                    //}

                }

                List<DefReportA> rptAsAnnualItems = projectContext.DefReportAs.Where(r => r.CostType == "ANNUAL").OrderBy(r => r.ReportAs).ToList();
                foreach (var rptAsAnnualItem in rptAsAnnualItems)
                {
                    costName = rptAsAnnualItem.ReportAs;
                    costType = rptAsAnnualItem.CostType;
                    species = string.Empty;
                    perAcre = 0;
                    perCcf = 0;
                    perLog = 0;
                    perMbf = 0;
                    perTon = 0;
                    totalDollars = 0;

                    List<DefReportAsDtl> rptAsAnnualItemDtls = projectContext.DefReportAsDtls.Where(d => d.DefReportAsId == rptAsAnnualItem.Id).ToList();
                    short annualItems = (short)(99 - (short)rptAsAnnualItemDtls.Count);
                    foreach (var rptAsAnnualItemDtl in rptAsAnnualItemDtls)
                    {
                        List<RptCostByLineItem> currentCostByLineItem = ctx.RptCostByLineItems.Where(c => c.CostName == rptAsAnnualItemDtl.Item).ToList();
                        if (currentCostByLineItem != null)
                        {
                            foreach (RptCostByLineItem item in currentCostByLineItem)
                            {
                                //costType = item.CostType;
                                perAcre += Convert.ToSingle(item.PerAcre);
                                perCcf += Convert.ToSingle(item.PerCcf);
                                perLog += Convert.ToSingle(item.PerLog);
                                perMbf += Convert.ToSingle(item.PerMbf);
                                perTon += Convert.ToSingle(item.PerTon);
                                totalDollars += Convert.ToSingle(item.TotalDollars);
                                ctx.RptCostByLineItems.Remove(item);
                            }
                        }
                    }
                    RptCostByLineItem newAnnualCostByLineItem = new RptCostByLineItem();
                    newAnnualCostByLineItem.CostName = costName;
                    newAnnualCostByLineItem.CostType = costType;
                    newAnnualCostByLineItem.CostTypeOrder = 3;
                    newAnnualCostByLineItem.PerAcre = perAcre;
                    newAnnualCostByLineItem.PerCcf = perCcf;
                    newAnnualCostByLineItem.PerLog = perLog;
                    newAnnualCostByLineItem.PerMbf = perMbf;
                    newAnnualCostByLineItem.PerTon = perTon;
                    newAnnualCostByLineItem.Species = string.Empty;
                    newAnnualCostByLineItem.TotalDollars = totalDollars;
                    newAnnualCostByLineItem.PrintOrder = annualItems++;
                    ctx.RptCostByLineItems.Add(newAnnualCostByLineItem);

                    //if (costName.StartsWith("HAULING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 98;
                    //}
                    //if (costName.StartsWith("YARDING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 99;
                    //}

                }

                List<DefReportA> rptAsHaulingItems = projectContext.DefReportAs.Where(r => r.CostType == "HAULING").OrderBy(r => r.ReportAs).ToList();
                foreach (var rptAsHaulingItem in rptAsHaulingItems)
                {
                    costName = rptAsHaulingItem.ReportAs;
                    costType = rptAsHaulingItem.CostType;
                    species = string.Empty;
                    perAcre = 0;
                    perCcf = 0;
                    perLog = 0;
                    perMbf = 0;
                    perTon = 0;
                    totalDollars = 0;

                    List<DefReportAsDtl> rptAsHaulingItemDtls = projectContext.DefReportAsDtls.Where(d => d.DefReportAsId == rptAsHaulingItem.Id).ToList();
                    short annualItems = (short)(99 - (short)rptAsHaulingItemDtls.Count);
                    foreach (var rptAsAnnualItemDtl in rptAsHaulingItemDtls)
                    {
                        List<RptCostByLineItem> currentCostByLineItem = ctx.RptCostByLineItems.Where(c => c.CostName == rptAsAnnualItemDtl.Item).ToList();
                        if (currentCostByLineItem != null)
                        {
                            foreach (RptCostByLineItem item in currentCostByLineItem)
                            {
                                //costType = item.CostType;
                                perAcre += Convert.ToSingle(item.PerAcre);
                                perCcf += Convert.ToSingle(item.PerCcf);
                                perLog += Convert.ToSingle(item.PerLog);
                                perMbf += Convert.ToSingle(item.PerMbf);
                                perTon += Convert.ToSingle(item.PerTon);
                                totalDollars += Convert.ToSingle(item.TotalDollars);
                                ctx.RptCostByLineItems.Remove(item);
                            }
                        }
                    }
                    RptCostByLineItem newHaulingCostByLineItem = new RptCostByLineItem();
                    newHaulingCostByLineItem.CostName = costName;
                    newHaulingCostByLineItem.CostType = costType;
                    newHaulingCostByLineItem.CostTypeOrder = 98;
                    newHaulingCostByLineItem.PerAcre = perAcre;
                    newHaulingCostByLineItem.PerCcf = perCcf;
                    newHaulingCostByLineItem.PerLog = perLog;
                    newHaulingCostByLineItem.PerMbf = perMbf;
                    newHaulingCostByLineItem.PerTon = perTon;
                    newHaulingCostByLineItem.Species = string.Empty;
                    newHaulingCostByLineItem.TotalDollars = totalDollars;
                    newHaulingCostByLineItem.PrintOrder = annualItems++;
                    ctx.RptCostByLineItems.Add(newHaulingCostByLineItem);

                    //if (costName.StartsWith("HAULING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 98;
                    //}
                    //if (costName.StartsWith("YARDING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 99;
                    //}

                }

                List<DefReportA> rptAsYardingItems = projectContext.DefReportAs.Where(r => r.CostType == "YARDING").OrderBy(r => r.ReportAs).ToList();
                foreach (var rptAsYardingItem in rptAsYardingItems)
                {
                    costName = rptAsYardingItem.ReportAs;
                    costType = rptAsYardingItem.CostType;
                    species = string.Empty;
                    perAcre = 0;
                    perCcf = 0;
                    perLog = 0;
                    perMbf = 0;
                    perTon = 0;
                    totalDollars = 0;

                    List<DefReportAsDtl> rptAsYardingItemDtls = projectContext.DefReportAsDtls.Where(d => d.DefReportAsId == rptAsYardingItem.Id).ToList();
                    short annualItems = (short)(99 - (short)rptAsYardingItemDtls.Count);
                    foreach (var rptAsYardingItemDtl in rptAsYardingItemDtls)
                    {
                        List<RptCostByLineItem> currentCostByLineItem = ctx.RptCostByLineItems.Where(c => c.CostName == rptAsYardingItemDtl.Item).ToList();
                        if (currentCostByLineItem != null)
                        {
                            foreach (RptCostByLineItem item in currentCostByLineItem)
                            {
                                //costType = item.CostType;
                                perAcre += Convert.ToSingle(item.PerAcre);
                                perCcf += Convert.ToSingle(item.PerCcf);
                                perLog += Convert.ToSingle(item.PerLog);
                                perMbf += Convert.ToSingle(item.PerMbf);
                                perTon += Convert.ToSingle(item.PerTon);
                                totalDollars += Convert.ToSingle(item.TotalDollars);
                                ctx.RptCostByLineItems.Remove(item);
                            }
                        }
                    }
                    RptCostByLineItem newYardingCostByLineItem = new RptCostByLineItem();
                    newYardingCostByLineItem.CostName = costName;
                    newYardingCostByLineItem.CostType = costType;
                    newYardingCostByLineItem.CostTypeOrder = 99;
                    newYardingCostByLineItem.PerAcre = perAcre;
                    newYardingCostByLineItem.PerCcf = perCcf;
                    newYardingCostByLineItem.PerLog = perLog;
                    newYardingCostByLineItem.PerMbf = perMbf;
                    newYardingCostByLineItem.PerTon = perTon;
                    newYardingCostByLineItem.Species = string.Empty;
                    newYardingCostByLineItem.TotalDollars = totalDollars;
                    newYardingCostByLineItem.PrintOrder = annualItems++;
                    ctx.RptCostByLineItems.Add(newYardingCostByLineItem);

                    //if (costName.StartsWith("HAULING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 98;
                    //}
                    //if (costName.StartsWith("YARDING"))
                    //{
                    //    newCostByLineItem.PrintOrder = 99;
                    //}

                }
                ctx.SaveChanges();
            }
        }

        //public static void BuildRptSpeciesSummaryLogVolumesCombined(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands, 
        //    float pAcres, float pPlots, float pTrees)
        //{
        //    int saveId = -1;
        //    float tLogs = 0;
        //    string currentKey = string.Empty;
        //    string saveKey = string.Empty;
        //    string tSpecies = string.Empty;
        //    string tStatus = string.Empty;
        //    float tTreesAcre = 0, tBasalArea = 0, tLogsAcre = 0;
        //    float tTotalNetBdFt = 0;
        //    float tTreeHeight = 0;
        //    float tTotalHeight = 0;
        //    float tNetBdFtVolume = 0, tNetCuFtVolume = 0;
        //    float tGrossBdFtVolume = 0, tGrossCuFtVolume = 0;
        //    float tNetMbf = 0;
        //    double temp = 0;
        //    //bool bCombine = bReportCombine;

        //    //if (bReportCombine)
        //    //{
        //    //    if (sColl.Count == 1)
        //    //        bCombine = false;
        //    //}
        //    DefStat defStat = projectContext.DefStats.First();
        //    List<double> mbfColl = new List<double>();
        //    List<double> xMColl = new List<double>();
        //    List<double> d2Coll = new List<double>();

        //    using (var ctx = new TempDbContext())
        //    {
        //        List<WrkTree> wColl = ctx.WrkTrees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).ToList();
        //        mbfColl.Clear();

        //        foreach (var treeItem in wColl)
        //        {
        //            if (string.IsNullOrEmpty(tSpecies))
        //            {
        //                //saveId = (int)item.StandsId;
        //                tSpecies = treeItem.SpeciesAbbreviation;
        //                tStatus = treeItem.TreeStatusDisplayCode;
        //                //if (pIsCombined)
        //                //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
        //                //else
        //                saveKey = String.Format("{0}{1}", tSpecies, tStatus);
        //            }
        //            //if (pIsCombined)
        //            //    currentKey = String.Format("{0}{1}{2}{3}", "*", "*", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
        //            //else
        //            currentKey = String.Format("{0}{1}", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
        //            if (saveKey != currentKey)
        //            {
        //                // write record
        //                RptSpeciesSummaryLogsVolume rec = new RptSpeciesSummaryLogsVolume();
        //                //SepStandsSelected stand = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId);
        //                //rec.StandsId = saveId;
        //                //rec.StandsSelectedId = stand.StandsSelectedId;
        //                rec.Descriptions = string.Empty;
        //                rec.TreesAcre = tTreesAcre / pPlots; // stand.Plots;
        //                rec.BasalArea = tBasalArea / pPlots; // stand.Plots;
        //                rec.GrossBdFtAcre = tGrossBdFtVolume / pPlots; // stand.Plots;
        //                rec.NetBdFtAcre = tNetBdFtVolume / pPlots; // stand.Plots;
        //                rec.GrossCuFtAcre = tGrossCuFtVolume / pPlots; // stand.Plots;
        //                rec.NetCuFtAcre = tNetCuFtVolume / pPlots; // stand.Plots;
        //                rec.TotalGrossCcf = (rec.GrossCuFtAcre * pAcres) / 100;
        //                rec.TotalNetCunits = (rec.NetCuFtAcre * pAcres) / 100;
        //                rec.TotalGrossMbf = (rec.GrossBdFtAcre * pAcres / 1000;
        //                rec.TotalNetMbf = (rec.NetBdFtAcre * pAcres / 1000;
        //                rec.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
        //                rec.LogsAcre = tLogsAcre / pPlots; // stand.Plots;
        //                rec.TotalTons = (rec.TotalNetCunits * rec.LbsCcf) / 2000;
        //                rec.Species = tSpecies;
        //                rec.Status = tStatus;
        //                rec.TonsAcre = rec.TotalTons / pAcres;
        //                rec.TonsMbf = rec.TotalTons / rec.TotalNetMbf;
        //                rec.PercentLogsAcre = 0;
        //                rec.PercentNetMbf = 0;
        //                rec.PercentTons = 0;
        //                rec.PercentTreesAcre = 0;
        //                rec.PerNetCunits = 0;
        //                //rec.AvgTreeHeight = tTreeHeight / tLogs;
        //                rec.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / pPlots / rec.TreesAcre));
        //                rec.QmDbh = (double)Math.Sqrt((double)rec.BasalArea / ((double)rec.TreesAcre * 0.005454154));

        //                if (speciesColl != null)
        //                {
        //                    string speciesStatus = rec.Species + rec.Status;
        //                    int iSpecies = ReportsBLL.GetSpeciesIdx(speciesStatus, speciesColl);
        //                    List<TempPlotSpecy> tempPlotSpecies = ctx.TempPlotSpecies.ToList();

        //                    mbfColl.Clear();
        //                    xMColl.Clear();
        //                    d2Coll.Clear();
        //                    foreach (var plotSpeciesItem in tempPlotSpecies)
        //                    {
        //                        switch (iSpecies)
        //                        {
        //                            case 0:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1));
        //                                break;
        //                            case 1:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2));
        //                                break;
        //                            case 2:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3));
        //                                break;
        //                            case 3:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4));
        //                                break;
        //                            case 4:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5));
        //                                break;
        //                            case 5:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6));
        //                                break;
        //                            case 6:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7));
        //                                break;
        //                            case 7:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8));
        //                                break;
        //                            case 8:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9, 0));
        //                                break;
        //                            case 9:
        //                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10));
        //                                break;
        //                        }
        //                    }
        //                    rec.Mean = mbfColl.Average();
        //                    foreach (var plotSpeciesItem in tempPlotSpecies)
        //                    {
        //                        switch (iSpecies)
        //                        {
        //                            case 0:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net1 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 1:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net2 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 2:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net3 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 3:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net4 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 4:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net5 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 5:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net6 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 6:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net7 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 7:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net8 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 8:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net9 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                            case 9:
        //                                xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10 - (double)rec.Mean, 0));
        //                                temp = (Convert.ToDouble(plotSpeciesItem.Net10 - (double)rec.Mean) * defStat.StdDeviation);
        //                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                                break;
        //                        }
        //                    }
        //                    double sumOfSquaresOfDifferences = d2Coll.Sum();
        //                    rec.Sd = Math.Round(Math.Sqrt(sumOfSquaresOfDifferences / (mbfColl.Count - 1)), 0);
        //                    rec.Se = Math.Round((double)(rec.Sd / Math.Sqrt(mbfColl.Count)), 1);
        //                    rec.CvPercent = Math.Round((double)((rec.Sd / rec.Mean / defStat.StdDeviation) * 100), 1);
        //                    rec.SePercent = Math.Round((double)((rec.Se / rec.Mean) * 100), 1);
        //                }
        //                else
        //                {
        //                    rec.Sd = 0;
        //                    rec.Se = 0;
        //                    rec.CvPercent = 0;
        //                    rec.SePercent = 0;
        //                }
        //                ctx.RptSpeciesSummaryLogsVolumes.Add(rec);

        //                // move fields
        //                tSpecies = treeItem.SpeciesAbbreviation;
        //                tStatus = treeItem.TreeStatusDisplayCode;
        //                //if (pIsCombined)
        //                //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
        //                //else
        //                saveKey = String.Format("{0}{1}", tSpecies, tStatus);
        //                //saveId = (int)item.StandsId;
        //                tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre;
        //                tBasalArea = Convert.ToSingle(treeItem.BasalArea;
        //                tLogsAcre = Convert.ToSingle(treeItem.TreesPerAcre * Convert.ToSingle(GetSegmentsInTree(treeItem);
        //                tGrossBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtGrossVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                tGrossCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtGrossVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
        //                //    tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
        //                //else
        //                //    tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
        //                if (treeItem.TotalHeight == 0)
        //                    tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
        //                else
        //                    tTotalHeight = Convert.ToSingle(treeItem.TotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
        //                mbfColl.Clear();
        //                mbfColl.Add((double)treeItem.TotalMbf);
        //                tLogs = 1;
        //            }
        //            else
        //            {
        //                tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre;
        //                tBasalArea += Convert.ToSingle(treeItem.BasalArea;
        //                tLogsAcre += Convert.ToSingle(treeItem.TreesPerAcre * Convert.ToSingle(GetSegmentsInTree(treeItem);
        //                //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
        //                //    tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
        //                //else
        //                //    tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
        //                if (treeItem.TotalHeight == 0)
        //                    tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
        //                else
        //                    tTotalHeight += Convert.ToSingle(treeItem.TotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
        //                if (treeItem.TotalBdFtGrossVolume != null)
        //                    tGrossBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtGrossVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                if (treeItem.TotalBdFtNetVolume != null)
        //                    tNetBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                if (treeItem.TotalCuFtGrossVolume != null)
        //                    tGrossCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtGrossVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                if (treeItem.TotalCuFtNetVolume != null)
        //                    tNetCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
        //                mbfColl.Add((double)treeItem.TotalMbf);
        //                tLogs += Convert.ToSingle(treeItem.TreeCount;
        //            }
        //        }
        //        RptSpeciesSummaryLogsVolume rec1 = new RptSpeciesSummaryLogsVolume();
        //        //SepStandsSelected stand1 = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId); // ProjectBLL.GetStand(ProjectDataSource, saveId);
        //        //rec1.StandsId = saveId; //stand1.StandsId;
        //        //rec1.StandsSelectedId = stand1.StandsSelectedId;
        //        //rec1.TractName = stand1.TractName;
        //        //rec1.StandName = stand1.StandName;
        //        rec1.Descriptions = string.Empty;
        //        rec1.TreesAcre = tTreesAcre / pPlots; // stand1.Plots;
        //        rec1.BasalArea = tBasalArea / pPlots; // stand1.Plots;
        //        rec1.GrossBdFtAcre = tGrossBdFtVolume / pPlots; // stand1.Plots;
        //        rec1.NetBdFtAcre = tNetBdFtVolume / pPlots; // stand1.Plots;
        //        rec1.GrossCuFtAcre = tGrossCuFtVolume / pPlots; // stand1.Plots;
        //        rec1.NetCuFtAcre = tNetCuFtVolume / pPlots; // stand1.Plots;
        //        rec1.TotalGrossCcf = (rec1.GrossCuFtAcre * pAcres) / 100;
        //        rec1.TotalNetCunits = (rec1.NetCuFtAcre * pAcres) / 100;
        //        rec1.TotalGrossMbf = (rec1.GrossBdFtAcre * pAcres / 1000;
        //        rec1.TotalNetMbf = (rec1.NetBdFtAcre * pAcres / 1000;
        //        rec1.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
        //        rec1.LogsAcre = tLogsAcre / pPlots; // stand1.Plots;
        //        rec1.TotalTons = (rec1.TotalNetCunits * rec1.LbsCcf) / 2000;
        //        rec1.Species = tSpecies;
        //        rec1.Status = tStatus;
        //        rec1.TonsAcre = rec1.TotalTons / pAcres;
        //        rec1.TonsMbf = rec1.TotalTons / rec1.TotalNetMbf;
        //        rec1.PercentLogsAcre = 0;
        //        rec1.PercentNetMbf = 0;
        //        rec1.PercentTons = 0;
        //        rec1.PercentTreesAcre = 0;
        //        rec1.PerNetCunits = 0;
        //        //rec1.AvgTreeHeight = tTreeHeight / tLogs;
        //        rec1.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / pPlots / rec1.TreesAcre));
        //        rec1.QmDbh = (double)Math.Sqrt((double)rec1.BasalArea / ((double)rec1.TreesAcre * 0.005454154));

        //        if (speciesColl != null)
        //        {
        //            string speciesStatus1 = rec1.Species + rec1.Status;
        //            int iSpecies1 = ReportsBLL.GetSpeciesIdx(speciesStatus1, speciesColl);
        //            List<TempPlotSpecy> tempPlotSpecies1 = ctx.TempPlotSpecies.ToList();

        //            mbfColl.Clear();
        //            xMColl.Clear();
        //            d2Coll.Clear();
        //            foreach (var plotSpeciesItem in tempPlotSpecies1)
        //            {
        //                switch (iSpecies1)
        //                {
        //                    case 0:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1));
        //                        break;
        //                    case 1:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2));
        //                        break;
        //                    case 2:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3));
        //                        break;
        //                    case 3:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4));
        //                        break;
        //                    case 4:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5));
        //                        break;
        //                    case 5:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6));
        //                        break;
        //                    case 6:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7));
        //                        break;
        //                    case 7:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8));
        //                        break;
        //                    case 8:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9, 0));
        //                        break;
        //                    case 9:
        //                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10));
        //                        break;
        //                }
        //            }

        //            rec1.Mean = mbfColl.Average();
        //            foreach (var plotSpeciesItem in tempPlotSpecies1)
        //            {
        //                switch (iSpecies1)
        //                {
        //                    case 0:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net1 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 1:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net2 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 2:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net3 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 3:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net4 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 4:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net5 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 5:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net6 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 6:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net7 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 7:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net8 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 8:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net9 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                    case 9:
        //                        xMColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10 - (double)rec1.Mean, 0));
        //                        temp = (Convert.ToDouble(plotSpeciesItem.Net10 - (double)rec1.Mean) * defStat.StdDeviation);
        //                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
        //                        break;
        //                }
        //            }
        //            double sumOfSquaresOfDifferences1 = d2Coll.Sum();
        //            rec1.Sd = Math.Round(Math.Sqrt(sumOfSquaresOfDifferences1 / (mbfColl.Count - 1)), 0);
        //            rec1.Se = Math.Round((double)(rec1.Sd / Math.Sqrt(mbfColl.Count)), 1);
        //            rec1.CvPercent = Math.Round((double)((rec1.Sd / rec1.Mean / defStat.StdDeviation) * 100), 1);
        //            rec1.SePercent = Math.Round((double)((rec1.Se / rec1.Mean) * 100), 1);
        //        }
        //        else
        //        {
        //            rec1.Sd = 0;
        //            rec1.Se = 0;
        //            rec1.CvPercent = 0;
        //            rec1.SePercent = 0;
        //        }
        //        ctx.RptSpeciesSummaryLogsVolumes.Add(rec1);
        //        //rColl.Save();
        //        ctx.SaveChanges();
        //    }
        //}

        public static void BuildRptSpeciesSummaryLogVolumes(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands)
        {
            int saveId = -1;
            float tLogs = 0;
            float tMeasuredTpa = 0;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tTreesAcre = 0, tBasalArea = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tTreeHeight = 0;
            float tTotalHeight = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0;
            float tGrossBdFtVolume = 0, tGrossCuFtVolume = 0;
            float tNetMbf = 0;
            double temp = 0;
            
            DefStat defStat = projectContext.DefStats.First();
            List<double> mbfColl = new List<double>();
            List<double> xMColl = new List<double>();
            List<double> d2Coll = new List<double>();

            using (var ctx = new TempDbContext())
            {
                foreach (Stand item in pStands)
                {
                    List<Tree> wColl = projectContext.Trees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).Where(w => w.StandsId == item.StandsId).ToList();
                    mbfColl.Clear();
                    xMColl.Clear();
                    d2Coll.Clear();
                    tLogs = 0;
                    tMeasuredTpa = 0;
                    currentKey = string.Empty;
                    saveKey = string.Empty;
                    tSpecies = string.Empty;
                    tStatus = string.Empty;
                    tTreesAcre = 0;
                    tBasalArea = 0;
                    tLogsAcre = 0;
                    tTotalNetBdFt = 0;
                    tTreeHeight = 0;
                    tTotalHeight = 0;
                    tNetBdFtVolume = 0;
                    tNetCuFtVolume = 0;
                    tGrossBdFtVolume = 0;
                    tGrossCuFtVolume = 0;
                    tNetMbf = 0;

                    foreach (var treeItem in wColl)
                    {
                        if (string.IsNullOrEmpty(tSpecies))
                        {
                            saveId = (int)item.StandsId;
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = treeItem.TreeStatusDisplayCode;
                            saveKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, tSpecies, tStatus);
                        }
                        currentKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                        if (saveKey != currentKey)
                        {
                            // write record
                            RptSpeciesSummaryLogsVolume rec = new RptSpeciesSummaryLogsVolume();
                            SepStandsSelected stand = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId);
                            rec.StandsId = saveId;
                            rec.StandsSelectedId = stand.StandsSelectedId;
                            rec.TractName = stand.TractName;
                            rec.StandName = stand.StandName;
                            rec.Descriptions = string.Empty;
                            rec.TreesAcre = tTreesAcre / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.BasalArea = tBasalArea / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.GrossBdFtAcre = tGrossBdFtVolume / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.NetBdFtAcre = tNetBdFtVolume / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.GrossCuFtAcre = tGrossCuFtVolume / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.NetCuFtAcre = tNetCuFtVolume / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.TotalGrossCcf = (rec.GrossCuFtAcre * stand.NetGeographicAcres) / 100;
                            rec.TotalNetCunits = (rec.NetCuFtAcre * stand.NetGeographicAcres) / 100;
                            rec.TotalGrossMbf = (rec.GrossBdFtAcre * Convert.ToSingle(stand.NetGeographicAcres) / 1000);
                            rec.TotalNetMbf = (rec.NetBdFtAcre * Convert.ToSingle(stand.NetGeographicAcres) / 1000);
                            rec.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
                            rec.LogsAcre = tLogsAcre / Convert.ToSingle(stand.Plots); // stand.Plots;
                            rec.TotalTons = (rec.TotalNetCunits * rec.LbsCcf) / 2000;
                            rec.Species = tSpecies;
                            rec.Status = tStatus;
                            rec.TonsAcre = rec.TotalTons / stand.NetGeographicAcres;
                            rec.TonsMbf = rec.TotalTons / rec.TotalNetMbf;
                            rec.PercentLogsAcre = 0;
                            rec.PercentNetMbf = 0;
                            rec.PercentTons = 0;
                            rec.PercentTreesAcre = 0;
                            rec.PerNetCunits = 0;
                            //rec.AvgTreeHeight = tTreeHeight / tLogs;
                            //rec.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / Convert.ToSingle(item.Plots) / rec.TreesAcre));
                            rec.AvgTreeHeight = Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTpa));
                            //rec.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / rec.TreesAcre));
                            rec.QmDbh = (double)Math.Sqrt((double)rec.BasalArea / ((double)rec.TreesAcre * 0.005454154));
                            if (speciesColl != null)
                            {
                                string speciesStatus = rec.Species + rec.Status;
                                int iSpecies = ReportsBLL.GetSpeciesIdx(speciesStatus, speciesColl);
                                List<TempPlotSpecy> tempPlotSpecies = ctx.TempPlotSpecies.Where(t => t.StandsId == (int)item.StandsId).ToList();

                                mbfColl.Clear();
                                xMColl.Clear();
                                d2Coll.Clear();
                                foreach (var plotSpeciesItem in tempPlotSpecies)
                                {
                                    switch (iSpecies)
                                    {
                                        case 0:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1)));
                                            break;
                                        case 1:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2)));
                                            break;
                                        case 2:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3)));
                                            break;
                                        case 3:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4)));
                                            break;
                                        case 4:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5)));
                                            break;
                                        case 5:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6)));
                                            break;
                                        case 6:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7)));
                                            break;
                                        case 7:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8)));
                                            break;
                                        case 8:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9), 0));
                                            break;
                                        case 9:
                                            mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10)));
                                            break;
                                    }
                                }
                                rec.Mean = mbfColl.Average();
                                foreach (var plotSpeciesItem in tempPlotSpecies)
                                {
                                    switch (iSpecies)
                                    {
                                        case 0:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net1) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 1:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net2) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 2:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net3) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 3:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net4) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 4:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net5) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 5:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net6) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 6:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net7) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 7:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net8) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 8:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net9) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                        case 9:
                                            temp = (Convert.ToDouble(plotSpeciesItem.Net10) - (double)rec.Mean) * defStat.StdDeviation;
                                            xMColl.Add(Math.Round(temp, 0));
                                            d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                            break;
                                    }
                                }
                                //double sumOfSquaresOfDifferences = (double)mbfColl.Select(val => ((val - rec.Mean)*defStat.StdDeviation) * ((val - rec.Mean)*defStat.StdDeviation)).Sum();
                                double sumOfSquaresOfDifferences = d2Coll.Sum();
                                double t1 = sumOfSquaresOfDifferences / (mbfColl.Count - 1);
                                rec.Sd = Math.Round(Math.Sqrt(t1), 0);
                                rec.Se = Math.Round((double)(rec.Sd / Math.Sqrt(mbfColl.Count)), 1);
                                rec.CvPercent = Math.Round((double)((rec.Sd / rec.Mean / defStat.StdDeviation) * 100), 1);
                                rec.SePercent = Math.Round((double)((rec.Se / rec.Mean) * 100), 1);
                            }
                            else
                            {
                                rec.Sd = 0;
                                rec.Se = 0;
                                rec.CvPercent = 0;
                                rec.SePercent = 0;
                            }
                            ctx.RptSpeciesSummaryLogsVolumes.Add(rec);

                            // move fields
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = treeItem.TreeStatusDisplayCode;
                            //if (pIsCombined)
                            //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
                            //else
                            saveKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, tSpecies, tStatus);
                            saveId = (int)item.StandsId;
                            tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre);
                            tBasalArea = Convert.ToSingle(treeItem.BasalArea);
                            tLogsAcre = Convert.ToSingle(treeItem.TreesPerAcre) * GetSegmentsInTree(treeItem);
                            tGrossBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetMbf = Convert.ToSingle(treeItem.TotalMbf);
                            //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
                            //    tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
                            //else
                            //    tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
                            tMeasuredTpa = 0;
                            if (treeItem.TreeType == "D")
                            {
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                                else
                                    tTotalHeight = Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                                tMeasuredTpa = Convert.ToSingle(treeItem.TreesPerAcre);
                            }
                            //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
                            //{
                            //    tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
                            //    tMeasuredLogs++;
                            //}
                            //else
                            //{
                            //    if (treeItem.TotalHeight != null && treeItem.CalcTotalHeight > 0)
                            //    {
                            //        tTreeHeight = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
                            //        tMeasuredLogs++;
                            //    }
                            //}
                            tLogs = 1;
                        }
                        else
                        {
                            tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre);
                            tBasalArea += Convert.ToSingle(treeItem.BasalArea);
                            tLogsAcre += Convert.ToSingle(treeItem.TreesPerAcre) * GetSegmentsInTree(treeItem);
                            //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
                            //    tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
                            //else
                            //    tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
                            if (treeItem.TreeType == "D")
                            {
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                                else
                                    tTotalHeight += Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                                tMeasuredTpa += Convert.ToSingle(treeItem.TreesPerAcre);
                            }
                            //if (treeItem.TotalHeight != null && treeItem.TotalHeight > 0)
                            //{
                            //    tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.TotalHeight;
                            //    tMeasuredLogs++;
                            //}
                            //else
                            //{
                            //    if (treeItem.TotalHeight != null && treeItem.CalcTotalHeight > 0)
                            //    {
                            //        tTreeHeight += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(treeItem.CalcTotalHeight;
                            //        tMeasuredLogs++;
                            //    }
                            //}
                            tGrossBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetMbf += Convert.ToSingle(treeItem.TotalMbf);
                            //mbfColl.Add((double)treeItem.TotalMbf);
                            tLogs += Convert.ToSingle(treeItem.TreeCount);
                        }
                    }
                    RptSpeciesSummaryLogsVolume rec1 = new RptSpeciesSummaryLogsVolume();
                    SepStandsSelected stand1 = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId); // ProjectBLL.GetStand(ProjectDataSource, saveId);
                    rec1.StandsId = saveId; //stand1.StandsId;
                    rec1.StandsSelectedId = stand1.StandsSelectedId;
                    //if (pIsCombined)
                    //{
                    //    rec1.StandsId = 0;
                    //    rec1.TractName = "*";
                    //    rec1.StandName = "*";
                    //}
                    //else
                    //{
                    rec1.TractName = stand1.TractName;
                    rec1.StandName = stand1.StandName;
                    //}
                    rec1.Descriptions = string.Empty;
                    rec1.TreesAcre = tTreesAcre / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.BasalArea = tBasalArea / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.GrossBdFtAcre = tGrossBdFtVolume / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.NetBdFtAcre = tNetBdFtVolume / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.GrossCuFtAcre = tGrossCuFtVolume / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.NetCuFtAcre = tNetCuFtVolume / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.TotalGrossCcf = (rec1.GrossCuFtAcre * stand1.NetGeographicAcres) / 100;
                    rec1.TotalNetCunits = (rec1.NetCuFtAcre * stand1.NetGeographicAcres) / 100;
                    rec1.TotalGrossMbf = (rec1.GrossBdFtAcre * Convert.ToSingle(stand1.NetGeographicAcres) / 1000);
                    rec1.TotalNetMbf = (rec1.NetBdFtAcre * Convert.ToSingle(stand1.NetGeographicAcres) / 1000);
                    rec1.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
                    rec1.LogsAcre = tLogsAcre / Convert.ToSingle(stand1.Plots); // stand1.Plots;
                    rec1.TotalTons = (rec1.TotalNetCunits * rec1.LbsCcf) / 2000;
                    rec1.Species = tSpecies;
                    rec1.Status = tStatus;
                    rec1.TonsAcre = rec1.TotalTons / stand1.NetGeographicAcres;
                    rec1.TonsMbf = rec1.TotalTons / rec1.TotalNetMbf;
                    rec1.PercentLogsAcre = 0;
                    rec1.PercentNetMbf = 0;
                    rec1.PercentTons = 0;
                    rec1.PercentTreesAcre = 0;
                    rec1.PerNetCunits = 0;
                    //rec1.AvgTreeHeight = tTreeHeight / tLogs;
                    //rec1.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / Convert.ToSingle(item.Plots) / rec1.TreesAcre));
                    rec1.AvgTreeHeight = Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTpa));
                    //rec1.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / rec1.TreesAcre));
                    rec1.QmDbh = (double)Math.Sqrt((double)rec1.BasalArea / ((double)rec1.TreesAcre * 0.005454154));

                    if (speciesColl != null)
                    {
                        string speciesStatus1 = rec1.Species + rec1.Status;
                        int iSpecies1 = ReportsBLL.GetSpeciesIdx(speciesStatus1, speciesColl);
                        List<TempPlotSpecy> tempPlotSpecies1 = ctx.TempPlotSpecies.Where(t => t.StandsId == (int)item.StandsId).ToList();

                        mbfColl.Clear();
                        xMColl.Clear();
                        d2Coll.Clear();
                        foreach (var plotSpeciesItem in tempPlotSpecies1)
                        {
                            switch (iSpecies1)
                            {
                                case 0:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1)));
                                    break;
                                case 1:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2)));
                                    break;
                                case 2:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3)));
                                    break;
                                case 3:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4)));
                                    break;
                                case 4:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5)));
                                    break;
                                case 5:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6)));
                                    break;
                                case 6:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7)));
                                    break;
                                case 7:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8)));
                                    break;
                                case 8:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9), 0));
                                    break;
                                case 9:
                                    mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10)));
                                    break;
                            }
                        }

                        rec1.Mean = mbfColl.Average();
                        foreach (var plotSpeciesItem in tempPlotSpecies1)
                        {
                            switch (iSpecies1)
                            {
                                case 0:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net1) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 1:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net2) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 2:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net3) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 3:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net4) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 4:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net5) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 5:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net6) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 6:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net7) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 7:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net8) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 8:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net9) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                                case 9:
                                    temp = (Convert.ToDouble(plotSpeciesItem.Net10) - (double)rec1.Mean) * defStat.StdDeviation;
                                    xMColl.Add(Math.Round(temp, 0));
                                    d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                    break;
                            }
                        }
                        ////double sumOfSquaresOfDifferences = (double)mbfColl.Select(val => ((val - rec.Mean)*defStat.StdDeviation) * ((val - rec.Mean)*defStat.StdDeviation)).Sum();
                        //double sumOfSquaresOfDifferences1 = d2Coll.Sum();
                        //rec1.Sd = Math.Round(Math.Sqrt(sumOfSquaresOfDifferences1 / (mbfColl.Count - 1)), 0);
                        //rec1.Se = Math.Round((double)(rec1.Sd / Math.Sqrt(mbfColl.Count)), 1);
                        //rec1.CvPercent = Math.Round((double)((rec1.Sd / rec1.Mean / defStat.StdDeviation) * 100), 1);
                        //rec1.SePercent = Math.Round((double)((rec1.Se / rec1.Mean) * 100), 1);

                        double sumOfSquaresOfDifferences1 = d2Coll.Sum();
                        double t2 = sumOfSquaresOfDifferences1 / (mbfColl.Count - 1);
                        rec1.Sd = Math.Round(Math.Sqrt(t2), 0);
                        rec1.Se = Math.Round((double)(rec1.Sd / Math.Sqrt(mbfColl.Count)), 1);
                        rec1.CvPercent = Math.Round((double)((rec1.Sd / rec1.Mean / defStat.StdDeviation) * 100), 1);
                        rec1.SePercent = Math.Round((double)((rec1.Se / rec1.Mean) * 100), 1);
                    }
                    else
                    {
                        rec1.Sd = 0;
                        rec1.Se = 0;
                        rec1.CvPercent = 0;
                        rec1.SePercent = 0;
                    }
                    ctx.RptSpeciesSummaryLogsVolumes.Add(rec1);
                    //rColl.Save();
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptSpeciesSummaryLogVolumesCombined(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands, float pAcres, float pPlots, float pTrees)
        {
            int saveId = -1;
            float tLogs = 0;
            float tMeasuredTpa = 0;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tTreesAcre = 0, tBasalArea = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tTreeHeight = 0;
            float tTotalHeight = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0;
            float tGrossBdFtVolume = 0, tGrossCuFtVolume = 0;
            float tNetMbf = 0;
            double temp = 0;
            //bool bCombine = bReportCombine;

            //if (bReportCombine)
            //{
            //    if (sColl.Count == 1)
            //        bCombine = false;
            //}
            DefStat defStat = projectContext.DefStats.First();
            List<double> mbfColl = new List<double>();
            List<double> xMColl = new List<double>();
            List<double> d2Coll = new List<double>();

            using (var ctx = new TempDbContext())
            {
                List<Tree> wColl = projectContext.Trees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).ToList();
                mbfColl.Clear();
                tLogs = 0;
                tMeasuredTpa = 0;
                currentKey = string.Empty;
                saveKey = string.Empty;
                tSpecies = string.Empty;
                tStatus = string.Empty;
                tTreesAcre = 0;
                tBasalArea = 0;
                tLogsAcre = 0;
                tTotalNetBdFt = 0;
                tTreeHeight = 0;
                tNetBdFtVolume = 0;
                tNetCuFtVolume = 0;
                tGrossBdFtVolume = 0;
                tGrossCuFtVolume = 0;
                tNetMbf = 0;

                foreach (var treeItem in wColl)
                {
                    if (string.IsNullOrEmpty(tSpecies))
                    {
                        //saveId = (int)item.StandsId;
                        tSpecies = treeItem.SpeciesAbbreviation;
                        tStatus = treeItem.TreeStatusDisplayCode;
                        saveKey = String.Format("{0}{1}", tSpecies, tStatus);
                    }
                    currentKey = String.Format("{0}{1}", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                    if (saveKey != currentKey)
                    {
                        // write record
                        RptSpeciesSummaryLogsVolume rec = new RptSpeciesSummaryLogsVolume();
                        //SepStandsSelected stand = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId);
                        //rec.StandsId = saveId;
                        //rec.StandsSelectedId = stand.StandsSelectedId;
                        //rec.TractName = stand.TractName;
                        //rec.StandName = stand.StandName;
                        rec.Descriptions = string.Empty;
                        rec.TreesAcre = tTreesAcre / pPlots; // stand.Plots;
                        rec.BasalArea = tBasalArea / pPlots; // stand.Plots;
                        rec.GrossBdFtAcre = tGrossBdFtVolume / pPlots; // stand.Plots;
                        rec.NetBdFtAcre = tNetBdFtVolume / pPlots; // stand.Plots;
                        rec.GrossCuFtAcre = tGrossCuFtVolume / pPlots; // stand.Plots;
                        rec.NetCuFtAcre = tNetCuFtVolume / pPlots; // stand.Plots;
                        rec.TotalGrossCcf = (rec.GrossCuFtAcre * pAcres) / 100;
                        rec.TotalNetCunits = (rec.NetCuFtAcre * pAcres) / 100;
                        rec.TotalGrossMbf = (rec.GrossBdFtAcre * pAcres / 1000);
                        rec.TotalNetMbf = (rec.NetBdFtAcre * pAcres / 1000);
                        rec.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
                        rec.LogsAcre = tLogsAcre / pPlots; // stand.Plots;
                        rec.TotalTons = (rec.TotalNetCunits * rec.LbsCcf) / 2000;
                        rec.Species = tSpecies;
                        rec.Status = tStatus;
                        rec.TonsAcre = rec.TotalTons / pAcres;
                        rec.TonsMbf = rec.TotalTons / rec.TotalNetMbf;
                        rec.PercentLogsAcre = 0;
                        rec.PercentNetMbf = 0;
                        rec.PercentTons = 0;
                        rec.PercentTreesAcre = 0;
                        rec.PerNetCunits = 0;
                        //rec.AvgTreeHeight = tTreeHeight / tLogs;
                        //rec.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / pPlots / rec.TreesAcre));
                        rec.AvgTreeHeight = Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTpa));
                        rec.QmDbh = (double)Math.Sqrt((double)rec.BasalArea / ((double)rec.TreesAcre * 0.005454154));
                        if (speciesColl != null)
                        {
                            string speciesStatus = rec.Species + rec.Status;
                            int iSpecies = ReportsBLL.GetSpeciesIdx(speciesStatus, speciesColl);
                            List<TempPlotSpecy> tempPlotSpecies = ctx.TempPlotSpecies.ToList();

                            mbfColl.Clear();
                            xMColl.Clear();
                            d2Coll.Clear();
                            foreach (var plotSpeciesItem in tempPlotSpecies)
                            {
                                switch (iSpecies)
                                {
                                    case 0:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1)));
                                        break;
                                    case 1:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2)));
                                        break;
                                    case 2:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3)));
                                        break;
                                    case 3:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4)));
                                        break;
                                    case 4:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5)));
                                        break;
                                    case 5:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6)));
                                        break;
                                    case 6:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7)));
                                        break;
                                    case 7:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8)));
                                        break;
                                    case 8:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9), 0));
                                        break;
                                    case 9:
                                        mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10)));
                                        break;
                                }
                            }
                            rec.Mean = mbfColl.Average();
                            foreach (var plotSpeciesItem in tempPlotSpecies)
                            {
                                switch (iSpecies)
                                {
                                    case 0:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net1) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 1:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net2) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 2:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net3) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 3:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net4) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 4:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net5) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 5:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net6) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 6:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net7) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 7:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net8) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 8:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net9) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                    case 9:
                                        temp = (Convert.ToDouble(plotSpeciesItem.Net10) - (double)rec.Mean) * defStat.StdDeviation;
                                        xMColl.Add(Math.Round(temp, 0));
                                        d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                        break;
                                }
                            }
                            //double sumOfSquaresOfDifferences = (double)mbfColl.Select(val => ((val - rec.Mean)*defStat.StdDeviation) * ((val - rec.Mean)*defStat.StdDeviation)).Sum();
                            double sumOfSquaresOfDifferences = d2Coll.Sum();
                            double t1 = sumOfSquaresOfDifferences / (mbfColl.Count - 1);
                            rec.Sd = Math.Round(Math.Sqrt(t1), 0);
                            rec.Se = Math.Round((double)(rec.Sd / Math.Sqrt(mbfColl.Count)), 1);
                            rec.CvPercent = Math.Round((double)((rec.Sd / rec.Mean / defStat.StdDeviation) * 100), 1);
                            rec.SePercent = Math.Round((double)((rec.Se / rec.Mean) * 100), 1);
                        }
                        else
                        {
                            rec.Sd = 0;
                            rec.Se = 0;
                            rec.CvPercent = 0;
                            rec.SePercent = 0;
                        }
                        ctx.RptSpeciesSummaryLogsVolumes.Add(rec);

                        // move fields
                        tSpecies = treeItem.SpeciesAbbreviation;
                        tStatus = treeItem.TreeStatusDisplayCode;
                        saveKey = String.Format("{0}{1}", tSpecies, tStatus);
                        //saveId = (int)item.StandsId;
                        tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre);
                        tBasalArea = Convert.ToSingle(treeItem.BasalArea);
                        tLogsAcre = Convert.ToSingle(treeItem.TreesPerAcre) * GetSegmentsInTree(treeItem);
                        tGrossBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tGrossCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetMbf = Convert.ToSingle(treeItem.TotalMbf);
                        //if (treeItem.TotalHeight == 0)
                        //    tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
                        //else
                        //    tTotalHeight = Convert.ToSingle(treeItem.TotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
                        tMeasuredTpa = 0;
                        if (treeItem.TreeType == "D")
                        {
                            if (treeItem.TotalHeight == 0)
                                tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                            else
                                tTotalHeight = Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                            tMeasuredTpa = Convert.ToSingle(treeItem.TreesPerAcre);
                        }
                        tLogs = 1;
                    }
                    else
                    {
                        tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre);
                        tBasalArea += Convert.ToSingle(treeItem.BasalArea);
                        tLogsAcre += Convert.ToSingle(treeItem.TreesPerAcre) * GetSegmentsInTree(treeItem);
                        //if (treeItem.TotalHeight == 0)
                        //    tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
                        //else
                        //    tTotalHeight += Convert.ToSingle(treeItem.TotalHeight * Convert.ToSingle(treeItem.TreesPerAcre;
                        if (treeItem.TreeType == "D")
                        {
                            if (treeItem.TotalHeight == 0)
                                tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                            else
                                tTotalHeight += Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre) * Convert.ToSingle(treeItem.TreeCount);
                            tMeasuredTpa += Convert.ToSingle(treeItem.TreesPerAcre);
                        }
                        tGrossBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tGrossCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtGrossVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                        tNetMbf += Convert.ToSingle(treeItem.TotalMbf);
                        tLogs += Convert.ToSingle(treeItem.TreeCount);
                    }
                }
                RptSpeciesSummaryLogsVolume rec1 = new RptSpeciesSummaryLogsVolume();
                //SepStandsSelected stand1 = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId); // ProjectBLL.GetStand(ProjectDataSource, saveId);
                //rec1.StandsId = saveId; //stand1.StandsId;
                //rec1.StandsSelectedId = stand1.StandsSelectedId;
                //rec1.TractName = stand1.TractName;
                //rec1.StandName = stand1.StandName;
                rec1.Descriptions = string.Empty;
                rec1.TreesAcre = tTreesAcre / pPlots; // stand1.Plots;
                rec1.BasalArea = tBasalArea / pPlots; // stand1.Plots;
                rec1.GrossBdFtAcre = tGrossBdFtVolume / pPlots; // stand1.Plots;
                rec1.NetBdFtAcre = tNetBdFtVolume / pPlots; // stand1.Plots;
                rec1.GrossCuFtAcre = tGrossCuFtVolume / pPlots; // stand1.Plots;
                rec1.NetCuFtAcre = tNetCuFtVolume / pPlots; // stand1.Plots;
                rec1.TotalGrossCcf = rec1.GrossCuFtAcre * pAcres / 100;
                rec1.TotalNetCunits = rec1.NetCuFtAcre * pAcres / 100;
                rec1.TotalGrossMbf = rec1.GrossBdFtAcre * pAcres / 1000;
                rec1.TotalNetMbf = rec1.NetBdFtAcre * pAcres / 1000;
                rec1.LbsCcf = Utils.ConvertToDouble(ReportsBLL.GetLbsPerCcf(ref projectContext, pProject, tSpecies, tStatus));
                rec1.LogsAcre = tLogsAcre / pPlots; // stand1.Plots;
                rec1.TotalTons = (rec1.TotalNetCunits * rec1.LbsCcf) / 2000;
                rec1.Species = tSpecies;
                rec1.Status = tStatus;
                rec1.TonsAcre = rec1.TotalTons / pAcres;
                rec1.TonsMbf = rec1.TotalTons / rec1.TotalNetMbf;
                rec1.PercentLogsAcre = 0;
                rec1.PercentNetMbf = 0;
                rec1.PercentTons = 0;
                rec1.PercentTreesAcre = 0;
                rec1.PerNetCunits = 0;
                //rec1.AvgTreeHeight = tTreeHeight / tLogs;
                //rec1.AvgTreeHeight = (short)Utils.ConvertToInt(string.Format("{0:0}", (tTotalHeight / pPlots / rec1.TreesAcre));
                rec1.AvgTreeHeight = Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTpa));
                rec1.QmDbh = (double)Math.Sqrt((double)rec1.BasalArea / ((double)rec1.TreesAcre * 0.005454154));

                if (speciesColl != null)
                {
                    string speciesStatus1 = rec1.Species + rec1.Status;
                    int iSpecies1 = ReportsBLL.GetSpeciesIdx(speciesStatus1, speciesColl);
                    List<TempPlotSpecy> tempPlotSpecies1 = ctx.TempPlotSpecies.ToList();

                    mbfColl.Clear();
                    xMColl.Clear();
                    d2Coll.Clear();
                    foreach (var plotSpeciesItem in tempPlotSpecies1)
                    {
                        switch (iSpecies1)
                        {
                            case 0:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net1)));
                                break;
                            case 1:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net2)));
                                break;
                            case 2:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net3)));
                                break;
                            case 3:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net4)));
                                break;
                            case 4:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net5)));
                                break;
                            case 5:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net6)));
                                break;
                            case 6:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net7)));
                                break;
                            case 7:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net8)));
                                break;
                            case 8:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net9), 0));
                                break;
                            case 9:
                                mbfColl.Add(Math.Round(Convert.ToDouble(plotSpeciesItem.Net10)));
                                break;
                        }
                    }

                    rec1.Mean = mbfColl.Average();
                    foreach (var plotSpeciesItem in tempPlotSpecies1)
                    {
                        switch (iSpecies1)
                        {
                            case 0:
                                temp = (plotSpeciesItem.Net1 - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 1:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net2) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 2:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net3) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 3:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net4) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 4:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net5) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 5:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net6) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 6:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net7) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 7:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net8) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 8:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net9) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                            case 9:
                                temp = (Convert.ToDouble(plotSpeciesItem.Net10) - (double)rec1.Mean) * defStat.StdDeviation;
                                xMColl.Add(Math.Round(temp, 0));
                                d2Coll.Add(Math.Round(Math.Pow(temp, 2), 0));
                                break;
                        }
                    }
                    ////double sumOfSquaresOfDifferences = (double)mbfColl.Select(val => ((val - rec.Mean)*defStat.StdDeviation) * ((val - rec.Mean)*defStat.StdDeviation)).Sum();
                    //double sumOfSquaresOfDifferences1 = d2Coll.Sum();
                    //rec1.Sd = Math.Round(Math.Sqrt(sumOfSquaresOfDifferences1 / (mbfColl.Count - 1)), 0);
                    //rec1.Se = Math.Round((double)(rec1.Sd / Math.Sqrt(mbfColl.Count)), 1);
                    //rec1.CvPercent = Math.Round((double)((rec1.Sd / rec1.Mean / defStat.StdDeviation) * 100), 1);
                    //rec1.SePercent = Math.Round((double)((rec1.Se / rec1.Mean) * 100), 1);

                    double sumOfSquaresOfDifferences1 = d2Coll.Sum();
                    double t2 = sumOfSquaresOfDifferences1 / (mbfColl.Count - 1);
                    rec1.Sd = Math.Round(Math.Sqrt(t2), 0);
                    rec1.Se = Math.Round((double)(rec1.Sd / Math.Sqrt(mbfColl.Count)), 1);
                    rec1.CvPercent = Math.Round((double)((rec1.Sd / rec1.Mean / defStat.StdDeviation) * 100), 1);
                    rec1.SePercent = Math.Round((double)((rec1.Se / rec1.Mean) * 100), 1);
                }
                else
                {
                    rec1.Sd = 0;
                    rec1.Se = 0;
                    rec1.CvPercent = 0;
                    rec1.SePercent = 0;
                }
                ctx.RptSpeciesSummaryLogsVolumes.Add(rec1);
                //rColl.Save();
                ctx.SaveChanges();
            }
        }

        public static void BuildSpeciesByPlot(ref ProjectDbContext projectContext, Project.DAL.Project currentProject, List<Stand> sColl)
        {
            DefStat defStat = projectContext.DefStats.First();

            using (var ctx = new TempDbContext())
            {
                foreach (Stand item in sColl)
                {
                    string currentKey = string.Empty;
                    string saveKey = string.Empty;
                    string tPlot = string.Empty;
                    string tSpecies = string.Empty;
                    string tStatus = string.Empty;
                    float tNetBdFtPerAcre = 0;
                    float tNetMbf = 0;
                    int idxPlot = 0;
                    int idxSpecies = 0;
                    bool bFirst = true;

                    if (plotColl != null)
                        plotColl.Clear();
                    if (speciesColl != null)
                        speciesColl.Clear();

                    List<Tree> wColl = projectContext.Trees.OrderBy(w => w.PlotNumber).ThenBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).Where(w => w.StandsId == item.StandsId).ToList();

                    plotColl = new List<string>();
                    var plotResult = (from t in projectContext.Trees
                                  where t.StandsId == item.StandsId
                                  group t by new { t.PlotNumber } into myGroup
                                  orderby myGroup.Key.PlotNumber
                                  select new { PlotNumber = myGroup.Key.PlotNumber }).ToList();
                    foreach (var plotItem in plotResult)
                        plotColl.Add(plotItem.PlotNumber);


                    speciesColl = new List<string>();
                    var speciesResult = (from t in projectContext.Trees
                                  where t.StandsId == item.StandsId
                                  group t by new { t.SpeciesAbbreviation, t.TreeStatusDisplayCode, } into myGroup
                                  orderby myGroup.Key.SpeciesAbbreviation, myGroup.Key.TreeStatusDisplayCode
                                  select new { Species = myGroup.Key.SpeciesAbbreviation, Status = myGroup.Key.TreeStatusDisplayCode }).ToList();
                    foreach (var speciesItem in speciesResult)
                        speciesColl.Add(string.Format("{0}{1}", speciesItem.Species.Trim(), speciesItem.Status.Trim()));

                    foreach (var plotItem in plotColl)
                    {
                        TempPlotSpecy rec = new TempPlotSpecy();
                        rec.StandsId = item.StandsId;
                        rec.PlotNumber = plotItem;
                        rec.NetTotal = 0;
                        if (speciesColl.Count == 1)
                            rec.SpeciesStatus1 = speciesColl[0];
                        else
                        {
                            rec.SpeciesStatus2 = string.Empty;
                            rec.SpeciesStatus3 = string.Empty;
                            rec.SpeciesStatus4 = string.Empty;
                            rec.SpeciesStatus5 = string.Empty;
                            rec.SpeciesStatus6 = string.Empty;
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net1 = 0;
                        if (speciesColl.Count == 2)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                        }
                        else
                        {
                            rec.SpeciesStatus3 = string.Empty;
                            rec.SpeciesStatus4 = string.Empty;
                            rec.SpeciesStatus5 = string.Empty;
                            rec.SpeciesStatus6 = string.Empty;
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net2 = 0;
                        if (speciesColl.Count == 3)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                        }
                        else
                        {
                            rec.SpeciesStatus4 = string.Empty;
                            rec.SpeciesStatus5 = string.Empty;
                            rec.SpeciesStatus6 = string.Empty;
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net3 = 0;
                        if (speciesColl.Count == 4)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                        }
                        else
                        {
                            rec.SpeciesStatus5 = string.Empty;
                            rec.SpeciesStatus6 = string.Empty;
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net4 = 0;
                        if (speciesColl.Count == 5)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                        }
                        else
                        {
                            rec.SpeciesStatus6 = string.Empty;
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net5 = 0;
                        if (speciesColl.Count == 6)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                            rec.SpeciesStatus6 = speciesColl[5];
                        }
                        else
                        {
                            rec.SpeciesStatus7 = string.Empty;
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net6 = 0;
                        if (speciesColl.Count == 7)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                            rec.SpeciesStatus6 = speciesColl[5];
                            rec.SpeciesStatus7 = speciesColl[6];
                        }
                        else
                        {
                            rec.SpeciesStatus8 = string.Empty;
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net7 = 0;
                        if (speciesColl.Count == 8)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                            rec.SpeciesStatus6 = speciesColl[5];
                            rec.SpeciesStatus7 = speciesColl[6];
                            rec.SpeciesStatus8 = speciesColl[7];
                        }
                        else
                        {
                            rec.SpeciesStatus9 = string.Empty;
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net8 = 0;
                        if (speciesColl.Count == 9)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                            rec.SpeciesStatus6 = speciesColl[5];
                            rec.SpeciesStatus7 = speciesColl[6];
                            rec.SpeciesStatus8 = speciesColl[7];
                            rec.SpeciesStatus9 = speciesColl[8];
                        }
                        else
                        {
                            rec.SpeciesStatus10 = string.Empty;
                        }
                        rec.Net9 = 0;
                        if (speciesColl.Count == 10)
                        {
                            rec.SpeciesStatus1 = speciesColl[0];
                            rec.SpeciesStatus2 = speciesColl[1];
                            rec.SpeciesStatus3 = speciesColl[2];
                            rec.SpeciesStatus4 = speciesColl[3];
                            rec.SpeciesStatus5 = speciesColl[4];
                            rec.SpeciesStatus6 = speciesColl[5];
                            rec.SpeciesStatus7 = speciesColl[6];
                            rec.SpeciesStatus8 = speciesColl[7];
                            rec.SpeciesStatus9 = speciesColl[8];
                            rec.SpeciesStatus10 = speciesColl[9];
                        }
                        rec.Net10 = 0;

                        ctx.TempPlotSpecies.Add(rec);
                    }
                    ctx.SaveChanges();

                    foreach (var treeItem in wColl)
                    {
                        if (bFirst)
                        {
                            tPlot = treeItem.PlotNumber;
                            tSpecies = treeItem.SpeciesAbbreviation.Trim() + treeItem.TreeStatusDisplayCode.Trim();
                            saveKey = String.Format("{0}{1}", tPlot, tSpecies);
                            bFirst = false;
                        }
                        currentKey = String.Format("{0}{1}", treeItem.PlotNumber, treeItem.SpeciesAbbreviation + treeItem.TreeStatusDisplayCode);
                        if (saveKey != currentKey)
                        {
                            TempPlotSpecy rec = ctx.TempPlotSpecies.FirstOrDefault(t => t.StandsId == item.StandsId && t.PlotNumber == tPlot);
                            rec.StandsId = item.StandsId;
                            rec.PlotNumber = tPlot;
                            int x = GetSpeciesIdx(tSpecies, speciesColl);
                            switch (x)
                            {
                                case 0:
                                    rec.SpeciesStatus1 = tSpecies;
                                    rec.Net1 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 1:
                                    rec.SpeciesStatus2 = tSpecies;
                                    rec.Net2 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 2:
                                    rec.SpeciesStatus3 = tSpecies;
                                    rec.Net3 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 3:
                                    rec.SpeciesStatus4 = tSpecies;
                                    rec.Net4 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 4:
                                    rec.SpeciesStatus5 = tSpecies;
                                    rec.Net5 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 5:
                                    rec.SpeciesStatus6 = tSpecies;
                                    rec.Net6 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 6:
                                    rec.SpeciesStatus7 = tSpecies;
                                    rec.Net7 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 7:
                                    rec.SpeciesStatus8 = tSpecies;
                                    rec.Net8 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 8:
                                    rec.SpeciesStatus9 = tSpecies;
                                    rec.Net9 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                                case 9:
                                    rec.SpeciesStatus10 = tSpecies;
                                    rec.Net10 = tNetBdFtPerAcre;
                                    rec.NetTotal += tNetBdFtPerAcre;
                                    break;
                            }

                            // move fields
                            tPlot = treeItem.PlotNumber;
                            tSpecies = treeItem.SpeciesAbbreviation.Trim() + treeItem.TreeStatusDisplayCode.Trim();
                            saveKey = currentKey;
                            tNetBdFtPerAcre = Convert.ToSingle(treeItem.TotalNetScribnerPerAcre) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots);
                        }
                        else
                        {
                            tNetBdFtPerAcre += Convert.ToSingle(treeItem.TotalNetScribnerPerAcre) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.Plots);
                        }
                    }
                    TempPlotSpecy rec1 = ctx.TempPlotSpecies.FirstOrDefault(t => t.StandsId == item.StandsId && t.PlotNumber == tPlot);
                    rec1.StandsId = item.StandsId;
                    rec1.PlotNumber = tPlot;
                    int x1 = GetSpeciesIdx(tSpecies, speciesColl);
                    switch (x1)
                    {
                        case 0:
                            rec1.SpeciesStatus1 = tSpecies;
                            rec1.Net1 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 1:
                            rec1.SpeciesStatus2 = tSpecies;
                            rec1.Net2 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 2:
                            rec1.SpeciesStatus3 = tSpecies;
                            rec1.Net3 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 3:
                            rec1.SpeciesStatus4 = tSpecies;
                            rec1.Net4 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 4:
                            rec1.SpeciesStatus5 = tSpecies;
                            rec1.Net5 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 5:
                            rec1.SpeciesStatus6 = tSpecies;
                            rec1.Net6 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 6:
                            rec1.SpeciesStatus7 = tSpecies;
                            rec1.Net7 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 7:
                            rec1.SpeciesStatus8 = tSpecies;
                            rec1.Net8 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 8:
                            rec1.SpeciesStatus9 = tSpecies;
                            rec1.Net9 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                        case 9:
                            rec1.SpeciesStatus10 = tSpecies;
                            rec1.Net10 = tNetBdFtPerAcre;
                            rec1.NetTotal += tNetBdFtPerAcre;
                            break;
                    }

                    ctx.SaveChanges();
                }
            }
        }

        public static void BuildSpeciesByPlotCombined(ref ProjectDbContext projectContext, Project.DAL.Project currentProject, List<Stand> sColl,
            float pAcres, float pPlots, float pTrees)
        {
            DefStat defStat = projectContext.DefStats.First();

            using (var ctx = new TempDbContext())
            {
                string currentKey = string.Empty;
                string saveKey = string.Empty;
                string tPlot = string.Empty;
                string tSpecies = string.Empty;
                string tStatus = string.Empty;
                float tNetBdFtPerAcre = 0;
                float tNetMbf = 0;
                int idxPlot = 0;
                int idxSpecies = 0;
                bool bFirst = true;

                List<Tree> wColl = projectContext.Trees.OrderBy(w => w.PlotNumber).ThenBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).ToList();

                plotColl = new List<string>();
                var plotResult = (from t in projectContext.Trees
                                    group t by new { t.PlotNumber } into myGroup
                                    orderby myGroup.Key.PlotNumber
                                    select new { PlotNumber = myGroup.Key.PlotNumber }).ToList();
                foreach (var plotItem in plotResult)
                    plotColl.Add(plotItem.PlotNumber);


                speciesColl = new List<string>();
                var speciesResult = (from t in projectContext.Trees
                                        group t by new { t.SpeciesAbbreviation, t.TreeStatusDisplayCode, } into myGroup
                                        orderby myGroup.Key.SpeciesAbbreviation, myGroup.Key.TreeStatusDisplayCode
                                        select new { Species = myGroup.Key.SpeciesAbbreviation, Status = myGroup.Key.TreeStatusDisplayCode }).ToList();
                foreach (var speciesItem in speciesResult)
                    speciesColl.Add(string.Format("{0}{1}", speciesItem.Species.Trim(), speciesItem.Status.Trim()));

                foreach (var plotItem in plotColl)
                {
                    TempPlotSpecy rec = new TempPlotSpecy();
                    rec.StandsId = -1;
                    rec.PlotNumber = plotItem;
                    rec.NetTotal = 0;
                    if (speciesColl.Count == 1)
                        rec.SpeciesStatus1 = speciesColl[0];
                    else
                    {
                        rec.SpeciesStatus2 = string.Empty;
                        rec.SpeciesStatus3 = string.Empty;
                        rec.SpeciesStatus4 = string.Empty;
                        rec.SpeciesStatus5 = string.Empty;
                        rec.SpeciesStatus6 = string.Empty;
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net1 = 0;
                    if (speciesColl.Count == 2)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                    }
                    else
                    {
                        rec.SpeciesStatus3 = string.Empty;
                        rec.SpeciesStatus4 = string.Empty;
                        rec.SpeciesStatus5 = string.Empty;
                        rec.SpeciesStatus6 = string.Empty;
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net2 = 0;
                    if (speciesColl.Count == 3)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                    }
                    else
                    {
                        rec.SpeciesStatus4 = string.Empty;
                        rec.SpeciesStatus5 = string.Empty;
                        rec.SpeciesStatus6 = string.Empty;
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net3 = 0;
                    if (speciesColl.Count == 4)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                    }
                    else
                    {
                        rec.SpeciesStatus5 = string.Empty;
                        rec.SpeciesStatus6 = string.Empty;
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net4 = 0;
                    if (speciesColl.Count == 5)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                    }
                    else
                    {
                        rec.SpeciesStatus6 = string.Empty;
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net5 = 0;
                    if (speciesColl.Count == 6)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                        rec.SpeciesStatus6 = speciesColl[5];
                    }
                    else
                    {
                        rec.SpeciesStatus7 = string.Empty;
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net6 = 0;
                    if (speciesColl.Count == 7)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                        rec.SpeciesStatus6 = speciesColl[5];
                        rec.SpeciesStatus7 = speciesColl[6];
                    }
                    else
                    {
                        rec.SpeciesStatus8 = string.Empty;
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net7 = 0;
                    if (speciesColl.Count == 8)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                        rec.SpeciesStatus6 = speciesColl[5];
                        rec.SpeciesStatus7 = speciesColl[6];
                        rec.SpeciesStatus8 = speciesColl[7];
                    }
                    else
                    {
                        rec.SpeciesStatus9 = string.Empty;
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net8 = 0;
                    if (speciesColl.Count == 9)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                        rec.SpeciesStatus6 = speciesColl[5];
                        rec.SpeciesStatus7 = speciesColl[6];
                        rec.SpeciesStatus8 = speciesColl[7];
                        rec.SpeciesStatus9 = speciesColl[8];
                    }
                    else
                    {
                        rec.SpeciesStatus10 = string.Empty;
                    }
                    rec.Net9 = 0;
                    if (speciesColl.Count == 10)
                    {
                        rec.SpeciesStatus1 = speciesColl[0];
                        rec.SpeciesStatus2 = speciesColl[1];
                        rec.SpeciesStatus3 = speciesColl[2];
                        rec.SpeciesStatus4 = speciesColl[3];
                        rec.SpeciesStatus5 = speciesColl[4];
                        rec.SpeciesStatus6 = speciesColl[5];
                        rec.SpeciesStatus7 = speciesColl[6];
                        rec.SpeciesStatus8 = speciesColl[7];
                        rec.SpeciesStatus9 = speciesColl[8];
                        rec.SpeciesStatus10 = speciesColl[9];
                    }
                    rec.Net10 = 0;

                    ctx.TempPlotSpecies.Add(rec);
                }
                ctx.SaveChanges();

                foreach (var treeItem in wColl)
                {
                    if (bFirst)
                    {
                        tPlot = treeItem.PlotNumber;
                        tSpecies = treeItem.SpeciesAbbreviation.Trim() + treeItem.TreeStatusDisplayCode.Trim();
                        saveKey = String.Format("{0}{1}", tPlot, tSpecies);
                        bFirst = false;
                    }
                    currentKey = String.Format("{0}{1}", treeItem.PlotNumber, treeItem.SpeciesAbbreviation + treeItem.TreeStatusDisplayCode);
                    if (saveKey != currentKey)
                    {
                        TempPlotSpecy rec = ctx.TempPlotSpecies.FirstOrDefault(t => t.PlotNumber == tPlot);
                        rec.StandsId = -1;
                        rec.PlotNumber = tPlot;
                        int x = GetSpeciesIdx(tSpecies, speciesColl);
                        switch (x)
                        {
                            case 0:
                                rec.SpeciesStatus1 = tSpecies;
                                rec.Net1 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 1:
                                rec.SpeciesStatus2 = tSpecies;
                                rec.Net2 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 2:
                                rec.SpeciesStatus3 = tSpecies;
                                rec.Net3 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 3:
                                rec.SpeciesStatus4 = tSpecies;
                                rec.Net4 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 4:
                                rec.SpeciesStatus5 = tSpecies;
                                rec.Net5 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 5:
                                rec.SpeciesStatus6 = tSpecies;
                                rec.Net6 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 6:
                                rec.SpeciesStatus7 = tSpecies;
                                rec.Net7 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 7:
                                rec.SpeciesStatus8 = tSpecies;
                                rec.Net8 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 8:
                                rec.SpeciesStatus9 = tSpecies;
                                rec.Net9 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                            case 9:
                                rec.SpeciesStatus10 = tSpecies;
                                rec.Net10 = tNetBdFtPerAcre;
                                rec.NetTotal += tNetBdFtPerAcre;
                                break;
                        }

                        // move fields
                        tPlot = treeItem.PlotNumber;
                        tSpecies = treeItem.SpeciesAbbreviation.Trim() + treeItem.TreeStatusDisplayCode.Trim();
                        saveKey = currentKey;
                        tNetBdFtPerAcre = Convert.ToSingle(treeItem.TotalNetScribnerPerAcre) * (pAcres / pPlots);
                    }
                    else
                    {
                        tNetBdFtPerAcre += Convert.ToSingle(treeItem.TotalNetScribnerPerAcre) * (pAcres / pPlots);
                    }
                }
                TempPlotSpecy rec1 = ctx.TempPlotSpecies.FirstOrDefault(t => t.PlotNumber == tPlot);
                rec1.StandsId = -1;
                rec1.PlotNumber = tPlot;
                int x1 = GetSpeciesIdx(tSpecies, speciesColl);
                switch (x1)
                {
                    case 0:
                        rec1.SpeciesStatus1 = tSpecies;
                        rec1.Net1 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 1:
                        rec1.SpeciesStatus2 = tSpecies;
                        rec1.Net2 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 2:
                        rec1.SpeciesStatus3 = tSpecies;
                        rec1.Net3 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 3:
                        rec1.SpeciesStatus4 = tSpecies;
                        rec1.Net4 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 4:
                        rec1.SpeciesStatus5 = tSpecies;
                        rec1.Net5 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 5:
                        rec1.SpeciesStatus6 = tSpecies;
                        rec1.Net6 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 6:
                        rec1.SpeciesStatus7 = tSpecies;
                        rec1.Net7 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 7:
                        rec1.SpeciesStatus8 = tSpecies;
                        rec1.Net8 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 8:
                        rec1.SpeciesStatus9 = tSpecies;
                        rec1.Net9 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                    case 9:
                        rec1.SpeciesStatus10 = tSpecies;
                        rec1.Net10 = tNetBdFtPerAcre;
                        rec1.NetTotal += tNetBdFtPerAcre;
                        break;
                }

                ctx.SaveChanges();
            }
        }

        private static int GetPlotIdx(string tPlot, List<string> plotColl)
        {
            int result = 0;
            for (int i = 0; i < plotColl.Count; i++)
            {
                if (tPlot == plotColl[i])
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private static int GetSpeciesIdx(string tSpecies, List<string> speciesColl)
        {
            int result = 0;
            for (int i = 0; i < speciesColl.Count; i++)
            {
                if (tSpecies == speciesColl[i])
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private static string GetLbsPerCcf(ref ProjectDbContext projectContext, Project.DAL.Project pProject, string pSpecies, string tStatus)
        {
            Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, pSpecies);

            return (spc.Lbs.ToString());
        }

        private static float GetLbsPerCcfFloat(ref ProjectDbContext projectContext, Project.DAL.Project pProject, string pSpecies)
        {
            Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, pSpecies);
            return Convert.ToSingle(spc.Lbs);
        }

        public static void BuildAdjCruisePlots(ref ProjectDbContext projectContext, Project.DAL.Project pProject, bool pIsCombined)
        {
            int saveId = -1;
            float tLogs = 0;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tTreesAcre = 0, tBasalArea = 0, tLogsAcre = 0;
            float tTotalHeight = 0;
            float tBoleHeight = 0;
            float tFF = 0;
            float tNetCuFtAcre = 0;
            float tNetBdFtAcre = 0;
            float tNetCuFtVolume = 0;
            float tNetBdFtVolume = 0;
            float tNetCCF = 0;
            float tNetMBF = 0;
            float tVbarNetCuFtAcre = 0;
            float tVbarNetBdFtAcre = 0;
            Stand standMaster = null;
            //bool bCombine = bReportCombine;

            //if (bReportCombine)
            //{
            //    if (sColl.Count == 1)
            //        bCombine = false;
            //}

            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> sColl = ctx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                foreach (SepStandsSelected item in sColl)
                {
                    standMaster = projectContext.Stands.FirstOrDefault(s => s.StandsId == item.StandsId);
                    List<Tree> tColl =
                        projectContext.Trees.OrderBy(t => t.SpeciesAbbreviation)
                            .ThenBy(t => t.TreeStatusDisplayCode)
                            .Where(t => t.StandsId == item.StandsId && t.TreeType == "D")
                            .ToList();
                    foreach (Tree treeItem in tColl)
                    {
                        if (string.IsNullOrEmpty(tSpecies))
                        {
                            saveId = (int) item.RealStandsId;
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = treeItem.TreeStatusDisplayCode;
                            //if (pIsCombined)
                            //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
                            //else
                            saveKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, tSpecies,
                                tStatus);
                        }
                        //if (pIsCombined)
                        //    currentKey = String.Format("{0}{1}{2}{3}", "*", "*", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                        //else
                        currentKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName,
                            treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                        if (saveKey != currentKey)
                        {
                            // write record
                            AdjCruisePlot rec = new AdjCruisePlot();
                            SepStandsSelected stand = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId);
                            standMaster = projectContext.Stands.FirstOrDefault(s => s.StandsId == saveId);
                            rec.StandsId = saveId;
                            rec.StandsSelectedId = stand.StandsSelectedId;
                            //if (pIsCombined)
                            //{
                            //    rec.TractName = "*";
                            //    rec.StandName = "*";
                            //}
                            //else
                            //{
                            rec.TractName = stand.TractName;
                            rec.StandName = stand.StandName;
                            rec.Species = tSpecies;
                            rec.Status = tStatus;
                            //}
                            rec.TreeCount = (short)tLogs;
                            rec.TreesPerAcre = tTreesAcre;
                            rec.BasalArea = tBasalArea;
                            rec.Ff = tFF / rec.TreesPerAcre;
                            rec.LogsPerAcre = tLogsAcre;
                            rec.BoleHeight = (double)tBoleHeight / (double)rec.TreesPerAcre;
                            rec.TotalHeight = (double)tTotalHeight / (double)rec.TreesPerAcre;
                            rec.NetBdFtAcre = tNetBdFtAcre; // stand.Plots;
                            rec.NetCuFtAcre = tNetCuFtAcre; // stand.Plots;
                            rec.TotalCcf = tNetCCF;
                            rec.TotalMbf = tNetMBF;
                            rec.VBarNetCuFtAcre = rec.NetCuFtAcre / rec.BasalArea;
                            rec.VBarNetBdFtAcre = rec.NetBdFtAcre / rec.BasalArea;
                            rec.QmDbh =
                                (double) Math.Sqrt((double) rec.BasalArea / ((double) rec.TreesPerAcre * 0.005454154));

                            projectContext.AdjCruisePlots.Add(rec);

                            // move fields
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = treeItem.TreeStatusDisplayCode;
                            //if (pIsCombined)
                            //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
                            //else
                            saveKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, tSpecies,
                                tStatus);
                            saveId = (int)item.RealStandsId;
                            tFF = Convert.ToSingle(treeItem.ReportedFormFactor) * (float)0.01 * Convert.ToSingle(treeItem.TreesPerAcre);
                            tBoleHeight = Convert.ToSingle(treeItem.BoleHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tTotalHeight = Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtAcre = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tNetBdFtAcre = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tBasalArea = Convert.ToSingle(treeItem.BasalArea);
                            tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre);
                            tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre);
                            tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCCF = Convert.ToSingle( treeItem.TotalCcf);
                            tNetMBF = Convert.ToSingle(treeItem.TotalMbf);
                            tVbarNetCuFtAcre = Convert.ToSingle(treeItem.TotalNetCubicPerAcre) / Convert.ToSingle(treeItem.BasalArea);
                            tVbarNetBdFtAcre = Convert.ToSingle(treeItem.TotalNetScribnerPerAcre) / Convert.ToSingle(treeItem.BasalArea);
                            tLogs = 1;
                        }
                        else
                        {
                            tFF += Convert.ToSingle( treeItem.ReportedFormFactor) * (float)0.01 * Convert.ToSingle( treeItem.TreesPerAcre);
                            tBoleHeight += Convert.ToSingle( treeItem.BoleHeight) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tTotalHeight += Convert.ToSingle( treeItem.TotalHeight) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tNetCuFtAcre += Convert.ToSingle( treeItem.TotalCuFtNetVolume) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tNetBdFtAcre += Convert.ToSingle( treeItem.TotalBdFtNetVolume) * Convert.ToSingle( treeItem.TreesPerAcre);
                            tNetCCF += Convert.ToSingle( treeItem.TotalCcf);
                            tNetMBF += Convert.ToSingle(treeItem.TotalMbf);

                            tBasalArea += Convert.ToSingle( treeItem.BasalArea);
                            tTreesAcre += Convert.ToSingle( treeItem.TreesPerAcre);
                            tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre);
                            //tNetBdFtVolume += Convert.ToSingle( treeItem.TotalBdFtNetVolume * Convert.ToSingle( treeItem.TreesPerAcre;
                            //tNetCuFtVolume += Convert.ToSingle( treeItem.TotalCuFtNetVolume * Convert.ToSingle( treeItem.TreesPerAcre;
                            tVbarNetCuFtAcre += Convert.ToSingle( treeItem.TotalNetCubicPerAcre) / Convert.ToSingle(treeItem.BasalArea);
                            tVbarNetBdFtAcre += Convert.ToSingle( treeItem.TotalNetScribnerPerAcre) / Convert.ToSingle(treeItem.BasalArea);
                            tLogs += Convert.ToSingle( treeItem.TreeCount);
                        }
                    }
                    AdjCruisePlot rec1 = new AdjCruisePlot();
                    SepStandsSelected stand1 = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsId == saveId);
                        // ProjectBLL.GetStand(ProjectDataSource, saveId);
                    rec1.StandsId = saveId; //stand1.StandsId;
                    rec1.StandsSelectedId = stand1.StandsSelectedId;
                    //if (pIsCombined)
                    //{
                    //    rec1.StandsId = 0;
                    //    rec1.TractName = "*";
                    //    rec1.StandName = "*";
                    //}
                    //else
                    //{
                    rec1.TractName = stand1.TractName;
                    rec1.StandName = stand1.StandName;
                    rec1.Species = tSpecies;
                    rec1.Status = tStatus;
                    //}
                    rec1.TreeCount = (short)tLogs;
                    rec1.TreesPerAcre = tTreesAcre;
                    rec1.BasalArea = tBasalArea;
                    rec1.Ff = tFF / rec1.TreesPerAcre;
                    rec1.LogsPerAcre = tLogsAcre;
                    rec1.BoleHeight = tBoleHeight / rec1.TreesPerAcre;
                    rec1.TotalHeight = tTotalHeight / rec1.TreesPerAcre;
                    rec1.NetBdFtAcre = tNetBdFtAcre; // stand.Plots;
                    rec1.NetCuFtAcre = tNetCuFtAcre; // stand.Plots;
                    rec1.TotalCcf = tNetCCF;
                    rec1.TotalMbf = tNetMBF;
                    rec1.VBarNetCuFtAcre = rec1.NetCuFtAcre / rec1.BasalArea;
                    rec1.VBarNetBdFtAcre = rec1.NetBdFtAcre / rec1.BasalArea;
                    rec1.QmDbh =
                        (double) Math.Sqrt((double) rec1.BasalArea / ((double) rec1.TreesPerAcre * 0.005454154));

                    projectContext.AdjCruisePlots.Add(rec1);
                    ctx.SaveChanges();
                }
            }
        }

        public static void BuildRptTreeSegmentVolume(ref ProjectDbContext projectContext, Project.DAL.Project currentProject, List<Stand> sColl)
        {
            float tpa = 0;
            float ba = 0;
            string rDestination = string.Empty;
            string rProduct = string.Empty;

            using (var tempCtx = new TempDbContext())
            {
                List<SepStandsSelected> tColl = tempCtx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                foreach (SepStandsSelected item in tColl) // Stand item in sColl
                {
                    List<Treesegment> gColl = projectContext.Treesegments
                        .OrderBy(t => t.PlotNumber)
                        .ThenBy(t => t.TreeNumber)
                        .ThenBy(t => t.SegmentNumber)
                        .Where(t => t.StandsId == item.StandsId)
                        .ToList();
                    foreach (Treesegment segItem in gColl)
                    {
                        if (segItem.TreeCount > 0)
                        {
                            RptTreeSegmentVolume wt = new RptTreeSegmentVolume();
                            wt.copyFrom(segItem);
                            // do any conditional assignments
                            wt.Ao = (segItem.TreeType == "C") ? 0 : segItem.Ao;
                            wt.Bark = (segItem.TreeType == "C") ? 0 : segItem.Bark;
                            wt.TotalHeight = (segItem.TotalHeight != 0) ? segItem.TotalHeight : 0;
                            // trickle BasalArea from first segment to subsequent ones?
                            if (segItem.SegmentNumber == 1 || segItem.SegmentNumber == 0)
                            {
                                ba = Convert.ToSingle(segItem.TreeBasalArea);
                                tpa = Convert.ToSingle(segItem.TreeTreesPerAcre);
                            }
                            wt.TreeBasalArea = ba;
                            wt.TreeTreesPerAcre = tpa;
                            // add to collection
                            tempCtx.RptTreeSegmentVolumes.Add(wt);
                        }
                    }
                }

                try
                {
                    tempCtx.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    XtraMessageBox.Show("Error when saving changes to Temp database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // Temp ctx
        }

        public static List<RptCatalogSpeciesVolume> BuildRptCatalogSpeciesVolumes(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, List<Stand> pStands)
        {
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;

            // build list of records;
            List<ReportRecord> records = new List<ReportRecord>();
            foreach (Stand stand in pStands)
            {
                // pull some stand specific data
                Temp.DAL.SepStandsSelected sl = TempBLL.GetSepStandsSelected(stand.StandsId);
                // reset keys for the new stand
                tCurrKey = tPrevKey = string.Empty;

                // Report summarizes across Trees on basis of Tree.(Age, Species, Status)
                List<Tree> wColl = projectContext.Trees.OrderBy(w => w.Age).ThenBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).Where(w => w.StandsId == stand.StandsId).ToList();
                // loop stores
                ReportRecord rec = null;

                // loop on each tree; compute running totals
                foreach (var treeItem in wColl)
                {
                    // generate a key to distinguich when new group is triggered. Report summarizes on (Age, Species, Status)
                    tCurrKey = String.Format("{0}{1}{2}", treeItem.Age.ToString().PadLeft(3, '0'), treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                    // build a new record when these indicators change; or add to current record if they are the same
                    if (!tCurrKey.Equals(tPrevKey))
                    {
                        tPrevKey = tCurrKey;    // copy the go-forward grouping key
                                                // create a new record, init the basics, and add it to the collection
                        rec = new ReportRecord();
                        rec.PullFrom(stand);
                        rec.PullFrom(treeItem);
                        // set properties that are grouping specific
                        rec.StandsSelectedId = sl.StandsSelectedId;
                        rec.SiteIndex = rec.GetSiteIndex(stand, treeItem.SpeciesAbbreviation);
                        // add to list
                        records.Add(rec);
                    }
                    // add running values to the existing record (will be adding to zero, on the first record)
                    rec.AddRunning(treeItem);
                }
            }

            // convert into DbRecords; later on build generic record that gets sent without copying
            // if "HardWrite" flag is set, then save to db else return collection onwards to caller
            if (Utils.Setting_RptPrepHardWrite) {
                using (var ctx = new TempDbContext())
                {
                    // generic records have been setup; invoke summarizer, and copy over to report table
                    foreach (ReportRecord r in records)
                    {
                        r.Summarize();
                        // compute stocking after summarization is complete
                        r.Stocking = Dominance.GetNormality(ref projectContext, CurrentProject, r);
                        RptCatalogSpeciesVolume rpt = new RptCatalogSpeciesVolume();
                        rpt.copyFrom(r);
                        ctx.RptCatalogSpeciesVolumes.Add(rpt);
                    }
                    ctx.SaveChanges();
                    return null;
                }
            }
            // soft-in-memory collections
            List<RptCatalogSpeciesVolume> list = new List<RptCatalogSpeciesVolume>();
            foreach (ReportRecord r in records)
            {
                r.Summarize();
                // compute stocking after summarization is complete
                r.Stocking = Dominance.GetNormality(ref projectContext, CurrentProject, r);
                RptCatalogSpeciesVolume rpt = new RptCatalogSpeciesVolume();
                rpt.copyFrom(r);
                list.Add(rpt);
            }
            return list;

        }

        public static void BuildRptStandSummaryCombined(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, float pAcres, float pPlots, float pTrees)
        {
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;

            int saveId = -1;
            bool bFirst = true;
            bool bPrintedFirst = false;

            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tAge = 0;
            float tDbh = 0;
            int tFF = 0;
            float tTotalHeight = 0;
            float tStocking = 0;

            float tMeasuredTPA = 0;
            short tTrees = 0;
            float tAvgLogCuFt = 0;
            float tAvgLogBdFt = 0;
            float tBAAcre = 0;
            float tTreesAcre = 0;
            float tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tScribnerNetVolume = 0;
            float tGrossBdFtVolume = 0, tNetBdFtVolume = 0, tNetCuFtVolume = 0;
            float tGrossScribnerPerAcre = 0;
            float tNetScribnerPerAcre = 0;
            float tNetCubicPerAcre = 0;

            using (var ctx = new TempDbContext())
            {
                tCurrKey = string.Empty;
                tPrevKey = string.Empty;

                saveId = -1;
                bFirst = true;
                bPrintedFirst = false;
                tSpecies = string.Empty;
                tStatus = string.Empty;
                tAge = 0;
                tDbh = 0;
                tFF = 0;
                tTotalHeight = 0;
                tStocking = 0;
                tMeasuredTPA = 0;
                tTrees = 0;
                tAvgLogCuFt = 0;
                tAvgLogBdFt = 0;
                tBAAcre = 0;
                tTreesAcre = 0;
                tLogsAcre = 0;
                tTotalNetBdFt = 0;
                tScribnerNetVolume = 0;
                tGrossBdFtVolume = 0;
                tNetBdFtVolume = 0;
                tNetCuFtVolume = 0;
                tGrossScribnerPerAcre = 0;
                tNetScribnerPerAcre = 0;
                tNetCubicPerAcre = 0;

                List<WrkTree> wColl = ctx.WrkTrees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.Dbh).ToList();
                //Temp.DAL.SepStandsSelected sl;

                foreach (var treeItem in wColl)
                {
                    //if (treeItem.Dbh != 0)
                    //{
                        if (bFirst)
                        {
                            //saveId = (int)item.StandsId;
                            tPrevKey = String.Format("{0}{1}", treeItem.SpeciesAbbreviation, (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0));
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tDbh = (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0);
                            bFirst = false;
                        }
                        tCurrKey = String.Format("{0}{1}", treeItem.SpeciesAbbreviation, (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0));
                        if (tPrevKey != tCurrKey)
                        {
                            // write record
                            RptStandTable rec = new RptStandTable();
                            //sl = TempBLL.GetSepStandsSelected(saveId);
                            rec.StandsSelectedId = -1;
                            //rec.StandsId = item.StandsId;
                            //rec.TractName = item.TractName;
                            //rec.StandName = item.StandName;
                            rec.Species = tSpecies;
                            rec.Status = string.Empty;
                            rec.DbhClass = tDbh;
                            rec.AvgAge = tAge / tTrees;
                            //rec.AvgFf = tFF / tTrees;
                            if (tTreesAcre > 0)
                                rec.TreesPerAcre = (tTreesAcre / pPlots * pAcres / pAcres);
                            else
                                rec.TreesPerAcre = 0;
                            if (tBAAcre > 0)
                                rec.BaPerAcre = (tBAAcre / pPlots * pAcres) / pAcres;
                            else
                                rec.BaPerAcre = 0;
                            if (tFF > 0 && tMeasuredTPA > 0)
                                rec.AvgFf = tFF / tMeasuredTPA;
                            else
                                rec.AvgFf = 0;
                            if (tTotalHeight > 0 && tMeasuredTPA > 0)
                                rec.AvgTotalHt = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTPA));
                            else
                                rec.AvgTotalHt = 0;
                            rec.LogsPerAcre = (tLogsAcre / pPlots * pAcres) / pAcres;
                            if (tNetCuFtVolume > 0)
                                rec.TotalNetCunits = (tNetCuFtVolume / pPlots * pAcres) / 100;
                            else
                                rec.TotalNetCunits = 0;
                            if (tNetBdFtVolume > 0)
                                rec.TotalNetMbf = (tNetBdFtVolume / pPlots * pAcres) / 1000;
                            else
                                rec.TotalNetMbf = 0;
                            if (tGrossScribnerPerAcre > 0)
                                rec.GrossPerAcreBdFt = tGrossScribnerPerAcre / pPlots;
                            else
                                rec.GrossPerAcreBdFt = 0;
                            if (tNetScribnerPerAcre > 0)
                                rec.NetPerAcreBdFt = tNetScribnerPerAcre / pPlots;
                            else
                                rec.NetPerAcreBdFt = 0;
                            if (tNetCubicPerAcre > 0)
                                rec.NetPerAcreCuFt = tNetCubicPerAcre / pPlots;
                            else
                                rec.NetPerAcreCuFt = 0;
                            if (tGrossScribnerPerAcre > 0)
                            {
                                rec.BdFtDefectPercent = (rec.GrossPerAcreBdFt - rec.NetPerAcreBdFt) / rec.GrossPerAcreBdFt;
                                rec.BdFtDefectPercent = rec.BdFtDefectPercent * 100;
                            }
                            else
                            {
                                rec.BdFtDefectPercent = 0;
                            }
                            rec.Cr = string.Empty;
                            if (rec.BaPerAcre > 0 && rec.DbhClass > 0)
                                rec.Rd = (double)rec.BaPerAcre / Math.Pow((double)rec.DbhClass, (double)0.5);
                            else
                                rec.Rd = 0;
                            if (rec.AvgTotalHt > 0 && rec.DbhClass > 0)
                                rec.HtDia = rec.AvgTotalHt * 12 / rec.DbhClass;
                            else
                            {
                                rec.HtDia = 0;
                            }
                            rec.SampleTrees = tTrees;
                            Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, rec.Species);
                            rec.TotalNetTons = (double)(rec.TotalNetCunits * (double)spcRec.Lbs) / (double)2000;
                            rec.NetPerAcreTons = rec.TotalNetTons / pAcres;
                            if (rec.NetPerAcreBdFt > 0 && rec.BaPerAcre > 0)
                                rec.VBar = rec.NetPerAcreBdFt / rec.BaPerAcre;
                            else
                                rec.VBar = 0;
                        if (rec.NetPerAcreBdFt > 0 && rec.LogsPerAcre > 0)
                                rec.AvgLogNetBdFt = rec.NetPerAcreBdFt / rec.LogsPerAcre;
                            else
                            {
                                rec.AvgLogNetBdFt = 0;
                            }
                            if (rec.NetPerAcreCuFt > 0 && rec.LogsPerAcre > 0)
                                rec.AvgLogNetCuFt = rec.NetPerAcreCuFt / rec.LogsPerAcre;
                            else
                            {
                                rec.AvgLogNetCuFt = 0;
                            }
                            ctx.RptStandTables.Add(rec);

                            // move fields
                            //saveId = (int)item.StandsId;
                            //sl = TempBLL.GetSepStandsSelected(saveId);
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = string.Empty;
                            tFF = 0;
                            tTotalHeight = 0;
                            tLogsAcre = 0;
                            tMeasuredTPA = 0;
                            tDbh = (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0);
                            tAge = Convert.ToSingle(treeItem.Age);
                            if (treeItem.TreeType == "D")
                            {
                                tFF = (int)(Convert.ToSingle(treeItem.ReportedFormFactor) * Convert.ToSingle(treeItem.TreesPerAcre));
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                else
                                    tTotalHeight = Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                tMeasuredTPA = Convert.ToSingle(treeItem.TreesPerAcre);
                                tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre);
                            }
                            tTrees = 1;
                            tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre);
                            tBAAcre = Convert.ToSingle(treeItem.BasalArea);
                            tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossScribnerPerAcre = Convert.ToSingle(treeItem.TotalGrossScribnerPerAcre);
                            tNetScribnerPerAcre = Convert.ToSingle(treeItem.TotalNetScribnerPerAcre);
                            tNetCubicPerAcre = Convert.ToSingle(treeItem.TotalNetCubicPerAcre);
                            tPrevKey = tCurrKey;
                        }
                        else
                        {
                            tTrees++;

                            tAge += Convert.ToSingle(treeItem.Age);
                            if (treeItem.TreeType == "D")
                            {
                                tFF += (int)(Convert.ToSingle(treeItem.ReportedFormFactor) * Convert.ToSingle(treeItem.TreesPerAcre));
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                else
                                    tTotalHeight += Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                tMeasuredTPA += Convert.ToSingle(treeItem.TreesPerAcre);
                                tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre);
                            }
                            tBAAcre += Convert.ToSingle(treeItem.BasalArea);
                            tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossScribnerPerAcre += Convert.ToSingle(treeItem.TotalGrossScribnerPerAcre);
                            tNetScribnerPerAcre += Convert.ToSingle(treeItem.TotalNetScribnerPerAcre);
                            tNetCubicPerAcre += Convert.ToSingle(treeItem.TotalNetCubicPerAcre);
                        }
                    //}
                }
                // write record
                RptStandTable rec1 = new RptStandTable();
                //sl = TempBLL.GetSepStandsSelected(saveId);
                rec1.StandsSelectedId = -1;
                rec1.StandsId = -1;
                //rec1.TractName = item.TractName;
                //rec1.StandName = item.StandName;
                rec1.Species = tSpecies;
                rec1.Status = string.Empty;
                rec1.DbhClass = tDbh;
                rec1.AvgAge = tAge / tTrees;
                //rec1.AvgFf = tFF / tTrees;
                if (tTreesAcre > 0)
                    rec1.TreesPerAcre = (tTreesAcre / pPlots * pAcres / pAcres);
                else
                    rec1.TreesPerAcre = 0;
                if (tBAAcre > 0)
                    rec1.BaPerAcre = tBAAcre / pPlots * pAcres / pAcres;
                else
                    rec1.BaPerAcre = 0;
                if (tFF > 0 && tMeasuredTPA > 0)
                    rec1.AvgFf = tFF / tMeasuredTPA;
                else
                    rec1.AvgFf = 0;
                if (tTotalHeight > 0 && tMeasuredTPA > 0)
                    rec1.AvgTotalHt = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTPA));
                else
                    rec1.AvgTotalHt = 0;
                rec1.LogsPerAcre = tLogsAcre / pPlots * pAcres / pAcres;
                if (tNetCuFtVolume > 0)
                    rec1.TotalNetCunits = tNetCuFtVolume / pPlots * pAcres / 100;
                else
                    rec1.TotalNetCunits = 0;
                if (tNetBdFtVolume > 0)
                    rec1.TotalNetMbf = tNetBdFtVolume / pPlots * pAcres / 1000;
                else
                    rec1.TotalNetMbf = 0;
                if (tGrossScribnerPerAcre > 0)
                    rec1.GrossPerAcreBdFt = tGrossScribnerPerAcre / pPlots;
                else
                    rec1.GrossPerAcreBdFt = 0;
                if (tNetScribnerPerAcre > 0)
                    rec1.NetPerAcreBdFt = tNetScribnerPerAcre / pPlots;
                else
                    rec1.NetPerAcreBdFt = 0;
                if (tNetCubicPerAcre > 0)
                    rec1.NetPerAcreCuFt = tNetCubicPerAcre / pPlots;
                else
                    rec1.NetPerAcreCuFt = 0;
                if (tGrossScribnerPerAcre > 0)
                {
                    rec1.BdFtDefectPercent = (rec1.GrossPerAcreBdFt - rec1.NetPerAcreBdFt) / rec1.GrossPerAcreBdFt;
                    rec1.BdFtDefectPercent = rec1.BdFtDefectPercent * 100;
                }
                else
                {
                    rec1.BdFtDefectPercent = 0;
                }
                rec1.Cr = string.Empty;
                if (rec1.BaPerAcre > 0 && rec1.DbhClass > 0)
                    rec1.Rd = (double)rec1.BaPerAcre / Math.Pow((double)rec1.DbhClass, (double)0.5);
                else
                    rec1.Rd = 0;
                if (rec1.AvgTotalHt > 0 && rec1.DbhClass > 0)
                    rec1.HtDia = rec1.AvgTotalHt * 12 / rec1.DbhClass;
                else
                {
                    rec1.HtDia = 0;
                }
                rec1.SampleTrees = tTrees;
                Species spcRec1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, rec1.Species);
                rec1.TotalNetTons = (double)(rec1.TotalNetCunits * (double)spcRec1.Lbs) / (double)2000;
                rec1.NetPerAcreTons = rec1.TotalNetTons / pAcres;
                if (rec1.NetPerAcreBdFt > 0 && rec1.BaPerAcre > 0)
                    rec1.VBar = rec1.NetPerAcreBdFt / rec1.BaPerAcre;
                else
                    rec1.VBar = 0;
                if (rec1.NetPerAcreBdFt > 0 && rec1.LogsPerAcre > 0)
                    rec1.AvgLogNetBdFt = rec1.NetPerAcreBdFt / rec1.LogsPerAcre;
                else
                {

                    rec1.AvgLogNetBdFt = 0;
                }
                if (rec1.NetPerAcreCuFt > 0 && rec1.LogsPerAcre > 0)
                    rec1.AvgLogNetCuFt = rec1.NetPerAcreCuFt / rec1.LogsPerAcre;
                else
                    rec1.AvgLogNetCuFt = 0;

                ctx.RptStandTables.Add(rec1);

                ctx.SaveChanges();
            }
        }

        public static void BuildRptStandSummary(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, List<Stand> pStands)
        {
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;

            int saveId = -1;
            bool bFirst = true;
            bool bPrintedFirst = false;

            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tAge = 0;
            float tDbh = 0;
            int tFF = 0;
            float tTotalHeight = 0;
            float tStocking = 0;

            float tMeasuredTPA = 0;
            short tTrees = 0;
            float tAvgLogCuFt = 0;
            float tAvgLogBdFt = 0;
            float tBAAcre = 0;
            float tTreesAcre = 0;
            float tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tScribnerNetVolume = 0;
            float tGrossBdFtVolume = 0, tNetBdFtVolume = 0, tNetCuFtVolume = 0;
            float tGrossScribnerPerAcre = 0;
            float tNetScribnerPerAcre = 0;
            float tNetCubicPerAcre = 0;

            using (var ctx = new TempDbContext())
            {
                foreach (Stand item in pStands)
                {
                    tCurrKey = string.Empty;
                    tPrevKey = string.Empty;

                    saveId = -1;
                    bFirst = true;
                    bPrintedFirst = false;
                    tSpecies = string.Empty;
                    tStatus = string.Empty;
                    tAge = 0;
                    tDbh = 0;
                    tFF = 0;
                    tTotalHeight = 0;
                    tStocking = 0;
                    tMeasuredTPA = 0;
                    tTrees = 0;
                    tAvgLogCuFt = 0;
                    tAvgLogBdFt = 0;
                    tBAAcre = 0;
                    tTreesAcre = 0;
                    tLogsAcre = 0;
                    tTotalNetBdFt = 0;
                    tScribnerNetVolume = 0;
                    tGrossBdFtVolume = 0;
                    tNetBdFtVolume = 0;
                    tNetCuFtVolume = 0;
                    tGrossScribnerPerAcre = 0;
                    tNetScribnerPerAcre = 0;
                    tNetCubicPerAcre = 0;

                    List<Tree> wColl = projectContext.Trees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.Dbh).Where(w => w.StandsId == item.StandsId).ToList();
                    Temp.DAL.SepStandsSelected sl;

                    foreach (var treeItem in wColl)
                    {
                        //if (treeItem.Dbh != 0)
                        //{
                        if (bFirst)
                        {
                            saveId = (int)item.StandsId;
                            //stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            tPrevKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, treeItem.SpeciesAbbreviation, (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0));
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tDbh = (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0);
                            bFirst = false;
                        }
                        tCurrKey = String.Format("{0}{1}{2}{3}", item.TractName, item.StandName, treeItem.SpeciesAbbreviation, (float)Math.Round(Convert.ToSingle(treeItem.Dbh), 0));
                        if (tPrevKey != tCurrKey)
                        {
                            // write record
                            RptStandTable rec = new RptStandTable();
                            //stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            sl = TempBLL.GetSepStandsSelected(saveId);
                            rec.StandsSelectedId = sl.StandsSelectedId;
                            rec.StandsId = item.StandsId;
                            rec.TractName = item.TractName;
                            rec.StandName = item.StandName;
                            rec.Species = tSpecies;
                            rec.Status = string.Empty;
                            rec.DbhClass = tDbh;
                            rec.AvgAge = tAge / tTrees;
                            //rec.AvgFf = tFF / tTrees;
                            if (tTreesAcre > 0)
                                rec.TreesPerAcre = tTreesAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                            else
                                rec.TreesPerAcre = 0;
                            if (tBAAcre > 0)
                                rec.BaPerAcre = tBAAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                            else
                                rec.BaPerAcre = 0;
                            if (tFF > 0 && tMeasuredTPA > 0)
                                rec.AvgFf = tFF / tMeasuredTPA;
                            else
                                rec.AvgFf = 0;
                            if (tTotalHeight > 0 && tMeasuredTPA > 0)
                                rec.AvgTotalHt = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTPA));
                            else
                                rec.AvgTotalHt = 0;
                            rec.LogsPerAcre = tLogsAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                            if (tNetCuFtVolume > 0)
                                rec.TotalNetCunits = tNetCuFtVolume / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / 100;
                            else
                                rec.TotalNetCunits = 0;
                            if (tNetBdFtVolume > 0)
                                rec.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / 1000;
                            else
                                rec.TotalNetMbf = 0;
                            if (tGrossScribnerPerAcre > 0)
                                rec.GrossPerAcreBdFt = tGrossScribnerPerAcre / Convert.ToSingle(item.Plots);
                            else
                                rec.GrossPerAcreBdFt = 0;
                            if (tNetScribnerPerAcre > 0)
                                rec.NetPerAcreBdFt = tNetScribnerPerAcre / Convert.ToSingle(item.Plots);
                            else
                                rec.NetPerAcreBdFt = 0;
                            if (tNetCubicPerAcre > 0)
                                rec.NetPerAcreCuFt = tNetCubicPerAcre / Convert.ToSingle(item.Plots);
                            else
                                rec.NetPerAcreCuFt = 0;
                            if (tGrossScribnerPerAcre > 0)
                            {
                                rec.BdFtDefectPercent = (rec.GrossPerAcreBdFt - rec.NetPerAcreBdFt) / rec.GrossPerAcreBdFt;
                                rec.BdFtDefectPercent = rec.BdFtDefectPercent * 100;
                            }
                            else
                            {
                                rec.BdFtDefectPercent = 0;
                            }
                            rec.Cr = string.Empty;
                            if (rec.BaPerAcre > 0 && rec.DbhClass > 0)
                                rec.Rd = (double)rec.BaPerAcre / Math.Pow((double)rec.DbhClass, (double)0.5);
                            else
                                rec.Rd = 0;
                            if (rec.AvgTotalHt > 0 && rec.DbhClass > 0)
                                rec.HtDia = rec.AvgTotalHt * 12 / rec.DbhClass;
                            else
                            {
                                rec.HtDia = 0;
                            }
                            rec.SampleTrees = tTrees;
                            Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, rec.Species);
                            rec.TotalNetTons = (double)(rec.TotalNetCunits * (double)spcRec.Lbs) / (double)2000;
                            rec.NetPerAcreTons = rec.TotalNetTons / item.NetGeographicAcres;
                            if (rec.NetPerAcreBdFt > 0 && rec.BaPerAcre > 0)
                                rec.VBar = rec.NetPerAcreBdFt / rec.BaPerAcre;
                            else
                                rec.VBar = 0;
                            if (rec.NetPerAcreBdFt > 0 && rec.LogsPerAcre > 0)
                                rec.AvgLogNetBdFt = rec.NetPerAcreBdFt / rec.LogsPerAcre;
                            else
                            {
                                rec.AvgLogNetBdFt = 0;
                            }
                            if (rec.NetPerAcreCuFt > 0 && rec.LogsPerAcre > 0)
                                rec.AvgLogNetCuFt = rec.NetPerAcreCuFt / rec.LogsPerAcre;
                            else
                            {
                                rec.AvgLogNetCuFt = 0;
                            }
                            ctx.RptStandTables.Add(rec);

                            // move fields
                            saveId = (int)item.StandsId;
                            //stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            sl = TempBLL.GetSepStandsSelected(saveId);
                            tSpecies = treeItem.SpeciesAbbreviation;
                            tStatus = string.Empty;
                            tFF = 0;
                            tTotalHeight = 0;
                            tLogsAcre = 0;
                            tMeasuredTPA = 0;
                            tDbh = (float)(Math.Round(Convert.ToSingle(treeItem.Dbh), 0));
                            tAge = Convert.ToSingle(treeItem.Age);
                            if (treeItem.TreeType == "D")
                            {
                                tFF = (int)(Convert.ToSingle(treeItem.ReportedFormFactor) * Convert.ToSingle(treeItem.TreesPerAcre));
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight = Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                else
                                    tTotalHeight = Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                tMeasuredTPA = Convert.ToSingle(treeItem.TreesPerAcre);
                                tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre);
                            }
                            tTrees = 1;
                            tTreesAcre = Convert.ToSingle(treeItem.TreesPerAcre);
                            tBAAcre = Convert.ToSingle(treeItem.BasalArea);
                            tNetBdFtVolume = Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume = Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossScribnerPerAcre = Convert.ToSingle(treeItem.TotalGrossScribnerPerAcre);
                            tNetScribnerPerAcre = Convert.ToSingle(treeItem.TotalNetScribnerPerAcre);
                            tNetCubicPerAcre = Convert.ToSingle(treeItem.TotalNetCubicPerAcre);
                            tPrevKey = tCurrKey;
                        }
                        else
                        {
                            tTrees++;

                            tAge += Convert.ToSingle(treeItem.Age);
                            if (treeItem.TreeType == "D")
                            {
                                tFF += (int)(Convert.ToSingle(treeItem.ReportedFormFactor) * Convert.ToSingle(treeItem.TreesPerAcre));
                                if (treeItem.TotalHeight == 0)
                                    tTotalHeight += Convert.ToSingle(treeItem.CalcTotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                else
                                    tTotalHeight += Convert.ToSingle(treeItem.TotalHeight) * Convert.ToSingle(treeItem.TreesPerAcre);
                                tMeasuredTPA += Convert.ToSingle(treeItem.TreesPerAcre);
                                tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre);
                            }
                            tBAAcre += Convert.ToSingle(treeItem.BasalArea);
                            tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetBdFtVolume += Convert.ToSingle(treeItem.TotalBdFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume += Convert.ToSingle(treeItem.TotalCuFtNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tGrossScribnerPerAcre += Convert.ToSingle(treeItem.TotalGrossScribnerPerAcre);
                            tNetScribnerPerAcre += Convert.ToSingle(treeItem.TotalNetScribnerPerAcre);
                            tNetCubicPerAcre += Convert.ToSingle(treeItem.TotalNetCubicPerAcre);
                        }
                        //}
                    }
                    // write record
                    RptStandTable rec1 = new RptStandTable();
                    //stand = ProjectBLL.GetStand(ref projectContext, saveId);
                    sl = TempBLL.GetSepStandsSelected(saveId);
                    rec1.StandsSelectedId = sl.StandsSelectedId;
                    rec1.StandsId = item.StandsId;
                    rec1.TractName = item.TractName;
                    rec1.StandName = item.StandName;
                    rec1.Species = tSpecies;
                    rec1.Status = string.Empty;
                    rec1.DbhClass = tDbh;
                    rec1.AvgAge = tAge / tTrees;
                    //rec1.AvgFf = tFF / tTrees;
                    if (tTreesAcre > 0)
                        rec1.TreesPerAcre = tTreesAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                    else
                        rec1.TreesPerAcre = 0;
                    if (tBAAcre > 0)
                        rec1.BaPerAcre = tBAAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                    else
                        rec1.BaPerAcre = 0;
                    if (tFF > 0 && tMeasuredTPA > 0)
                        rec1.AvgFf = tFF / tMeasuredTPA;
                    else
                        rec1.AvgFf = 0;
                    if (tTotalHeight > 0 && tMeasuredTPA > 0)
                        rec1.AvgTotalHt = (short)Utils.ConvertToInt(string.Format("{0:0}", tTotalHeight / tMeasuredTPA));
                    else
                        rec1.AvgTotalHt = 0;
                    rec1.LogsPerAcre = tLogsAcre / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / Convert.ToSingle(item.NetGeographicAcres);
                    if (tNetCuFtVolume > 0)
                        rec1.TotalNetCunits = tNetCuFtVolume / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / 100;
                    else
                        rec1.TotalNetCunits = 0;
                    if (tNetBdFtVolume > 0)
                        rec1.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(item.Plots) * Convert.ToSingle(item.NetGeographicAcres) / 1000;
                    else
                        rec1.TotalNetMbf = 0;
                    if (tGrossScribnerPerAcre > 0)
                        rec1.GrossPerAcreBdFt = tGrossScribnerPerAcre / Convert.ToSingle(item.Plots);
                    else
                        rec1.GrossPerAcreBdFt = 0;
                    if (tNetScribnerPerAcre > 0)
                        rec1.NetPerAcreBdFt = tNetScribnerPerAcre / Convert.ToSingle(item.Plots);
                    else
                        rec1.NetPerAcreBdFt = 0;
                    if (tNetCubicPerAcre > 0)
                        rec1.NetPerAcreCuFt = tNetCubicPerAcre / Convert.ToSingle(item.Plots);
                    else
                        rec1.NetPerAcreCuFt = 0;
                    if (tGrossScribnerPerAcre > 0)
                    {
                        rec1.BdFtDefectPercent = (rec1.GrossPerAcreBdFt - rec1.NetPerAcreBdFt) / rec1.GrossPerAcreBdFt;
                        rec1.BdFtDefectPercent = rec1.BdFtDefectPercent * 100;
                    }
                    else
                    {
                        rec1.BdFtDefectPercent = 0;
                    }
                    rec1.Cr = string.Empty;
                    if (rec1.BaPerAcre > 0 && rec1.DbhClass > 0)
                        rec1.Rd = (double)rec1.BaPerAcre / Math.Pow((double)rec1.DbhClass, (double)0.5);
                    else
                        rec1.Rd = 0;
                    if (rec1.AvgTotalHt > 0 && rec1.DbhClass > 0)
                        rec1.HtDia = rec1.AvgTotalHt * 12 / rec1.DbhClass;
                    else
                    {
                        rec1.HtDia = 0;
                    }
                    rec1.SampleTrees = tTrees;
                    Species spcRec1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, rec1.Species);
                    rec1.TotalNetTons = (double)(rec1.TotalNetCunits * (double)spcRec1.Lbs) / (double)2000;
                    rec1.NetPerAcreTons = rec1.TotalNetTons / item.NetGeographicAcres;
                    if (rec1.NetPerAcreBdFt > 0 && rec1.BaPerAcre > 0)
                        rec1.VBar = rec1.NetPerAcreBdFt / rec1.BaPerAcre;
                    else
                        rec1.VBar = 0;
                    if (rec1.NetPerAcreBdFt > 0 && rec1.LogsPerAcre > 0)
                        rec1.AvgLogNetBdFt = rec1.NetPerAcreBdFt / rec1.LogsPerAcre;
                    else
                    {
                        rec1.AvgLogNetBdFt = 0;
                    }
                    if (rec1.NetPerAcreCuFt > 0 && rec1.LogsPerAcre > 0)
                        rec1.AvgLogNetCuFt = rec1.NetPerAcreCuFt / rec1.LogsPerAcre;
                    else
                        rec1.AvgLogNetCuFt = 0;

                    ctx.RptStandTables.Add(rec1);
                }
                ctx.SaveChanges();
            }
        }

        //public static void BuildRptStatistics(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, Stand CurrentStand)
        //{
        //    CruiseStatistics cs = new CruiseStatistics();
        //    cs.BuildRptStatistics(ref projectContext, CurrentProject, CurrentStand);
        //}

        public static void BuildRptStatistics(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject)
        {
            CruiseStatistics cs = new CruiseStatistics();
            List<SepStandsSelected> standsSelected = TempBLL.GetSepStandsSelected();
            foreach (var standItem in standsSelected)
            {
                cs.BuildRptStatistics(ref projectContext, CurrentProject, standItem);
            }
        }

        public static void BuildRptStatisticsCombinded(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, float pTotalAcres, float pTotalPlots, float pTotalTrees)
        {
            CruiseStatistics cs = new CruiseStatistics();
            //List<SepStandsSelected> standsSelected = TempBLL.GetSepStandsSelected();
            //foreach (var standItem in standsSelected)
            //{
                cs.BuildRptStatisticsCombined(ref projectContext, CurrentProject, pTotalAcres, pTotalPlots);
            //}
        }

        public static void AddPercentToRptSpeciesSummaryLogVolumes()
        {
            using (var ctx = new TempDbContext())
            {
                float totalTreesAcre = 0;
                float totalLogsAcre = 0;
                float totalNetMbf = 0;
                float totalTons = 0;
                float totalNetCuints = 0;

                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var item in coll)
                {
                    List<RptSpeciesSummaryLogsVolume> rColl = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == item.StandsSelectedId).OrderByDescending(r => r.TreesAcre).ToList();
                    totalTreesAcre = 0;
                    totalLogsAcre = 0;
                    totalNetMbf = 0;
                    totalTons = 0;
                    totalNetCuints = 0;
                    foreach (var dataItem in rColl)
	                {
		                totalTreesAcre += Convert.ToSingle(dataItem.TreesAcre);
                        totalLogsAcre += Convert.ToSingle(dataItem.LogsAcre);
                        totalNetMbf += Convert.ToSingle(dataItem.TotalNetMbf);
                        totalNetCuints += Convert.ToSingle(dataItem.TotalNetCunits);
                        totalTons += Convert.ToSingle(dataItem.TotalTons);
	                }

                    List<RptSpeciesSummaryLogsVolume> xColl = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == item.StandsSelectedId).OrderByDescending(r => r.TreesAcre).ToList();

                    foreach (RptSpeciesSummaryLogsVolume itemRpt in xColl)
                    {
                        itemRpt.PercentTreesAcre = (itemRpt.TreesAcre / totalTreesAcre) * 100;
                        itemRpt.PercentLogsAcre = (itemRpt.LogsAcre / totalLogsAcre) * 100;
                        itemRpt.PercentNetMbf = (itemRpt.TotalNetMbf / totalNetMbf) * 100;
                        itemRpt.PercentTons = (itemRpt.TotalTons / totalTons) * 100;
                        itemRpt.PerNetCunits = (itemRpt.TotalNetCunits / totalNetCuints) * 100;
                    }
                }
                
                ctx.SaveChanges();
            }
        }

        public static void AddPercentToRptSpeciesSummaryLogVolumesCombined()
        {
            using (var ctx = new TempDbContext())
            {
                float totalTreesAcre = 0;
                float totalLogsAcre = 0;
                float totalNetMbf = 0;
                float totalTons = 0;
                float totalNetCuints = 0;

                List<RptSpeciesSummaryLogsVolume> rColl = ctx.RptSpeciesSummaryLogsVolumes.OrderByDescending(r => r.TreesAcre).ToList();
                foreach (var dataItem in rColl)
                {
                    totalTreesAcre += Convert.ToSingle(dataItem.TreesAcre);
                    totalLogsAcre += Convert.ToSingle(dataItem.LogsAcre);
                    totalNetMbf += Convert.ToSingle(dataItem.TotalNetMbf);
                    totalNetCuints += Convert.ToSingle(dataItem.TotalNetCunits);
                    totalTons += Convert.ToSingle(dataItem.TotalTons);
                }

                List<RptSpeciesSummaryLogsVolume> xColl = ctx.RptSpeciesSummaryLogsVolumes.OrderByDescending(r => r.TreesAcre).ToList();

                foreach (RptSpeciesSummaryLogsVolume itemRpt in xColl)
                {
                    itemRpt.PercentTreesAcre = (itemRpt.TreesAcre / totalTreesAcre) * 100;
                    itemRpt.PercentLogsAcre = (itemRpt.LogsAcre / totalLogsAcre) * 100;
                    itemRpt.PercentNetMbf = (itemRpt.TotalNetMbf / totalNetMbf) * 100;
                    itemRpt.PercentTons = (itemRpt.TotalTons / totalTons) * 100;
                    itemRpt.PerNetCunits = (itemRpt.TotalNetCunits / totalNetCuints) * 100;
                }

                ctx.SaveChanges();
            }
        }

        public static void BuildRptPlotTreeList(ref ProjectDbContext projectContext)
        {
            //mTotalAcres = 0;
            //mPlots = 0;
            //mTrees = 0;
            using (var ctx = new TempDbContext())
            {
                //mTotalAcres = 0;
                List<SepStandsSelected> sColl = ctx.SepStandsSelecteds.OrderBy(t => t.TractName).ThenBy(t => t.StandName).ToList();
                foreach (SepStandsSelected item in sColl)
                {
                    //mTotalAcres += Convert.ToSingle(item.NetGeographicAcres;
                    //mPlots += Convert.ToSingle(item.Plots;
                    //mTrees += Convert.ToSingle(item.Trees.Count;
                    List<Tree> tColl = ProjectBLL.GetTreesForStand(ref projectContext, (int)item.StandsId);
                    foreach (Tree treeItem in tColl)
                    {
                        PlotTreeList rec1 = new PlotTreeList();
                        rec1.StandsSelectedId = item.StandsSelectedId;
                        rec1.TractName = item.TractName;
                        rec1.StandName = item.StandName;
                        rec1.AgeCode = (byte)treeItem.AgeCode;
                        if (treeItem.BoleHeight != null)
                            rec1.BoleHeight = treeItem.BoleHeight;
                        else
                            rec1.BoleHeight = 0;
                        rec1.TreeType = treeItem.TreeType;
                        rec1.CrownPosition = treeItem.CrownPositionDisplayCode;
                        rec1.CrownRatio = treeItem.CrownRatioDisplayCode;
                        rec1.Damage = treeItem.DamageDisplayCode;
                        if (treeItem.Dbh != null)
                            rec1.Dbh = treeItem.Dbh;
                        else
                        {
                            rec1.Dbh = 0;
                        }
                        if (treeItem.ReportedFormFactor != null)
                            rec1.FormFactor = (byte)treeItem.ReportedFormFactor;
                        else
                        {
                            rec1.FormFactor = 0;
                        }
                        if (treeItem.FormPoint != null)
                            rec1.FormPoint = (byte)treeItem.FormPoint;
                        else
                        {
                            rec1.FormPoint = 0;
                        }
                        rec1.PlotFactor = treeItem.PlotFactorInput;
                        rec1.PlotId = (int)treeItem.PlotId;
                        rec1.PlotNumber = treeItem.PlotNumber;
                        rec1.Species = treeItem.SpeciesAbbreviation;
                        rec1.TreeStatus = treeItem.TreeStatusDisplayCode;
                        rec1.StandsId = (int)treeItem.StandsId;
                        rec1.Tdf = treeItem.TopDiameterFractionCode;
                        if (treeItem.TotalHeight != null)
                            rec1.TotalHeight = treeItem.TotalHeight;
                        else
                        {
                            rec1.TotalHeight = 0;
                        }
                        rec1.CalcTotalHtUsedYn = treeItem.CalcTotalHtUsedYn;
                        rec1.TreeCount = (byte)treeItem.TreeCount;
                        rec1.TreeNumber = (int)treeItem.TreeNumber;
                        rec1.TreeStatus = treeItem.TreeStatusDisplayCode;
                        rec1.UserDefined = treeItem.UserDefinedDisplayCode;
                        rec1.Vigor = treeItem.VigorDisplayCode;

                        if (!string.IsNullOrEmpty(treeItem.Sort1))
                        {
                            rec1.Segment1 = 1;
                            rec1.Sort1 = treeItem.Sort1;
                            rec1.Grade1 = treeItem.Grade1;
                            rec1.Length1 = treeItem.Length1;
                            rec1.BdFtLd1 = (byte)treeItem.BdFtLd1;
                            rec1.BdFtDd1 = (byte)treeItem.BdFtDd1;
                            rec1.CuFtLd1 = (byte)treeItem.CuFtLd1;
                            rec1.CuFtDd1 = (byte)treeItem.CuFtDd1;
                            rec1.BdFtPd1 = treeItem.BdFtPd1;
                        }
                        if (!string.IsNullOrEmpty(treeItem.Sort2))
                        {
                            rec1.Segment2 = 2;
                            rec1.Sort2 = treeItem.Sort2;
                            rec1.Grade2 = treeItem.Grade2;
                            rec1.Length2 = treeItem.Length2;
                            rec1.BdFtLd2 = (byte)treeItem.BdFtLd2;
                            rec1.BdFtDd2 = (byte)treeItem.BdFtDd2;
                            rec1.CuFtLd2 = (byte)treeItem.CuFtLd2;
                            rec1.CuFtDd2 = (byte)treeItem.CuFtDd2;
                            rec1.BdFtPd2 = treeItem.BdFtPd2;
                        }
                        if (!string.IsNullOrEmpty(treeItem.Sort3))
                        {
                            rec1.Segment3 = 3;
                            rec1.Sort3 = treeItem.Sort3;
                            rec1.Grade3 = treeItem.Grade3;
                            rec1.Length3 = treeItem.Length3;
                            rec1.BdFtLd3 = (byte)treeItem.BdFtLd3;
                            rec1.BdFtDd3 = (byte)treeItem.BdFtDd3;
                            rec1.CuFtLd3 = (byte)treeItem.CuFtLd3;
                            rec1.CuFtDd3 = (byte)treeItem.CuFtDd3;
                            rec1.BdFtPd3 = treeItem.BdFtPd3;
                        }
                        ctx.PlotTreeLists.Add(rec1);

                        if (!string.IsNullOrEmpty(treeItem.Sort4))
                        {
                            PlotTreeList rec2 = new PlotTreeList();
                            rec2.StandsSelectedId = item.StandsSelectedId;
                            rec2.TractName = item.TractName;
                            rec2.StandName = item.StandName;
                            rec2.AgeCode = (byte)0;
                            rec2.BoleHeight = 0;
                            rec2.CrownPosition = string.Empty;
                            rec2.CrownRatio = string.Empty;
                            rec2.Damage = string.Empty;
                            rec2.Dbh = 0;
                            rec2.FormFactor = (byte)0;
                            rec2.FormPoint = (byte)0;
                            rec2.PlotFactor = string.Empty;
                            rec2.PlotId = (int)treeItem.PlotId;
                            rec2.PlotNumber = treeItem.PlotNumber;
                            rec2.Species = string.Empty;
                            rec2.TreeStatus = string.Empty;
                            rec2.StandsId = (int)treeItem.StandsId;
                            rec2.Tdf = string.Empty;
                            rec2.TotalHeight = 0;
                            rec2.TreeCount = (byte)0;
                            rec2.TreeNumber = (int)treeItem.TreeNumber;
                            rec2.TreeStatus = string.Empty;
                            rec2.UserDefined = string.Empty;
                            rec2.Vigor = string.Empty;
                            rec2.TreeType = string.Empty;

                            if (!string.IsNullOrEmpty(treeItem.Sort4))
                            {
                                rec2.Segment1 = 4;
                                rec2.Sort1 = treeItem.Sort4;
                                rec2.Grade1 = treeItem.Grade4;
                                rec2.Length1 = treeItem.Length4;
                                rec2.BdFtLd1 = (byte)treeItem.BdFtLd4;
                                rec2.BdFtDd1 = (byte)treeItem.BdFtDd4;
                                rec2.CuFtLd1 = (byte)treeItem.CuFtLd4;
                                rec2.CuFtDd1 = (byte)treeItem.CuFtDd4;
                                rec2.BdFtPd1 = treeItem.BdFtPd4;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort5))
                            {
                                rec2.Segment2 = 5;
                                rec2.Sort2 = treeItem.Sort5;
                                rec2.Grade2 = treeItem.Grade5;
                                rec2.Length2 = treeItem.Length5;
                                rec2.BdFtLd2 = (byte)treeItem.BdFtLd5;
                                rec2.BdFtDd2 = (byte)treeItem.BdFtDd5;
                                rec2.CuFtLd2 = (byte)treeItem.CuFtLd5;
                                rec2.CuFtDd2 = (byte)treeItem.CuFtDd5;
                                rec2.BdFtPd2 = treeItem.BdFtPd5;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort6))
                            {
                                rec2.Segment3 = 6;
                                rec2.Sort3 = treeItem.Sort6;
                                rec2.Grade3 = treeItem.Grade6;
                                rec2.Length3 = treeItem.Length6;
                                rec2.BdFtLd3 = (byte)treeItem.BdFtLd6;
                                rec2.BdFtDd3 = (byte)treeItem.BdFtDd6;
                                rec2.CuFtLd3 = (byte)treeItem.CuFtLd6;
                                rec2.CuFtDd3 = (byte)treeItem.CuFtDd6;
                                rec2.BdFtPd3 = treeItem.BdFtPd6;
                            }
                            ctx.PlotTreeLists.Add(rec2);
                        }
                        if (!string.IsNullOrEmpty(treeItem.Sort7))
                        {
                            PlotTreeList rec3 = new PlotTreeList();
                            rec3.StandsSelectedId = item.StandsSelectedId;
                            rec3.TractName = item.TractName;
                            rec3.StandName = item.StandName;
                            rec3.AgeCode = (byte)0;
                            rec3.BoleHeight = 0;
                            rec3.CrownPosition = string.Empty;
                            rec3.CrownRatio = string.Empty;
                            rec3.Damage = string.Empty;
                            rec3.Dbh = 0;
                            rec3.FormFactor = (byte)0;
                            rec3.FormPoint = (byte)0;
                            rec3.PlotFactor = string.Empty;
                            rec3.PlotId = (int)treeItem.PlotId;
                            rec3.PlotNumber = treeItem.PlotNumber;
                            rec3.Species = string.Empty;
                            rec3.TreeStatus = string.Empty;
                            rec3.StandsId = (int)treeItem.StandsId;
                            rec3.Tdf = string.Empty;
                            rec3.TotalHeight = 0;
                            rec3.TreeCount = (byte)0;
                            rec3.TreeNumber = (int)treeItem.TreeNumber;
                            rec3.TreeStatus = string.Empty;
                            rec3.UserDefined = string.Empty;
                            rec3.Vigor = string.Empty;
                            rec3.TreeType = string.Empty;

                            if (!string.IsNullOrEmpty(treeItem.Sort7))
                            {
                                rec3.Segment1 = 7;
                                rec3.Sort1 = treeItem.Sort7;
                                rec3.Grade1 = treeItem.Grade7;
                                rec3.Length1 = treeItem.Length7;
                                rec3.BdFtLd1 = (byte)treeItem.BdFtLd7;
                                rec3.BdFtDd1 = (byte)treeItem.BdFtDd7;
                                rec3.CuFtLd1 = (byte)treeItem.CuFtLd7;
                                rec3.CuFtDd1 = (byte)treeItem.CuFtDd7;
                                rec3.BdFtPd1 = treeItem.BdFtPd7;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort8))
                            {
                                rec3.Segment2 = 8;
                                rec3.Sort2 = treeItem.Sort8;
                                rec3.Grade2 = treeItem.Grade8;
                                rec3.Length2 = treeItem.Length8;
                                rec3.BdFtLd2 = (byte)treeItem.BdFtLd8;
                                rec3.BdFtDd2 = (byte)treeItem.BdFtDd8;
                                rec3.CuFtLd2 = (byte)treeItem.CuFtLd8;
                                rec3.CuFtDd2 = (byte)treeItem.CuFtDd8;
                                rec3.BdFtPd2 = treeItem.BdFtPd8;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort9))
                            {
                                rec3.Segment3 = 9;
                                rec3.Sort3 = treeItem.Sort9;
                                rec3.Grade3 = treeItem.Grade9;
                                rec3.Length3 = treeItem.Length9;
                                rec3.BdFtLd3 = (byte)treeItem.BdFtLd9;
                                rec3.BdFtDd3 = (byte)treeItem.BdFtDd9;
                                rec3.CuFtLd3 = (byte)treeItem.CuFtLd9;
                                rec3.CuFtDd3 = (byte)treeItem.CuFtDd9;
                                rec3.BdFtPd3 = treeItem.BdFtPd9;
                            }
                            ctx.PlotTreeLists.Add(rec3);
                        }
                        if (!string.IsNullOrEmpty(treeItem.Sort10))
                        {
                            PlotTreeList rec4 = new PlotTreeList();
                            rec4.StandsSelectedId = item.StandsSelectedId;
                            rec4.TractName = item.TractName;
                            rec4.StandName = item.StandName;
                            rec4.AgeCode = 0;
                            rec4.BoleHeight = 0;
                            rec4.CrownPosition = string.Empty;
                            rec4.CrownRatio = string.Empty;
                            rec4.Damage = string.Empty;
                            rec4.Dbh = 0;
                            rec4.FormFactor = (byte)0;
                            rec4.FormPoint = (byte)0;
                            rec4.PlotFactor = string.Empty;
                            rec4.PlotId = (int)treeItem.PlotId;
                            rec4.PlotNumber = treeItem.PlotNumber;
                            rec4.Species = string.Empty;
                            rec4.TreeStatus = string.Empty;
                            rec4.StandsId = (int)treeItem.StandsId;
                            rec4.Tdf = string.Empty;
                            rec4.TotalHeight = 0;
                            rec4.TreeCount = (byte)0;
                            rec4.TreeNumber = (int)treeItem.TreeNumber;
                            rec4.TreeStatus = string.Empty;
                            rec4.UserDefined = string.Empty;
                            rec4.Vigor = string.Empty;
                            rec4.TreeType = string.Empty;

                            if (!string.IsNullOrEmpty(treeItem.Sort10))
                            {
                                rec4.Segment1 = 10;
                                rec4.Sort1 = treeItem.Sort10;
                                rec4.Grade1 = treeItem.Grade10;
                                rec4.Length1 = treeItem.Length10;
                                rec4.BdFtLd1 = (byte)treeItem.BdFtLd10;
                                rec4.BdFtDd1 = (byte)treeItem.BdFtDd10;
                                rec4.CuFtLd1 = (byte)treeItem.CuFtLd10;
                                rec4.CuFtDd1 = (byte)treeItem.CuFtDd10;
                                rec4.BdFtPd1 = treeItem.BdFtPd10;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort11))
                            {
                                rec4.Segment2 = 11;
                                rec4.Sort2 = treeItem.Sort11;
                                rec4.Grade2 = treeItem.Grade11;
                                rec4.Length2 = treeItem.Length11;
                                rec4.BdFtLd2 = (byte)treeItem.BdFtLd11;
                                rec4.BdFtDd2 = (byte)treeItem.BdFtDd11;
                                rec4.CuFtLd2 = (byte)treeItem.CuFtLd11;
                                rec4.CuFtDd2 = (byte)treeItem.CuFtDd11;
                                rec4.BdFtPd2 = treeItem.BdFtPd11;
                            }
                            if (!string.IsNullOrEmpty(treeItem.Sort12))
                            {
                                rec4.Segment3 = 12;
                                rec4.Sort3 = treeItem.Sort12;
                                rec4.Grade3 = treeItem.Grade12;
                                rec4.Length3 = treeItem.Length12;
                                rec4.BdFtLd3 = (byte)treeItem.BdFtLd12;
                                rec4.BdFtDd3 = (byte)treeItem.BdFtDd12;
                                rec4.CuFtLd3 = (byte)treeItem.CuFtLd12;
                                rec4.CuFtDd3 = (byte)treeItem.CuFtDd12;
                                rec4.BdFtPd3 = treeItem.BdFtPd12;
                            }
                            ctx.PlotTreeLists.Add(rec4);
                        }
                    }
                }
                ctx.SaveChanges();

                //bRptPlotTreeList = true;
            }
        }

        public static void BuildRptPolesPiling(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject)
        {
            TempDbContext ctx = new TempDbContext();
            long saveId = -1;
            int pieces = 0;
            float top = 0;
            float butt = 0;
            float length = 0;
            float tons = 0;
            float cuft = 0;
            float bdft = 0;
            float tmpNumberOfPieces = 0;
            float tmpLinealFeet = 0;
            float tmpTotalTons = 0;
            float tmpTotalCcf = 0;
            float tmpTotalMbf = 0;
            float tmpTreesPerAcre = 0;
            string tSpecies = string.Empty;
            string tProduct = string.Empty;
            string saveKey = string.Empty;
            string currentKey = string.Empty;

            long saveStandsSelectedId = -1;
            long saveStandsId = -1;
            string saveTractName = string.Empty;
            string saveStandName = string.Empty;
            Stand standRec = null;

            List<WrkPolePiling> tColl = ctx.WrkPolePilings.OrderBy(t => t.TreesId)
                .ThenBy(t => t.SortCode)
                .ThenBy(t => t.Species)
                .ThenBy(t => t.PerLogLineal)
                .ToList();
            
            foreach (WrkPolePiling item in tColl) // Stand item in sColl
            {
                if (string.IsNullOrEmpty(tSpecies))
                {
                    standRec = ProjectBLL.GetStand(ref projectContext, (int)item.StandsId);
                    saveId = (long)item.RealStandsId;
                    tSpecies = item.Species;
                    tProduct = item.ProductName;
                    length = Utils.ConvertToFloat(item.PerLogLineal);
                    saveStandsSelectedId = (long)item.StandsSelectedId;
                    saveStandsId = (long)item.StandsId;
                    saveTractName = item.TractName;
                    saveStandName = item.StandName;
                    saveKey = string.Format("{0}{1}{2}", item.ProductName, item.Species, item.PerLogLineal);
                }
                currentKey = string.Format("{0}{1}{2}", item.ProductName, item.Species, item.PerLogLineal);
                if (saveKey != currentKey)
                {
                    // write record
                    if (tProduct == "POLES")
                    {
                        RptPole rptPole = new RptPole();
                        Pole poleRec = ProjectBLL.GetPoleRecord(ref projectContext, CurrentProject.PolesTableName,
                            tSpecies);
                        rptPole.StandsSelectedId = saveStandsSelectedId;
                        rptPole.StandsId = saveStandsId;
                        rptPole.TractName = saveTractName;
                        rptPole.StandName = saveStandName;
                        rptPole.SortGroup = "A";
                        rptPole.Species = tSpecies;
                        rptPole.Sort = tProduct;
                        rptPole.LenFeet = (double) length;
                        rptPole.Butt = (double) butt / pieces;
                        rptPole.Top = (double) top / pieces;
                        rptPole.Class = ProjectBLL.GetPoleClass(poleRec, rptPole.Top, rptPole.Butt);
                            // pClass.Replace("H", "");
                        rptPole.NumberPieces = tmpNumberOfPieces;
                        rptPole.LinealFeet = tmpLinealFeet; // rptPole.Len * rptPole.NumberPieces;
                        rptPole.TotalTons = (double) tmpTotalTons; //rptPole.AvgTon * rptPole.NumberPieces;
                        rptPole.TotalCcf = (double) tmpTotalCcf; //(rptPole.NumberPieces * rptPole.AvgCuFt) / 100;
                        rptPole.TotalMbf = (double) tmpTotalMbf; //(rptPole.AvgBdFt * 30) / 1000;
                        rptPole.AvgCuFt = rptPole.TotalCcf / rptPole.NumberPieces * 100; // cuft/pieces;
                        rptPole.AvgBdFt = rptPole.TotalMbf / rptPole.NumberPieces * 1000; // bdft/pieces;
                        rptPole.AvgTon = rptPole.TotalTons / rptPole.NumberPieces;
                        rptPole.DollarsPerMbf = ProjectBLL.GetPolePrice(ref projectContext, poleRec.PolesId,
                            rptPole.Class,
                            rptPole.LenFeet); // rptPole.TotalDollars / rptPole.TotalMbf;
                        if (rptPole.DollarsPerMbf == 0)
                        {
                            Price p = ProjectBLL.GetPrice(projectContext, CurrentProject.PriceTableName,
                                rptPole.Species, rptPole.Sort, rptPole.LenFeet, rptPole.Top);
                            if (p != null)
                            {
                                switch ((int) rptPole.LenFeet)
                                {
                                    case 40:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass40;
                                        break;
                                    case 39:
                                    case 38:
                                    case 37:
                                    case 36:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass3639;
                                        break;
                                    case 35:
                                    case 34:
                                    case 33:
                                    case 32:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass3235;
                                        break;
                                    case 31:
                                    case 30:
                                    case 29:
                                    case 28:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass2831;
                                        break;
                                    case 27:
                                    case 26:
                                    case 25:
                                    case 24:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass2427;
                                        break;
                                    case 23:
                                    case 22:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass2223;
                                        break;
                                    case 21:
                                    case 20:
                                    case 19:
                                    case 18:
                                    case 17:
                                    case 16:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass1621;
                                        break;
                                    case 15:
                                    case 14:
                                    case 13:
                                    case 12:
                                        rptPole.DollarsPerMbf = (double) p.LengthClass1215;
                                        break;
                                    default:
                                        rptPole.DollarsPerMbf = 0;
                                        break;
                                }
                            }
                        }
                        rptPole.TotalDollars = rptPole.DollarsPerMbf * rptPole.TotalMbf;
                        rptPole.DollarsPerLinealFt = rptPole.TotalDollars / rptPole.LinealFeet;
                        rptPole.DollarsPerTon = rptPole.TotalDollars / rptPole.TotalTons;
                        rptPole.DollarsPerCcf = rptPole.TotalDollars / rptPole.TotalCcf;
                        rptPole.DollarsPiece = rptPole.TotalDollars / rptPole.NumberPieces;

                        ctx.RptPoles.Add(rptPole);
                    }
                    else
                    {
                        // write record
                        RptPiling rptPiling = new RptPiling();
                        //RptPole rptPole = new RptPole();
                        Piling pilingRec = ProjectBLL.GetPilingRecord(ref projectContext, CurrentProject.PilingTableName, tSpecies);
                        rptPiling.StandsSelectedId = saveStandsSelectedId;
                        rptPiling.StandsId = saveStandsId;
                        rptPiling.TractName = saveTractName;
                        rptPiling.StandName = saveStandName;
                        rptPiling.SortGroup = "B";
                        rptPiling.Species = tSpecies;
                        rptPiling.Sort = tProduct;
                        rptPiling.Len = (double)length;
                        rptPiling.Butt = (double)butt / pieces;
                        rptPiling.Top = (double)top / pieces;
                        rptPiling.Class = ProjectBLL.GetPilingClass(ref projectContext, rptPiling.Len, rptPiling.Top, rptPiling.Butt); 
                        rptPiling.NumberPieces = tmpNumberOfPieces;
                        rptPiling.LinealFeet = tmpLinealFeet; // rptPole.Len * rptPole.NumberPieces;
                        rptPiling.TotalTons = (double)tmpTotalTons; //rptPole.AvgTon * rptPole.NumberPieces;
                        rptPiling.TotalCcf = (double)tmpTotalCcf; //(rptPole.NumberPieces * rptPole.AvgCuFt) / 100;
                        rptPiling.TotalMbf = (double)tmpTotalMbf; //(rptPole.AvgBdFt * 30) / 1000;
                        rptPiling.AvgCuFt = rptPiling.TotalCcf / rptPiling.NumberPieces * 100; // cuft/pieces;
                        rptPiling.AvgBdFt = rptPiling.TotalMbf / rptPiling.NumberPieces * 1000; // bdft/pieces;
                        rptPiling.AvgTon = rptPiling.TotalTons / rptPiling.NumberPieces;
                        rptPiling.DollarsPerMbf = ProjectBLL.GetPilingPrice(ref projectContext, rptPiling.Top, rptPiling.Butt, rptPiling.Len);
                        if (rptPiling.DollarsPerMbf == 0)
                        {
                            Price p = ProjectBLL.GetPrice(projectContext, CurrentProject.PriceTableName,
                                rptPiling.Species, rptPiling.Sort, rptPiling.Len, rptPiling.Top);
                            if (p != null)
                            {
                                switch ((int)rptPiling.Len)
                                {
                                    case 40:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass40;
                                        break;
                                    case 39:
                                    case 38:
                                    case 37:
                                    case 36:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass3639;
                                        break;
                                    case 35:
                                    case 34:
                                    case 33:
                                    case 32:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass3235;
                                        break;
                                    case 31:
                                    case 30:
                                    case 29:
                                    case 28:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass2831;
                                        break;
                                    case 27:
                                    case 26:
                                    case 25:
                                    case 24:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass2427;
                                        break;
                                    case 23:
                                    case 22:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass2223;
                                        break;
                                    case 21:
                                    case 20:
                                    case 19:
                                    case 18:
                                    case 17:
                                    case 16:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass1621;
                                        break;
                                    case 15:
                                    case 14:
                                    case 13:
                                    case 12:
                                        rptPiling.DollarsPerMbf = (double)p.LengthClass1215;
                                        break;
                                    default:
                                        rptPiling.DollarsPerMbf = 0;
                                        break;
                                }
                            }
                        }
                        rptPiling.TotalDollars = rptPiling.DollarsPerMbf * rptPiling.TotalMbf;
                        rptPiling.DollarsPerLinealFt = rptPiling.TotalDollars / rptPiling.LinealFeet;
                        rptPiling.DollarsPerTon = rptPiling.TotalDollars / rptPiling.TotalTons;
                        rptPiling.DollarsPerCunit = rptPiling.TotalDollars / rptPiling.TotalCcf;
                        rptPiling.DollarsPiece = rptPiling.TotalDollars / rptPiling.NumberPieces;

                        ctx.RptPilings.Add(rptPiling);
                    }

                    // move fields
                    tSpecies = item.Species;
                    tProduct = item.ProductName;
                    saveKey = string.Format("{0}{1}{2}", item.ProductName, item.Species, item.PerLogLineal);
                    saveId = (long)item.RealStandsId;
                    tmpTreesPerAcre = Convert.ToSingle(item.TreesPerAcre);
                    // RC: need to adjust for nPlots
                    tmpNumberOfPieces = Convert.ToSingle(item.TreesPerAcre) * Convert.ToSingle(standRec.NetGeographicAcres) / Convert.ToInt32(standRec.Plots);
                    tmpLinealFeet = Convert.ToSingle(item.TotalLineal);
                    tmpTotalTons = Convert.ToSingle( item.TotalTons);
                    tmpTotalCcf = Convert.ToSingle(item.TotalCcf);
                    tmpTotalMbf = Convert.ToSingle(item.TotalMbf);
                    top = Convert.ToSingle(item.CalcTopDia);
                    butt = Convert.ToSingle( item.CalcButtDia);
                    length = Convert.ToSingle( item.CalcLen);
                    cuft = Convert.ToSingle(item.PerLogCubicNetVolume);
                    bdft = Convert.ToSingle( item.PerLogScribnerNetVolume);
                    saveKey = currentKey;
                }
                else
                {
                    pieces++;
                    top += Convert.ToSingle(item.CalcTopDia);
                    butt += Convert.ToSingle( item.CalcButtDia);
                    //length += Convert.ToSingle(item.CalcLen;
                    cuft += Convert.ToSingle(item.PerLogCubicNetVolume);
                    bdft += Convert.ToSingle(item.PerLogScribnerNetVolume);

                    tmpTreesPerAcre += Convert.ToSingle( item.TreesPerAcre);
                    // RC: need to adjust for nPlots
                    tmpNumberOfPieces += Convert.ToSingle(item.TreesPerAcre) * Convert.ToSingle(standRec.NetGeographicAcres) / Convert.ToInt32(standRec.Plots);
                    tmpLinealFeet += Convert.ToSingle(item.TotalLineal);
                    tmpTotalTons += Convert.ToSingle(item.TotalTons);
                    tmpTotalCcf += Convert.ToSingle(item.TotalCcf);
                    tmpTotalMbf += Convert.ToSingle(item.TotalMbf);
                }
            }
            if (tProduct == "POLES")
            {
                // write record
                RptPole rptPole = new RptPole();
                Pole poleRec = ProjectBLL.GetPoleRecord(ref projectContext, CurrentProject.PolesTableName, tSpecies);
                rptPole.StandsSelectedId = saveStandsSelectedId;
                rptPole.StandsId = saveStandsId;
                rptPole.TractName = saveTractName;
                rptPole.StandName = saveStandName;
                rptPole.SortGroup = "A";
                rptPole.Species = tSpecies;
                rptPole.Sort = tProduct;
                rptPole.LenFeet = (double) length; //length / pieces;
                rptPole.Butt = (double)butt / pieces;
                rptPole.Top = (double)top / pieces;
                rptPole.Class = ProjectBLL.GetPoleClass(poleRec, rptPole.Top, rptPole.Butt); // pClass.Replace("H", "");
                // need to divide by number of plots
                rptPole.NumberPieces = tmpTreesPerAcre * Convert.ToSingle(standRec.NetGeographicAcres) / Convert.ToInt32(standRec.Plots); 
                rptPole.LinealFeet = tmpLinealFeet; // rptPole.LenFeet * rptPole.NumberPieces;
                rptPole.AvgCuFt = cuft / pieces;
                rptPole.AvgBdFt = bdft / pieces;
                rptPole.AvgTon = (rptPole.AvgCuFt * 55) / 2000;
                rptPole.TotalTons = rptPole.AvgTon * rptPole.NumberPieces;
                rptPole.TotalCcf = (rptPole.NumberPieces * rptPole.AvgCuFt) / 100;
                rptPole.TotalMbf = (rptPole.NumberPieces * rptPole.AvgBdFt) / 1000;
                rptPole.DollarsPerMbf = ProjectBLL.GetPolePrice(ref projectContext, poleRec.PolesId, rptPole.Class, length); // rptPole.TotalDollars / rptPole.TotalMbf;
                if (rptPole.DollarsPerMbf == 0)
                {
                    Price p = ProjectBLL.GetPrice(projectContext, CurrentProject.PriceTableName,
                        rptPole.Species, rptPole.Sort, rptPole.LenFeet, rptPole.Top);
                    if (p != null)
                    {
                        switch ((int)rptPole.LenFeet)
                        {
                            case 40:
                                rptPole.DollarsPerMbf = (double)p.LengthClass40;
                                break;
                            case 39:
                            case 38:
                            case 37:
                            case 36:
                                rptPole.DollarsPerMbf = (double)p.LengthClass3639;
                                break;
                            case 35:
                            case 34:
                            case 33:
                            case 32:
                                rptPole.DollarsPerMbf = (double)p.LengthClass3235;
                                break;
                            case 31:
                            case 30:
                            case 29:
                            case 28:
                                rptPole.DollarsPerMbf = (double)p.LengthClass2831;
                                break;
                            case 27:
                            case 26:
                            case 25:
                            case 24:
                                rptPole.DollarsPerMbf = (double)p.LengthClass2427;
                                break;
                            case 23:
                            case 22:
                                rptPole.DollarsPerMbf = (double)p.LengthClass2223;
                                break;
                            case 21:
                            case 20:
                            case 19:
                            case 18:
                            case 17:
                            case 16:
                                rptPole.DollarsPerMbf = (double)p.LengthClass1621;
                                break;
                            case 15:
                            case 14:
                            case 13:
                            case 12:
                                rptPole.DollarsPerMbf = (double)p.LengthClass1215;
                                break;
                            default:
                                rptPole.DollarsPerMbf = 0;
                                break;
                        }
                    }
                }
                rptPole.TotalDollars = rptPole.DollarsPerMbf * rptPole.TotalMbf;
                rptPole.DollarsPerLinealFt = rptPole.TotalDollars / rptPole.LinealFeet;
                rptPole.DollarsPerTon = rptPole.TotalDollars / rptPole.TotalTons;
                rptPole.DollarsPerCcf = rptPole.TotalDollars / rptPole.TotalCcf;
                rptPole.DollarsPiece = rptPole.TotalDollars / rptPole.NumberPieces;
                ctx.RptPoles.Add(rptPole);
            }
            else
            {
                // write record
                RptPiling rptPiling = new RptPiling();
                Piling pilingRec = ProjectBLL.GetPilingRecord(ref projectContext, CurrentProject.PilingTableName, tSpecies);
                rptPiling.StandsSelectedId = saveStandsSelectedId;
                rptPiling.StandsId = saveStandsId;
                rptPiling.TractName = saveTractName;
                rptPiling.StandName = saveStandName;
                rptPiling.SortGroup = "B";
                rptPiling.Species = tSpecies;
                rptPiling.Sort = tProduct;
                rptPiling.Len = (double)length; //length / pieces;
                rptPiling.Butt = (double)butt / pieces;
                rptPiling.Top = (double)top / pieces;
                rptPiling.Class = ProjectBLL.GetPilingClass(ref projectContext, rptPiling.Len, rptPiling.Top, rptPiling.Butt);
                // need to divide by number of plots
                rptPiling.NumberPieces = tmpTreesPerAcre * Convert.ToSingle(standRec.NetGeographicAcres) / Convert.ToInt32(standRec.Plots); 
                rptPiling.LinealFeet = tmpLinealFeet; // rptPole.LenFeet * rptPole.NumberPieces;
                rptPiling.AvgCuFt = cuft / pieces;
                rptPiling.AvgBdFt = bdft / pieces;
                rptPiling.AvgTon = (rptPiling.AvgCuFt * 55) / 2000;
                rptPiling.TotalTons = rptPiling.AvgTon * rptPiling.NumberPieces;
                rptPiling.TotalCcf = (rptPiling.NumberPieces * rptPiling.AvgCuFt) / 100;
                rptPiling.TotalMbf = (rptPiling.NumberPieces * rptPiling.AvgBdFt) / 1000;
                rptPiling.DollarsPerMbf = ProjectBLL.GetPilingPrice(ref projectContext, rptPiling.Top, rptPiling.Butt, rptPiling.Len); // rptPole.TotalDollars / rptPole.TotalMbf;
                if (rptPiling.DollarsPerMbf == 0)
                {
                    Price p = ProjectBLL.GetPrice(projectContext, CurrentProject.PriceTableName,
                        rptPiling.Species, rptPiling.Sort, rptPiling.Len, rptPiling.Top);
                    if (p != null)
                    {
                        switch ((int)rptPiling.Len)
                        {
                            case 40:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass40;
                                break;
                            case 39:
                            case 38:
                            case 37:
                            case 36:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass3639;
                                break;
                            case 35:
                            case 34:
                            case 33:
                            case 32:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass3235;
                                break;
                            case 31:
                            case 30:
                            case 29:
                            case 28:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass2831;
                                break;
                            case 27:
                            case 26:
                            case 25:
                            case 24:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass2427;
                                break;
                            case 23:
                            case 22:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass2223;
                                break;
                            case 21:
                            case 20:
                            case 19:
                            case 18:
                            case 17:
                            case 16:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass1621;
                                break;
                            case 15:
                            case 14:
                            case 13:
                            case 12:
                                rptPiling.DollarsPerMbf = (double)p.LengthClass1215;
                                break;
                            default:
                                rptPiling.DollarsPerMbf = 0;
                                break;
                        }
                    }
                }
                rptPiling.TotalDollars = rptPiling.DollarsPerMbf * rptPiling.TotalMbf;
                rptPiling.DollarsPerLinealFt = rptPiling.TotalDollars / rptPiling.LinealFeet;
                rptPiling.DollarsPerTon = rptPiling.TotalDollars / rptPiling.TotalTons;
                rptPiling.DollarsPerCunit = rptPiling.TotalDollars / rptPiling.TotalCcf;
                rptPiling.DollarsPiece = rptPiling.TotalDollars / rptPiling.NumberPieces;
                ctx.RptPilings.Add(rptPiling);
            }

            try
            {
                ctx.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                XtraMessageBox.Show(exceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Throw a new DbEntityValidationException with the improved exception message.
                //throw new DbEntityValidation, Exception(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    XtraMessageBox.Show(ex.InnerException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //XtraMessageBox.Show("Error when saving changes to Temp database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ctx.Dispose();
        }

        public static void AddRptPolePiling(ref TempDbContext ctx, SepStandsSelected pItem, string pSpecies, string pSort, int pLen, double pButt, double pTop, string pClass, int pPieces, int pAvgCuFt, int pAvgBdFt, double pDollars)
        {
            RptPole rec = new RptPole();
            rec.StandsSelectedId = pItem.StandsSelectedId;
            rec.StandsId = pItem.StandsId;
            rec.TractName = pItem.TractName;
            rec.StandName = pItem.StandName;
            if (pSort.StartsWith("Pil"))
                rec.SortGroup = "B";
            else
                rec.SortGroup = "A";
            rec.Species = pSpecies;
            rec.Sort = pSort;
            rec.LenFeet = pLen;
            rec.Butt = pButt;
            rec.Top = pTop;
            rec.Class = pClass.Replace("H", "");
            rec.NumberPieces = pPieces;
            rec.LinealFeet = rec.LenFeet * rec.NumberPieces;
            rec.AvgCuFt = pAvgCuFt;
            rec.AvgBdFt = pAvgBdFt;
            rec.AvgTon = (rec.AvgCuFt * 55) / 2000;
            rec.TotalTons = rec.AvgTon * rec.NumberPieces;
            rec.TotalCcf = (rec.AvgCuFt * rec.NumberPieces) / 100;
            rec.TotalMbf = (rec.AvgBdFt * 30) / 1000;
            rec.TotalDollars = pDollars;
            rec.DollarsPerLinealFt = rec.TotalDollars / rec.LinealFeet;
            rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
            rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCcf;
            rec.DollarsPerMbf = rec.TotalDollars / rec.TotalMbf;
            rec.DollarsPiece = rec.TotalDollars / rec.NumberPieces;
            ctx.RptPoles.Add(rec);
        }

        public static int? GetPriceForTimeberEval(ref ProjectDbContext projectContext, string p, string tSpecies, string tSort, double? pLen, double? pDia)
        {
            int? price = 0;

            switch (tSort)
            {
                case "P":
                    price = 1000; //GetPolePrice(string tSpecies, string tSort, double? pLen, double? pDia);
                    break;
                case "G":
                    price = 1000; //GetPilingPrice(string tSpecies, string tSort, double? pLen, double? pDia);
                    break;
                default:
                    price = ProjectBLL.GetPriceByCatagory(ref projectContext, p, tSpecies, tSort, pLen, pDia);
                    break;
            }
            return price;
        }

        public static void BuildRptSpeciesSortTimberEvaluation(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands)
        {
            string saveKey = string.Empty;
            string currentKey = string.Empty;
            int saveId = -1;
            long saveStandsSelected = -1;
            string tSpecies = string.Empty;
            string tProductName = string.Empty;
            string tSort = string.Empty;
            string tStatus = string.Empty;
            string tTreeType = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tLogCount = 0;
            float tCountTrees = 0;
            float tMeasuredTrees = 0;
            short tSeq = 1;
            int? price = -1;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            string tTractName = string.Empty, tStandName = string.Empty;

            using (TempDbContext ctx = new TempDbContext())
            {
                foreach (var standItem in pStands)
                {
                    List<WrkTreeSegment> wColl = ctx.WrkTreeSegments.Where(w => w.StandsId == standItem.StandsId).OrderBy(w => w.Species).ThenBy(w => w.ProductName).ToList();
                    saveKey = string.Empty;
                    currentKey = string.Empty;
                    saveId = -1;
                    saveStandsSelected = -1;
                    tSpecies = string.Empty;
                    tProductName = string.Empty;
                    tSort = string.Empty;
                    tStatus = string.Empty;
                    tTreeType = string.Empty;
                    tTreesAcre = 0;
                    tLogsAcre = 0;
                    tTotalNetBdFt = 0;
                    tDia = 0;
                    tLen = 0;
                    tLogs = 0;
                    tLogCount = 0;
                    tCountTrees = 0;
                    tMeasuredTrees = 0;
                    tSeq = 1;
                    price = -1;
                    tNetBdFtVolume = 0;
                    tNetCuFtVolume = 0;
                    tTons = 0;
                    tTractName = string.Empty;
                    tStandName = string.Empty;
                    foreach (var treeItem in wColl)
                    {
                        if (string.IsNullOrEmpty(tSpecies))
                        {
                            tTractName = treeItem.TractName;
                            tStandName = treeItem.StandName;
                            tSpecies = treeItem.Species;
                            tStatus = treeItem.TreeStatus;
                            tTreeType = treeItem.TreeType;
                            tSort = treeItem.SortCode;
                            tProductName = treeItem.ProductName;
                            saveId = (int)treeItem.StandsId;
                            saveStandsSelected = (long)treeItem.StandsSelectedId;
                            //saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
                            saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
                        }
                        //currentKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
                        currentKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
                        if (saveKey != currentKey)
                        {
                            // write record
                            RptSpeciesSortTimberEvaluation rec = new RptSpeciesSortTimberEvaluation();
                            //Stand stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            rec.StandsId = standItem.StandsId;
                            rec.StandsSelectedId = saveStandsSelected;
                            rec.TractName = tTractName;
                            rec.StandName = tStandName;
                            rec.LbsCcf = Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
                            rec.TotalCunits = tNetCuFtVolume / Convert.ToSingle(standItem.Plots) * Convert.ToSingle(standItem.NetGeographicAcres) / 100;
                            rec.TotalMbf = tNetBdFtVolume / Convert.ToSingle(standItem.Plots) * Convert.ToSingle(standItem.NetGeographicAcres) / 1000;
                            rec.TotalTons = ((rec.TotalCunits * rec.LbsCcf) / 2000); // (int)tTons; // (rec.TotalCunits * rec.LbsCcf) / 2000;
                            rec.TotalLogs = tLogCount * Convert.ToSingle(standItem.NetGeographicAcres);
                            rec.LogAveLen = tLen / tLogCount;
                            rec.LogAvgDia = tDia / tLogCount;
                            rec.MeasuredTrees = tMeasuredTrees;
                            rec.CountTrees = tCountTrees;
                            rec.SpeciesCode = tSpecies;
                            rec.SortCode = tSort;
                            price = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec.LogAveLen, rec.LogAvgDia);
                            if (price != -1 && tSort != "0")
                            {
                                tSeq++;
                                rec.Seq = (byte)tSeq;
                                rec.Sort = tProductName;
                                rec.DollarsPerMbf = price;
                                if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                    rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                                else
                                    rec.TotalDollars = 0;
                                if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                    rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                                else
                                    rec.DollarsPerLog = 0;
                                if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                    rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                                else
                                    rec.DollarsPerAcre = 0;
                                if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                    rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                                else
                                    rec.DollarsPerTon = 0;
                                if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                    rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                                else
                                    rec.DollarsPerCcf = 0;
                            }
                            else
                            {
                                if (tProductName == "COUNT") // was tSort
                                {
                                    rec.Seq = 1;
                                    rec.Sort = tProductName;
                                    rec.LogAveLen = 0;
                                    rec.LogAvgDia = 0;
                                    rec.DollarsPerMbf = 0;
                                    if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                        rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                                    else
                                        rec.TotalDollars = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                        rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                                    else
                                        rec.DollarsPerLog = 0;
                                    if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                        rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                                    else
                                        rec.DollarsPerAcre = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                        rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                                    else
                                        rec.DollarsPerTon = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                        rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                                    else
                                        rec.DollarsPerCcf = 0;
                                }
                                else
                                if (tProductName == "UTILITY") // was tSort
                                {
                                    rec.Seq = 98;
                                    rec.Sort = tProductName;
                                    rec.LogAveLen = 0;
                                    rec.LogAvgDia = 0;
                                    rec.DollarsPerMbf = 0;
                                    if (rec.TotalMbf > 0 && rec.DollarsPerMbf > 0)
                                        rec.TotalDollars = rec.DollarsPerMbf * rec.TotalMbf;
                                    else
                                        rec.TotalDollars = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalLogs > 0)
                                        rec.DollarsPerLog = rec.TotalDollars / rec.TotalLogs;
                                    else
                                        rec.DollarsPerLog = 0;
                                    if (rec.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                        rec.DollarsPerAcre = rec.TotalDollars / standItem.NetGeographicAcres;
                                    else
                                        rec.DollarsPerAcre = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalTons > 0)
                                        rec.DollarsPerTon = rec.TotalDollars / rec.TotalTons;
                                    else
                                        rec.DollarsPerTon = 0;
                                    if (rec.TotalDollars > 0 && rec.TotalCunits > 0)
                                        rec.DollarsPerCcf = rec.TotalDollars / rec.TotalCunits;
                                    else
                                        rec.DollarsPerCcf = 0;
                                }
                                else
                                {
                                    if (tProductName == "CULL")
                                    {
                                        rec.Seq = 99;
                                        rec.Sort = "CULL";
                                        rec.TotalDollars = 0;
                                        rec.DollarsPerLog = 0;
                                        rec.DollarsPerAcre = 0;
                                        rec.DollarsPerTon = 0;
                                        rec.DollarsPerCcf = 0;
                                        rec.DollarsPerMbf = 0;
                                        rec.LogAveLen = 0;
                                        rec.LogAvgDia = 0;
                                    }
                                }
                            }

                            Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
                            rec.Species = spc.Description;

                            ctx.RptSpeciesSortTimberEvaluations.Add(rec);

                            // move fields
                            tSeq = 1;
                            tLogs = 0;
                            tCountTrees = 0;
                            tMeasuredTrees = 0;
                            tLogCount = 0;
                            tLen = 0;
                            tDia = 0;
                            tNetBdFtVolume = 0;
                            tNetCuFtVolume = 0;
                            tTons = 0;
                            tTractName = treeItem.TractName;
                            tStandName = treeItem.StandName;
                            tSpecies = treeItem.Species;
                            tSort = treeItem.SortCode;
                            tStatus = treeItem.TreeStatus;
                            tTreeType = treeItem.TreeType;
                            tProductName = treeItem.ProductName;
                            saveId = (int)treeItem.StandsId;
                            saveStandsSelected = (long)treeItem.StandsSelectedId;
                            //saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.TreeType.Trim() + treeItem.ProductName.Trim();
                            saveKey = treeItem.TractName.Trim() + treeItem.StandName.Trim() + treeItem.Species.Trim() + treeItem.TreeStatus.Trim() + treeItem.ProductName.Trim();
                            if (treeItem.TreeType != "C")
                            {
                                tLogCount++;
                                tMeasuredTrees++;
                                //tLogs = Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(standItem.NetGeographicAcres;
                                tLen = Convert.ToSingle(treeItem.CalcLen); // was treeItem.CalcLen
                                tDia = Convert.ToSingle(treeItem.CalcTopDia); // was treeItem.CalcTopDia
                                //tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
                                //tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
                                //tTons = Convert.ToSingle(treeItem.TotalTons;
                            }
                            else
                            {
                                tLogs = 0;
                                tLen = 0;
                                tDia = 0;
                                tCountTrees++;
                            }
                            tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tTons = Convert.ToSingle(treeItem.TotalTons);
                            if (rec.Sort == "POLES")
                                rec.TotalLogs--;
                        }
                        else
                        {
                            if (treeItem.TreeType != "C")
                            {
                                //tLogs++;
                                tLogCount++;
                                tMeasuredTrees++;
                                Stand stand = ProjectBLL.GetStand(ref projectContext, (int)treeItem.StandsId);
                                //tLogs += Convert.ToSingle(treeItem.TreeCount * Convert.ToSingle(stand.NetGeographicAcres;
                                tLen += Convert.ToSingle(treeItem.CalcLen); // was treeItem.CalcLen
                                tDia += Convert.ToSingle(treeItem.CalcTopDia); // was treeItem.CalcTopDia
                                //tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
                                //tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
                                //tTons = Convert.ToSingle(treeItem.TotalTons;
                            }
                            else
                                tCountTrees++;
                            tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolume) * Convert.ToSingle(treeItem.TreesPerAcre);
                            tTons = Convert.ToSingle(treeItem.TotalTons);
                        }
                    }
                    if (wColl.Count > 0)
                    {
                        RptSpeciesSortTimberEvaluation rec1 = new RptSpeciesSortTimberEvaluation();
                        //Stand stand1 = ProjectBLL.GetStand(ref projectContext, saveId);
                        rec1.StandsId = standItem.StandsId;
                        rec1.StandsSelectedId = saveStandsSelected;
                        rec1.TractName = tTractName;
                        rec1.StandName = tStandName;
                        rec1.LbsCcf = Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""));
                        rec1.TotalCunits = tNetCuFtVolume / Convert.ToSingle(standItem.Plots) * Convert.ToSingle(standItem.NetGeographicAcres) / 100;
                        rec1.TotalMbf = tNetBdFtVolume / Convert.ToSingle(standItem.Plots) * Convert.ToSingle(standItem.NetGeographicAcres) / 1000;
                        rec1.TotalTons = ((rec1.TotalCunits * rec1.LbsCcf) / 2000); // (int)tTons; // (rec.TotalCunits * rec.LbsCcf) / 2000;
                        rec1.TotalLogs = tLogCount * Convert.ToSingle(standItem.NetGeographicAcres);
                        rec1.LogAveLen = tLen / tLogCount;
                        rec1.LogAvgDia = tDia / tLogCount;
                        rec1.MeasuredTrees = tMeasuredTrees;
                        rec1.CountTrees = tCountTrees;
                        rec1.SpeciesCode = tSpecies;
                        rec1.SortCode = tSort;
                        //int? price1 = ProjectBLL.GetPriceByCatagory(ProjectDataSource, CurrentProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
                        int? price1 = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, rec1.LogAveLen, rec1.LogAvgDia);
                        if (price1 != -1 && tSort != "0")
                        {
                            tSeq++;
                            rec1.Seq = (byte)tSeq;
                            rec1.Sort = tProductName;
                            rec1.DollarsPerMbf = price;
                            if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                                rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                            else
                                rec1.TotalDollars = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                                rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                            else
                                rec1.DollarsPerLog = 0;
                            if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                            else
                                rec1.DollarsPerAcre = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                                rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                            else
                                rec1.DollarsPerTon = 0;
                            if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                                rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                            else
                                rec1.DollarsPerCcf = 0;
                        }
                        else
                        {
                            if (tProductName == "COUNT") // was tSort
                            {
                                rec1.Seq = 1;
                                rec1.Sort = tProductName;
                                rec1.LogAveLen = 0;
                                rec1.LogAvgDia = 0;
                                rec1.DollarsPerMbf = 0;
                                if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                                    rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                                else
                                    rec1.TotalDollars = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                                    rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                                else
                                    rec1.DollarsPerLog = 0;
                                if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                    rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                                else
                                    rec1.DollarsPerAcre = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                                    rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                                else
                                    rec1.DollarsPerTon = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                                    rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                                else
                                    rec1.DollarsPerCcf = 0;
                            }
                            else
                            if (tProductName == "UTILITY") // was tSort
                            {
                                rec1.Seq = 98;
                                rec1.Sort = tProductName;
                                rec1.LogAveLen = 0;
                                rec1.LogAvgDia = 0;
                                rec1.DollarsPerMbf = 0;
                                if (rec1.TotalMbf > 0 && rec1.DollarsPerMbf > 0)
                                    rec1.TotalDollars = rec1.DollarsPerMbf * rec1.TotalMbf;
                                else
                                    rec1.TotalDollars = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalLogs > 0)
                                    rec1.DollarsPerLog = rec1.TotalDollars / rec1.TotalLogs;
                                else
                                    rec1.DollarsPerLog = 0;
                                if (rec1.TotalDollars > 0 && standItem.NetGeographicAcres > 0)
                                    rec1.DollarsPerAcre = rec1.TotalDollars / standItem.NetGeographicAcres;
                                else
                                    rec1.DollarsPerAcre = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalTons > 0)
                                    rec1.DollarsPerTon = rec1.TotalDollars / rec1.TotalTons;
                                else
                                    rec1.DollarsPerTon = 0;
                                if (rec1.TotalDollars > 0 && rec1.TotalCunits > 0)
                                    rec1.DollarsPerCcf = rec1.TotalDollars / rec1.TotalCunits;
                                else
                                    rec1.DollarsPerCcf = 0;
                            }
                            else
                            {
                                if (tProductName == "CULL")
                                {
                                    rec1.Seq = 99;
                                    rec1.Sort = "CULL";
                                    rec1.TotalDollars = 0;
                                    rec1.DollarsPerLog = 0;
                                    rec1.DollarsPerAcre = 0;
                                    rec1.DollarsPerTon = 0;
                                    rec1.DollarsPerCcf = 0;
                                    rec1.DollarsPerMbf = 0;
                                    rec1.LogAveLen = 0;
                                    rec1.LogAvgDia = 0;
                                }
                            }
                        }
                        Species spc1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);
                        rec1.Species = spc1.Description;
                        if (rec1.Sort == "POLES")
                            rec1.TotalLogs--;
                        ctx.RptSpeciesSortTimberEvaluations.Add(rec1);
                    }
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptCostByLineItem(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands)
        {
            int saveId = -1;
            bool hasFall = false;
            bool hasRoad = false;
            string tSpecies = string.Empty;
            string tProductName = string.Empty;
            string tSort = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            bool yardUsed = false;
            bool haulingUsed = false;
            string saveCostType = string.Empty;

            using (TempDbContext ctx = new TempDbContext())
            {
                foreach (var standItem in pStands)
                {
                    saveId = -1;
                    hasFall = false;
                    hasRoad = false;
                    tSpecies = string.Empty;
                    tProductName = string.Empty;
                    tSort = string.Empty;
                    tTreesAcre = 0;
                    tLogsAcre = 0;
                    tTotalNetBdFt = 0;
                    tDia = 0;
                    tLen = 0;
                    tLogs = 0;
                    tNetBdFtVolume = 0;
                    tNetCuFtVolume = 0;
                    tTons = 0;
                    yardUsed = false;
                    haulingUsed = false;
                    saveCostType = string.Empty;

                    RptSpeciesSortTimberEvalTotal spcTotal = TempBLL.GetRevenueBySpeciesTotals(standItem.StandsId);
                    List<Cost> fixedColl = ProjectBLL.GetCostList(ref projectContext, pProject.CostTableName, "FIXED");
                    foreach (var fixedItem in fixedColl)
                    {
                        RptCostByLineItem recFixed = new RptCostByLineItem();
                        //if (saveCostType != fixedItem.CostType)
                        //{
                        //    recFixed.CostType = fixedItem.CostType;
                        //    saveCostType = fixedItem.CostType;
                        //}
                        //else
                        //{
                        //    recFixed.CostType = string.Empty;
                        //}
                        recFixed.CostType = fixedItem.CostType;
                        recFixed.CostTypeOrder = 1;
                        recFixed.StandsSelectedId = standItem.StandsId;
                        recFixed.CostName = fixedItem.CostDescription;
                        recFixed.PrintOrder = (short)fixedItem.ReportPrintOrder;
                        recFixed.Species = fixedItem.Species;
                        AssignCostItemValues(ref recFixed, fixedItem, spcTotal, standItem);
                        //recFixed.PerAcre = (double)(fixedItem.CostAmount / pStands[0].NetGeographicAcres);
                        //recFixed.PerCcf = (double)(fixedItem.CostAmount / spcTotal.TotalCunits);
                        //recFixed.PerLog = (double)(fixedItem.CostAmount / spcTotal.TotalLogs);
                        //recFixed.PerMbf = (double)(fixedItem.CostAmount / spcTotal.TotalMbf);
                        //recFixed.PerTon = (double)(fixedItem.CostAmount / spcTotal.TotalTons);
                        //recFixed.TotalDollars = (double)fixedItem.CostAmount;
                        ctx.RptCostByLineItems.Add(recFixed);
                    }

                    List<Cost> annualColl = ProjectBLL.GetCostList(ref projectContext, pProject.CostTableName, "ANNUAL");
                    foreach (var annualItem in annualColl)
                    {
                        RptCostByLineItem recAnnual = new RptCostByLineItem();
                        //if (saveCostType != annualItem.CostType)
                        //{
                        //    recAnuall.CostType = annualItem.CostType;
                        //    saveCostType = annualItem.CostType;
                        //}
                        //else
                        //{
                        //    recAnuall.CostType = string.Empty;
                        //}
                        recAnnual.CostType = annualItem.CostType;
                        recAnnual.CostTypeOrder = 3;
                        recAnnual.StandsSelectedId = standItem.StandsId;
                        recAnnual.CostName = annualItem.CostDescription;
                        recAnnual.PrintOrder = (short)annualItem.ReportPrintOrder;
                        recAnnual.Species = annualItem.Species;
                        AssignCostItemValues(ref recAnnual, annualItem, spcTotal, standItem);
                        //recAnuall.PerAcre = 0;
                        //recAnuall.PerCcf = 0;
                        //recAnuall.PerLog = 0;
                        //recAnuall.PerMbf = 0;
                        //recAnuall.PerTon = 0;
                        //recAnuall.TotalDollars = 0;
                        ctx.RptCostByLineItems.Add(recAnnual);
                    }

                    List<Cost> varColl = ProjectBLL.GetCostList(ref projectContext, pProject.CostTableName, "VARIABLE");
                    foreach (var varItem in varColl)
                    {
                        RptCostByLineItem recVar = new RptCostByLineItem();
                        //if (saveCostType != varItem.CostType)
                        //{
                        //    recVar.CostType = varItem.CostType;
                        //    saveCostType = varItem.CostType;
                        //}
                        //else
                        //{
                        //    recVar.CostType = string.Empty;
                        //}
                        recVar.CostType = varItem.CostType;
                        recVar.CostTypeOrder = 2;
                        recVar.StandsSelectedId = standItem.StandsId;
                        recVar.CostName = varItem.CostDescription;
                        recVar.PrintOrder = (short)varItem.ReportPrintOrder;
                        recVar.Species = varItem.Species;
                        AssignCostItemValues(ref recVar, varItem, spcTotal, standItem);
                        //recVar.PerAcre = 0;
                        //recVar.PerCcf = 0;
                        //recVar.PerLog = 0;
                        //recVar.PerMbf = 0;
                        //recVar.PerTon = 0;
                        //recVar.TotalDollars = 0;
                        ctx.RptCostByLineItems.Add(recVar);
                    }

                    List<Cost> haulColl = ProjectBLL.GetCostList(ref projectContext, pProject.CostTableName, "HAULING");
                    foreach (var haulItem in haulColl)
                    {
                        RptCostByLineItem haulVar = new RptCostByLineItem();
                        haulVar.CostType = haulItem.CostType;
                        haulVar.CostTypeOrder = 98;
                        haulVar.StandsSelectedId = standItem.StandsId;
                        haulVar.CostName = haulItem.CostDescription;
                        haulVar.PrintOrder = (short)haulItem.ReportPrintOrder;
                        haulVar.Species = haulItem.Species;
                        AssignCostItemValues(ref haulVar, haulItem, spcTotal, standItem);
                        ctx.RptCostByLineItems.Add(haulVar);
                    }

                    List<Cost> yrdColl = ProjectBLL.GetCostList(ref projectContext, pProject.CostTableName, "YARDING");
                    foreach (var yrdItem in yrdColl)
                    {
                        RptCostByLineItem yrdVar = new RptCostByLineItem();
                        yrdVar.CostType = yrdItem.CostType;
                        yrdVar.CostTypeOrder = 99;
                        yrdVar.StandsSelectedId = standItem.StandsId;
                        yrdVar.CostName = yrdItem.CostDescription;
                        yrdVar.PrintOrder = (short)yrdItem.ReportPrintOrder;
                        yrdVar.Species = yrdItem.Species;
                        AssignCostItemValues(ref yrdVar, yrdItem, spcTotal, standItem);
                        ctx.RptCostByLineItems.Add(yrdVar);
                    }


                    //RptCostByLineItem hrec = new RptCostByLineItem();
                    //hrec.StandsSelectedId = standItem.StandsId;
                    //hrec.CostType = "HAULING";
                    //hrec.CostName = "HAULING AVG";
                    //hrec.PrintOrder = (short)98;
                    ////AssignCostItemValues(ref hrec, fixedItem, spcTotal, standItem);
                    //hrec.PerAcre = 0;
                    //hrec.PerCcf = 0;
                    //hrec.PerLog = 0;
                    //hrec.PerMbf = 0;
                    //hrec.PerTon = 0;
                    //hrec.TotalDollars = 0;
                    //ctx.RptCostByLineItems.Add(hrec);

                    //RptCostByLineItem yrec = new RptCostByLineItem();
                    //yrec.StandsSelectedId = standItem.StandsId;
                    //yrec.CostType = "YARDING";
                    //yrec.CostName = "YARDING AVG";
                    //yrec.PrintOrder = (short)99;
                    ////AssignCostItemValues(ref recFixed, fixedItem, spcTotal, standItem);
                    //yrec.PerAcre = 0;
                    //yrec.PerCcf = 0;
                    //yrec.PerLog = 0;
                    //yrec.PerMbf = 0;
                    //yrec.PerTon = 0;
                    //yrec.TotalDollars = 0;
                    //ctx.RptCostByLineItems.Add(yrec);
                }
                ctx.SaveChanges();
            }
        }

        private static void AssignCostItemValues(ref RptCostByLineItem rec, Cost costItem, RptSpeciesSortTimberEvalTotal spcTotal, Stand stand)
        {
            switch(costItem.Units)
            {
                case "YEAR":
                    rec.PerAcre = (double)(costItem.CostAmount / stand.NetGeographicAcres);
                    rec.PerCcf = (double)(costItem.CostAmount / spcTotal.TotalCunits);
                    rec.PerLog = (double)(costItem.CostAmount / spcTotal.TotalLogs);
                    rec.PerMbf = (double)(costItem.CostAmount / spcTotal.TotalMbf);
                    rec.PerTon = (double)(costItem.CostAmount / spcTotal.TotalTons);
                    rec.TotalDollars = (double)costItem.CostAmount;
                    break;
                case "MBF":
                    rec.PerMbf = (double)costItem.CostAmount;
                    rec.TotalDollars = (double)(costItem.CostAmount * spcTotal.TotalMbf);
                    rec.PerAcre = (double)(rec.TotalDollars / stand.NetGeographicAcres);
                    rec.PerCcf = (double)(rec.TotalDollars / spcTotal.TotalCunits);
                    rec.PerLog = (double)(rec.TotalDollars / spcTotal.TotalLogs);
                    rec.PerTon = (double)(rec.TotalDollars / spcTotal.TotalTons);
                    break;
                case "CCF":
                    rec.PerCcf = (double)costItem.CostAmount;
                    rec.TotalDollars = (double)(costItem.CostAmount * spcTotal.TotalCunits);
                    rec.PerAcre = (double)(rec.TotalDollars / stand.NetGeographicAcres);
                    rec.PerCcf = (double)(rec.TotalDollars / spcTotal.TotalCunits);
                    rec.PerLog = (double)(rec.TotalDollars / spcTotal.TotalLogs);
                    rec.PerTon = (double)(rec.TotalDollars / spcTotal.TotalTons);
                    break;
                case "ACRE":
                    if (costItem.TreatmentAcresPerYear != null && costItem.TreatmentAcresPerYear > 0)
                    {
                        if (costItem.Percent != null && costItem.Percent > 0)
                            rec.TotalDollars = (double)((costItem.TreatmentAcresPerYear * (costItem.Percent/100)) * costItem.CostAmount);
                        else
                            rec.TotalDollars = (double)(costItem.TreatmentAcresPerYear * costItem.CostAmount);
                        rec.PerMbf = (double)(rec.TotalDollars / spcTotal.TotalMbf);
                        rec.PerAcre = (double)(rec.TotalDollars / stand.NetGeographicAcres);
                        rec.PerCcf = (double)(rec.TotalDollars / spcTotal.TotalCunits);
                        rec.PerLog = (double)(rec.TotalDollars / spcTotal.TotalLogs);
                        rec.PerTon = (double)(rec.TotalDollars / spcTotal.TotalTons);
                    }
                    else
                    {
                        rec.TotalDollars = 0;
                        rec.PerMbf = 0;
                        rec.PerAcre = 0;
                        rec.PerCcf = 0;
                        rec.PerLog = 0;
                        rec.PerTon = 0;
                    }
                    rec.TotalDollars = (double)(costItem.CostAmount * spcTotal.TotalMbf);
                    rec.PerMbf = (double)costItem.CostAmount;
                    
                    rec.PerAcre = (double)(rec.TotalDollars / stand.NetGeographicAcres);
                    rec.PerCcf = (double)(rec.TotalDollars / spcTotal.TotalCunits);
                    rec.PerLog = (double)(rec.TotalDollars / spcTotal.TotalLogs);
                    rec.PerTon = (double)(rec.TotalDollars / spcTotal.TotalTons);
                    break;
            }
        }

        public static void BuildRptTransportation(ref ProjectDbContext projectContext, Project.DAL.Project pProject, List<Stand> pStands)
        {
            int saveId = -1;
            long saveStandsSelectedId = -1;
            string tDestination = string.Empty;
            string tSpecies = string.Empty;
            string tProductName = string.Empty;
            string tSort = string.Empty;

            float totalTons = 0, totalNetCcf = 0;
            float totalNetMbf = 0, tripHaulHours = 0, numberOfLoads = 0;
            float truckCostHour = 0, haulMbf = 0, loggingMbf = 0, logValueMbf = 0, deliveredValueMbf = 0, totalValueReturn = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tNetCuFtVolume = 0, tNetBdFtVolume = 0;
            float avgLoadTon = 0, avgLoadCcf = 0, avgLoadMbf = 0;

            TempDbContext ctx = new TempDbContext();

            AddRptTransportation(ref ctx, "DARRINGTON", "SAWMILL", "DF", 1056, 406, 211, 1.25, 28.0, 8.0, 5.200, 38, 85.00, 20.43, 150.00, 500.00, 329.57, 69538.70);
            AddRptTransportation(ref ctx, "DARRINGTON", "SAWMILL", "WH", 29, 10, 4, 1.25, 28.0, 8.0, 4.900, 1, 85.00, 21.68, 150.00, 550.00, 378.32, 1513.27);
            AddRptTransportation(ref ctx, "MT VERNON", "SAWMILL", "DF", 1893, 728, 332, 3.00, 28.0, 8.0, 4.900, 68, 85.00, 52.04, 150.00, 550.00, 347.96, 115522.45);
            AddRptTransportation(ref ctx, "EVERETT", "SAWMILL", "DF", 32, 11, 6, 2.50, 28.0, 8.0, 5.900, 1, 85.00, 36.02, 150.00, 450.00, 263.98, 1583.90);
            //List<WrkTreeSegments> wColl = ctx.WrkTreeSegments.OrderBy(w => w.Species).ThenBy(w => w.ProductName).ToList();

            //foreach (var treeItem in wColl)
            //{
            //    if (treeItem.SortCode != "0" && treeItem.GradeCode != "0")
            //    {
            //        if (string.IsNullOrEmpty(tSpecies))
            //        {
            //            tSpecies = treeItem.Species;
            //            tSort = treeItem.SortCode;
            //            tProductName = treeItem.ProductName;
            //            tDestination = treeItem.Destination;
            //            saveId = (int)treeItem.StandsId;
            //            saveStandsSelectedId = (long)treeItem.StandsSelectedId;
            //        }
            //        string saveKey = tDestination.Trim() + tProductName.Trim() + tSpecies.Trim();
            //        string currentKey = treeItem.Destination.Trim() + treeItem.ProductName.Trim() + treeItem.Species.Trim();
            //        if (saveKey != currentKey)
            //        {
            //            // write record
            //            RptTransportation rec = new RptTransportation();
            //            Stand stand = ProjectBLL.GetStand(ref projectContext, saveId);

            //            totalNetCcf = (int)((tNetCuFtVolume / Convert.ToSingle(stand.Plots) * stand.NetGeographicAcres) / 100;
            //            totalNetMbf = (int)((tNetBdFtVolume / Convert.ToSingle(stand.Plots) * stand.NetGeographicAcres) / 1000;
            //            totalTons = (int)((totalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""))) / 2000);
            //            //rec.LogAveLen = tLen / tLogs;
            //            //rec.LogAvgDia = tDia / tLogs;
            //            //rec.TotalLogs = (int)tLogs;
            //            float logAveLen1 = tLen / tLogs;
            //            float logAvgDia1 = tDia / tLogs;
            //            Price priceRec = ProjectBLL.GetPrice(ref projectContext, pProject.PriceTableName, tSpecies, tSort, logAveLen1, logAvgDia1);
            //            int? price = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, logAveLen1, logAvgDia1);

            //            rec.StandsSelectedId = saveStandsSelectedId;
            //            if (priceRec != null && tSort != "0")
            //            {
            //                rec.Product = tProductName;
            //                rec.Destination = tDestination;
            //                rec.Species = tSpecies;
            //                rec.TotalCcf = totalNetCcf;
            //                rec.TotalNetMbf = totalNetMbf;
            //                rec.TotalTons = totalTons;
            //            }
            //            else
            //            {
            //                rec.Product = tProductName;
            //                rec.Destination = tDestination;
            //                rec.Species = tSpecies;
            //                rec.TotalCcf = totalNetCcf;
            //                rec.TotalNetMbf = totalNetMbf;
            //                rec.TotalTons = totalTons;
            //            }

            //            // move fields
            //            tDestination = treeItem.Destination;
            //            tSpecies = treeItem.Species;
            //            tSort = treeItem.SortCode;
            //            tProductName = treeItem.ProductName;
            //            saveId = (int)treeItem.StandsId;
            //            saveStandsSelectedId = (long)treeItem.StandsSelectedId;
            //            tLen = Convert.ToSingle(treeItem.CalcLen;
            //            tDia = Convert.ToSingle(treeItem.CalcTopDia;
            //            tLogs = 1;
            //            tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
            //            tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
            //            //tTons = Convert.ToSingle(treeItem.TotalTons;
            //            //if (tSort == "POLES")
            //            //    rec.TotalLogs--;
            //            ctx.RptTransportation.Add(rec);
            //        }
            //        else
            //        {
            //            tLogs++;
            //            tLen += Convert.ToSingle(treeItem.CalcLen;
            //            tDia += Convert.ToSingle(treeItem.CalcTopDia;
            //            tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
            //            tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolume * Convert.ToSingle(treeItem.TreesPerAcre;
            //            //tTons = Convert.ToSingle(treeItem.TotalTons;
            //        }
            //    }
            //}
            //RptTransportation rec1 = new RptTransportation();
            //Stand stand1 = ProjectBLL.GetStand(ref projectContext, saveId);

            //totalNetCcf = (int)((tNetCuFtVolume / Convert.ToSingle(stand1.Plots) * stand1.NetGeographicAcres) / 100;
            //totalNetMbf = (int)((tNetBdFtVolume / Convert.ToSingle(stand1.Plots) * stand1.NetGeographicAcres) / 1000;
            //totalTons = (int)((totalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, pProject, tSpecies, ""))) / 2000);
            //float logAveLen2 = tLen / tLogs;
            //float logAvgDia2 = tDia / tLogs;
            //Price priceRec1 = ProjectBLL.GetPrice(ref projectContext, pProject.PriceTableName, tSpecies, tSort, logAveLen2, logAvgDia2);
            //int? price1 = GetPriceForTimeberEval(ref projectContext, pProject.PriceTableName, tSpecies, tSort, logAveLen2, logAvgDia2);

            //rec1.StandsSelectedId = saveStandsSelectedId;
            //if (price1 != -1 && tSort != "0")
            //{
            //    rec1.Product = tProductName;
            //    rec1.Destination = tDestination;
            //    rec1.Species = tSpecies;
            //    rec1.TotalCcf = totalNetCcf;
            //    rec1.TotalNetMbf = totalNetMbf;
            //    rec1.TotalTons = totalTons;
            //}
            //else
            //{
            //    rec1.Product = tProductName;
            //    rec1.Destination = tDestination;
            //    rec1.Species = tSpecies;
            //    rec1.TotalCcf = totalNetCcf;
            //    rec1.TotalNetMbf = totalNetMbf;
            //    rec1.TotalTons = totalTons;
            //}
            //rec1.Species = tSpecies;
            ////if (tSort == "POLES")
            ////    rec1.TotalLogs--;
            //ctx.RptTransportation.Add(rec1);
            ctx.SaveChanges();
            ctx.Dispose();
        }

        private static void AddRptTransportation(ref TempDbContext ctx, string p1, string p2, string p3, int p4, int p5, int p6, double p7, double p8, double p9, double p10, int p11, double p12, double p13, double p14, double p15, double p16, double p17)
        {
            RptTransportation rec = new RptTransportation();
            rec.Destination = p1;
            rec.Product = p2;
            rec.Species = p3;
            rec.TotalTons = p4;
            rec.TotalCcf = p5;
            rec.TotalNetMbf = p6;
            rec.TripHaulHours = p7;
            rec.AvgLoadTon = p8;
            rec.AvgLoadCcf = p9;
            rec.AvgLoadMbf = p10;
            rec.NumberOfLoads = p11;
            rec.TruckCostPerHour = p12;
            rec.HaulPerMbf = p13;
            rec.LoggingDollarsPerMbf = p14;
            rec.LogValuePerMbf = p15;
            rec.DeliveredValuePerMbf = p16;
            rec.TotalValueReturn = p17;
            ctx.RptTransportations.Add(rec);
        }

        public static int GetDiamClass(List<DefReportScalingDiaClass> diaClass, float p)
        {
            float fDia = 0;
            float tDia = 0;

            if (diaClass.Count > 0)
            {
                for (int i = 0; i < diaClass.Count; i++)
                {
                    string[] sDia = diaClass[i].Item.Split('-');
                    switch (i)
                    {
                        case 0:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 1;
                            break;
                        case 1:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 2;
                            break;
                        case 2:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 3;
                            break;
                        case 3:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 4;
                            break;
                        case 4:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 5;
                            break;
                        case 5:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 6;
                            break;
                        case 6:
                            fDia = Utils.ConvertToFloat(sDia[0]);
                            tDia = Utils.ConvertToFloat(sDia[1] + ".9999");
                            if (p >= fDia && p <= tDia)
                                return 7;
                            break;
                        case 7:
                            fDia = Utils.ConvertToFloat(sDia[0].Replace("+", ""));
                            tDia = Utils.ConvertToFloat("99.9999");
                            if (p >= fDia && p <= tDia)
                                return 8;
                            break;
                    }
                }
                return 0;
            }
            else
                return 0;
        }

        public static void BuildRptLogStockSortGradeMbf(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, List<Stand> pStands, bool pIsCombined)
        {
            int saveId = -1;
            long saveStandsSelectedId = -1;
            string saveKey = string.Empty;
            string currentKey = string.Empty;
            string tTract = string.Empty;
            string tStand = string.Empty;
            //long tStandId = -1;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            string tGrade = string.Empty;
            string tSort = string.Empty;
            string tLenClass = string.Empty;
            string tTreeType = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tTotalGrossBdFt = 0;
            float tTotalNetCuFt = 0;
            float tTotalGrossCuFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tScribnerNetVolume = 0;
            float tGrossBdFtVolume = 0, tNetBdFtVolume = 0, tGrossCuFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            float Dia1 = 0, Dia2 = 0, Dia3 = 0, Dia4 = 0, Dia5 = 0, Dia6 = 0, Dia7 = 0, Dia8 = 0;

            //List<DefReportLogLengthClasses> lenClass = projectContext.DefReportLogLengthClasses.ToList();
            List<DefReportScalingDiaClass> diaClass = projectContext.DefReportScalingDiaClasses.ToList();

            using (TempDbContext ctx = new TempDbContext())
            {
                //List<WrkTreeSegment> wColl = ctx.WrkTreeSegments
                //    .OrderBy(w => w.TractName)
                //    .ThenBy(w => w.StandName)
                //    .ThenBy(w => w.Species)
                //    .ThenBy(w => w.TreeStatus)
                //    .ThenBy(w => w.TreeType)
                //    .ThenBy(w => w.SortCode)
                //    .ThenBy(w => w.GradeCode)
                //    .ThenBy(w => w.CalcLen)
                //    .ToList();
                foreach (Stand item in pStands)
                {
                    List<Treesegment> wColl = projectContext.Treesegments
                        .Where(w => w.StandsId == item.StandsId)
                        //.OrderBy(w => w.StandsId)
                        .OrderBy(w => w.Species)
                        .ThenBy(w => w.TreeStatusDisplayCode)
                        .ThenBy(w => w.TreeType)
                        .ThenBy(w => w.SortCode)
                        .ThenBy(w => w.GradeCode)
                        .ThenBy(w => w.CalcLen)
                        .ToList();
                    Stand stand = null;
                    foreach (var treeItem in wColl)
                    {
                        if (treeItem.GradeCode != "0")
                        {
                            if (string.IsNullOrEmpty(tSpecies))
                            {
                                tTract = item.TractName;
                                tStand = item.StandName;
                                //tStandId = (long)treeItem.StandsId;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                //saveStandsSelectedId = (long)treeItem.StandsId;
                                saveKey = tTract.Trim() +
                                    tStand.Trim() + 
                                    tSpecies.Trim() +
                                    tStatus +
                                    tTreeType +
                                    tSort.Trim() +
                                    tGrade.Trim() +
                                    tLenClass;
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            }
                            currentKey = item.TractName.Trim() +
                                item.StandName.Trim() + 
                                treeItem.Species.Trim() +
                                treeItem.TreeStatusDisplayCode.Trim() +
                                treeItem.TreeType.Trim() +
                                treeItem.SortCode.Trim() +
                                treeItem.GradeCode.Trim() +
                                string.Format("{0:0}", treeItem.CalcLen);
                            if (saveKey != currentKey)
                            {
                                // write record
                                RptLogStockSortGradeMbf rec = new RptLogStockSortGradeMbf();
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                                rec.RealStandsId = saveId;
                                rec.StandsId = saveId;
                                rec.StandsSelectedId = saveId; // saveStandsSelectedId;
                                rec.TractName = tTract;
                                rec.StandName = tStand;
                                rec.Species = tSpecies;
                                Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                                rec.Status = tStatus;
                                if (string.IsNullOrEmpty(tSort))
                                    rec.Sort = tSort;
                                else
                                {
                                    Sort sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec.SpeciesGroup);
                                    if (sort != null)
                                        rec.Sort = sort.Abbreviation;
                                    else
                                        rec.Sort = "?";
                                }
                                if (string.IsNullOrEmpty(tGrade))
                                    rec.Grade = tGrade;
                                else
                                {
                                    Grade grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec.SpeciesGroup);
                                    if (grade != null)
                                        rec.Grade = grade.Abbreviation;
                                    else
                                        rec.Grade = "?";
                                }
                                rec.LogLengthClass = tLenClass;
                                //rec.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                                //rec.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                                rec.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                rec.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                if (rec.TotalGrossMbf > 0 && rec.TotalNetMbf > 0)
                                    rec.MbfPercentDefect = (double)(((float)rec.TotalGrossMbf - (float)rec.TotalNetMbf) / (float)rec.TotalGrossMbf * 100);
                                else
                                    rec.MbfPercentDefect = 0;
                                rec.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalTons = rec.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                                if (rec.TotalGrossCcf > 0 && rec.TotalNetCcf > 0)
                                    rec.CcfPercentDefect = (double)(((float)rec.TotalGrossCcf - (float)rec.TotalNetCcf) / (float)rec.TotalGrossCcf * 100);
                                else
                                    rec.CcfPercentDefect = 0;
                                //rec.CcfPercentDefect = (double)((rec.TotalGrossCcf - rec.TotalNetCcf) / rec.TotalGrossCcf) * 100;
                                rec.LogsPerAcre = ((tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)stand.NetGeographicAcres;
                                rec.AvgLogLen = tLen / tLogs;
                                rec.AvgScaleDia = tDia / tLogs;
                                rec.AvgBfPerLog = tScribnerNetVolume / tLogs;
                                rec.TonsPerMbf = rec.TotalTons / rec.TotalNetMbf;
                                rec.CfPerMbf = rec.TotalNetCcf * 100 / rec.TotalNetMbf;
                                //rec.ScribnerNetVolume = tScribnerNetVolume;
                                rec.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                                rec.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                                rec.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                                rec.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                                rec.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                                rec.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                                rec.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                                rec.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                                rec.Percent = Math.Round(rec.Class1, 0) + Math.Round(rec.Class2, 0) + Math.Round(rec.Class3, 0) + Math.Round(rec.Class4, 0) +
                                    Math.Round(rec.Class5, 0) + Math.Round(rec.Class6, 0) + Math.Round(rec.Class7, 0) + Math.Round(rec.Class8, 0);
                                rec.PercentLen = rec.Percent;

                                // move fields
                                saveKey = currentKey;
                                //tStandId = (long)treeItem.StandsId;
                                tTract = item.TractName;
                                tStand = item.StandName;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                saveStandsSelectedId = (long)item.StandsId;
                                tLogs = 1;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen = Convert.ToSingle(treeItem.CalcLen);
                                    tDia = Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                else
                                {
                                    tLen = 0;
                                    tDia = 0;
                                }
                                tTreesAcre = Convert.ToSingle(treeItem.TreeTreesPerAcre);
                                //tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume = Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume = Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume = Convert.ToSingle(treeItem.ScribnerNetVolume);
                                ctx.RptLogStockSortGradeMbfs.Add(rec);
                                Dia1 = 0;
                                Dia2 = 0;
                                Dia3 = 0;
                                Dia4 = 0;
                                Dia5 = 0;
                                Dia6 = 0;
                                Dia7 = 0;
                                Dia8 = 0;

                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.ScribnerNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                tLogs++;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen += Convert.ToSingle(treeItem.CalcLen);
                                    tDia += Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                //tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre;
                                //tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume += Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume += Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume += Convert.ToSingle(treeItem.ScribnerNetVolume);
                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.ScribnerNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    RptLogStockSortGradeMbf rec1 = new RptLogStockSortGradeMbf();
                    stand = ProjectBLL.GetStand(ref projectContext, saveId);
                    rec1.RealStandsId = saveId;
                    rec1.StandsId = saveId;
                    rec1.StandsSelectedId = saveStandsSelectedId;
                    rec1.TractName = tTract;
                    rec1.StandName = tStand;
                    rec1.Species = tSpecies;
                    Species spcRec1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                    rec1.Status = tStatus;
                    if (string.IsNullOrEmpty(tSort))
                        rec1.Sort = tSort;
                    else
                        rec1.Sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec1.SpeciesGroup).Abbreviation;
                    if (string.IsNullOrEmpty(tGrade))
                        rec1.Grade = tGrade;
                    else
                        rec1.Grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec1.SpeciesGroup).Abbreviation;
                    rec1.LogLengthClass = tLenClass;
                    //rec1.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                    //rec1.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                    rec1.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    rec1.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    if (rec1.TotalGrossMbf > 0 && rec1.TotalNetMbf > 0)
                        rec1.MbfPercentDefect = rec1.TotalGrossMbf - rec1.TotalNetMbf / rec1.TotalGrossMbf * 100;
                    else
                        rec1.MbfPercentDefect = 0;
                    rec1.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalTons = rec1.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                    if (rec1.TotalGrossCcf > 0 && rec1.TotalNetCcf > 0)
                        rec1.CcfPercentDefect = (rec1.TotalGrossCcf - rec1.TotalNetCcf) / rec1.TotalGrossCcf * 100;
                    else
                        rec1.CcfPercentDefect = 0;
                    rec1.LogsPerAcre = ((tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)stand.NetGeographicAcres;
                    rec1.AvgLogLen = tLen / tLogs;
                    rec1.AvgScaleDia = tDia / tLogs;
                    rec1.AvgBfPerLog = tScribnerNetVolume / tLogs;
                    rec1.TonsPerMbf = rec1.TotalTons / rec1.TotalNetMbf;
                    rec1.CfPerMbf = rec1.TotalNetCcf * 100 / rec1.TotalNetMbf;
                    //rec1.ScribnerNetVolume = tScribnerNetVolume;
                    rec1.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                    rec1.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                    rec1.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                    rec1.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                    rec1.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                    rec1.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                    rec1.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                    rec1.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                    rec1.Percent = Math.Round(rec1.Class1, 0) + Math.Round(rec1.Class2, 0) + Math.Round(rec1.Class3, 0) + Math.Round(rec1.Class4, 0) +
                                    Math.Round(rec1.Class5, 0) + Math.Round(rec1.Class6, 0) + Math.Round(rec1.Class7, 0) + Math.Round(rec1.Class8, 0);
                    rec1.PercentLen = rec1.Percent;
                    ctx.RptLogStockSortGradeMbfs.Add(rec1);
                }
                ctx.SaveChanges();
            }
        }

        public static void AddPercentToRptLogStockSortGradeMbf()
        {
            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var item in coll)
                {
                    var q = ctx.RptLogStockSortGradeMbfs.GroupBy(c => c.Species)
                        .Select(g => new { Species = g.Key, Total = g.Sum(c => c.TotalNetMbf) });
                    foreach (var i in q)
                    {
                        List<RptLogStockSortGradeMbf> rColl = ctx.RptLogStockSortGradeMbfs.Where(r => r.StandsSelectedId == item.StandsId && r.Species == i.Species).ToList();
                        foreach (var rpt in rColl)
                        {
                            rpt.Percent = (rpt.TotalNetMbf / i.Total) * 100;
                        }
                    }
                }

                ctx.SaveChanges();
            }
        }

        public static void BuildRptLogStockSortGradeCcf(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, List<Stand> pStands, bool pIsCombined)
        {
            int saveId = -1;
            long saveStandsSelectedId = -1;
            string saveKey = string.Empty;
            string currentKey = string.Empty;
            string tTract = string.Empty;
            string tStand = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            string tGrade = string.Empty;
            string tSort = string.Empty;
            string tTreeType = string.Empty;
            string tLenClass = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tTotalGrossBdFt = 0;
            float tTotalNetCuFt = 0;
            float tTotalGrossCuFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tScribnerNetVolume = 0;
            float tGrossBdFtVolume = 0, tNetBdFtVolume = 0, tGrossCuFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            float Dia1 = 0, Dia2 = 0, Dia3 = 0, Dia4 = 0, Dia5 = 0, Dia6 = 0, Dia7 = 0, Dia8 = 0;

            //List<DefReportLogLengthClasses> lenClass = projectContext.DefReportLogLengthClasses.ToList();
            List<DefReportScalingDiaClass> diaClass = projectContext.DefReportScalingDiaClasses.ToList();

            using (TempDbContext ctx = new TempDbContext())
            {
                foreach (Stand item in pStands)
                {
                    //List<WrkTreeSegment> wColl = ctx.WrkTreeSegments
                    //    .OrderBy(w => w.TractName)
                    //    .ThenBy(w => w.StandName)
                    //    .ThenBy(w => w.Species)
                    //    .ThenBy(w => w.TreeStatus)
                    //    .ThenBy(w => w.TreeType)
                    //    .ThenBy(w => w.SortCode)
                    //    .ThenBy(w => w.GradeCode)
                    //    .ThenBy(w => w.CalcLen)
                    //    .ToList();
                    List<Treesegment> wColl = projectContext.Treesegments
                        .Where(w => w.StandsId == item.StandsId)
                        //.OrderBy(w => w.StandsId)
                        .OrderBy(w => w.Species)
                        .ThenBy(w => w.TreeStatusDisplayCode)
                        .ThenBy(w => w.TreeType)
                        .ThenBy(w => w.SortCode)
                        .ThenBy(w => w.GradeCode)
                        .ThenBy(w => w.CalcLen)
                        .ToList();
                    Stand stand = null;
                    foreach (var treeItem in wColl)
                    {
                        if (treeItem.GradeCode != "0")
                        {
                            if (string.IsNullOrEmpty(tSpecies))
                            {
                                tTract = item.TractName;
                                tStand = item.StandName;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                saveStandsSelectedId = (long)item.StandsId;
                                saveKey = tTract.Trim() +
                                    tStand.Trim() +
                                    tSpecies.Trim() +
                                    tStatus +
                                    tTreeType +
                                    tSort.Trim() +
                                    tGrade.Trim() +
                                    tLenClass;
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            }
                            currentKey = item.TractName.Trim() +
                                item.StandName.Trim() +
                                treeItem.Species.Trim() +
                                treeItem.TreeStatusDisplayCode.Trim() +
                                treeItem.TreeType.Trim() +
                                treeItem.SortCode.Trim() +
                                treeItem.GradeCode.Trim() +
                                string.Format("{0:0}", treeItem.CalcLen);
                            if (saveKey != currentKey)
                            {
                                // write record
                                RptLogStockSortGradeMbf rec = new RptLogStockSortGradeMbf();
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                                rec.RealStandsId = saveId;
                                rec.StandsId = saveId;
                                rec.StandsSelectedId = saveStandsSelectedId;
                                rec.TractName = tTract;
                                rec.StandName = tStand;
                                rec.Species = tSpecies;
                                Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                                rec.Status = tStatus;
                                if (string.IsNullOrEmpty(tSort))
                                    rec.Sort = tSort;
                                else
                                    rec.Sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec.SpeciesGroup).Abbreviation;
                                if (string.IsNullOrEmpty(tGrade))
                                    rec.Grade = tGrade;
                                else
                                    rec.Grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec.SpeciesGroup).Abbreviation;
                                rec.LogLengthClass = tLenClass;
                                //rec.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                                //rec.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                                rec.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                rec.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                if (rec.TotalGrossMbf > 0 && rec.TotalNetMbf > 0)
                                    rec.MbfPercentDefect = (double)(((float)rec.TotalGrossMbf - (float)rec.TotalNetMbf) / (float)rec.TotalGrossMbf * 100);
                                else
                                    rec.MbfPercentDefect = 0;
                                rec.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalTons = rec.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                                if (rec.TotalGrossCcf > 0 && rec.TotalNetCcf > 0)
                                    rec.CcfPercentDefect = (double)(((float)rec.TotalGrossCcf - (float)rec.TotalNetCcf) / (float)rec.TotalGrossCcf * 100);
                                else
                                    rec.CcfPercentDefect = 0;
                                //rec.CcfPercentDefect = (double)((rec.TotalGrossCcf - rec.TotalNetCcf) / rec.TotalGrossCcf) * 100;
                                rec.LogsPerAcre = ((tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)stand.NetGeographicAcres;
                                rec.AvgLogLen = tLen / tLogs;
                                rec.AvgScaleDia = tDia / tLogs;
                                rec.AvgBfPerLog = tScribnerNetVolume / tLogs;
                                rec.TonsPerMbf = rec.TotalTons / rec.TotalNetMbf;
                                rec.CfPerMbf = rec.TotalNetCcf * 100 / rec.TotalNetMbf;
                                //rec.ScribnerNetVolume = tScribnerNetVolume;
                                rec.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                                rec.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                                rec.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                                rec.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                                rec.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                                rec.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                                rec.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                                rec.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                                rec.Percent = Math.Round(rec.Class1, 0) + Math.Round(rec.Class2, 0) + Math.Round(rec.Class3, 0) + Math.Round(rec.Class4, 0) +
                                    Math.Round(rec.Class5, 0) + Math.Round(rec.Class6, 0) + Math.Round(rec.Class7, 0) + Math.Round(rec.Class8, 0);
                                rec.PercentLen = rec.Percent;

                                // move fields
                                saveKey = currentKey;
                                tTract = item.TractName;
                                tStand = item.StandName;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                saveStandsSelectedId = (long)item.StandsId;
                                tLogs = 1;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen = Convert.ToSingle(treeItem.CalcLen);
                                    tDia = Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                else
                                {
                                    tLen = 0;
                                    tDia = 0;
                                }
                                tTreesAcre = Convert.ToSingle(treeItem.TreeTreesPerAcre);
                                //tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume = Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume = Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume = Convert.ToSingle(treeItem.ScribnerNetVolume);
                                ctx.RptLogStockSortGradeMbfs.Add(rec);
                                Dia1 = 0;
                                Dia2 = 0;
                                Dia3 = 0;
                                Dia4 = 0;
                                Dia5 = 0;
                                Dia6 = 0;
                                Dia7 = 0;
                                Dia8 = 0;

                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.CubicNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                tLogs++;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen += Convert.ToSingle(treeItem.CalcLen);
                                    tDia += Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                //tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre;
                                //tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume += Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume += Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume += Convert.ToSingle(treeItem.ScribnerNetVolume);
                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.CubicNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    RptLogStockSortGradeMbf rec1 = new RptLogStockSortGradeMbf();
                    stand = ProjectBLL.GetStand(ref projectContext, saveId);
                    rec1.RealStandsId = saveId;
                    rec1.StandsId = saveId;
                    rec1.StandsSelectedId = saveStandsSelectedId;
                    rec1.TractName = tTract;
                    rec1.StandName = tStand;
                    rec1.Species = tSpecies;
                    Species spcRec1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                    rec1.Status = tStatus;
                    if (string.IsNullOrEmpty(tSort))
                        rec1.Sort = tSort;
                    else
                        rec1.Sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec1.SpeciesGroup).Abbreviation;
                    if (string.IsNullOrEmpty(tGrade))
                        rec1.Grade = tGrade;
                    else
                        rec1.Grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec1.SpeciesGroup).Abbreviation;
                    rec1.LogLengthClass = tLenClass;
                    //rec1.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                    //rec1.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                    rec1.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    rec1.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    if (rec1.TotalGrossMbf > 0 && rec1.TotalNetMbf > 0)
                        rec1.MbfPercentDefect = (double)(((float)rec1.TotalGrossMbf - (float)rec1.TotalNetMbf) / (float)rec1.TotalGrossMbf * 100);
                    else
                        rec1.MbfPercentDefect = 0;
                    rec1.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalTons = rec1.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                    if (rec1.TotalGrossCcf > 0 && rec1.TotalNetCcf > 0)
                        rec1.CcfPercentDefect = (double)(((float)rec1.TotalGrossCcf - (float)rec1.TotalNetCcf) / (float)rec1.TotalGrossCcf * 100);
                    else
                        rec1.CcfPercentDefect = 0;
                    rec1.LogsPerAcre = ((tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)stand.NetGeographicAcres;
                    rec1.AvgLogLen = tLen / tLogs;
                    rec1.AvgScaleDia = tDia / tLogs;
                    rec1.AvgBfPerLog = tScribnerNetVolume / tLogs;
                    rec1.TonsPerMbf = rec1.TotalTons / rec1.TotalNetMbf;
                    rec1.CfPerMbf = rec1.TotalNetCcf * 100 / rec1.TotalNetMbf;
                    //rec1.ScribnerNetVolume = tScribnerNetVolume;
                    rec1.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                    rec1.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                    rec1.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                    rec1.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                    rec1.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                    rec1.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                    rec1.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                    rec1.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                    rec1.Percent = Math.Round(rec1.Class1, 0) + Math.Round(rec1.Class2, 0) + Math.Round(rec1.Class3, 0) + Math.Round(rec1.Class4, 0) +
                                    Math.Round(rec1.Class5, 0) + Math.Round(rec1.Class6, 0) + Math.Round(rec1.Class7, 0) + Math.Round(rec1.Class8, 0);
                    rec1.PercentLen = rec1.Percent;
                    ctx.RptLogStockSortGradeMbfs.Add(rec1);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptLogStockSortGradeTons(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, List<Stand> pStands, bool pIsCombined)
        {
            int saveId = -1;
            long saveStandsSelectedId = -1;
            string saveKey = string.Empty;
            string currentKey = string.Empty;
            string tTract = string.Empty;
            string tStand = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            string tTreeType = string.Empty;
            string tGrade = string.Empty;
            string tSort = string.Empty;
            string tLenClass = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tTotalGrossBdFt = 0;
            float tTotalNetCuFt = 0;
            float tTotalGrossCuFt = 0;
            float tDia = 0;
            float tLen = 0;
            float tLogs = 0;
            float tScribnerNetVolume = 0;
            float tGrossBdFtVolume = 0, tNetBdFtVolume = 0, tGrossCuFtVolume = 0, tNetCuFtVolume = 0, tTons = 0;
            float Dia1 = 0, Dia2 = 0, Dia3 = 0, Dia4 = 0, Dia5 = 0, Dia6 = 0, Dia7 = 0, Dia8 = 0;

            //List<DefReportLogLengthClasses> lenClass = projectContext.DefReportLogLengthClasses.ToList();
            List<DefReportScalingDiaClass> diaClass = projectContext.DefReportScalingDiaClasses.ToList();

            using (TempDbContext ctx = new TempDbContext())
            {
                foreach (Stand item in pStands)
                {
                    //List<WrkTreeSegment> wColl = ctx.WrkTreeSegments
                    //    .OrderBy(w => w.TractName)
                    //    .ThenBy(w => w.StandName)
                    //    .ThenBy(w => w.Species)
                    //    .ThenBy(w => w.TreeStatus)
                    //    .ThenBy(w => w.TreeType)
                    //    .ThenBy(w => w.SortCode)
                    //    .ThenBy(w => w.GradeCode)
                    //    .ThenBy(w => w.CalcLen)
                    //    .ToList();
                    List<Treesegment> wColl = projectContext.Treesegments
                        .Where(w => w.StandsId == item.StandsId)
                        //.OrderBy(w => w.StandsId)
                        .OrderBy(w => w.Species)
                        .ThenBy(w => w.TreeStatusDisplayCode)
                        .ThenBy(w => w.TreeType)
                        .ThenBy(w => w.SortCode)
                        .ThenBy(w => w.GradeCode)
                        .ThenBy(w => w.CalcLen)
                        .ToList();
                    Stand stand = null;
                    foreach (var treeItem in wColl)
                    {
                        if (treeItem.GradeCode != "0")
                        {
                            if (string.IsNullOrEmpty(tSpecies))
                            {
                                tTract = item.TractName;
                                tStand = item.StandName;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                saveStandsSelectedId = (long)item.StandsId;
                                saveKey = tTract.Trim() +
                                    tStand.Trim() +
                                    tSpecies.Trim() +
                                    tStatus +
                                    tTreeType +
                                    tSort.Trim() +
                                    tGrade.Trim() +
                                    tLenClass;
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                            }
                            currentKey = item.TractName.Trim() +
                                item.StandName.Trim() +
                                treeItem.Species.Trim() +
                                treeItem.TreeStatusDisplayCode.Trim() +
                                treeItem.TreeType.Trim() +
                                treeItem.SortCode.Trim() +
                                treeItem.GradeCode.Trim() +
                                string.Format("{0:0}", treeItem.CalcLen);
                            if (saveKey != currentKey)
                            {
                                // write record
                                RptLogStockSortGradeMbf rec = new RptLogStockSortGradeMbf();
                                stand = ProjectBLL.GetStand(ref projectContext, saveId);
                                rec.RealStandsId = saveId;
                                rec.StandsId = saveId;
                                rec.StandsSelectedId = saveStandsSelectedId;
                                rec.TractName = tTract;
                                rec.StandName = tStand;
                                rec.Species = tSpecies;
                                Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                                rec.Status = tStatus;
                                if (string.IsNullOrEmpty(tSort))
                                    rec.Sort = tSort;
                                else
                                    rec.Sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec.SpeciesGroup).Abbreviation;
                                if (string.IsNullOrEmpty(tGrade))
                                    rec.Grade = tGrade;
                                else
                                    rec.Grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec.SpeciesGroup).Abbreviation;
                                rec.LogLengthClass = tLenClass;
                                //rec.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                                //rec.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                                //rec.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                                rec.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                rec.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                                if (rec.TotalGrossMbf > 0 && rec.TotalNetMbf > 0)
                                    rec.MbfPercentDefect = (double)(((float)rec.TotalGrossMbf - (float)rec.TotalNetMbf) / (float)rec.TotalGrossMbf * 100);
                                else
                                    rec.MbfPercentDefect = 0;
                                rec.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                                rec.TotalTons = rec.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                                if (rec.TotalGrossCcf > 0 && rec.TotalNetCcf > 0)
                                    rec.CcfPercentDefect = (double)(((float)rec.TotalGrossCcf - (float)rec.TotalNetCcf) / (float)rec.TotalGrossCcf * 100);
                                else
                                    rec.CcfPercentDefect = 0;
                                //rec.CcfPercentDefect = (double)((rec.TotalGrossCcf - rec.TotalNetCcf) / rec.TotalGrossCcf) * 100;
                                rec.LogsPerAcre = ((tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)stand.NetGeographicAcres;
                                rec.AvgLogLen = tLen / tLogs;
                                rec.AvgScaleDia = tDia / tLogs;
                                rec.AvgBfPerLog = tScribnerNetVolume / tLogs;
                                rec.TonsPerMbf = rec.TotalTons / rec.TotalNetMbf;
                                rec.CfPerMbf = rec.TotalNetCcf * 100 / rec.TotalNetMbf;
                                //rec.ScribnerNetVolume = tScribnerNetVolume;
                                rec.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                                rec.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                                rec.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                                rec.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                                rec.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                                rec.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                                rec.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                                rec.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                                rec.Percent = Math.Round(rec.Class1, 0) + Math.Round(rec.Class2, 0) + Math.Round(rec.Class3, 0) + Math.Round(rec.Class4, 0) +
                                    Math.Round(rec.Class5, 0) + Math.Round(rec.Class6, 0) + Math.Round(rec.Class7, 0) + Math.Round(rec.Class8, 0);
                                rec.PercentLen = rec.Percent;

                                // move fields
                                saveKey = currentKey;
                                tTract = item.TractName;
                                tStand = item.StandName;
                                tSpecies = treeItem.Species;
                                tStatus = treeItem.TreeStatusDisplayCode;
                                tTreeType = treeItem.TreeType;
                                tSort = treeItem.SortCode;
                                tGrade = treeItem.GradeCode;
                                tLenClass = string.Format("{0:0}", treeItem.CalcLen); // GetLengthClass(lenClass, Convert.ToSingle(treeItem.CalcLen);
                                saveId = (int)item.StandsId;
                                saveStandsSelectedId = (long)item.StandsId;
                                tLogs = 1;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen = Convert.ToSingle(treeItem.CalcLen);
                                    tDia = Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                else
                                {
                                    tLen = 0;
                                    tDia = 0;
                                }
                                tTreesAcre = Convert.ToSingle(treeItem.TreeTreesPerAcre);
                                //tLogsAcre = Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume = Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume = Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume = Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume = Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume = Convert.ToSingle(treeItem.ScribnerNetVolume);
                                ctx.RptLogStockSortGradeMbfs.Add(rec);
                                Dia1 = 0;
                                Dia2 = 0;
                                Dia3 = 0;
                                Dia4 = 0;
                                Dia5 = 0;
                                Dia6 = 0;
                                Dia7 = 0;
                                Dia8 = 0;

                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.CubicNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100
                                                * Utils.ConvertToFloat(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                tLogs++;
                                if (treeItem.TreeType != "C")
                                {
                                    tLen += Convert.ToSingle(treeItem.CalcLen);
                                    tDia += Convert.ToSingle(treeItem.CalcTopDia);
                                }
                                //tTreesAcre += Convert.ToSingle(treeItem.TreesPerAcre;
                                //tLogsAcre += Convert.ToSingle(treeItem.LogsPerAcre;
                                tNetBdFtVolume += Convert.ToSingle(treeItem.ScribnerNetVolumePerAce);
                                tGrossBdFtVolume += Convert.ToSingle(treeItem.ScribnerGrossVolumePerAce);
                                tNetCuFtVolume += Convert.ToSingle(treeItem.CubicNetVolumePerAce);
                                tGrossCuFtVolume += Convert.ToSingle(treeItem.CubicGrossVolumePerAce);
                                tScribnerNetVolume += Convert.ToSingle(treeItem.ScribnerNetVolume);
                                if (treeItem.TreeType != "C")
                                {
                                    float fd = Convert.ToSingle(treeItem.CubicNetVolumePerAce) / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100
                                                * Utils.ConvertToFloat(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                                    switch (GetDiamClass(diaClass, Convert.ToSingle(treeItem.CalcTopDia)))
                                    {
                                        case 1:
                                            Dia1 += fd;
                                            break;
                                        case 2:
                                            Dia2 += fd;
                                            break;
                                        case 3:
                                            Dia3 += fd;
                                            break;
                                        case 4:
                                            Dia4 += fd;
                                            break;
                                        case 5:
                                            Dia5 += fd;
                                            break;
                                        case 6:
                                            Dia6 += fd;
                                            break;
                                        case 7:
                                            Dia7 += fd;
                                            break;
                                        case 8:
                                            Dia8 += fd;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    RptLogStockSortGradeMbf rec1 = new RptLogStockSortGradeMbf();
                    stand = ProjectBLL.GetStand(ref projectContext, saveId);
                    rec1.RealStandsId = saveId;
                    rec1.StandsId = saveId;
                    rec1.StandsSelectedId = saveStandsSelectedId;
                    rec1.TractName = tTract;
                    rec1.StandName = tStand;
                    rec1.Species = tSpecies;
                    Species spcRec1 = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, tSpecies);
                    rec1.Status = tStatus;
                    if (string.IsNullOrEmpty(tSort))
                        rec1.Sort = tSort;
                    else
                        rec1.Sort = ProjectBLL.GetSort(ref projectContext, CurrentProject.SortsTableName, tSort, spcRec1.SpeciesGroup).Abbreviation;
                    if (string.IsNullOrEmpty(tGrade))
                        rec1.Grade = tGrade;
                    else
                        rec1.Grade = ProjectBLL.GetGrade(ref projectContext, CurrentProject.GradesTableName, tGrade, spcRec1.SpeciesGroup).Abbreviation;
                    rec1.LogLengthClass = tLenClass;
                    //rec1.LogsPerAcre = (((double)tLogsAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.TreesPerAcre = (((double)tTreesAcre / (double)stand.Plots) * (double)stand.NetGeographicAcres) / (double)mTotalAcres;
                    //rec1.Gross = (double)tGrossBdFtVolume / (double)mTotalAcres;
                    //rec1.Net = (double)tNetBdFtVolume / (double)mTotalAcres;
                    rec1.TotalGrossMbf = tGrossBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    rec1.TotalNetMbf = tNetBdFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 1000;
                    if (rec1.TotalGrossMbf > 0 && rec1.TotalNetMbf > 0)
                        rec1.MbfPercentDefect = rec1.TotalGrossMbf - rec1.TotalNetMbf / rec1.TotalGrossMbf * 100;
                    else
                        rec1.MbfPercentDefect = 0;
                    rec1.TotalGrossCcf = tGrossCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalNetCcf = tNetCuFtVolume / Convert.ToSingle(stand.Plots) * Convert.ToSingle(stand.NetGeographicAcres) / 100;
                    rec1.TotalTons = rec1.TotalNetCcf * Utils.ConvertToDouble(GetLbsPerCcf(ref projectContext, CurrentProject, tSpecies, "")) / 2000;
                    if (rec1.TotalGrossCcf > 0 && rec1.TotalNetCcf > 0)
                        rec1.CcfPercentDefect = (rec1.TotalGrossCcf - rec1.TotalNetCcf) / rec1.TotalGrossCcf * 100;
                    else
                        rec1.CcfPercentDefect = 0;
                    rec1.LogsPerAcre = tLogsAcre / (double)stand.Plots * (double)stand.NetGeographicAcres / (double)stand.NetGeographicAcres;
                    rec1.AvgLogLen = tLen / tLogs;
                    rec1.AvgScaleDia = tDia / tLogs;
                    rec1.AvgBfPerLog = tScribnerNetVolume / tLogs;
                    rec1.TonsPerMbf = rec1.TotalTons / rec1.TotalNetMbf;
                    rec1.CfPerMbf = rec1.TotalNetCcf * 100 / rec1.TotalNetMbf;
                    //rec1.ScribnerNetVolume = tScribnerNetVolume;
                    rec1.Class1 = Dia1; // (Dia1 / tNetBdFtVolume) * 100;
                    rec1.Class2 = Dia2; // (Dia2 / tNetBdFtVolume) * 100;
                    rec1.Class3 = Dia3; // (Dia3 / tNetBdFtVolume) * 100;
                    rec1.Class4 = Dia4; // (Dia4 / tNetBdFtVolume) * 100;
                    rec1.Class5 = Dia5; // (Dia5 / tNetBdFtVolume) * 100;
                    rec1.Class6 = Dia6; // (Dia6 / tNetBdFtVolume) * 100;
                    rec1.Class7 = Dia7; // (Dia7 / tNetBdFtVolume) * 100;
                    rec1.Class8 = Dia8; // (Dia8 / tNetBdFtVolume) * 100;
                    rec1.Percent = Math.Round(rec1.Class1, 0) + Math.Round(rec1.Class2, 0) + Math.Round(rec1.Class3, 0) + Math.Round(rec1.Class4, 0) +
                                    Math.Round(rec1.Class5, 0) + Math.Round(rec1.Class6, 0) + Math.Round(rec1.Class7, 0) + Math.Round(rec1.Class8, 0);
                    rec1.PercentLen = rec1.Percent;
                    ctx.RptLogStockSortGradeMbfs.Add(rec1);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptRelationshipsBySpecies1(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var item in coll)
                {
                    Stand currentStand = ProjectBLL.GetStand(ref projectContext, (int)item.StandsId);
                    RptRelationshipsBySpecies1 rec1 = new RptRelationshipsBySpecies1();
                    rec1.StandsSelectedId = item.StandsSelectedId;
                    rec1.StandsId = (int)item.StandsId;
                    rec1.Seq = 1;
                    rec1.Item = "Major Age Years";
                    rec1.Totals = GetMajorAgeYears(ref projectContext, currentStand, "*");
                    rec1.Df = GetMajorAgeYears(ref projectContext, currentStand, "DF"); ;
                    rec1.Wh = GetMajorAgeYears(ref projectContext, currentStand, "WH"); ;
                    rec1.Rc = GetMajorAgeYears(ref projectContext, currentStand, "RC"); ;
                    rec1.Gf = GetMajorAgeYears(ref projectContext, currentStand, "GF"); ;
                    rec1.Ra = GetMajorAgeYears(ref projectContext, currentStand, "RA"); ;
                    rec1.Oc = 0;
                    rec1.Oh = 0;
                    ctx.RptRelationshipsBySpecies1.Add(rec1);

                    RptRelationshipsBySpecies1 rec2 = new RptRelationshipsBySpecies1();
                    rec2.StandsSelectedId = item.StandsSelectedId;
                    rec2.StandsId = (int)item.StandsId;
                    rec2.Seq = 2;
                    rec2.Item = "Site Index";
                    rec2.Totals = 0;
                    rec2.Df = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "DF");
                    rec2.Wh = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "WH");
                    rec2.Rc = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "RC");
                    rec2.Gf = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "GF");
                    rec2.Ra = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "RA");
                    rec2.Oc = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "OC");
                    rec2.Oh = GetStandSiteIndexForSpecies(ref projectContext, currentStand, "OH");

                    ctx.RptRelationshipsBySpecies1.Add(rec2);
                }
                ctx.SaveChanges();
            }
        }

        private static double GetMajorAgeYears(ref ProjectDbContext projectContext, Stand currentStand, string pSpecies)
        {
            switch (pSpecies)
            {
                case "*":
                    IList<Tree> ccTotal = projectContext.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == currentStand.StandsId).ToList();
                    if (ccTotal.Count > 0)
                        return (double)GetAge(ref projectContext, currentStand, (short)ccTotal[0].AgeCode);
                    break;
                default:
                    IList<Tree> cc = projectContext.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == currentStand.StandsId && t.SpeciesAbbreviation == pSpecies).ToList();
                    if (cc.Count > 0)
                        return (double)GetAge(ref projectContext, currentStand, (short)cc[0].AgeCode);
                    break;
            }
            return 0;
        }

        private static short GetAge(ref ProjectDbContext projectContext, Project.DAL.Stand pStand, short p)
        {
            StandAge rec = projectContext.StandAges.FirstOrDefault(s => s.StandsId == pStand.StandsId && s.AgeCode == p);
            if (rec != null)
                return rec.Age;
            else
                return 50;
            //switch (p.ToString())
            //{
            //    case "1":
            //        return (short)pStand.Age1;
            //    case "2":
            //        return (short)pStand.Age2;
            //    case "3":
            //        return (short)pStand.Age3;
            //    case "4":
            //        return (short)pStand.Age4;
            //    case "5":
            //        return (short)pStand.Age5;
            //    case "6":
            //        return (short)pStand.Age6;
            //    case "7":
            //        return (short)pStand.Age7;
            //    case "8":
            //        return (short)pStand.Age8;
            //    case "9":
            //        return (short)pStand.Age9;
            //    default:
            //        return 50;
            //}
        }

        private static double GetStandSiteIndexForSpecies(ref ProjectDbContext projectContext, Stand currentStand, string pSpecies)
        {
            if (currentStand.Species11.Trim() == pSpecies || currentStand.Species12.Trim() == pSpecies || currentStand.Species13.Trim() == pSpecies)
                return (double)currentStand.SiteIndex1;
            if (currentStand.Species21.Trim() == pSpecies || currentStand.Species22.Trim() == pSpecies || currentStand.Species23.Trim() == pSpecies)
                return (double)currentStand.SiteIndex2;
            if (currentStand.Species31.Trim() == pSpecies || currentStand.Species32.Trim() == pSpecies || currentStand.Species33.Trim() == pSpecies)
                return (double)currentStand.SiteIndex3;
            if (currentStand.Species41.Trim() == pSpecies || currentStand.Species42.Trim() == pSpecies || currentStand.Species43.Trim() == pSpecies)
                return (double)currentStand.SiteIndex4;
            return 0;
        }

        public static void BuildRptRelationshipsBySpecies2(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            float tpaTotal = 0;
            float tpaDF = 0;
            float tpaWH = 0;
            float tpaRC = 0;
            float tpaGF = 0;
            float tpaRA = 0;
            float tpaOC = 0;
            float tpaOH = 0;

            float baTotal = 0;
            float baDF = 0;
            float baWH = 0;
            float baRC = 0;
            float baGF = 0;
            float baRA = 0;
            float baOC = 0;
            float baOH = 0;

            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var stand in coll)
                {
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvDF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "DF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvWH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "WH").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvGF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "GF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRA = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RA").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OH").FirstOrDefault();

                    RptRelationshipsBySpecies2 rec1 = new RptRelationshipsBySpecies2();
                    rec1.StandsId = (int)stand.StandsId;
                    rec1.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec1.Seq = 1;
                    rec1.Item = "Basal Area per Acre Sq Ft";
                    rec1.Df = 0;
                    rec1.Oc = 0;
                    rec1.Oh = 0;
                    rec1.Ra = 0;
                    rec1.Rc = 0;
                    rec1.Wh = 0;
                    rec1.Gf = 0;
                    if (rsslvDF != null)
                        rec1.Df = (double)rsslvDF.BasalArea;
                    if (rsslvWH != null)
                        rec1.Wh = (double)rsslvWH.BasalArea;
                    if (rsslvRC != null)
                        rec1.Rc = (double)rsslvRC.BasalArea;
                    if (rsslvGF != null)
                        rec1.Gf = (double)rsslvGF.BasalArea;
                    if (rsslvRA != null)
                        rec1.Ra = (double)rsslvRA.BasalArea;
                    if (rsslvOC != null)
                        rec1.Oc = (double)rsslvOC.BasalArea;
                    if (rsslvOH != null)
                        rec1.Oh = (double)rsslvOH.BasalArea;
                    baTotal = (float)rec1.Df + (float)rec1.Wh + (float)rec1.Rc + (float)rec1.Gf + (float)rec1.Ra + (float)rec1.Oc + (float)rec1.Oh;
                    baDF = (float)rec1.Df;
                    baWH = (float)rec1.Wh;
                    baRC = (float)rec1.Rc;
                    baGF = (float)rec1.Gf;
                    baRA = (float)rec1.Ra;
                    baOC = (float)rec1.Oc;
                    baOH = (float)rec1.Oh;
                    rec1.Totals = baTotal;
                    ctx.RptRelationshipsBySpecies2.Add(rec1);

                    RptRelationshipsBySpecies2 rec2 = new RptRelationshipsBySpecies2();
                    rec2.StandsId = (int)stand.StandsId;
                    rec2.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec2.Seq = 2;
                    rec2.Item = "Trees per Acre";
                    rec2.Df = 0;
                    rec2.Oc = 0;
                    rec2.Oh = 0;
                    rec2.Ra = 0;
                    rec2.Rc = 0;
                    rec2.Wh = 0;
                    rec2.Gf = 0;
                    if (rsslvDF != null)
                        rec2.Df = (double)rsslvDF.TreesAcre;
                    if (rsslvWH != null)
                        rec2.Wh = (double)rsslvWH.TreesAcre;
                    if (rsslvRC != null)
                        rec2.Rc = (double)rsslvRC.TreesAcre;
                    if (rsslvGF != null)
                        rec2.Gf = (double)rsslvGF.TreesAcre;
                    if (rsslvRA != null)
                        rec2.Ra = (double)rsslvRA.TreesAcre;
                    if (rsslvOC != null)
                        rec2.Oc = (double)rsslvOC.TreesAcre;
                    if (rsslvOH != null)
                        rec2.Oh = (double)rsslvOH.TreesAcre;
                    tpaTotal = (float)rec2.Df + (float)rec2.Wh + (float)rec2.Rc + (float)rec2.Gf + (float)rec2.Ra + (float)rec2.Oc + (float)rec2.Oh;
                    tpaDF = (float)rec2.Df;
                    tpaWH = (float)rec2.Wh;
                    tpaRC = (float)rec2.Rc;
                    tpaGF = (float)rec2.Gf;
                    tpaRA = (float)rec2.Ra;
                    tpaOC = (float)rec2.Oc;
                    tpaOH = (float)rec2.Oh;
                    rec2.Totals = tpaTotal;
                    ctx.RptRelationshipsBySpecies2.Add(rec2);

                    RptRelationshipsBySpecies2 rec3 = new RptRelationshipsBySpecies2();
                    rec3.StandsId = (int)stand.StandsId;
                    rec3.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec3.Seq = 3;
                    rec3.Item = "% Trees per Acre";
                    rec3.Df = 0;
                    rec3.Oc = 0;
                    rec3.Oh = 0;
                    rec3.Ra = 0;
                    rec3.Rc = 0;
                    rec3.Wh = 0;
                    rec3.Gf = 0;
                    if (tpaDF > 0)
                        rec3.Df = tpaDF / tpaTotal;
                    if (tpaWH > 0)
                        rec3.Wh = tpaWH / tpaTotal;
                    if (tpaRC > 0)
                        rec3.Rc = tpaRC / tpaTotal;
                    if (tpaGF > 0)
                        rec3.Gf = tpaGF / tpaTotal;
                    if (tpaRA > 0)
                        rec3.Ra = tpaRA / tpaTotal;
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    rec3.Totals = 100;
                    ctx.RptRelationshipsBySpecies2.Add(rec3);

                    RptRelationshipsBySpecies2 rec4 = new RptRelationshipsBySpecies2();
                    rec4.StandsId = (int)stand.StandsId;
                    rec4.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec4.Seq = 4;
                    rec4.Item = "QM DBH";
                    rec4.Df = 0;
                    rec4.Oc = 0;
                    rec4.Oh = 0;
                    rec4.Ra = 0;
                    rec4.Rc = 0;
                    rec4.Wh = 0;
                    rec4.Gf = 0;
                    rec4.Totals = Math.Sqrt(baTotal / (tpaTotal * 0.005454154));
                    if (baDF > 0 && tpaDF > 0)
                        rec4.Df = Math.Sqrt(baDF / (tpaDF * 0.005454154));
                    if (baWH > 0 && tpaWH > 0)
                        rec4.Wh = Math.Sqrt(baWH / (tpaWH * 0.005454154));
                    if (baRC > 0 && tpaRC > 0)
                        rec4.Rc = Math.Sqrt(baRC / (tpaRC * 0.005454154));
                    if (baGF > 0 && tpaGF > 0)
                        rec4.Gf = Math.Sqrt(baGF / (tpaGF * 0.005454154));
                    if (baRA > 0 && tpaRA > 0)
                        rec4.Ra = Math.Sqrt(baRA / (tpaRA * 0.005454154));
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    ctx.RptRelationshipsBySpecies2.Add(rec4);

                    RptRelationshipsBySpecies2 rec5 = new RptRelationshipsBySpecies2();
                    rec5.StandsId = (int)stand.StandsId;
                    rec5.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec5.Seq = 5;
                    rec5.Item = "Avg FF 16 Ft";
                    rec5.Df = 0;
                    rec5.Oc = 0;
                    rec5.Oh = 0;
                    rec5.Ra = 0;
                    rec5.Rc = 0;
                    rec5.Wh = 0;
                    rec5.Gf = 0;
                    rec5.Totals = 0;
                    ctx.RptRelationshipsBySpecies2.Add(rec5);

                    RptRelationshipsBySpecies2 rec6 = new RptRelationshipsBySpecies2();
                    rec6.StandsId = (int)stand.StandsId;
                    rec6.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec6.Seq = 6;
                    rec6.Item = "Avg Crown Ratio";
                    rec6.Df = 0;
                    rec6.Oc = 0;
                    rec6.Oh = 0;
                    rec6.Ra = 0;
                    rec6.Rc = 0;
                    rec6.Wh = 0;
                    rec6.Gf = 0;
                    rec6.Totals = 0;
                    ctx.RptRelationshipsBySpecies2.Add(rec6);

                    RptRelationshipsBySpecies2 rec7 = new RptRelationshipsBySpecies2();
                    rec7.StandsId = (int)stand.StandsId;
                    rec7.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec7.Seq = 7;
                    rec7.Item = "Avg Total Ht Ft";
                    rec7.Df = 0;
                    rec7.Oc = 0;
                    rec7.Oh = 0;
                    rec7.Ra = 0;
                    rec7.Rc = 0;
                    rec7.Wh = 0;
                    rec7.Gf = 0;
                    rec7.Totals = 0;
                    ctx.RptRelationshipsBySpecies2.Add(rec7);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptRelationshipsBySpecies3(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            float netTotal = 0;
            float netDF = 0;
            float netWH = 0;
            float netRC = 0;
            float netGF = 0;
            float netRA = 0;
            float netOC = 0;
            float netOH = 0;

            float grossTotal = 0;
            float grossDF = 0;
            float grossWH = 0;
            float grossRC = 0;
            float grossGF = 0;
            float grossRA = 0;
            float grossOC = 0;
            float grossOH = 0;

            float grossMbfPerAcreTotal = 0;
            float grossMbfPerAcreDF = 0;
            float grossMbfPerAcreWH = 0;
            float grossMbfPerAcreRC = 0;
            float grossMbfPerAcreGF = 0;
            float grossMbfPerAcreRA = 0;
            float grossMbfPerAcreOC = 0;
            float grossMbfPerAcreOH = 0;

            float netMbfPerAcreTotal = 0;
            float netMbfPerAcreDF = 0;
            float netMbfPerAcreWH = 0;
            float netMbfPerAcreRC = 0;
            float netMbfPerAcreGF = 0;
            float netMbfPerAcreRA = 0;
            float netMbfPerAcreOC = 0;
            float netMbfPerAcreOH = 0;

            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var stand in coll)
                {
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvDF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "DF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvWH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "WH").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvGF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "GF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRA = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RA").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OH").FirstOrDefault();

                    RptRelationshipsBySpecies3 rec1 = new RptRelationshipsBySpecies3();
                    rec1.StandsId = (int)stand.StandsId;
                    rec1.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec1.Seq = 1;
                    rec1.Item = "Total Gross Mbf (1000 bf)";
                    rec1.Df = 0;
                    rec1.Oc = 0;
                    rec1.Oh = 0;
                    rec1.Ra = 0;
                    rec1.Rc = 0;
                    rec1.Wh = 0;
                    rec1.Gf = 0;
                    if (rsslvDF != null)
                        rec1.Df = (double)rsslvDF.TotalGrossMbf;
                    if (rsslvWH != null)
                        rec1.Wh = (double)rsslvWH.TotalGrossMbf;
                    if (rsslvRC != null)
                        rec1.Rc = (double)rsslvRC.TotalGrossMbf;
                    if (rsslvGF != null)
                        rec1.Gf = (double)rsslvGF.TotalGrossMbf;
                    if (rsslvRA != null)
                        rec1.Ra = (double)rsslvRA.TotalGrossMbf;
                    if (rsslvOC != null)
                        rec1.Oc = (double)rsslvOC.TotalGrossMbf;
                    if (rsslvOH != null)
                        rec1.Oh = (double)rsslvOH.TotalGrossMbf;
                    grossTotal = (float)rec1.Df + (float)rec1.Wh + (float)rec1.Rc + (float)rec1.Gf + (float)rec1.Ra + (float)rec1.Oc + (float)rec1.Oh;
                    grossDF = (float)rec1.Df;
                    grossWH = (float)rec1.Wh;
                    grossRC = (float)rec1.Rc;
                    grossGF = (float)rec1.Gf;
                    grossRA = (float)rec1.Ra;
                    grossOC = (float)rec1.Oc;
                    grossOH = (float)rec1.Oh;
                    rec1.Totals = grossTotal;
                    ctx.RptRelationshipsBySpecies3.Add(rec1);

                    RptRelationshipsBySpecies3 rec2 = new RptRelationshipsBySpecies3();
                    rec2.StandsId = (int)stand.StandsId;
                    rec2.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec2.Seq = 2;
                    rec2.Item = "Total Net Mbf";
                    rec2.Df = 0;
                    rec2.Oc = 0;
                    rec2.Oh = 0;
                    rec2.Ra = 0;
                    rec2.Rc = 0;
                    rec2.Wh = 0;
                    rec2.Gf = 0;
                    if (rsslvDF != null)
                        rec2.Df = (double)rsslvDF.TotalNetMbf;
                    if (rsslvWH != null)
                        rec2.Wh = (double)rsslvWH.TotalNetMbf;
                    if (rsslvRC != null)
                        rec2.Rc = (double)rsslvRC.TotalNetMbf;
                    if (rsslvGF != null)
                        rec2.Gf = (double)rsslvGF.TotalNetMbf;
                    if (rsslvRA != null)
                        rec2.Ra = (double)rsslvRA.TotalNetMbf;
                    if (rsslvOC != null)
                        rec2.Oc = (double)rsslvOC.TotalNetMbf;
                    if (rsslvOH != null)
                        rec2.Oh = (double)rsslvOH.TotalNetMbf;
                    netTotal = (float)rec2.Df + (float)rec2.Wh + (float)rec2.Rc + (float)rec2.Gf + (float)rec2.Ra + (float)rec2.Oc + (float)rec2.Oh;
                    netDF = (float)rec2.Df;
                    netWH = (float)rec2.Wh;
                    netRC = (float)rec2.Rc;
                    netGF = (float)rec2.Gf;
                    netRA = (float)rec2.Ra;
                    netOC = (float)rec2.Oc;
                    netOH = (float)rec2.Oh;
                    rec2.Totals = netTotal;
                    ctx.RptRelationshipsBySpecies3.Add(rec2);

                    RptRelationshipsBySpecies3 rec3 = new RptRelationshipsBySpecies3();
                    rec3.StandsId = (int)stand.StandsId;
                    rec3.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec3.Seq = 3;
                    rec3.Item = "% Defect";
                    rec3.Df = 0;
                    rec3.Oc = 0;
                    rec3.Oh = 0;
                    rec3.Ra = 0;
                    rec3.Rc = 0;
                    rec3.Wh = 0;
                    rec3.Gf = 0;
                    if (grossDF > 0)
                        rec3.Df = (grossDF-netDF)/grossDF * 100;
                    if (grossWH > 0)
                        rec3.Wh = (grossWH - netWH) / grossWH * 100;
                    if (grossRC > 0)
                        rec3.Rc = (grossRC - netRC) / grossRC * 100;
                    if (grossGF > 0)
                        rec3.Gf = (grossGF - netGF) / grossGF * 100;
                    if (grossRA > 0)
                        rec3.Ra = (grossRA - netRA) / grossRA * 100;
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    rec3.Totals = (grossTotal - netTotal) / grossTotal * 100; ;
                    ctx.RptRelationshipsBySpecies3.Add(rec3);

                    RptRelationshipsBySpecies3 rec4 = new RptRelationshipsBySpecies3();
                    rec4.StandsId = (int)stand.StandsId;
                    rec4.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec4.Seq = 4;
                    rec4.Item = "Gross Mbf per Acre";
                    rec4.Df = 0;
                    rec4.Oc = 0;
                    rec4.Oh = 0;
                    rec4.Ra = 0;
                    rec4.Rc = 0;
                    rec4.Wh = 0;
                    rec4.Gf = 0;
                    if (rsslvDF != null)
                        rec4.Df = (double)rsslvDF.TotalGrossMbf;
                    if (rsslvWH != null)
                        rec4.Wh = (double)rsslvWH.TotalGrossMbf;
                    if (rsslvRC != null)
                        rec4.Rc = (double)rsslvRC.TotalGrossMbf;
                    if (rsslvGF != null)
                        rec4.Gf = (double)rsslvGF.TotalGrossMbf;
                    if (rsslvRA != null)
                        rec4.Ra = (double)rsslvRA.TotalGrossMbf;
                    if (rsslvOC != null)
                        rec4.Oc = (double)rsslvOC.TotalGrossMbf;
                    if (rsslvOH != null)
                        rec4.Oh = (double)rsslvOH.TotalGrossMbf;
                    grossMbfPerAcreTotal = (float)rec4.Df + (float)rec4.Wh + (float)rec4.Rc + (float)rec4.Gf + (float)rec4.Ra + (float)rec4.Oc + (float)rec4.Oh;
                    grossMbfPerAcreDF = (float)rec4.Df;
                    grossMbfPerAcreWH = (float)rec4.Wh;
                    grossMbfPerAcreRC = (float)rec4.Rc;
                    grossMbfPerAcreGF = (float)rec4.Gf;
                    grossRA = (float)rec4.Ra;
                    grossOC = (float)rec4.Oc;
                    grossOH = (float)rec4.Oh;
                    rec4.Totals = grossMbfPerAcreTotal;
                    ctx.RptRelationshipsBySpecies3.Add(rec4);

                    RptRelationshipsBySpecies3 rec5 = new RptRelationshipsBySpecies3();
                    rec5.StandsId = (int)stand.StandsId;
                    rec5.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec5.Seq = 5;
                    rec5.Item = "Net Mbf per Acre";
                    rec5.Df = 0;
                    rec5.Oc = 0;
                    rec5.Oh = 0;
                    rec5.Ra = 0;
                    rec5.Rc = 0;
                    rec5.Wh = 0;
                    rec5.Gf = 0;
                    if (rsslvDF != null)
                        rec5.Df = (double)rsslvDF.TotalNetMbf;
                    if (rsslvWH != null)
                        rec5.Wh = (double)rsslvWH.TotalNetMbf;
                    if (rsslvRC != null)
                        rec5.Rc = (double)rsslvRC.TotalNetMbf;
                    if (rsslvGF != null)
                        rec5.Gf = (double)rsslvGF.TotalNetMbf;
                    if (rsslvRA != null)
                        rec5.Ra = (double)rsslvRA.TotalNetMbf;
                    if (rsslvOC != null)
                        rec5.Oc = (double)rsslvOC.TotalNetMbf;
                    if (rsslvOH != null)
                        rec5.Oh = (double)rsslvOH.TotalNetMbf;
                    netMbfPerAcreTotal = (float)rec5.Df + (float)rec5.Wh + (float)rec5.Rc + (float)rec5.Gf + (float)rec5.Ra + (float)rec5.Oc + (float)rec5.Oh;
                    netMbfPerAcreDF = (float)rec5.Df;
                    netMbfPerAcreWH = (float)rec5.Wh;
                    netMbfPerAcreRC = (float)rec5.Rc;
                    netMbfPerAcreGF = (float)rec5.Gf;
                    netMbfPerAcreRA = (float)rec5.Ra;
                    netMbfPerAcreOC = (float)rec5.Oc;
                    netMbfPerAcreOH = (float)rec5.Oh;
                    rec5.Totals = netMbfPerAcreTotal;
                    ctx.RptRelationshipsBySpecies3.Add(rec5);

                    RptRelationshipsBySpecies3 rec6 = new RptRelationshipsBySpecies3();
                    rec6.StandsId = (int)stand.StandsId;
                    rec6.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec6.Seq = 6;
                    rec6.Item = "% Net Mbf by Species";
                    rec6.Df = 0;
                    rec6.Oc = 0;
                    rec6.Oh = 0;
                    rec6.Ra = 0;
                    rec6.Rc = 0;
                    rec6.Wh = 0;
                    rec6.Gf = 0;
                    if (netMbfPerAcreDF > 0)
                        rec6.Df = netMbfPerAcreDF / netMbfPerAcreTotal;
                    if (netMbfPerAcreWH > 0)
                        rec6.Wh = netMbfPerAcreWH / netMbfPerAcreTotal;
                    if (netMbfPerAcreRC > 0)
                        rec6.Rc = netMbfPerAcreRC / netMbfPerAcreTotal;
                    if (netMbfPerAcreGF > 0)
                        rec6.Gf = netMbfPerAcreGF / netMbfPerAcreTotal;
                    if (netMbfPerAcreRA > 0)
                        rec6.Ra = netMbfPerAcreRA / netMbfPerAcreTotal;
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    rec6.Totals = 100.0;
                    ctx.RptRelationshipsBySpecies3.Add(rec6);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptRelationshipsBySpecies4(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            float netTotal = 0;
            float netDF = 0;
            float netWH = 0;
            float netRC = 0;
            float netGF = 0;
            float netRA = 0;
            float netOC = 0;
            float netOH = 0;

            float grossTotal = 0;
            float grossDF = 0;
            float grossWH = 0;
            float grossRC = 0;
            float grossGF = 0;
            float grossRA = 0;
            float grossOC = 0;
            float grossOH = 0;

            float grossMbfPerAcreTotal = 0;
            float grossMbfPerAcreDF = 0;
            float grossMbfPerAcreWH = 0;
            float grossMbfPerAcreRC = 0;
            float grossMbfPerAcreGF = 0;
            float grossMbfPerAcreRA = 0;
            float grossMbfPerAcreOC = 0;
            float grossMbfPerAcreOH = 0;

            float netMbfPerAcreTotal = 0;
            float netMbfPerAcreDF = 0;
            float netMbfPerAcreWH = 0;
            float netMbfPerAcreRC = 0;
            float netMbfPerAcreGF = 0;
            float netMbfPerAcreRA = 0;
            float netMbfPerAcreOC = 0;
            float netMbfPerAcreOH = 0;

            using (var ctx = new TempDbContext())
            {
                List<SepStandsSelected> coll = TempBLL.GetSepStandsSelected();
                foreach (var stand in coll)
                {
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvDF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "DF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvWH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "WH").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvGF = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "GF").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvRA = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "RA").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOC = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OC").FirstOrDefault();
                    Temp.DAL.RptSpeciesSummaryLogsVolume rsslvOH = ctx.RptSpeciesSummaryLogsVolumes.Where(r => r.StandsSelectedId == stand.StandsSelectedId && r.Species == "OH").FirstOrDefault();

                    RptRelationshipsBySpecies4 rec1 = new RptRelationshipsBySpecies4();
                    rec1.StandsId = (int)stand.StandsId;
                    rec1.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec1.Seq = 1;
                    rec1.Item = "Total Gross Ccf (100 bf)";
                    rec1.Df = 0;
                    rec1.Oc = 0;
                    rec1.Oh = 0;
                    rec1.Ra = 0;
                    rec1.Rc = 0;
                    rec1.Wh = 0;
                    rec1.Gf = 0;
                    if (rsslvDF != null)
                        rec1.Df = (double)rsslvDF.TotalGrossCcf;
                    if (rsslvWH != null)
                        rec1.Wh = (double)rsslvWH.TotalGrossCcf;
                    if (rsslvRC != null)
                        rec1.Rc = (double)rsslvRC.TotalGrossCcf;
                    if (rsslvGF != null)
                        rec1.Gf = (double)rsslvGF.TotalGrossCcf;
                    if (rsslvRA != null)
                        rec1.Ra = (double)rsslvRA.TotalGrossCcf;
                    if (rsslvOC != null)
                        rec1.Oc = (double)rsslvOC.TotalGrossCcf;
                    if (rsslvOH != null)
                        rec1.Oh = (double)rsslvOH.TotalGrossCcf;
                    grossTotal = (float)rec1.Df + (float)rec1.Wh + (float)rec1.Rc + (float)rec1.Gf + (float)rec1.Ra + (float)rec1.Oc + (float)rec1.Oh;
                    grossDF = (float)rec1.Df;
                    grossWH = (float)rec1.Wh;
                    grossRC = (float)rec1.Rc;
                    grossGF = (float)rec1.Gf;
                    grossRA = (float)rec1.Ra;
                    grossOC = (float)rec1.Oc;
                    grossOH = (float)rec1.Oh;
                    rec1.Totals = grossTotal;
                    ctx.RptRelationshipsBySpecies4.Add(rec1);

                    RptRelationshipsBySpecies4 rec2 = new RptRelationshipsBySpecies4();
                    rec2.StandsId = (int)stand.StandsId;
                    rec2.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec2.Seq = 2;
                    rec2.Item = "Total Net Ccf";
                    rec2.Df = 0;
                    rec2.Oc = 0;
                    rec2.Oh = 0;
                    rec2.Ra = 0;
                    rec2.Rc = 0;
                    rec2.Wh = 0;
                    rec2.Gf = 0;
                    if (rsslvDF != null)
                        rec2.Df = (double)rsslvDF.TotalNetCunits;
                    if (rsslvWH != null)
                        rec2.Wh = (double)rsslvWH.TotalNetCunits;
                    if (rsslvRC != null)
                        rec2.Rc = (double)rsslvRC.TotalNetCunits;
                    if (rsslvGF != null)
                        rec2.Gf = (double)rsslvGF.TotalNetCunits;
                    if (rsslvRA != null)
                        rec2.Ra = (double)rsslvRA.TotalNetCunits;
                    if (rsslvOC != null)
                        rec2.Oc = (double)rsslvOC.TotalNetCunits;
                    if (rsslvOH != null)
                        rec2.Oh = (double)rsslvOH.TotalNetCunits;
                    netTotal = (float)rec2.Df + (float)rec2.Wh + (float)rec2.Rc + (float)rec2.Gf + (float)rec2.Ra + (float)rec2.Oc + (float)rec2.Oh;
                    netDF = (float)rec2.Df;
                    netWH = (float)rec2.Wh;
                    netRC = (float)rec2.Rc;
                    netGF = (float)rec2.Gf;
                    netRA = (float)rec2.Ra;
                    netOC = (float)rec2.Oc;
                    netOH = (float)rec2.Oh;
                    rec2.Totals = netTotal;
                    ctx.RptRelationshipsBySpecies4.Add(rec2);

                    RptRelationshipsBySpecies4 rec3 = new RptRelationshipsBySpecies4();
                    rec3.StandsId = (int)stand.StandsId;
                    rec3.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec3.Seq = 3;
                    rec3.Item = "% Defect";
                    rec3.Df = 0;
                    rec3.Oc = 0;
                    rec3.Oh = 0;
                    rec3.Ra = 0;
                    rec3.Rc = 0;
                    rec3.Wh = 0;
                    rec3.Gf = 0;
                    if (grossDF > 0)
                        rec3.Df = ((grossDF - netDF) / grossDF) * 100;
                    if (grossWH > 0)
                        rec3.Wh = ((grossWH - netWH) / grossWH) * 100;
                    if (grossRC > 0)
                        rec3.Rc = ((grossRC - netRC) / grossRC) * 100;
                    if (grossGF > 0)
                        rec3.Gf = ((grossGF - netGF) / grossGF) * 100;
                    if (grossRA > 0)
                        rec3.Ra = ((grossRA - netRA) / grossRA) * 100;
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    rec3.Totals = ((grossTotal - netTotal) / grossTotal) * 100;
                    ctx.RptRelationshipsBySpecies4.Add(rec3);

                    RptRelationshipsBySpecies4 rec4 = new RptRelationshipsBySpecies4();
                    rec4.StandsId = (int)stand.StandsId;
                    rec4.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec4.Seq = 4;
                    rec4.Item = "Gross Ccf per Acre";
                    rec4.Df = 0;
                    rec4.Oc = 0;
                    rec4.Oh = 0;
                    rec4.Ra = 0;
                    rec4.Rc = 0;
                    rec4.Wh = 0;
                    rec4.Gf = 0;
                    if (rsslvDF != null)
                        rec4.Df = (double)rsslvDF.TotalGrossCcf;
                    if (rsslvWH != null)
                        rec4.Wh = (double)rsslvWH.TotalGrossCcf;
                    if (rsslvRC != null)
                        rec4.Rc = (double)rsslvRC.TotalGrossCcf;
                    if (rsslvGF != null)
                        rec4.Gf = (double)rsslvGF.TotalGrossCcf;
                    if (rsslvRA != null)
                        rec4.Ra = (double)rsslvRA.TotalGrossCcf;
                    if (rsslvOC != null)
                        rec4.Oc = (double)rsslvOC.TotalGrossCcf;
                    if (rsslvOH != null)
                        rec4.Oh = (double)rsslvOH.TotalGrossCcf;
                    grossMbfPerAcreTotal = (float)rec4.Df + (float)rec4.Wh + (float)rec4.Rc + (float)rec4.Gf + (float)rec4.Ra + (float)rec4.Oc + (float)rec4.Oh;
                    grossMbfPerAcreDF = (float)rec4.Df;
                    grossMbfPerAcreWH = (float)rec4.Wh;
                    grossMbfPerAcreRC = (float)rec4.Rc;
                    grossMbfPerAcreGF = (float)rec4.Gf;
                    grossRA = (float)rec4.Ra;
                    grossOC = (float)rec4.Oc;
                    grossOH = (float)rec4.Oh;
                    rec4.Totals = grossMbfPerAcreTotal;
                    ctx.RptRelationshipsBySpecies4.Add(rec4);

                    RptRelationshipsBySpecies4 rec5 = new RptRelationshipsBySpecies4();
                    rec5.StandsId = (int)stand.StandsId;
                    rec5.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec5.Seq = 5;
                    rec5.Item = "Net Ccf per Acre";
                    rec5.Df = 0;
                    rec5.Oc = 0;
                    rec5.Oh = 0;
                    rec5.Ra = 0;
                    rec5.Rc = 0;
                    rec5.Wh = 0;
                    rec5.Gf = 0;
                    if (rsslvDF != null)
                        rec5.Df = (double)rsslvDF.TotalNetCunits;
                    if (rsslvWH != null)
                        rec5.Wh = (double)rsslvWH.TotalNetCunits;
                    if (rsslvRC != null)
                        rec5.Rc = (double)rsslvRC.TotalNetCunits;
                    if (rsslvGF != null)
                        rec5.Gf = (double)rsslvGF.TotalNetCunits;
                    if (rsslvRA != null)
                        rec5.Ra = (double)rsslvRA.TotalNetCunits;
                    if (rsslvOC != null)
                        rec5.Oc = (double)rsslvOC.TotalNetCunits;
                    if (rsslvOH != null)
                        rec5.Oh = (double)rsslvOH.TotalNetCunits;
                    netMbfPerAcreTotal = (float)(rec5.Df + rec5.Wh + rec5.Rc + rec5.Gf + rec5.Ra + rec5.Oc + rec5.Oh);
                    netMbfPerAcreDF = (float)rec5.Df;
                    netMbfPerAcreWH = (float)rec5.Wh;
                    netMbfPerAcreRC = (float)rec5.Rc;
                    netMbfPerAcreGF = (float)rec5.Gf;
                    netMbfPerAcreRA = (float)rec5.Ra;
                    netMbfPerAcreOC = (float)rec5.Oc;
                    netMbfPerAcreOH = (float)rec5.Oh;
                    rec5.Totals = netMbfPerAcreTotal;
                    ctx.RptRelationshipsBySpecies4.Add(rec5);

                    RptRelationshipsBySpecies4 rec6 = new RptRelationshipsBySpecies4();
                    rec6.StandsId = (int)stand.StandsId;
                    rec6.StandsSelectedId = (long)stand.StandsSelectedId;
                    rec6.Seq = 6;
                    rec6.Item = "% Net Ccf by Species";
                    rec6.Df = 0;
                    rec6.Oc = 0;
                    rec6.Oh = 0;
                    rec6.Ra = 0;
                    rec6.Rc = 0;
                    rec6.Wh = 0;
                    rec6.Gf = 0;
                    if (netMbfPerAcreDF > 0)
                        rec6.Df = (netMbfPerAcreDF / netMbfPerAcreTotal) * 100;
                    if (netMbfPerAcreWH > 0)
                        rec6.Wh = (netMbfPerAcreWH / netMbfPerAcreTotal) * 100;
                    if (netMbfPerAcreRC > 0)
                        rec6.Rc = (netMbfPerAcreRC / netMbfPerAcreTotal) * 100;
                    if (netMbfPerAcreGF > 0)
                        rec6.Gf = (netMbfPerAcreGF / netMbfPerAcreTotal) * 100;
                    if (netMbfPerAcreRA > 0)
                        rec6.Ra = (netMbfPerAcreRA / netMbfPerAcreTotal) * 100;
                    //if (rsslvOC != null)
                    //    rec2.Oc = (double)rsslvOC.TreesAcre;
                    //if (rsslvOH != null)
                    //    rec2.Oh = (double)rsslvOH.TreesAcre;
                    rec6.Totals = 100.0;
                    ctx.RptRelationshipsBySpecies4.Add(rec6);
                }
                ctx.SaveChanges();
            }
        }

        public static void BuildRptRelationshipsBySpecies5(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void BuildRptRelationshipsBySpecies6(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void BuildRptRelationshipsBySpecies7(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void BuildRptRelationshipsBySpecies8(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void BuildRptRelationshipsBySpecies9(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void BuildRptRelationshipsBySpecies10(ref ProjectDbContext projectContext, Project.DAL.Project currentProject)
        {
            throw new NotImplementedException();
        }

        public static void AddToDataPoint(ref List<DataPoint> pointList, double pY, double pX)
        {
            DataPoint point = new DataPoint();
            point.Y = pY;
            point.X = pX;
            pointList.Add(point);
        }

        public class DataPoint
        {
            public double X { get; set; }
            public double Y { get; set; }
        }

        public static void CalcValues(List<DataPoint> data, out double slope, out double intercept)
        {
            double xSum = 0;
            double ySum = 0;
            double xySum = 0;
            double xSqSum = 0;
            double ySqSum = 0;

            foreach (var point in data)
            {
                var x = point.X;
                var y = point.Y;

                xSum += x;
                ySum += y;
                xySum += (x * y);
                xSqSum += (x * x);
                ySqSum += (y * y);
            }

            slope = Math.Round(((data.Count * xySum) - (xSum * ySum)) /
                         ((data.Count * xSqSum) - (xSum * xSum)), 3);

            intercept = Math.Round(((xSqSum * ySum) - (xSum * xySum)) /
                              ((data.Count * xSqSum) - (xSum * xSum)), 3);

            if (intercept < 0)
                intercept = 0;

            //var a = ((data.Count * xySum) - (xSum * ySum));
            //var b = (((data.Count * xSqSum) - (xSum * xSum)) *
            //             ((data.Count * ySqSum) - (ySum * ySum)));
            //rSquared = (a * a) / b;
        }

        //public static void BuildTempCountTable(ref ProjectDbContext projectContext, GridView pGridView)
        //{
        //    string tCurrKey = string.Empty;
        //    string tPrevKey = string.Empty;

        //    using (var ctx = new TempDbContext())
        //    {
        //        List<long> sColl = new List<long>();
        //        List<CountWithDbh> wColl = null;
        //        int[] rowHandles = pGridView.GetSelectedRows();
        //        for (int i = 0; i < rowHandles.Length; i++)
        //        {
        //            if (rowHandles[i] >= 0)
        //            {
        //                Stand rec = pGridView.GetRow(rowHandles[i]) as Stand;
        //                sColl.Add(rec.StandsId);
        //            }
        //        }

        //        foreach (var item in sColl)
        //        {
        //            List<CountWithDbh> tColl = projectContext.CountWithDbhs.Where(s => s.StandsId == item).ToList();
        //            foreach (var cItem in tColl)
        //            {
        //                TempCountTable rec = new TempCountTable();
        //                rec.CalcCfTree = cItem.GrossCalcCfTree;
        //                rec.CalcCfTree = cItem.CalcCfTree;
        //                rec.CalcFf = cItem.CalcFf;
        //                rec.CalcLogsAcre = cItem.CalcLogsAcre;
        //                rec.CalcTotalHt = cItem.CalcTotalHt;
        //                rec.CalcVBarBdFt = cItem.CalcVBarBdFt;
        //                rec.CalcVBarCuFt = cItem.CalcVBarCuFt;
        //                rec.CfTree = cItem.CfTree;
        //                rec.Dbh = cItem.Dbh;
        //                rec.Ff = cItem.Ff;
        //                rec.Instance = cItem.Instance;
        //                rec.LogsAcre = cItem.LogsAcre;
        //                rec.Species = cItem.Species;
        //                rec.TotalHt = cItem.TotalHt;
        //                rec.VBarBdFt = cItem.VBarBdFt;
        //                rec.VBarCuFt = cItem.VBarCuFt;
        //                ctx.TempCountTables.Add(rec);
        //            }
        //        }
        //        ctx.SaveChanges();

        //        //string saveSpecies = string.Empty;
        //        //int instance = 0;
        //        //foreach (CountWithDia item in wColl)
        //        //{
        //        //    // write record
        //        //    TempCountTable rec = new TempCountTable();
        //        //    if (string.IsNullOrEmpty(saveSpecies))
        //        //        saveSpecies = treeItem.SpeciesAbbreviation;
        //        //    if (saveSpecies != treeItem.SpeciesAbbreviation)
        //        //    {
        //        //        instance = 0;
        //        //        saveSpecies = treeItem.SpeciesAbbreviation;
        //        //    }
        //        //    rec.Dbh = (double)treeItem.Dbh;
        //        //    rec.Species = treeItem.SpeciesAbbreviation;
        //        //    //rec.Status = string.Empty;
        //        //    rec.Instance = instance++;
        //        //    rec.Ff = (double)treeItem.ReportedFormFactor;
        //        //    rec.TotalHt = (double)treeItem.TotalHeight;
        //        //    rec.CfTree = Math.Round((double)treeItem.TotalNetCubicPerAcre/(double)treeItem.TreesPerAcre, 0);
        //        //    rec.VBarBdFt = Math.Round((double)treeItem.TotalNetScribnerPerAcre / (double)treeItem.BasalArea);
        //        //    rec.VBarCuFt = Math.Round((double)treeItem.TotalNetCubicPerAcre / (double)treeItem.BasalArea);
        //        //    rec.LogsAcre = Math.Round((double)treeItem.LogsPerAcre, 1);
        //        //    rec.CalcFf = 0;
        //        //    rec.CalcTotalHt = 0;
        //        //    rec.CalcCfTree = 0;
        //        //    rec.CalcVBarBdFt = 0;
        //        //    rec.CalcVBarCuFt = 0;
        //        //    ctx.TempCountTables.Add(rec);
        //        //}
        //        //ctx.SaveChanges();

        //        List<string> speciesColl = new List<string>();
        //        var result = (from t in ctx.TempCountTables
        //                      group t by t.Species into myGroup
        //                      orderby myGroup.Key
        //                      select new { Species = myGroup.Key }).ToList();
        //        foreach (var item in result)
        //            speciesColl.Add(item.Species);

        //        foreach (var speciesItem in result)
        //        {
        //            List<DataPoint> ff = new List<DataPoint>();
        //            List<DataPoint> totHt = new List<DataPoint>();
        //            List<DataPoint> cfTree = new List<DataPoint>();
        //            List<DataPoint> vbarBdFt = new List<DataPoint>();
        //            List<DataPoint> vbarCuFt = new List<DataPoint>();
        //            List<DataPoint> logsAc = new List<DataPoint>();

        //            double ffSlope = 0;
        //            double ffIntercept = 0;
        //            double totHtSlope = 0;
        //            double totHtIntercept = 0;
        //            double cfTreeSlope = 0;
        //            double cfTreeIntercept = 0;
        //            double vbarBdFtSlope = 0;
        //            double vbarBdFtIntercept = 0;
        //            double vbarCuFtSlope = 0;
        //            double vbarCuFtIntercept = 0;
        //            double logsAcSlope = 0;
        //            double logsAcIntercept = 0;

        //            double ffPrev = 0;
        //            double totHtPrev = 0;
        //            double cfTreePrev = 0;
        //            double vbarBdFtPrev = 0;
        //            double vbarCuFtPrev = 0;
        //            double logsAcPrev = 0;

        //            List<TempCountTable> spcTbl = ctx.TempCountTables.OrderBy(t => t.Instance).Where(t => t.Species == speciesItem.Species).ToList();
        //            foreach (var item in spcTbl)
        //            {
        //                AddToDataPoint(ref ff, (double)item.Ff, item.Instance);
        //                AddToDataPoint(ref totHt, item.TotalHt, item.Instance);
        //                AddToDataPoint(ref cfTree, item.CfTree, item.Instance);
        //                AddToDataPoint(ref vbarBdFt, item.VBarBdFt, item.Instance);
        //                AddToDataPoint(ref vbarCuFt, item.VBarCuFt, item.Instance);
        //                AddToDataPoint(ref logsAc, item.LogsAcre, item.Instance);
        //            }
        //            CalcValues(ff, out ffSlope, out ffIntercept);
        //            CalcValues(totHt, out totHtSlope, out totHtIntercept);
        //            CalcValues(cfTree, out cfTreeSlope, out cfTreeIntercept);
        //            CalcValues(vbarBdFt, out vbarBdFtSlope, out vbarBdFtIntercept);
        //            CalcValues(vbarCuFt, out vbarCuFtSlope, out vbarCuFtIntercept);
        //            CalcValues(logsAc, out logsAcSlope, out logsAcIntercept);

        //            bool bFirst = true;
        //            foreach (var item in spcTbl)
        //            {
        //                if (spcTbl.Count > 1)
        //                {
        //                    if (bFirst)
        //                    {
        //                        item.CalcFf = ffIntercept;
        //                        item.CalcTotalHt = totHtIntercept;
        //                        item.CalcCfTree = cfTreeIntercept;
        //                        item.CalcVBarBdFt = vbarBdFtIntercept;
        //                        item.CalcVBarCuFt = vbarCuFtIntercept;
        //                        item.CalcLogsAcre = logsAcIntercept;

        //                        ffPrev = ffIntercept;
        //                        totHtPrev = totHtIntercept;
        //                        cfTreePrev = cfTreeIntercept;
        //                        vbarBdFtPrev = vbarBdFtIntercept;
        //                        vbarCuFtPrev = vbarCuFtIntercept;
        //                        logsAcPrev = logsAcIntercept;
        //                        bFirst = false;
        //                    }
        //                    else
        //                    {
        //                        item.CalcFf = ffPrev + ffSlope;
        //                        item.CalcTotalHt = totHtPrev + totHtSlope;
        //                        item.CalcCfTree = cfTreePrev + cfTreeSlope;
        //                        item.CalcVBarBdFt = vbarBdFtPrev + vbarBdFtSlope;
        //                        item.CalcVBarCuFt = vbarCuFtPrev + vbarCuFtSlope;
        //                        item.CalcLogsAcre = logsAcPrev + logsAcSlope;

        //                        ffPrev = (double)item.CalcFf;
        //                        totHtPrev = item.CalcTotalHt;
        //                        cfTreePrev = item.CalcCfTree;
        //                        vbarBdFtPrev = item.CalcVBarBdFt;
        //                        vbarCuFtPrev = item.CalcVBarCuFt;
        //                        logsAcPrev = item.CalcLogsAcre;
        //                    }
        //                }
        //                else
        //                {
        //                    item.CalcFf = item.Ff;
        //                    item.CalcTotalHt = item.TotalHt;
        //                    item.CalcCfTree = item.CfTree;
        //                    item.CalcVBarBdFt = item.VBarBdFt;
        //                    item.CalcVBarCuFt = item.VBarCuFt;
        //                    item.CalcLogsAcre = item.LogsAcre;
        //                }
        //            }
        //            ctx.SaveChanges();
        //        }
        //    }
        //}
    }
}
