﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class RelationshipsBySpeciesReport : DevExpress.XtraReports.UI.XtraReport
    {
        public RelationshipsBySpeciesReport()
        {
            InitializeComponent();

            this.DataSource = TempBLL.GetSepStandsSelected();
        }
    }
}
