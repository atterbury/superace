﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace SuperACE.Reports
{
    public partial class TreeSegmentVolumeReport : DevExpress.XtraReports.UI.XtraReport
    {
        public TreeSegmentVolumeReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRSubreport)sender).ReportSource.Parameters["Acres"].Value = Convert.ToString(xrLabel114.Text);
            ((XRSubreport)sender).ReportSource.Parameters["Plots"].Value = Convert.ToString(xrLabel105.Text);
        }
    }
}
