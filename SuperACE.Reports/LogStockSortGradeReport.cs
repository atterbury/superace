﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class LogStockSortGradeReport : DevExpress.XtraReports.UI.XtraReport
    {
        public LogStockSortGradeReport()
        {
            InitializeComponent();
            objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        // use same report for Mbf, CCf and Ton
        // need to 
        // (a) replace strings in report outputs
        // (b) replace string in dereferencing the right column from the data table

        private string _reportVariantString = "Mbf";               // default to Mbf
        private const string placeholder = "{Xxx}";
        private LogStockSortGradeType _reportVariantType = LogStockSortGradeType.Mbf;    // default to Mbf
        #region LogStockSortGradeType enum handling
        private LogStockSortGradeType ReportVariantType
        {
            get { return _reportVariantType; }
            set
            {
                _reportVariantType = value;
                switch (value)
                {
                    case LogStockSortGradeType.Ccf: _reportVariantString = "Ccf"; break;
                    case LogStockSortGradeType.Ton: _reportVariantString = "Ton"; break;
                    default: _reportVariantString = "Mbf"; break;
                }
            }
        }
        private string ReportVariantString
        {
            get { return _reportVariantString; }
            set
            {
                _reportVariantString = value;
                switch (value)
                {
                    case "Ccf": _reportVariantType = LogStockSortGradeType.Ccf; break;
                    case "Ton": _reportVariantType = LogStockSortGradeType.Ton; break;
                    default: _reportVariantType = LogStockSortGradeType.Mbf; break;
                }
            }
        }
        /*
         * another optional style
         *        public LogStockSortGradeReport(LogStockSortGradeType variant)
                {
                    InitializeComponent();
                    ReportVariantType = variant;
                    objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
                }
         */
        #endregion
        public enum LogStockSortGradeType { Mbf, Ccf, Ton }

        protected LogStockSortGradeReport(string variant)
        {
            InitializeComponent();
            ReportVariantString = variant;
            objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = GetCurrentColumnValue("State").ToString();
            string county = GetCurrentColumnValue("County").ToString();
            if (!string.IsNullOrEmpty(state))
                xrLabel36.Text = state;
            else
                xrLabel36.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel37.Text = state;
            else
                xrLabel37.Text = "Blank";

            // replace placeholder with variant
            xrXxxTitle.Text = xrXxxTitle.Text.Replace(placeholder, _reportVariantString);
            xrXxxGrossColHdr.Text = xrXxxGrossColHdr.Text.Replace(placeholder, _reportVariantString);
            xrXxxMultiColHdr.Text = xrXxxMultiColHdr.Text.Replace(placeholder, _reportVariantString);
            xrXxxNetColHdr.Text = xrXxxNetColHdr.Text.Replace(placeholder, _reportVariantString);
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRSubreport sub = (XRSubreport)sender;
            ((LogStockSortGradeSubReport)sub.ReportSource).AssignVariant(_reportVariantString);
        }
    }
}
