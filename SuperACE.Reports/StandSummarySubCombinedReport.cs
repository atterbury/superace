﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class StandSummarySubCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        float speciesTotalHeight = 0;
        float speciesFF = 0;
        float speciesAge = 0;
        float speciesTreesPerAcre = 0;
        float speciesBasalArea = 0;
        float speciesMeasuredTPA = 0;
        float speciesAvgLogNetBdFt = 0;
        float speciesGrossPerAcreBdFt = 0;
        float speciesNetPerAcreBdFt = 0;

        float standTotalHeight = 0;
        float standFF = 0;
        float standAge = 0;
        float standTreesPerAcre = 0;
        float standBasalArea = 0;
        float standMeasuredTPA = 0;
        float standAvgLogNetBdFt = 0;
        float standGrossPerAcreBdFt = 0;
        float standNetPerAcreBdFt = 0;

        public StandSummarySubCombinedReport()
        {
            InitializeComponent();
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel43.Text = string.Format("{0:0}", ageTotalHeight / ageTreesPerAcre);
            //xrLabel44.Text = string.Format("{0:0}", ageFF / ageTreesPerAcre);
            //xrLabel45.Text = string.Format("{0:0.0}", Math.Sqrt(ageBasalAreaPerAcre / (ageTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            //ageTotalHeight = 0;
            //ageFF = 0;
            //ageTreesPerAcre = 0;
            //ageBasalAreaPerAcre = 0;
        }

        private void Detail1_AfterPrint(object sender, EventArgs e)
        {
            ////= Sum(Fields.TotalHeight * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sum(Fields.FF * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sqrt(Sum(Fields.BasalAreaPerAcre)/(Sum(Fields.TreesPerAcre) * 0.005454154))
            //ageTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //ageBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    ageTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    ageFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}

            //standTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //standBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    standTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    standFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
        }

        private void GroupFooter3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel57.Text = string.Format("{0:0}", standTotalHeight / standTreesPerAcre);
            //xrLabel58.Text = string.Format("{0:0}", standFF / standTreesPerAcre);
            //xrLabel59.Text = string.Format("{0:0.0}", Math.Sqrt(standBasalAreaPerAcre / (standTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter3_AfterPrint(object sender, EventArgs e)
        {
            //standTotalHeight = 0;
            //standFF = 0;
            //standTreesPerAcre = 0;
            //standBasalAreaPerAcre = 0;
        }

        private void StandSummarySubReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.GetCurrentColumnValue("DbhClass") != null)
            {
                if (Convert.ToSingle(this.GetCurrentColumnValue("DbhClass").ToString()) > 0)
                {
                    xrLabel27.Text = string.Format("{0:0}", Convert.ToSingle(this.GetCurrentColumnValue("DbhClass").ToString()));
                }
                else
                    xrLabel27.Text = "*";
            }
            if (this.GetCurrentColumnValue("AvgFf") != null)
            {
                if (Convert.ToSingle(this.GetCurrentColumnValue("AvgFf").ToString()) > 0)
                {
                    speciesMeasuredTPA += Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                    standMeasuredTPA += Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                    speciesFF += Convert.ToSingle(this.GetCurrentColumnValue("AvgFf").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                    standFF += Convert.ToSingle(this.GetCurrentColumnValue("AvgFf").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                }
            }
            if (this.GetCurrentColumnValue("AvgTotalHt") != null)
            {
                if (Convert.ToSingle(this.GetCurrentColumnValue("AvgTotalHt").ToString()) > 0)
                {
                    speciesTotalHeight += Convert.ToSingle(this.GetCurrentColumnValue("AvgTotalHt").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                    standTotalHeight += Convert.ToSingle(this.GetCurrentColumnValue("AvgTotalHt").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                }
            }
            if (this.GetCurrentColumnValue("TreesPerAcre") != null)
            {
                speciesAge += Convert.ToSingle(this.GetCurrentColumnValue("AvgAge").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                standAge += Convert.ToSingle(this.GetCurrentColumnValue("AvgAge").ToString()) * Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                speciesTreesPerAcre += Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
                standTreesPerAcre += Convert.ToSingle(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            }
            if (this.GetCurrentColumnValue("BaPerAcre") != null)
            {
                speciesBasalArea += Convert.ToSingle(this.GetCurrentColumnValue("BaPerAcre").ToString());
                standBasalArea += Convert.ToSingle(this.GetCurrentColumnValue("BaPerAcre").ToString());
            }
            if (this.GetCurrentColumnValue("AvgLogNetBdFt") != null)
            {
                speciesAvgLogNetBdFt += Convert.ToSingle(this.GetCurrentColumnValue("AvgLogNetBdFt").ToString());
                standAvgLogNetBdFt += Convert.ToSingle(this.GetCurrentColumnValue("AvgLogNetBdFt").ToString());
            }
            if (this.GetCurrentColumnValue("GrossPerAcreBdFt") != null)
            {
                speciesGrossPerAcreBdFt += Convert.ToSingle(this.GetCurrentColumnValue("GrossPerAcreBdFt").ToString());
                standGrossPerAcreBdFt += Convert.ToSingle(this.GetCurrentColumnValue("GrossPerAcreBdFt").ToString());
            }
            if (this.GetCurrentColumnValue("NetPerAcreBdFt") != null)
            {
                speciesNetPerAcreBdFt += Convert.ToSingle(this.GetCurrentColumnValue("NetPerAcreBdFt").ToString());
                standNetPerAcreBdFt += Convert.ToSingle(this.GetCurrentColumnValue("NetPerAcreBdFt").ToString());
            }
        }

        private void StandSummarySubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptStandTable();
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string tmpDbh = string.Format("{0:0}", (float)Math.Sqrt(speciesBasalArea / (speciesTreesPerAcre * 0.005454154)));
            xrLabel53.Text = string.Format("{0:0}", (float)Math.Sqrt(speciesBasalArea / (speciesTreesPerAcre * 0.005454154)));
            xrLabel54.Text = string.Format("{0:0}", (float)speciesAge / speciesTreesPerAcre);
            xrLabel55.Text = string.Format("{0:0}", (float)speciesFF / speciesMeasuredTPA);
            xrLabel56.Text = string.Format("{0:0}", (float)speciesTotalHeight / speciesMeasuredTPA);
            
            xrLabel57.Text = string.Format("{0:0.000}", (float)speciesBasalArea / Math.Pow(Convert.ToSingle(tmpDbh), 0.5));
            xrLabel58.Text = string.Format("{0:0}", speciesAvgLogNetBdFt / speciesBasalArea);

            xrLabel59.Text = string.Format("{0:0.0}", (((float)speciesTotalHeight / (float)speciesMeasuredTPA)*12) / Convert.ToSingle(tmpDbh));
            xrLabel64.Text = string.Format("{0:0}", ((speciesGrossPerAcreBdFt-speciesNetPerAcreBdFt)/speciesGrossPerAcreBdFt)*100);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            speciesTotalHeight = 0;
            speciesFF = 0;
            speciesTreesPerAcre = 0;
            speciesBasalArea = 0;
            speciesMeasuredTPA = 0;
            speciesAge = 0;
            speciesAvgLogNetBdFt = 0;
            speciesGrossPerAcreBdFt = 0;
            speciesNetPerAcreBdFt = 0;
        }

        private void GroupFooter1_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string tmpDbh = string.Format("{0:0}", (float)Math.Sqrt(standBasalArea / (standTreesPerAcre * 0.005454154)));
            xrLabel65.Text = string.Format("{0:0}", (float)Math.Sqrt(standBasalArea / (standTreesPerAcre * 0.005454154)));
            xrLabel73.Text = string.Format("{0:0}", (float)standAge / standTreesPerAcre);
            xrLabel72.Text = string.Format("{0:0}", (float)standFF / standMeasuredTPA);
            xrLabel71.Text = string.Format("{0:0}", (float)standTotalHeight / standMeasuredTPA);

            xrLabel70.Text = string.Format("{0:0.000}", (float)standBasalArea / Math.Pow(Convert.ToSingle(tmpDbh), 0.5));
            xrLabel69.Text = string.Format("{0:0}", standAvgLogNetBdFt / standBasalArea);

            xrLabel68.Text = string.Format("{0:0.0}", (((float)standTotalHeight / (float)standMeasuredTPA) * 12) / Convert.ToSingle(tmpDbh));
            xrLabel74.Text = string.Format("{0:0}", ((standGrossPerAcreBdFt - standNetPerAcreBdFt) / standGrossPerAcreBdFt) * 100);
        }

        private void GroupFooter1_AfterPrint_1(object sender, EventArgs e)
        {
            standTotalHeight = 0;
            standFF = 0;
            standTreesPerAcre = 0;
            standBasalArea = 0;
            standMeasuredTPA = 0;
            standAvgLogNetBdFt = 0;
            standGrossPerAcreBdFt = 0;
            standNetPerAcreBdFt = 0;
            standAge = 0;
        }
    }
}
