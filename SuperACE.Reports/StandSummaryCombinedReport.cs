﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class StandSummaryCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        private string project = string.Empty;
        private float totalAcres = 0;
        private float totalPlots = 0;
        private float totalTrees = 0;

        float ageTotalHeight = 0;
        float ageFF = 0;
        float ageTreesPerAcre = 0;
        float ageBasalAreaPerAcre = 0;

        float standTotalHeight = 0;
        float standFF = 0;
        float standTreesPerAcre = 0;
        float standBasalAreaPerAcre = 0;

        public StandSummaryCombinedReport(string pProject, float pAcres, float pPlots, float pTrees)
        {
            InitializeComponent();

            //bindingSource1.DataSource = TempBLL.GetSepStandsSelected();
            project = pProject;
            totalAcres = pAcres;
            totalPlots = pPlots;
            totalTrees = pTrees;
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string state = GetCurrentColumnValue("State").ToString();
            //string county = GetCurrentColumnValue("County").ToString();
            //if (!string.IsNullOrEmpty(state))
            //    xrLabel110.Text = state;
            //else
            //    xrLabel110.Text = "Blank";
            //if (!string.IsNullOrEmpty(county))
            //    xrLabel109.Text = state;
            //else
            //    xrLabel109.Text = "Blank";
            xrLabel111.Text = project;
            xrLabel114.Text = totalAcres.ToString();
            xrLabel105.Text = totalPlots.ToString();
            xrLabel108.Text = totalTrees.ToString();
        }
    }
}
