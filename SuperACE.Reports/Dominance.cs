﻿using DevExpress.XtraReports.UI;
using Project.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Temp.DAL;

namespace SuperACE.Reports
{
    class Dominance
    {

        #region setup data set for dominance

        /* 
         * TODO: setup seccond public method which recognizes normality for inter-stand-dominant-priority
         * currently have erroneous data in data-sets, where multiple normalities for same stand (not correct) 
         * read normality value when input data storage is properly fixed
         */

        public static string GetDomPrioritySelectorForStand(ref ProjectDbContext projectContext, int pStandId)
        {
            // look up stand input table to make the decision at the species level for the stand
            Dictionary<string, double> speciesBaPa = new Dictionary<string, double>();
            Dictionary<string, double> speciesMbfPa = new Dictionary<string, double>();
            Dictionary<string, double> speciesCcfPa = new Dictionary<string, double>();
            Dictionary<string, double> speciesTreesPa = new Dictionary<string, double>();

            // read data from StandInput for this stand; and get sum of each interest-item at PER SPECIES LEVEL
            try
            {
                List<StandInput> stands = projectContext.StandInputs.Where(s => s.StandsId == pStandId).ToList(); // returns species rows for this stand
                foreach (StandInput row in stands)       // one or many rows to a species; sum intems of interest per species
                {
                    // even though field names do not say it, these fields are actually all stored as PerAcre values (change names in long run)
                    speciesBaPa[row.Species] = (speciesBaPa.ContainsKey(row.Species) ? speciesBaPa[row.Species] : 0) + row.BasalArea;
                    speciesMbfPa[row.Species] = (speciesMbfPa.ContainsKey(row.Species) ? speciesMbfPa[row.Species] : 0) + row.NetBdFt;
                    speciesCcfPa[row.Species] = (speciesCcfPa.ContainsKey(row.Species) ? speciesCcfPa[row.Species] : 0) + row.NetCuFt;
                    speciesTreesPa[row.Species] = (speciesTreesPa.ContainsKey(row.Species) ? speciesTreesPa[row.Species] : 0) + row.TreesPerAcre;
                }
            }
            catch (DbEntityValidationException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            // for each item of interest get the maximum value across all species
            double bapa = 0, mbfpa = 0, ccfpa = 0, tpa = 0;
            int count = speciesTreesPa.Count;       // all Dictionaries, should have same counts; pull from first one
            for (int i = 0; i < count; i++)
            {
                bapa = Math.Max(speciesBaPa.ElementAt(i).Value, bapa);
                mbfpa = Math.Max(speciesMbfPa.ElementAt(i).Value, mbfpa);
                ccfpa = Math.Max(speciesCcfPa.ElementAt(i).Value, ccfpa);
                tpa = Math.Max(speciesTreesPa.ElementAt(i).Value, tpa);
            }

            // invoke comparator to check stand data against priority table specs
            return computeDominantPriorityWithoutNormality(ref projectContext, bapa, mbfpa, ccfpa, tpa);
        }

        private static string computeDominantPriorityWithoutNormality(ref ProjectDbContext projectContext, double bapa, double mbfpa, double ccfpa, double tpa) {
            return computeDominantPriorityWithNormality(ref projectContext, bapa, mbfpa, ccfpa, tpa, null);
        }

        private static string computeDominantPriorityWithNormality(ref ProjectDbContext projectContext, double bapa, double mbfpa, double ccfpa, double tpa, double? normality)
        {
            // read priorities from table, and compare against incoming stand data
            try
            {
                List<DefReportDomPri> domPri = projectContext.DefReportDomPris.OrderBy(p => p.Priority).ToList();
                foreach (var priority in domPri)
                {
                    // which item will we compare against
                    double? compareto = 0;
                    switch (priority.Item)
                    {
                        case "Trees per Acre": compareto = tpa; break;
                        case "Net Cubic Feet / Acre": compareto = ccfpa; break;
                        case "Net Board Feet / Acre": compareto = mbfpa; break;
                        case "Basal Area / Acre": compareto = bapa; break;
                        case "Normality": compareto = normality; break;
                        default: // error: "dom priority data has an unexpected option"
                            return null;    // unexpected option, did not recognize (?throw an error?)
                    }
                    // process the comparison
                    if (compareto != null)  // first avoid the nullable normality situation
                    {          
                        if (priority.Minimum == null)   // no minimum set, this priority is now dominant
                            return priority.Item;       // later switch to enum?
                        else if (compareto >= priority.Minimum) // has minimum set, is this still dominant based on incoming data?
                            return priority.Item;       // good to go (later switch to enum?)
                    }
                    // did not measure up, continue to next priority item
                }
                // if dropped through without a legal match, user input had malformed specs
                // error: "Dom priority input does not permit valid choice"
                return null;
            }
            catch (DbEntityValidationException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private static byte computeDominantPriorityFromYieldTable(ref ProjectDbContext projectContext, YieldTable yld, ReportRecord rec)
        {
            // read priorities from table, and compare against incoming yield table
            try
            {
                List<DefReportDomPri> domPri = projectContext.DefReportDomPris.OrderBy(p => p.Priority).ToList();
                foreach (var priority in domPri)
                {
                    // which item will we compare against
                    double? comp = null, val = 0;
                    switch (priority.Item)
                    {
                        case "Trees per Acre": comp= yld.TreesPerAcre; val = rec.TreesPerAcre; break;
                        case "Net Cubic Feet / Acre": comp = yld.CubicNet; val = rec.NetCuftPerAcre; break;
                        case "Net Board Feet / Acre": comp = yld.ScribnerNet; val = rec.NetBdftPerAcre; break;
                        case "Basal Area / Acre": comp = yld.BasalArea; val = rec.BasalAreaPerAcre; break;
                        case "Normality": comp = null; break;
                        default: // error: "dom priority data has an unexpected option"
                            return 0;    // unexpected option, did not recognize (?throw an error?)
                    }
                    // process the comparison
                    if ((comp != null) && (comp > 0))
                        return (byte)((val / comp) * 100);
                    // did not measure up, continue to next priority item
                }
                // if dropped through without a legal match, user input had malformed specs
                // error: "Dom priority input does not permit valid choice"
                return 0;
            }
            catch (DbEntityValidationException ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }


        public static byte GetNormality(ref ProjectDbContext projectContext, Project.DAL.Project CurrentProject, ReportRecord rec)
        {
            string species = rec.Species;
            short age = (short)(rec.Age);
            byte siteIndex = rec.SiteIndex;

            Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, CurrentProject.SpeciesTableName, species);
            YieldTable yldTable = ProjectBLL.GetYieldTableRecord(ref projectContext, spcRec.YieldTableName, age, siteIndex);
            // multiply by 100 before returning (RC: why?)
            return computeDominantPriorityFromYieldTable(ref projectContext, yldTable, rec);
        }


        #endregion


        #region private members for tracking dominance
        private string _groupbycolumn = null;
        private string[] maxcolumns = null;         // retain original, if want to switch back
        private string[] weightedcolumns = null;    // add weightage model
        // RC: currently support row and set level; do we need to upgrade to support multiple sets (ie set of sets)?
        // group by & weighted methodoloy
        private Dictionary<string, double> rowGroupBys = new Dictionary<string, double>();
        private Dictionary<string, double> setGroupBys = new Dictionary<string, double>();
        // weighted has sum of multiplications; keep as double for safety on big data sets
        private Dictionary<string, Dictionary<string, double>> rowGbWeightedDominances = new Dictionary<string, Dictionary<string, double>>();
        private Dictionary<string, Dictionary<string, double>> setGbWeightedDominances = new Dictionary<string, Dictionary<string, double>>();
        // simple max methodology
        private Dictionary<string, KeyValuePair<float, object>> rowMaxDominances = new Dictionary<string, KeyValuePair<float, object>>();
        private Dictionary<string, KeyValuePair<float, object>> setMaxDominances = new Dictionary<string, KeyValuePair<float, object>>();
        #endregion

        /* 
         * 
         * initial spec changed two fold;
         * (a)  dominance is to be assessed using a GroupBy methodology
         *      and is not by simple per-row max function that was originally built
         *      eg, GroupBy "Species' column means that values of the Dominance columns are added per species
         *      Dominant species for a plot, is computed on GroupBy summation; 
         * (b)  need support for sum-product-average still retainining GroupBy methodology
         *      ie: sum(coi.value * dom.value, by group) / sum(dom.value, by group)
         * retaining initial capability & adding newer groupby capability
         * 
         */
        public string DominanceColumn { get; set; }

        #region track dominance during reporting with simple model
        // storage for initial max style dominance
        public void SetColumnsOfSimpleInterest(params string[] columnnames)
        {
            // retain access to the comun names
            maxcolumns = columnnames;
            // init the counters
            foreach (string column in columnnames)
            {
                if (!rowMaxDominances.ContainsKey(column))
                {  // safety check, in case called twice with same column
                    rowMaxDominances.Add(column, new KeyValuePair<float, object>(0, null));
                    setMaxDominances.Add(column, new KeyValuePair<float, object>(0, null));
                }
            }
        }

        private KeyValuePair<float, object>? choose(XtraReport report, string column, Dictionary<string, KeyValuePair<float, object>> dominances)
        {
            KeyValuePair<float, object> kvp = dominances[column];
            float chooseOn = ReportUtils.GetFloatFromColumn(report, DominanceColumn);
            object choosing = report.GetCurrentColumnValue(column);
            if (kvp.Key < chooseOn) // reset kvp, if previous value has been dominated
                return new KeyValuePair<float, object>(chooseOn, choosing);
            return null;
        }

        // process values in the row
        public void processSimpleRow(XtraReport report)
        {
            // pull values for this row, and check whether to update 
            foreach (string column in maxcolumns)
            {
                KeyValuePair<float, object>? noo = null;
                // process at row level
                noo = choose(report, column, rowMaxDominances);
                if (noo != null)
                    rowMaxDominances[column] = (KeyValuePair<float, object>)noo;
                // repeat at set level
                noo = choose(report, column, setMaxDominances);
                if (noo != null)
                    setMaxDominances[column] = (KeyValuePair<float, object>)noo;
            }

        }

        public object GetSimpleRowItem(string pColumnname)
        {
            if (!rowMaxDominances.ContainsKey(pColumnname))   // safety check, in case caller uses wrong column name
                return null;                            // throw error?
            KeyValuePair<float, object> kvp = rowMaxDominances[pColumnname];
            return kvp.Value;
        }

        public object GetSimpleSetItem(string pColumnname)
        {
            if (!setMaxDominances.ContainsKey(pColumnname))   // safety check, in case caller uses wrong column name
                return null;                            // throw error?
            KeyValuePair<float, object> kvp = setMaxDominances[pColumnname];
            return kvp.Value;
        }

        // reset the row
        public void resetSimpleRow()
        {
            // re-init the counters
            foreach (string column in maxcolumns)
            {
                rowMaxDominances[column] = new KeyValuePair<float, object>(0, null);
            }
        }
        #endregion

        #region track dominance during reporting with GroupBy model
        public string GroupByColumn {
            get { return _groupbycolumn; }
            set {
                _groupbycolumn = value;     // store incoming data
                rowGroupBys.Clear();           // reset data store
                setGroupBys.Clear();
            }
        }
        
        // currently tracked gb columns are all weighted; later add support for non-weighted gb columns
        public void SetColumnsOfWeightedInterest(params string[] columnnames)
        {
            // retain access to the comun names
            weightedcolumns = columnnames;

            // init the counters
            foreach (string column in columnnames)
            {
                if (!rowGbWeightedDominances.ContainsKey(column))
                {  // safety check, in case called twice with same column
                    rowGbWeightedDominances.Add(column, new Dictionary<string, double>());
                    setGbWeightedDominances.Add(column, new Dictionary<string, double>());
                }
            }
        }

        private void updateStoredValue(Dictionary<string, double>storage, string grouper, double value)
        {
            double d = storage.ContainsKey(grouper) ? storage[grouper] : 0;
            storage[grouper] = value + d;
        }

        // process values in the row
        public void processGroupByRow(XtraReport report)
        {// keep track of elements at the grouping level

            // get the data element to group on
            string groupon = ReportUtils.GetStringFromColumn(report, GroupByColumn);
            if (string.IsNullOrEmpty(groupon))      // skip further processing, if there was no value to group on
                return;

            double domcol = 0;
            // 1: group-by column itself; sum dominance per groupon
            domcol = ReportUtils.GetDoubleFromColumn(report, DominanceColumn);
            updateStoredValue(rowGroupBys, groupon, domcol);
            updateStoredValue(setGroupBys, groupon, domcol);

            // 2: update each column-of-interest; sum weighted
            foreach (string column in weightedcolumns)
            {
                double d = ReportUtils.GetDoubleFromColumn(report, column) * domcol;
                updateStoredValue(rowGbWeightedDominances[column], groupon, d);
                updateStoredValue(setGbWeightedDominances[column], groupon, d);
            }

        }

        public string GetGroupByRowDominator()
        {
            double maxVal = 0;
            string dominator = "";
            foreach (string groupon in rowGroupBys.Keys)
            {
                double d = rowGroupBys[groupon];
                if (d >= maxVal)    // will take last of equals; will also handle zero dominance situation
                {
                    maxVal = d;
                    dominator = groupon;
                }
            }
            return dominator;
        }

        public string GetGroupBySetDominator()
        {
            double maxVal = 0;
            string dominator = "";
            foreach (string groupon in setGroupBys.Keys)
            {
                double d = setGroupBys[groupon];
                if (d > maxVal)
                {
                    maxVal = d;
                    dominator = groupon;
                }
            }
            return dominator;
        }

        public double GetGroupByRowItem(string pColumnname, string Dominator)
        {
            if (!rowGbWeightedDominances.ContainsKey(pColumnname))
                return 0;    // safety check. throw programming error?
            // send back weighted average
            double denom = rowGroupBys[Dominator];
            return (denom > 0) ? (rowGbWeightedDominances[pColumnname][Dominator]) / denom : 0;
        }

        public object GetGroupBySetItem(string pColumnname, string Dominator)
        {
            if (!setGbWeightedDominances.ContainsKey(pColumnname))
                return 0;    // safety check. throw programming error?
            // send back weighted average
            double denom = setGroupBys[Dominator];
            return (denom > 0) ? (setGbWeightedDominances[pColumnname][Dominator]) / denom : 0;
        }

        // reset group by row
        public void resetGroupByRow()
        {
            rowGroupBys.Clear();
            foreach (string column in weightedcolumns)
                rowGbWeightedDominances[column].Clear();
        }
        #endregion

        #region cleanup
        // reset everything
        public void reset()
        {
            maxcolumns = null;
            weightedcolumns = null;
            rowMaxDominances.Clear();
            setMaxDominances.Clear();
            rowGroupBys.Clear();
            rowGbWeightedDominances.Clear();
            setGbWeightedDominances.Clear();
            DominanceColumn = null;
            GroupByColumn = null;
        }
        #endregion



    }
}
