﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

using SuperACEUtils;
using Temp.DAL;
using System.Collections.Generic;

namespace SuperACE.Reports
{
    public partial class LogStockSortGradeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        float speciesGross = 0;
        float speciesNet = 0;
        float standGross = 0;
        float standNet = 0;
        float logLengthClass = 0;
        float totalNetMbf = 0;

        public LogStockSortGradeSubReport()
        {
            InitializeComponent();
        }

        string TotalNetCol = "TotalNetMbf";     // default to mbf
        string TotalGrossCol = "TotalGrossMbf"; // default to mbf
        string _variant = "Mbf";                 // default to mbf
        private const string placeholder = "{Xxx}";

        public void AssignVariant(string variant)
        {
            _variant = variant;
            switch (variant) 
            {
                case "Ccf":
                    TotalNetCol = "TotalNetCcf";
                    TotalGrossCol = "TotalGrossCcf";
                    break;

                case "Ton":
                    TotalNetCol = "TotalTons";
                    TotalGrossCol = "TotalPerMbf";
                    break;

                default:                        // default to Mbf
                    TotalNetCol = "TotalNetMbf";     
                    TotalGrossCol = "TotalGrossMbf";
                    break;
            }
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ////= SUM(Fields.TotalTons) / SUM(Fields.TotalTotalNetMbf)
            if (this.GetCurrentColumnValue("LogLengthClass") != null)
            {
                logLengthClass = Utils.ConvertToFloat(this.GetCurrentColumnValue("LogLengthClass").ToString());
            }
            string sort = this.GetCurrentColumnValue("Sort").ToString();
            string grade = this.GetCurrentColumnValue("Grade").ToString();
            if (logLengthClass == 0)
            {
                if (string.IsNullOrEmpty(sort))
                    xrLabel24.Text = "Count";
                else
                    xrLabel24.Text = sort;
                if (string.IsNullOrEmpty(grade))
                    xrLabel22.Text = "Count";
                else
                    xrLabel22.Text = grade;
            }
            else
            {
                xrLabel24.Text = sort;
                xrLabel22.Text = grade;
            }
            // if (this.GetCurrentColumnValue("TotalGrossMbf") != null)
            string gross = Convert.ToString(this.GetCurrentColumnValue(TotalGrossCol));
            if (!String.IsNullOrEmpty(gross))
            {
                float f = Convert.ToSingle(gross);
                xrXxxTotalGross.Text = string.Format("{0:#.#}", f);
                speciesGross += f;
                standGross += f;
            }
            string net = Convert.ToString(this.GetCurrentColumnValue(TotalNetCol));
            if (!String.IsNullOrEmpty(net))
            {
                float f = Convert.ToSingle(net);
                xrXxxTotalNet.Text = string.Format("{0:#.#}", f);
                speciesNet += f;
                standNet += f;
            }
            //totalTons += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalTons").ToString());
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel35.Text = string.Format("{0:#.#}", ((standGross - standNet) / standGross) * 100);
            xrStandSumGross.Text = string.Format("{0:#.#}", standGross);
            xrStandSumNet.Text = string.Format("{0:#.#}", standNet);
            // compute the class percents
            float perClass1 = Convert.ToSingle(xrSpeciesSumClass1.Summary.GetResult()) / standNet * 100;
            float perClass2 = Convert.ToSingle(xrSpeciesSumClass2.Summary.GetResult()) / standNet * 100;
            float perClass3 = Convert.ToSingle(xrSpeciesSumClass3.Summary.GetResult()) / standNet * 100;
            float perClass4 = Convert.ToSingle(xrSpeciesSumClass4.Summary.GetResult()) / standNet * 100;
            float perClass5 = Convert.ToSingle(xrSpeciesSumClass5.Summary.GetResult()) / standNet * 100;
            float perClass6 = Convert.ToSingle(xrSpeciesSumClass6.Summary.GetResult()) / standNet * 100;
            float perClass7 = Convert.ToSingle(xrSpeciesSumClass7.Summary.GetResult()) / standNet * 100;
            float perClass8 = Convert.ToSingle(xrSpeciesSumClass8.Summary.GetResult()) / standNet * 100;
            // assign to labels
            xrSpeciesPercentClass1.Text = string.Format("{0:#.#}", perClass1);
            xrSpeciesPercentClass2.Text = string.Format("{0:#.#}", perClass2);
            xrSpeciesPercentClass3.Text = string.Format("{0:#.#}", perClass3);
            xrSpeciesPercentClass4.Text = string.Format("{0:#.#}", perClass4);
            xrSpeciesPercentClass5.Text = string.Format("{0:#.#}", perClass5);
            xrSpeciesPercentClass6.Text = string.Format("{0:#.#}", perClass6);
            xrSpeciesPercentClass7.Text = string.Format("{0:#.#}", perClass7);
            xrSpeciesPercentClass8.Text = string.Format("{0:#.#}", perClass8);
            // sum and assign to sum label
            float sumPerClass = perClass1 + perClass2 + perClass3 + perClass4 + perClass5 + perClass6 + perClass7 + perClass8;
            xrSpeciesPercentSum.Text = string.Format("{0:#.#}", sumPerClass);
            // replace placeholder
            xrXxxStandFooterLabel.Text = xrXxxStandFooterLabel.Text.Replace(placeholder, _variant);
        }

        private void LogStockSortGradeMbfSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptLogStockSortGradeMbf(Convert.ToInt64(this.Parameters["StandsId"].Value));
            List<RptLogStockSortGradeMbf> t = TempBLL.GetRptLogStockSortGradeMbf(Convert.ToInt64(this.Parameters["StandsId"].Value));
            foreach (var item in t)
            {
                totalNetMbf += (float)item.TotalNetMbf;
            }
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel20.Text = string.Format("{0:#.#}", ((speciesGross - speciesNet) / speciesGross) * 100);
            xrLabel8.Text = string.Format("{0:#}", ((float)speciesNet / (float)totalNetMbf) * 100);
            xrSpeciesSumGross.Text = string.Format("{0:#}", speciesGross);
            xrSpeciesSumNet.Text = string.Format("{0:#}", speciesNet);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            speciesGross = 0;
            speciesNet = 0;
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            standGross = 0;
            standNet = 0;
        }

        private void xrLabel23_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (logLengthClass == 0)
            {
                ((XRLabel)sender).ForeColor = Color.Chocolate;
            }
            else
                ((XRLabel)sender).ForeColor = Color.Black;
        }

        private void xrLabel24_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (logLengthClass == 0)
            {
                ((XRLabel)sender).ForeColor = Color.Chocolate;
            }
            else
                ((XRLabel)sender).ForeColor = Color.Black;
        }

        private void xrLabel22_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (logLengthClass == 0)
            {
                ((XRLabel)sender).ForeColor = Color.Chocolate;
            }
            else
                ((XRLabel)sender).ForeColor = Color.Black;
        }
    }
}
