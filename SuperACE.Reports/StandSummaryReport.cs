﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class StandSummaryReport : DevExpress.XtraReports.UI.XtraReport
    {
        float ageTotalHeight = 0;
        float ageFF = 0;
        float ageTreesPerAcre = 0;
        float ageBasalAreaPerAcre = 0;

        float standTotalHeight = 0;
        float standFF = 0;
        float standTreesPerAcre = 0;
        float standBasalAreaPerAcre = 0;

        public StandSummaryReport()
        {
            InitializeComponent();

            bindingSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel43.Text = string.Format("{0:0}", ageTotalHeight / ageTreesPerAcre);
            //xrLabel44.Text = string.Format("{0:0}", ageFF / ageTreesPerAcre);
            //xrLabel45.Text = string.Format("{0:0.0}", Math.Sqrt(ageBasalAreaPerAcre / (ageTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            //ageTotalHeight = 0;
            //ageFF = 0;
            //ageTreesPerAcre = 0;
            //ageBasalAreaPerAcre = 0;
        }

        private void Detail1_AfterPrint(object sender, EventArgs e)
        {
            ////= Sum(Fields.TotalHeight * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sum(Fields.FF * Fields.TreesPerAcre)/Sum(Fields.TreesPerAcre)
            ////= Sqrt(Sum(Fields.BasalAreaPerAcre)/(Sum(Fields.TreesPerAcre) * 0.005454154))
            //ageTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //ageBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    ageTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    ageFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}

            //standTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            //standBasalAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            //{
            //    standTotalHeight += (Utils.ConvertToFloat(xrLabel17.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
            //if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            //{
            //    standFF += (Utils.ConvertToFloat(xrLabel16.Text) * Utils.ConvertToFloat(xrLabel19.Text));
            //}
        }

        private void GroupFooter3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel57.Text = string.Format("{0:0}", standTotalHeight / standTreesPerAcre);
            //xrLabel58.Text = string.Format("{0:0}", standFF / standTreesPerAcre);
            //xrLabel59.Text = string.Format("{0:0.0}", Math.Sqrt(standBasalAreaPerAcre / (standTreesPerAcre * 0.005454154)));
        }

        private void GroupFooter3_AfterPrint(object sender, EventArgs e)
        {
            //standTotalHeight = 0;
            //standFF = 0;
            //standTreesPerAcre = 0;
            //standBasalAreaPerAcre = 0;
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = GetCurrentColumnValue("State").ToString();
            string county = GetCurrentColumnValue("County").ToString();
            if (!string.IsNullOrEmpty(state))
                xrLabel110.Text = state;
            else
                xrLabel110.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel109.Text = state;
            else
                xrLabel109.Text = "Blank";
        }
    }
}
