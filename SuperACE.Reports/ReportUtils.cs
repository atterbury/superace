﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperACE.Reports
{
    class ReportUtils
    {
        #region column extracts
        public static float GetFloatFromColumn(XtraReport report, string pColumnname)
        {
            Object obj = report.GetCurrentColumnValue(pColumnname);
            if (obj == null)
                return 0;
            return Convert.ToSingle(obj);
        }

        public static double GetDoubleFromColumn(XtraReport report, string pColumnname)
        {
            Object obj = report.GetCurrentColumnValue(pColumnname);
            if (obj == null)
                return 0;
            return Convert.ToDouble(obj);
        }

        public static int GetIntFromColumn(XtraReport report, string pColumnname)
        {
            Object obj = report.GetCurrentColumnValue(pColumnname);
            if (obj == null)
                return 0;
            return Convert.ToInt32(obj);
        }

        public static string GetStringFromColumn(XtraReport report, string pColumnname)
        {
            Object obj = report.GetCurrentColumnValue(pColumnname);
            if (obj == null)
                return "";
            // Convert is safe to use for nulls, and returns empty string
            return Convert.ToString(report.GetCurrentColumnValue(pColumnname));
        }
        #endregion

        #region content processing
        public static string StarIfNotUnique(string current, string noo)
        {
            if (string.IsNullOrEmpty(current))
                return noo;
            return current.Equals(noo) ? current : "*"; // replace with star if find differing values
        }

        #endregion

        #region cell formats
        public static void StyleCellHighlights(XRLabel label, bool bHiglighted)
        {
            if (bHiglighted)
                HighlightCellStyling(label);
            else
                ResetCellStyling(label);
        }

        private static void HighlightCellStyling(XRLabel label)
        {
            // label.ForeColor = Color.DarkGray;
            label.Font = new System.Drawing.Font(label.Font.FontFamily, label.Font.Size, FontStyle.Italic);
        }

        private static void ResetCellStyling(XRLabel label)
        {
            // label.ForeColor = Color.Black;
            label.Font = new System.Drawing.Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);
        }
        #endregion

    }
}
