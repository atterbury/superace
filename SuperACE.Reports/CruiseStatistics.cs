﻿using Project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Temp.DAL;

using SuperACEUtils;

namespace SuperACE.Reports
{
    public class CruiseStatistics
    {
        double[] t99 = new double[30] { 63.657, 9.925, 5.841, 4.604, 4.032,3.707, 3.499, 3.355, 3.250, 3.169, 3.106, 3.055, 3.012, 2.977, 2.947,2.921, 2.898, 2.878, 2.861, 2.845, 2.831, 2.819, 2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.576 };
        double[] t95 = new double[30] {12.706, 4.303, 3.182, 2.776, 2.571,2.447, 2.365, 2.306, 2.262, 2.228,2.201, 2.179, 2.160, 2.145, 2.131,2.120, 2.110, 2.101, 2.093, 2.086,2.080, 2.074, 2.069, 2.064, 2.060,2.056, 2.052, 2.048, 2.045, 1.960 };

        DefStat def = null;

        const int array_size = 200;

        private double i45 = 0;
        private double j45 = 0;
        private double k45 = 0;
        private double i32 = 0;
        private double j32 = 0;
        private double k32 = 0;
        private double i47 = 0;
        private double j47 = 0;
        private double k47 = 0;
        private double i34 = 0;
        private double j34 = 0;
        private double k34 = 0;

        int 		top, sub;
        int 		records_processed;
        bool 		plt_break,type_break;
        bool 		hundred_percent_plot;
        int 		rtn;
        int 		species_total_sub, sample_total_sub;
        bool first_species;
        bool first_sample;
        bool first_count;

        string 		part_head_finite;
        string 		part_head;
        string		head_finite;

        int 		head_finites;
        int 		head_deviations;

        float avgNetMbf = 0, avgNetCcf = 0, avgTons = 0, avgBA = 0, avgTpa = 0, avgVBar = 0;
        float sampleTreesBdFt = 0, sampleTreesCuFt = 0;

        float 	standard_deviation, coefficient, standard_error, confidence_level;
        float 	standard_error_percent, actual_value, actual_value_squared;
        float 	squared_value, mean, adj_trees, adj_basal_area;
        float 	rl_value;
        bool add_new_plot = false;
        bool first_plot = false;

        int 	given_n;

        float 	total_count_plots, total_blank_plots, total_reforest_plots, total_baf_plots, total_fixed_plots, total_strip_plots;
        float 	total_count_trees, total_reforest_trees, total_tpa, total_reforest_blank_plots;

        float 	total_cruised_plots, total_cruised_trees;
        float 	total_trees, total_plots;
        float 	total_prism_factor;
        float 	type_average_d4h;
        float 	type_average_baf;
        float 	type_trees_per_acre;
        float 	plt_radius, plt_area, plots_per_acre;

        float 	total_100_percent, work_count_trees, work_cruised_trees, work_reforest_trees;
        int   	cnt_sub, cnt_top;

        // Count data
        //CountData[] cnt = new CountData[array_size];
        string[] cnt_species = new string[array_size];
        float[] cnt_baf = new float[array_size];
        string[] cnt_plot = new string[array_size];

        /*  plot table   */
        List<PlotData> pltData = new List<PlotData>();
        List<SpeciesData> speciesData = new List<SpeciesData>();

        int 		plt_sub, plt_top, plt_total_sub;
        string[] plt_number = new string[array_size];
        int[] plt_baf = new int[array_size];
        int[] plt_cruised_trees = new int[array_size];
        float[] plt_trees_per_acre = new float[array_size];
        float[] plt_basal_area = new float[array_size];
        float[] plt_net_scribner = new float[array_size];
        float[] plt_net_cubic = new float[array_size];
        float[] plt_bole_length = new float[array_size];
        float[] plt_gross_scribner = new float[array_size];
        float[] plt_gross_cubic = new float[array_size];
        float[] plt_na_basal_area = new float[array_size];
        float[] plt_na_trees_per_acre = new float[array_size];
        float[] plt_na_net_scribner = new float[array_size];
        float[] plt_na_net_cubic = new float[array_size];
        float plt_sqd_basal_area;
        float plt_sqd_trees_per_acre;
        float plt_sqd_net_scribner;
        float plt_sqd_net_cubic;
        float plt_sqd_tons;
        float plt_sqd_vbar;

										          /* species table */

        int  type_sub, type_top;
        string last_species;
        string last_plot;
        string last_count_species;
        string last_count_plot;
        //SpeciesData[] species = new SpeciesData[array_size];
        //string[] species = new string[array_size];
        SpeciesGroup[] speciesGroup = new SpeciesGroup[array_size];
        float[] species_cruised_baf = new float[array_size];
        float[] species_count_baf = new float[array_size];
        float[] species_adj_factor = new float[array_size];
        int[] species_cruised_trees = new int[array_size];
        float[] species_trees_per_acre = new float[array_size];
        float[] species_na_trees_per_acre = new float[array_size];
        float[] species_basal_area = new float[array_size];
        float[] species_net_scribner = new float[array_size];
        float[] species_net_cubic = new float[array_size];
        float[] species_bole_length = new float[array_size];
        float[] species_gross_scribner = new float[array_size];
        float[] species_gross_cubic = new float[array_size];
        float[] species_na_basal_area = new float[array_size];
        float[] species_na_gross_scribner = new float[array_size];
        float[] species_na_gross_cubic = new float[array_size];
        float[] species_na_net_scribner = new float[array_size];
        float[] species_na_net_cubic = new float[array_size];

        float[] species_na_tons = new float[array_size];
        float[] species_na_vbar = new float[array_size];

        //TempData[] temp = new TempData[array_size];
        float[] temp_basal_area = new float[array_size];
        float[] temp_trees_per_acre = new float[array_size];
        float[] temp_scribner = new float[array_size];
        float[] temp_cubic = new float[array_size];
        float[] temp_tons = new float[array_size];
        float[] temp_vbar = new float[array_size];
        float type_acres;

        int sample_sub,sample_top;
        int sample_trees;
        //SampleData[] sample = new SampleData[array_size];
        string[] sample_species = new string[array_size];
        float[] sample_board_feet = new float[array_size];
        float[] sample_sqd_board_feet = new float[array_size];

        int default_deviations;
        int lines_needed;

        float trees, basal_area, total_trees_planted_tpa, above_ground_trees;
        float acceptable_root_checks, root_checks;
        float total_plant_spots;
        float plt_sqd_plantable_quads, plt_sqd_acceptable_trees;
        float[] plt_plantable_quads = new float[array_size];
        float[] plt_acceptable_trees = new float[array_size];

        float baPerPlotTotal = 0;
        float tpaPerPlotTotal = 0;
        float CuFtPerPlotTotal = 0;
        float BdFtPerPlotTotal = 0;
        float tonsPerPlotTotal = 0;
        float vbarTotal = 0;

        float baD2Total = 0;
        float tpaD2Total = 0;
        float CuFtD2Total = 0;
        float BdFtD2Total = 0;
        float tonsD2Total = 0;
        float vbarD2Total = 0;

        float baSd = 0;
        float baCv = 0;
        float ba1Sd = 0;
        float baSE = 0;

        float tpaSd = 0;
        float tpaCv = 0;
        float tpa1Sd = 0;
        float tpaSE = 0;

        float CuFtSd = 0;
        float CuFtCv = 0;
        float CuFt1Sd = 0;
        float CuFtSE = 0;

        float BdFtSd = 0;
        float BdFtCv = 0;
        float BdFt1Sd = 0;
        float BdFtSE = 0;

        float tonsSd = 0;
        float tonsCv = 0;
        float tons1Sd = 0;
        float tonsSE = 0;

        float vbarSd = 0;
        float vbarCv = 0;
        float vbar1Sd = 0;
        float vbarSE = 0;

        private List<float> bdFtPerTreeVol = new List<float>();
        private List<float> bdFtPerTreexm = new List<float>();
        private List<float> bdFtPerTreed2 = new List<float>();

        private List<float> cuFtPerTreeVol = new List<float>();
        private List<float> cuFtPerTreexm = new List<float>();
        private List<float> cuFtPerTreed2 = new List<float>();

        double bdSumOfSquaresOfDifferences = 0;
        float bdFtSd = 0;
        float bdFtSe = 0;
        float bdFtCvPercent = 0;
        float bdFtSePercent = 0;
        double cuSumOfSquaresOfDifferences = 0;
        float cuFtSd = 0;
        float cuFtSe = 0;
        float cuFtCvPercent = 0;
        float cuFtSePercent = 0;

    private ProjectDbContext projectContext = null;
        private TempDbContext ctx = null;
        private Project.DAL.Project currentProject;
        //private Stand currentStand;
        private SepStandsSelected currentSelectedStand;
        private string ProjectDataSource;

        public void BuildRptStatistics(ref ProjectDbContext pProjectContext, Project.DAL.Project pProject, SepStandsSelected pStand)
        {
            currentProject = pProject;
            currentSelectedStand = pStand;
            projectContext = pProjectContext;
            //ProjectDataSource = pProjectDataSource;

            int saveId = -1;
            string tSpecies = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0;

            InitSpeciesGroup();

            def = ProjectBLL.GetDefStatistics(ref projectContext);
            ctx = new TempDbContext();
            List<WrkTree> wColl = ctx.WrkTrees.OrderBy(w => w.PlotNumber).ThenBy(w => w.TreeNumber).Where(w => w.StandsSelectedId == currentSelectedStand.StandsSelectedId).ToList();

            total_tpa = 0;
            total_plots = 0;
            total_cruised_plots = 0;
            total_count_plots = 0;
            total_blank_plots = 0;
            total_reforest_plots = 0;
            total_baf_plots = 0;
            total_fixed_plots = 0;
            total_strip_plots = 0;

            avgNetMbf = 0;
            avgNetCcf = 0;
            avgTons = 0;
            avgBA = 0;
            avgTpa = 0;
            avgVBar = 0;

            InitValues();
            add_new_plot = true;
            type_acres = (float)currentSelectedStand.NetGeographicAcres;
            for (int i = 0; i < array_size; i++)
            {
                ZeroPlotData(i);
                ZeroTypeSpecies(i);
                ZeroTreeSample(i);
            }

            foreach (var treeItem in wColl)
            {
                AccumulateSpeciesBaf(treeItem);
            }
            CalculateSpeciesAjustment();

            InitValues();
            //add_new_plot = false;
            //type_acres = atof(master.acres);

            // new 10/13/17
            pltData.Clear();

            bdFtPerTreeVol.Clear();
            bdFtPerTreexm.Clear();
            bdFtPerTreed2.Clear();

            cuFtPerTreeVol.Clear();
            cuFtPerTreexm.Clear();
            cuFtPerTreed2.Clear();

            foreach (var treeItem in wColl)
            {
                ProcessCruiseRecord(treeItem);
                if (treeItem.TreeType == "D")
                {
                    ProcessTree(treeItem);
                    bdFtPerTreeVol.Add((float)treeItem.TotalBdFtNetVolume);
                    cuFtPerTreeVol.Add((float)treeItem.TotalCuFtNetVolume);
                }
            }

            float bdFtPerTreeMean = bdFtPerTreeVol.Average();
            float cuFtPerTreeMean = cuFtPerTreeVol.Average();

            foreach (var bdFtPerTreeItem in bdFtPerTreeVol)
            {
                bdFtPerTreexm.Add((float)Math.Round((double)bdFtPerTreeItem - (double)bdFtPerTreeMean, 0));
                bdFtPerTreed2.Add((float)Math.Round(Math.Pow((bdFtPerTreeItem - (double)bdFtPerTreeMean), 2), 0));
            }
            bdSumOfSquaresOfDifferences = bdFtPerTreed2.Sum();
            bdFtSd = (float)Math.Round(Math.Sqrt(bdSumOfSquaresOfDifferences / (bdFtPerTreeVol.Count - 1)), 0);
            bdFtSe = (float)Math.Round((double)(bdFtSd / Math.Sqrt(bdFtPerTreeVol.Count)), 1);
            bdFtCvPercent = (float)Math.Round((double)((bdFtSd / bdFtPerTreeMean) * 100), 1);
            bdFtSePercent = (float)Math.Round((double)((bdFtSe / bdFtPerTreeMean) * 100), 1);

            foreach (var cuFtPerTreeItem in cuFtPerTreeVol)
            {
                cuFtPerTreexm.Add((float)Math.Round((double)cuFtPerTreeItem - (double)cuFtPerTreeMean, 0));
                cuFtPerTreed2.Add((float)Math.Round(Math.Pow((cuFtPerTreeItem - (double)cuFtPerTreeMean), 2), 0));
            }
            cuSumOfSquaresOfDifferences = cuFtPerTreed2.Sum();
            cuFtSd = (float)Math.Round(Math.Sqrt(cuSumOfSquaresOfDifferences / (cuFtPerTreeVol.Count - 1)), 0);
            cuFtSe = (float)Math.Round((double)(cuFtSd / Math.Sqrt(cuFtPerTreeVol.Count)), 1);
            cuFtCvPercent = (float)Math.Round((double)((cuFtSd / cuFtPerTreeMean) * 100), 1);
            cuFtSePercent = (float)Math.Round((double)((cuFtSe / cuFtPerTreeMean) * 100), 1);

            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string savePlotNumber = string.Empty;
            long saveStandsId = -1;
            float saveBa = 0;
            float saveTpa = 0;
            float saveCuFt = 0;
            float saveBdFt = 0;
            float saveTons = 0;
            float saveMbf = 0;
            float saveCcf = 0;
            float saveVBar = 0;

            foreach (var item in wColl)
            {
                if (string.IsNullOrEmpty(saveKey))
                {
                    saveKey = string.Format("{0}-{1}", item.StandsId, item.PlotNumber);
                    saveStandsId = (long)item.StandsId;
                    savePlotNumber = item.PlotNumber;
                }
                currentKey = string.Format("{0}-{1}", item.StandsId, item.PlotNumber);
                if (saveKey != currentKey)
                {
                    // write
                    PlotData rec = new PlotData();
                    Stand stand = ProjectBLL.GetStand(ref projectContext, (int)saveStandsId);
                    rec.StandsId = saveStandsId;
                    rec.PlotNumber = savePlotNumber;
                    rec.BAPerAcre = saveBa;
                    rec.TreesPerAcre = saveTpa;
                    rec.CuFtPerAcre = saveCuFt;
                    rec.BdFtPerAcre = saveBdFt;
                    rec.TonsPerAcre = saveTons;
                    rec.NetVBar = rec.BdFtPerAcre / rec.BAPerAcre;
                    rec.TotalCcf = (((float)rec.CuFtPerAcre * (float)stand.NetGeographicAcres) / 100) / (float)stand.Plots;
                    rec.TotalMbf = (((float)rec.BdFtPerAcre * (float)stand.NetGeographicAcres) / 1000) / (float)stand.Plots;

                    rec.BAPerPlot = rec.BAPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.BAPerPlotD = 0;
                    rec.BAPerPlotDSqrd = 0;

                    rec.TreesPerPlot = rec.TreesPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.TreesPerPlotD = 0;
                    rec.TreesPerPlotDSqrd = 0;

                    rec.CuFtPerPlot = rec.CuFtPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.CuFtPerPlotD = 0;
                    rec.CuFtPerPlotDSqrd = 0;

                    rec.BdFtPerPlot = rec.BdFtPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.BdFtPerPlotD = 0;
                    rec.BdFtPerPlotDSqrd = 0;

                    rec.TonsPerPlot = rec.TonsPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.TonsPerPlotD = 0;
                    rec.TonsPerPlotDSqrd = 0;

                    if (rec.BdFtPerAcre > 0 && rec.BAPerAcre > 0)
                        rec.VBar = rec.BdFtPerAcre / rec.BAPerAcre;
                    else
                        rec.VBar = 0;
                    rec.VBarD = 0;
                    rec.VBarDSqrd = 0;

                    pltData.Add(rec);

                    // move
                    saveStandsId = (long)item.StandsId;
                    savePlotNumber = item.PlotNumber;
                    saveBa = (float)item.BasalArea;
                    saveTpa = (float)item.TreesPerAcre;
                    saveCuFt = (float)item.TotalNetCubicPerAcre;
                    saveBdFt = (float)item.TotalNetScribnerPerAcre;
                    saveTons = (float)item.TotalTonsPerAcre;
                    saveKey = currentKey;
                }
                else
                {
                    saveBa += (float)item.BasalArea;
                    saveTpa += (float)item.TreesPerAcre;
                    saveCuFt += (float)item.TotalNetCubicPerAcre;
                    saveBdFt += (float)item.TotalNetScribnerPerAcre;
                    saveTons += (float)item.TotalTonsPerAcre;
                }
            }
            PlotData rec1 = new PlotData();
            Stand stand1 = ProjectBLL.GetStand(ref projectContext, (int)saveStandsId);
            rec1.StandsId = saveStandsId;
            rec1.PlotNumber = savePlotNumber;
            rec1.BAPerAcre = saveBa;
            rec1.TreesPerAcre = saveTpa;
            rec1.CuFtPerAcre = saveCuFt;
            rec1.BdFtPerAcre = saveBdFt;
            rec1.TonsPerAcre = saveTons;
            rec1.TotalCcf = (((float)rec1.CuFtPerAcre * (float)stand1.NetGeographicAcres) / 100) / (float)stand1.Plots;
            rec1.TotalMbf = (((float)rec1.BdFtPerAcre * (float)stand1.NetGeographicAcres) / 1000) / (float)stand1.Plots;

            rec1.BAPerPlot = rec1.BAPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.BAPerPlotD = 0;
            rec1.BAPerPlotDSqrd = 0;

            rec1.TreesPerPlot = rec1.TreesPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.TreesPerPlotD = 0;
            rec1.TreesPerPlotDSqrd = 0;

            rec1.CuFtPerPlot = rec1.CuFtPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.CuFtPerPlotD = 0;
            rec1.CuFtPerPlotDSqrd = 0;

            rec1.BdFtPerPlot = rec1.BdFtPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.BdFtPerPlotD = 0;
            rec1.BdFtPerPlotDSqrd = 0;

            rec1.TonsPerPlot = rec1.TonsPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.TonsPerPlotD = 0;
            rec1.TonsPerPlotDSqrd = 0;

            if (rec1.BdFtPerAcre > 0 && rec1.BAPerAcre > 0)
                rec1.VBar = rec1.BdFtPerAcre / rec1.BAPerAcre;
            else
                rec1.VBar = 0;
            rec1.VBarD = 0;
            rec1.VBarDSqrd = 0;

            pltData.Add(rec1);

            baPerPlotTotal = 0;
            tpaPerPlotTotal = 0;
            CuFtPerPlotTotal = 0;
            BdFtPerPlotTotal = 0;
            tonsPerPlotTotal = 0;
            vbarTotal = 0;

            baD2Total = 0;
            tpaD2Total = 0;
            CuFtD2Total = 0;
            BdFtD2Total = 0;
            tonsD2Total = 0;
            vbarD2Total = 0;
            foreach (var pdItem in pltData)
            {
                baPerPlotTotal += pdItem.BAPerPlot;
                tpaPerPlotTotal += pdItem.TreesPerPlot;
                CuFtPerPlotTotal += pdItem.CuFtPerPlot;
                BdFtPerPlotTotal += pdItem.BdFtPerPlot;
                tonsPerPlotTotal += pdItem.TonsPerPlot;
                vbarTotal += pdItem.VBar;
            }

            foreach (var pdItem in pltData)
            {
                pdItem.BAPerPlotD = (float)((pdItem.BAPerPlot - (baPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.BAPerPlotDSqrd = pdItem.BAPerPlotD * pdItem.BAPerPlotD;
                baD2Total += pdItem.BAPerPlotDSqrd;

                pdItem.TreesPerPlotD = (float)((pdItem.TreesPerPlot - (tpaPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.TreesPerPlotDSqrd = pdItem.TreesPerPlotD * pdItem.TreesPerPlotD;
                tpaD2Total += pdItem.TreesPerPlotDSqrd;

                pdItem.CuFtPerPlotD = (float)((pdItem.CuFtPerPlot - (CuFtPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.CuFtPerPlotDSqrd = pdItem.CuFtPerPlotD * pdItem.CuFtPerPlotD;
                CuFtD2Total += pdItem.CuFtPerPlotDSqrd;

                pdItem.BdFtPerPlotD = (float)((pdItem.BdFtPerPlot - (BdFtPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.BdFtPerPlotDSqrd = pdItem.BdFtPerPlotD * pdItem.BdFtPerPlotD;
                BdFtD2Total += pdItem.BdFtPerPlotDSqrd;

                pdItem.TonsPerPlotD = (float)((pdItem.TonsPerPlot - (tonsPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.TonsPerPlotDSqrd = pdItem.TonsPerPlotD * pdItem.TonsPerPlotD;
                tonsD2Total += pdItem.TonsPerPlotDSqrd;

                pdItem.VBarD = (float)((pdItem.VBar - (vbarTotal / pltData.Count)) * def.StdDeviation);
                pdItem.VBarDSqrd = pdItem.VBarD * pdItem.VBarD;
                vbarD2Total += pdItem.VBarDSqrd;
            }

            baSd = (float)Math.Sqrt((double)baD2Total / ((double)pltData.Count - (double)1));
            baCv = (baSd / (baPerPlotTotal / pltData.Count)) * 100;
            ba1Sd = (float)(baSd / Math.Sqrt(pltData.Count));
            baSE = (ba1Sd / (baPerPlotTotal / pltData.Count)) * 100;

            tpaSd = (float)Math.Sqrt((double)tpaD2Total / ((double)pltData.Count - (double)1));
            tpaCv = (tpaSd / (tpaPerPlotTotal / pltData.Count)) * 100;
            tpa1Sd = (float)(tpaSd / Math.Sqrt(pltData.Count));
            tpaSE = (tpa1Sd / (tpaPerPlotTotal / pltData.Count)) * 100;

            CuFtSd = (float)Math.Sqrt((double)CuFtD2Total / ((double)pltData.Count - (double)1));
            CuFtCv = (CuFtSd / (CuFtPerPlotTotal / pltData.Count)) * 100;
            CuFt1Sd = (float)(CuFtSd / Math.Sqrt(pltData.Count));
            CuFtSE = (CuFt1Sd / (CuFtPerPlotTotal / pltData.Count)) * 100;

            BdFtSd = (float)Math.Sqrt((double)BdFtD2Total / ((double)pltData.Count - (double)1));
            BdFtCv = (BdFtSd / (BdFtPerPlotTotal / pltData.Count)) * 100;
            BdFt1Sd = (float)(BdFtSd / Math.Sqrt(pltData.Count));
            BdFtSE = (BdFt1Sd / (BdFtPerPlotTotal / pltData.Count)) * 100;

            tonsSd = (float)Math.Sqrt((double)tonsD2Total / ((double)pltData.Count - (double)1));
            tonsCv = (tonsSd / (tonsPerPlotTotal / pltData.Count)) * 100;
            tons1Sd = (float)(tonsSd / Math.Sqrt(pltData.Count));
            tonsSE = (tons1Sd / (tonsPerPlotTotal / pltData.Count)) * 100;

            vbarSd = (float)Math.Sqrt((double)vbarD2Total / ((double)pltData.Count - (double)1));
            vbarCv = (vbarSd / (vbarTotal / pltData.Count)) * 100;
            vbar1Sd = (float)(vbarSd / Math.Sqrt(pltData.Count));
            vbarSE = (vbar1Sd / (vbarTotal / pltData.Count)) * 100;

            if (records_processed > 0)
                WriteTempTables(currentSelectedStand);

            ctx.Dispose();
        }

        public void BuildRptStatisticsCombined(ref ProjectDbContext pProjectContext, Project.DAL.Project pProject, float pTotalAcres, float pTotalPlots)
        {
            currentProject = pProject;
            projectContext = pProjectContext;
            //ProjectDataSource = pProjectDataSource;

            int saveId = -1;
            string tSpecies = string.Empty;
            float tTreesAcre = 0, tLogsAcre = 0;
            float tTotalNetBdFt = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0;

            InitSpeciesGroup();

            def = ProjectBLL.GetDefStatistics(ref projectContext);
            ctx = new TempDbContext();
            List<WrkTree> wColl = ctx.WrkTrees.OrderBy(w => w.PlotNumber).ThenBy(w => w.TreeNumber).ToList();

            total_tpa = 0;
            total_plots = 0;
            total_cruised_plots = 0;
            total_count_plots = 0;
            total_blank_plots = 0;
            total_reforest_plots = 0;
            total_baf_plots = 0;
            total_fixed_plots = 0;
            total_strip_plots = 0;
            InitValues();
            add_new_plot = true;
            type_acres = pTotalAcres;
            for (int i = 0; i < array_size; i++)
            {
                ZeroPlotData(i);
                ZeroTypeSpecies(i);
                ZeroTreeSample(i);
            }

            foreach (var treeItem in wColl)
            {
                AccumulateSpeciesBaf(treeItem);
            }
            CalculateSpeciesAjustment();

            InitValues();
            //add_new_plot = false;
            //type_acres = atof(master.acres);

            foreach (var treeItem in wColl)
            {
                ProcessCruiseRecord(treeItem);
                if (treeItem.TreeType == "D")
                { 
                    ProcessTree(treeItem);
                    bdFtPerTreeVol.Add((float)treeItem.TotalBdFtNetVolume);
                    cuFtPerTreeVol.Add((float)treeItem.TotalCuFtNetVolume);
                }
            }

            float bdFtPerTreeMean = bdFtPerTreeVol.Average();
            float cuFtPerTreeMean = cuFtPerTreeVol.Average();

            foreach (var bdFtPerTreeItem in bdFtPerTreeVol)
            {
                bdFtPerTreexm.Add((float)Math.Round((double)bdFtPerTreeItem - (double)bdFtPerTreeMean, 0));
                bdFtPerTreed2.Add((float)Math.Round(Math.Pow((bdFtPerTreeItem - (double)bdFtPerTreeMean), 2), 0));
            }
            bdSumOfSquaresOfDifferences = bdFtPerTreed2.Sum();
            bdFtSd = (float)Math.Round(Math.Sqrt(bdSumOfSquaresOfDifferences / (bdFtPerTreeVol.Count - 1)), 0);
            bdFtSe = (float)Math.Round((double)(bdFtSd / Math.Sqrt(bdFtPerTreeVol.Count)), 1);
            bdFtCvPercent = (float)Math.Round((double)((bdFtSd / bdFtPerTreeMean) * 100), 1);
            bdFtSePercent = (float)Math.Round((double)((bdFtSe / bdFtPerTreeMean) * 100), 1);

            foreach (var cuFtPerTreeItem in cuFtPerTreeVol)
            {
                cuFtPerTreexm.Add((float)Math.Round((double)cuFtPerTreeItem - (double)cuFtPerTreeMean, 0));
                cuFtPerTreed2.Add((float)Math.Round(Math.Pow((cuFtPerTreeItem - (double)cuFtPerTreeMean), 2), 0));
            }
            cuSumOfSquaresOfDifferences = cuFtPerTreed2.Sum();
            cuFtSd = (float)Math.Round(Math.Sqrt(cuSumOfSquaresOfDifferences / (cuFtPerTreeVol.Count - 1)), 0);
            cuFtSe = (float)Math.Round((double)(cuFtSd / Math.Sqrt(cuFtPerTreeVol.Count)), 1);
            cuFtCvPercent = (float)Math.Round((double)((cuFtSd / cuFtPerTreeMean) * 100), 1);
            cuFtSePercent = (float)Math.Round((double)((cuFtSe / cuFtPerTreeMean) * 100), 1);

            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string savePlotNumber = string.Empty;
            long saveStandsId = -1;
            float saveBa = 0;
            float saveTpa = 0;
            float saveCuFt = 0;
            float saveBdFt = 0;
            float saveCcf = 0;
            float saveMbf = 0;
            float saveTons = 0;
            float saveVBar = 0;
            foreach (var item in wColl)
            {
                if (string.IsNullOrEmpty(saveKey))
                {
                    saveKey = string.Format("{0}-{1}", item.StandsId, item.PlotNumber);
                    saveStandsId = (long)item.StandsId;
                    savePlotNumber = item.PlotNumber;
                }
                currentKey = string.Format("{0}-{1}", item.StandsId, item.PlotNumber);
                if (saveKey != currentKey)
                {
                    // write
                    PlotData rec = new PlotData();
                    Stand stand = ProjectBLL.GetStand(ref projectContext, (int)saveStandsId);
                    rec.StandsId = saveStandsId;
                    rec.PlotNumber = savePlotNumber;
                    rec.BAPerAcre = saveBa;
                    rec.TreesPerAcre = saveTpa;
                    rec.CuFtPerAcre = saveCuFt;
                    rec.BdFtPerAcre = saveBdFt;
                    rec.TonsPerAcre = saveTons;
                    rec.NetVBar = rec.BdFtPerAcre / rec.BAPerAcre;
                    rec.TotalCcf = (((float)rec.CuFtPerAcre * (float)stand.NetGeographicAcres) / 100) / (float)stand.Plots;
                    rec.TotalMbf = (((float)rec.BdFtPerAcre * (float)stand.NetGeographicAcres) / 1000) / (float)stand.Plots;

                    rec.BAPerPlot = rec.BAPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.BAPerPlotD = 0;
                    rec.BAPerPlotDSqrd = 0;

                    rec.TreesPerPlot = rec.TreesPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.TreesPerPlotD = 0;
                    rec.TreesPerPlotDSqrd = 0;

                    rec.CuFtPerPlot = rec.CuFtPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.CuFtPerPlotD = 0;
                    rec.CuFtPerPlotDSqrd = 0;

                    rec.BdFtPerPlot = rec.BdFtPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.BdFtPerPlotD = 0;
                    rec.BdFtPerPlotDSqrd = 0;

                    rec.TonsPerPlot = rec.TonsPerAcre * ((float)stand.NetGeographicAcres / (float)stand.Plots);
                    rec.TonsPerPlotD = 0;
                    rec.TonsPerPlotDSqrd = 0;

                    if (rec.BdFtPerAcre > 0 && rec.BAPerAcre > 0)
                        rec.VBar = rec.BdFtPerAcre / rec.BAPerAcre;
                    else
                        rec.VBar = 0;
                    rec.VBarD = 0;
                    rec.VBarDSqrd = 0;

                    pltData.Add(rec);

                    // move
                    saveStandsId = (long)item.StandsId;
                    savePlotNumber = item.PlotNumber;
                    saveBa = (float)item.BasalArea;
                    saveTpa = (float)item.TreesPerAcre;
                    saveCuFt = (float)item.TotalNetCubicPerAcre;
                    saveBdFt = (float)item.TotalNetScribnerPerAcre;
                    saveTons = (float)item.TotalTonsPerAcre;
                    saveKey = currentKey;
                }
                else
                {
                    saveBa += (float)item.BasalArea;
                    saveTpa += (float)item.TreesPerAcre;
                    saveCuFt += (float)item.TotalNetCubicPerAcre;
                    saveBdFt += (float)item.TotalNetScribnerPerAcre;
                    saveTons += (float)item.TotalTonsPerAcre;
                }
            }
            PlotData rec1 = new PlotData();
            Stand stand1 = ProjectBLL.GetStand(ref projectContext, (int)saveStandsId);
            rec1.StandsId = saveStandsId;
            rec1.PlotNumber = savePlotNumber;
            rec1.BAPerAcre = saveBa;
            rec1.TreesPerAcre = saveTpa;
            rec1.CuFtPerAcre = saveCuFt;
            rec1.BdFtPerAcre = saveBdFt;
            rec1.TonsPerAcre = saveTons;
            rec1.TotalCcf = (((float)rec1.CuFtPerAcre * (float)stand1.NetGeographicAcres) / 100) / (float)stand1.Plots;
            rec1.TotalMbf = (((float)rec1.BdFtPerAcre * (float)stand1.NetGeographicAcres) / 1000) / (float)stand1.Plots;

            rec1.BAPerPlot = rec1.BAPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.BAPerPlotD = 0;
            rec1.BAPerPlotDSqrd = 0;

            rec1.TreesPerPlot = rec1.TreesPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.TreesPerPlotD = 0;
            rec1.TreesPerPlotDSqrd = 0;

            rec1.CuFtPerPlot = rec1.CuFtPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.CuFtPerPlotD = 0;
            rec1.CuFtPerPlotDSqrd = 0;

            rec1.BdFtPerPlot = rec1.BdFtPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.BdFtPerPlotD = 0;
            rec1.BdFtPerPlotDSqrd = 0;

            rec1.TonsPerPlot = rec1.TonsPerAcre * ((float)stand1.NetGeographicAcres / (float)stand1.Plots);
            rec1.TonsPerPlotD = 0;
            rec1.TonsPerPlotDSqrd = 0;

            if (rec1.BdFtPerAcre > 0 && rec1.BAPerAcre > 0)
                rec1.VBar = rec1.BdFtPerAcre / rec1.BAPerAcre;
            else
                rec1.VBar = 0;
            rec1.VBarD = 0;
            rec1.VBarDSqrd = 0;

            pltData.Add(rec1);

            baPerPlotTotal = 0;
            tpaPerPlotTotal = 0;
            CuFtPerPlotTotal = 0;
            BdFtPerPlotTotal = 0;
            tonsPerPlotTotal = 0;
            vbarTotal = 0;

            baD2Total = 0;
            tpaD2Total = 0;
            CuFtD2Total = 0;
            BdFtD2Total = 0;
            tonsD2Total = 0;
            vbarD2Total = 0;
            foreach (var pdItem in pltData)
            {
                baPerPlotTotal += pdItem.BAPerPlot;
                tpaPerPlotTotal += pdItem.TreesPerPlot;
                CuFtPerPlotTotal += pdItem.CuFtPerPlot;
                BdFtPerPlotTotal += pdItem.BdFtPerPlot;
                tonsPerPlotTotal += pdItem.TonsPerPlot;
                vbarTotal += pdItem.VBar;
            }

            foreach (var pdItem in pltData)
            {
                pdItem.BAPerPlotD = (float)((pdItem.BAPerPlot - (baPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.BAPerPlotDSqrd = pdItem.BAPerPlotD * pdItem.BAPerPlotD;
                baD2Total += pdItem.BAPerPlotDSqrd;

                pdItem.TreesPerPlotD = (float)((pdItem.TreesPerPlot - (tpaPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.TreesPerPlotDSqrd = pdItem.TreesPerPlotD * pdItem.TreesPerPlotD;
                tpaD2Total += pdItem.TreesPerPlotDSqrd;

                pdItem.CuFtPerPlotD = (float)((pdItem.CuFtPerPlot - (CuFtPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.CuFtPerPlotDSqrd = pdItem.CuFtPerPlotD * pdItem.CuFtPerPlotD;
                CuFtD2Total += pdItem.CuFtPerPlotDSqrd;

                pdItem.BdFtPerPlotD = (float)((pdItem.BdFtPerPlot - (BdFtPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.BdFtPerPlotDSqrd = pdItem.BdFtPerPlotD * pdItem.BdFtPerPlotD;
                BdFtD2Total += pdItem.BdFtPerPlotDSqrd;

                pdItem.TonsPerPlotD = (float)((pdItem.TonsPerPlot - (tonsPerPlotTotal / pltData.Count)) * def.StdDeviation);
                pdItem.TonsPerPlotDSqrd = pdItem.TonsPerPlotD * pdItem.TonsPerPlotD;
                tonsD2Total += pdItem.TonsPerPlotDSqrd;

                pdItem.VBarD = (float)((pdItem.VBar - (vbarTotal / pltData.Count)) * def.StdDeviation);
                pdItem.VBarDSqrd = pdItem.VBarD * pdItem.VBarD;
                vbarD2Total += pdItem.VBarDSqrd;
            }

            baSd = (float)Math.Sqrt((double)baD2Total / ((double)pltData.Count - (double)1));
            baCv = (baSd / (baPerPlotTotal / pltData.Count)) * 100;
            ba1Sd = (float)(baSd / Math.Sqrt(pltData.Count));
            baSE = (ba1Sd / (baPerPlotTotal / pltData.Count)) * 100;

            tpaSd = (float)Math.Sqrt((double)tpaD2Total / ((double)pltData.Count - (double)1));
            tpaCv = (tpaSd / (tpaPerPlotTotal / pltData.Count)) * 100;
            tpa1Sd = (float)(tpaSd / Math.Sqrt(pltData.Count));
            tpaSE = (tpa1Sd / (tpaPerPlotTotal / pltData.Count)) * 100;

            CuFtSd = (float)Math.Sqrt((double)CuFtD2Total / ((double)pltData.Count - (double)1));
            CuFtCv = (CuFtSd / (CuFtPerPlotTotal / pltData.Count)) * 100;
            CuFt1Sd = (float)(CuFtSd / Math.Sqrt(pltData.Count));
            CuFtSE = (CuFt1Sd / (CuFtPerPlotTotal / pltData.Count)) * 100;

            BdFtSd = (float)Math.Sqrt((double)BdFtD2Total / ((double)pltData.Count - (double)1));
            BdFtCv = (BdFtSd / (BdFtPerPlotTotal / pltData.Count)) * 100;
            BdFt1Sd = (float)(BdFtSd / Math.Sqrt(pltData.Count));
            BdFtSE = (BdFt1Sd / (BdFtPerPlotTotal / pltData.Count)) * 100;

            tonsSd = (float)Math.Sqrt((double)tonsD2Total / ((double)pltData.Count - (double)1));
            tonsCv = (tonsSd / (tonsPerPlotTotal / pltData.Count)) * 100;
            tons1Sd = (float)(tonsSd / Math.Sqrt(pltData.Count));
            tonsSE = (tons1Sd / (tonsPerPlotTotal / pltData.Count)) * 100;

            vbarSd = (float)Math.Sqrt((double)vbarD2Total / ((double)pltData.Count - (double)1));
            vbarCv = (vbarSd / (vbarTotal / pltData.Count)) * 100;
            vbar1Sd = (float)(vbarSd / Math.Sqrt(pltData.Count));
            vbarSE = (vbar1Sd / (vbarTotal / pltData.Count)) * 100;

            if (records_processed > 0)
                WriteTempTables(null);

            ctx.Dispose();
        }

        private void InitSpeciesGroup()
        {
            for (int i = 0; i < array_size; i++)
                speciesGroup[i] = new SpeciesGroup();
        }

        private void ZeroPlotData(int sub)
        {
            plt_baf[sub] = 0;
            plt_cruised_trees[sub] = 0;
            plt_trees_per_acre[sub] = 0;
            plt_basal_area[sub] = 0;
            plt_net_scribner[sub] = 0;
            plt_net_cubic[sub] = 0;
            plt_bole_length[sub] = 0;
            plt_gross_scribner[sub] = 0;
            plt_gross_cubic[sub] = 0;
            plt_na_basal_area[sub] = 0;
            plt_na_trees_per_acre[sub] = 0;
            plt_na_net_scribner[sub] = 0;
            plt_na_net_cubic[sub] = 0;

            plt_plantable_quads[sub] = 0;
            plt_acceptable_trees[sub] = 0;
        }

        private void ZeroTreeSample(int sub)
        {
            sample_species[sub] = string.Empty;
            sample_board_feet[sub] = 0;
            sample_sqd_board_feet[sub] = 0;
        }

        private void FindPlotNumber(Temp.DAL.WrkTree treeItem)
        {
            bool done;

            if (treeItem.PlotNumber == last_plot)
                return;
            last_plot = treeItem.PlotNumber;
            add_new_plot = true;

            if (first_plot)
            {
                ZeroPlotData(plt_top);
                plt_number[plt_top] = treeItem.PlotNumber;
                first_plot = false;
                if (add_new_plot)
                    NewPlot(treeItem);
            }

            /*   find the plot                                                  */
            plt_sub = 0;
            done = false;

            while (!done)
            {
                if (plt_sub > plt_top)
                {
                    plt_top = plt_sub;
                    ZeroPlotData(plt_top);
                    plt_number[plt_top] = treeItem.PlotNumber;
                    if (add_new_plot)
                        NewPlot(treeItem);
                    done = true;
                    break;
                }
                if (treeItem.PlotNumber == plt_number[sub])
                    done = true;
                else
                    plt_sub++;
            }
        }

        private void FindSample(Temp.DAL.WrkTree treeItem)
        {
            bool done;

            if (first_sample)
            {
                ZeroTreeSample(sample_top);
                sample_trees = 0;
                sample_species[sample_top] = treeItem.SpeciesAbbreviation;
                first_sample = false;
            }

            /*   find sample                                                    */
            sample_sub = 0;
            done = false;

            while (!done)
            {
                if (sample_sub > sample_top)
                {
                    sample_top = sample_sub;
                    ZeroTreeSample(sample_top);
                    sample_species[sample_top] = treeItem.SpeciesAbbreviation;
                    done = true;
                    break;
                }
                if (treeItem.SpeciesAbbreviation == sample_species[sample_sub])
                    done = true;
                else
                    sample_sub++;
            }
        }

        private void ProcessCruiseRecord(Temp.DAL.WrkTree treeItem)
        {
            FindPlotNumber(treeItem);
            FindTypeSpecies(treeItem);
            FindSample(treeItem);

            records_processed = 1;

            //  Reforestation
            if (treeItem.TreeType == "R")
            {
                work_reforest_trees += (float)treeItem.TreeCount;
                return;
            }

            //records_processed = 1;

            if (!treeItem.PlotNumber.StartsWith("-"))
                hundred_percent_plot = false;
            else
                hundred_percent_plot = true;

            // Count Plot
            if (treeItem.TreeType == "C")
            {
                work_count_trees += (float)treeItem.TreeCount;
                if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                    total_prism_factor += (Utils.ConvertToFloat(treeItem.PlotFactorInput) * (float)treeItem.TreeCount);
                //if (treeItem.TreeCount > 0)
                //{
                    FindCountPlot(treeItem);
                    if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                        cnt_baf[cnt_sub] = (float)treeItem.TreeCount * Utils.ConvertToFloat(treeItem.PlotFactorInput);
                //}
                return;
            }

            if (!treeItem.PlotNumber.StartsWith("-"))
            {
                if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                    total_prism_factor += Utils.ConvertToFloat(treeItem.PlotFactorInput);
                work_cruised_trees += (float)treeItem.TreeCount;
            }

            if (species_adj_factor[type_sub] > 0.00)
            {
                adj_trees = (float)treeItem.TreesPerAcre * species_adj_factor[type_sub];
                adj_basal_area = (float)treeItem.BasalArea * species_adj_factor[type_sub];
            }
            else
            {
                adj_trees = (float)treeItem.TreesPerAcre;
                adj_basal_area = (float)treeItem.BasalArea;
            }

            if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                plt_baf[plt_sub] += Utils.ConvertToInt(treeItem.PlotFactorInput);
            plt_number[plt_sub] = treeItem.PlotNumber;
            plt_cruised_trees[plt_sub] += (int)treeItem.TreeCount;
            plt_trees_per_acre[plt_sub] += adj_trees;
            plt_na_trees_per_acre[plt_sub] += (float)treeItem.TreesPerAcre;
            plt_na_basal_area[plt_sub] += (float)treeItem.BasalArea;
            plt_basal_area[plt_sub] += adj_basal_area;

            species_cruised_trees[type_sub] += (int)treeItem.TreeCount;
            species_na_trees_per_acre[type_sub] += (float)treeItem.TreesPerAcre;
            species_trees_per_acre[type_sub] += adj_trees;
            species_na_basal_area[type_sub] += (float)treeItem.BasalArea;
            species_basal_area[type_sub] += adj_basal_area;

            if (treeItem.TreeType == "D")
            {
                sample_trees += (int)treeItem.TreeCount;
                //total_tpa += (float)treeItem.TreesPerAcre;
            }
        }

        private void ProcessTree(WrkTree treeItem)
        {
            float temporary_scribner;
            float unadjusted_scribner;

            if (string.IsNullOrEmpty(treeItem.Length1))
                return;

            temporary_scribner = 0;
            unadjusted_scribner = 0;

            sampleTreesBdFt += (float)treeItem.TotalBdFtNetVolume;
            sampleTreesCuFt += (float)treeItem.TotalCuFtNetVolume;

            plt_gross_cubic[plt_sub] += ((float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre);
            plt_gross_scribner[plt_sub] += ((float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre);
            plt_na_net_cubic[plt_sub] += ((float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre);
            plt_na_net_scribner[plt_sub] += ((float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre);
            plt_net_scribner[plt_sub] += ((float)treeItem.TotalBdFtNetVolume * adj_trees);
            plt_net_cubic[plt_sub] += ((float)treeItem.TotalCuFtNetVolume * adj_trees);
            plt_bole_length[plt_sub] += ((float)treeItem.BoleHeight * adj_trees);

            species_na_gross_cubic[type_sub] += ((float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre);
            species_na_gross_scribner[type_sub] += ((float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre);
            species_gross_cubic[type_sub] += ((float)treeItem.TotalCuFtGrossVolume * adj_trees);
            species_gross_scribner[type_sub] += (float)(treeItem.TotalBdFtGrossVolume * adj_trees);
            species_na_net_cubic[type_sub] += ((float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre);
            species_na_net_scribner[type_sub] += ((float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre);
            species_net_scribner[type_sub] += ((float)treeItem.TotalBdFtNetVolume * adj_trees);
            species_net_cubic[type_sub] += ((float)treeItem.TotalCuFtNetVolume * adj_trees);
            species_bole_length[type_sub] += ((float)treeItem.BoleHeight * adj_trees);

            species_na_tons[type_sub] += (float)treeItem.TotalTonsPerAcre * adj_trees;

            temporary_scribner += ((float)treeItem.TotalBdFtNetVolume * adj_trees);
            unadjusted_scribner += (float)treeItem.TotalBdFtNetVolume;

            sample_board_feet[sample_sub] += unadjusted_scribner;
            sample_sqd_board_feet[sample_sub] += (float)unadjusted_scribner * unadjusted_scribner;
        }

        private void AccumulateSpeciesBaf(WrkTree treeItem)
        {
            FindPlotNumber(treeItem);
            FindTypeSpecies(treeItem);
            FindSample(treeItem);

            /*   Reforestation                                                  */
            if (treeItem.TreeType == "R")
                return;

            if (!treeItem.PlotNumber.StartsWith("-"))
                hundred_percent_plot = false;
            else
                hundred_percent_plot = true;

            /*   Count Plot                                                     */
            if (treeItem.TreeType != "D")
            {
                work_count_trees += (float)treeItem.TreeCount;
                if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                    species_count_baf[type_sub] += (Utils.ConvertToFloat(treeItem.PlotFactorInput) * (float)treeItem.TreeCount);
                return;
            }

            if (Utils.ConvertToFloat(treeItem.PlotFactorInput) > 0)
                species_cruised_baf[type_sub] += Utils.ConvertToFloat(treeItem.PlotFactorInput);
            if (!hundred_percent_plot)
                work_cruised_trees += (float)treeItem.TreeCount;
        }

        private void ZeroTypeSpecies(int sub)
        {
            speciesGroup[sub].Species = string.Empty;
            speciesGroup[sub].Status = string.Empty;
            species_cruised_trees[sub] = 0;
            species_trees_per_acre[sub] = 0;
            species_na_trees_per_acre[sub] = 0;
            species_basal_area[sub] = 0;
            species_net_scribner[sub] = 0;
            species_net_cubic[sub] = 0;
            species_bole_length[sub] = 0;
            species_gross_scribner[sub] = 0;
            species_gross_cubic[sub] = 0;
            species_na_basal_area[sub] = 0;
            species_na_gross_scribner[sub] = 0;
            species_na_gross_cubic[sub] = 0;
            species_na_net_scribner[sub] = 0;
            species_na_net_cubic[sub] = 0;

            species_na_tons[sub] = 0;
            species_na_vbar[sub] = 0;
        }

        private void FindTypeSpecies(WrkTree treeItem)
        {
            type_sub = 0;

            if (first_species)
            {
                speciesGroup[type_top].Species = treeItem.SpeciesAbbreviation;
                speciesGroup[type_top].Status = treeItem.TreeStatusDisplayCode;
                first_species = false;
            }

            while (true)
            {
                if (type_sub > type_top)
                {
                    type_top = type_sub;
                    ZeroTypeSpecies(type_top);
                    speciesGroup[type_top].Species = treeItem.SpeciesAbbreviation;
                    speciesGroup[type_top].Status = treeItem.TreeStatusDisplayCode.Trim();
                    return;
                }
                if (treeItem.SpeciesAbbreviation == speciesGroup[type_sub].Species && treeItem.TreeStatusDisplayCode == speciesGroup[type_sub].Status)
                    return;
                type_sub++;
            }
        }

        private void FindCountPlot(WrkTree treeItem)
        {
            bool done;

            if (treeItem.PlotNumber == last_count_plot && treeItem.SpeciesAbbreviation == last_count_species)
                return;
            last_count_plot = treeItem.PlotNumber;
            last_count_species = treeItem.SpeciesAbbreviation;

            if (first_count)
            {
                cnt_plot[cnt_top] = treeItem.PlotNumber;
                cnt_species[cnt_top] = treeItem.SpeciesAbbreviation;
                first_count = false;
            }

            /*   find the count plot                                            */
            cnt_sub = 0;
            done = false;

            while (!done)
            {
                if (cnt_sub > cnt_top)
                {
                    cnt_top = cnt_sub;
                    cnt_baf[cnt_top] = 0;
                    cnt_plot[cnt_top] = treeItem.PlotNumber;
                    cnt_species[cnt_top] = treeItem.SpeciesAbbreviation;
                    done = true;
                    break;
                }
                if (treeItem.PlotNumber == cnt_plot[cnt_sub] && treeItem.SpeciesAbbreviation == cnt_species[cnt_sub])
                    done = true;
                else
                    cnt_sub++;
            }
        }

        private void AccumulatePlotAnswers()
        {
            total_trees = work_count_trees + work_cruised_trees;
            total_count_trees += work_count_trees;
            total_reforest_trees += work_reforest_trees;
            total_cruised_trees += work_cruised_trees;
        }

        private void PrintHeading()
        {
            //static char temp[31];

            //page++;

            //prt_position(1, (80-19)/2, "STATISTICAL SUMMARY");
            //prt_position(3, 1, "ATTERBURY CONSULTANTS, INC.                     Plot: %-3.0f    PAGE: %-d",
            //        total_plots, page);
            //get_date(temp);
            //prt_position(4, 1, "PROJECT: %-8s   TRACT: %-15s      Tree: %-3.0f    DATE: %-s",
            //        master.project, master.tract, total_cruised_trees, temp);
            //prt_position(5, 1, "TWP:");
            //strncpy(temp, master.trst, 3);
            //temp[3] = '\0';
            //prt_position(5, 6, "%3s", temp);
            //temp[0] = master.trst[3]; temp[1] = master.trst[4];
            //temp[2] = master.trst[5]; temp[3] = '\0';
            //prt_position(5, 10, "RGE: %3s", temp);
            //temp[0] = master.trst[6]; temp[1] = master.trst[7];
            //temp[2] = '\0';
            //prt_position(5, 19, "SEC: %2s", temp);
            //temp[0] = master.trst[8]; temp[1] = master.trst[9];
            //temp[2] = '\0';
            //prt_position(5, 27, "TY: %2s", temp);
            //prt_position(5, 34, "AC: %7s", master.acres);
            //prt_position(5, 63, "TIME:");
            //get_time(temp);
            //prt_position(5, 69, "%8s", temp);

            //prt_position(7, 1,
            //        "-------------------------------------------------------------------------------");
            //row = 8;
        }

        private void PrintTypeOfCruise()
        {
            int str_ave_pf;
            string temp;

            type_average_baf = total_prism_factor / total_trees;
            temp = string.Format("%4.0f", type_average_baf);
            str_ave_pf = Utils.ConvertToInt(temp);

            //if (Utils.ConvertToFloat(MainForm.CurrentStand. master.fixed_factor[0]) > 0 || atof(master.fixed_factor[1]) > 0 ||
            //        atof(master.fixed_factor[2]) > 0) {
            //        prt_position(row, 1, "FIXED AREA PLOT SIZE:   F1: %-s   F2: %-s   F3: %-s",
            //                master.fixed_factor[0], master.fixed_factor[1],
            //                master.fixed_factor[2]);
            //}
            //else {
            //        if (atof(master.fixed_factor[0]) > 0 || atof(master.fixed_factor[1]) > 0 ||
            //                atof(master.fixed_factor[2]) > 0) {
            //                prt_position(row, 1, "STRIP CRUISE BLOWUP FACTOR:    F1: %-s   F2: %-s   F3: %-s",
            //                        master.strip_cruise[0], master.strip_cruise[1],
            //                        master.strip_cruise[2]);
            //        }
            //        else {
            //                prt_position(row, 1, "PRISM CRUISE  PLUS 100%%     %-d BAF",
            //                        str_ave_pf);
            //        }
            //}
        }

        private void WriteSampleData(SepStandsSelected pCurrentSelectedStand)
        {
            float str_ave_trees;
            float tTreesPerAcre = 0;
            float tEstTotalTrees = 0;
            float tTreesPerPlot = 0;

            str_ave_trees = 0;
            if (total_plots > 0)
                str_ave_trees = total_trees / total_plots;

            str_ave_trees = 0;
            if (total_cruised_plots > 0)
                str_ave_trees = total_cruised_trees / total_cruised_plots;
            // string pName, float pCruisePlots, float pBlankPlots, float pSampleTrees, float pDBHTrees, float pCountTrees, 
            // float p100Trees, float pTreesPerPlot, float pTreesPerAcre, float pEstTotalTrees, float pPercentSampleTrees
            AddSampleData(pCurrentSelectedStand, 
                "BAF Plots (B)",
                (float)total_baf_plots, // CruisePlots
                (float)total_blank_plots, // pBlankPlots
                (float)total_cruised_trees, // pSampleTrees
                (float)0.0, //pDBHTrees
                (float)0, // pCountTrees
                (float)total_100_percent, // p100Trees
                (float)total_cruised_trees / total_baf_plots, // pTreesPerPlot
                (float)species_trees_per_acre[species_total_sub]/total_baf_plots, //((total_tpa/total_baf_plots) * type_acres)/type_acres, // pTreesPerAcre
                (float)(species_trees_per_acre[species_total_sub]/total_baf_plots) / type_acres, // pEstTotalTrees
                (float)(total_cruised_trees) / ((species_trees_per_acre[species_total_sub]/total_baf_plots) / type_acres), 
                (float)0); // pPercentSampleTrees
            str_ave_trees = 0;
            tTreesPerAcre = 0;
            tEstTotalTrees = 0;
            tTreesPerPlot = 0;
            if (total_count_plots > 0)
            {
                str_ave_trees = total_count_trees / total_count_plots;
                tTreesPerAcre = (float)(species_trees_per_acre[species_total_sub] / total_count_plots) / type_acres;
                tEstTotalTrees = (float)(total_cruised_trees) / ((species_trees_per_acre[species_total_sub] / total_count_plots) / type_acres);
                tTreesPerPlot = (float)species_trees_per_acre[species_total_sub] / total_count_plots;
            }
            AddSampleData(pCurrentSelectedStand, 
                "BAF Count Plots (B)",
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)total_count_trees,
                (float)0.0,
                (float)str_ave_trees,
                (float)tTreesPerPlot,
                (float)tTreesPerAcre,
                (float)tEstTotalTrees,
                (float)total_count_plots);
            if (total_fixed_plots > 0)
            {
                AddSampleData(pCurrentSelectedStand, 
                    "Fixed Area (F,R,V)",
                    (float) total_fixed_plots,
                    (float) total_blank_plots,
                    (float) total_cruised_trees,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0,
                    (float) 0.0);
            }
            else
                if (total_reforest_plots > 0)
                {
                    AddSampleData(pCurrentSelectedStand, 
                        "Fixed Area (F,R,V)",
                        (float)total_reforest_plots,
                        (float)total_reforest_blank_plots,
                        (float)total_reforest_trees,
                        (float)0.0,
                        (float)total_count_trees,
                        (float)total_100_percent,
                        (float)total_reforest_trees / total_reforest_plots,
                        (float)species_trees_per_acre[species_total_sub] / total_reforest_plots,
                        (float)(species_trees_per_acre[species_total_sub] / total_reforest_plots) / type_acres,
                        (float)(total_reforest_trees) / ((species_trees_per_acre[species_total_sub] / total_reforest_plots) / type_acres),
                        (float)0.0);
                }
                else
                {
                    AddSampleData(pCurrentSelectedStand, 
                        "Fixed Area (F,R,V)",
                        (float)0,
                        (float)0,
                        (float)0,
                        (float)0.0,
                        (float)0,
                        (float)0,
                        (float)0,
                        (float)0,
                        (float)0,
                        (float)0,
                        (float)0.0);
                }
            AddSampleData(pCurrentSelectedStand, 
                "Strip Plots (S)",
                (float)total_strip_plots,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0);
            AddSampleData(pCurrentSelectedStand, 
                "Rectang Plots (A)",
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0);
            AddSampleData(pCurrentSelectedStand, 
                "100% (00)",
                (float)total_100_percent,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0,
                (float)0.0);
            ctx.SaveChanges();
        }

        private void AddSampleData(SepStandsSelected pCurrentSelectedStand, string pName, float pCruisePlots, float pBlankPlots, float pSampleTrees, float pDBHTrees, float pCountTrees, 
            float p100Trees, float pTreesPerPlot, float pTreesPerAcre, float pEstTotalTrees, float pPercentSampleTrees, float pCountPlots)
        {
            StatsSampleData ssd = new StatsSampleData();
            if (pCurrentSelectedStand != null)
            {
                ssd.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                ssd.StandsId = pCurrentSelectedStand.StandsId;
            }
            else
            {
                ssd.StandsSelectedId = -1;
                ssd.StandsId = -1;
            }
            ssd.Name = pName;
            ssd.CruisePlots = pCruisePlots;
            ssd.BlankPlots = pBlankPlots;
            ssd.BafCount = pCountPlots;
            ssd.SampleTrees = pSampleTrees;
            ssd.DbhTrees = pDBHTrees;
            ssd.CountTrees = pCountTrees;
            ssd.C100Trees = p100Trees;
            ssd.TreesPerPlot = pTreesPerPlot;
            ssd.TreesPerAcre = pTreesPerAcre;
            ssd.EstTotalTrees = pTreesPerAcre * type_acres;
            if (pSampleTrees > 0 && (pTreesPerAcre * type_acres) > 0)
                ssd.PercentSampleTrees = (pSampleTrees / (pTreesPerAcre * type_acres) * 100);
            else
                ssd.PercentSampleTrees = 0.0;
            ctx.StatsSampleDatas.Add(ssd);
        }

        private void MoveStandSummary(SepStandsSelected pCurrentSelectedStand, int sub, bool tflg)
        {
            float str_d4h, tsdi;

            str_d4h = 0;

            Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, currentProject.SpeciesTableName, speciesGroup[type_sub].Species);
            //if (!tflg)
            //    validate_sdi(species[type_sub]);
            if (species_trees_per_acre[sub] > 0)
                str_d4h = (float)Math.Sqrt(species_basal_area[sub] / (species_trees_per_acre[sub] * 0.005454154));
            //prt_position(row, 12, "%-d", species_cruised_trees[sub]);
            //prt_position(row, 18, "%-4.1f", species_trees_per_acre[sub]);
            //prt_position(row, 25, "%-4.1f", str_d4h);
            //prt_position(row, 31, "%-3.0f", species_bole_length[sub] / species_trees_per_acre[sub]);
            //if (!tflg)
            //{
            //    tsdi = species_trees_per_acre[sub] * ((str_d4h / 10) * 1.6);
            //    tsdi = (tsdi / atof(sdi.Sdi)) * 100;
            //    prt_position(row, 36, "%3d", (int)tsdi);
            //}
            //prt_position(row, 41, "%-4.1f",
            //           species_basal_area[sub]);
            //prt_position(row, 48, "%-5.0f", species_gross_scribner[sub]);
            //prt_position(row, 56, "%-5.0f", species_net_scribner[sub]);
            //prt_position(row, 64, "%-5.0f", species_gross_cubic[sub]);
            //prt_position(row, 72, "%-5.0f", species_net_cubic[sub]);

            long id = -1;
            if (pCurrentSelectedStand != null)
                id = (long)pCurrentSelectedStand.StandsId;
            RptSpeciesSummaryLogsVolume rsslv = TempBLL.GetRptSpeciesSummaryLogVolumeRecord(id, speciesGroup[type_sub].Species, speciesGroup[type_sub].Status);
            //TempStandData tsd = null;
            //if (string.IsNullOrEmpty(rsslv.Status))
            //    tsd = ctx.TempStandDatas.FirstOrDefault(t => t.TractName == currentSelectedStand.TractName && t.StandName == currentSelectedStand.StandName &&
            //        t.Species == spc.Abbreviation);
            //else
            //    tsd = ctx.TempStandDatas.FirstOrDefault(t => t.TractName == currentSelectedStand.TractName && t.StandName == currentSelectedStand.StandName &&
            //        t.Species == spc.Abbreviation && t.Status == rsslv.Status);
            AddStandSummaryData(pCurrentSelectedStand, spc.Abbreviation, spc.Description, rsslv.Status, species_cruised_trees[sub], (float)rsslv.QmDbh, (float)rsslv.AvgTreeHeight, (float)rsslv.TreesAcre, (float)rsslv.BasalArea, 
                0, (float)rsslv.NetCuFtAcre, (float)rsslv.NetBdFtAcre, (float)rsslv.TotalNetCunits, (float)rsslv.TotalNetMbf, (float)rsslv.TonsAcre, (float)rsslv.CvPercent, (float)rsslv.SePercent);
        }


        private void AddStandSummaryData(SepStandsSelected pCurrentSelectedStand, string pSpecies, string pDesc, string pStatus, float pSampleTrees, float pQmDbh, 
            float pAvgTotalHt, float pTreesPerAcre, float pBasalAreaPerAcre, 
            float pRD, float pNetCuFtPerAcre, float pNetBdFtPerAcre, float pTotalNetCcf, float pTotalNetMbf, float pTotalTonsAcre, float pCV, float pSE)
        {
            StatsStandSummary sss = new StatsStandSummary();
            if (pCurrentSelectedStand != null)
            {
                sss.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                sss.StandsId = pCurrentSelectedStand.StandsId;
            }
            else
            {
                sss.StandsSelectedId = -1;
                sss.StandsId = -1;
            }
            sss.AvgTotalHt = pAvgTotalHt;
            sss.BasalAreaPerAcre = pBasalAreaPerAcre; // / total_plots;
            sss.NetBdFtPerAcre = (double)pNetBdFtPerAcre/1000; // (double)(pNetBdFtPerAcre / total_plots) / type_acres;
            sss.NetCuFtPerAcre = (double)pNetCuFtPerAcre/100; // (pNetCuFtPerAcre / total_plots) / type_acres;
            sss.QmDbh = (double)pQmDbh;
            sss.Rd = sss.BasalAreaPerAcre / Math.Pow(sss.QmDbh, 0.5); // pRD;
            sss.SampleTrees = pSampleTrees;
            sss.Species = pDesc;
            sss.Status = pStatus;
            sss.TotalNetCcf = (double)pTotalNetCcf; //(sss.NetCuFtPerAcre * type_acres) / 100; // pTotalNetCcf;
            sss.TotalNetMbf = (double)pTotalNetMbf; //(sss.NetBdFtPerAcre * type_acres) / 1000; // pTotalNetMbf;
            sss.TreesPerAcre = (double)pTreesPerAcre; // pTreesPerAcre / total_plots;
            sss.TonsPerAcre = pTotalTonsAcre;
            sss.MbfCv = pCV;
            sss.MbfSd = pSE;

            avgBA += (float)sss.BasalAreaPerAcre;
            avgNetCcf += (float)sss.NetCuFtPerAcre;
            avgNetMbf += (float)sss.NetBdFtPerAcre;
            avgTons += (float)sss.TonsPerAcre;
            avgTpa += (float)sss.TreesPerAcre;
            avgVBar += (float)sss.NetBdFtPerAcre/(float)sss.BasalAreaPerAcre;

            ctx.StatsStandSummaries.Add(sss);
        }

        private void WriteStandSummary(SepStandsSelected pCurrentSelectedStand)
        {
            //bool sw_head;

            //sw_head = true;
            type_sub = 0;
            while (type_sub <= type_top)
            {
                //if (sw_head)
                //{
                //    prt_position(++row, ((80 - 13) / 2), "STAND SUMMARY");
                //    prt_position(++row, 10,
                //        "SAMPLE  Tree  AVE   BOL  REL  BASAL  GROSS    NET    GROSS    NET");
                //    prt_position(++row, 10,
                //        " Tree  /ACRE  D4H   LEN  DEN  AREA   BF/AC   BF/AC   CF/AC   CF/AC");
                //}
                //sw_head = false;
                //prt_position(++row, 1, "%-s  ", spc.desc);
                MoveStandSummary(pCurrentSelectedStand, type_sub, false);
                //    if (row > 57)
                //    {
                //        print_heading();
                //        sw_head = true;
                //    }
                type_sub++;
            }
            ctx.SaveChanges();
        }

        private void PrintCruiseSummaryTotal()
        {
            //prt_position(++row, 13,
            //        "------------------------------------------------------------------");
            //prt_position(++row, 1, "TOTAL     ");
            //move_cruise_summary(species_total_sub, 1);
            //row++;
            //prt_position(++row, 1,
            //      "------------------------------------------------------------------------------");
            type_average_d4h = 0;
            if (species_trees_per_acre[species_total_sub] > 0)
                type_average_d4h = (float)Math.Sqrt(species_basal_area[species_total_sub] / (species_trees_per_acre[species_total_sub] * 0.005454));
        }

        private void ComputeDeviations(SepStandsSelected pCurrentSelectedStand)
        {
            float percent = 0, tmp = 0;
            float low, high, possible_plots, five_percent, ten_percent;
            float fifteen_percent, top_of_n, bottom_of_n;
            float t_value;
            int x, given_n;

            given_n = (int)total_plots;
            if (rtn == 1)
                given_n = sample_trees;
            if (given_n < 2)
                given_n = 2;  /* 04-27-90 */
            mean = actual_value / given_n;
            actual_value_squared = actual_value * actual_value;
            rl_value = squared_value - actual_value_squared / given_n;

            if (rl_value > 0)
                standard_deviation = (float)Math.Sqrt(rl_value / (given_n - 1));
            else
                standard_deviation = 0;

            if (mean > 0)
                coefficient = standard_deviation / mean;
            else
                coefficient = 0;
            standard_error = standard_deviation / (float)Math.Sqrt(given_n);

            if (mean > 0)
                standard_error_percent = standard_error / mean;
            else
                standard_error_percent = 0;

            t_value = head_deviations;

            if (head_deviations == 1 || given_n > 30)
            {
            }
            else
            {
                x = given_n - 1;
                if (x < 1)
                    x = 1;
                if (head_deviations == 2)
                    t_value = (float)t95[x];
                if (head_deviations == 3)
                    t_value = (float)t99[x];
            }
            low = mean - t_value * standard_error;
            if (low < 0.0)
                low = 0;
            high = mean + t_value * standard_error;

            standard_error_percent = standard_error_percent * 100;
            coefficient = (float)100.0 * coefficient;

            t_value = head_deviations;

            if (head_deviations == 1 || given_n > 30)
            {
            }
            else
            {
                x = given_n - 1;
                if (x < 1)
                    x = 1;
                if (head_deviations == 2)
                    t_value = (float)t95[x];
                if (head_deviations == 3)
                    t_value = (float)t99[x];
            }

            possible_plots = type_acres * (float)5.0;
            possible_plots = type_acres * plots_per_acre;
            x = given_n;
            top_of_n = possible_plots * (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            bottom_of_n = possible_plots * (float)25.0 + (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            if (bottom_of_n > 0)
                five_percent = top_of_n / bottom_of_n;
            else
                five_percent = 0;
            top_of_n = possible_plots * (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            bottom_of_n = possible_plots * (float)100.0 + (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            if (bottom_of_n > 0)
                ten_percent = top_of_n / bottom_of_n;
            else
                ten_percent = 0;
            top_of_n = possible_plots * (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            bottom_of_n = possible_plots * (float)225 + (float)Math.Sqrt(t_value) * (float)Math.Sqrt(coefficient);
            if (bottom_of_n > 0)
                fifteen_percent = top_of_n / bottom_of_n;
            else
                fifteen_percent = 0;
            if (head_finite == "F")
            {
                five_percent = (float)Math.Sqrt(coefficient) * (float)Math.Sqrt(t_value) / (float)25.0;
                ten_percent = (float)Math.Sqrt(coefficient) * (float)Math.Sqrt(t_value) / (float)100.0;
                fifteen_percent = (float)Math.Sqrt(coefficient) * (float)Math.Sqrt(t_value) / (float)225.0;
            }

            switch (rtn)
            {
                case 4: // was 3
                    Temp.DAL.StatsPerAcre spa3 = new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa3.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa3.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa3.StandsSelectedId = -1;
                        spa3.StandsId = -1;
                    }
                    spa3.Species = "BA per Acre";
                    spa3.Seq = 4;
                    spa3.Sd = (double)def.StdDeviation;
                    spa3.Cl = (double)def.Confidence;
                    spa3.CvPercent = baCv; // coefficient;
                    spa3.SePercent = baSE; // standard_error_percent;
                    percent = baSE / 100; // standard_error_percent / 100;
                    tmp = avgBA * percent;
                    spa3.Low = avgBA - tmp; //low;
                    spa3.Avg = avgBA; // mean;
                    spa3.High = avgBA + tmp; // high;
                    spa3.Trees5 = (Math.Pow(spa3.Sd, 2)*Math.Pow(spa3.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spa3.Trees10 = (Math.Pow(spa3.Sd, 2)*Math.Pow(spa3.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa3.Trees15 = (Math.Pow(spa3.Sd, 2)*Math.Pow(spa3.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    spa3.TotalLow = 0;
                    spa3.TotalAvg = 0;
                    spa3.TotalHigh = 0;
                    ctx.StatsPerAcres.Add(spa3);
                    break;
                case 1: // was 4
                    Temp.DAL.StatsPerAcre spa4 = new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa4.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa4.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa4.StandsSelectedId = -1;
                        spa4.StandsId = -1;
                    }
                    spa4.Species = "Net Mbf (1000 bf)";
                    spa4.Seq = 1;
                    spa4.Sd = (double)def.StdDeviation;
                    spa4.Cl = (double)def.Confidence;
                    spa4.CvPercent = BdFtCv; // coefficient;
                    spa4.SePercent = BdFtSE; // standard_error_percent;
                    percent = BdFtSE / 100; // standard_error_percent / 100;
                    tmp = avgNetMbf * percent;
                    spa4.Low = avgNetMbf - tmp; // low;
                    spa4.Avg = avgNetMbf; // mean;
                    spa4.High = avgNetMbf + tmp; // high;
                    spa4.Trees5 = (Math.Pow(spa4.Sd, 2)*Math.Pow(spa4.CvPercent, 2))/Math.Pow(5, 2); // five_percent;
                    spa4.Trees10 = (Math.Pow(spa4.Sd, 2)*Math.Pow(spa4.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa4.Trees15 = (Math.Pow(spa4.Sd, 2)*Math.Pow(spa4.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    i32 = spa4.Trees5;
                    j32 = spa4.Trees10;
                    k32 = spa4.Trees15;
                    spa4.TotalLow = spa4.Low * type_acres;
                    spa4.TotalAvg = spa4.Avg * type_acres;
                    spa4.TotalHigh = spa4.High * type_acres;
                    ctx.StatsPerAcres.Add(spa4);
                    break;
                case 2: // was 5
                    Temp.DAL.StatsPerAcre spa5= new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa5.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa5.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa5.StandsSelectedId = -1;
                        spa5.StandsId = -1;
                    }
                    spa5.Species = "Net Ccf (100 cf)";
                    spa5.Seq = 2;
                    spa5.Sd = (double)def.StdDeviation;
                    spa5.Cl = (double)def.Confidence;
                    spa5.CvPercent = CuFtCv; // coefficient;
                    spa5.SePercent = CuFtSE; // standard_error_percent;
                    percent = CuFtSE / 100; // standard_error_percent / 100;
                    tmp = avgNetCcf * percent;
                    spa5.Low = avgNetCcf - tmp; // low;
                    spa5.Avg = avgNetCcf; // mean;
                    spa5.High = avgNetCcf + tmp; // high;
                    spa5.Trees5 = (Math.Pow(spa5.Sd, 2)*Math.Pow(spa5.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spa5.Trees10 = (Math.Pow(spa5.Sd, 2)*Math.Pow(spa5.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa5.Trees15 = (Math.Pow(spa5.Sd, 2)*Math.Pow(spa5.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    i34 = spa5.Trees5;
                    j34 = spa5.Trees10;
                    k34 = spa5.Trees15;
                    spa5.TotalLow = spa5.Low * type_acres;
                    spa5.TotalAvg = spa5.Avg * type_acres;
                    spa5.TotalHigh = spa5.High * type_acres;
                    ctx.StatsPerAcres.Add(spa5);
                    break;
                case 3: // was 6
                    Temp.DAL.StatsPerAcre spa6= new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa6.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa6.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa6.StandsId = -1;
                        spa6.StandsSelectedId = -1;
                    }
                    spa6.Species = "Tons per Acre";
                    spa6.Seq = 3;
                    spa6.Sd = (double)def.StdDeviation;
                    spa6.Cl = (double)def.Confidence;
                    //TempPerValue t6 = TempBLL.GetTempPerValuesRecord(currentSelectedStand.TractName, currentSelectedStand.StandName, "Tons per Acre");
                    //spa6.CvPercent = t6.Cv; //coefficient;
                    //spa6.SePercent = t6.Se; //standard_error_percent;
                    //percent = (float)t6.Se / 100;
                    //tmp = avgTons * percent;
                    //spa6.Low = avgTons - tmp; // low;
                    //spa6.Avg = avgTons; // mean;
                    //spa6.High = avgTons + tmp; // high;
                    //spa6.Trees5 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    //spa6.Trees10 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    //spa6.Trees15 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    //spa6.TotalLow = spa6.Low * type_acres;
                    //spa6.TotalAvg = spa6.Avg * type_acres;
                    //spa6.TotalHigh = spa6.High * type_acres;

                    spa6.CvPercent = tonsCv; //coefficient;
                    spa6.SePercent = tonsSE; //standard_error_percent;
                    percent = (float)tonsSE / 100;
                    tmp = avgTons * percent;
                    spa6.Low = avgTons - tmp; // low;
                    spa6.Avg = avgTons; // mean;
                    spa6.High = avgTons + tmp; // high;
                    spa6.Trees5 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spa6.Trees10 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa6.Trees15 = (Math.Pow(spa6.Sd, 2)*Math.Pow(spa6.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    spa6.TotalLow = spa6.Low * type_acres;
                    spa6.TotalAvg = spa6.Avg * type_acres;
                    spa6.TotalHigh = spa6.High * type_acres;
                    ctx.StatsPerAcres.Add(spa6);
                    break;
                case 6: // was 7
                    Temp.DAL.StatsPerAcre spa7 = new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa7.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa7.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa7.StandsSelectedId = -1;
                        spa7.StandsId = -1;
                    }
                    spa7.Species = "VBar per Acre";
                    spa7.Seq = 6;
                    spa7.Sd = (double)def.StdDeviation;
                    spa7.Cl = (double)def.Confidence;
                    //TempPerValue t7 = TempBLL.GetTempPerValuesRecord(currentSelectedStand.TractName, currentSelectedStand.StandName, "VBar per Acre");
                    spa7.CvPercent = vbarCv; // t7.Cv; //coefficient;
                    spa7.SePercent = vbarSE; // t7.Se; //standard_error_percent;
                    percent = vbarSE / 100; // standard_error_percent / 100;
                    tmp = avgVBar * percent;
                    spa7.Low = avgVBar - tmp; // low;
                    spa7.Avg = avgVBar; // mean;
                    spa7.High = avgVBar + tmp; // high;
                    spa7.Trees5 = (Math.Pow(spa7.Sd, 2)*Math.Pow(spa7.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spa7.Trees10 = (Math.Pow(spa7.Sd, 2)*Math.Pow(spa7.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa7.Trees15 = (Math.Pow(spa7.Sd, 2)*Math.Pow(spa7.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    spa7.TotalLow = 0;
                    spa7.TotalAvg = 0;
                    spa7.TotalHigh = 0;
                    ctx.StatsPerAcres.Add(spa7);
                    break;
                case 7: // was 1
                    Temp.DAL.StatsPerTree spt1 = new StatsPerTree();
                    if (pCurrentSelectedStand != null)
                    {
                        spt1.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spt1.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spt1.StandsSelectedId = -1;
                        spt1.StandsId = -1;
                    }
                    spt1.Species = "Sample Trees BF";
                    spt1.Seq = 1;
                    spt1.Sd = (double)def.StdDeviation;
                    spt1.Cl = (double)def.Confidence;
                    //TempPerValue t1 = TempBLL.GetTempPerValuesRecord(currentSelectedStand.TractName, currentSelectedStand.StandName, "Sample Trees BF");
                    //spt1.CvPercent = t1.Cv; //coefficient;
                    //spt1.SePercent = t1.Se; //standard_error_percent;
                    //percent = standard_error_percent / 100;
                    //tmp = sampleTreesBdFt * percent;
                    //spt1.Low = t1.Low; //sampleTreesBdFt - tmp; // low;
                    //spt1.Avg = t1.Avg; //sampleTreesBdFt; // mean;
                    //spt1.High = t1.High; //sampleTreesBdFt + tmp; // high;
                    //spt1.Trees5 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    //spt1.Trees10 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    //spt1.Trees15 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    //i45 = spt1.Trees5;
                    //j45 = spt1.Trees10;
                    //k45 = spt1.Trees15;

                    spt1.CvPercent = bdFtCvPercent; //coefficient;
                    spt1.SePercent = bdFtSePercent; //standard_error_percent;
                    percent = bdFtSePercent / 100;
                    tmp = (sampleTreesBdFt/sample_trees) * percent;
                    spt1.Low = (sampleTreesBdFt / sample_trees) - tmp; // low;
                    spt1.Avg = (sampleTreesBdFt / sample_trees); // mean;
                    spt1.High = (sampleTreesBdFt / sample_trees) + tmp; // high;
                    spt1.Trees5 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spt1.Trees10 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spt1.Trees15 = (Math.Pow(spt1.Sd, 2)*Math.Pow(spt1.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    i45 = spt1.Trees5;
                    j45 = spt1.Trees10;
                    k45 = spt1.Trees15;
                    ctx.StatsPerTrees.Add(spt1);
                    break;
                case 8: // was 2
                    Temp.DAL.StatsPerTree spt2 = new StatsPerTree();
                    if (pCurrentSelectedStand != null)
                    {
                        spt2.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spt2.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spt2.StandsSelectedId = -1;
                        spt2.StandsId = -1;
                    }
                    spt2.Species = "Sample Trees CF";
                    spt2.Seq = 2;
                    spt2.Sd = (double)def.StdDeviation;
                    spt2.Cl = (double)def.Confidence;
                    //TempPerValue t2 = TempBLL.GetTempPerValuesRecord(currentSelectedStand.TractName, currentSelectedStand.StandName, "Sample Trees CF");
                    //spt2.CvPercent = t2.Cv; //coefficient;
                    //spt2.SePercent = t2.Se; //standard_error_percent;
                    //percent = standard_error_percent / 100;
                    //tmp = sampleTreesCuFt * percent;
                    //spt2.Low = t2.Low; //sampleTreesCuFt - tmp; // low;
                    //spt2.Avg = t2.Avg; //sampleTreesCuFt; // mean;
                    //spt2.High = t2.High; //sampleTreesCuFt + tmp; // high;
                    //spt2.Trees5 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    //spt2.Trees10 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    //spt2.Trees15 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    //i47 = spt2.Trees5;
                    //j47 = spt2.Trees10;
                    //k47 = spt2.Trees15;

                    spt2.CvPercent = cuFtCvPercent; //coefficient;
                    spt2.SePercent = cuFtSePercent; //standard_error_percent;
                    percent = cuFtSePercent / 100;
                    tmp = (sampleTreesCuFt / sample_trees) * percent;
                    spt2.Low = (sampleTreesCuFt / sample_trees) - tmp; // low;
                    spt2.Avg = (sampleTreesCuFt / sample_trees); // mean;
                    spt2.High = (sampleTreesCuFt / sample_trees) + tmp; // high;
                    spt2.Trees5 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spt2.Trees10 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spt2.Trees15 = (Math.Pow(spt2.Sd, 2)*Math.Pow(spt2.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    i47 = spt2.Trees5;
                    j47 = spt2.Trees10;
                    k47 = spt2.Trees15;
                    ctx.StatsPerTrees.Add(spt2);
                    break;
                case 9: // was 8
                    Temp.DAL.StatsPerPlot spp8 = new StatsPerPlot();
                    if (pCurrentSelectedStand != null)
                    {
                        spp8.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spp8.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spp8.StandsSelectedId = -1;
                        spp8.StandsId = -1;
                    }
                    spp8.Species = "Required Measured Trees per Plot - BdFt per Acre";
                    spp8.Seq = 1;
                    spp8.Trees5 = i45 / i32;
                    spp8.Trees10 = j45 / j32;
                    spp8.Trees15 = k45 / k32;
                    ctx.StatsPerPlots.Add(spp8);
                    break;
                case 10: // was 9
                    Temp.DAL.StatsPerPlot spp9 = new StatsPerPlot();
                    if (pCurrentSelectedStand != null)
                    {
                        spp9.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spp9.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spp9.StandsSelectedId = -1;
                        spp9.StandsId = -1;
                    }
                    spp9.Species = "Required Measured Trees per Plot - CuFt per Acre";
                    spp9.Seq = 2;
                    spp9.Trees5 = i47 / i34;
                    spp9.Trees10 = j47 / j34;
                    spp9.Trees15 = k47 / k34;
                    ctx.StatsPerPlots.Add(spp9);
                    break;
                case 5: // was 10
                    Temp.DAL.StatsPerAcre spa10 = new StatsPerAcre();
                    if (pCurrentSelectedStand != null)
                    {
                        spa10.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                        spa10.StandsId = pCurrentSelectedStand.StandsId;
                    }
                    else
                    {
                        spa10.StandsSelectedId = -1;
                        spa10.StandsId = -1;
                    }
                    spa10.Species = "Trees per Acre";
                    spa10.Seq = 5;
                    spa10.Sd = (double)def.StdDeviation;
                    spa10.Cl = (double)def.Confidence;
                    spa10.CvPercent = tpaCv; // coefficient;
                    spa10.SePercent = tpaSE; // standard_error_percent;
                    percent = tpaSE / 100; // standard_error_percent / 100;
                    tmp = avgTpa * percent;
                    spa10.Low = avgTpa - tmp; // low;
                    spa10.Avg = avgTpa; // mean;
                    spa10.High = avgTpa + tmp; // high;
                    spa10.Trees5 = (Math.Pow(spa10.Sd, 2)*Math.Pow(spa10.CvPercent, 2))/Math.Pow(5, 2); //five_percent;
                    spa10.Trees10 = (Math.Pow(spa10.Sd, 2)*Math.Pow(spa10.CvPercent, 2))/Math.Pow(10, 2); //ten_percent;
                    spa10.Trees15 = (Math.Pow(spa10.Sd, 2)*Math.Pow(spa10.CvPercent, 2))/Math.Pow(15, 2); //fifteen_percent;
                    spa10.TotalLow = 0;
                    spa10.TotalAvg = 0;
                    spa10.TotalHigh = 0;
                    ctx.StatsPerAcres.Add(spa10);
                    break;
            }

            //prt_position(row, 9, "%-3.1f", coefficient);
            //prt_position(row, 16, "%-3.1f", standard_error_percent);
            //row++;
            //switch (rtn)
            //{
            //    case 2: // Trees/Acre
            //    case 3: // Basal Area/Acre
            //        prt_position(row++, 1, "T/A LOW        %-3.1f", low);
            //        prt_position(row++, 1, "T/A AVE        %-3.1f", mean);
            //        prt_position(row++, 1, "T/A HIGH       %-3.1f", high);
            //        break;
            //    case 1: // Sample Trees - BF
            //    case 4: // Net BF/Acre
            //    case 5: // Net CF/Acre
            //        prt_position(row++, 1, "T/A LOW        %-5.0f", low);
            //        prt_position(row++, 1, "T/A AVE        %-5.0f", mean);
            //        prt_position(row++, 1, "T/A HIGH       %-5.0f", high);
            //        break;
            //}
            //row++;
            //prt_position(row++, 1, "#Plot REQUIRED");
            //switch (rtn)
            //{
            //    case 2:
            //    case 3:
            //        prt_position(row++, 1, "5%%             %-2.0f", five_percent);
            //        prt_position(row++, 1, "10%%            %-2.0f", ten_percent);
            //        prt_position(row++, 1, "15%%            %-2.0f", fifteen_percent);
            //        break;
            //    case 1:
            //    case 4:
            //    case 5:
            //        prt_position(row++, 1, "5%%             %-2.0f", five_percent);
            //        prt_position(row++, 1, "10%%            %-2.0f", ten_percent);
            //        prt_position(row++, 1, "15%%            %-2.0f", fifteen_percent);
            //        break;
            //}
            //prt_position(row++, 1, "--------------------");
            ///*		  switch (rtn) {
            //                     case 2:
            //                     case 3:
            //                                prt_position(row, 30, "%-3.1f", low);
            //                                prt_position(row, 38, "%-3.1f", mean);
            //                                prt_position(row, 45, "%-3.1f", high);
            //                                prt_position(row, 56, "%-2.0f", five_percent);
            //                                prt_position(row, 64, "%-2.0f", ten_percent);
            //                                prt_position(row, 73, "%-2.0f", fifteen_percent);
            //                                break;
            //                     case 1:
            //                     case 4:
            //                     case 5:
            //                                prt_position(row, 30, "%-5.0f", low);
            //                                prt_position(row, 38, "%-5.0f", mean);
            //                                prt_position(row, 45, "%-5.0f", high);
            //                                prt_position(row, 56, "%-2.0f", five_percent);
            //                                prt_position(row, 64, "%-2.0f", ten_percent);
            //                                prt_position(row, 73, "%-2.0f", fifteen_percent);
            //                                break;
            //          }
            //*/
        }

        private void AdjustDeviationTotals()
        {
            int x;
            float dvar_scribner, dvar_cubic, dvar_basal_area, dvar_trees_per_acre, dvar_tons, dvar_vbar;
            float w_scribner, w_cubic, w_basal, w_trees_per_acre, w_tons, w_vbar;

            for (x = 0; x <= type_top + 1; x++)
            {
                temp_basal_area[x] = species_na_basal_area[x];
                temp_trees_per_acre[x] = species_na_trees_per_acre[x];
                temp_scribner[x] = species_na_net_scribner[x];
                temp_cubic[x] = species_na_net_cubic[x];

                temp_tons[x] = species_na_tons[x];
                temp_vbar[x] = species_na_vbar[x];
            }

            w_scribner = 0;
            w_cubic = 0;
            w_basal = 0;
            w_trees_per_acre = 0;
            w_tons = 0;
            w_vbar = 0;
            cnt_sub = 0;
            type_sub = 0;
            if (String.IsNullOrEmpty(cnt_species[0]))
                return;
            /*
                      if (cnt_top < 1)
                         return;
            */
            while (true)
            {
                if (cnt_species[cnt_sub] == null)
                    break;
                // new added 4/12/17
                if (string.IsNullOrEmpty(speciesGroup[type_sub].Species))
                    break;
                if (cnt_species[cnt_sub] != speciesGroup[type_sub].Species) // Here
                {
                    type_sub++;
                    continue;
                }
                if (type_sub > type_top)
                    break;
                if (cnt_sub > cnt_top)
                    break;
                dvar_scribner = 0;
                dvar_cubic = 0;
                dvar_basal_area = 0;
                dvar_trees_per_acre = 0;
                dvar_tons = 0;
                dvar_vbar = 0;
                if (species_cruised_baf[type_sub] > 0)
                {
                    dvar_basal_area =
                              temp_basal_area[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                    dvar_trees_per_acre =
                              temp_trees_per_acre[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                    dvar_scribner =
                              temp_scribner[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                    dvar_cubic =
                              temp_cubic[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                    dvar_tons =
                              temp_tons[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                    dvar_vbar =
                              temp_vbar[type_sub] * cnt_baf[cnt_sub] / species_cruised_baf[type_sub];
                }

                species_na_basal_area[type_sub] += dvar_basal_area;
                w_basal += dvar_basal_area;
                plt_sqd_basal_area += (dvar_basal_area * dvar_basal_area);

                species_na_trees_per_acre[type_sub] += dvar_trees_per_acre;
                w_trees_per_acre += dvar_trees_per_acre;
                plt_sqd_trees_per_acre += (dvar_trees_per_acre * dvar_trees_per_acre);

                species_na_net_scribner[type_sub] += dvar_scribner;
                w_scribner += dvar_scribner;
                plt_sqd_net_scribner += (dvar_scribner * dvar_scribner);

                species_na_net_cubic[type_sub] += dvar_cubic;
                w_cubic += dvar_cubic;
                plt_sqd_net_cubic += (dvar_cubic * dvar_cubic);

                species_na_tons[type_sub] += dvar_tons;
                w_tons += dvar_tons;
                plt_sqd_tons += (dvar_tons * dvar_tons);

                species_na_vbar[type_sub] += dvar_vbar;
                w_vbar += dvar_vbar;
                plt_sqd_vbar += (dvar_vbar * dvar_vbar);

                cnt_sub++;
                type_sub = 0;
            }
            species_na_basal_area[species_total_sub] += w_basal;
            species_na_trees_per_acre[species_total_sub] += w_trees_per_acre;
            species_na_net_scribner[species_total_sub] += w_scribner;
            species_na_net_cubic[species_total_sub] += w_cubic;
            species_na_tons[species_total_sub] += w_tons;
            species_na_vbar[species_total_sub] += w_vbar;
        }

        private void WriteDeviationsHead()
        {
            bool sw_head;

            sw_head = true;
            //prt_position(++row, 1, "--------------------");
            if (sw_head)
            {
                //prt_position(++row, 1,
                //           "SD:%2d    COEFF.", head_deviations);
                //prt_position(++row, 9,
                //           "VAR.%%  S.E.%%");
                ///*					 prt_position(++row, 1,
                //                                "SD: %-d        COEFF.            %-s    # OF Plot REQ. - %-s. POP.",
                //                                head_deviations, part_head, part_head_finite);
                //                     prt_position(++row, 12,
                //                                " VAR.%%   S.E.%%    LOW     AVE    HIGH       5%%      10%%      15%%");
                //*/
            }
            sw_head = false;
        }

        private void AssignDeviationValues()
        {
            //prt_position(++row, 1, "TOTAL");

            switch (rtn)
            {
                case 1:
                    
                    break;
                case 4:
                    
                    break;
                case 5:
                    
                    break;
            }

            switch (rtn)
            {
                case 3:
                    actual_value = species_na_basal_area[species_total_sub];
                    squared_value = plt_sqd_basal_area;
                    break;
                case 4:
                    actual_value = species_na_net_scribner[species_total_sub];
                    squared_value = plt_sqd_net_scribner;
                    break;
                case 5:
                    actual_value = species_na_net_cubic[species_total_sub];
                    squared_value = plt_sqd_net_cubic;
                    break;
                case 6:
                    actual_value = species_na_tons[species_total_sub];;
                    squared_value = plt_sqd_tons;
                    break;
                case 7:
                    actual_value = species_na_vbar[species_total_sub];;
                    squared_value = plt_sqd_vbar;
                    break;
                case 1:
                    actual_value = sample_board_feet[sample_total_sub];
                    squared_value = sample_sqd_board_feet[sample_total_sub];
                    break;
                case 2:
                    actual_value = 0;
                    squared_value = 0;
                    break;
                case 8:
                    actual_value = 0;
                    squared_value = 0;
                    break;
                case 9:
                    actual_value = 0;
                    squared_value = 0;
                    break;
                case 10:
                    actual_value = species_na_trees_per_acre[species_total_sub];
                    squared_value = plt_sqd_trees_per_acre;
                    break;
            }
        }

        private void WriteDeviations(SepStandsSelected pCurrentSelectedStand)
        {
            int x;

            if (total_plots < 2)
                return;
            plots_per_acre = 1;
            if (type_average_d4h > 1 && type_average_baf > 1)
            {
                if (type_average_d4h > 1)
                {
                    type_trees_per_acre = type_average_baf / ((float)0.005454154 * type_average_d4h * type_average_d4h);
                    if (type_trees_per_acre > 1)
                    {
                        plt_radius = (float)Math.Sqrt((float)13865.0 / type_trees_per_acre);
                        plt_area = plt_radius * plt_radius * (float)3.14159 / (float)43560.0;
                        if (plt_area > 0)
                            plots_per_acre = (float)1.0 / plt_area;
                    }
                }
            }

            rtn = 1;

            do
            {
                switch (rtn)
                {
                    case 1:
                        //part_head = "SAMPLE Tree-BF  ";
                        //lines_needed = row;
                        break;
                    case 2:
                        //part_head = " Tree/ACRE ";
                        //lines_needed = row - lines_needed;
                        break;
                    case 3:
                        //part_head = " BASAL AREA/ACRE ";
                        /*										  lines_needed = row - lines_needed;	*/
                        break;
                    case 4:
                        //part_head = "   NET BF/ACRE   ";
                        break;
                    case 5:
                        //part_head = "NET CUBIC FT/ACRE";
                        break;
                    case 6:
                        //part_head = "   NET BF/ACRE   ";
                        break;
                    case 7:
                        //part_head = "NET CUBIC FT/ACRE";
                        break;
                    case 8:
                        //part_head = "   NET BF/ACRE   ";
                        break;
                    case 9:
                        //part_head = "NET CUBIC FT/ACRE";
                        break;
                    case 10:
                        //part_head = "NET CUBIC FT/ACRE";
                        break;
                }
                //x = row;
                //if (rtn > 1)
                //    x += lines_needed;
                //if (x > 57)
                //    print_heading();

                WriteDeviationsHead();
                AssignDeviationValues();
                ComputeDeviations(pCurrentSelectedStand);

                rtn++;
                //prt_position(++row, 1,
                //           "------------------------------------------------------------------------------");
            } while (rtn <= 10);
            ctx.SaveChanges();
        }

        private void WriteTempTables(SepStandsSelected pCurrentSelecetStand)
        {
            WriteConfStd(def, pCurrentSelecetStand);
            head_deviations = (int)def.StdDeviation;
            if (def.FiniteFlag == "Y")
            {
                head_finite = "F";
                part_head_finite = "FIN";
            }
            else
            {
                head_finite = "I";
                part_head_finite = "INF";
            }

            if (head_deviations < 1)
                head_deviations = 1;
            if (head_deviations > 3)
                head_deviations = 3;

            //page = 0;
            //row = 66;
            species_total_sub = type_top + 1;
            sample_total_sub = sample_top + 1;
            plt_total_sub = plt_top + 1;
            ZeroTypeSpecies(species_total_sub);
            ZeroTreeSample(sample_total_sub);
            ZeroPlotData(plt_total_sub);
            TotalThePlot();
            TotalTheSpecies();
            TotalTheSample();

            AccumulatePlotAnswers();
            //print_heading();
            PrintTypeOfCruise();
            WriteSampleData(pCurrentSelecetStand);
            WriteStandSummary(pCurrentSelecetStand);
            PrintCruiseSummaryTotal();
            AdjustDeviationTotals();
            WriteDeviations(pCurrentSelecetStand);
        }

        private void WriteConfStd(DefStat def, SepStandsSelected pCurrentSelectedStand)
        {
            Temp.DAL.StatsConfStd scs = new StatsConfStd();
            scs.ConfidenceLevel = (double)def.Confidence;
            scs.StandardDeviation = (double)def.StdDeviation;
            if (pCurrentSelectedStand != null)
            {
                scs.StandsSelectedId = pCurrentSelectedStand.StandsSelectedId;
                scs.StandsId = pCurrentSelectedStand.StandsId;
            }
            else
            {
                scs.StandsSelectedId = -1;
                scs.StandsId = -1;
            }
            ctx.StatsConfStds.Add(scs);
            ctx.SaveChanges();
        }

        private void NewPlot(WrkTree treeItem)
        {
            add_new_plot = false;

            if (!hundred_percent_plot)
                total_plots++;

            if (treeItem.TreeType == "R")
            {
                if (treeItem.TreeCount == 0)
                    total_reforest_blank_plots++;
                else
                    total_reforest_plots++;
                return;
            }

            if (treeItem.PlotNumber.StartsWith("-"))
            {
                total_100_percent++;
                return;
            }

            if (treeItem.TreeType == "C")
            {
                if (treeItem.TreeCount > 0)
                {
                    total_count_plots++;
                }
                else
                {
                    total_blank_plots++;
                }
            }

            if (treeItem.TreeType == "D")
            {
                if (treeItem.PlotFactorInput.StartsWith("B") || Utils.IsValidInt(treeItem.PlotFactorInput))
                {
                    total_baf_plots++;
                    return;
                }
                if (treeItem.PlotFactorInput.StartsWith("F"))
                {
                    total_fixed_plots++;
                    return;
                }
                if (treeItem.PlotFactorInput.StartsWith("S"))
                {
                    total_strip_plots++;
                    return;
                }
                
                //total_baf_plots++;
            }
            //if (treeItem.TreeType != "D")
            //{
            //    if (treeItem.TreeType == "R")
            //        total_blank_plots++;
            //    else
            //        total_count_plots++;
            //    return;
            //}

            //if (!treeItem.PlotNumber.StartsWith("-"))
            //    total_cruised_plots++;
            //else
            //    total_100_percent++;
            //add_new_plot = false;
        }

        /*------------------------------------------------------------------*/
        private void TotalTheSpecies()
        {
            type_sub = 0;

            while (type_sub <= type_top)
            {
                species_cruised_trees[species_total_sub] += species_cruised_trees[type_sub];
                species_trees_per_acre[species_total_sub] += species_trees_per_acre[type_sub];
                species_na_trees_per_acre[species_total_sub] += species_na_trees_per_acre[type_sub];
                species_basal_area[species_total_sub] += species_basal_area[type_sub];
                species_na_basal_area[species_total_sub] += species_na_basal_area[type_sub];
                species_gross_scribner[species_total_sub] += species_gross_scribner[type_sub];
                species_gross_cubic[species_total_sub] += species_gross_cubic[type_sub];
                species_net_scribner[species_total_sub] += species_net_scribner[type_sub];
                species_na_net_scribner[species_total_sub] += species_na_net_scribner[type_sub];
                species_net_cubic[species_total_sub] += species_net_cubic[type_sub];
                species_na_net_cubic[species_total_sub] += species_na_net_cubic[type_sub];
                species_cruised_baf[species_total_sub] += species_cruised_baf[type_sub];
                species_count_baf[species_total_sub] += species_count_baf[type_sub];
                species_bole_length[species_total_sub] += species_bole_length[type_sub];
                species_na_tons[species_total_sub] += species_na_tons[type_sub];
                species_na_vbar[species_total_sub] += species_na_vbar[type_sub];
                type_sub++;
            }
        }

        private void TotalThePlot()
        {
            plt_sub = 0;

            while (plt_sub <= plt_top)
            {
                plt_na_basal_area[plt_total_sub] += plt_na_basal_area[plt_sub];
                plt_na_trees_per_acre[plt_total_sub] += plt_na_trees_per_acre[plt_sub];
                plt_sqd_basal_area += (plt_na_basal_area[plt_sub] * plt_na_basal_area[plt_sub]);
                plt_sqd_trees_per_acre += (plt_na_trees_per_acre[plt_sub] * plt_na_trees_per_acre[plt_sub]);
                plt_sqd_net_scribner += (plt_na_net_scribner[plt_sub] * plt_na_net_scribner[plt_sub]);
                plt_sqd_net_cubic += (plt_na_net_cubic[plt_sub] * plt_na_net_cubic[plt_sub]);
                plt_sub++;
            }
        }

        private void TotalTheSample()
        {
            sample_sub = 0;

            while (sample_sub <= sample_top)
            {
                sample_board_feet[sample_total_sub] += sample_board_feet[sample_sub];
                sample_sqd_board_feet[sample_total_sub] += sample_sqd_board_feet[sample_sub];
                sample_sub++;
            }
        }

        private void InitValues()
        {
            first_species = first_plot = first_sample = first_count = true;
            sample_top = 0;
            last_species = String.Empty;
            type_top = 0;
            cnt_top = 0;
            plt_top = 0;

            sampleTreesBdFt = 0;
            sampleTreesCuFt = 0;

            //
            total_baf_plots = 0;
            total_plots = 0;
            total_cruised_plots = 0;
            total_count_plots = 0;
            total_blank_plots = 0;
            total_reforest_blank_plots = 0;
            total_reforest_plots = 0;
            total_fixed_plots = 0;
            total_strip_plots = 0;

            total_trees = 0;
            total_cruised_trees = 0;
            total_reforest_trees = 0;
            total_count_trees = 0;
            total_prism_factor = 0;
            total_100_percent = 0;
            work_count_trees = 0;
            work_cruised_trees = 0;
            work_reforest_trees = 0;
            type_break = false;
            records_processed = 0;
            plt_sqd_basal_area = 0;
            plt_sqd_trees_per_acre = 0;
            plt_sqd_net_scribner = 0;
            plt_sqd_net_cubic = 0;
            head_finite = "I";
        }

        private void CalculateSpeciesAjustment()
        {
            int i;
            float temp_adj, temp;

            for (i = 0; i <= type_top; i++)
            {
                temp = species_count_baf[i] + species_cruised_baf[i];
                if (temp > 0)
                {
                    temp_adj = temp / species_cruised_baf[i];
                    species_adj_factor[i] =
                                      temp_adj / (total_count_plots + total_blank_plots + total_cruised_plots);
                }
                else
                    species_adj_factor[i] = 1;
            }
        }

        private float Roundup(float value, int digits)
        {
            long j;
            float b = 1.0f;

            while (digits-- > 0)
                b *= 10;

            j = (long)(value * b + (value > 0.0 ? 0.5 : -0.5));

            return ((float)(j / b));
        }


        /*------------------------------------------------------------------*/
        //private int DisplayFile(char* fname)
        //{
            //int rt;

            //rt = process_prg("cmd mced temp.prn");
            //if (rt == -1)
            //{
            //    clear_screen();
            //    switch (errno)
            //    {
            //        case ENOENT:
            //            print_line(4, 1, "No File/Dir");
            //            break;
            //        case ENOMEM:
            //            print_line(4, 1, "Not Enough Memory");
            //            break;
            //        case E2BIG:
            //            print_line(4, 1, "Arg List Too Big");
            //            break;
            //        case ENOEXEC:
            //            print_line(4, 1, "Exec format error");
            //            break;
            //    }
            //    read_keyboard();
            //}
        //    return (1);
        //}

        private void RefComputeDeviations()
        {
            float low, high, possible_plots, five_percent, ten_percent;
            float fifteen_percent, top_of_n, bottom_of_n;
            float t_value;
            int x, given_n;

            given_n = (int)total_plots;
            if (given_n < 2)
                given_n = 2;  /* 04-27-90 */
            mean = actual_value / given_n;
            actual_value_squared = actual_value * actual_value;
            rl_value = squared_value - actual_value_squared / given_n;

            if (rl_value > 0)
                standard_deviation = (float)Math.Sqrt(rl_value / (given_n - 1));
            else
                standard_deviation = 0;

            if (mean > 0)
                coefficient = standard_deviation / mean;
            else
                coefficient = 0;
            standard_error = standard_deviation / (float)Math.Sqrt(given_n);

            if (mean > 0)
                standard_error_percent = standard_error / mean;
            else
                standard_error_percent = 0;

            t_value = head_deviations;

            if (head_deviations == 1 || given_n > 30)
            {
            }
            else
            {
                x = given_n - 1;
                if (x < 1)
                    x = 1;
                if (head_deviations == 2)
                    t_value = (float)t95[x];
                if (head_deviations == 3)
                    t_value = (float)t99[x];
            }
            low = mean - t_value * standard_error;
            if (low < 0.0)
                low = 0;
            high = mean + t_value * standard_error;

            standard_error_percent = standard_error_percent * 100;
            coefficient = 100.0f * coefficient;

            t_value = head_deviations;

            if (head_deviations == 1 || given_n > 30)
            {
            }
            else
            {
                x = given_n - 1;
                if (x < 1)
                    x = 1;
                if (head_deviations == 2)
                    t_value = (float)t95[x];
                if (head_deviations == 3)
                    t_value = (float)t99[x];
            }

            possible_plots = type_acres * 5.0f;
            /*		  possible_plots = type_acres * plots_per_acre;		*/
            x = given_n;
            top_of_n = possible_plots * (t_value * t_value) * (coefficient * coefficient);
            bottom_of_n = possible_plots * 25.0f + (t_value * t_value) * (coefficient * coefficient);
            if (bottom_of_n > 0)
                five_percent = top_of_n / bottom_of_n;
            else
                five_percent = 0;
            top_of_n = possible_plots * (t_value * t_value) * (coefficient * coefficient);
            bottom_of_n = possible_plots * 100.0f + (t_value * t_value) * (coefficient * coefficient);
            if (bottom_of_n > 0)
                ten_percent = top_of_n / bottom_of_n;
            else
                ten_percent = 0;
            top_of_n = possible_plots * (t_value * t_value) * (coefficient * coefficient);
            bottom_of_n = possible_plots * 225 + (t_value * t_value) * (coefficient * coefficient);
            if (bottom_of_n > 0)
                fifteen_percent = top_of_n / bottom_of_n;
            else
                fifteen_percent = 0;
            if (head_finite == "F")
            {
                five_percent = (coefficient * coefficient) * (t_value * t_value) / 25.0f;
                ten_percent = (coefficient * coefficient) * (t_value * t_value) / 100.0f;
                fifteen_percent = (coefficient * coefficient) * (t_value * t_value) / 225.0f;
            }

            //if (rtn == 1)
            //{
            //    prt_position(row, 9, "%-3.1f", coefficient);
            //    prt_position(row, 16, "%-3.1f", standard_error_percent);
            //}
            //row++;
            //if (rtn == 1)
            //{
            //    prt_position(row, 1, "T/A LOW        %-3.0f", low);
            //    prt_position(row, 1, "T/A AVE        %-3.0f", mean);
            //    prt_position(row, 1, "T/A HIGH       %-3.0f", high);
            //}
            //if (rtn == 1)
            //{
            //    prt_position(row, 1, "5%%             %-2.0f", five_percent);
            //    prt_position(row, 1, "10%%            %-2.0f", ten_percent);
            //    prt_position(row, 1, "15%%            %-2.0f", fifteen_percent);
            //}
        }

        private void RefPrintDeviationsHead()
        {
            bool sw_head;

            sw_head = true;
            //if (sw_head)
            //{
            //    prt_position(++row, 1,
            //               "SD:%2d    COEFF.", head_deviations, part_head);
            //    prt_position(++row, 1,
            //               "VAR.%%   S.E.%%");
            //}
            sw_head = false;
        }

        private void RefAssignDeviationValues()
        {
            //prt_position(++row, 1, "TOTAL ");

            actual_value = species_na_trees_per_acre[species_total_sub];
            squared_value = plt_sqd_trees_per_acre;
        }

        private void RefPrintDeviations()
        {
            int x;

            if (total_plots < 2)
                return;
            plots_per_acre = 1;
            if (type_average_d4h > 1 && type_average_baf > 1)
            {
                if (type_average_d4h > 1)
                {
                    type_trees_per_acre =
                              type_average_baf / (0.005454154f * type_average_d4h * type_average_d4h);
                    if (type_trees_per_acre > 1)
                    {
                        plt_radius = (float)Math.Sqrt(13865.0f / type_trees_per_acre);
                        plt_area = plt_radius * plt_radius * 3.14159f / 43560.0f;
                        if (plt_area > 0)
                            plots_per_acre = 1.0f / plt_area;
                    }
                }
            }

            rtn = 1;

            //part_head = " Tree/ACRE ";
            //lines_needed = row - lines_needed;
            //x = row;
            //if (x > 57)
            //    ref_print_heading();

            //ref_print_deviations_head();
            //ref_assign_deviation_values();
            //ref_compute_deviations();

            //prt_position(++row, 1,
            //      "------------------------------------------------------------------------------");
        }

        private void RefPrintRemainder()
        {
            float above_ground_tpa, root_placement,
                     plantable_ground, acceptable_tpa, sqrt_temp;

            //prt_position(++row, 1, "T/A PLANTED:");
            //prt_position(row, 16, "%-4.0f",
            //    total_trees_planted_tpa / total_plots);

            //above_ground_tpa = above_ground_trees / total_plots;
            //prt_position(++row, 1, "ACC. ABOVE T/A:");
            //prt_position(row, 16, "%-4.0f", above_ground_tpa);

            //root_placement = acceptable_root_checks / root_checks;
            //prt_position(++row, 1, "%% ACC. ROOT PL:");
            //prt_position(row, 16, "%-3.0f", root_placement * 100);

            //plantable_ground = total_plant_spots / (total_plots * 4);
            //prt_position(++row, 1, "%% PLANTABLE GR:");
            //prt_position(row, 16, "%-3.0f", plantable_ground * 100);

            //acceptable_tpa = (above_ground_tpa * root_placement) / plantable_ground;
            //prt_position(++row, 1, "TOT ACC T/A:");
            //prt_position(row, 16, "%-4.0f", acceptable_tpa);

            //sqrt_temp = sqrt((root_placement * (1.0 - root_placement)) / (total_plots - 1));

            //prt_position(++row, 1, "SE ACC. ROOTS:");
            //prt_position(row, 16, "%-3.1f", sqrt_temp * 100);

            //rtn = 2;
            //actual_value = total_plant_spots;
            //squared_value = plt_sqd_plantable_quads;
            //ref_compute_deviations();
            //prt_position(++row, 1, "SE PLNT. QUADS:");
            //prt_position(row, 16, "%-3.1f", standard_error_percent);

            //rtn = 3;
            //actual_value = above_ground_trees;
            //squared_value = plt_sqd_acceptable_trees;
            //ref_compute_deviations();
            //prt_position(++row, 1, "SE ACC. Tree:");
            //prt_position(row, 16, "%-3.1f", standard_error_percent);
            //prt_position(++row, 1, "      ");
        }
    }

    public class SpeciesGroup
    {
        public string Species { get; set; }
        public string Status { get; set; }
    }

    public class SampleDataInfo
    {
        public float Plots { get; set; }
        public float Trees { get; set; }
        public float DbhTrees { get; set; }
        public float CountTrees { get; set; }
        public float HundredPercentTrees { get; set; }
        public float TreesPerPlot { get; set; }
        public float TreesPerAcre { get; set; }
        public float EstTotalTrees { get; set; }
        public float PercentSampleTrees { get; set; }
    }

    public class CountData
    {
        public string species { get; set; }
        public float baf { get; set; }
        public string plot { get; set; }
    }

    public class PlotData
    {
        public long StandsId { get; set; }
        public string PlotNumber { get; set; }
        public float PlotAcres { get; set; }
        public float BAPerAcre { get; set; }
        public float BAPerPlot { get; set; }
        public float BAPerPlotD { get; set; }
        public float BAPerPlotDSqrd { get; set; }
        public float TreesPerAcre { get; set; }
        public float TreesPerPlot { get; set; }
        public float TreesPerPlotD { get; set; }
        public float TreesPerPlotDSqrd { get; set; }
        public float CuFtPerAcre { get; set; }
        public float CuFtPerPlot { get; set; }
        public float CuFtPerPlotD { get; set; }
        public float CuFtPerPlotDSqrd { get; set; }
        public float BdFtPerAcre { get; set; }
        public float BdFtPerPlot { get; set; }
        public float BdFtPerPlotD { get; set; }
        public float BdFtPerPlotDSqrd { get; set; }
        public float TonsPerAcre { get; set; }
        public float TonsPerPlot { get; set; }
        public float TonsPerPlotD { get; set; }
        public float TonsPerPlotDSqrd { get; set; }
        public float NetVBar { get; set; }
        public float VBar { get; set; }
        public float VBarD { get; set; }
        public float VBarDSqrd { get; set; }
        public float TotalCcf { get; set; }
        public float TotalMbf { get; set; }
    }

    public class SpeciesData
    {
        public long StandsId { get; set; }
        public string Species  { get; set; }
        public string Status { get; set; }
        public float TotalMbf  { get; set; }
        public float MbfPerSpecies { get; set; }
        public float MbfPerSpeciesD { get; set; }
        public float MbfPerSpeciesDSqrd { get; set; }
    }

    public class TempData
    {
        public float basal_area { get; set; }
        public float trees_per_acre  { get; set; }
        public float scribner  { get; set; }
        public float cubic  { get; set; }
    }

    public class SampleData
    {
        public string species { get; set; }
        public float board_feet { get; set; }
        public float sqd_board_feet { get; set; }
    }
}
