﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class StatsSampleDataSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        public StatsSampleDataSubReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            float cruisePlots = Convert.ToSingle(this.GetCurrentColumnValue("CruisePlots").ToString());
            float bafCountPlots = Convert.ToSingle(this.GetCurrentColumnValue("BAFCount").ToString());
            float blankPlots = Convert.ToSingle(this.GetCurrentColumnValue("BlankPlots").ToString());
            float totalPlots = cruisePlots + bafCountPlots + blankPlots;
            xrLabel26.Text = string.Format("{0:#}", totalPlots);
        }

        //private void StatsSampleDataSubReport_DataSourceDemanded(object sender, EventArgs e)
        //{
        //    objectDataSource1.DataSource =
        //        TempBLL.GetStatsSampleData(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        //}
    }
}
