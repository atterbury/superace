﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class TransportationSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        public TransportationSubReport()
        {
            InitializeComponent();
        }

        private void TransportationSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptTransportation(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }

    }
}
