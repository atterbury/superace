﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class PlotTreeListVolumeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        // private string currentTotalHt = string.Empty;
        // private string treeType = string.Empty;
        // private int treeNumber = 0;
        // private string plotFactor;

            // retain some flags to be accessible by multiple event handlers
        bool bIsCountTree = false;
        bool bWasCalculatedHt = false;
        bool bWasStripCruise = false;
        bool bFirstTree = false;

        public int standPlots = 0;
        public float standAcres = 0;
        private int nMeasuredPlots = 0;
        private float plotlpa = 0;
        private float treeNetCuFt = 0, treeNetBdFt = 0;
        private float plotba = 0, plottpa = 0, plotCcf = 0, plotMbf = 0;
        private float standba = 0, standtpa = 0, standlpa = 0, standCuFt = 0, standBdFt = 0, standCcf = 0, standMbf = 0;
        // private float cuft = 0;
        // private float bdft = 0;
        // private float ba = 0;
        // private float tpa = 0;
        private float lpa = 0;


        public PlotTreeListVolumeSubReport()
        {
            InitializeComponent();
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Plot
            xrPlotSumBasalPA.Text = string.Format("{0:0.0}", plotba);
            xrPlotSumTreesPA.Text = string.Format("{0:0.0}", plottpa);
            xrPlotSumTotalCcf.Text = string.Format("{0:#,##0.0}", plotCcf);
            xrPlotSumTotalMbf.Text = string.Format("{0:#,##0.0}", plotMbf);
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            if (plotlpa > 0)
                nMeasuredPlots++;   // inc if had measured TPAs
            plotlpa = 0;
            // Plot
            plotba = 0;
            plottpa = 0;
            plotMbf = 0;
            plotCcf = 0;
            treeNetBdFt = 0;
            treeNetCuFt = 0;
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Stand
            xrTractSumBasalPA.Text = string.Format("{0:0.0}", standba); // was /standPlots
            xrTractSumTreesPA.Text = string.Format("{0:0.0}", standtpa); // /standPlots
            xrLabel11.Text = string.Format("{0:#,###}", standCcf); // /standPlots
            xrLabel10.Text = string.Format("{0:#,###}", standMbf); // /standPlots
            float lpa = (nMeasuredPlots > 0) ? standlpa / nMeasuredPlots : 0;
            xrTractSumLogsPA.Text = string.Format("{0:0.0}", lpa);
            xrTractSumNetCcfPA.Text = string.Format("{0:#,###}", standCuFt);
            xrTractSumNetMbfPA.Text = string.Format("{0:#,###}", standBdFt);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            // Stand
            standba = 0;
            standtpa = 0;
            standlpa = 0;
            standMbf = 0;
            standCcf = 0;
            standCuFt = 0;
            standBdFt = 0;
            treeNetBdFt = 0;
            treeNetCuFt = 0;
            nMeasuredPlots = 0;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // this should only occur once per stand
            standAcres = Convert.ToSingle(this.Parameters["Acres"].Value.ToString());
            standPlots = Convert.ToInt16(this.Parameters["Plots"].Value.ToString());

            // read flags
            string plotFactor = ReportUtils.GetStringFromColumn(this, "PlotFactor");
            bWasStripCruise = (plotFactor.StartsWith("S"));
            int treeNumber = ReportUtils.GetIntFromColumn(this, "TreeNumber");
            bFirstTree = (treeNumber == 1);
            string treeType = ReportUtils.GetStringFromColumn(this, "TreeType");
            bIsCountTree = (treeType == "C");   // lookup tree type to check whether this is a count plot
            string currentTotalHt = ReportUtils.GetStringFromColumn(this, "CalcTotalHtUsedYN");
            bWasCalculatedHt = (currentTotalHt == "Y");

            // use flag to derive divisor
            float divisor = bWasStripCruise ? standAcres : standPlots;

            // start the row-by-row math
            float ba = ReportUtils.GetFloatFromColumn(this, "BasalArea");
            float tpa = ReportUtils.GetFloatFromColumn(this, "TreesPerAcre");
            float lpa = ReportUtils.GetFloatFromColumn(this, "LogsPerAcre");
            float cuft = ReportUtils.GetFloatFromColumn(this, "NetCcfPerAcre");
            float bdft = ReportUtils.GetFloatFromColumn(this, "NetMbfPerAcre");
            float ccf = ReportUtils.GetFloatFromColumn(this, "TotalCcf");
            float mbf = ReportUtils.GetFloatFromColumn(this, "TotalMbf");

            // direct, weighted-direct accumulators
            plotba += ba;
            plottpa += tpa;
            treeNetCuFt += cuft;
            treeNetBdFt += bdft;
            plotCcf += cuft * tpa;
            plotMbf += bdft * tpa;

            // conditional averaging
            plotlpa += lpa;
            standlpa += lpa;    // data is already zero upon condition, no compute here

            //averaged,  weighted-average accumulators
            if (divisor > 0) {
                standba += ba / divisor;
                standtpa += tpa / divisor;
                standCuFt += cuft / divisor;
                standBdFt += bdft / divisor;
                standCcf += (ccf * tpa) / divisor;
                standMbf += (bdft * tpa) / divisor;
            }
        }

        // Default highlight handling; special treatments are below
        // this event is triggered as handler by detail cells
        private void xrLabel95_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // style count plot higlights (includes fonting)
            ReportUtils.StyleCellHighlights(sender as XRLabel, bIsCountTree);
        }

        // override default: spcl color to indicate Calculated Heights
        private void xrLabel84_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel95_BeforePrint(sender, e);
            XRLabel label = sender as XRLabel;
            label.ForeColor = (bWasCalculatedHt) ? Color.Red : label.ForeColor;
        }

        // override default: don't print if first tree
        private void xrLabel96_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (bFirstTree)
            {
                ((XRLabel)sender).Text = string.Empty;
                return;
            }
            xrLabel95_BeforePrint(sender, e);
        }

        private void PlotTreeListVolumeSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            //objectDataSource1.DataSource =
            //    TempBLL.GetRptPlotTreeList(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
            objectDataSource1.DataSource =
                TempBLL.GetRptPlotTreeListVolume(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }
    }
}
