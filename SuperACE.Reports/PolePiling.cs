﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperACE.Reports
{
    public class PolePiling
    {
        public float TreesPerAcre = 0;
        public float ButtDia = 0;
        public float TopDia = 0;
        public float PerLogNetScribner = 0;
        public float PerLogNetCubic = 0;
        public float PerLogLineal = 0;
        public float PerAcreNetScribner = 0;
        public float PerAcreNetCubic = 0;
        public float PerAcreLineal = 0;

        public float TotalTons = 0;
        public float TotalNetScribner = 0;
        public float TotalNetCubic = 0;
        public float TotalLineal = 0;

        public long StandsSelectedId = -1;
        public long saveTreesIdreesId = -1;
        public long StandsId = -1;
        public long RealStandsId = -1;
        public long TreesId = -1;
        public long TreeSegmentsId = -1;
        public string Species = string.Empty;
        public string TractName = string.Empty;
        public string StandName = string.Empty;
        public string SortGroup = string.Empty;
        public string Sort = string.Empty;

        public void Init()
        {
            TreesPerAcre = 0;
            ButtDia = 0;
            TopDia = 0;
            PerLogNetScribner = 0;
            PerLogNetCubic = 0;
            PerLogLineal = 0;
            PerAcreNetScribner = 0;
            PerAcreNetCubic = 0;
            PerAcreLineal = 0;
            TotalTons = 0;
            TotalNetScribner = 0;
            TotalNetCubic = 0;
            TotalLineal = 0;

            StandsSelectedId = -1;
            saveTreesIdreesId = -1;
            StandsId = -1;
            RealStandsId = -1;
            TreesId = -1;
            TreeSegmentsId = -1;
            Species = string.Empty;
            TractName = string.Empty;
            StandName = string.Empty;
            SortGroup = string.Empty;
            Sort = string.Empty;
        }
    }
}
