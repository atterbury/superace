﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using SuperACEUtils;
using Temp.DAL;
using Project.DAL;

namespace SuperACE.Reports
{
    public partial class RevenueBySpeciesProductSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        private float acres = 0;
        private float speciesTotalLogs = 0;
        private float speciesTotalDollars = 0;
        private float speciesTotalTons = 0;
        private float speciesTotalCcf = 0;
        private float speciesTotalMbf = 0;
        private float speciesAvgLogDia = 0;
        private float speciesAvgLogLen = 0;
        private float speciesDollarsLog = 0;
        private float speciesDollarsAcre = 0;
        private float speciesDollarsTon = 0;
        private float speciesDollarsCcf = 0;
        private float speciesDollarsMbf = 0;

        private float speciesTotalsTotalLogs = 0;
        private float speciesTotalsTotalDollars = 0;
        private float speciesTotalsTotalTons = 0;
        private float speciesTotalsTotalCcf = 0;
        private float speciesTotalsTotalMbf = 0;
        private float speciesTotalsAvgLogDia = 0;
        private float speciesTotalsAvgLogLen = 0;
        private float speciesTotalsDollarsLog = 0;
        private float speciesTotalsDollarsAcre = 0;
        private float speciesTotalsDollarsTon = 0;
        private float speciesTotalsDollarsCcf = 0;
        private float speciesTotalsDollarsMbf = 0;


        public RevenueBySpeciesProductSubReport()
        {
            InitializeComponent();
        }

        private void RevenueBySpeciesProductSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptSpeciesSortTimberEvaluation(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
            acres = TempBLL.GetStandAcresForReport(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAvgDia").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesAvgLogDia += Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAvgDia").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsAvgLogDia += Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAvgDia").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAveLen").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesAvgLogLen += Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAveLen").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsAvgLogLen += Utils.ConvertToFloat(this.GetCurrentColumnValue("LogAveLen").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerLog").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesDollarsLog += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerLog").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsDollarsLog += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerLog").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerAcre").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesDollarsAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerAcre").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsDollarsAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerAcre").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerTon").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesDollarsTon += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerTon").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsDollarsTon += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerTon").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerCcf").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesDollarsCcf += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerCcf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsDollarsCcf += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerCcf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerMbf").ToString()) > 0 && Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesDollarsMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerMbf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsDollarsMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("DollarsPerMbf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
            //if (Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString()) > 0)
            //{
            //    speciesTotalLogs += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //    speciesTotalsTotalLogs += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalLogs").ToString());
            //}
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            if (Utils.ConvertToFloat(xrLabel28.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesAvgLogDia += Utils.ConvertToFloat(xrLabel28.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsAvgLogDia += Utils.ConvertToFloat(xrLabel28.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel29.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesAvgLogLen += Utils.ConvertToFloat(xrLabel29.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsAvgLogLen += Utils.ConvertToFloat(xrLabel29.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel18.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesDollarsLog += Utils.ConvertToFloat(xrLabel18.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsDollarsLog += Utils.ConvertToFloat(xrLabel18.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel19.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesDollarsAcre += Utils.ConvertToFloat(xrLabel19.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsDollarsAcre += Utils.ConvertToFloat(xrLabel19.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel20.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesDollarsTon += Utils.ConvertToFloat(xrLabel20.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsDollarsTon += Utils.ConvertToFloat(xrLabel20.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel24.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesDollarsCcf += Utils.ConvertToFloat(xrLabel24.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsDollarsCcf += Utils.ConvertToFloat(xrLabel24.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel21.Text) > 0 && Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesDollarsMbf += Utils.ConvertToFloat(xrLabel21.Text) * Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsDollarsMbf += Utils.ConvertToFloat(xrLabel21.Text) * Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel14.Text) > 0)
            {
                speciesTotalLogs += Utils.ConvertToFloat(xrLabel14.Text);
                speciesTotalsTotalLogs += Utils.ConvertToFloat(xrLabel14.Text);
            }
            if (Utils.ConvertToFloat(xrLabel15.Text) > 0)
            {
                speciesTotalTons += Utils.ConvertToFloat(xrLabel15.Text);
                speciesTotalsTotalTons += Utils.ConvertToFloat(xrLabel15.Text);
            }
            if (Utils.ConvertToFloat(xrLabel16.Text) > 0)
            {
                speciesTotalCcf += Utils.ConvertToFloat(xrLabel16.Text);
                speciesTotalsTotalCcf += Utils.ConvertToFloat(xrLabel16.Text);
            }
            if (Utils.ConvertToFloat(xrLabel17.Text) > 0)
            {
                speciesTotalMbf += Utils.ConvertToFloat(xrLabel17.Text);
                speciesTotalsTotalMbf += Utils.ConvertToFloat(xrLabel17.Text);
            }
            if (Utils.ConvertToFloat(xrLabel22.Text) > 0)
            {
                speciesTotalDollars += Utils.ConvertToFloat(xrLabel22.Text);
                speciesTotalsTotalDollars += Utils.ConvertToFloat(xrLabel22.Text);
            }
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (speciesTotalLogs > 0)
                xrLabel35.Text = string.Format("{0:#,0}", speciesTotalLogs);
            else
                xrLabel35.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalTons > 0)
                xrLabel34.Text = string.Format("{0:#,0}", speciesTotalTons);
            else
                xrLabel34.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalCcf > 0)
                xrLabel33.Text = string.Format("{0:#,0}", speciesTotalCcf);
            else
                xrLabel33.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalMbf > 0)
                xrLabel32.Text = string.Format("{0:#,0}", speciesTotalMbf);
            else
                xrLabel32.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalDollars > 0)
                xrLabel31.Text = string.Format("{0:#,0}", speciesTotalDollars);
            else
                xrLabel31.Text = string.Format("{0:#,0}", 0);

            if (speciesAvgLogDia > 0)
                xrLabel41.Text = string.Format("{0:0.0}", speciesAvgLogDia/speciesTotalLogs);
            else
                xrLabel41.Text = string.Format("{0:0.0}", 0);
            if (speciesAvgLogLen > 0)
                xrLabel42.Text = string.Format("{0:0.0}", speciesAvgLogLen / speciesTotalLogs);
            else
                xrLabel42.Text = string.Format("{0:0.0}", 0);

            if (speciesTotalLogs > 0)
                xrLabel47.Text = string.Format("{0:#,0}", speciesTotalDollars / speciesTotalLogs); // speciesDollarsLog / speciesTotalLogs
            else
                xrLabel47.Text = string.Format("{0:#,0}", 0);
            if (speciesDollarsAcre > 0)
                xrLabel44.Text = string.Format("{0:#,0}", speciesTotalDollars / 30.26); // speciesTotalLogs / speciesTotalLogs
            else
                xrLabel44.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalTons > 0)
                xrLabel45.Text = string.Format("{0:#,0}", speciesTotalDollars / speciesTotalTons); // speciesDollarsTon / speciesTotalLogs
            else
                xrLabel45.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalCcf > 0)
                xrLabel46.Text = string.Format("{0:#,0}", speciesTotalDollars / speciesTotalCcf); //speciesDollarsCcf / speciesTotalLogs
            else
                xrLabel46.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalMbf > 0)
                xrLabel43.Text = string.Format("{0:#,0}", speciesTotalDollars / speciesTotalMbf); // speciesDollarsMbf / speciesTotalLogs
            else
                xrLabel43.Text = string.Format("{0:#,0}", 0);

            //if (speciesTotalDollars > 0 && acres > 0)
            //    xrLabel44.Text = string.Format("{0:#,0}", speciesTotalDollars / acres);
            //else
            //    xrLabel44.Text = string.Format("{0:#,0}", 0);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            speciesTotalLogs = 0;
            speciesAvgLogDia = 0;
            speciesAvgLogLen = 0;
            speciesDollarsLog = 0;
            speciesDollarsAcre = 0;
            speciesDollarsTon = 0;
            speciesDollarsCcf = 0;
            speciesDollarsMbf = 0;
            speciesTotalDollars = 0;
            speciesTotalTons = 0;
            speciesTotalCcf = 0;
            speciesTotalMbf = 0;
    }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (speciesTotalsAvgLogDia > 0)
                xrLabel49.Text = string.Format("{0:0.0}", speciesTotalsAvgLogDia / speciesTotalsTotalLogs);
            else
                xrLabel49.Text = string.Format("{0:0.0}", 0);
            if (speciesTotalsAvgLogLen > 0)
                xrLabel54.Text = string.Format("{0:0.0}", speciesTotalsAvgLogLen / speciesTotalsTotalLogs);
            else
                xrLabel54.Text = string.Format("{0:0.0}", 0);

            if (speciesTotalsDollarsLog > 0)
                xrLabel53.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / speciesTotalsTotalLogs); // speciesTotalsDollarsLog / speciesTotalsTotalLogs
            else
                xrLabel53.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsDollarsAcre > 0)
                xrLabel52.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / 30.26); // speciesTotalsDollarsAcre / speciesTotalsTotalLogs
            else
                xrLabel52.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalTons > 0)
                xrLabel51.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / speciesTotalsTotalTons); // speciesTotalsDollarsTon / speciesTotalsTotalLogs
            else
                xrLabel51.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalCcf > 0)
                xrLabel50.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / speciesTotalsTotalCcf); // speciesTotalsDollarsCcf / speciesTotalsTotalLogs
            else
                xrLabel50.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalMbf > 0)
                xrLabel55.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / speciesTotalsTotalMbf); // speciesTotalsDollarsMbf / speciesTotalsTotalLogs
            else
                xrLabel55.Text = string.Format("{0:#,0}", 0);

            if (speciesTotalsTotalLogs > 0)
                xrLabel36.Text = string.Format("{0:#,0}", speciesTotalsTotalLogs);
            else
                xrLabel36.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalTons > 0)
                xrLabel37.Text = string.Format("{0:#,0}", speciesTotalsTotalTons);
            else
                xrLabel37.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalCcf > 0)
                xrLabel38.Text = string.Format("{0:#,0}", speciesTotalsTotalCcf);
            else
                xrLabel38.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalMbf > 0)
                xrLabel39.Text = string.Format("{0:#,0}", speciesTotalsTotalMbf);
            else
                xrLabel39.Text = string.Format("{0:#,0}", 0);
            if (speciesTotalsTotalDollars > 0)
                xrLabel40.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars);
            else
                xrLabel40.Text = string.Format("{0:#,0}", 0);

            //if (speciesTotalsTotalDollars > 0 && acres > 0)
            //    xrLabel52.Text = string.Format("{0:#,0}", speciesTotalsTotalDollars / acres);
            //else
            //    xrLabel52.Text = string.Format("{0:#,0}", 0);
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            speciesTotalsTotalLogs = 0;
            speciesTotalsAvgLogDia = 0;
            speciesTotalsAvgLogLen = 0;
            speciesTotalsDollarsLog = 0;
            speciesTotalsDollarsAcre = 0;
            speciesTotalsDollarsTon = 0;
            speciesTotalsDollarsCcf = 0;
            speciesTotalsDollarsMbf = 0;

            speciesTotalsTotalLogs = 0;
            speciesTotalsTotalDollars = 0;
            speciesTotalsTotalTons = 0;
            speciesTotalsTotalCcf = 0;
            speciesTotalsTotalMbf = 0;
        }
    }
}
