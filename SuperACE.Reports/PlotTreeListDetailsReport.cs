﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

using SuperACEUtils;

namespace SuperACE.Reports
{
    public partial class PlotTreeListDetailsReport : DevExpress.XtraReports.UI.XtraReport
    {
        SepStandsSelected current = null;
        int currentSegment  = 0;
        string treeType = string.Empty;
        string calculatedHt = string.Empty;

        public PlotTreeListDetailsReport()
        {
            InitializeComponent();

            bindingSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void DetailReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            bindingSource2.DataSource = TempBLL.GetRptPlotTreeList(current.StandsSelectedId);
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            current = bindingSource1.Current as SepStandsSelected;
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            currentSegment = ReportUtils.GetIntFromColumn(this, "Segment1");
            treeType = ReportUtils.GetStringFromColumn(this, "TreeType");
            calculatedHt = ReportUtils.GetStringFromColumn(this, "CalcTotalHtUsedYN");
        }

        // name is mismatched; this method is called on almost all detail labels that are shown
        private void xrLabel81_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = sender as XRLabel;
            if (currentSegment != 0 && currentSegment != 1)
            {
                label.Text = string.Empty;
                return;
            }
            ReportUtils.StyleCellHighlights(label, treeType == "C");
        }

        // this is the only exception, where total ht column gets some additional special treatment
        private void xrLabel102_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = sender as XRLabel;
            if (currentSegment != 0 && currentSegment != 1)
            {
                label.Text = string.Empty;
                return;
            }
            // first style higlights (includes fonting)
            ReportUtils.StyleCellHighlights(label, treeType == "C");

            // now override color of this specific cell
            label.ForeColor = (calculatedHt == "Y") ? Color.Red : label.ForeColor;

        }


        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            current = bindingSource1.Current as SepStandsSelected;
        }

    }
}
