﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class PlotListVolumeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        public int standPlots = 0;
        public float standAcres = 0;
        private int nMeasuredPlots = 0; 
        private float plotba = 0, plottpa = 0, plotCcf = 0, plotMbf = 0;
        private float standbapa = 0, standtpa = 0, standlpa = 0, standCuFt = 0, standBdFt = 0, standCcf = 0, standMbf = 0, standFF = 0, standTotalHt = 0;
        private float standBA = 0, standTPA = 0;
        // Plot FF and TotalHt are weighted averaged (using tpa, ie trees per acre); but only using measured trees.
        private float plotFF = 0, plotTotalHt = 0, plotTpaOnlyMeasured = 0, standTpaOnlyMeasured = 0;
        string plotPF = "";     // no PF monitoring for stand

        Dominance dominance = new Dominance();

        public PlotListVolumeSubReport()
        {
            InitializeComponent();
        }

        private void PlotListVolumeSubReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // process parameters for the report
            standAcres = Convert.ToSingle(this.Parameters["Acres"].Value);
            standPlots = Convert.ToInt32(this.Parameters["Plots"].Value);
            
            // setup dominance. start with reset, in case of re-entry for a second stand
            dominance.reset();
            // work out which column has been set as the DominantFeature for this stand
            string feature = Convert.ToString(this.Parameters["DominantFeature"].Value);
            // switch on feature and convert to column of interest
            string dc = null;
            switch (feature)
            {
                case "Trees per Acre": dc = "TreesPerAcre"; break;
                case "Net Cubic Feet / Acre": dc = "NetCCFPerAcre"; break;
                case "Net Board Feet / Acre": dc = "NetMBFPerAcre"; break;
                case "Basal Area / Acre": dc = "BasalArea"; break;
            }
            // pass down columns to dom tracker
            dominance.DominanceColumn = dc;
            dominance.GroupByColumn = "Species";
            dominance.SetColumnsOfWeightedInterest("AgeCode");
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Plot
            xrlPlotBaPa.Text = string.Format("{0:0.00}", plotba);
            xrlPlotTreesPa.Text = string.Format("{0:0.00}", plottpa);
            xrlPlotTotCcf.Text = string.Format("{0:#,##0.00}", plotCcf);
            xrlPlotTotMbf.Text = string.Format("{0:#,##0.000}", plotMbf);

            xrlPltTotHeight.Text = string.Format("{0:0}", (plotTpaOnlyMeasured > 0) ? plotTotalHt / plotTpaOnlyMeasured : 0);
            xrlPlotFF.Text = string.Format("{0:0}", (plotTpaOnlyMeasured > 0) ? plotFF / plotTpaOnlyMeasured : 0);
            xrlPlotDbh.Text = string.Format("{0:0.0}", (plottpa > 0) ? Math.Sqrt(plotba / (plottpa * 0.005454154)) : 0);
            xrlPlotPlotFactor.Text = plotPF;

            // set the dominance based column values
            string dominator = dominance.GetGroupByRowDominator();
            xrlPlotSpecies.Text = dominator;
            xrlPlotAge.Text = string.Format("{0:0}", dominance.GetGroupByRowItem("AgeCode", dominator));

            // compute LPA factors 
            nMeasuredPlots += (plotTpaOnlyMeasured > 0) ? 1 : 0;    // inc count of measured plots


        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            // Plot
            plotba = 0;
            plottpa = 0;
            plotMbf = 0;
            plotCcf = 0;
            plotFF = 0;
            plotTotalHt = 0;
            plotTpaOnlyMeasured = 0;
            plotPF = "";
            // dominance based
            dominance.resetGroupByRow();
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Stand
            xrlStandBaPa.Text = string.Format("{0:0.00}", standbapa); // was /standPlots
            xrlStandTreesPa.Text = string.Format("{0:0.00}", standtpa);
            float lpa = (nMeasuredPlots > 0) ? standlpa / (float)nMeasuredPlots : 0;
            // float lpa = (nMeasuredPlots > 0) ? standlpa / (float)standPlots : 0;
            xrlStandLogsPa.Text = string.Format("{0:0.0}", lpa);
            xrlStandNetCcfPa.Text = string.Format("{0:#,###}", standCuFt); // /standPlots
            xrlStandNetMbpPa.Text = string.Format("{0:#,###}", standBdFt);
            xrlStandTotCcf.Text = string.Format("{0:#,###}", standCcf); // /standPlots
            xrlStandTotMbf.Text = string.Format("{0:#,###}", standMbf); // /standPlots

            xrlStandTotHeight.Text = string.Format("{0:0}", (standTpaOnlyMeasured > 0) ? standTotalHt / standTpaOnlyMeasured : 0);
            xrlStandFF.Text = string.Format("{0:0}", (standTpaOnlyMeasured > 0) ? standFF / standTpaOnlyMeasured : 0);
            xrlStandDbh.Text = string.Format("{0:0.0}", (standTPA > 0) ? Math.Sqrt(standBA / (standTPA * 0.005454154)) : 0);

            // no stand plotFactor to be shown
            xrlStandPlotFactor.Text = "";

            // set the dominance based column values
            string dominator = dominance.GetGroupBySetDominator();
            xrlStandSpecies.Text = dominator;
            xrlStandAge.Text = string.Format("{0:0}", dominance.GetGroupBySetItem("AgeCode", dominator));
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            // Stand
            standBA = 0;
            standTPA = 0;
            standbapa = 0;
            standtpa = 0;
            standlpa = 0;
            standMbf = 0;
            standCcf = 0;
            standCuFt = 0;
            standBdFt = 0;
            standFF = 0;
            standTotalHt = 0;
            standTpaOnlyMeasured = 0;
            nMeasuredPlots = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // trigger tracking of dominance
            dominance.processGroupByRow(this);

            float ba = ReportUtils.GetFloatFromColumn(this, "BasalArea");
            plotba += ba;
            standBA += ba;

            float tpa = ReportUtils.GetFloatFromColumn(this, "TreesPerAcre");
            plottpa += tpa;
            standTPA += tpa;

            float ccf = ReportUtils.GetFloatFromColumn(this, "TotalCcf");
            plotCcf += ccf * tpa;

            float mbf = ReportUtils.GetFloatFromColumn(this, "TotalMbf");
            plotMbf += mbf * tpa;

            // get pf; retain if same across plot, otherwise replace with *
            string pf = ReportUtils.GetStringFromColumn(this, "PlotFactorCode");
            plotPF = ReportUtils.StarIfNotUnique(plotPF, pf);
            
            // if plotFactor starts with "S" use standAcres; otherwise use PlotAcres as a divisor
            float divisor = ((pf.Length > 0) && (pf[0] == 'S')) ? standAcres : standPlots;
            if (divisor > 0)
            {
                standbapa += ba / divisor;  // computing running average and reporting at the end
                standtpa += tpa / divisor;
                standCcf += ccf * tpa / divisor;
                standMbf += mbf * tpa / divisor;
                standCuFt += ReportUtils.GetFloatFromColumn(this, "NetCCFPerAcre") / divisor;
                standBdFt += ReportUtils.GetFloatFromColumn(this, "NetMBFPerAcre") / divisor;
            }

            // process only for measured trees
            if ((this.GetCurrentColumnValue("TreeMeasured") as byte?).GetValueOrDefault() > 0)
            {
                plotTpaOnlyMeasured += tpa;
                standTpaOnlyMeasured += tpa;
                standlpa += ReportUtils.GetFloatFromColumn(this, "LogsPerAcre"); 
                // sum weighted totalHt
                float totalHt = ReportUtils.GetFloatFromColumn(this, "TotalHeight") * tpa;
                plotTotalHt += totalHt;
                standTotalHt += totalHt;
                // sum weighted FF
                float fft = ReportUtils.GetFloatFromColumn(this, "FormFactor") * tpa;
                plotFF += fft;
                standFF += fft;
            }

            e.Cancel = true;
        }

        private void PlotTreeListVolumeSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            //objectDataSource1.DataSource =
            //    TempBLL.GetRptPlotTreeList(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
            //objectDataSource1.DataSource =
            //    TempBLL.GetRptPlotTreeListVolume(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }
    }
}
