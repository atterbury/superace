﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class PlotTreeListVolumeReport : DevExpress.XtraReports.UI.XtraReport
    {
        public PlotTreeListVolumeReport()
        {
            InitializeComponent();

            objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRSubreport)sender).ReportSource.Parameters["Acres"].Value = Convert.ToString(xrLabel114.Text);
            ((XRSubreport)sender).ReportSource.Parameters["Plots"].Value = Convert.ToString(xrLabel105.Text);
        }
    }
}
