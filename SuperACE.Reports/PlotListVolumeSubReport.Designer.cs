﻿namespace SuperACE.Reports
{
    partial class PlotListVolumeSubReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.Sql.SelectQuery selectQuery1 = new DevExpress.DataAccess.Sql.SelectQuery();
            DevExpress.DataAccess.Sql.Column column1 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression1 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Table table1 = new DevExpress.DataAccess.Sql.Table();
            DevExpress.DataAccess.Sql.Column column2 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression2 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column3 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression3 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column4 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression4 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column5 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression5 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column6 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression6 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column7 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression7 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column8 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression8 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column9 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression9 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column10 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression10 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column11 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression11 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column12 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression12 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column13 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression13 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column14 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression14 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column15 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression15 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column16 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression16 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column17 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression17 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column18 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression18 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column19 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression19 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column20 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression20 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column21 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression21 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column22 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression22 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column23 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression23 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column24 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression24 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column25 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression25 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column26 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression26 = new DevExpress.DataAccess.Sql.ColumnExpression();
            DevExpress.DataAccess.Sql.Column column27 = new DevExpress.DataAccess.Sql.Column();
            DevExpress.DataAccess.Sql.ColumnExpression columnExpression27 = new DevExpress.DataAccess.Sql.ColumnExpression();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlotListVolumeSubReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrlTreeMeasured = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotFactor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlAge = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlSpecies = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTreeStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTreeCount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlDbh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlFF = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTotHeight = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlBaPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTreesPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlLogsPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlNetCcfPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlNetMbfPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTotCcf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTotMbf = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.StandsSelectedId = new DevExpress.XtraReports.Parameters.Parameter();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlPlotAge = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotPlotFactor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBlank0 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotSpecies = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotTreeMeasured = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotDbh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotFF = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPltTotHeight = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotTreeCount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotPlotNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotBaPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotNetCcfPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotLogsPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotTreesPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotMbpPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotTotCcf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlPlotTotMbf = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlStandPlotFactor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandAge = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandSpecies = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTreeMeasured = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandDbh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandFF = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTotHeight = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTreeCount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrStandNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTotMbf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTotCcf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandNetMbpPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandNetCcfPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandLogsPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandTreesPa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlStandBaPa = new DevExpress.XtraReports.UI.XRLabel();
            this.Acres = new DevExpress.XtraReports.Parameters.Parameter();
            this.Plots = new DevExpress.XtraReports.Parameters.Parameter();
            this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
            this.DominantFeature = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlTreeMeasured,
            this.xrlPlotNumber,
            this.xrlPlotFactor,
            this.xrlAge,
            this.xrlSpecies,
            this.xrlTreeStatus,
            this.xrlTreeCount,
            this.xrlDbh,
            this.xrlFF,
            this.xrlTotHeight,
            this.xrlBaPa,
            this.xrlTreesPa,
            this.xrlLogsPa,
            this.xrlNetCcfPa,
            this.xrlNetMbfPa,
            this.xrlTotCcf,
            this.xrlTotMbf});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detail.HeightF = 15F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrlTreeMeasured
            // 
            this.xrlTreeMeasured.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeMeasured", "{0:#,#}")});
            this.xrlTreeMeasured.LocationFloat = new DevExpress.Utils.PointFloat(180F, 0F);
            this.xrlTreeMeasured.Name = "xrlTreeMeasured";
            this.xrlTreeMeasured.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTreeMeasured.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlTreeMeasured.StylePriority.UseTextAlignment = false;
            this.xrlTreeMeasured.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotNumber
            // 
            this.xrlPlotNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.PlotNumber")});
            this.xrlPlotNumber.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrlPlotNumber.Name = "xrlPlotNumber";
            this.xrlPlotNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotNumber.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrlPlotNumber.StylePriority.UseTextAlignment = false;
            this.xrlPlotNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrlPlotFactor
            // 
            this.xrlPlotFactor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.PlotFactorCode")});
            this.xrlPlotFactor.LocationFloat = new DevExpress.Utils.PointFloat(40F, 0F);
            this.xrlPlotFactor.Name = "xrlPlotFactor";
            this.xrlPlotFactor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotFactor.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlPlotFactor.StylePriority.UseTextAlignment = false;
            this.xrlPlotFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrlAge
            // 
            this.xrlAge.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.AgeCode")});
            this.xrlAge.LocationFloat = new DevExpress.Utils.PointFloat(75F, 0F);
            this.xrlAge.Name = "xrlAge";
            this.xrlAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlAge.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlAge.StylePriority.UseTextAlignment = false;
            this.xrlAge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrlSpecies
            // 
            this.xrlSpecies.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.Species")});
            this.xrlSpecies.LocationFloat = new DevExpress.Utils.PointFloat(110F, 0F);
            this.xrlSpecies.Name = "xrlSpecies";
            this.xrlSpecies.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlSpecies.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlSpecies.StylePriority.UseTextAlignment = false;
            this.xrlSpecies.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrlTreeStatus
            // 
            this.xrlTreeStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeStatus")});
            this.xrlTreeStatus.LocationFloat = new DevExpress.Utils.PointFloat(145F, 0F);
            this.xrlTreeStatus.Name = "xrlTreeStatus";
            this.xrlTreeStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTreeStatus.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlTreeStatus.StylePriority.UseTextAlignment = false;
            this.xrlTreeStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrlTreeCount
            // 
            this.xrlTreeCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeCount", "{0:#,#}")});
            this.xrlTreeCount.LocationFloat = new DevExpress.Utils.PointFloat(215F, 0F);
            this.xrlTreeCount.Name = "xrlTreeCount";
            this.xrlTreeCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTreeCount.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlTreeCount.StylePriority.UseTextAlignment = false;
            this.xrlTreeCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlDbh
            // 
            this.xrlDbh.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.Dbh", "{0:n1}")});
            this.xrlDbh.LocationFloat = new DevExpress.Utils.PointFloat(250F, 0F);
            this.xrlDbh.Name = "xrlDbh";
            this.xrlDbh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlDbh.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlDbh.StylePriority.UseTextAlignment = false;
            this.xrlDbh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlFF
            // 
            this.xrlFF.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.FormFactor", "{0:n0}")});
            this.xrlFF.LocationFloat = new DevExpress.Utils.PointFloat(285F, 0F);
            this.xrlFF.Name = "xrlFF";
            this.xrlFF.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlFF.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlFF.StylePriority.UseTextAlignment = false;
            this.xrlFF.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrlTotHeight
            // 
            this.xrlTotHeight.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalHeight", "{0:n0}")});
            this.xrlTotHeight.LocationFloat = new DevExpress.Utils.PointFloat(320F, 0F);
            this.xrlTotHeight.Name = "xrlTotHeight";
            this.xrlTotHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTotHeight.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrlTotHeight.StylePriority.UseTextAlignment = false;
            this.xrlTotHeight.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlBaPa
            // 
            this.xrlBaPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.BasalArea")});
            this.xrlBaPa.LocationFloat = new DevExpress.Utils.PointFloat(355F, 0F);
            this.xrlBaPa.Name = "xrlBaPa";
            this.xrlBaPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlBaPa.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrlBaPa.StylePriority.UseTextAlignment = false;
            this.xrlBaPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrlBaPa.TextFormatString = "{0:#.0}";
            // 
            // xrlTreesPa
            // 
            this.xrlTreesPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreesPerAcre")});
            this.xrlTreesPa.LocationFloat = new DevExpress.Utils.PointFloat(395F, 0F);
            this.xrlTreesPa.Name = "xrlTreesPa";
            this.xrlTreesPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTreesPa.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrlTreesPa.StylePriority.UseTextAlignment = false;
            this.xrlTreesPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrlTreesPa.TextFormatString = "{0:#.0}";
            // 
            // xrlLogsPa
            // 
            this.xrlLogsPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.LogsPerAcre", "{0:n1}")});
            this.xrlLogsPa.LocationFloat = new DevExpress.Utils.PointFloat(435F, 0F);
            this.xrlLogsPa.Name = "xrlLogsPa";
            this.xrlLogsPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlLogsPa.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrlLogsPa.StylePriority.UseTextAlignment = false;
            this.xrlLogsPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlNetCcfPa
            // 
            this.xrlNetCcfPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.NetCCFPerAcre", "{0:n0}")});
            this.xrlNetCcfPa.LocationFloat = new DevExpress.Utils.PointFloat(475F, 0F);
            this.xrlNetCcfPa.Name = "xrlNetCcfPa";
            this.xrlNetCcfPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlNetCcfPa.SizeF = new System.Drawing.SizeF(55F, 15F);
            this.xrlNetCcfPa.StylePriority.UseTextAlignment = false;
            this.xrlNetCcfPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlNetMbfPa
            // 
            this.xrlNetMbfPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.NetMBFPerAcre", "{0:n0}")});
            this.xrlNetMbfPa.LocationFloat = new DevExpress.Utils.PointFloat(530F, 0F);
            this.xrlNetMbfPa.Name = "xrlNetMbfPa";
            this.xrlNetMbfPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlNetMbfPa.SizeF = new System.Drawing.SizeF(55F, 15F);
            this.xrlNetMbfPa.StylePriority.UseTextAlignment = false;
            this.xrlNetMbfPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlTotCcf
            // 
            this.xrlTotCcf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalCCF", "{0:n0}")});
            this.xrlTotCcf.LocationFloat = new DevExpress.Utils.PointFloat(585F, 0F);
            this.xrlTotCcf.Name = "xrlTotCcf";
            this.xrlTotCcf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlTotCcf.SizeF = new System.Drawing.SizeF(50F, 15F);
            this.xrlTotCcf.StylePriority.UseTextAlignment = false;
            this.xrlTotCcf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlTotMbf
            // 
            this.xrlTotMbf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalMBF", "{0:n0}")});
            this.xrlTotMbf.LocationFloat = new DevExpress.Utils.PointFloat(635F, 0F);
            this.xrlTotMbf.Name = "xrlTotMbf";
            this.xrlTotMbf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 17, 0, 0, 100F);
            this.xrlTotMbf.SizeF = new System.Drawing.SizeF(65F, 15F);
            this.xrlTotMbf.StylePriority.UsePadding = false;
            this.xrlTotMbf.StylePriority.UseTextAlignment = false;
            this.xrlTotMbf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // StandsSelectedId
            // 
            this.StandsSelectedId.Name = "StandsSelectedId";
            this.StandsSelectedId.Type = typeof(long);
            this.StandsSelectedId.ValueInfo = "0";
            this.StandsSelectedId.Visible = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("PlotNumber", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Visible = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlPlotAge,
            this.xrlPlotPlotFactor,
            this.xrBlank0,
            this.xrlPlotSpecies,
            this.xrlPlotTreeMeasured,
            this.xrlPlotDbh,
            this.xrlPlotFF,
            this.xrlPltTotHeight,
            this.xrlPlotTreeCount,
            this.xrlPlotPlotNumber,
            this.xrlPlotBaPa,
            this.xrlPlotNetCcfPa,
            this.xrlPlotLogsPa,
            this.xrlPlotTreesPa,
            this.xrlPlotMbpPa,
            this.xrlPlotTotCcf,
            this.xrlPlotTotMbf});
            this.GroupFooter1.HeightF = 23.00001F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter1_BeforePrint);
            this.GroupFooter1.AfterPrint += new System.EventHandler(this.GroupFooter1_AfterPrint);
            // 
            // xrlPlotAge
            // 
            this.xrlPlotAge.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotAge.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotAge.LocationFloat = new DevExpress.Utils.PointFloat(74.99999F, 0F);
            this.xrlPlotAge.Name = "xrlPlotAge";
            this.xrlPlotAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotAge.SizeF = new System.Drawing.SizeF(34.99999F, 23.00001F);
            this.xrlPlotAge.StylePriority.UseBorders = false;
            this.xrlPlotAge.StylePriority.UseFont = false;
            this.xrlPlotAge.StylePriority.UseTextAlignment = false;
            this.xrlPlotAge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotPlotFactor
            // 
            this.xrlPlotPlotFactor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotPlotFactor.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotPlotFactor.LocationFloat = new DevExpress.Utils.PointFloat(40F, 0F);
            this.xrlPlotPlotFactor.Name = "xrlPlotPlotFactor";
            this.xrlPlotPlotFactor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotPlotFactor.SizeF = new System.Drawing.SizeF(34.99999F, 23.00001F);
            this.xrlPlotPlotFactor.StylePriority.UseBorders = false;
            this.xrlPlotPlotFactor.StylePriority.UseFont = false;
            this.xrlPlotPlotFactor.StylePriority.UseTextAlignment = false;
            this.xrlPlotPlotFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrBlank0
            // 
            this.xrBlank0.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrBlank0.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrBlank0.LocationFloat = new DevExpress.Utils.PointFloat(145F, 0F);
            this.xrBlank0.Name = "xrBlank0";
            this.xrBlank0.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrBlank0.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrBlank0.StylePriority.UseBorders = false;
            this.xrBlank0.StylePriority.UseFont = false;
            this.xrBlank0.StylePriority.UseTextAlignment = false;
            this.xrBlank0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotSpecies
            // 
            this.xrlPlotSpecies.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotSpecies.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotSpecies.LocationFloat = new DevExpress.Utils.PointFloat(110F, 0F);
            this.xrlPlotSpecies.Name = "xrlPlotSpecies";
            this.xrlPlotSpecies.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotSpecies.SizeF = new System.Drawing.SizeF(34.99999F, 23.00001F);
            this.xrlPlotSpecies.StylePriority.UseBorders = false;
            this.xrlPlotSpecies.StylePriority.UseFont = false;
            this.xrlPlotSpecies.StylePriority.UseTextAlignment = false;
            this.xrlPlotSpecies.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotTreeMeasured
            // 
            this.xrlPlotTreeMeasured.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotTreeMeasured.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeMeasured")});
            this.xrlPlotTreeMeasured.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotTreeMeasured.LocationFloat = new DevExpress.Utils.PointFloat(180F, 0F);
            this.xrlPlotTreeMeasured.Name = "xrlPlotTreeMeasured";
            this.xrlPlotTreeMeasured.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotTreeMeasured.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrlPlotTreeMeasured.StylePriority.UseBorders = false;
            this.xrlPlotTreeMeasured.StylePriority.UseFont = false;
            this.xrlPlotTreeMeasured.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotTreeMeasured.Summary = xrSummary1;
            this.xrlPlotTreeMeasured.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotDbh
            // 
            this.xrlPlotDbh.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotDbh.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotDbh.LocationFloat = new DevExpress.Utils.PointFloat(250F, 0F);
            this.xrlPlotDbh.Name = "xrlPlotDbh";
            this.xrlPlotDbh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotDbh.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrlPlotDbh.StylePriority.UseBorders = false;
            this.xrlPlotDbh.StylePriority.UseFont = false;
            this.xrlPlotDbh.StylePriority.UseTextAlignment = false;
            this.xrlPlotDbh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotFF
            // 
            this.xrlPlotFF.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotFF.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotFF.LocationFloat = new DevExpress.Utils.PointFloat(285F, 0F);
            this.xrlPlotFF.Name = "xrlPlotFF";
            this.xrlPlotFF.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotFF.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrlPlotFF.StylePriority.UseBorders = false;
            this.xrlPlotFF.StylePriority.UseFont = false;
            this.xrlPlotFF.StylePriority.UseTextAlignment = false;
            this.xrlPlotFF.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPltTotHeight
            // 
            this.xrlPltTotHeight.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPltTotHeight.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPltTotHeight.LocationFloat = new DevExpress.Utils.PointFloat(320F, 0F);
            this.xrlPltTotHeight.Name = "xrlPltTotHeight";
            this.xrlPltTotHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPltTotHeight.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrlPltTotHeight.StylePriority.UseBorders = false;
            this.xrlPltTotHeight.StylePriority.UseFont = false;
            this.xrlPltTotHeight.StylePriority.UseTextAlignment = false;
            this.xrlPltTotHeight.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotTreeCount
            // 
            this.xrlPlotTreeCount.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotTreeCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeCount")});
            this.xrlPlotTreeCount.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotTreeCount.LocationFloat = new DevExpress.Utils.PointFloat(215F, 0F);
            this.xrlPlotTreeCount.Name = "xrlPlotTreeCount";
            this.xrlPlotTreeCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotTreeCount.SizeF = new System.Drawing.SizeF(35F, 23.00001F);
            this.xrlPlotTreeCount.StylePriority.UseBorders = false;
            this.xrlPlotTreeCount.StylePriority.UseFont = false;
            this.xrlPlotTreeCount.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:#,#}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotTreeCount.Summary = xrSummary2;
            this.xrlPlotTreeCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotPlotNumber
            // 
            this.xrlPlotPlotNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotPlotNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.PlotNumber")});
            this.xrlPlotPlotNumber.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotPlotNumber.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrlPlotPlotNumber.Name = "xrlPlotPlotNumber";
            this.xrlPlotPlotNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotPlotNumber.SizeF = new System.Drawing.SizeF(40F, 23F);
            this.xrlPlotPlotNumber.StylePriority.UseBorders = false;
            this.xrlPlotPlotNumber.StylePriority.UseFont = false;
            this.xrlPlotPlotNumber.StylePriority.UseTextAlignment = false;
            this.xrlPlotPlotNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrlPlotBaPa
            // 
            this.xrlPlotBaPa.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotBaPa.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotBaPa.LocationFloat = new DevExpress.Utils.PointFloat(355F, 0F);
            this.xrlPlotBaPa.Name = "xrlPlotBaPa";
            this.xrlPlotBaPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotBaPa.SizeF = new System.Drawing.SizeF(40.00003F, 23.00001F);
            this.xrlPlotBaPa.StylePriority.UseBorders = false;
            this.xrlPlotBaPa.StylePriority.UseFont = false;
            this.xrlPlotBaPa.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n1}";
            this.xrlPlotBaPa.Summary = xrSummary3;
            this.xrlPlotBaPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotNetCcfPa
            // 
            this.xrlPlotNetCcfPa.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotNetCcfPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.NetCCFPerAcre")});
            this.xrlPlotNetCcfPa.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotNetCcfPa.LocationFloat = new DevExpress.Utils.PointFloat(475.0002F, 0F);
            this.xrlPlotNetCcfPa.Name = "xrlPlotNetCcfPa";
            this.xrlPlotNetCcfPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotNetCcfPa.SizeF = new System.Drawing.SizeF(54.99969F, 23.00001F);
            this.xrlPlotNetCcfPa.StylePriority.UseBorders = false;
            this.xrlPlotNetCcfPa.StylePriority.UseFont = false;
            this.xrlPlotNetCcfPa.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n0}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotNetCcfPa.Summary = xrSummary4;
            this.xrlPlotNetCcfPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotLogsPa
            // 
            this.xrlPlotLogsPa.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotLogsPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.LogsPerAcre")});
            this.xrlPlotLogsPa.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotLogsPa.LocationFloat = new DevExpress.Utils.PointFloat(435F, 0F);
            this.xrlPlotLogsPa.Name = "xrlPlotLogsPa";
            this.xrlPlotLogsPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotLogsPa.SizeF = new System.Drawing.SizeF(39.99997F, 23.00001F);
            this.xrlPlotLogsPa.StylePriority.UseBorders = false;
            this.xrlPlotLogsPa.StylePriority.UseFont = false;
            this.xrlPlotLogsPa.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n1}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotLogsPa.Summary = xrSummary5;
            this.xrlPlotLogsPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotTreesPa
            // 
            this.xrlPlotTreesPa.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotTreesPa.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotTreesPa.LocationFloat = new DevExpress.Utils.PointFloat(395F, 0F);
            this.xrlPlotTreesPa.Name = "xrlPlotTreesPa";
            this.xrlPlotTreesPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotTreesPa.SizeF = new System.Drawing.SizeF(39.99997F, 23.00001F);
            this.xrlPlotTreesPa.StylePriority.UseBorders = false;
            this.xrlPlotTreesPa.StylePriority.UseFont = false;
            this.xrlPlotTreesPa.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            this.xrlPlotTreesPa.Summary = xrSummary6;
            this.xrlPlotTreesPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotMbpPa
            // 
            this.xrlPlotMbpPa.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotMbpPa.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.NetMBFPerAcre")});
            this.xrlPlotMbpPa.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotMbpPa.LocationFloat = new DevExpress.Utils.PointFloat(530F, 0F);
            this.xrlPlotMbpPa.Name = "xrlPlotMbpPa";
            this.xrlPlotMbpPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotMbpPa.SizeF = new System.Drawing.SizeF(54.99994F, 23F);
            this.xrlPlotMbpPa.StylePriority.UseBorders = false;
            this.xrlPlotMbpPa.StylePriority.UseFont = false;
            this.xrlPlotMbpPa.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:n0}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotMbpPa.Summary = xrSummary7;
            this.xrlPlotMbpPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotTotCcf
            // 
            this.xrlPlotTotCcf.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotTotCcf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalCCF")});
            this.xrlPlotTotCcf.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotTotCcf.LocationFloat = new DevExpress.Utils.PointFloat(584.9999F, 0F);
            this.xrlPlotTotCcf.Name = "xrlPlotTotCcf";
            this.xrlPlotTotCcf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlPlotTotCcf.SizeF = new System.Drawing.SizeF(50.00006F, 23F);
            this.xrlPlotTotCcf.StylePriority.UseBorders = false;
            this.xrlPlotTotCcf.StylePriority.UseFont = false;
            this.xrlPlotTotCcf.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:n0}";
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotTotCcf.Summary = xrSummary8;
            this.xrlPlotTotCcf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlPlotTotMbf
            // 
            this.xrlPlotTotMbf.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlPlotTotMbf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalMBF")});
            this.xrlPlotTotMbf.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrlPlotTotMbf.LocationFloat = new DevExpress.Utils.PointFloat(635F, 0F);
            this.xrlPlotTotMbf.Name = "xrlPlotTotMbf";
            this.xrlPlotTotMbf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 17, 0, 0, 100F);
            this.xrlPlotTotMbf.SizeF = new System.Drawing.SizeF(65F, 23F);
            this.xrlPlotTotMbf.StylePriority.UseBorders = false;
            this.xrlPlotTotMbf.StylePriority.UseFont = false;
            this.xrlPlotTotMbf.StylePriority.UsePadding = false;
            this.xrlPlotTotMbf.StylePriority.UseTextAlignment = false;
            xrSummary9.FormatString = "{0:n0}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlPlotTotMbf.Summary = xrSummary9;
            this.xrlPlotTotMbf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Visible = false;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlStandPlotFactor,
            this.xrlStandAge,
            this.xrlStandSpecies,
            this.xrLabel33,
            this.xrlStandTreeMeasured,
            this.xrlStandDbh,
            this.xrlStandFF,
            this.xrlStandTotHeight,
            this.xrlStandTreeCount,
            this.xrStandNumber,
            this.xrlStandTotMbf,
            this.xrlStandTotCcf,
            this.xrlStandNetMbpPa,
            this.xrlStandNetCcfPa,
            this.xrlStandLogsPa,
            this.xrlStandTreesPa,
            this.xrlStandBaPa});
            this.GroupFooter2.HeightF = 35F;
            this.GroupFooter2.Level = 1;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter2_BeforePrint);
            this.GroupFooter2.AfterPrint += new System.EventHandler(this.GroupFooter2_AfterPrint);
            // 
            // xrlStandPlotFactor
            // 
            this.xrlStandPlotFactor.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandPlotFactor.BorderWidth = 2F;
            this.xrlStandPlotFactor.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandPlotFactor.LocationFloat = new DevExpress.Utils.PointFloat(40F, 0F);
            this.xrlStandPlotFactor.Name = "xrlStandPlotFactor";
            this.xrlStandPlotFactor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandPlotFactor.SizeF = new System.Drawing.SizeF(34.99999F, 35F);
            this.xrlStandPlotFactor.StylePriority.UseBorders = false;
            this.xrlStandPlotFactor.StylePriority.UseBorderWidth = false;
            this.xrlStandPlotFactor.StylePriority.UseFont = false;
            this.xrlStandPlotFactor.StylePriority.UseTextAlignment = false;
            this.xrlStandPlotFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandAge
            // 
            this.xrlStandAge.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandAge.BorderWidth = 2F;
            this.xrlStandAge.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandAge.LocationFloat = new DevExpress.Utils.PointFloat(75.00002F, 0F);
            this.xrlStandAge.Name = "xrlStandAge";
            this.xrlStandAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandAge.SizeF = new System.Drawing.SizeF(34.99999F, 35F);
            this.xrlStandAge.StylePriority.UseBorders = false;
            this.xrlStandAge.StylePriority.UseBorderWidth = false;
            this.xrlStandAge.StylePriority.UseFont = false;
            this.xrlStandAge.StylePriority.UseTextAlignment = false;
            this.xrlStandAge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandSpecies
            // 
            this.xrlStandSpecies.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandSpecies.BorderWidth = 2F;
            this.xrlStandSpecies.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandSpecies.LocationFloat = new DevExpress.Utils.PointFloat(110F, 0F);
            this.xrlStandSpecies.Name = "xrlStandSpecies";
            this.xrlStandSpecies.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandSpecies.SizeF = new System.Drawing.SizeF(34.99999F, 35F);
            this.xrlStandSpecies.StylePriority.UseBorders = false;
            this.xrlStandSpecies.StylePriority.UseBorderWidth = false;
            this.xrlStandSpecies.StylePriority.UseFont = false;
            this.xrlStandSpecies.StylePriority.UseTextAlignment = false;
            this.xrlStandSpecies.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel33.BorderWidth = 2F;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(145F, 0F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(35F, 35F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseBorderWidth = false;
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandTreeMeasured
            // 
            this.xrlStandTreeMeasured.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTreeMeasured.BorderWidth = 2F;
            this.xrlStandTreeMeasured.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeMeasured")});
            this.xrlStandTreeMeasured.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTreeMeasured.LocationFloat = new DevExpress.Utils.PointFloat(180F, 0F);
            this.xrlStandTreeMeasured.Name = "xrlStandTreeMeasured";
            this.xrlStandTreeMeasured.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandTreeMeasured.SizeF = new System.Drawing.SizeF(34.99998F, 35F);
            this.xrlStandTreeMeasured.StylePriority.UseBorders = false;
            this.xrlStandTreeMeasured.StylePriority.UseBorderWidth = false;
            this.xrlStandTreeMeasured.StylePriority.UseFont = false;
            this.xrlStandTreeMeasured.StylePriority.UseTextAlignment = false;
            xrSummary10.FormatString = "{0:#}";
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlStandTreeMeasured.Summary = xrSummary10;
            this.xrlStandTreeMeasured.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandDbh
            // 
            this.xrlStandDbh.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandDbh.BorderWidth = 2F;
            this.xrlStandDbh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandDbh.LocationFloat = new DevExpress.Utils.PointFloat(250F, 0F);
            this.xrlStandDbh.Name = "xrlStandDbh";
            this.xrlStandDbh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandDbh.SizeF = new System.Drawing.SizeF(35F, 35F);
            this.xrlStandDbh.StylePriority.UseBorders = false;
            this.xrlStandDbh.StylePriority.UseBorderWidth = false;
            this.xrlStandDbh.StylePriority.UseFont = false;
            this.xrlStandDbh.StylePriority.UseTextAlignment = false;
            this.xrlStandDbh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandFF
            // 
            this.xrlStandFF.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandFF.BorderWidth = 2F;
            this.xrlStandFF.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandFF.LocationFloat = new DevExpress.Utils.PointFloat(285F, 0F);
            this.xrlStandFF.Name = "xrlStandFF";
            this.xrlStandFF.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandFF.SizeF = new System.Drawing.SizeF(35F, 35F);
            this.xrlStandFF.StylePriority.UseBorders = false;
            this.xrlStandFF.StylePriority.UseBorderWidth = false;
            this.xrlStandFF.StylePriority.UseFont = false;
            this.xrlStandFF.StylePriority.UseTextAlignment = false;
            this.xrlStandFF.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandTotHeight
            // 
            this.xrlStandTotHeight.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTotHeight.BorderWidth = 2F;
            this.xrlStandTotHeight.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTotHeight.LocationFloat = new DevExpress.Utils.PointFloat(320F, 0F);
            this.xrlStandTotHeight.Name = "xrlStandTotHeight";
            this.xrlStandTotHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandTotHeight.SizeF = new System.Drawing.SizeF(35.00003F, 35F);
            this.xrlStandTotHeight.StylePriority.UseBorders = false;
            this.xrlStandTotHeight.StylePriority.UseBorderWidth = false;
            this.xrlStandTotHeight.StylePriority.UseFont = false;
            this.xrlStandTotHeight.StylePriority.UseTextAlignment = false;
            this.xrlStandTotHeight.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandTreeCount
            // 
            this.xrlStandTreeCount.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTreeCount.BorderWidth = 2F;
            this.xrlStandTreeCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TreeCount")});
            this.xrlStandTreeCount.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTreeCount.LocationFloat = new DevExpress.Utils.PointFloat(215F, 0F);
            this.xrlStandTreeCount.Name = "xrlStandTreeCount";
            this.xrlStandTreeCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandTreeCount.SizeF = new System.Drawing.SizeF(35F, 35F);
            this.xrlStandTreeCount.StylePriority.UseBorders = false;
            this.xrlStandTreeCount.StylePriority.UseBorderWidth = false;
            this.xrlStandTreeCount.StylePriority.UseFont = false;
            this.xrlStandTreeCount.StylePriority.UseTextAlignment = false;
            xrSummary11.FormatString = "{0:#,#}";
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlStandTreeCount.Summary = xrSummary11;
            this.xrlStandTreeCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrStandNumber
            // 
            this.xrStandNumber.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrStandNumber.BorderWidth = 2F;
            this.xrStandNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "StandName")});
            this.xrStandNumber.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrStandNumber.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrStandNumber.Name = "xrStandNumber";
            this.xrStandNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrStandNumber.SizeF = new System.Drawing.SizeF(40F, 35F);
            this.xrStandNumber.StylePriority.UseBorders = false;
            this.xrStandNumber.StylePriority.UseBorderWidth = false;
            this.xrStandNumber.StylePriority.UseFont = false;
            // 
            // xrlStandTotMbf
            // 
            this.xrlStandTotMbf.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTotMbf.BorderWidth = 2F;
            this.xrlStandTotMbf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalMBF")});
            this.xrlStandTotMbf.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTotMbf.LocationFloat = new DevExpress.Utils.PointFloat(635.0001F, 0F);
            this.xrlStandTotMbf.Name = "xrlStandTotMbf";
            this.xrlStandTotMbf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 17, 0, 0, 100F);
            this.xrlStandTotMbf.SizeF = new System.Drawing.SizeF(64.99994F, 35F);
            this.xrlStandTotMbf.StylePriority.UseBorders = false;
            this.xrlStandTotMbf.StylePriority.UseBorderWidth = false;
            this.xrlStandTotMbf.StylePriority.UseFont = false;
            this.xrlStandTotMbf.StylePriority.UsePadding = false;
            this.xrlStandTotMbf.StylePriority.UseTextAlignment = false;
            xrSummary12.FormatString = "{0:n0}";
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlStandTotMbf.Summary = xrSummary12;
            this.xrlStandTotMbf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandTotCcf
            // 
            this.xrlStandTotCcf.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTotCcf.BorderWidth = 2F;
            this.xrlStandTotCcf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RptPlotTreeListVolume.TotalCCF")});
            this.xrlStandTotCcf.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTotCcf.LocationFloat = new DevExpress.Utils.PointFloat(585F, 0F);
            this.xrlStandTotCcf.Name = "xrlStandTotCcf";
            this.xrlStandTotCcf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandTotCcf.SizeF = new System.Drawing.SizeF(50F, 35F);
            this.xrlStandTotCcf.StylePriority.UseBorders = false;
            this.xrlStandTotCcf.StylePriority.UseBorderWidth = false;
            this.xrlStandTotCcf.StylePriority.UseFont = false;
            this.xrlStandTotCcf.StylePriority.UseTextAlignment = false;
            xrSummary13.FormatString = "{0:n0}";
            xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrlStandTotCcf.Summary = xrSummary13;
            this.xrlStandTotCcf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandNetMbpPa
            // 
            this.xrlStandNetMbpPa.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandNetMbpPa.BorderWidth = 2F;
            this.xrlStandNetMbpPa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandNetMbpPa.LocationFloat = new DevExpress.Utils.PointFloat(530F, 0F);
            this.xrlStandNetMbpPa.Name = "xrlStandNetMbpPa";
            this.xrlStandNetMbpPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandNetMbpPa.SizeF = new System.Drawing.SizeF(55F, 35F);
            this.xrlStandNetMbpPa.StylePriority.UseBorders = false;
            this.xrlStandNetMbpPa.StylePriority.UseBorderWidth = false;
            this.xrlStandNetMbpPa.StylePriority.UseFont = false;
            this.xrlStandNetMbpPa.StylePriority.UseTextAlignment = false;
            xrSummary14.FormatString = "{0:n0}";
            this.xrlStandNetMbpPa.Summary = xrSummary14;
            this.xrlStandNetMbpPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandNetCcfPa
            // 
            this.xrlStandNetCcfPa.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandNetCcfPa.BorderWidth = 2F;
            this.xrlStandNetCcfPa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandNetCcfPa.LocationFloat = new DevExpress.Utils.PointFloat(475.0002F, 0F);
            this.xrlStandNetCcfPa.Name = "xrlStandNetCcfPa";
            this.xrlStandNetCcfPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandNetCcfPa.SizeF = new System.Drawing.SizeF(54.99963F, 35F);
            this.xrlStandNetCcfPa.StylePriority.UseBorders = false;
            this.xrlStandNetCcfPa.StylePriority.UseBorderWidth = false;
            this.xrlStandNetCcfPa.StylePriority.UseFont = false;
            this.xrlStandNetCcfPa.StylePriority.UseTextAlignment = false;
            xrSummary15.FormatString = "{0:n2}";
            this.xrlStandNetCcfPa.Summary = xrSummary15;
            this.xrlStandNetCcfPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrlStandNetCcfPa.TextFormatString = "{0:#,#}";
            // 
            // xrlStandLogsPa
            // 
            this.xrlStandLogsPa.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandLogsPa.BorderWidth = 2F;
            this.xrlStandLogsPa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandLogsPa.LocationFloat = new DevExpress.Utils.PointFloat(435.0001F, 0F);
            this.xrlStandLogsPa.Name = "xrlStandLogsPa";
            this.xrlStandLogsPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandLogsPa.SizeF = new System.Drawing.SizeF(39.99997F, 35F);
            this.xrlStandLogsPa.StylePriority.UseBorders = false;
            this.xrlStandLogsPa.StylePriority.UseBorderWidth = false;
            this.xrlStandLogsPa.StylePriority.UseFont = false;
            this.xrlStandLogsPa.StylePriority.UseTextAlignment = false;
            xrSummary16.FormatString = "{0:n1}";
            this.xrlStandLogsPa.Summary = xrSummary16;
            this.xrlStandLogsPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrlStandTreesPa
            // 
            this.xrlStandTreesPa.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandTreesPa.BorderWidth = 2F;
            this.xrlStandTreesPa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandTreesPa.LocationFloat = new DevExpress.Utils.PointFloat(395F, 0F);
            this.xrlStandTreesPa.Name = "xrlStandTreesPa";
            this.xrlStandTreesPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandTreesPa.SizeF = new System.Drawing.SizeF(40.00009F, 35F);
            this.xrlStandTreesPa.StylePriority.UseBorders = false;
            this.xrlStandTreesPa.StylePriority.UseBorderWidth = false;
            this.xrlStandTreesPa.StylePriority.UseFont = false;
            this.xrlStandTreesPa.StylePriority.UseTextAlignment = false;
            xrSummary17.FormatString = "{0:n0}";
            this.xrlStandTreesPa.Summary = xrSummary17;
            this.xrlStandTreesPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrlStandTreesPa.TextFormatString = "{0:n2}";
            // 
            // xrlStandBaPa
            // 
            this.xrlStandBaPa.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrlStandBaPa.BorderWidth = 2F;
            this.xrlStandBaPa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrlStandBaPa.LocationFloat = new DevExpress.Utils.PointFloat(355F, 0F);
            this.xrlStandBaPa.Name = "xrlStandBaPa";
            this.xrlStandBaPa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlStandBaPa.SizeF = new System.Drawing.SizeF(39.99997F, 35F);
            this.xrlStandBaPa.StylePriority.UseBorders = false;
            this.xrlStandBaPa.StylePriority.UseBorderWidth = false;
            this.xrlStandBaPa.StylePriority.UseFont = false;
            this.xrlStandBaPa.StylePriority.UseTextAlignment = false;
            xrSummary18.FormatString = "{0:n1}";
            this.xrlStandBaPa.Summary = xrSummary18;
            this.xrlStandBaPa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // Acres
            // 
            this.Acres.Name = "Acres";
            this.Acres.Type = typeof(float);
            this.Acres.ValueInfo = "0";
            this.Acres.Visible = false;
            // 
            // Plots
            // 
            this.Plots.Name = "Plots";
            this.Plots.Type = typeof(float);
            this.Plots.ValueInfo = "0";
            this.Plots.Visible = false;
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionName = "Temp";
            this.sqlDataSource1.Name = "sqlDataSource1";
            columnExpression1.ColumnName = "Id";
            table1.MetaSerializable = "<Meta X=\"30\" Y=\"30\" Width=\"125\" Height=\"571\" />";
            table1.Name = "RptPlotTreeListVolume";
            columnExpression1.Table = table1;
            column1.Expression = columnExpression1;
            columnExpression2.ColumnName = "PlotID";
            columnExpression2.Table = table1;
            column2.Expression = columnExpression2;
            columnExpression3.ColumnName = "TreeStatus";
            columnExpression3.Table = table1;
            column3.Expression = columnExpression3;
            columnExpression4.ColumnName = "PlotFactor";
            columnExpression4.Table = table1;
            column4.Expression = columnExpression4;
            columnExpression5.ColumnName = "AgeCode";
            columnExpression5.Table = table1;
            column5.Expression = columnExpression5;
            columnExpression6.ColumnName = "Species";
            columnExpression6.Table = table1;
            column6.Expression = columnExpression6;
            columnExpression7.ColumnName = "TreeNumber";
            columnExpression7.Table = table1;
            column7.Expression = columnExpression7;
            columnExpression8.ColumnName = "Dbh";
            columnExpression8.Table = table1;
            column8.Expression = columnExpression8;
            columnExpression9.ColumnName = "TreeCount";
            columnExpression9.Table = table1;
            column9.Expression = columnExpression9;
            columnExpression10.ColumnName = "FormFactor";
            columnExpression10.Table = table1;
            column10.Expression = columnExpression10;
            columnExpression11.ColumnName = "TotalHeight";
            columnExpression11.Table = table1;
            column11.Expression = columnExpression11;
            columnExpression12.ColumnName = "StandsID";
            columnExpression12.Table = table1;
            column12.Expression = columnExpression12;
            columnExpression13.ColumnName = "PlotNumber";
            columnExpression13.Table = table1;
            column13.Expression = columnExpression13;
            columnExpression14.ColumnName = "TractName";
            columnExpression14.Table = table1;
            column14.Expression = columnExpression14;
            columnExpression15.ColumnName = "StandName";
            columnExpression15.Table = table1;
            column15.Expression = columnExpression15;
            columnExpression16.ColumnName = "CalcTotalHtUsedYN";
            columnExpression16.Table = table1;
            column16.Expression = columnExpression16;
            columnExpression17.ColumnName = "BasalArea";
            columnExpression17.Table = table1;
            column17.Expression = columnExpression17;
            columnExpression18.ColumnName = "TreesPerAcre";
            columnExpression18.Table = table1;
            column18.Expression = columnExpression18;
            columnExpression19.ColumnName = "LogsPerAcre";
            columnExpression19.Table = table1;
            column19.Expression = columnExpression19;
            columnExpression20.ColumnName = "NetCCFPerAcre";
            columnExpression20.Table = table1;
            column20.Expression = columnExpression20;
            columnExpression21.ColumnName = "NetMBFPerAcre";
            columnExpression21.Table = table1;
            column21.Expression = columnExpression21;
            columnExpression22.ColumnName = "TotalCCF";
            columnExpression22.Table = table1;
            column22.Expression = columnExpression22;
            columnExpression23.ColumnName = "TotalMBF";
            columnExpression23.Table = table1;
            column23.Expression = columnExpression23;
            columnExpression24.ColumnName = "StandsSelectedID";
            columnExpression24.Table = table1;
            column24.Expression = columnExpression24;
            columnExpression25.ColumnName = "TreeMeasured";
            columnExpression25.Table = table1;
            column25.Expression = columnExpression25;
            columnExpression26.ColumnName = "TreeType";
            columnExpression26.Table = table1;
            column26.Expression = columnExpression26;
            columnExpression27.ColumnName = "PlotFactorCode";
            columnExpression27.Table = table1;
            column27.Expression = columnExpression27;
            selectQuery1.Columns.Add(column1);
            selectQuery1.Columns.Add(column2);
            selectQuery1.Columns.Add(column3);
            selectQuery1.Columns.Add(column4);
            selectQuery1.Columns.Add(column5);
            selectQuery1.Columns.Add(column6);
            selectQuery1.Columns.Add(column7);
            selectQuery1.Columns.Add(column8);
            selectQuery1.Columns.Add(column9);
            selectQuery1.Columns.Add(column10);
            selectQuery1.Columns.Add(column11);
            selectQuery1.Columns.Add(column12);
            selectQuery1.Columns.Add(column13);
            selectQuery1.Columns.Add(column14);
            selectQuery1.Columns.Add(column15);
            selectQuery1.Columns.Add(column16);
            selectQuery1.Columns.Add(column17);
            selectQuery1.Columns.Add(column18);
            selectQuery1.Columns.Add(column19);
            selectQuery1.Columns.Add(column20);
            selectQuery1.Columns.Add(column21);
            selectQuery1.Columns.Add(column22);
            selectQuery1.Columns.Add(column23);
            selectQuery1.Columns.Add(column24);
            selectQuery1.Columns.Add(column25);
            selectQuery1.Columns.Add(column26);
            selectQuery1.Columns.Add(column27);
            selectQuery1.Name = "RptPlotTreeListVolume";
            selectQuery1.Tables.Add(table1);
            this.sqlDataSource1.Queries.AddRange(new DevExpress.DataAccess.Sql.SqlQuery[] {
            selectQuery1});
            this.sqlDataSource1.ResultSchemaSerializable = resources.GetString("sqlDataSource1.ResultSchemaSerializable");
            // 
            // DominantFeature
            // 
            this.DominantFeature.Description = "Which column to use as chooser";
            this.DominantFeature.Name = "DominantFeature";
            this.DominantFeature.Visible = false;
            // 
            // PlotListVolumeSubReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader2,
            this.GroupFooter2});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.sqlDataSource1});
            this.DataMember = "RptPlotTreeListVolume";
            this.DataSource = this.sqlDataSource1;
            this.FilterString = "[StandsSelectedId] = ?StandsSelectedId";
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.StandsSelectedId,
            this.Acres,
            this.Plots,
            this.DominantFeature});
            this.Version = "17.2";
            this.DataSourceDemanded += new System.EventHandler<System.EventArgs>(this.PlotTreeListVolumeSubReport_DataSourceDemanded);
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PlotListVolumeSubReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotNumber;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotFactor;
        private DevExpress.XtraReports.UI.XRLabel xrlAge;
        private DevExpress.XtraReports.UI.XRLabel xrlSpecies;
        private DevExpress.XtraReports.UI.XRLabel xrlTreeStatus;
        private DevExpress.XtraReports.UI.XRLabel xrlTreeCount;
        private DevExpress.XtraReports.UI.XRLabel xrlDbh;
        private DevExpress.XtraReports.UI.XRLabel xrlFF;
        private DevExpress.XtraReports.UI.XRLabel xrlTotHeight;
        private DevExpress.XtraReports.UI.XRLabel xrlBaPa;
        private DevExpress.XtraReports.UI.XRLabel xrlTreesPa;
        private DevExpress.XtraReports.UI.XRLabel xrlLogsPa;
        private DevExpress.XtraReports.UI.XRLabel xrlNetCcfPa;
        private DevExpress.XtraReports.UI.XRLabel xrlNetMbfPa;
        private DevExpress.XtraReports.UI.XRLabel xrlTotCcf;
        private DevExpress.XtraReports.UI.XRLabel xrlTotMbf;
        private DevExpress.XtraReports.Parameters.Parameter StandsSelectedId;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotBaPa;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotNetCcfPa;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotLogsPa;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotTreesPa;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotMbpPa;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotTotCcf;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotTotMbf;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRLabel xrStandNumber;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTotMbf;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTotCcf;
        private DevExpress.XtraReports.UI.XRLabel xrlStandNetMbpPa;
        private DevExpress.XtraReports.UI.XRLabel xrlStandNetCcfPa;
        private DevExpress.XtraReports.UI.XRLabel xrlStandLogsPa;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTreesPa;
        private DevExpress.XtraReports.UI.XRLabel xrlStandBaPa;
        private DevExpress.XtraReports.Parameters.Parameter Acres;
        private DevExpress.XtraReports.Parameters.Parameter Plots;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotPlotNumber;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotTreeCount;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTreeCount;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotDbh;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotFF;
        private DevExpress.XtraReports.UI.XRLabel xrlPltTotHeight;
        private DevExpress.XtraReports.UI.XRLabel xrlStandDbh;
        private DevExpress.XtraReports.UI.XRLabel xrlStandFF;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTotHeight;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotTreeMeasured;
        private DevExpress.XtraReports.UI.XRLabel xrlTreeMeasured;
        private DevExpress.XtraReports.UI.XRLabel xrlStandTreeMeasured;
        private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
        private DevExpress.XtraReports.UI.XRLabel xrBlank0;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotSpecies;
        private DevExpress.XtraReports.UI.XRLabel xrlStandSpecies;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotAge;
        private DevExpress.XtraReports.UI.XRLabel xrlPlotPlotFactor;
        private DevExpress.XtraReports.UI.XRLabel xrlStandPlotFactor;
        private DevExpress.XtraReports.UI.XRLabel xrlStandAge;
        private DevExpress.XtraReports.Parameters.Parameter DominantFeature;
    }
}
