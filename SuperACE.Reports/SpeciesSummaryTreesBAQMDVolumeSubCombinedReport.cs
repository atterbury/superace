﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryTreesBAQMDVolumeSubCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        float totalTons = 0;
        float totalLbsCcf = 0;
        float totalNetMbf = 0;
        float totalNetCcf = 0;
        float totalBA = 0;
        float totalTPA = 0;
        float totalTotalHeight = 0;

        public SpeciesSummaryTreesBAQMDVolumeSubCombinedReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //= SUM(Fields.TotalTons) / SUM(Fields.TotalTotalNetMbf)
            totalTotalHeight += (Utils.ConvertToFloat(this.GetCurrentColumnValue("AvgTreeHeight").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesAcre").ToString()));
            totalLbsCcf += (Utils.ConvertToFloat(this.GetCurrentColumnValue("LbsCcf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetCunits").ToString()));
            totalTPA += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesAcre").ToString());
            totalBA += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalArea").ToString());
            totalNetMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString());
            totalNetCcf += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetCunits").ToString());
            totalTons += (Utils.ConvertToFloat(this.GetCurrentColumnValue("TonsMbf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString()));
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            float totalTonsMbf = totalTons / totalNetMbf;
            float totalQMDbh = (float)Math.Sqrt(totalBA / (totalTPA * 0.005454154));
            xrLabel18.Text = string.Format("{0:0.00}", totalTonsMbf);
            xrLabel19.Text = string.Format("{0:0.0}", totalQMDbh);
            xrLabel20.Text = string.Format("{0:0.0}", totalTotalHeight / totalTPA);
            xrLabel21.Text = string.Format("{0:0,000}", totalLbsCcf / totalNetCcf);
        }

        private void SpeciesSummaryTreesBAQMDVolumeSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptSpeciesSummaryLogVolume();
        }
    }
}
