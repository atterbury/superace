﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class AgeClassDistributionMbfByWoodTypeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        float speciesGross = 0;
        float speciesNet = 0;
        float standGross = 0;
        float standNet = 0;

        public AgeClassDistributionMbfByWoodTypeSubReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ////= SUM(Fields.TotalTons) / SUM(Fields.TotalTotalNetMbf)
            speciesGross += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalGrossMbf").ToString());
            speciesNet += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString());
            standGross += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalGrossMbf").ToString());
            standNet += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString());
            //totalTons += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalTons").ToString());
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel35.Text = string.Format("{0:#.#}", ((standGross - standNet) / standGross) * 100);
        }

        private void LogStockSortGradeMbfSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptLogStockSortGradeMbf(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel20.Text = string.Format("{0:#.#}", ((speciesGross - speciesNet) / speciesGross) * 100);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            speciesGross = 0;
            speciesNet = 0;
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            standGross = 0;
            standNet = 0;
        }
    }
}
