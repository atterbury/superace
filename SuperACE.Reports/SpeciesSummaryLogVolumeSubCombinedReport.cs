﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

using SuperACEUtils;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryLogVolumeSubCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        float totalTons = 0;
        float totalLbsCcf = 0;
        float totalNetMbf = 0;
        float totalNetCcf = 0;
        float totalBA = 0;
        float totalTPA = 0;

        public SpeciesSummaryLogVolumeSubCombinedReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //= SUM(Fields.TotalTons) / SUM(Fields.TotalTotalNetMbf)
            //totalNetMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString());
            //totalTons += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalTons").ToString());
            totalLbsCcf += (Utils.ConvertToFloat(this.GetCurrentColumnValue("LbsCcf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetCunits").ToString()));
            totalTPA += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesAcre").ToString());
            totalBA += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalArea").ToString());
            totalNetMbf += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString());
            totalNetCcf += Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetCunits").ToString());
            totalTons += (Utils.ConvertToFloat(this.GetCurrentColumnValue("TonsMbf").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TotalNetMbf").ToString()));
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //float totalTonsMbf = totalTons / totalNetMbf;
            //xrLabel18.Text = string.Format("{0:0.00}", totalTonsMbf);
            float totalTonsMbf = totalTons / totalNetMbf;
            xrLabel8.Text = string.Format("{0:0.00}", totalTonsMbf);
            xrLabel9.Text = string.Format("{0:n0}", totalLbsCcf / totalNetCcf);
        }

        private void SpeciesSummaryLogVolumeSubReport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }

        private void SpeciesSummaryLogVolumeSubReport1_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.DataSource = TempBLL.GetRptSpeciesSummaryLogVolume();
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            totalTons = 0;
            totalLbsCcf = 0;
            totalNetMbf = 0;
            totalNetCcf = 0;
            totalBA = 0;
            totalTPA = 0;
        }
    }
}
