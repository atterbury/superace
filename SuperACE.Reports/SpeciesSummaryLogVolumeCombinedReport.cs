﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryLogVolumeCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        private string project = string.Empty;
        private float totalAcres = 0;
        private float totalPlots = 0;
        private float totalTrees = 0;

        public SpeciesSummaryLogVolumeCombinedReport(string pProject, float pAcres, float pPlots, float pTrees)
        {
            InitializeComponent();

            project = project;
            totalAcres = pAcres;
            totalPlots = pPlots;
            totalTrees = pTrees;

            //objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel35.Text = project;
            xrLabel32.Text = totalAcres.ToString();
            xrLabel48.Text = totalTrees.ToString();
            xrLabel51.Text = totalPlots.ToString();
        }
    }
}
