﻿namespace SuperACE.Reports
{
    partial class PlotTreeListVolumeSubReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.StandsSelectedId = new DevExpress.XtraReports.Parameters.Parameter();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumBasalPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumNetCcfPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumLogsPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumTreesPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumNetMbfPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumTotalCcf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPlotSumTotalMbf = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTractSumNetMbfPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTractSumTreesPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTractSumLogsPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTractSumNetCcfPA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTractSumBasalPA = new DevExpress.XtraReports.UI.XRLabel();
            this.Acres = new DevExpress.XtraReports.Parameters.Parameter();
            this.Plots = new DevExpress.XtraReports.Parameters.Parameter();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18,
            this.xrLabel96,
            this.xrLabel95,
            this.xrLabel94,
            this.xrLabel93,
            this.xrLabel92,
            this.xrLabel91,
            this.xrLabel90,
            this.xrLabel89,
            this.xrLabel87,
            this.xrLabel84,
            this.xrLabel75,
            this.xrLabel74,
            this.xrLabel73,
            this.xrLabel69,
            this.xrLabel68,
            this.xrLabel67,
            this.xrLabel66});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detail.HeightF = 15F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrLabel18
            // 
            this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeMeasured", "{0:#}")});
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(195F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel96
            // 
            this.xrLabel96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PlotNumber")});
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel96.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel96_BeforePrint);
            // 
            // xrLabel95
            // 
            this.xrLabel95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeNumber")});
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(35F, 0F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(25F, 15F);
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel95.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel94
            // 
            this.xrLabel94.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PlotFactor")});
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(60F, 0F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(30F, 15F);
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel94.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel93
            // 
            this.xrLabel93.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AgeCode")});
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(90F, 0F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel93.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel92
            // 
            this.xrLabel92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Species")});
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(125F, 0F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel92.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel91
            // 
            this.xrLabel91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeStatus")});
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(160F, 0F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel91.StylePriority.UseTextAlignment = false;
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel91.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel90
            // 
            this.xrLabel90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeCount", "{0:#}")});
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(230F, 0F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel90.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel89
            // 
            this.xrLabel89.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Dbh", "{0:n1}")});
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(265F, 0F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel89.StylePriority.UseTextAlignment = false;
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel89.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel87
            // 
            this.xrLabel87.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FormFactor", "{0:n0}")});
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(300F, 0F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel87.StylePriority.UseTextAlignment = false;
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel87.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel84
            // 
            this.xrLabel84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalHeight", "{0:n0}")});
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(335F, 0F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel84.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel84_BeforePrint);
            // 
            // xrLabel75
            // 
            this.xrLabel75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BasalArea", "{0:n1}")});
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(370F, 0F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(35F, 15F);
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel75.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel74
            // 
            this.xrLabel74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreesPerAcre", "{0:n2}")});
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(405F, 0F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel74.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel73
            // 
            this.xrLabel73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LogsPerAcre", "{0:n1}")});
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(445F, 0F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(40F, 15F);
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel73.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel69
            // 
            this.xrLabel69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NetCcfPerAcre", "{0:#,#}")});
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(485F, 0F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(50F, 15F);
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel69.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel68
            // 
            this.xrLabel68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NetMbfPerAcre", "{0:#,#}")});
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(535F, 0F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(50F, 15F);
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel68.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel67
            // 
            this.xrLabel67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalCcf", "{0:n0}")});
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(585F, 0F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(50F, 15F);
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel67.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // xrLabel66
            // 
            this.xrLabel66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalMbf", "{0:n0}")});
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(635F, 0F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 12, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(65F, 15F);
            this.xrLabel66.StylePriority.UsePadding = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel66.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel95_BeforePrint);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // StandsSelectedId
            // 
            this.StandsSelectedId.Name = "StandsSelectedId";
            this.StandsSelectedId.Type = typeof(long);
            this.StandsSelectedId.ValueInfo = "0";
            this.StandsSelectedId.Visible = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("PlotNumber", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Visible = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel17,
            this.xrPlotSumBasalPA,
            this.xrPlotSumNetCcfPA,
            this.xrPlotSumLogsPA,
            this.xrPlotSumTreesPA,
            this.xrPlotSumNetMbfPA,
            this.xrPlotSumTotalCcf,
            this.xrPlotSumTotalMbf,
            this.xrLabel1});
            this.GroupFooter1.HeightF = 25F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter1_BeforePrint);
            this.GroupFooter1.AfterPrint += new System.EventHandler(this.GroupFooter1_AfterPrint);
            // 
            // xrLabel20
            // 
            this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeMeasured")});
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(195F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(34.99998F, 25F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel20.Summary = xrSummary1;
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel19
            // 
            this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeCount")});
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(230F, 0F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(35.00002F, 25F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:#}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel19.Summary = xrSummary2;
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel17
            // 
            this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PlotNumber")});
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(35F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(90F, 25F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPlotSumBasalPA
            // 
            this.xrPlotSumBasalPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumBasalPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumBasalPA.LocationFloat = new DevExpress.Utils.PointFloat(370F, 0F);
            this.xrPlotSumBasalPA.Name = "xrPlotSumBasalPA";
            this.xrPlotSumBasalPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumBasalPA.SizeF = new System.Drawing.SizeF(35F, 25F);
            this.xrPlotSumBasalPA.StylePriority.UseBorders = false;
            this.xrPlotSumBasalPA.StylePriority.UseFont = false;
            this.xrPlotSumBasalPA.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n1}";
            this.xrPlotSumBasalPA.Summary = xrSummary3;
            this.xrPlotSumBasalPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumNetCcfPA
            // 
            this.xrPlotSumNetCcfPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumNetCcfPA.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NetCcfPerAcre")});
            this.xrPlotSumNetCcfPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumNetCcfPA.LocationFloat = new DevExpress.Utils.PointFloat(485.0001F, 0F);
            this.xrPlotSumNetCcfPA.Name = "xrPlotSumNetCcfPA";
            this.xrPlotSumNetCcfPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumNetCcfPA.SizeF = new System.Drawing.SizeF(49.99994F, 25F);
            this.xrPlotSumNetCcfPA.StylePriority.UseBorders = false;
            this.xrPlotSumNetCcfPA.StylePriority.UseFont = false;
            this.xrPlotSumNetCcfPA.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:#,#}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrPlotSumNetCcfPA.Summary = xrSummary4;
            this.xrPlotSumNetCcfPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumLogsPA
            // 
            this.xrPlotSumLogsPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumLogsPA.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LogsPerAcre")});
            this.xrPlotSumLogsPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumLogsPA.LocationFloat = new DevExpress.Utils.PointFloat(445F, 0F);
            this.xrPlotSumLogsPA.Name = "xrPlotSumLogsPA";
            this.xrPlotSumLogsPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumLogsPA.SizeF = new System.Drawing.SizeF(40F, 25F);
            this.xrPlotSumLogsPA.StylePriority.UseBorders = false;
            this.xrPlotSumLogsPA.StylePriority.UseFont = false;
            this.xrPlotSumLogsPA.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n1}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrPlotSumLogsPA.Summary = xrSummary5;
            this.xrPlotSumLogsPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumTreesPA
            // 
            this.xrPlotSumTreesPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumTreesPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumTreesPA.LocationFloat = new DevExpress.Utils.PointFloat(405F, 0F);
            this.xrPlotSumTreesPA.Name = "xrPlotSumTreesPA";
            this.xrPlotSumTreesPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumTreesPA.SizeF = new System.Drawing.SizeF(40.00003F, 25F);
            this.xrPlotSumTreesPA.StylePriority.UseBorders = false;
            this.xrPlotSumTreesPA.StylePriority.UseFont = false;
            this.xrPlotSumTreesPA.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            this.xrPlotSumTreesPA.Summary = xrSummary6;
            this.xrPlotSumTreesPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumNetMbfPA
            // 
            this.xrPlotSumNetMbfPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumNetMbfPA.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NetMbfPerAcre")});
            this.xrPlotSumNetMbfPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumNetMbfPA.LocationFloat = new DevExpress.Utils.PointFloat(535F, 0F);
            this.xrPlotSumNetMbfPA.Name = "xrPlotSumNetMbfPA";
            this.xrPlotSumNetMbfPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumNetMbfPA.SizeF = new System.Drawing.SizeF(49.99994F, 25F);
            this.xrPlotSumNetMbfPA.StylePriority.UseBorders = false;
            this.xrPlotSumNetMbfPA.StylePriority.UseFont = false;
            this.xrPlotSumNetMbfPA.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:#,#}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrPlotSumNetMbfPA.Summary = xrSummary7;
            this.xrPlotSumNetMbfPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumTotalCcf
            // 
            this.xrPlotSumTotalCcf.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumTotalCcf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalCcf")});
            this.xrPlotSumTotalCcf.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumTotalCcf.LocationFloat = new DevExpress.Utils.PointFloat(585F, 0F);
            this.xrPlotSumTotalCcf.Name = "xrPlotSumTotalCcf";
            this.xrPlotSumTotalCcf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPlotSumTotalCcf.SizeF = new System.Drawing.SizeF(50F, 25F);
            this.xrPlotSumTotalCcf.StylePriority.UseBorders = false;
            this.xrPlotSumTotalCcf.StylePriority.UseFont = false;
            this.xrPlotSumTotalCcf.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:n0}";
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrPlotSumTotalCcf.Summary = xrSummary8;
            this.xrPlotSumTotalCcf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPlotSumTotalMbf
            // 
            this.xrPlotSumTotalMbf.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrPlotSumTotalMbf.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalMbf")});
            this.xrPlotSumTotalMbf.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrPlotSumTotalMbf.LocationFloat = new DevExpress.Utils.PointFloat(635F, 0F);
            this.xrPlotSumTotalMbf.Name = "xrPlotSumTotalMbf";
            this.xrPlotSumTotalMbf.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 12, 0, 0, 100F);
            this.xrPlotSumTotalMbf.SizeF = new System.Drawing.SizeF(65F, 25F);
            this.xrPlotSumTotalMbf.StylePriority.UseBorders = false;
            this.xrPlotSumTotalMbf.StylePriority.UseFont = false;
            this.xrPlotSumTotalMbf.StylePriority.UsePadding = false;
            this.xrPlotSumTotalMbf.StylePriority.UseTextAlignment = false;
            xrSummary9.FormatString = "{0:n0}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrPlotSumTotalMbf.Summary = xrSummary9;
            this.xrPlotSumTotalMbf.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(35F, 25F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Plot";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Visible = false;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel11,
            this.xrTractSumNetMbfPA,
            this.xrTractSumTreesPA,
            this.xrTractSumLogsPA,
            this.xrTractSumNetCcfPA,
            this.xrTractSumBasalPA});
            this.GroupFooter2.HeightF = 35F;
            this.GroupFooter2.Level = 1;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupFooter2_BeforePrint);
            this.GroupFooter2.AfterPrint += new System.EventHandler(this.GroupFooter2_AfterPrint);
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel24.BorderWidth = 2F;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(265F, 0F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(105F, 34.99997F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel23.BorderWidth = 2F;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(89.99999F, 0F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(105F, 34.99997F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel21.BorderWidth = 2F;
            this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeCount")});
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(230F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(35.00005F, 34.99997F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseBorderWidth = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            xrSummary10.FormatString = "{0:#}";
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel21.Summary = xrSummary10;
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel22.BorderWidth = 2F;
            this.xrLabel22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TreeMeasured")});
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(195F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(34.99998F, 34.99997F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseBorderWidth = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            xrSummary11.FormatString = "{0:#}";
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel22.Summary = xrSummary11;
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel9.BorderWidth = 2F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(90F, 35F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Tract/Stand";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel10.BorderWidth = 2F;
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalMbf")});
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(635F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 12, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(65F, 34.99997F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            xrSummary12.FormatString = "{0:n0}";
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel10.Summary = xrSummary12;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel11.BorderWidth = 2F;
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalCcf")});
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(585F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(50F, 34.99997F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseBorderWidth = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            xrSummary13.FormatString = "{0:n0}";
            xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel11.Summary = xrSummary13;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTractSumNetMbfPA
            // 
            this.xrTractSumNetMbfPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTractSumNetMbfPA.BorderWidth = 2F;
            this.xrTractSumNetMbfPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTractSumNetMbfPA.LocationFloat = new DevExpress.Utils.PointFloat(535F, 0F);
            this.xrTractSumNetMbfPA.Name = "xrTractSumNetMbfPA";
            this.xrTractSumNetMbfPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTractSumNetMbfPA.SizeF = new System.Drawing.SizeF(50F, 34.99997F);
            this.xrTractSumNetMbfPA.StylePriority.UseBorders = false;
            this.xrTractSumNetMbfPA.StylePriority.UseBorderWidth = false;
            this.xrTractSumNetMbfPA.StylePriority.UseFont = false;
            this.xrTractSumNetMbfPA.StylePriority.UseTextAlignment = false;
            xrSummary14.FormatString = "{0:n0}";
            this.xrTractSumNetMbfPA.Summary = xrSummary14;
            this.xrTractSumNetMbfPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTractSumTreesPA
            // 
            this.xrTractSumTreesPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTractSumTreesPA.BorderWidth = 2F;
            this.xrTractSumTreesPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTractSumTreesPA.LocationFloat = new DevExpress.Utils.PointFloat(405F, 0F);
            this.xrTractSumTreesPA.Name = "xrTractSumTreesPA";
            this.xrTractSumTreesPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTractSumTreesPA.SizeF = new System.Drawing.SizeF(40.00003F, 34.99997F);
            this.xrTractSumTreesPA.StylePriority.UseBorders = false;
            this.xrTractSumTreesPA.StylePriority.UseBorderWidth = false;
            this.xrTractSumTreesPA.StylePriority.UseFont = false;
            this.xrTractSumTreesPA.StylePriority.UseTextAlignment = false;
            xrSummary15.FormatString = "{0:n2}";
            this.xrTractSumTreesPA.Summary = xrSummary15;
            this.xrTractSumTreesPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTractSumLogsPA
            // 
            this.xrTractSumLogsPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTractSumLogsPA.BorderWidth = 2F;
            this.xrTractSumLogsPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTractSumLogsPA.LocationFloat = new DevExpress.Utils.PointFloat(445F, 0F);
            this.xrTractSumLogsPA.Name = "xrTractSumLogsPA";
            this.xrTractSumLogsPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTractSumLogsPA.SizeF = new System.Drawing.SizeF(40.00003F, 34.99997F);
            this.xrTractSumLogsPA.StylePriority.UseBorders = false;
            this.xrTractSumLogsPA.StylePriority.UseBorderWidth = false;
            this.xrTractSumLogsPA.StylePriority.UseFont = false;
            this.xrTractSumLogsPA.StylePriority.UseTextAlignment = false;
            xrSummary16.FormatString = "{0:n1}";
            this.xrTractSumLogsPA.Summary = xrSummary16;
            this.xrTractSumLogsPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTractSumNetCcfPA
            // 
            this.xrTractSumNetCcfPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTractSumNetCcfPA.BorderWidth = 2F;
            this.xrTractSumNetCcfPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTractSumNetCcfPA.LocationFloat = new DevExpress.Utils.PointFloat(485.0001F, 0F);
            this.xrTractSumNetCcfPA.Name = "xrTractSumNetCcfPA";
            this.xrTractSumNetCcfPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTractSumNetCcfPA.SizeF = new System.Drawing.SizeF(49.99994F, 34.99997F);
            this.xrTractSumNetCcfPA.StylePriority.UseBorders = false;
            this.xrTractSumNetCcfPA.StylePriority.UseBorderWidth = false;
            this.xrTractSumNetCcfPA.StylePriority.UseFont = false;
            this.xrTractSumNetCcfPA.StylePriority.UseTextAlignment = false;
            xrSummary17.FormatString = "{0:n0}";
            this.xrTractSumNetCcfPA.Summary = xrSummary17;
            this.xrTractSumNetCcfPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTractSumBasalPA
            // 
            this.xrTractSumBasalPA.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTractSumBasalPA.BorderWidth = 2F;
            this.xrTractSumBasalPA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTractSumBasalPA.LocationFloat = new DevExpress.Utils.PointFloat(370F, 0F);
            this.xrTractSumBasalPA.Name = "xrTractSumBasalPA";
            this.xrTractSumBasalPA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTractSumBasalPA.SizeF = new System.Drawing.SizeF(35F, 34.99997F);
            this.xrTractSumBasalPA.StylePriority.UseBorders = false;
            this.xrTractSumBasalPA.StylePriority.UseBorderWidth = false;
            this.xrTractSumBasalPA.StylePriority.UseFont = false;
            this.xrTractSumBasalPA.StylePriority.UseTextAlignment = false;
            xrSummary18.FormatString = "{0:n1}";
            this.xrTractSumBasalPA.Summary = xrSummary18;
            this.xrTractSumBasalPA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // Acres
            // 
            this.Acres.Name = "Acres";
            this.Acres.Type = typeof(float);
            this.Acres.ValueInfo = "0";
            this.Acres.Visible = false;
            // 
            // Plots
            // 
            this.Plots.Name = "Plots";
            this.Plots.Type = typeof(float);
            this.Plots.ValueInfo = "0";
            this.Plots.Visible = false;
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.Constructor = objectConstructorInfo1;
            this.objectDataSource1.DataSource = typeof(Temp.DAL.RptPlotTreeListVolume);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // PlotTreeListVolumeSubReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader2,
            this.GroupFooter2});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.FilterString = "[StandsSelectedId] = [StandsSelectedId]";
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.StandsSelectedId,
            this.Acres,
            this.Plots});
            this.Version = "17.2";
            this.DataSourceDemanded += new System.EventHandler<System.EventArgs>(this.PlotTreeListVolumeSubReport_DataSourceDemanded);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.Parameters.Parameter StandsSelectedId;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumBasalPA;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumNetCcfPA;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumLogsPA;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumTreesPA;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumNetMbfPA;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumTotalCcf;
        private DevExpress.XtraReports.UI.XRLabel xrPlotSumTotalMbf;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrTractSumNetMbfPA;
        private DevExpress.XtraReports.UI.XRLabel xrTractSumTreesPA;
        private DevExpress.XtraReports.UI.XRLabel xrTractSumLogsPA;
        private DevExpress.XtraReports.UI.XRLabel xrTractSumNetCcfPA;
        private DevExpress.XtraReports.UI.XRLabel xrTractSumBasalPA;
        private DevExpress.XtraReports.Parameters.Parameter Acres;
        private DevExpress.XtraReports.Parameters.Parameter Plots;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
    }
}
