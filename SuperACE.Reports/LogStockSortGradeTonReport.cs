﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperACE.Reports
{
    public class LogStockSortGradeTonReport : LogStockSortGradeReport
    {
        public LogStockSortGradeTonReport() : base("Ton") { }
    }
}
