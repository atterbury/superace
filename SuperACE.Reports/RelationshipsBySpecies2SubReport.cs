﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace SuperACE.Reports
{
    public partial class RelationshipsBySpecies2SubReport : DevExpress.XtraReports.UI.XtraReport
    {
        private float tpaTotal = 0;
        private float tpaDF = 0;
        private float tpaWH = 0;
        private float tpaRC = 0;
        private float tpaGF = 0;
        private float tpaRA = 0;
        private float tpaOC = 0;
        private float tpaOH = 0;

        private float baTotal = 0;
        private float baDF = 0;
        private float baWH = 0;
        private float baRC = 0;
        private float baGF = 0;
        private float baRA = 0;
        private float baOC = 0;
        private float baOH = 0;

        public RelationshipsBySpecies2SubReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string item = GetCurrentColumnValue("Item").ToString();
            if (item == "Basal Area per Acre Sq Ft")
            {
                xrLabel1.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                baTotal = Convert.ToSingle(GetCurrentColumnValue("Totals").ToString());
                xrLabel3.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                baDF = Convert.ToSingle(GetCurrentColumnValue("DF").ToString());
                xrLabel4.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                baWH = Convert.ToSingle(GetCurrentColumnValue("WH").ToString());
                xrLabel5.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                baRC = Convert.ToSingle(GetCurrentColumnValue("RC").ToString());
                xrLabel6.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                baGF = Convert.ToSingle(GetCurrentColumnValue("GF").ToString());
                xrLabel7.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                baRA = Convert.ToSingle(GetCurrentColumnValue("RA").ToString());
                xrLabel17.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                baOC = Convert.ToSingle(GetCurrentColumnValue("OC").ToString());
                xrLabel18.Text = string.Format("{0:#.#}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                baOH = Convert.ToSingle(GetCurrentColumnValue("OH").ToString());
            }
            if (item == "Trees per Acre")
            {
                xrLabel1.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                tpaTotal = Convert.ToSingle(GetCurrentColumnValue("Totals").ToString());
                xrLabel3.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                tpaDF = Convert.ToSingle(GetCurrentColumnValue("DF").ToString());
                xrLabel4.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                tpaWH = Convert.ToSingle(GetCurrentColumnValue("WH").ToString());
                xrLabel5.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                tpaRC = Convert.ToSingle(GetCurrentColumnValue("RC").ToString());
                xrLabel6.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                tpaGF = Convert.ToSingle(GetCurrentColumnValue("GF").ToString());
                xrLabel7.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                tpaRA = Convert.ToSingle(GetCurrentColumnValue("RA").ToString());
                xrLabel17.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                tpaOC = Convert.ToSingle(GetCurrentColumnValue("OC").ToString());
                xrLabel18.Text = string.Format("{0:#.##}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                tpaOH = Convert.ToSingle(GetCurrentColumnValue("OH").ToString());
            }
            if (item == "% Trees per Acre")
            {
                xrLabel1.Text = string.Format("{0:0.0}", 100.0);
                if (tpaDF > 0)
                    xrLabel3.Text = string.Format("{0:0.0}", Math.Round((double)tpaDF, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (tpaWH > 0)
                    xrLabel4.Text = string.Format("{0:0.0}", Math.Round((double)tpaWH, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel14.Text = string.Empty;
                }
                if (tpaRC > 0)
                    xrLabel5.Text = string.Format("{0:0.0}", Math.Round((double)tpaRC, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (tpaGF > 0)
                    xrLabel6.Text = string.Format("{0:0.0}", Math.Round((double)tpaGF, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (tpaRA > 0)
                    xrLabel7.Text = string.Format("{0:0.0}", Math.Round((double)tpaRA, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (tpaOC > 0)
                    xrLabel17.Text = string.Format("{0:0.0}", Math.Round((double)tpaOC, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel17.Text = string.Empty;
                }
                if (tpaOH > 0)
                    xrLabel18.Text = string.Format("{0:0.0}", Math.Round((double)tpaOH, 2) / Math.Round((double)tpaTotal, 2) * 100);
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
            if (item == "QM DBH")
            {
                if (baTotal > 0 && tpaTotal > 0)
                    xrLabel1.Text = string.Format("{0:0.0}", Math.Sqrt(baTotal/(tpaTotal * 0.005454154)));
                else
                {
                    xrLabel1.Text = string.Empty;
                }
                if (baDF > 0 && tpaDF > 0)
                    xrLabel3.Text = string.Format("{0:0.0}", Math.Sqrt(baDF / (tpaDF * 0.005454154)));
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (baWH > 0 && tpaWH > 0)
                    xrLabel4.Text = string.Format("{0:0.0}", Math.Sqrt(baWH / (tpaWH * 0.005454154)));
                else
                {
                    xrLabel4.Text = string.Empty;
                }
                if (baRC > 0 && tpaRC > 0)
                    xrLabel5.Text = string.Format("{0:0.0}", Math.Sqrt(baRC / (tpaRC * 0.005454154)));
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (baGF > 0 && tpaGF > 0)
                    xrLabel6.Text = string.Format("{0:0.0}", Math.Sqrt(baGF / (tpaGF * 0.005454154)));
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (baRA > 0 && tpaRA > 0)
                    xrLabel7.Text = string.Format("{0:0.0}", Math.Sqrt(baRA / (tpaRA * 0.005454154)));
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (baOC > 0 && tpaOC > 0)
                    xrLabel17.Text = string.Format("{0:0.0}", Math.Sqrt(baOC / (tpaOC * 0.005454154)));
                else
                {
                    xrLabel17.Text = string.Empty;
                }
                if (baOH > 0 && tpaOH > 0)
                    xrLabel18.Text = string.Format("{0:0.0}", Math.Sqrt(baOH / (tpaOH * 0.005454154)));
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
            if (item == "Avg FF 16 Ft")
            {
                xrLabel1.Text = string.Format("{0:#.#}", 0);
                xrLabel3.Text = string.Format("{0:#,#}", 0);
                xrLabel4.Text = string.Format("{0:#,#}", 0);
                xrLabel5.Text = string.Format("{0:#,#}", 0);
                xrLabel6.Text = string.Format("{0:#,#}", 0);
                xrLabel7.Text = string.Format("{0:#,#}", 0);
                xrLabel17.Text = string.Format("{0:#,#}", 0);
                xrLabel18.Text = string.Format("{0:#,#}", 0);
            }
            if (item == "Avg Crown Ratio")
            {
                xrLabel1.Text = string.Format("{0:#,#}", 0);
                xrLabel3.Text = string.Format("{0:#,#}", 0);
                xrLabel4.Text = string.Format("{0:#,#}", 0);
                xrLabel5.Text = string.Format("{0:#,#}", 0);
                xrLabel6.Text = string.Format("{0:#,#}", 0);
                xrLabel7.Text = string.Format("{0:#,#}", 0);
                xrLabel17.Text = string.Format("{0:#,#}", 0);
                xrLabel18.Text = string.Format("{0:#,#}", 0);
            }
            if (item == "Avg Total Ht Ft")
            {
                xrLabel1.Text = string.Format("{0:#,#}", 0);
                xrLabel3.Text = string.Format("{0:#,#}", 0);
                xrLabel4.Text = string.Format("{0:#,#}", 0);
                xrLabel5.Text = string.Format("{0:#,#}", 0);
                xrLabel6.Text = string.Format("{0:#,#}", 0);
                xrLabel7.Text = string.Format("{0:#,#}", 0);
                xrLabel17.Text = string.Format("{0:#,#}", 0);
                xrLabel18.Text = string.Format("{0:#,#}", 0);
            }
        }
    }
}
