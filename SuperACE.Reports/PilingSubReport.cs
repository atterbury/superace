﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class PilingSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        float ppTotalPieces = 0;
        float ppTotalMbf = 0;
        float ppTotalCcf = 0;
        float ppTotalTons = 0;
        float ppTotalLinealFeet = 0;
        float ppTotalDollars = 0;

        float speciesTotalPieces = 0;
        float speciesTotalMbf = 0;
        float speciesTotalCcf = 0;
        float speciesTotalTons = 0;
        float speciesTotalLinealFeet = 0;
        float speciesTotalDollars = 0;

        float standTotalPieces = 0;
        float standTotalMbf = 0;
        float standTotalCcf = 0;
        float standTotalTons = 0;
        float standTotalLinealFeet = 0;
        float standTotalDollars = 0;

        public PilingSubReport()
        {
            InitializeComponent();

        }

        private void PilingSubReport_DataSourceDemanded(object sender, EventArgs e)
        {
            //sqlDataSource1.DataSource = TempBLL.GetRptPolePiling(Convert.ToInt64(this.Parameters["StandsSelectedId"].Value));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ppTotalPieces += Convert.ToSingle(GetCurrentColumnValue("NumberPieces"));
            ppTotalDollars += Convert.ToSingle(GetCurrentColumnValue("TotalDollars"));
            ppTotalMbf += Convert.ToSingle(GetCurrentColumnValue("TotalMBF"));
            ppTotalCcf += Convert.ToSingle(GetCurrentColumnValue("TotalCCF"));
            ppTotalTons += Convert.ToSingle(GetCurrentColumnValue("TotalTons"));
            ppTotalLinealFeet += Convert.ToSingle(GetCurrentColumnValue("LinealFeet"));

            speciesTotalPieces += Convert.ToSingle(GetCurrentColumnValue("NumberPieces"));
            speciesTotalDollars += Convert.ToSingle(GetCurrentColumnValue("TotalDollars"));
            speciesTotalMbf += Convert.ToSingle(GetCurrentColumnValue("TotalMBF"));
            speciesTotalCcf += Convert.ToSingle(GetCurrentColumnValue("TotalCCF"));
            speciesTotalTons += Convert.ToSingle(GetCurrentColumnValue("TotalTons"));
            speciesTotalLinealFeet += Convert.ToSingle(GetCurrentColumnValue("LinealFeet"));

            standTotalPieces += Convert.ToSingle(GetCurrentColumnValue("NumberPieces"));
            standTotalDollars += Convert.ToSingle(GetCurrentColumnValue("TotalDollars"));
            standTotalMbf += Convert.ToSingle(GetCurrentColumnValue("TotalMBF"));
            standTotalCcf += Convert.ToSingle(GetCurrentColumnValue("TotalCCF"));
            standTotalTons += Convert.ToSingle(GetCurrentColumnValue("TotalTons"));
            standTotalLinealFeet += Convert.ToSingle(GetCurrentColumnValue("LinealFeet"));
        }

        private void GroupFooter3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Sort
            xrLabel33.Text = string.Format("{0:0.00}", ppTotalDollars/ppTotalLinealFeet);
            xrLabel35.Text = string.Format("{0:0.00}", ppTotalDollars/ppTotalTons);
            xrLabel34.Text = string.Format("{0:0.00}", ppTotalDollars/ppTotalCcf);
            xrLabel36.Text = string.Format("{0:0.00}", ppTotalDollars / ppTotalMbf);
            xrlSortAvgTons.Text = string.Format("{0:0.00}", ppTotalTons / ppTotalPieces);
            xrlSortAvgCuFt.Text = string.Format("{0:#,#}", ppTotalCcf * 100 / ppTotalPieces);
            xrlSortAvgBdFt.Text = string.Format("{0:#,#}", ppTotalMbf * 1000 / ppTotalPieces);
            xrlSortDollarsPerPiece.Text = string.Format("{0:0.00}", ppTotalDollars / ppTotalPieces);
        }

        private void GroupFooter3_AfterPrint(object sender, EventArgs e)
        {
            // Sort
            ppTotalPieces = 0;
            ppTotalDollars = 0;
            ppTotalMbf = 0;
            ppTotalCcf = 0;
            ppTotalTons = 0;
            ppTotalLinealFeet = 0;
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Species
            xrLabel50.Text = string.Format("{0:0.00}", speciesTotalDollars / speciesTotalLinealFeet);
            xrLabel48.Text = string.Format("{0:0.00}", speciesTotalDollars / speciesTotalTons);
            xrLabel49.Text = string.Format("{0:0.00}", speciesTotalDollars / speciesTotalCcf);
            xrLabel47.Text = string.Format("{0:0.00}", speciesTotalDollars / speciesTotalMbf);

            xrlSpcAvgTons.Text = string.Format("{0:0.00}", speciesTotalTons / speciesTotalPieces);
            xrlSpcAvgCuFt.Text = string.Format("{0:#,#}", speciesTotalCcf * 100 / speciesTotalPieces);
            xrlSpcAvgBdFt.Text = string.Format("{0:#,#}", speciesTotalMbf * 1000 / speciesTotalPieces);
            xrlSpcDollarsPerPiece.Text = string.Format("{0:0.00}", speciesTotalDollars / speciesTotalPieces);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            // Species
            speciesTotalPieces = 0;
            speciesTotalDollars = 0;
            speciesTotalMbf = 0;
            speciesTotalCcf = 0;
            speciesTotalTons = 0;
            speciesTotalLinealFeet = 0;
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // stand
            xrLabel64.Text = string.Format("{0:0.00}", standTotalDollars / standTotalLinealFeet);
            xrLabel62.Text = string.Format("{0:0.00}", standTotalDollars / standTotalTons);
            xrLabel63.Text = string.Format("{0:0.00}", standTotalDollars / standTotalCcf);
            xrLabel61.Text = string.Format("{0:0.00}", standTotalDollars / standTotalMbf);

            xrlStandAvgTons.Text = string.Format("{0:0.00}", standTotalTons / standTotalPieces);
            xrlStandAvgCuFt.Text = string.Format("{0:#,#}", standTotalCcf * 100 / standTotalPieces);
            xrlStandAvgBdFt.Text = string.Format("{0:#,#}", standTotalMbf * 1000 / standTotalPieces);
            xrlStandDollarsPerPiece.Text = string.Format("{0:0.00}", standTotalDollars / standTotalPieces);
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            // stand
            standTotalPieces = 0;
            standTotalDollars = 0;
            standTotalMbf = 0;
            standTotalCcf = 0;
            standTotalTons = 0;
            standTotalLinealFeet = 0;
        }
    }
}
