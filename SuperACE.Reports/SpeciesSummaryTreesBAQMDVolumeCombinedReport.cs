﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryTreesBAQMDVolumeCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        private string project = string.Empty;
        private float totalAcres = 0;
        private float totalPlots = 0;
        private float totalTrees = 0;

        public SpeciesSummaryTreesBAQMDVolumeCombinedReport(string pProject, float pAcres, float pPlots, float pTrees)
        {
            InitializeComponent();

            project = pProject;
            totalAcres = pAcres;
            totalPlots = pPlots;
            totalTrees = pTrees;

            //objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel111.Text = project;
            xrLabel114.Text = totalAcres.ToString();
            xrLabel105.Text = totalPlots.ToString();
            xrLabel108.Text = totalTrees.ToString();
        }
    }
}
