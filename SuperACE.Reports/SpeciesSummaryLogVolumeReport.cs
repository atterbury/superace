﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryLogVolumeReport : DevExpress.XtraReports.UI.XtraReport
    {
        SepStandsSelected current = null;
        
        public SpeciesSummaryLogVolumeReport()
        {
            InitializeComponent();

            bindingSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void DetailReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            bindingSource2.DataSource = TempBLL.GetRptSpeciesSummaryLogVolume(Convert.ToInt64(xrLabel142.Text)); // was current.StandsSelectedId
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //current = bindingSource1.Current as SepStandsSelected;
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = this.GetCurrentColumnValue("State").ToString();
            string county = this.GetCurrentColumnValue("County").ToString();

            if (!string.IsNullOrEmpty(state))
                xrLabel110.Text = state;
            else
                xrLabel110.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel109.Text = state;
            else
                xrLabel109.Text = "Blank";
        }
    }
}
