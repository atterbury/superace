﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

namespace SuperACE.Reports
{
    public partial class SpeciesSummaryLogVolumeReport1 : DevExpress.XtraReports.UI.XtraReport
    {
        public SpeciesSummaryLogVolumeReport1()
        {
            InitializeComponent();

            objectDataSource1.DataSource = TempBLL.GetSepStandsSelected();
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string state = this.GetCurrentColumnValue("State").ToString();
            string county = this.GetCurrentColumnValue("County").ToString();

            if (!string.IsNullOrEmpty(state))
                xrLabel36.Text = state;
            else
                xrLabel36.Text = "Blank";
            if (!string.IsNullOrEmpty(county))
                xrLabel37.Text = state;
            else
                xrLabel37.Text = "Blank";
        }
    }
}
