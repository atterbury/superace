﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;
using DevExpress.XtraRichEdit.Model;

namespace SuperACE.Reports
{
    public partial class StatisticsCombinedReport : DevExpress.XtraReports.UI.XtraReport
    {
        private string project = string.Empty;
        private float acres = 0;
        private float plots = 0;
        private float trees = 0;

        public StatisticsCombinedReport(string pProject, float pAcres, float pPlots, float pTrees)
        {
            InitializeComponent();

            project = pProject;
            acres = pAcres;
            plots = pPlots;
            trees = pTrees;
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel111.Text = project;
            xrLabel114.Text = acres.ToString();
            xrLabel105.Text = plots.ToString();
            xrLabel108.Text = trees.ToString();
        }
    }
}
