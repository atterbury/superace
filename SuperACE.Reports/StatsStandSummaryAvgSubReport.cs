﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Temp.DAL;

using SuperACEUtils;
using System.Linq;

namespace SuperACE.Reports
{
    public partial class StatsStandSummaryAvgSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        private float totalBasAreaPerAcre = 0;
        private float totalTreesPerAcre = 0;
        private float standTotalHeight = 0;
        private float standTreesPerAcre = 0;
        private float cv = 0;
        private float se = 0;

        public StatsStandSummaryAvgSubReport()
        {
            InitializeComponent();
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            // QB = Sqrt(Sum(Fields.BasalAreaPerAcre)/(Sum(Fields.TreesPerAcre)*0.005454154))
            // RD = Sum(Fields.BasalAreaPerAcre)/Pow(Sqrt(Sum(Fields.BasalAreaPerAcre)/(Sum(Fields.TreesPerAcre)*0.005454154)),0.5)
            totalBasAreaPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("BasalAreaPerAcre").ToString());
            //totalTreesPerAcre += Utils.ConvertToFloat(this.xrLabel21.Text);
            totalTreesPerAcre += Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString());
            if (Utils.ConvertToFloat(this.GetCurrentColumnValue("AvgTotalHt").ToString()) > 0)
            {
                standTotalHeight += (Utils.ConvertToFloat(this.GetCurrentColumnValue("AvgTotalHt").ToString()) * Utils.ConvertToFloat(this.GetCurrentColumnValue("TreesPerAcre").ToString()));
            }
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            StatsPerAcre t = TempBLL.GetPerAcreInfo();
            this.xrLabel40.Text = string.Format("{0:0}", standTotalHeight / totalTreesPerAcre); // was standTreesPerAcre
            this.xrLabel19.Text = string.Format("{0:0.0}", Math.Sqrt(totalBasAreaPerAcre/(totalTreesPerAcre*0.005454154)));
            this.xrLabel37.Text = string.Format("{0:0.0}", totalBasAreaPerAcre/Math.Pow(Math.Sqrt(totalBasAreaPerAcre/(totalTreesPerAcre*0.005454154)),0.5));
            this.xrLabel43.Text = string.Format("{0:0.0}", t.CvPercent);
            this.xrLabel44.Text = string.Format("{0:0.0}", t.SePercent);
            this.xrLabel41.Text = string.Format("{0:0.0}", Math.Sqrt(totalBasAreaPerAcre/(totalTreesPerAcre * 0.005454154)));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            standTotalHeight = 0;
            standTreesPerAcre = 0;
        }
    }
}
