﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using SuperACEUtils;

namespace SuperACE.Reports
{
    public partial class TreeSegmentVolumeSubReport : DevExpress.XtraReports.UI.XtraReport
    {
        // private string plotFactor = string.Empty;
        // private string currentTotalHt = string.Empty;
        // private int currentSegment = 0;
        // private string treeType = string.Empty;

        // private float cuft = 0;
        // private float bdft = 0;
        // private float ba = 0;
        // private float tpa = 0;

        // retain some flags to be accessible by multiple event handlers
        bool bIsCountTree = false;
        bool bWasCalculatedHt = false;
        bool bWasStripCruise = false;
        bool bSubSegment = false;

        public int standPlots = 0;
        public float standAcres = 0;
        private float treeNetCuFt = 0, treeNetBdFt = 0;
        private float plotba = 0, plottpa = 0, plotNetCuFt = 0, plotNetBdFt = 0;
        private float standba = 0, standtpa = 0, standNetCuFt = 0, standNetBdFt = 0;

        public TreeSegmentVolumeSubReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // this should only occur once per stand
            standAcres = Convert.ToSingle(this.Parameters["Acres"].Value.ToString());
            standPlots = Convert.ToInt16(this.Parameters["Plots"].Value.ToString());

            // read flags
            string plotFactor = ReportUtils.GetStringFromColumn(this, "PlotFactor");
            bWasStripCruise = (plotFactor.StartsWith("S"));
            int currentSegment = ReportUtils.GetIntFromColumn(this, "SegmentNumber");
            bSubSegment = (currentSegment != 0 && currentSegment != 1);
            string treeType = ReportUtils.GetStringFromColumn(this, "TreeType");
            bIsCountTree = (treeType == "C");   // lookup tree type to check whether this is a count plot
            string currentTotalHt = ReportUtils.GetStringFromColumn(this, "CalcTotalHtUsedYN");
            bWasCalculatedHt = (currentTotalHt == "Y");

            // use flag to derive divisor
            float divisor = bWasStripCruise ? standAcres : standPlots;

            // start the row-by-row math
            float ba = ReportUtils.GetFloatFromColumn(this, "TreeBasalArea");
            float tpa = ReportUtils.GetFloatFromColumn(this, "TreeTreesPerAcre");

            // will be invoked on every tree segment; some math only applies at the tree level
            // for measured trees, execute on first segment; for count trees, there are no segments.
            if ((bIsCountTree) || (currentSegment == 1))
            {
                standba += (divisor > 0) ? ba / divisor : 0;
                plotba += ba;
                standtpa += (divisor > 0) ? tpa / divisor : 0;
                plottpa += tpa;
            }

            float cuft = ReportUtils.GetFloatFromColumn(this, "CubicNetVolume");
            standNetCuFt += (divisor > 0) ? (cuft * tpa) / divisor : 0;
            treeNetCuFt += cuft;
            plotNetCuFt += cuft * tpa;

            float bdft = ReportUtils.GetFloatFromColumn(this, "ScribnerNetVolume");
            standNetBdFt += (divisor > 0) ? (bdft * tpa) / divisor : 0;
            treeNetBdFt += bdft;
            plotNetBdFt += bdft * tpa;
        }

        // Default highlight handling; special treatments are below
        // this event is triggered as handler by detail cells
        private void xrLabel35_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = sender as XRLabel;
            if (bSubSegment)
            {
                label.Text = string.Empty;
                return;
            }
            // style count plot higlights (includes fonting)
            ReportUtils.StyleCellHighlights(label, bIsCountTree);
        }

        private void xrLabel47_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // call default handler
            xrLabel35_BeforePrint(sender, e);

            // now override color of this specific cell, to indicate Calcualted Heights
            XRLabel label = sender as XRLabel;
            label.ForeColor = (bWasCalculatedHt) ? Color.Red : label.ForeColor;
        }

        private void xrLabel21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // non-default treatment; change data, but no color treatment
            ((XRLabel)sender).Text = bIsCountTree ? "Count" : string.Empty;
        }

        // used on two occasions
        private void xrLabel65_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // non-default treatment; retain data, only color treatment
            ReportUtils.StyleCellHighlights(sender as XRLabel, bIsCountTree);
        }

        private void GroupFooter2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Before Plot Totals
            xrLabel20.Text = string.Format("{0:0.00}", plotba);
            xrLabel17.Text = string.Format("{0:0.000}", plottpa);
            xrLabel5.Text = string.Format("{0:#,###}", plotNetCuFt);
            xrLabel6.Text = string.Format("{0:#,###}", plotNetBdFt);
        }

        private void GroupFooter2_AfterPrint(object sender, EventArgs e)
        {
            // After Plot Totals
            plotba = 0;
            plottpa = 0;
            plotNetBdFt = 0;
            plotNetCuFt = 0;
            treeNetBdFt = 0;
            treeNetCuFt = 0;
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Before Tree Totals
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            //After Tree Totals
            treeNetBdFt = 0;
            treeNetCuFt = 0;
        }

        private void GroupFooter3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Before Stand
            xrLabel27.Text = string.Format("{0:0.00}", standba); // was /standPlots
            xrLabel28.Text = string.Format("{0:0.000}", standtpa); // /standPlots
            xrLabel7.Text = string.Format("{0:#,###}", standNetCuFt); // /standPlots
            xrLabel8.Text = string.Format("{0:#,###}", standNetBdFt); // /standPlots
        }

        private void GroupFooter3_AfterPrint(object sender, EventArgs e)
        {
            // After Stand
            standba = 0;
            standtpa = 0;
            standNetBdFt = 0;
            standNetCuFt = 0;
            treeNetBdFt = 0;
            treeNetCuFt = 0;
        }
    }
}
