﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace SuperACE.Reports
{
    public partial class RelationshipsBySpecies3SubReport : DevExpress.XtraReports.UI.XtraReport
    {
        private float netTotal = 0;
        private float netDF = 0;
        private float netWH = 0;
        private float netRC = 0;
        private float netGF = 0;
        private float netRA = 0;
        private float netOC = 0;
        private float netOH = 0;

        private float grossTotal = 0;
        private float grossDF = 0;
        private float grossWH = 0;
        private float grossRC = 0;
        private float grossGF = 0;
        private float grossRA = 0;
        private float grossOC = 0;
        private float grossOH = 0;

        private float grossMbfPerAcreTotal = 0;
        private float grossMbfPerAcreDF = 0;
        private float grossMbfPerAcreWH = 0;
        private float grossMbfPerAcreRC = 0;
        private float grossMbfPerAcreGF = 0;
        private float grossMbfPerAcreRA = 0;
        private float grossMbfPerAcreOC = 0;
        private float grossMbfPerAcreOH = 0;

        private float netMbfPerAcreTotal = 0;
        private float netMbfPerAcreDF = 0;
        private float netMbfPerAcreWH = 0;
        private float netMbfPerAcreRC = 0;
        private float netMbfPerAcreGF = 0;
        private float netMbfPerAcreRA = 0;
        private float netMbfPerAcreOC = 0;
        private float netMbfPerAcreOH = 0;
        public RelationshipsBySpecies3SubReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string item = GetCurrentColumnValue("Item").ToString();
            if (item.Contains("Total Gross Mbf"))
            {
                xrLabel1.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                grossTotal = Convert.ToSingle(GetCurrentColumnValue("Totals").ToString());
                xrLabel3.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                grossDF = Convert.ToSingle(GetCurrentColumnValue("DF").ToString());
                xrLabel4.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                grossWH = Convert.ToSingle(GetCurrentColumnValue("WH").ToString());
                xrLabel5.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                grossRC = Convert.ToSingle(GetCurrentColumnValue("RC").ToString());
                xrLabel6.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                grossGF = Convert.ToSingle(GetCurrentColumnValue("GF").ToString());
                xrLabel7.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                grossRA = Convert.ToSingle(GetCurrentColumnValue("RA").ToString());
                xrLabel17.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                grossOC = Convert.ToSingle(GetCurrentColumnValue("OC").ToString());
                xrLabel18.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                grossOH = Convert.ToSingle(GetCurrentColumnValue("OH").ToString());
            }
            if (item.Contains("Total Net Mbf"))
            {
                xrLabel1.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                netTotal = Convert.ToSingle(GetCurrentColumnValue("Totals").ToString());
                xrLabel3.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                netDF = Convert.ToSingle(GetCurrentColumnValue("DF").ToString());
                xrLabel4.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                netWH = Convert.ToSingle(GetCurrentColumnValue("WH").ToString());
                xrLabel5.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                netRC = Convert.ToSingle(GetCurrentColumnValue("RC").ToString());
                xrLabel6.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                netGF = Convert.ToSingle(GetCurrentColumnValue("GF").ToString());
                xrLabel7.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                netRA = Convert.ToSingle(GetCurrentColumnValue("RA").ToString());
                xrLabel17.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                netOC = Convert.ToSingle(GetCurrentColumnValue("OC").ToString());
                xrLabel18.Text = string.Format("{0:#,#}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                netOH = Convert.ToSingle(GetCurrentColumnValue("OH").ToString());
            }
            if (item.Contains("% Defect"))
            {
                xrLabel1.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                if (Convert.ToSingle(GetCurrentColumnValue("DF").ToString()) > 0)
                    xrLabel3.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("WH").ToString()) > 0)
                    xrLabel4.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                else
                {
                    xrLabel14.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RC").ToString()) > 0)
                    xrLabel5.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("GF").ToString()) > 0)
                    xrLabel6.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RA").ToString()) > 0)
                    xrLabel7.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OC").ToString()) > 0)
                    xrLabel17.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                {
                    xrLabel17.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OH").ToString()) > 0)
                    xrLabel18.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
            if (item.Contains("Gross Mbf per Acre"))
            {
                if (Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()) > 0)
                    xrLabel1.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                else
                {
                    xrLabel1.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("DF").ToString()) > 0)
                    xrLabel3.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("WH").ToString()) > 0)
                    xrLabel4.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                else
                {
                    xrLabel4.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RC").ToString()) > 0)
                    xrLabel5.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("GF").ToString()) > 0)
                    xrLabel6.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RA").ToString()) > 0)
                    xrLabel7.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OC").ToString()) > 0)
                    xrLabel17.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                else
                {
                    xrLabel17.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OH").ToString()) > 0)
                    xrLabel18.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
            if (item.Contains("Net Mbf per Acre"))
            {
                if (Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()) > 0)
                    xrLabel1.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                else
                {
                    xrLabel1.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("DF").ToString()) > 0)
                    xrLabel3.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("WH").ToString()) > 0)
                    xrLabel4.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                else
                {
                    xrLabel4.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RC").ToString()) > 0)
                    xrLabel5.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("GF").ToString()) > 0)
                    xrLabel6.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RA").ToString()) > 0)
                    xrLabel7.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OC").ToString()) > 0)
                    xrLabel17.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                else
                {
                    xrLabel17.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OH").ToString()) > 0)
                    xrLabel18.Text = string.Format("{0:0.000}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
            if (item.Contains("% Net Mbf by Species"))
            {
                xrLabel1.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("Totals").ToString()));
                if (Convert.ToSingle(GetCurrentColumnValue("DF").ToString()) > 0)
                    xrLabel3.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("DF").ToString()));
                else
                {
                    xrLabel3.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("WH").ToString()) > 0)
                    xrLabel4.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("WH").ToString()));
                else
                {
                    xrLabel14.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RC").ToString()) > 0)
                    xrLabel5.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("RC").ToString()));
                else
                {
                    xrLabel5.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("GF").ToString()) > 0)
                    xrLabel6.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("GF").ToString()));
                else
                {
                    xrLabel6.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("RA").ToString()) > 0)
                    xrLabel7.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("RA").ToString()));
                else
                {
                    xrLabel7.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OC").ToString()) > 0)
                    xrLabel17.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("OC").ToString()));
                {
                    xrLabel17.Text = string.Empty;
                }
                if (Convert.ToSingle(GetCurrentColumnValue("OH").ToString()) > 0)
                    xrLabel18.Text = string.Format("{0:0.0}", Convert.ToSingle(GetCurrentColumnValue("OH").ToString()));
                else
                {
                    xrLabel18.Text = string.Empty;
                }
            }
        }
    }
}
