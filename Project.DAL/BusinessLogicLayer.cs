﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.IO;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlServerCe;
using System.Runtime.InteropServices.ComTypes;
using DevExpress.XtraGrid.Views.Grid;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using Project.DAL;

namespace Project.DAL
{
    public class ProjectBLL
    {
        #region Gets
        public static List<StandHauling> GetStandHauling(ref ProjectDbContext pCtx, long? id)
        {
            var result = pCtx.StandHaulings.OrderBy(t => t.Destination).Where(t => t.StandsId == id).ToList();
            return result;
        }

        public static List<Treesegment> GetSegmentsForStand(ref ProjectDbContext pCtx, long? id)
        {
            var result = pCtx.Treesegments.OrderBy(t => t.Species).ThenBy(t => t.SortCode).Where(t => t.StandsId == id).ToList();
            return result;
        }

        public static Stand GetStand(ref ProjectDbContext pCtx, int pId)
        {
            var result = pCtx.Stands.FirstOrDefault(s => s.StandsId == pId);
            return result;
        }

        public static string GetPlotFactor(Stand pStand, Tree pItem)
        {
            switch (pItem.PlotFactorInput)
            {
                case "B1":
                    return string.Format("{0:0.00}",  pStand.Baf1);
                case "B2":
                    return string.Format("{0:0.00}",  pStand.Baf2);
                case "B3":
                    return string.Format("{0:0.00}",  pStand.Baf3);
                case "B4":
                    return string.Format("{0:0.00}",  pStand.Baf4);
                case "B5":
                    return string.Format("{0:0.00}", pStand.Baf5);

                case "S1":
                    return string.Format("{0:0.00}",  pStand.StripBlowup1); // was pStand.Strip1
                case "S2":
                    return string.Format("{0:0.00}",  pStand.StripBlowup2);
                case "S3":
                    return string.Format("{0:0.00}",  pStand.StripBlowup3);
                case "S4":
                    return string.Format("{0:0.00}",  pStand.StripBlowup4);
                case "S5":
                    return string.Format("{0:0.00}",  pStand.StripBlowup5);

                case "R1":
                case "R2":
                case "R3":
                case "R4":
                case "R5":
                    return GetAreaValue(pStand, pItem.PlotFactorInput);

                case "F1":
                case "A1":
                    return string.Format("{0:0.00}", pStand.AreaBlowup1);
                case "F2":
                case "A2":
                    return string.Format("{0:0.00}", pStand.AreaBlowup2);
                case "F3":
                case "A3":
                    return string.Format("{0:0.00}", pStand.AreaBlowup3);
                case "F4":
                case "A4":
                    return string.Format("{0:0.00}", pStand.AreaBlowup4);
                case "F5":
                case "A5":
                    return string.Format("{0:0.00}", pStand.AreaBlowup5);
                default:
                    return pItem.PlotFactorInput;
            }
            return string.Empty;
        }

        public static string GetAreaValue(Stand pStand, string p)
        {
            if (p[0] == 'R')
            {
                if (pStand.AreaCode1 == p)
                    return string.Format("{0:0.00}", pStand.AreaPlotRadius1);
                if (pStand.AreaCode2 == p)
                    return string.Format("{0:0.00}", pStand.AreaPlotRadius2);
                if (pStand.AreaCode3 == p)
                    return string.Format("{0:0.00}", pStand.AreaPlotRadius3);
                if (pStand.AreaCode4 == p)
                    return string.Format("{0:0.00}", pStand.AreaPlotRadius4);
                if (pStand.AreaCode5 == p)
                    return string.Format("{0:0.00}", pStand.AreaPlotRadius5);
            }

            if (p[0] == 'F')
            {
                if (pStand.AreaCode1 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup1);
                if (pStand.AreaCode2 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup2);
                if (pStand.AreaCode3 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup3);
                if (pStand.AreaCode4 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup4);
                if (pStand.AreaCode5 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup5);
            }

            if (p[0] == 'A')
            {
                if (pStand.AreaCode1 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup1);
                if (pStand.AreaCode2 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup2);
                if (pStand.AreaCode3 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup3);
                if (pStand.AreaCode4 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup4);
                if (pStand.AreaCode5 == p)
                    return string.Format("{0:0.00}", pStand.AreaBlowup5);
            }

            return "";
        }

        public static List<RecentReport> GetRecentReports(ref ProjectDbContext pCtx)
        {
            var result = pCtx.RecentReports.OrderByDescending(t => t.LastRun).ThenBy(t => t.JobName).ToList();
            return result;
        }

        public static Project GetProject(ref ProjectDbContext pContext, string pProjectLocation)
        {
            if (!string.IsNullOrEmpty(pProjectLocation))
            {
                if (File.Exists(pProjectLocation))
                {
                    try
                    {
                        string projectDataSource = pProjectLocation;
                        Project project = new Project();
                        project = pContext.Projects.FirstOrDefault();
                        return project;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                    return null;
            }
            else
                return null;
        }

        public static List<Stand> GetAllStands(ref ProjectDbContext pContext)
        {
            var stands = pContext.Stands.OrderBy(s => s.TractGroup).ThenBy(s => s.StandName).ToList();
            //var q = ctx.Stand
            //    .OrderBy(s => s.TractName)
            //    .ThenBy(s => s.StandName)
            //    .Select(s => new
            //    {
            //        s.TractGroup,
            //        s.TractName,
            //        s.StandName,
            //        s.Township,
            //        s.Range,
            //        s.Section,
            //        s.NetGeographicAcres,
            //        s.Source,
            //        s.DateOfStandData,
            //        s.MajorAge,
            //        s.SiteIndex,
            //        s.MajorSpecies,
            //        s.TreesPerAcre,
            //        s.BasalAreaPerAcre,
            //        s.QmDbh,
            //        s.CcfPerAcre,
            //        s.MbfPerAcre,
            //        s.TotalCcf,
            //        s.TotalMbf,
            //        s.State,
            //        s.County,
            //        s.Harvest,
            //        s.LandClass,
            //        s.GrownToDate,
            //        s.Attachments
            //    }).ToList();
            //foreach (var item in q)
            //{
            //    StandListDTO s = new StandListDTO();
            //    s.TractGroup = item.TractGroup;
            //    s.TractName = item.TractName;
            //    s.StandName = item.StandName;
            //    s.Township = item.Township;
            //    s.Range = item.Range;
            //    s.Section = item.Section;
            //    s.NetGeographicAcres = item.NetGeographicAcres;
            //    s.Source = item.Source;
            //    s.DateOfStandData = item.DateOfStandData;
            //    s.MajorAge = item.MajorAge;
            //    s.SiteIndex = item.SiteIndex;
            //    s.MajorSpecies = item.MajorSpecies;
            //    s.TreesPerAcre = item.TreesPerAcre;
            //    s.BasalAreaPerAcre = item.BasalAreaPerAcre;
            //    s.QmDbh = item.QmDbh;
            //    s.CcfPerAcre = item.CcfPerAcre;
            //    s.MbfPerAcre = item.MbfPerAcre;
            //    s.TotalCcf = item.TotalCcf;
            //    s.TotalMbf = item.TotalMbf;
            //    s.State = item.State;
            //    s.County = item.County;
            //    s.Harvest = item.Harvest;
            //    s.LandClass = item.LandClass;
            //    s.GrownToDate = item.GrownToDate;
            //    s.Attachments = item.Attachments;
            //    stands.Add(s);
            //}
            return stands;
        }

        public static string GetConnectionString(string pDataSource)
        {
            //string CONNECTIONSTRING = string.Format(@"data source=(LocalDB)\v11.0;attachdbfilename={0};integrated security=True;connect timeout=30;connect timeout=30;MultipleActiveResultSets=True;App=EntityFramework", pDataSource);
            //return CONNECTIONSTRING; // ecb.ToString();

            var connectionString = string.Format("Data Source={0}", pDataSource);
            System.Data.SqlClient.SqlConnectionStringBuilder scsb = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);

            var ecb = new EntityConnectionStringBuilder();
            ecb.Metadata = "res://*/csdl|res://*/ssdl|res://*/msl";
            ecb.Provider = "System.Data.SqlServerCe.4.0";
            ecb.ProviderConnectionString = connectionString;

            return ecb.ToString();
        }

        public static int GetTreeCount(ref ProjectDbContext pCtx, int p)
        {
            int result = 0;
            //var query = ctx.Trees.Where(t => t.StandsId == p && t.Dbh > 0).ToList();
            float f = pCtx.Trees.Where(t => t.StandsId == p && t.Dbh > 0).Select(t => (float)t.TreeCount).Sum();
            result = (int)f;
            //result = query.Count();
            return result;
        }

        public static DefStat GetDefStatistics(ref ProjectDbContext pCtx)
        {
            try
            {
                var result = pCtx.DefStats.FirstOrDefault();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public static List<Attachment> GetAttachmentForStandAndPlot(ref ProjectDbContext pCtx, Stand pStand, Plot pPlot)
        {
            var result = pCtx.Attachments.Where(a => a.StandsId == pStand.StandsId && a.PlotId == pPlot.PlotsId).ToList();
            return result;
        }

        public static List<Attachment> GetAttachmentForStand(ref ProjectDbContext pCtx, Stand pStand)
        {
            var result = pCtx.Attachments.Where(a => a.StandsId == pStand.StandsId).ToList();
            return result;
        }
        #endregion

        #region Save
        public static Project SaveProject(ref ProjectDbContext pCtx,  Project pProject)
        {
            Project project = pProject;
            if (project.ProjectsId > 0)
            {
                pCtx.Entry(project).State = EntityState.Modified;
                pCtx.SaveChanges();
            }
            else
            {
                pCtx.Projects.Add(project);
                pCtx.SaveChanges();
            }
            return project;
        }

        public static Stand SaveStand(ref ProjectDbContext pCtx, Stand pStand)
        {
            Stand existingStand = pStand;
            pCtx.Entry(existingStand).State = EntityState.Modified;
            pCtx.SaveChanges();
            return existingStand;
        }

        public static void SaveStandHauling(List<StandHauling> pColl, ref ProjectDbContext pCtx)
        {
            //StandHauling existingStandHauling = pStand;
            foreach (var item in pColl)
            {
                switch (pCtx.Entry(item).State)
                {
                    case EntityState.Added:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Unchanged:
                        break;
                }
            }
            //ctx.Entry(existingStand).State = EntityState.Modified;
            //ctx.SaveChanges();
        }

        public static void SaveDefStats(ref ProjectDbContext pCtx, DefStat rec)
        {
            if (rec.Id > 0)
            {
                pCtx.Entry(rec).State = EntityState.Modified;
                pCtx.SaveChanges();
            }
            else
            {
                pCtx.DefStats.Add(rec);
                pCtx.SaveChanges();
            }
        }

        public static int? GetPriceByCatagory(ref ProjectDbContext pCtx, string pTableName, string pSpecies, string pSort, double? pLen, double? pDia)
        {
            List<Price> prices = pCtx.Prices.Where(p => p.TableName == pTableName).ToList();
            foreach (var item in prices)
            {
                if (item.Species.Contains(pSpecies) && item.Sorts.Contains(pSort))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                    return (int)item.PriceByLengthCategory1;
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                    return (int)item.PriceByLengthCategory2;
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                    return (int)item.PriceByLengthCategory3;
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                    return (int)item.PriceByLengthCategory4;
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return (int)item.PriceByLengthCategory5;
                                default:
                                    return -1;
                            }
                    }
                }
            }

            List<Price> speciesSortPrices = pCtx.Prices.Where(p => p.TableName == pTableName && p.Species == pSpecies && p.Sorts == "*").ToList();
            foreach (var item in speciesSortPrices)
            {
                if (item.Species.Contains(pSpecies))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                    return (int)item.PriceByLengthCategory1;
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                    return (int)item.PriceByLengthCategory2;
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                    return (int)item.PriceByLengthCategory3;
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                    return (int)item.PriceByLengthCategory4;
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return (int)item.PriceByLengthCategory5;
                                default:
                                    return -1;
                            }
                    }
                }
            }

            List<Price> speciesPrices = pCtx.Prices.Where(p => p.TableName == pTableName && p.Species == "*").ToList();
            foreach (var item in speciesPrices)
            {
                if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                {
                    if (pLen != null)
                        switch ((int)pLen)
                        {
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                                return (int)item.PriceByLengthCategory1;
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                            case 21:
                            case 22:
                            case 23:
                            case 24:
                            case 25:
                                return (int)item.PriceByLengthCategory2;
                            case 26:
                            case 27:
                            case 28:
                            case 29:
                            case 30:
                            case 31:
                                return (int)item.PriceByLengthCategory3;
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                                return (int)item.PriceByLengthCategory4;
                            case 36:
                            case 37:
                            case 38:
                            case 39:
                            case 40:
                                return (int)item.PriceByLengthCategory5;
                            default:
                                return -1;
                        }
                }
            }

            return -1;
        }

        public static string GetPriceProduct(ref ProjectDbContext pCtx, string pTableName, string pSpecies, string pSort, double? pLen, double? pDia)
        {
            List<Price> prices = pCtx.Prices.Where(p => p.TableName == pTableName).ToList();
            foreach (var item in prices)
            {
                if (item.Species.Contains(pSpecies) && item.Sorts.Contains(pSort))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return item.ProductName;
                            }
                    }
                }
            }

            List<Price> speciesPrices = pCtx.Prices.Where(p => p.TableName == pTableName && p.Sorts == "*").ToList();
            foreach (var item in speciesPrices)
            {
                if (item.Species.Contains(pSpecies))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return item.ProductName;
                            }
                    }
                }
            }
            return "POLES";
        }

        public static Price GetPrice(ProjectDbContext pCtx, string pTableName, string pSpecies, string pSort, double? pLen, double? pDia)
        {
            List<Price> prices = pCtx.Prices.Where(p => p.TableName == pTableName).ToList();
            if (string.IsNullOrEmpty(pSort))
                return null;
            if (pSort == "0")
                return null;

            if (pSort.Contains("POL"))
                pSort = "P";
            if (pSort.Contains("PIL"))
                pSort = "G";
            
            foreach (var item in prices)
            {
                if (item.Species.Contains(pSpecies) && item.Sorts.Contains(pSort))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return item;
                            }
                    }
                }
            }

            List<Price> speciesPrices1 = pCtx.Prices.Where(p => p.TableName == pTableName && p.Sorts == "*").ToList();
            foreach (var item in speciesPrices1)
            {
                if (item.Species.Contains(pSpecies))
                {
                    if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                    {
                        if (pLen != null)
                            switch ((int)pLen)
                            {
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                    return item;
                            }
                    }
                }
            }

            List<Price> speciesPrices2 = pCtx.Prices.Where(p => p.TableName == pTableName && p.Species == "*" && p.Sorts == "*").ToList();
            foreach (var item in speciesPrices2)
            {
                if (pDia >= item.MinimumDiameter && pDia <= item.MaximumDiameter)
                {
                    if (pLen != null)
                        switch ((int)pLen)
                        {
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                            case 21:
                            case 22:
                            case 23:
                            case 24:
                            case 25:
                            case 26:
                            case 27:
                            case 28:
                            case 29:
                            case 30:
                            case 31:
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                            case 36:
                            case 37:
                            case 38:
                            case 39:
                            case 40:
                                return item;
                        }
                }
            }

            Debug.WriteLine(string.Format("GetPrice - Species:{0} Sort:{1} Len:{2:0} Dia:{3:0}", pSpecies, pSort, pLen, pDia));
            return null;
        }

        public static List<Harvestsystem> GetHarvestSystems(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Harvestsystems.OrderBy(h => h.DisplayOrder).Where(h => h.TableName == pTable).ToList();
            return result;
        }

        public static List<Nonstocked> GetNonStocked(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Nonstockeds.OrderBy(h => h.Description).Where(h => h.TableName == pTable).ToList();
            return result;
        }

        public static List<Nontimbered> GetNonTimbered(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Nontimbereds.OrderBy(h => h.Description).Where(h => h.TableName == pTable).ToList();
            return result;
        }

        public static List<string> GetYieldTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.YieldTables
                            group s by s.TableName into myGroup
                            orderby myGroup.Key
                            select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetYardingCosts(ref ProjectDbContext pCtx, string tableName)
        {
            List<string> varList = new List<string>();
            var result = pCtx.Costs.Where(c => c.TableName == tableName && c.CostType == "YARDING").OrderBy(c => c.CostDescription).ToList();
            foreach (var item in result)
                varList.Add(item.CostDescription);
            return varList;
        }

        public static List<string> GetSpeciesTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Species
                group s by s.TableName into myGroup
                orderby myGroup.Key
                select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetSortTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varSorts = new List<string>();
            var result = (from s in pCtx.Sorts
                group s by s.TableName into myGroup
                orderby myGroup.Key
                select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varSorts.Add(item.TableName);
            return varSorts;
        }

        public static List<string> GetGradeTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Grades
                group s by s.TableName into myGroup
                orderby myGroup.Key
                select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetPriceTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Prices
                group s by s.TableName into myGroup
                orderby myGroup.Key
                select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetCostTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Costs
                group s by s.TableName into myGroup
                orderby myGroup.Key
                select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static Species GetSpecieByAbbrev(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Species.FirstOrDefault(s => s.TableName == pTable && s.Abbreviation == pCode);
            return result;
        }

        public static Species GetSpecieByAbbrev(string pDataSource, string pTable, string pCode)
        {
            using (var ctx = new ProjectDbContext("Data Source = " + pDataSource))
            {
                var result = ctx.Species.FirstOrDefault(s => s.TableName == pTable && s.Abbreviation == pCode);
                return result;
            }
        }

        public static Species GetSpecieByCode(ref ProjectDbContext pCtx, string pTable, int pCode)
        {
            var result = pCtx.Species.FirstOrDefault(s => s.TableName == pTable && s.InputCode == pCode);
            return result;
        }
        #endregion

        public static void SetTreeType(ref ProjectDbContext pCtx, Project pProject)
        {
            List<Stand> tColl = pCtx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).Where(s => s.ProjectsId == pProject.ProjectsId).ToList();
            foreach (var standItem in tColl)
            {
                List<Tree> treeList = pCtx.Trees.Where(t => t.StandsId == standItem.StandsId).OrderBy(t => t.StandsId).ThenBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();
                foreach (var treeItem in treeList)
                {
                    treeItem.TreeType = "D";

                    if (!string.IsNullOrEmpty(treeItem.PlotFactorInput))
                    {
                        if (treeItem.PlotFactorInput.StartsWith("R"))
                            treeItem.TreeType = "R";
                        else
                        {
                            if (treeItem.BoleHeight == 0 && treeItem.TotalHeight == 0)
                                treeItem.TreeType = "C";
                            else
                                treeItem.TreeType = "D";
                        }
                    }
                    else
                    {
                        if (treeItem.PlotFactorInput.StartsWith("R"))
                            treeItem.TreeType = "R";
                        else
                            treeItem.TreeType = "C";
                    }

                    //if (treeItem.PlotFactorInput.StartsWith("R") == false && treeItem.TreeCount > 1)
                    //    treeItem.TreeType = "C";
                    //else
                    //if (treeItem.PlotFactorInput.StartsWith("R"))
                    //    treeItem.TreeType = "R";
                    //else
                    //    if (treeItem.PlotFactorInput.StartsWith("S"))
                    //        treeItem.TreeType = "S";
                }
                pCtx.SaveChanges();
            }
        }

        public static void SetTreeType(ref Tree treeItem)
        {
            treeItem.TreeType = "D";

            if (!string.IsNullOrEmpty(treeItem.PlotFactorInput))
            {
                if (treeItem.PlotFactorInput.StartsWith("R"))
                    treeItem.TreeType = "R";
                else
                {
                    if (treeItem.BoleHeight == 0 && treeItem.TotalHeight == 0)
                        treeItem.TreeType = "C";
                    else
                        treeItem.TreeType = "D";
                }
            }
            else
            {
                if (treeItem.PlotFactorInput.StartsWith("R"))
                    treeItem.TreeType = "R";
                else
                    treeItem.TreeType = "C";
            }
        }

        public static void SetCalcTotalHeightUsed(ref ProjectDbContext pCtx, Project pProject)
        {
            List<Stand> tColl = pCtx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).Where(s => s.ProjectsId == pProject.ProjectsId).ToList();
            foreach (var standItem in tColl)
            {
                List<Tree> treeList = pCtx.Trees.Where(t => t.StandsId == standItem.StandsId).OrderBy(t => t.StandsId).ThenBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();
                foreach (var treeItem in treeList)
                {
                    if (treeItem.TotalHeight == null || treeItem.TotalHeight == 0)
                    {
                        if (treeItem.CalcTotalHeight > treeItem.BoleHeight)
                        {
                            treeItem.TotalHeight = treeItem.CalcTotalHeight;
                            treeItem.CalcTotalHtUsedYn = "Y";
                        }
                    }
                    else
                    {
                        if (treeItem.BoleHeight > treeItem.TotalHeight)
                        {
                            treeItem.TotalHeight = 0;
                            treeItem.CalcTotalHtUsedYn = "N";
                        }
                    }
                }
                pCtx.SaveChanges();
            }
        }

        public static void SetTdf(ref ProjectDbContext pCtx, Project pProject)
        {
            List<Tree> coll = pCtx.Trees.OrderBy(t => t.StandsId).ThenBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).ToList();
            foreach (var treeItem in coll)
            {
                if (!string.IsNullOrEmpty(treeItem.TopDiameterFractionCode))
                {
                    if (treeItem.TopDiameterFractionCode.Length == 1)
                    {
                        switch (treeItem.TopDiameterFractionCode)
                        {
                            case "0":
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                            case "5":
                            case "6":
                            case "7":
                            case "8":
                            case "9":
                                treeItem.TopDiameterFractionCode = treeItem.TopDiameterFractionCode + "0";
                                break;
                        }
                    }
                }
            }
            pCtx.SaveChanges();
        }

        #region Delete
        public static void DeleteStand(ref ProjectDbContext pCtx, Stand pStand)
        {
            Stand sRec = pCtx.Stands.FirstOrDefault(s => s.StandsId == pStand.StandsId);
            if (sRec != null)
            {
                pCtx.AdditionalTreeMeasurements.RemoveRange(pCtx.AdditionalTreeMeasurements.Where(a => a.StandsId == sRec.StandsId));
                pCtx.Treesegments.RemoveRange(pCtx.Treesegments.Where(s => s.StandsId == sRec.StandsId));
                pCtx.Trees.RemoveRange(pCtx.Trees.Where(t => t.StandsId == sRec.StandsId));
                pCtx.Plots.RemoveRange(pCtx.Plots.Where(p => p.StandsId == sRec.StandsId));
                pCtx.StandInputs.RemoveRange(pCtx.StandInputs.Where(s => s.StandsId == sRec.StandsId));
                pCtx.AdjCruisePlots.RemoveRange(pCtx.AdjCruisePlots.Where(a => a.StandsId == sRec.StandsId));
                pCtx.CountWithDbhs.RemoveRange(pCtx.CountWithDbhs.Where(c => c.StandsId == sRec.StandsId));
                pCtx.StandAges.RemoveRange(pCtx.StandAges.Where(c => c.StandsId == sRec.StandsId));
                pCtx.StandHaulings.RemoveRange(pCtx.StandHaulings.Where(c => c.StandsId == sRec.StandsId));
                pCtx.StandPermPlots.RemoveRange(pCtx.StandPermPlots.Where(c => c.StandsId == sRec.StandsId));
                pCtx.StandYardingSystems.RemoveRange(pCtx.StandYardingSystems.Where(c => c.StandsId == sRec.StandsId));


                pCtx.Stands.Remove(sRec);
                pCtx.SaveChanges();
            }
        }

        public static void DeleteAllStands(ref ProjectDbContext pCtx)
        {
            pCtx.AdditionalTreeMeasurements.RemoveRange(pCtx.AdditionalTreeMeasurements);
            pCtx.Treesegments.RemoveRange(pCtx.Treesegments);
            pCtx.Trees.RemoveRange(pCtx.Trees);
            pCtx.Plots.RemoveRange(pCtx.Plots);
            pCtx.StandInputs.RemoveRange(pCtx.StandInputs);
            pCtx.Stands.RemoveRange(pCtx.Stands);
            pCtx.SaveChanges();
        }

        public static void DeleteTree(ref ProjectDbContext pCtx, int pStandsId, int pTreesId)
        {
            pCtx.AdditionalTreeMeasurements.RemoveRange(pCtx.AdditionalTreeMeasurements.Where(a => a.StandsId == pStandsId && a.TreesId == pTreesId));
            pCtx.Treesegments.RemoveRange(pCtx.Treesegments.Where(s => s.StandsId == pStandsId && s.TreesId == pTreesId));
            pCtx.Trees.RemoveRange(pCtx.Trees.Where(t => t.StandsId == pStandsId && t.TreesId == pTreesId));
            //List<TREE> tColl = pContext.Trees.Where(t => t.StandsId == pStandsId).Where(t => t.TreesId == pTreesId).ToList();
            //for (int t = tColl.Count - 1; t >= 0; t--)
            //{
            //    pContext.Treesegments.RemoveRange(pContext.Treesegments.Where(s => s.TreesId == tColl[t].TreesId));
            //    pContext.Trees.Remove(tColl[t]);
            //}
            pCtx.SaveChanges();
        }

        public static void DeleteSegmentForStand(ref ProjectDbContext pCtx, Project pProject)
        {
            List<Stand> sColl = pCtx.Stands.Where(s => s.ProjectsId == pProject.ProjectsId).ToList();
            foreach (var item in sColl)
            {
                pCtx.Treesegments.RemoveRange(pCtx.Treesegments.Where(s => s.StandsId == item.StandsId));
            }
            //for (int t = sColl.Count - 1; t >= 0; t--)
            //    pContext.Treesegments.RemoveRange(pContext.Treesegments.Where(s => s.StandsId == sColl[t].StandsId));
            pCtx.SaveChanges();
        }

        public static void DeleteSegmentForTree(ref ProjectDbContext pCtx, int pStandsId, int pTreesId)
        {
            List<Tree> tColl = pCtx.Trees.Where(t => t.StandsId == pStandsId).Where(t => t.TreesId == pTreesId).ToList();
            foreach (var item in tColl)
                pCtx.Treesegments.RemoveRange(pCtx.Treesegments.Where(s => s.TreesId == item.TreesId));
            pCtx.SaveChanges();
        }
        #endregion

        public static List<Tree> GetTreesForStand(ref ProjectDbContext pCtx, int p)
        {
            var result = pCtx.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == p).ToList();
            return result;
        }

        public static Tree GetTree(ref ProjectDbContext pCtx, Stand pStand, int pId)
        {
            var result = pCtx.Trees.FirstOrDefault(t => t.StandsId == pStand.StandsId && t.TreesId == pId);
            return result;
        }

        public static List<Plot> GetPlotsByStandId(ref ProjectDbContext pCtx, long pId)
        {
            List<Plot> result;
            try
            {
                result = pCtx.Plots.Where(p => p.StandsId == pId).OrderBy(p => p.UserPlotNumber).ToList();
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public static List<Hauling> GetHauling(ref ProjectDbContext projectContext, string costTableName)
        {
            var result = projectContext.Haulings.OrderBy(t => t.Destination).Where(t => t.TableName == costTableName).ToList();
            return result;
        }

        public static Plot GetPlotByStandIdPlot(ref ProjectDbContext pCtx, long pId, string pPlot)
        {
            Plot result;
            try
            {
                result = pCtx.Plots.First(p => p.StandsId == pId && p.UserPlotNumber == pPlot);
                if (result == null)
                    result = AddPlot(ref pCtx, pId, pPlot);
            }
            catch
            {
                result = AddPlot(ref pCtx, pId, pPlot);
            }
            return result;
        }

        private static Plot AddPlot(ref ProjectDbContext pCtx, long pId, string pPlot)
        {
            Plot result = new Plot();
            result.AddFlag = string.Empty;
            result.Aspect = string.Empty;
            result.Attachments = false;
            result.BafInAt = "FP";
            result.BasalAreaAcre = 0;
            result.CruFlag = "Y";
            result.Cruiser = string.Empty;
            result.DwFlag = string.Empty;
            result.Environmental = string.Empty;
            result.Habitat = string.Empty;
            result.NetBfAcre = 0;
            result.NetCfAcre = 0;
            result.Notes = string.Empty;
            result.RefFlag = string.Empty;
            result.Slope = string.Empty;
            result.Species = string.Empty;
            result.StandsId = (int)pId;
            result.TreatmentDisplayCode = string.Empty;
            result.TreesPerAcre = 0;
            result.UserPlotNumber = pPlot;
            result.X = 0;
            result.Y = 0;
            result.Z = 0;
            pCtx.Plots.Add(result);
            pCtx.SaveChanges();
            return result;
        }

        public static void UpdateInAtForPlot(ref ProjectDbContext pCtx)
        {
            List<Stand> sColl = pCtx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (var standItem in sColl)
            {
                List<Plot> lColl = pCtx.Plots.Where(l => l.StandsId == standItem.StandsId).ToList();
                foreach (var plotItem in lColl)
                {
                    List<Tree> tColl = pCtx.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.PlotId == plotItem.PlotsId).ToList();
                    if (tColl.Count > 0)
                    {
                        if (tColl[0].Dbh == 4.0)
                            plotItem.BafInAt = "DBH";
                        else
                            plotItem.BafInAt = "FP";
                    }
                    //else
                    //    ctx.Plots.Remove(lColl[l]);
                }
            }
            pCtx.SaveChanges();
        }

        public static Project GetProject(ref ProjectDbContext pCtx)
        {
            Project result;
            try
            {
                result = pCtx.Projects.FirstOrDefault();
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public static bool DoesSpeciesExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Species.Any(s => s.TableName == pTable && s.Abbreviation == pCode);
            return result;
        }

        public static List<Cost> GetCostList(ref ProjectDbContext ctx, string pTable, string pCostType)
        {
            var result = ctx.Costs.OrderBy(t => t.ReportPrintOrder).Where(t => t.TableName == pTable && t.CostType == pCostType).ToList();
            return result;
        }

        public static object GetDestination(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Destinations.OrderBy(t => t.Destination_).Where(t => t.TableName == pTable).ToList();
            return result;
        }

        public static Asubo GetASubo(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Asuboes.FirstOrDefault(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static Boardfootrule GetBdFtRule(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Boardfootrules.FirstOrDefault(s => s.TableName == pTable && s.InputCode == pCode);
            return result;
        }

        public static Treesegment GetSegmentForCountTree(ref ProjectDbContext projectContext, int treesId)
        {
            Treesegment result = projectContext.Treesegments.FirstOrDefault(s => s.TreesId == treesId && s.TreeType == "C");
            return result;
        }

        public static Cubicfootrule GetCuFtRule(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Cubicfootrules.FirstOrDefault(s => s.TableName == pTable && s.InputCode == pCode);
            return result;
        }

        public static Sort GetSort(ref ProjectDbContext pCtx, string pTable, string pCode, string pPrimarySpecies)
        {
            var result = pCtx.Sorts.FirstOrDefault(s => s.TableName == pTable && s.PrimarySpecies == pPrimarySpecies && s.InputCode == pCode);
            if (result == null)
                result = pCtx.Sorts.FirstOrDefault(s => s.TableName == pTable && s.PrimarySpecies == "*" && s.InputCode == pCode);
            return result;
        }

        public static Grade GetGrade(ref ProjectDbContext pCtx, string pTable, string pCode, string pPrimarySpecies)
        {
            var result = pCtx.Grades.FirstOrDefault(s => s.TableName == pTable && s.PrimarySpecies == pPrimarySpecies && s.InputCode == pCode);
            if (result == null)
                result = pCtx.Grades.FirstOrDefault(s => s.TableName == pTable && s.PrimarySpecies == "*" && s.InputCode == pCode);
            return result;
        }

        public static Formfactor GetFormFactor(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Formfactors.FirstOrDefault(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static List<Price> GetPrices(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Prices.OrderBy(t => t.Species).ThenBy(t => t.ProductName).Where(t => t.TableName == pTable).ToList();
            return result;
        }

        public static List<Boardfootrule> GetBdFtRules(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Boardfootrules.OrderBy(t => t.InputCode).Where(t => t.TableName == pTable).ToList();
            return result;
        }

        public static List<Cubicfootrule> GetCuFtRules(ref ProjectDbContext pCtx, string pTable)
        {
            var result = pCtx.Cubicfootrules.OrderBy(t => t.InputCode).Where(t => t.TableName == pTable).ToList();
            return result;
        }

        public static bool DoesStatusExist(ref ProjectDbContext pCtx, string pTable, string pCode)
        {
            var result = pCtx.Treestatus.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static bool DoesGradeExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Grades.Any(s => s.TableName == pTable && s.InputCode == pCode);
            return result;
        }

        public static bool DoesSortExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Sorts.Any(s => s.TableName == pTable && s.InputCode == pCode);
            return result;
        }

        public static bool DoesT5Exist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Userdefineds.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static bool DoesDamageExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Damages.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static bool DoesVigorExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Vigors.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static bool DoesCrownPositionExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Crownpositions.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static bool DoesAspectExist(ref ProjectDbContext ctx, string pTable, string pCode)
        {
            var result = ctx.Aspects.Any(s => s.TableName == pTable && s.DisplayCode == pCode);
            return result;
        }

        public static void UpdateCountPlotsForStands(ref ProjectDbContext ctx)
        {
            List<Stand> stands = ctx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (var standItem in stands)
	        {
                UpdateCountPlotsForStand(ref ctx, standItem.StandsId);
	        }
            ctx.SaveChanges();
        }

        public static void UpdateCountPlotsForStand(ref ProjectDbContext ctx, long pStandId)
        {
            var countPlots = ctx.Trees.Where(t => t.StandsId == pStandId && t.TreeType == "C").ToList();
            // check to see if there are count plots in the stand
            if (countPlots.Count > 0)
            {
                var q = from t in ctx.Trees
                        group t by t.SpeciesAbbreviation into myGroup
                        orderby myGroup.Key
                        select new { Species = myGroup.Key, BasalArea = myGroup.Sum(t => t.BasalArea) };
                foreach (var item in q)
                {
                    string t = item.Species;
                    float ba = (float)item.BasalArea;
                }
            }
            ctx.SaveChanges();
        }

        public static void UpdatePlotsForStands(ref ProjectDbContext ctx)
        {
            try
            {
                List<Stand> stands = ctx.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
                foreach (var standItem in stands)
                {
                    UpdatePlotsForStand(ref ctx, standItem.StandsId);
                }

                ctx.SaveChanges();
            }
            catch
            {
                // ignored
            }
        }

        public static void UpdatePlotsForStand(ref ProjectDbContext ctx, long pStandsId)
        {
            try
            {
                List<Plot> tPlots = ctx.Plots.OrderBy(p => p.UserPlotNumber).Where(p => p.StandsId == pStandsId).ToList();
                foreach (Plot itemPlot in tPlots)
                {
                    List<Tree> tTrees = ctx.Trees.Where(t => t.PlotId == itemPlot.PlotsId).ToList();
                    float ba = 0;
                    float tpa = 0;
                    float netcfpa = 0;
                    float netbfpa = 0;
                    foreach (var itemTree in tTrees)
                    {
                        if (itemTree.BasalArea != null && Double.IsNaN((double)itemTree.BasalArea) == false)
                            ba += (float)itemTree.BasalArea;
                        if (itemTree.TreesPerAcre != null && Double.IsNaN((double)itemTree.TreesPerAcre) == false)
                            tpa += (float)itemTree.TreesPerAcre;
                        if (itemTree.TotalNetCubicPerAcre != null && Double.IsNaN((double)itemTree.TotalNetCubicPerAcre) == false)
                            netcfpa += (float)itemTree.TotalNetCubicPerAcre;
                        if (itemTree.TotalNetScribnerPerAcre != null && Double.IsNaN((double)itemTree.TotalNetScribnerPerAcre) == false)
                            netbfpa += (float)itemTree.TotalNetScribnerPerAcre;
                    }
                    itemPlot.BasalAreaAcre = ba;
                    itemPlot.TreesPerAcre = tpa;
                    itemPlot.NetCfAcre = netcfpa;
                    itemPlot.NetBfAcre = netbfpa;
                    if (tTrees.Count > 0)
                    {
                        if (tTrees[0].Dbh == 4.0)
                            itemPlot.BafInAt = "DBH";
                        else
                            itemPlot.BafInAt = "FP";
                    }
                }

                ctx.SaveChanges();
            }
            catch
            {
                // ignored
            }
        }

        public static short GetAge(ref ProjectDbContext ctx, Stand pStand, string p)
        {
            if (pStand != null)
            {
                StandAge rec = ctx.StandAges.FirstOrDefault(s => s.StandsId == pStand.StandsId && s.AgeCode == Convert.ToInt16(p));
                if (rec != null)
                    return rec.Age;
                else
                    return 50;
            }
            else
                return 50;
            //    switch (p)
            //    {
            //        case "1":
            //            if (pStand.Age1 != null)
            //                return (short) pStand.Age1;
            //            break;
            //        case "2":
            //            if (pStand.Age2 != null)
            //                return (short) pStand.Age2;
            //            break;
            //        case "3":
            //            if (pStand.Age3 != null) return (short) pStand.Age3;
            //            break;
            //        case "4":
            //            if (pStand.Age4 != null) return (short) pStand.Age4;
            //            break;
            //        case "5":
            //            if (pStand.Age5 != null) return (short) pStand.Age5;
            //            break;
            //        case "6":
            //            if (pStand.Age6 != null) return (short) pStand.Age6;
            //            break;
            //        case "7":
            //            if (pStand.Age7 != null) return (short) pStand.Age7;
            //            break;
            //        case "8":
            //            if (pStand.Age8 != null) return (short) pStand.Age8;
            //            break;
            //        case "9":
            //            if (pStand.Age9 != null) return (short) pStand.Age9;
            //            break;
            //        default:
            //            return 50;
            //    }
            //}
            //return 50;
        }

        public static short GetAgeCode(ref ProjectDbContext ctx, Stand pStand, short p)
        {
            if (pStand != null)
            {
                StandAge rec = ctx.StandAges.FirstOrDefault(s => s.StandsId == pStand.StandsId && s.Age == p);
                if (rec != null)
                    return rec.AgeCode;
                else
                    return 1;
                //if (pStand.Age1 == p)
                //    return 1;
                //if (pStand.Age2 == p)
                //    return 2;
                //if (pStand.Age3 == p)
                //    return 3;
                //if (pStand.Age4 == p)
                //    return 4;
                //if (pStand.Age5 == p)
                //    return 5;
                //if (pStand.Age6 == p)
                //    return 6;
                //if (pStand.Age7 == p)
                //    return 7;
                //if (pStand.Age8 == p)
                //    return 8;
                //if (pStand.Age9 == p)
                //    return 9;
            }
            return 1;
        }

        public static void UpdateTotalsForStand(ref ProjectDbContext ctx, ref Project currentProject, ref Stand pStand)
        {
            double dBaPerAcre = 0;
            double dLogsPa = 0;
            double dTmpBa = 0;
            double dTmp2 = 0;
            double dTmpTpa = 0;
            double dTotalBdFtGross = 0;
            double dTotalBdFtNet = 0;
            double dTotalCuFtGross = 0;
            double dTotalCuFtNet = 0;

            double dTotalBdFtGrossPerAcre = 0;
            double dTotalBdFtNetPerAcre = 0;
            double dTotalCuFtGrossPerAcre = 0;
            double dTotalCuFtNetPerAcre = 0;

            double dTotalCCFPerAcre = 0;
            double dTotalMBFPerAcre = 0;

            double dTotalCuFtMerch = 0;
            double dTons = 0;
            double dTonPerAcre = 0;

            double dTotalCcf = 0;
            double dTotalMbf = 0;

            int iAvgTotalHt = 0;
            double dTotalHt = 0;
            double dTrees = 0;

            Stand sRec = pStand;
            var q = (from p in ctx.Plots
                     where p.StandsId == sRec.StandsId
                     group p by p.UserPlotNumber into g
                     select g);
            sRec.Plots = q.Count();

            List<Tree> coll = ctx.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == sRec.StandsId).ToList();
            foreach (var item in coll)
            {
                if (item.BasalArea > 0)
                    dTmpBa += (double)item.BasalArea;
                if (item.TreesPerAcre > 0)
                    dTmpTpa += (double)item.TreesPerAcre;
                if (item.LogsPerAcre > 0)
                    dLogsPa += (double)item.LogsPerAcre;
                if (item.TotalHeight > 0)
                {
                    dTrees++;
                    dTotalHt += (double)item.TotalHeight;
                }

                if (item.TotalBdFtGrossVolume != null && item.TreesPerAcre != null)
                    dTotalBdFtGross += (double)item.TotalBdFtGrossVolume * (double)item.TreesPerAcre;
                if (item.TotalBdFtNetVolume != null && item.TreesPerAcre != null)
                    dTotalBdFtNet += (double)item.TotalBdFtNetVolume * (double)item.TreesPerAcre;
                if (item.TotalCuFtGrossVolume != null && item.TreesPerAcre != null)
                    dTotalCuFtGross += (double)item.TotalCuFtGrossVolume * (double)item.TreesPerAcre;
                if (item.TotalCuFtNetVolume != null && item.TreesPerAcre != null)
                    dTotalCuFtNet += (double)item.TotalCuFtNetVolume * (double)item.TreesPerAcre;

                if (item.TotalGrossCubicPerAcre != null)
                    dTotalCuFtGrossPerAcre += (double)item.TotalGrossCubicPerAcre;
                if (item.TotalNetCubicPerAcre != null)
                    dTotalCuFtNetPerAcre += (double)item.TotalNetCubicPerAcre;
                if (item.TotalGrossScribnerPerAcre != null)
                    dTotalBdFtGrossPerAcre += (double)item.TotalGrossScribnerPerAcre;
                if (item.TotalNetScribnerPerAcre != null)
                    dTotalBdFtNetPerAcre += (double)item.TotalNetScribnerPerAcre;

                if (item.TotalCcf != null)
                    dTotalCcf += (double)item.TotalCcf;
                if (item.TotalMbf != null)
                    dTotalMbf += (double)item.TotalMbf;
            }

            if (dTotalHt > 0 && dTrees > 0)
                sRec.AvgTotalHeight = (short)(dTotalHt / dTrees);
            if (dLogsPa > 0 && sRec.Plots > 0)
                sRec.LogsPerAcre = dLogsPa / (double)sRec.Plots;
            else
                sRec.LogsPerAcre = 0;
            if (dTmpTpa > 0 && sRec.Plots > 0)
                sRec.TreesPerAcre = dTmpTpa / (double)sRec.Plots;
            else
                sRec.TreesPerAcre = 0;
            if (dTmpBa > 0 && sRec.Plots > 0)
                sRec.BasalAreaPerAcre = dTmpBa / (double)sRec.Plots;
            else
                sRec.BasalAreaPerAcre = 0;
            if (sRec.BasalAreaPerAcre != null && sRec.TreesPerAcre != null)
            {
                if (sRec.BasalAreaPerAcre > 0 && sRec.TreesPerAcre > 0)
                    sRec.QmDbh = Math.Sqrt((double)sRec.BasalAreaPerAcre / ((double)sRec.TreesPerAcre * 0.005454154));
            }

            if (dTotalBdFtGross > 0)
                sRec.TotalBdFtGross = dTotalBdFtGross / (double)sRec.Plots;
            else
                sRec.TotalBdFtGross = 0;
            if (dTotalBdFtNet > 0)
                sRec.TotalBdFtNet = dTotalBdFtNet / (double)sRec.Plots;
            else
                sRec.TotalBdFtNet = 0;

            if (dTotalCuFtGross > 0)
                sRec.TotalCuFtGross = dTotalCuFtGross / (double)sRec.Plots;
            else
                sRec.TotalCuFtGross = 0;
            if (dTotalCuFtNet > 0)
                sRec.TotalCuFtNet = dTotalCuFtNet / (double)sRec.Plots;
            else
                sRec.TotalCuFtNet = 0;

            if (dTotalCuFtMerch > 0)
                sRec.TotalCuFtMerch = dTotalCuFtMerch;
            else
                sRec.TotalCuFtMerch = 0;

            if (sRec.TotalCuFtNet > 0 && sRec.NetGeographicAcres > 0)
                sRec.TotalCcf = ((sRec.TotalCuFtNet * (double)sRec.NetGeographicAcres)) / 100;
            else
                sRec.TotalCcf = 0;
            if (sRec.TotalBdFtNet > 0 && sRec.NetGeographicAcres > 0)
                sRec.TotalMbf = ((sRec.TotalBdFtNet * (double)sRec.NetGeographicAcres)) / 1000;
            else
                sRec.TotalMbf = 0;

            if (sRec.TotalCcf > 0)
                sRec.CcfPerAcre = sRec.TotalCcf / sRec.NetGeographicAcres;
            if (sRec.TotalMbf > 0)
                sRec.MbfPerAcre = sRec.TotalMbf / sRec.NetGeographicAcres;

            // Major Species
            var q1 = (from s in ctx.Treesegments
                      where s.StandsId == sRec.StandsId
                      group s by s.Species into g
                      select new
                      {
                          tSpecies = g.Key,
                          TotalGross = g.Sum(s => s.ScribnerGrossVolume)
                      }).OrderByDescending(x => x.TotalGross);
            int f = 0;
            foreach (var item in q1)
            {
                if (f == 0)
                {
                    sRec.MajorSpecies = item.tSpecies;
                    f = 1;
                }
            }

            if (sRec.MajorSpecies == null || string.IsNullOrEmpty(sRec.MajorSpecies.Trim()))
            {
                var q2 = (from t in ctx.Trees
                          where t.StandsId == sRec.StandsId
                          group t by t.SpeciesAbbreviation into g
                          select new
                          {
                              tSpecies = g.Key,
                              TreesPerAcre = g.Sum(s => s.TreesPerAcre)
                          }).OrderByDescending(x => x.TreesPerAcre);
                int f2 = 0;
                foreach (var item in q2)
                {
                    if (f2 == 0)
                    {
                        sRec.MajorSpecies = item.tSpecies;
                        f2 = 1;
                    }
                }
            }

            List<DefReportDomPri> dpColl = ctx.DefReportDomPris.OrderBy(t => t.Priority).ToList();
            IList<Tree> cc = null; 
            if (dpColl.Count > 0)
            {
                switch (dpColl[0].Item)
                {
                    case "Trees per Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    //case "Normality":
                    //    cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                    //    break;
                    case "Net Cubic Feet / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetCubicPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    case "Net Board Feet / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    case "Basal Area / Acre":
                        cc = ctx.Trees.OrderByDescending(t => t.BasalArea).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                    default:
                        cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
                        break;
                }
            }
            else
            {
                cc = ctx.Trees.OrderByDescending(t => t.TotalNetScribnerPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
            }
            //IList<Tree> cc = ctx.Trees.OrderByDescending(t => t.TreesPerAcre).Where(t => t.StandsId == sRec.StandsId).ToList();
            if (cc.Count > 0)
                sRec.MajorAge = GetAge(ref ctx, sRec, cc[0].AgeCode.ToString());

            if (sRec.MajorSpecies != null && !string.IsNullOrEmpty(sRec.MajorSpecies.Trim()))
            {
                Species spc = GetSpecieByAbbrev(ref ctx, currentProject.SpeciesTableName, sRec.MajorSpecies);
                sRec.TonsPerAcre = sRec.CcfPerAcre * spc.Lbs / 2000;
            }

            // SiteIndex
            if (sRec.Species11.Trim() == sRec.MajorSpecies || sRec.Species12.Trim() == sRec.MajorSpecies || sRec.Species13.Trim() == sRec.MajorSpecies)
                sRec.SiteIndex = sRec.SiteIndex1;
            else
                if (sRec.Species21.Trim() == sRec.MajorSpecies || sRec.Species22.Trim() == sRec.MajorSpecies || sRec.Species23.Trim() == sRec.MajorSpecies)
                    sRec.SiteIndex = sRec.SiteIndex2;
                else
                    if (sRec.Species31.Trim() == sRec.MajorSpecies || sRec.Species32.Trim() == sRec.MajorSpecies || sRec.Species33.Trim() == sRec.MajorSpecies)
                        sRec.SiteIndex = sRec.SiteIndex3;
                    else
                        if (sRec.Species41.Trim() == sRec.MajorSpecies || sRec.Species42.Trim() == sRec.MajorSpecies || sRec.Species43.Trim() == sRec.MajorSpecies)
                            sRec.SiteIndex = sRec.SiteIndex4;
        }

        public static Stand GetStand(string pDataSource, string sTract, string sStand, string sTownship, string sRange, string sSection)
        {
            Stand result;
            using (var ctx = new ProjectDbContext("Data Source = " + pDataSource))
            {
                result = ctx.Stands.FirstOrDefault(s => s.TractName == sTract && s.StandName == sStand && s.Township == sTownship && s.Range == sRange && s.Section == sSection);
            }
            return result;
        }

        public static Stand GetStand(ref ProjectDbContext ctx, string sTract, string sStand, string sTownship, string sRange, string sSection)
        {
            Stand result = ctx.Stands.FirstOrDefault(s => s.TractName == sTract && s.StandName == sStand && s.Township == sTownship && s.Range == sRange && s.Section == sSection);
            return result;
        }

        public static List<string> GetCrownPositionTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Crownpositions
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        //public static List<string> GetCrownRatioTableNames(ref ProjectDbContext ctx)
        //{
        //    List<string> varList = new List<string>();
        //    var result = (from s in pCtx.CROWNRATIO
        //                  group s by s.TableName into myGroup
        //                  orderby myGroup.Key
        //                  select new { TableName = myGroup.Key }).ToList();
        //    foreach (var item in result)
        //        varList.Add(item.TableName);
        //    return varList;
        //}

        public static List<string> GetVigorTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Vigors
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetDamageTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Damages
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetUserDefinedTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Userdefineds
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetAspectTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Aspects
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetComponentTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Components
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetEnvironmentTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Environments
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetFormTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Formfactors
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetHarvestTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Harvestsystems
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetHistoryPlanningTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.HistoryPlannings
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetRoadTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Roads
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetSeedZoneTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Seedzones
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetSlashTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Slashdistributions
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetSoilTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Soils
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetTreeStatusTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Treestatus
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetStreamTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Streams
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetTreatmentTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Treatments
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetTreeSourceTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Treesources
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetVegetationTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Vegetations
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetWoodTypeTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Woodtypes
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetNonStockedTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Nonstockeds
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static List<string> GetNonTimberedTableNames(ref ProjectDbContext pCtx)
        {
            List<string> varList = new List<string>();
            var result = (from s in pCtx.Nontimbereds
                          group s by s.TableName into myGroup
                          orderby myGroup.Key
                          select new { TableName = myGroup.Key }).ToList();
            foreach (var item in result)
                varList.Add(item.TableName);
            return varList;
        }

        public static double? GetLogsPerAcreFromTree(ref ProjectDbContext projectContext, int? pId)
        {
            double? result;
            Tree rec = projectContext.Trees.FirstOrDefault(t => t.TreesId == pId);
            if (rec != null)
                result = rec.LogsPerAcre;
            else
                result = 0;
            return result;
        }

        public static double? GetTreesPerAcreFromTree(ref ProjectDbContext projectContext, int? pId)
        {
            double? result;
            Tree rec = projectContext.Trees.FirstOrDefault(t => t.TreesId == pId);
            if (rec != null)
                result = rec.TreesPerAcre;
            else
                result = 0;
            return result;
        }

        public static Pole GetPoleRecord(ref ProjectDbContext projectContext, string currentProjectPolesTableName, string tSpecies)
        {
            List<Pole> poles = projectContext.Poles.Where(p => p.TableName == currentProjectPolesTableName).ToList();
            Pole result = null;
            foreach (var item in poles)
            {
                if (item.SpeciesCodes.Contains(tSpecies))
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public static Piling GetPilingRecord(ref ProjectDbContext projectContext, string currentProjectPilingTableName, string tSpecies)
        {
            List<Piling> pilings = projectContext.Pilings.Where(p => p.TableName == currentProjectPilingTableName).ToList();
            Piling result = null;
            foreach (var item in pilings)
            {
                if (item.Species.Contains(tSpecies))
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public static string GetPoleClass(Pole poleRec, double rptPoleTop, double rptPoleButt)
        {
            if (poleRec.H6Min != null)
            {
                if (rptPoleTop >= (double) poleRec.H6Min)
                    return "H6";
            }
            if (poleRec.H5Min != null)
            {
                if (rptPoleTop >= (double)poleRec.H5Min)
                    return "H5";
            }
            if (poleRec.H4Min != null)
            {
                if (rptPoleTop >= (double)poleRec.H4Min)
                    return "H4";
            }
            if (poleRec.H3Min != null)
            {
                if (rptPoleTop >= (double)poleRec.H3Min)
                    return "H3";
            }
            if (poleRec.H2Min != null)
            {
                if (rptPoleTop >= (double)poleRec.H2Min)
                    return "H2";
            }
            if (poleRec.H1Min != null)
            {
                if (rptPoleTop >= (double)poleRec.H1Min)
                    return "H1";
            }
            if (poleRec.C1Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C1Min)
                    return "1";
            }
            if (poleRec.C2Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C2Min)
                    return "2";
            }
            if (poleRec.C3Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C3Min)
                    return "3";
            }
            if (poleRec.C4Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C4Min)
                    return "4";
            }
            if (poleRec.C5Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C5Min)
                    return "5";
            }
            if (poleRec.C6Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C6Min)
                    return "6";
            }
            if (poleRec.C7Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C7Min)
                    return "7";
            }
            if (poleRec.C8Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C8Min)
                    return "8";
            }
            if (poleRec.C9Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C9Min)
                    return "9";
            }
            if (poleRec.C10Min != null)
            {
                if (rptPoleTop >= (double)poleRec.C10Min)
                    return "10";
            }
            return string.Empty;
        }

        public static double GetPolePrice(ref ProjectDbContext projectContext, long poleRecPolesId, string rptPoleClass, double rptPoleLen)
        {
            PolesDtl rec = projectContext.PolesDtls.FirstOrDefault(p => p.Length == (int) rptPoleLen);
            if (rec != null)
            {
                switch (rptPoleClass)
                {
                    case "H6":
                        if (rec.H6 != null)
                            return (double) rec.H6;
                        else
                            return 0.0;
                        break;
                    case "H5":
                        if (rec.H5 != null)
                            return (double) rec.H5;
                        else
                            return 0.0;
                        break;
                    case "H4":
                        if (rec.H4 != null)
                            return (double) rec.H4;
                        else
                            return 0.0;
                        break;
                    case "H3":
                        if (rec.H3 != null)
                            return (double) rec.H3;
                        else
                            return 0.0;
                        break;
                    case "H2":
                        if (rec.H2 != null)
                            return (double) rec.H2;
                        else
                            return 0.0;
                        break;
                    case "H1":
                        if (rec.H1 != null)
                            return (double) rec.H1;
                        else
                            return 0.0;
                        break;
                    case "1":
                        if (rec.C1 != null)
                            return (double) rec.C1;
                        else
                            return 0.0;
                        break;
                    case "2":
                        if (rec.C2 != null)
                            return (double) rec.C2;
                        else
                            return 0.0;
                        break;
                    case "3":
                        if (rec.C3 != null)
                            return (double) rec.C3;
                        else
                            return 0.0;
                        break;
                    case "4":
                        if (rec.C4 != null)
                            return (double) rec.C4;
                        else
                            return 0.0;
                        break;
                    case "5":
                        if (rec.C5 != null)
                            return (double) rec.C5;
                        else
                            return 0.0;
                        break;
                    case "6":
                        if (rec.C6 != null)
                            return (double) rec.C6;
                        else
                            return 0.0;
                        break;
                    case "7":
                        if (rec.C7 != null)
                            return (double) rec.C7;
                        else
                            return 0.0;
                        break;
                    case "8":
                        if (rec.C8 != null)
                            return (double) rec.C8;
                        else
                            return 0.0;
                        break;
                    case "9":
                        if (rec.C9 != null)
                            return (double) rec.C9;
                        else
                            return 0.0;
                        break;
                    case "10":
                        if (rec.C10 != null)
                            return (double) rec.C10;
                        else
                            return 0.0;
                        break;
                    default:
                        return 0.0;
                }
            }
            else
            {
                return 0;
            }
        }

        public static string GetPilingClass(ref ProjectDbContext projectContext, double len, double top, double butt)
        {
            PilingDimension piling = projectContext.PilingDimensions.Where(p => p.Length <= len).OrderByDescending(p => p.Length).FirstOrDefault();
            if (piling == null)         // fields are non-nullable
                return string.Empty;
            // got back closest record for the length; check whether it matches classA or falls back to classB
            return ((butt >= piling.ClassAMinButt) && (top >= piling.ClassAMinTop)) ? "A" : "B";
        }



        public static double GetPilingPrice(ref ProjectDbContext projectContext, double rptTop, double rptButt, double rptLen)
        {
            PilingDimension dimRec = projectContext.PilingDimensions.FirstOrDefault(p => p.Length == (int)rptLen);
            PilingPrice priceRec = projectContext.PilingPrices.FirstOrDefault(p => p.Length == (int)rptLen);
            double result = 0;
            string classType = string.Empty;

            if (dimRec != null)
            {
                if (rptTop >= dimRec.ClassAMinTop && rptButt >= dimRec.ClassAMinButt)
                {
                    result = priceRec.ClassAPer;
                }
                else
                    if (rptTop >= dimRec.ClassBMinTop && rptButt >= dimRec.ClassBMinButt)
                    {
                        result = priceRec.ClassBPer;
                    }
                    else
                    {
                        result = 0;
                    }
            }
            else
            {
                result = 0;
            }
            return result;
        }

        public static DataTable GetDataTableFromQuery(ref ProjectDbContext projectContext, string pSql)
        {
            SqlCeConnection cn = (SqlCeConnection)projectContext.Database.Connection;
            DataTable result = new DataTable();
            SqlCeCommand cmd = new SqlCeCommand(pSql, cn);
            cmd.CommandType = CommandType.Text;
            SqlCeDataAdapter da = new SqlCeDataAdapter(cmd);
            da.Fill(result);
            return result;
        }

        public static void DeleteCountWithDiaTable(ref ProjectDbContext pCtx)
        {
            pCtx.CountWithDbhs.RemoveRange(pCtx.CountWithDbhs);
            pCtx.SaveChanges();
        }

        public static void DeleteCountWithDiaTable(ref ProjectDbContext pCtx, int pId)
        {
            try
            {
                var toDelete = pCtx.CountWithDbhs.Where(s => s.StandsId == pId).Select(a => a);
                pCtx.CountWithDbhs.RemoveRange(toDelete);
                pCtx.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public static void BuildCountWithDiaTable(ref ProjectDbContext projectContext, List<Stand> sColl)
        {
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;

            foreach (Stand standItem in sColl)
                BuildCountWithDiaTable(ref projectContext, standItem.StandsId);
        }

        public static void BuildCountWithDiaTable(ref ProjectDbContext projectContext, int pId)
        {
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;

            var q = (from s in projectContext.Trees
                     where s.StandsId == pId && s.TreeType == "D" && s.Dbh > 0
                     orderby s.PfType, s.SpeciesAbbreviation, s.TreeStatusDisplayCode, s.Dbh, s.TotalHeight
                     select new
                     {
                         StandsID = s.StandsId,
                         PFType = s.PfType,
                         SpeciesAbbreviation = s.SpeciesAbbreviation.Trim(),
                         Status = s.TreeStatusDisplayCode.Trim(),
                         Dbh = s.Dbh,
                         ReportedFormFactor = s.ReportedFormFactor,
                         TotalHeight = s.TotalHeight,
                         TreesPerAcre = s.TreesPerAcre,
                         BasalArea = s.BasalArea,
                         TotalNetCubicPerAcre = s.TotalNetCubicPerAcre,
                         TotalNetScribnerPerAcre = s.TotalNetScribnerPerAcre,
                         CubicGrossVolume = s.TotalCuFtGrossVolume,
                         ScribnerGrossVolume = s.TotalBdFtGrossVolume,
                         CubicNetVolume = s.TotalCuFtNetVolume,
                         ScribnerNetVolume = s.TotalBdFtNetVolume,
                         LogsPerAcre = s.LogsPerAcre
                     }).ToList();
            //string saveSpecies = string.Empty;
            //string saveStatus = string.Empty;
            string saveKey = string.Empty;
            string currKey = string.Empty;
            int instance = 0;
            long id = -1;

            foreach (var treeItem in q)
            {
                // write record
                CountWithDbh rec = new CountWithDbh();
                if (id == -1)
                    id = (long)treeItem.StandsID;
                if (string.IsNullOrEmpty(saveKey))
                {
                    //saveSpecies = treeItem.SpeciesAbbreviation;
                    //saveStatus = treeItem.Status.Trim();
                    saveKey = treeItem.PFType.Trim() + treeItem.SpeciesAbbreviation.Trim() + treeItem.Status.Trim();
                }
                currKey = treeItem.PFType.Trim() + treeItem.SpeciesAbbreviation.Trim() + treeItem.Status.Trim();
                if (saveKey != currKey)
                {
                    instance = 0;
                    //saveSpecies = treeItem.SpeciesAbbreviation;
                    //saveStatus = treeItem.Status.Trim();
                    saveKey = currKey;
                }
                rec.StandsId = (int)treeItem.StandsID;
                rec.PfType = treeItem.PFType;
                rec.Dbh = (double)treeItem.Dbh;
                rec.Species = treeItem.SpeciesAbbreviation.Trim();
                rec.Status = treeItem.Status.Trim();
                rec.Instance = instance++;
                rec.Ff = (double)treeItem.ReportedFormFactor;
                rec.TotalHt = (double)treeItem.TotalHeight;
                rec.GrossCfTree = (double)treeItem.CubicGrossVolume;
                rec.NetCfTree = (double)treeItem.CubicNetVolume;
                rec.GrossBfTree = (double)treeItem.ScribnerGrossVolume;
                rec.NetBfTree = (double)treeItem.ScribnerNetVolume;
                rec.VBarBdFt = Math.Round((double)treeItem.TotalNetScribnerPerAcre / (double)treeItem.BasalArea);
                rec.VBarCuFt = Math.Round((double)treeItem.TotalNetCubicPerAcre / (double)treeItem.BasalArea);
                rec.LogsAcre = Math.Round((double)treeItem.LogsPerAcre, 1);
                rec.CalcFf = 0;
                rec.CalcTotalHt = 0;
                rec.GrossCalcCfTree = 0;
                rec.GrossCalcBfTree = 0;
                rec.NetCalcCfTree = 0;
                rec.NetCalcBfTree = 0;
                rec.CalcVBarBdFt = 0;
                rec.CalcVBarCuFt = 0;
                projectContext.CountWithDbhs.Add(rec);
            }
            projectContext.SaveChanges();

            List<string> speciesColl = new List<string>();
            var result = (from t in projectContext.CountWithDbhs
                          where t.StandsId == pId
                          group t by new { t.PfType, t.Species, t.Status, } into myGroup
                          orderby myGroup.Key.PfType, myGroup.Key.Species, myGroup.Key.Status
                          select new { PFType = myGroup.Key.PfType, Species = myGroup.Key.Species, Status = myGroup.Key.Status }).ToList();
            foreach (var item in result)
                speciesColl.Add(string.Format("{0}-{1}-{2}", item.PFType, item.Species, item.Status));

            foreach (var speciesItem in result)
            {
                List<DataPoint> ff = new List<DataPoint>();
                List<DataPoint> totHt = new List<DataPoint>();
                List<DataPoint> GrossCfTree = new List<DataPoint>();
                List<DataPoint> GrossBfTree = new List<DataPoint>();
                List<DataPoint> NetCfTree = new List<DataPoint>();
                List<DataPoint> NetBfTree = new List<DataPoint>();
                List<DataPoint> vbarBdFt = new List<DataPoint>();
                List<DataPoint> vbarCuFt = new List<DataPoint>();
                List<DataPoint> logsAc = new List<DataPoint>();

                double ffSlope = 0;
                double ffIntercept = 0;
                double totHtSlope = 0;
                double totHtIntercept = 0;
                double GrossCfTreeSlope = 0;
                double GrossCfTreeIntercept = 0;
                double NetCfTreeSlope = 0;
                double NetCfTreeIntercept = 0;
                double GrossBfTreeSlope = 0;
                double GrossBfTreeIntercept = 0;
                double NetBfTreeSlope = 0;
                double NetBfTreeIntercept = 0;
                double vbarBdFtSlope = 0;
                double vbarBdFtIntercept = 0;
                double vbarCuFtSlope = 0;
                double vbarCuFtIntercept = 0;
                double logsAcSlope = 0;
                double logsAcIntercept = 0;

                double ffPrev = 0;
                double totHtPrev = 0;
                double GrossCfTreePrev = 0;
                double NetCfTreePrev = 0;
                double GrossBfTreePrev = 0;
                double NetBfTreePrev = 0;
                double vbarBdFtPrev = 0;
                double vbarCuFtPrev = 0;
                double logsAcPrev = 0;

                List<CountWithDbh> spcTbl = projectContext.CountWithDbhs.OrderBy(t => t.Instance).
                    Where(t => t.StandsId == pId && t.PfType == speciesItem.PFType && t.Species == speciesItem.Species && t.Status == speciesItem.Status).ToList();
                foreach (var item in spcTbl)
                {
                    AddToDataPoint(ref ff, (double)item.Ff, item.Instance);
                    AddToDataPoint(ref totHt, item.TotalHt, item.Instance);
                    AddToDataPoint(ref GrossCfTree, item.GrossCfTree, item.Instance);
                    AddToDataPoint(ref NetCfTree, item.NetCfTree, item.Instance);
                    AddToDataPoint(ref GrossBfTree, (double)item.GrossBfTree, item.Instance);
                    AddToDataPoint(ref NetBfTree, (double)item.NetBfTree, item.Instance);
                    AddToDataPoint(ref vbarBdFt, item.VBarBdFt, item.Instance);
                    AddToDataPoint(ref vbarCuFt, item.VBarCuFt, item.Instance);
                    AddToDataPoint(ref logsAc, item.LogsAcre, item.Instance);
                }
                CalcValues(ff, out ffSlope, out ffIntercept);
                CalcValues(totHt, out totHtSlope, out totHtIntercept);
                CalcValues(GrossCfTree, out GrossCfTreeSlope, out GrossCfTreeIntercept);
                CalcValues(NetCfTree, out NetCfTreeSlope, out NetCfTreeIntercept);
                CalcValues(GrossBfTree, out GrossBfTreeSlope, out GrossBfTreeIntercept);
                CalcValues(NetBfTree, out NetBfTreeSlope, out NetBfTreeIntercept);
                CalcValues(vbarBdFt, out vbarBdFtSlope, out vbarBdFtIntercept);
                CalcValues(vbarCuFt, out vbarCuFtSlope, out vbarCuFtIntercept);
                CalcValues(logsAc, out logsAcSlope, out logsAcIntercept);

                bool bFirst = true;
                foreach (var item in spcTbl)
                {
                    if (spcTbl.Count > 1)
                    {
                        if (bFirst)
                        {
                            item.CalcFf = ffIntercept;
                            item.CalcTotalHt = totHtIntercept;
                            item.GrossCalcCfTree = GrossCfTreeIntercept;
                            item.NetCalcCfTree = NetCfTreeIntercept;
                            item.GrossCalcBfTree = GrossCfTreeIntercept;
                            item.NetCalcBfTree = NetBfTreeIntercept;
                            item.CalcVBarBdFt = vbarBdFtIntercept;
                            item.CalcVBarCuFt = vbarCuFtIntercept;
                            item.CalcLogsAcre = logsAcIntercept;

                            ffPrev = ffIntercept;
                            totHtPrev = totHtIntercept;
                            GrossCfTreePrev = GrossCfTreeIntercept;
                            NetCfTreePrev = NetCfTreeIntercept;
                            GrossBfTreePrev = GrossBfTreeIntercept;
                            NetBfTreePrev = NetBfTreeIntercept;
                            vbarBdFtPrev = vbarBdFtIntercept;
                            vbarCuFtPrev = vbarCuFtIntercept;
                            logsAcPrev = logsAcIntercept;
                            bFirst = false;
                        }
                        else
                        {
                            item.CalcFf = ffPrev + ffSlope;
                            item.CalcTotalHt = totHtPrev + totHtSlope;
                            item.GrossCalcCfTree = GrossCfTreePrev + GrossCfTreeSlope;
                            item.NetCalcCfTree = NetCfTreePrev + NetCfTreeSlope;
                            item.GrossCalcBfTree = GrossBfTreePrev + GrossBfTreeSlope;
                            item.NetCalcBfTree = NetBfTreePrev + NetBfTreeSlope;
                            item.CalcVBarBdFt = vbarBdFtPrev + vbarBdFtSlope;
                            item.CalcVBarCuFt = vbarCuFtPrev + vbarCuFtSlope;
                            item.CalcLogsAcre = logsAcPrev + logsAcSlope;

                            ffPrev = (double)item.CalcFf;
                            totHtPrev = item.CalcTotalHt;
                            GrossCfTreePrev = item.GrossCalcCfTree;
                            NetCfTreePrev = item.NetCalcCfTree;
                            GrossBfTreePrev = (double)item.GrossCalcBfTree;
                            NetBfTreePrev = (double)item.NetCalcBfTree;
                            vbarBdFtPrev = item.CalcVBarBdFt;
                            vbarCuFtPrev = item.CalcVBarCuFt;
                            logsAcPrev = item.CalcLogsAcre;
                        }
                    }
                    else
                    {
                        item.CalcFf = item.Ff;
                        item.CalcTotalHt = item.TotalHt;
                        item.GrossCalcCfTree = item.GrossCfTree;
                        item.NetCalcCfTree = item.NetCfTree;
                        item.GrossCalcBfTree = item.GrossBfTree;
                        item.NetCalcBfTree = item.NetBfTree;
                        item.CalcVBarBdFt = item.VBarBdFt;
                        item.CalcVBarCuFt = item.VBarCuFt;
                        item.CalcLogsAcre = item.LogsAcre;
                    }
                }
                projectContext.SaveChanges();
            }
        }

        public static void AddToDataPoint(ref List<DataPoint> pointList, double pY, double pX)
        {
            DataPoint point = new DataPoint();
            point.Y = pY;
            point.X = pX;
            pointList.Add(point);
        }

        public class DataPoint
        {
            public double X { get; set; }
            public double Y { get; set; }
        }

        public static double GetSlope(List<DataPoint> data)
        {
            double n = data.Count;
            double sumxy = 0, sumx = 0, sumy = 0, sumx2 = 0;
            for (int i = 0; i < data.Count; i++)
            {
                sumxy += data[i].X * data[i].Y;
                sumx += data[i].X;
                sumy += data[i].Y;
                sumx2 += data[i].X * data[i].X;
            }
            return ((sumxy - sumx * sumy / n) / (sumx2 - sumx * sumx / n));
        }

        public static double Slope(IEnumerable<DataPoint> data)
        {
            double averageX = data.Average(d => d.X);
            double averageY = data.Average(d => d.Y);

            return data.Sum(d => (d.X - averageX) * (d.Y - averageY)) / data.Sum(d => Math.Pow(d.X - averageX, 2));
        }

        public static double Intercept(IEnumerable<DataPoint> data)
        {
            double slope = Slope(data);
            return data.Average(d => d.Y) - slope * data.Average(d => d.X);
        }

        public static void CalcValues(List<DataPoint> data, out double slope, out double intercept)
        {
            double xSum = 0;
            double ySum = 0;
            double xySum = 0;
            double xSqSum = 0;
            double ySqSum = 0;

            foreach (var point in data)
            {
                var x = point.X;
                var y = point.Y;

                xSum += x;
                ySum += y;
                xySum += (x * y);
                xSqSum += (x * x);
                ySqSum += (y * y);
            }

            slope = Math.Round(((data.Count * xySum) - (xSum * ySum)) /
                         ((data.Count * xSqSum) - (xSum * xSum)), 3);

            intercept = Math.Round(((xSqSum * ySum) - (xSum * xySum)) /
                              ((data.Count * xSqSum) - (xSum * xSum)), 3);

            if (intercept < 0)
                intercept = 0;

            //var a = ((data.Count * xySum) - (xSum * ySum));
            //var b = (((data.Count * xSqSum) - (xSum * xSum)) *
            //             ((data.Count * ySqSum) - (ySum * ySum)));
            //rSquared = (a * a) / b;
        }

        public static CountWithDbh GetCountWithDbh(ref ProjectDbContext projectContext, Tree selectedTree)
        {
            List<CountWithDbh> cwdColl = projectContext.CountWithDbhs
                .Where(t => t.StandsId == (long)selectedTree.StandsId &&
                    t.PfType == selectedTree.PfType &&
                    t.Species == selectedTree.SpeciesAbbreviation && 
                    t.Status == selectedTree.TreeStatusDisplayCode && 
                    t.Dbh == selectedTree.Dbh)
                .OrderBy(t => t.Species)
                .ThenBy(t => t.Status)
                .ThenBy(t => t.Instance)
                .ToList();
            if (cwdColl.Count == 1)
            {
                return cwdColl[0];
            }
            if (cwdColl.Count > 1)
            {
                return GetCountWithDbhAvg(cwdColl);
            }
            if (cwdColl.Count == 0)
            {
                // Dbh was not found
                cwdColl = projectContext.CountWithDbhs
                    .Where(t => t.StandsId == selectedTree.StandsId &&
                        t.PfType == selectedTree.PfType &&
                        t.Species == selectedTree.SpeciesAbbreviation && 
                        t.Status == selectedTree.TreeStatusDisplayCode)
                    .OrderBy(t => t.Species)
                    .ThenBy(t => t.Status)
                    .ThenBy(t => t.Instance)
                    .ToList();
                if (cwdColl.Count == 1)
                {
                    return cwdColl[0];
                }
                else
                if (cwdColl.Count > 1)
                {
                    return GetCountWithDbhNotFound(cwdColl, selectedTree);
                }
                else
                if (cwdColl.Count == 0)
                {
                    cwdColl = projectContext.CountWithDbhs
                        .Where(t => t.StandsId == selectedTree.StandsId && t.PfType == selectedTree.PfType && t.Species == selectedTree.SpeciesAbbreviation)
                        .OrderBy(t => t.Species)
                        .ThenBy(t => t.Status)
                        .ThenBy(t => t.Instance)
                        .ToList();
                    if (cwdColl.Count == 1)
                        return cwdColl[0];
                    else
                    if (cwdColl.Count > 1)
                    {
                        return GetCountWithDbhNotFound(cwdColl, selectedTree);
                    }
                }
            }
            return null;
        }

        public static void DeleteAdjCruisePlots(ref ProjectDbContext projectContext)
        {
            projectContext.AdjCruisePlots.RemoveRange(projectContext.AdjCruisePlots);
            projectContext.SaveChanges();
        }

        public static void DeleteAdjCruisePlots(ref ProjectDbContext projectContext, long pId)
        {
            projectContext.AdjCruisePlots.RemoveRange(projectContext.AdjCruisePlots.Where(t => t.StandsId == pId));
            projectContext.SaveChanges();
        }

        public static void BuildAdjCruisePlots(ref ProjectDbContext projectContext, List<Stand> pStands)
        {
            foreach (Stand item in pStands)
            {
                BuildAdjCruisePlots(ref projectContext, item);
            }
            projectContext.SaveChanges();
        }

        public static void BuildAdjCruisePlots(ref ProjectDbContext projectContext, Stand pStand)
        {
            int saveId = -1;
            float tLogs = 0;
            string currentKey = string.Empty;
            string saveKey = string.Empty;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            string tPF = string.Empty;
            float tPFValue = 0;
            float tTreesAcre = 0, tBasalArea = 0, tLogsAcre = 0;
            float tTotalHeight = 0;
            float tBoleHeight = 0;
            float tFF = 0;
            float tGrossCuFtAcre = 0;
            float tGrossBdFtAcre = 0;
            float tGrossCuFtVolume = 0;
            float tGrossBdFtVolume = 0;
            float tNetCuFtAcre = 0;
            float tNetBdFtAcre = 0;
            float tNetCuFtVolume = 0;
            float tNetBdFtVolume = 0;
            float tNetCCF = 0;
            float tNetMBF = 0;
            float tVbarNetCuFtAcre = 0;
            float tVbarNetBdFtAcre = 0;
            Stand standMaster = null;

            //standMaster = projectContext.Stands.FirstOrDefault(s => s.StandsId == pStand.StandsId);
            List<Tree> tColl =
                projectContext.Trees.OrderBy(t => t.PfType)
                    .ThenBy(t => t.SpeciesAbbreviation)
                    .ThenBy(t => t.TreeStatusDisplayCode)
                    .Where(t => t.StandsId == pStand.StandsId && t.TreeType == "D")
                    .ToList();
            if (tColl.Count == 0)
                return;

            foreach (Tree treeItem in tColl)
            {
                if (string.IsNullOrEmpty(tSpecies))
                {
                    saveId = (int)pStand.StandsId;
                    tPF = treeItem.PfType;
                    tPFValue = Convert.ToSingle(ProjectBLL.GetPlotFactor(pStand, treeItem)); // (float)treeItem.PfValue;
                    tSpecies = treeItem.SpeciesAbbreviation;
                    tStatus = treeItem.TreeStatusDisplayCode;
                    saveKey = String.Format("{0}{1}{2}{3}", pStand.StandsId, tPF, tSpecies, tStatus);
                }
                //if (pIsCombined)
                //    currentKey = String.Format("{0}{1}{2}{3}", "*", "*", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                //else
                currentKey = String.Format("{0}{1}{2}{3}", pStand.StandsId, treeItem.PfType, treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode);
                if (saveKey != currentKey)
                {
                    // write record
                    AdjCruisePlot rec = new AdjCruisePlot();
                    float totalTpa = ProjectBLL.GetTotalTpaForSpecies(ref projectContext, pStand.StandsId, tSpecies, tStatus);
                    rec.StandsId = pStand.StandsId; // saveId;
                    rec.TractName = pStand.TractName;
                    rec.StandName = pStand.StandName;
                    rec.PfType = tPF;
                    rec.Species = tSpecies;
                    rec.Status = tStatus;
                    rec.TreeCount = (short)tLogs;
                    //rec.TreesPerAcre = tTreesAcre;
                    //rec.BasalArea = tBasalArea; // tBasalArea / pStand.Plots; // tBasalArea;
                    rec.LogsPerAcre = tLogsAcre; // tLogsAcre;
                    rec.BoleHeight = 0; // (double)tBoleHeight / (double)rec.TreesPerAcre;
                    rec.TotalHeight = 0; // (double)tTotalHeight / (double)rec.TreesPerAcre;
                    //rec.VBarNetCuFtAcre = tNetCuFtAcre / tBasalArea; // rec.NetCuFtAcre / tBasalArea;
                    //rec.VBarNetBdFtAcre = tNetBdFtAcre / tBasalArea; // rec.NetBdFtAcre / tBasalArea;
                    //rec.GrossBdFtAcre = (tBasalArea / pStand.Plots) * (tGrossBdFtAcre / tBasalArea); // tGrossBdFtAcre; // stand.Plots;
                    //rec.NetBdFtAcre = (tBasalArea / pStand.Plots) * (tNetBdFtAcre / tBasalArea); // tNetBdFtAcre; // stand.Plots;
                    //rec.GrossCuFtAcre = (tBasalArea / pStand.Plots) * (tGrossCuFtAcre / tBasalArea); // tGrossCuFtAcre; // stand.Plots;
                    //rec.NetCuFtAcre = (tBasalArea / pStand.Plots) * (tNetCuFtAcre / tBasalArea); // tNetCuFtAcre; // stand.Plots;
                    //rec.TotalCcf = 0; // tNetCCF;
                    //rec.TotalMbf = 0; // tNetMBF;
                    
                    rec.BasalArea = tBasalArea / tLogs; // tBasalArea / pStand.Plots; // tBasalArea;
                    rec.TreesPerAcre = tTreesAcre / tLogs;
                    rec.Ff = tFF / rec.TreesPerAcre;
                    rec.GrossBdFtAcre = tGrossBdFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
                    rec.NetBdFtAcre = tNetBdFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
                    rec.GrossCuFtAcre = tGrossCuFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
                    rec.NetCuFtAcre = tNetCuFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
                    rec.GrossCuFtLogs = tGrossCuFtVolume / totalTpa;
                    rec.NetCuFtLogs = tNetCuFtVolume / totalTpa;
                    rec.GrossBdFtLogs = tGrossBdFtVolume / totalTpa;
                    rec.NetBdFtLogs = tNetBdFtVolume / totalTpa;
                    
                    rec.VBarNetCuFtAcre = rec.NetCuFtAcre / rec.BasalArea; // rec.NetCuFtAcre / tBasalArea;
                    rec.VBarNetBdFtAcre = rec.NetBdFtAcre / rec.BasalArea; // rec.NetBdFtAcre / tBasalArea;
                    
                    rec.TotalCcf = tNetCCF / totalTpa;
                    rec.TotalMbf = tNetMBF / totalTpa;

                    rec.QmDbh = (double)Math.Sqrt((double)tBasalArea / ((double)rec.TreesPerAcre * 0.005454154));

                    projectContext.AdjCruisePlots.Add(rec);

                    // move fields
                    tPF = treeItem.PfType;
                    tPFValue = Convert.ToSingle(ProjectBLL.GetPlotFactor(pStand, treeItem)); // tPFValue = (float)treeItem.PfValue;
                    tSpecies = treeItem.SpeciesAbbreviation;
                    tStatus = treeItem.TreeStatusDisplayCode;
                    //if (pIsCombined)
                    //    saveKey = String.Format("{0}{1}{2}{3}", "*", "*", tSpecies, tStatus);
                    //else
                    saveKey = String.Format("{0}{1}{2}{3}", pStand.StandsId, tPF, tSpecies, tStatus);
                    saveId = (int)pStand.StandsId;
                    tFF = (((float)treeItem.ReportedFormFactor * (float)0.01) * (float)treeItem.TreesPerAcre);
                    tBoleHeight = (float)treeItem.BoleHeight * (float)treeItem.TreesPerAcre;
                    tTotalHeight = (float)treeItem.TotalHeight * (float)treeItem.TreesPerAcre;
                    tGrossCuFtAcre = (float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetCuFtAcre = (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                    tGrossBdFtAcre = (float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetBdFtAcre = (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    tBasalArea = (float)treeItem.BasalArea;
                    tTreesAcre = (float)treeItem.TreesPerAcre;
                    tLogsAcre = (float)treeItem.LogsPerAcre;
                    tGrossBdFtVolume = (float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetBdFtVolume = (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    tGrossCuFtVolume = (float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetCuFtVolume = (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                    tNetCCF = (float)treeItem.TotalCcf;
                    tNetMBF = (float)treeItem.TotalMbf;
                    //tVbarNetCuFtAcre = (float)treeItem.TotalNetCubicPerAcre / (float)treeItem.BasalArea;
                    //tVbarNetBdFtAcre = (float)treeItem.TotalNetScribnerPerAcre / (float)treeItem.BasalArea;
                    tLogs = 1;
                }
                else
                {
                    tFF += (((float)treeItem.ReportedFormFactor * (float)0.01) * (float)treeItem.TreesPerAcre);
                    tBoleHeight += (float)treeItem.BoleHeight * (float)treeItem.TreesPerAcre;
                    tTotalHeight += (float)treeItem.TotalHeight * (float)treeItem.TreesPerAcre;
                    tGrossCuFtAcre += (float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetCuFtAcre += (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                    tGrossBdFtAcre += (float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre;
                    tNetBdFtAcre += (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    tNetCCF += (float)treeItem.TotalCcf;
                    tNetMBF += (float)treeItem.TotalMbf;

                    tBasalArea += (float)treeItem.BasalArea;
                    tTreesAcre += (float)treeItem.TreesPerAcre;
                    tLogsAcre += (float)treeItem.LogsPerAcre;
                    tGrossBdFtVolume += (float)treeItem.TotalBdFtGrossVolume * (float) treeItem.TreesPerAcre;
                    tNetBdFtVolume += (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    tGrossCuFtVolume += (float)treeItem.TotalCuFtGrossVolume * (float) treeItem.TreesPerAcre;
                    tNetCuFtVolume += (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                    //tVbarNetCuFtAcre += (float)treeItem.TotalNetCubicPerAcre / (float)treeItem.BasalArea;
                    //tVbarNetBdFtAcre += (float)treeItem.TotalNetScribnerPerAcre / (float)treeItem.BasalArea;
                    tLogs += (float)treeItem.TreeCount;
                }
            }
            AdjCruisePlot rec1 = new AdjCruisePlot();
            float totalTpa1 = ProjectBLL.GetTotalTpaForSpecies(ref projectContext, pStand.StandsId, tSpecies, tStatus);
            rec1.StandsId = pStand.StandsId; // saveId;
            rec1.TractName = pStand.TractName;
            rec1.StandName = pStand.StandName;
            rec1.PfType = tPF;
            rec1.Species = tSpecies;
            rec1.Status = tStatus;
            rec1.TreeCount = (short)tLogs;
            //rec.TreesPerAcre = tTreesAcre;
            //rec.BasalArea = tBasalArea; // tBasalArea / pStand.Plots; // tBasalArea;
            rec1.LogsPerAcre = tLogsAcre; // tLogsAcre;
            rec1.BoleHeight = 0; // (double)tBoleHeight / (double)rec.TreesPerAcre;
            rec1.TotalHeight = 0; // (double)tTotalHeight / (double)rec.TreesPerAcre;
            //rec.VBarNetCuFtAcre = tNetCuFtAcre / tBasalArea; // rec.NetCuFtAcre / tBasalArea;
            //rec.VBarNetBdFtAcre = tNetBdFtAcre / tBasalArea; // rec.NetBdFtAcre / tBasalArea;
            //rec.GrossBdFtAcre = (tBasalArea / pStand.Plots) * (tGrossBdFtAcre / tBasalArea); // tGrossBdFtAcre; // stand.Plots;
            //rec.NetBdFtAcre = (tBasalArea / pStand.Plots) * (tNetBdFtAcre / tBasalArea); // tNetBdFtAcre; // stand.Plots;
            //rec.GrossCuFtAcre = (tBasalArea / pStand.Plots) * (tGrossCuFtAcre / tBasalArea); // tGrossCuFtAcre; // stand.Plots;
            //rec.NetCuFtAcre = (tBasalArea / pStand.Plots) * (tNetCuFtAcre / tBasalArea); // tNetCuFtAcre; // stand.Plots;
            //rec.TotalCcf = 0; // tNetCCF;
            //rec.TotalMbf = 0; // tNetMBF;
            rec1.BasalArea = tBasalArea / tLogs; // tBasalArea / pStand.Plots; // tBasalArea;
            rec1.TreesPerAcre = tTreesAcre / tLogs;
            rec1.Ff = tFF / rec1.TreesPerAcre;
            rec1.GrossBdFtAcre = tGrossBdFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
            rec1.NetBdFtAcre = tNetBdFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
            rec1.GrossCuFtAcre = tGrossCuFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
            rec1.NetCuFtAcre = tNetCuFtVolume / pStand.Plots; // totalTpa; // stand.Plots;
            rec1.GrossCuFtLogs = tGrossCuFtVolume / totalTpa1;
            rec1.NetCuFtLogs = tNetCuFtVolume / totalTpa1;
            rec1.GrossBdFtLogs = tGrossBdFtVolume / totalTpa1;
            rec1.NetBdFtLogs = tNetBdFtVolume / totalTpa1;

            rec1.VBarNetCuFtAcre = rec1.NetCuFtAcre / rec1.BasalArea; // rec.NetCuFtAcre / tBasalArea;
            rec1.VBarNetBdFtAcre = rec1.NetBdFtAcre / rec1.BasalArea; // rec.NetBdFtAcre / tBasalArea;

            rec1.TotalCcf = tNetCCF / totalTpa1;
            rec1.TotalMbf = tNetMBF / totalTpa1;

            rec1.QmDbh = (double)Math.Sqrt((double)tBasalArea / ((double)rec1.TreesPerAcre * 0.005454154));

            projectContext.AdjCruisePlots.Add(rec1);

            saveId = -1;
            tLogs = 0;
            currentKey = string.Empty;
            saveKey = string.Empty;
            tSpecies = string.Empty;
            tStatus = string.Empty;

            tFF = 0;
            tBoleHeight = 0;
            tTotalHeight = 0;
            tNetCuFtAcre = 0;
            tNetBdFtAcre = 0;
            tBasalArea = 0;
            tTreesAcre = 0;
            tLogsAcre = 0;
            tNetBdFtVolume = 0;
            tNetCuFtVolume = 0;
            tNetCCF = 0;
            tNetMBF = 0;
            tVbarNetCuFtAcre = 0;
            tVbarNetBdFtAcre = 0;
            projectContext.SaveChanges();
        }

        private static float GetTotalTpaForSpecies(ref ProjectDbContext projectContext, int id, string tSpecies, string tStatus)
        {
            float result = 0;
            List<Tree> coll = null;

            if (string.IsNullOrEmpty(tStatus))
                coll = projectContext.Trees.Where(t => t.StandsId == id && t.TreeType == "D" && t.SpeciesAbbreviation == tSpecies).ToList();
            else
                coll = projectContext.Trees.Where(t => t.StandsId == id && t.TreeType == "D" && t.SpeciesAbbreviation == tSpecies && t.TreeStatusDisplayCode == tStatus).ToList();
            foreach (var item in coll)
            {
                result += (float)item.TreesPerAcre;
            }
            return result;
        }

        public static int GetFormPointFromTree(ref ProjectDbContext projectContext, int standsId)
        {
            Tree result = projectContext.Trees.FirstOrDefault(t => t.StandsId == standsId);
            return (int)result.FormPoint;
        }

        public static void ResetCountRecords(ref ProjectDbContext projectContext)
        {
            List<Stand> stands = projectContext.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            List<string> countPlots = new List<string>();
            foreach (Stand standItem in stands)
            {
                List<Tree> trees = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == standItem.StandsId && t.TreeType == "C").ToList();
                foreach (Tree treeItem in trees)
                {
                    List<Treesegment> segments = projectContext.Treesegments.OrderBy(s => s.PlotNumber).ThenBy(s => s.SegmentNumber)
                        .Where(s => s.TreesId == treeItem.TreesId).ToList();
                    foreach (Treesegment segItem in segments)
                    {
                        segItem.FormFactor = 0;
                        segItem.TotalHeight = 0;
                        segItem.TreeBasalArea = 0;
                        segItem.TreeTreesPerAcre = 0;
                        segItem.CubicGrossVolume = 0;
                        segItem.CubicNetVolume = 0;
                        segItem.ScribnerGrossVolume = 0;
                        segItem.ScribnerNetVolume = 0;
                        segItem.CubicNetVolumePerAce = 0;
                        segItem.ScribnerNetVolumePerAce = 0;
                        segItem.Ccf = 0;
                        segItem.Mbf = 0;
                    }
                    countPlots.Add(treeItem.PlotNumber);
                    treeItem.BasalArea = 0;
                    treeItem.TreesPerAcre = 0;
                    treeItem.TotalCuFtGrossVolume = 0;
                    treeItem.TotalCuFtNetVolume = 0;
                    treeItem.TotalBdFtGrossVolume = 0;
                    treeItem.TotalBdFtNetVolume = 0;
                    treeItem.TotalGrossCubicPerAcre = 0;
                    treeItem.TotalNetCubicPerAcre = 0;
                    treeItem.TotalGrossScribnerPerAcre = 0;
                    treeItem.TotalNetScribnerPerAcre = 0;
                    treeItem.TotalMbf = 0;
                    treeItem.TotalCcf = 0;
                }
                foreach (var itemPlot in countPlots)
                {
                    Plot rec = projectContext.Plots.FirstOrDefault(p => p.StandsId == standItem.StandsId && p.UserPlotNumber == itemPlot);
                    if (rec != null)
                    {
                        rec.BasalAreaAcre = 0;
                        rec.NetBfAcre = 0;
                        rec.NetCfAcre = 0;
                        rec.TreesPerAcre = 0;
                    }
                }
            }
            projectContext.SaveChanges();
        }

        public static void ResetCountSegments(ref ProjectDbContext projectContext)
        {
            List<Stand> stands = projectContext.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (Stand standItem in stands)
            {
                List<Tree> trees = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == standItem.StandsId && t.TreeType == "C").ToList();
                foreach (Tree treeItem in trees)
                {
                    List<Treesegment> segments = projectContext.Treesegments.OrderBy(s => s.PlotNumber).ThenBy(s => s.SegmentNumber)
                        .Where(s => s.TreesId == treeItem.TreesId).ToList();
                    foreach (Treesegment segItem in segments)
                    {
                        segItem.FormFactor = 0;
                        segItem.TotalHeight = 0;
                        segItem.TreeBasalArea = 0;
                        segItem.TreeTreesPerAcre = 0;
                        segItem.CubicGrossVolume = 0;
                        segItem.CubicNetVolume = 0;
                        segItem.ScribnerGrossVolume = 0;
                        segItem.ScribnerNetVolume = 0;
                        segItem.CubicNetVolumePerAce = 0;
                        segItem.ScribnerNetVolumePerAce = 0;
                        segItem.Ccf = 0;
                        segItem.Mbf = 0;
                    }
                }
            }
            projectContext.SaveChanges();
        }

        public static void ProcessCountSegments(ref ProjectDbContext projectContext)
        {
            int defaultFP = 0;
            List<Stand> stands = projectContext.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (Stand standItem in stands)
            {
                defaultFP = ProjectBLL.GetFormPointFromTree(ref projectContext, standItem.StandsId);
                List<Tree> trees = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == standItem.StandsId && t.TreeType == "C").ToList();
                foreach (Tree treeItem in trees)
                {
                    List<Treesegment> segments = projectContext.Treesegments.OrderBy(s => s.PlotNumber).ThenBy(s => s.SegmentNumber)
                        .Where(s => s.TreesId == treeItem.TreesId).ToList();
                    foreach (Treesegment segItem in segments)
                    {
                        float pf = 0;
                        if (IsValidFloat(treeItem.PlotFactorInput))
                            pf = ConvertToFloat(treeItem.PlotFactorInput);
                        else
                            pf = ConvertToFloat(ProjectBLL.GetPlotFactor(standItem, treeItem));
                        if (pf.ToString().Contains("."))
                            pf = (float)Math.Round(pf, 2); //.PlotFactorInput; // ProjectBLL.GetPlotFactor(MainForm.CurrentStand, treeItem);
                        else
                            pf = (float)Math.Round(pf, 0);
                        if (segItem.Dbh != null && segItem.Dbh > 0)
                        {
                            CountWithDbh cwd = new CountWithDbh();
                            List<CountWithDbh> cwdColl = projectContext.CountWithDbhs
                                .Where(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species && t.Status == segItem.TreeStatusDisplayCode && t.Dbh == segItem.Dbh)
                                .ToList();
                            if (cwdColl.Count == 1)
                            {
                                cwd = cwdColl[0];
                            }
                            if (cwdColl.Count > 1)
                            {
                                cwd = GetCountWithDbhAvg(cwdColl);
                            }
                            if (cwdColl.Count == 0)
                            {
                                // Dbh was not found
                                cwdColl = projectContext.CountWithDbhs.Where(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species && t.Status == segItem.TreeStatusDisplayCode).ToList();
                                if (cwdColl.Count == 1)
                                {
                                    cwd = cwdColl[0];
                                }
                                else
                                if (cwdColl.Count > 1)
                                {
                                    cwd = GetCountWithDbhNotFound(cwdColl, segItem);
                                }
                            }
                            if (cwd != null)
                            {
                                try
                                {
                                    int fp = 0;
                                    if (segItem.FormPoint == null || segItem.FormPoint == 0)
                                        fp = defaultFP;
                                    else
                                        fp = (int)segItem.FormPoint;
                                    if (fp == 4)
                                        segItem.TreeBasalArea = pf * treeItem.TreeCount;
                                    else
                                        segItem.TreeBasalArea = pf * treeItem.TreeCount / ((cwd.CalcFf / 100) * (cwd.CalcFf / 100));
                                    segItem.TreeTreesPerAcre = segItem.TreeBasalArea / (0.005454154 * (segItem.Dbh * segItem.Dbh));
                                    segItem.TotalHeight = (short)cwd.CalcTotalHt;
                                    segItem.FormFactor = (short)cwd.CalcFf;
                                    segItem.CubicGrossVolume = Math.Round(cwd.GrossCalcCfTree, 0);
                                    segItem.CubicNetVolume = Math.Round(cwd.NetCalcCfTree, 0);
                                    segItem.ScribnerGrossVolume = Math.Round((double)cwd.GrossCalcBfTree, 0);
                                    segItem.ScribnerNetVolume = Math.Round((double)cwd.NetCalcBfTree, 0);
                                }
                                catch
                                {

                                }
                            }
                        }
                        else
                        {
                            AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species);
                            if (acp != null)
                            {
                                segItem.FormFactor = (short)(acp.Ff * 100);
                                segItem.TotalHeight = (short)acp.TotalHeight;
                                segItem.TreeBasalArea = pf * treeItem.TreeCount / (acp.Ff * acp.Ff); //(pf * rec1.TreeCount)/(ss.Ff * ss.Ff);
                                segItem.TreeTreesPerAcre = segItem.TreeBasalArea / (0.005454154 * (acp.QmDbh * acp.QmDbh)); //rec1.BasalArea/(0.005454154*(rec1.Dbh * rec1.Dbh));
                                //segItem.LogsPerAcre = acp.LogsPerAcre / standMaster.Plots;
                                segItem.ScribnerGrossVolume = acp.GrossBdFtLogs;
                                segItem.ScribnerNetVolume = acp.NetBdFtLogs;
                                segItem.CubicGrossVolume = acp.GrossCuFtLogs;
                                segItem.CubicNetVolume = acp.NetCuFtLogs;
                                //segItem.CubicNetVolumePerAce = acp.NetCuFtAcre / standItem.Plots; //ss.NetCuFtAcre * rec1.BasalArea;
                                //segItem.ScribnerNetVolumePerAce = acp.NetBdFtAcre / standItem.Plots; //ss.NetBdFtAcre * rec1.BasalArea;
                                //segItem.Ccf = (((float)segItem.CubicNetVolumePerAce * (float)standItem.NetGeographicAcres) / 100) / standItem.Plots;
                                //segItem.Mbf = (((float)segItem.ScribnerNetVolumePerAce * (float)standItem.NetGeographicAcres) / 1000) / standItem.Plots;
                            }
                        }

                        //else
                        //{
                        //    if (segItem.Dbh == 0)
                        //        segItem.Dbh = Math.Round((double)acp.QmDbh, 0);
                        //}
                    }
                }
            }
            projectContext.SaveChanges();
        }

        public static void ProcessCountRecords(ref ProjectDbContext projectContext)
        {
            int defaultFP = 0;
            List<Stand> stands = projectContext.Stands.OrderBy(s => s.TractName).ThenBy(s => s.StandName).ToList();
            foreach (Stand standItem in stands)
            {
                defaultFP = ProjectBLL.GetFormPointFromTree(ref projectContext, standItem.StandsId);
                List<Tree> trees = projectContext.Trees.OrderBy(t => t.PlotNumber).ThenBy(t => t.TreeNumber).Where(t => t.StandsId == standItem.StandsId && t.TreeType == "C").ToList();
                foreach (Tree treeItem in trees)
                {
                    List<Treesegment> segments = projectContext.Treesegments.OrderBy(s => s.PlotNumber).ThenBy(s => s.SegmentNumber)
                        .Where(s => s.TreesId == treeItem.TreesId).ToList();
                    foreach (Treesegment segItem in segments)
                    {
                        float pf = 0;
                        if (IsValidFloat(treeItem.PlotFactorInput))
                            pf = ConvertToFloat(treeItem.PlotFactorInput);
                        else
                            pf = ConvertToFloat(ProjectBLL.GetPlotFactor(standItem, treeItem));
                        if (pf.ToString().Contains("."))
                            pf = (float)Math.Round(pf, 2); //.PlotFactorInput; // ProjectBLL.GetPlotFactor(MainForm.CurrentStand, treeItem);
                        else
                            pf = (float)Math.Round(pf, 0);
                        if (segItem.Dbh != null && segItem.Dbh > 0)
                        {
                            CountWithDbh cwd = new CountWithDbh();
                            List<CountWithDbh> cwdColl = projectContext.CountWithDbhs
                                .Where(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species && t.Status == segItem.TreeStatusDisplayCode && t.Dbh == segItem.Dbh)
                                .ToList();
                            if (cwdColl.Count == 1)
                            {
                                cwd = cwdColl[0];
                            }
                            if (cwdColl.Count > 1)
                            {
                                cwd = GetCountWithDbhAvg(cwdColl);
                            }
                            if (cwdColl.Count == 0)
                            {
                                // Dbh was not found
                                cwdColl = projectContext.CountWithDbhs.Where(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species && t.Status == segItem.TreeStatusDisplayCode).ToList();
                                if (cwdColl.Count == 1)
                                {
                                    cwd = cwdColl[0];
                                }
                                else
                                if (cwdColl.Count > 1)
                                {
                                    cwd = GetCountWithDbhNotFound(cwdColl, segItem);
                                }
                            }
                            if (cwd != null)
                            {
                                try
                                {
                                    int fp = 0;
                                    if (segItem.FormPoint == null || segItem.FormPoint == 0)
                                        fp = defaultFP;
                                    else
                                        fp = (int)segItem.FormPoint;
                                    if (fp == 4)
                                        segItem.TreeBasalArea = pf * treeItem.TreeCount;
                                    else
                                        segItem.TreeBasalArea = pf * treeItem.TreeCount / ((cwd.CalcFf / 100) * (cwd.CalcFf / 100));
                                    segItem.TreeTreesPerAcre = segItem.TreeBasalArea / (0.005454154 * (segItem.Dbh * segItem.Dbh));
                                    segItem.TotalHeight = (short)0; // cwd.CalcTotalHt;
                                    segItem.FormFactor = (short)0; // cwd.CalcFf;
                                    segItem.CubicGrossVolume = Math.Round(cwd.GrossCalcCfTree, 0);
                                    segItem.CubicNetVolume = Math.Round(cwd.NetCalcCfTree, 0);
                                    segItem.ScribnerGrossVolume = Math.Round((double)cwd.GrossCalcBfTree, 0);
                                    segItem.ScribnerNetVolume = Math.Round((double)cwd.NetCalcBfTree, 0);
                                    segItem.CubicGrossVolumePerAce = segItem.CubicGrossVolume * segItem.TreeTreesPerAcre;
                                    segItem.CubicNetVolumePerAce = segItem.CubicNetVolume * segItem.TreeTreesPerAcre;
                                    segItem.ScribnerGrossVolumePerAce = segItem.ScribnerGrossVolume * segItem.TreeTreesPerAcre;
                                    segItem.ScribnerNetVolumePerAce = segItem.ScribnerNetVolume * segItem.TreeTreesPerAcre;
                                    segItem.Ccf = (segItem.CubicNetVolumePerAce * standItem.NetGeographicAcres) / 100;
                                    segItem.Mbf = (segItem.ScribnerNetVolumePerAce * standItem.NetGeographicAcres) / 1000;

                                    treeItem.BasalArea += segItem.TreeBasalArea;
                                    treeItem.TreesPerAcre += segItem.TreeTreesPerAcre;
                                    treeItem.LogsPerAcre += cwd.CalcLogsAcre; // (1 * segItem.TreeTreesPerAcre);
                                    treeItem.TotalCuFtGrossVolume += segItem.CubicGrossVolume;
                                    treeItem.TotalBdFtGrossVolume += segItem.ScribnerGrossVolume;
                                    treeItem.TotalCuFtNetVolume += segItem.CubicNetVolume;
                                    treeItem.TotalBdFtNetVolume += segItem.ScribnerNetVolume;
                                    treeItem.TotalGrossCubicPerAcre += (segItem.CubicGrossVolume* segItem.TreeTreesPerAcre);
                                    treeItem.TotalGrossScribnerPerAcre += (segItem.ScribnerGrossVolume* segItem.TreeTreesPerAcre);
                                    treeItem.TotalNetCubicPerAcre += (segItem.CubicNetVolume * segItem.TreeTreesPerAcre);
                                    treeItem.TotalNetScribnerPerAcre += (segItem.ScribnerNetVolume * segItem.TreeTreesPerAcre);
                                    treeItem.TotalCcf += segItem.Ccf;
                                    treeItem.TotalMbf += segItem.Mbf;
                                }
                                catch
                                {

                                }
                            }
                        }
                        else
                        {
                            AdjCruisePlot acp = projectContext.AdjCruisePlots.FirstOrDefault(t => t.StandsId == segItem.StandsId && t.Species == segItem.Species);
                            if (acp != null)
                            {
                                segItem.FormFactor = (short)0; // (acp.Ff * 100);
                                segItem.TotalHeight = (short)0; // acp.TotalHeight;
                                segItem.TreeBasalArea = acp.BasalArea; // pf * treeItem.TreeCount / (acp.Ff * acp.Ff); //(pf * rec1.TreeCount)/(ss.Ff * ss.Ff);
                                segItem.TreeTreesPerAcre = acp.TreesPerAcre; // segItem.TreeBasalArea / (0.005454154 * (acp.QmDbh * acp.QmDbh)); //rec1.BasalArea/(0.005454154*(rec1.Dbh * rec1.Dbh));
                                //segItem.LogsPerAcre = acp.LogsPerAcre / standMaster.Plots;
                                if (acp.GrossBdFtLogs != null)
                                    segItem.ScribnerGrossVolume = acp.GrossBdFtLogs;
                                else
                                    segItem.ScribnerGrossVolume = 0;
                                if (acp.GrossCuFtLogs != null)
                                    segItem.CubicGrossVolume = acp.GrossCuFtLogs;
                                else
                                    segItem.CubicGrossVolume = 0;
                                if (acp.NetBdFtLogs != null)
                                    segItem.ScribnerNetVolume = acp.NetBdFtLogs;
                                else
                                    segItem.ScribnerNetVolume = 0;
                                if (acp.NetCuFtLogs != null)
                                    segItem.CubicNetVolume = acp.NetCuFtLogs;
                                else
                                    segItem.CubicNetVolume = 0;
                                //segItem.CubicNetVolumePerAce = acp.NetCuFtAcre / standItem.Plots; //ss.NetCuFtAcre * rec1.BasalArea;
                                //segItem.ScribnerNetVolumePerAce = acp.NetBdFtAcre / standItem.Plots; //ss.NetBdFtAcre * rec1.BasalArea;
                                //segItem.Ccf = (((float)segItem.CubicNetVolumePerAce * (float)standItem.NetGeographicAcres) / 100) / standItem.Plots;
                                //segItem.Mbf = (((float)segItem.ScribnerNetVolumePerAce * (float)standItem.NetGeographicAcres) / 1000) / standItem.Plots;

                                treeItem.BasalArea += segItem.TreeBasalArea;
                                treeItem.TreesPerAcre += segItem.TreeTreesPerAcre;
                                treeItem.LogsPerAcre += (1 * segItem.TreeTreesPerAcre);
                                treeItem.TotalCuFtGrossVolume += segItem.CubicGrossVolume;
                                treeItem.TotalBdFtGrossVolume += segItem.ScribnerGrossVolume;
                                treeItem.TotalCuFtNetVolume += segItem.CubicNetVolume;
                                treeItem.TotalBdFtNetVolume += segItem.ScribnerNetVolume;
                                treeItem.TotalNetCubicPerAcre += (segItem.CubicNetVolume * segItem.TreeTreesPerAcre);
                                treeItem.TotalNetScribnerPerAcre += (segItem.ScribnerNetVolume * segItem.TreeTreesPerAcre);
                                treeItem.TotalCcf += ((segItem.CubicNetVolume * segItem.TreeTreesPerAcre) * standItem.NetGeographicAcres) / 100;
                                treeItem.TotalMbf += ((segItem.ScribnerNetVolume * segItem.TreeTreesPerAcre) * standItem.NetGeographicAcres) / 1000;
                            }
                        }

                        //else
                        //{
                        //    if (segItem.Dbh == 0)
                        //        segItem.Dbh = Math.Round((double)acp.QmDbh, 0);
                        //}
                    }
                }
            }
            projectContext.SaveChanges();
        }
        
        public static CountWithDbh GetCountWithDbhAvg(List<CountWithDbh> cwdColl)
        {
            CountWithDbh result = new CountWithDbh();

            float GrossCfTree = 0;
            float GrossBfTree = 0;
            float NetCfTree = 0;
            float NetBfTree = 0;
            float ff = 0;
            float totalHeight = 0;

            foreach (var item in cwdColl)
            {
                GrossCfTree += (float)item.GrossCalcCfTree;
                NetCfTree += (float)item.NetCalcCfTree;
                GrossBfTree += (float)item.GrossCalcBfTree;
                NetBfTree += (float)item.NetCalcBfTree;
                ff += (float)item.CalcFf;
                totalHeight += (float)item.CalcTotalHt;
            }
            result.GrossCalcBfTree = GrossBfTree / cwdColl.Count;
            result.GrossCalcCfTree = GrossCfTree / cwdColl.Count;
            result.NetCalcBfTree = NetBfTree / cwdColl.Count;
            result.NetCalcCfTree = NetCfTree / cwdColl.Count;
            result.CalcFf = ff / cwdColl.Count;
            result.CalcTotalHt = totalHeight / cwdColl.Count;

            return result;
        }

        public static CountWithDbh GetCountWithDbhNotFound(List<CountWithDbh> cwdColl, Tree treeItem)
        {
            CountWithDbh result = new CountWithDbh();
            int low = -1;
            int high = -1;

            for (int c = 0; c < cwdColl.Count; c++)
            {
                if (treeItem.Dbh < cwdColl[c].Dbh)
                {
                    high = c;
                    low = c - 1;
                    break;
                }
            }
            if (low != -1 && high != -1)
            {
                result.GrossCalcBfTree = (cwdColl[low].GrossCalcBfTree + cwdColl[high].GrossCalcBfTree) / 2;
                result.GrossCalcCfTree = (cwdColl[low].GrossCalcCfTree + cwdColl[high].GrossCalcCfTree) / 2;
                result.NetCalcBfTree = (cwdColl[low].NetCalcBfTree + cwdColl[high].NetCalcBfTree) / 2;
                result.NetCalcCfTree = (cwdColl[low].NetCalcCfTree + cwdColl[high].NetCalcCfTree) / 2;
                result.CalcFf = (cwdColl[low].CalcFf + cwdColl[high].CalcFf) / 2;
                result.CalcTotalHt = (cwdColl[low].CalcTotalHt + cwdColl[high].CalcTotalHt) / 2;
                result.CalcLogsAcre = (cwdColl[low].CalcLogsAcre + cwdColl[high].CalcLogsAcre) / 2;
            }
            else
                result = cwdColl[cwdColl.Count - 1];

            return result;
        }

        public static CountWithDbh GetCountWithDbhNotFound(List<CountWithDbh> cwdColl, Treesegment segItem)
        {
            CountWithDbh result = new CountWithDbh();
            int low = -1;
            int high = -1;

            for (int c = 0; c < cwdColl.Count; c++)
            {
                if (segItem.Dbh < cwdColl[c].Dbh)
                {
                    high = c;
                    low = c - 1;
                    break;
                }
            }
            if (low != -1 && high != -1)
            {
                result.GrossCalcBfTree = (cwdColl[low].GrossCalcBfTree + cwdColl[high].GrossCalcBfTree) / 2;
                result.GrossCalcCfTree = (cwdColl[low].GrossCalcCfTree + cwdColl[high].GrossCalcCfTree) / 2;
                result.NetCalcBfTree = (cwdColl[low].NetCalcBfTree + cwdColl[high].NetCalcBfTree) / 2;
                result.NetCalcCfTree = (cwdColl[low].NetCalcCfTree + cwdColl[high].NetCalcCfTree) / 2;
                result.CalcFf = (cwdColl[low].CalcFf + cwdColl[high].CalcFf) / 2;
                result.CalcTotalHt = (cwdColl[low].CalcTotalHt + cwdColl[high].CalcTotalHt) / 2;
                result.CalcLogsAcre = (cwdColl[low].CalcLogsAcre + cwdColl[high].CalcLogsAcre) / 2;
            }
            else
                result = cwdColl[cwdColl.Count - 1];

            return result;
        }

        public static bool IsValidFloat(string sTmp)
        {
            try
            {
                float fTmp = Single.Parse(sTmp);
                return (true);
            }
            catch
            {
                return (false);
            }
        }

        public static float ConvertToFloat(string sTmp)
        {
            float f;
            string tmp = sTmp.Replace(",", "");
            tmp = sTmp.Replace("$", "");

            try
            {
                f = Convert.ToSingle(tmp);
            }
            catch
            {
                f = 0.0f;
            }
            return (f);
        }

        public static bool ProcessCruiseToForestInventory(ref ProjectDbContext projectContext, Project currentProject, Stand st, bool pDelete)
        {
            bool bResult = false;
            projectContext.TmpStandInputExceptions.RemoveRange(projectContext.TmpStandInputExceptions.Where(s => s.StandsId == st.StandsId));
            if (pDelete)
            {
                bool result = projectContext.StandInputs.Any(s => s.StandsId == st.StandsId);
                if (result)
                {
                    projectContext.StandInputs.RemoveRange(projectContext.StandInputs.Where(s => s.StandsId == st.StandsId));
                }
            }
            bResult = CreateStandInput(ref projectContext, currentProject, st);
            return bResult;
        }

        public static bool CreateStandInput(ref ProjectDbContext projectContext, Project currentProject, Stand st)
        {
            bool bResult = true;
            string tCurrKey = string.Empty;
            string tPrevKey = string.Empty;
            string yldTableName = string.Empty;
            int siteIndex = 0;

            int saveId = -1;
            bool bFirst = true;
            bool bPrintedFirst = false;

            short tPlots = 0;
            float tAcres = 0;
            string tSource = string.Empty;
            string tDate = string.Empty;
            short tAge = 0;
            short firstAge = 0;
            string tSpecies = string.Empty;
            string tStatus = string.Empty;
            float tDbh = 0;
            float tFF = 0;
            float tTotalHeight = 0;
            float tStocking = 0;

            short tTrees = 0;
            float tBAAcre = 0;
            float tTreesAcre = 0;
            float tLogsAcre = 0;
            float tGrossBdFtVolume = 0, tGrossCuFtVolume = 0;
            float tNetBdFtVolume = 0, tNetCuFtVolume = 0;

            List<Tree> wColl = projectContext.Trees.OrderBy(w => w.SpeciesAbbreviation).ThenBy(w => w.TreeStatusDisplayCode).ThenBy(w => w.Age)
                .Where(w => w.StandsId == st.StandsId).ToList();

            foreach (var treeItem in wColl)
            {
                if (bFirst)
                {
                    tPrevKey = String.Format("{0}{1}{2}", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode, treeItem.Age.ToString().PadLeft(3, '0'));
                    saveId = (int)st.StandsId;
                    tAcres = (float)st.NetGeographicAcres;
                    tPlots = (short)st.Plots;
                    tSource = st.Source;
                    DateTime d = (DateTime)st.DateOfStandData;
                    tDate = string.Format("{0}", d.Year);
                    tAge = (short)treeItem.Age;
                    firstAge = (short)treeItem.Age;
                    tSpecies = treeItem.SpeciesAbbreviation;
                    tStatus = treeItem.TreeStatusDisplayCode;
                    bFirst = false;
                }
                tCurrKey = String.Format("{0}{1}{2}", treeItem.SpeciesAbbreviation, treeItem.TreeStatusDisplayCode, treeItem.Age.ToString().PadLeft(3, '0'));
                if (tPrevKey != tCurrKey)
                {
                    if (st.Species11.Trim() == tSpecies || st.Species12.Trim() == tSpecies || st.Species13.Trim() == tSpecies)
                        siteIndex = (byte)st.SiteIndex1;
                    else
                        if (st.Species21.Trim() == tSpecies || st.Species22.Trim() == tSpecies || st.Species23.Trim() == tSpecies)
                        siteIndex = (byte)st.SiteIndex2;
                    else
                            if (st.Species31.Trim() == tSpecies || st.Species32.Trim() == tSpecies || st.Species33.Trim() == tSpecies)
                        siteIndex = (byte)st.SiteIndex3;
                    else
                            if (st.Species41.Trim() == tSpecies || st.Species42.Trim() == tSpecies || st.Species43.Trim() == tSpecies)
                        siteIndex = (byte)st.SiteIndex4;
                    if (siteIndex == 0)
                    {
                        TmpStandInputException siRec = new TmpStandInputException();
                        siRec.StandsId = st.StandsId;
                        siRec.TractName = st.TractName;
                        siRec.StandName = st.StandName;
                        siRec.SiteIndex = (short)siteIndex;
                        siRec.Species = tSpecies;
                        siRec.ErrorMessage = "SiteIndex not defined in Stand Master";
                        bResult = false;
                        projectContext.TmpStandInputExceptions.Add(siRec);
                    }
                    else
                    {
                        // write record
                        StandInput rec = new StandInput();
                        Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, currentProject.SpeciesTableName, tSpecies);
                        yldTableName = spcRec.YieldTableName;
                        rec.Cost = 0;
                        rec.DcHeight = 0;
                        rec.OrigDcHeight = 0;
                        rec.OrigLogDib = 0;
                        rec.OrigLogLen = 0;
                        rec.OrigStocking = 0;
                        rec.PoleSeq = 0;
                        rec.Revenue = 0;
                        rec.StandsId = (int)st.StandsId;
                        rec.BirthYear = (short)((short)Convert.ToInt32(tDate) - (int)tAge);
                        rec.NetAcres = st.NetGeographicAcres;
                        rec.OrigAge = (byte)tAge;
                        rec.Age = rec.OrigAge;
                        rec.Species = tSpecies;
                        rec.Status = tStatus;
                        rec.SiteIndex = (short)siteIndex;

                        YieldTable yldTable = GetYieldTableRecord(ref projectContext, yldTableName, tAge, (short)siteIndex);

                        rec.OrigTreesPerAcre = Math.Round((tTreesAcre / (float)st.Plots), 2);
                        rec.TreesPerAcre = rec.OrigTreesPerAcre;
                        rec.OrigBasalArea = tBAAcre / (float)st.Plots;
                        rec.BasalArea = rec.OrigBasalArea;
                        if (rec.OrigBasalArea > 0 && rec.OrigTreesPerAcre > 0)
                            rec.OrigD4H = (short)Math.Sqrt((double)rec.OrigBasalArea / (double)(rec.OrigTreesPerAcre * .005454154));
                        else
                            rec.OrigD4H = 0;
                        rec.D4H = rec.OrigD4H;
                        float t = 0;
                        if (rec.OrigTreesPerAcre > 0)
                            t = (tTotalHeight / (float)st.Plots) / (float)rec.TreesPerAcre;
                        if (t > 0)
                            rec.OrigTotalHeight = (short)Convert.ToInt16(string.Format("{0:0}", t));
                        else
                            rec.OrigTotalHeight = 0;
                        rec.TotalHeight = rec.OrigTotalHeight;
                        rec.OrigGrossBdFt = (double)tGrossBdFtVolume / (double)st.Plots;
                        rec.GrossBdFt = rec.OrigGrossBdFt;
                        rec.OrigNetBdFt = (double)tNetBdFtVolume / (double)st.Plots;
                        rec.NetBdFt = rec.OrigNetBdFt;
                        rec.OrigGrossCuFt = (double)tGrossCuFtVolume / (double)st.Plots;
                        rec.GrossCuFt = rec.OrigGrossCuFt;
                        rec.OrigNetCuFt = (double)tNetCuFtVolume / (double)st.Plots;
                        rec.NetCuFt = rec.OrigNetCuFt;
                        rec.OrigLogsPerAcre = tLogsAcre / (float)st.Plots;
                        rec.LogsPerAcre = rec.OrigLogsPerAcre;
                        rec.OrigStocking = ProjectBLL.GetNormality(ref projectContext, rec, yldTable);
                        rec.Stocking = rec.OrigStocking;
                        rec.OrigTotalCcf = (rec.OrigNetCuFt * st.NetGeographicAcres) / 100;
                        rec.OrigTotalMbf = (rec.OrigNetBdFt * (float)st.NetGeographicAcres) / 1000;
                        rec.TotalCcf = rec.OrigTotalCcf;
                        rec.TotalMbf = rec.OrigTotalMbf;

                        projectContext.StandInputs.Add(rec);
                    }

                    // move fields
                    saveId = (int)treeItem.StandsId;
                    tAcres = (float)st.NetGeographicAcres;
                    tPlots = (short)st.Plots;
                    tSource = st.Source;
                    DateTime d = (DateTime)st.DateOfStandData;
                    tDate = string.Format("{0}", d.Year);
                    tAge = (short)treeItem.Age;
                    tSpecies = treeItem.SpeciesAbbreviation;
                    tStatus = treeItem.TreeStatusDisplayCode;
                    tDbh = 0;
                    if (treeItem.TotalHeight == 0)
                        tTotalHeight = (float)treeItem.CalcTotalHeight * (float)treeItem.TreesPerAcre;
                    else
                        tTotalHeight = (float)treeItem.TotalHeight * (float)treeItem.TreesPerAcre;
                    tTrees = 1;
                    tTreesAcre = (float)treeItem.TreesPerAcre;
                    if (treeItem.LogsPerAcre != null)
                        tLogsAcre = (float)treeItem.LogsPerAcre;
                    else
                        tLogsAcre = 0;
                    tBAAcre = (float)treeItem.BasalArea;
                    if (treeItem.TotalBdFtGrossVolume != null)
                        tGrossBdFtVolume = (float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalCuFtGrossVolume != null)
                        tGrossCuFtVolume = (float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalBdFtNetVolume != null)
                        tNetBdFtVolume = (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalCuFtNetVolume != null)
                        tNetCuFtVolume = (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                    siteIndex = 0;

                    tPrevKey = tCurrKey;
                }
                else
                {
                    tTrees++;

                    if (treeItem.Dbh != null)
                        tDbh += (float)treeItem.Dbh;
                    if (treeItem.TotalHeight == 0)
                        tTotalHeight += (float)treeItem.CalcTotalHeight * (float)treeItem.TreesPerAcre;
                    else
                        tTotalHeight += (float)treeItem.TotalHeight * (float)treeItem.TreesPerAcre;
                    tBAAcre += (float)treeItem.BasalArea;
                    tTreesAcre += (float)treeItem.TreesPerAcre;
                    if (treeItem.LogsPerAcre != null)
                        tLogsAcre += (float)treeItem.LogsPerAcre;
                    if (treeItem.TotalBdFtGrossVolume != null)
                        tGrossBdFtVolume += (float)treeItem.TotalBdFtGrossVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalCuFtGrossVolume != null)
                        tGrossCuFtVolume += (float)treeItem.TotalCuFtGrossVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalBdFtNetVolume != null)
                        tNetBdFtVolume += (float)treeItem.TotalBdFtNetVolume * (float)treeItem.TreesPerAcre;
                    if (treeItem.TotalCuFtNetVolume != null)
                        tNetCuFtVolume += (float)treeItem.TotalCuFtNetVolume * (float)treeItem.TreesPerAcre;
                }
            }
            if (st.Species11.Trim() == tSpecies || st.Species12.Trim() == tSpecies || st.Species13.Trim() == tSpecies)
                siteIndex = (byte)st.SiteIndex1;
            else
            if (st.Species21.Trim() == tSpecies || st.Species22.Trim() == tSpecies || st.Species23.Trim() == tSpecies)
                siteIndex = (byte)st.SiteIndex2;
            else
            if (st.Species31.Trim() == tSpecies || st.Species32.Trim() == tSpecies || st.Species33.Trim() == tSpecies)
                siteIndex = (byte)st.SiteIndex3;
            else
            if (st.Species41.Trim() == tSpecies || st.Species42.Trim() == tSpecies || st.Species43.Trim() == tSpecies)
                siteIndex = (byte)st.SiteIndex4;
            if (siteIndex == 0)
            {
                TmpStandInputException siRec = new TmpStandInputException();
                siRec.StandsId = st.StandsId;
                siRec.TractName = st.TractName;
                siRec.StandName = st.StandName;
                siRec.SiteIndex = (short)siteIndex;
                siRec.Species = tSpecies;
                siRec.ErrorMessage = "SiteIndex not defined in Stand Master";
                bResult = false;
                projectContext.TmpStandInputExceptions.Add(siRec);
            }
            else
            {
                // write record
                StandInput rec = new StandInput();
                Species spcRec = ProjectBLL.GetSpecieByAbbrev(ref projectContext, currentProject.SpeciesTableName, tSpecies);
                yldTableName = spcRec.YieldTableName;
                rec.Cost = 0;
                rec.DcHeight = 0;
                rec.OrigDcHeight = 0;
                rec.OrigLogDib = 0;
                rec.OrigLogLen = 0;
                rec.OrigStocking = 0;
                rec.PoleSeq = 0;
                rec.Revenue = 0;
                rec.StandsId = (int)st.StandsId;
                rec.BirthYear = (short)((short)Convert.ToInt32(tDate) - (int)tAge);
                rec.NetAcres = st.NetGeographicAcres;
                rec.OrigAge = (byte)tAge;
                rec.Age = rec.OrigAge;
                rec.Species = tSpecies;
                rec.Status = tStatus;
                rec.SiteIndex = (short)siteIndex;

                YieldTable yldTable = GetYieldTableRecord(ref projectContext, yldTableName, tAge, (short)siteIndex);

                rec.OrigTreesPerAcre = Math.Round((tTreesAcre / (float)st.Plots), 2);
                rec.TreesPerAcre = rec.OrigTreesPerAcre;
                rec.OrigBasalArea = tBAAcre / (float)st.Plots;
                rec.BasalArea = rec.OrigBasalArea;
                if (rec.OrigBasalArea > 0 && rec.OrigTreesPerAcre > 0)
                    rec.OrigD4H = (short)Math.Sqrt((double)rec.OrigBasalArea / (double)(rec.OrigTreesPerAcre * .005454154));
                else
                    rec.OrigD4H = 0;
                rec.D4H = rec.OrigD4H;
                float t = 0;
                if (rec.OrigTreesPerAcre > 0)
                    t = (tTotalHeight / (float)st.Plots) / (float)rec.OrigTreesPerAcre;
                if (t > 0)
                    rec.OrigTotalHeight = (short)Convert.ToInt16(string.Format("{0:0}", t));
                else
                    rec.OrigTotalHeight = 0;
                rec.TotalHeight = rec.OrigTotalHeight;
                rec.OrigGrossBdFt = (double)tGrossBdFtVolume / (double)st.Plots;
                rec.GrossBdFt = rec.OrigGrossBdFt;
                rec.OrigNetBdFt = (double)tNetBdFtVolume / (double)st.Plots;
                rec.NetBdFt = rec.OrigNetBdFt;
                rec.OrigGrossCuFt = (double)tGrossCuFtVolume / (double)st.Plots;
                rec.GrossCuFt = rec.OrigGrossCuFt;
                rec.OrigNetCuFt = (double)tNetCuFtVolume / (double)st.Plots;
                rec.NetCuFt = rec.OrigNetCuFt;
                rec.OrigLogsPerAcre = tLogsAcre / (float)st.Plots;
                rec.LogsPerAcre = rec.OrigLogsPerAcre;
                rec.OrigStocking = ProjectBLL.GetNormality(ref projectContext, rec, yldTable);
                rec.Stocking = rec.OrigStocking;
                rec.OrigTotalCcf = (rec.OrigNetCuFt * st.NetGeographicAcres) / 100;
                rec.OrigTotalMbf = (rec.OrigNetBdFt * (float)st.NetGeographicAcres) / 1000;
                rec.TotalCcf = rec.OrigTotalCcf;
                rec.TotalMbf = rec.OrigTotalMbf;

                projectContext.StandInputs.Add(rec);
            }

            projectContext.SaveChanges();

            return bResult;
        }

        public static short GetNormality(ref ProjectDbContext projectContext, StandInput rec, YieldTable yldTable)
        {
            short result = 0;
            int x = 0;
            List<DefReportDomPri> domPri = projectContext.DefReportDomPris.OrderBy(p => p.Priority).ToList();
            x = GetDomPriority(yldTable, domPri);

            switch (domPri[x].Item)
            {
                case "Trees per Acre":
                    result = (short)((rec.OrigTreesPerAcre / yldTable.TreesPerAcre)*100);
                    break;
                case "Net Cubic Feet / Acre":
                    result = (short)((rec.OrigNetCuFt / yldTable.CubicNet) * 100);
                    break;
                case "Net Board Feet / Acre":
                    result = (short)((rec.OrigNetBdFt / yldTable.ScribnerNet) * 100);
                    break;
                case "Basal Area / Acre":
                    result = (short)((rec.OrigBasalArea / yldTable.BasalArea) * 100);
                    break;
                default:
                    result = (short)((rec.OrigNetBdFt / yldTable.ScribnerNet) * 100);
                    break;
            }
            return result;
        }

        private static int GetDomPriority(YieldTable yldTable, List<DefReportDomPri> domPri)
        {
            int x = 0;
            bool done = false;
            foreach (var item in domPri)
            {
                switch (item.Item)
                {
                    case "Trees per Acre":
                        if (yldTable.TreesPerAcre > 0)
                            done = true;
                        break;
                    case "Net Cubic Feet / Acre":
                        if (yldTable.CubicNet > 0)
                            done = true;
                        break;
                    case "Net Board Feet / Acre":
                        if (yldTable.ScribnerNet > 0)
                            done = true;
                        break;
                    case "Basal Area / Acre":
                        if (yldTable.BasalArea > 0)
                            done = true;
                        break;
                    default:
                        if (yldTable.ScribnerNet > 0)
                            done = true;
                        break;
                }
                if (done)
                    break;
                else
                    x++;
            }
            return x;
        }

        private static float interpolation(float si, float siLow, float siHigh, float dataLow, float dataHigh)
        {
            return dataLow + ((si - siLow) / (siHigh - siLow)) * (dataHigh - dataLow);

        }

        public static YieldTable GetYieldTableRecord(ref ProjectDbContext projectContext, string pYieldTableName, short pAge, short pSiteIndex)
        {
            YieldTable result = projectContext.YieldTables.FirstOrDefault(y => y.TableName == pYieldTableName && y.BhAge == pAge && y.SiteIndex == pSiteIndex);
            if (result != null)
                return result;

            YieldTable yldLow = new YieldTable();
            YieldTable yldHigh = new YieldTable();
            YieldTable yldRec = new YieldTable();
            List<YieldTable> tbls = projectContext.YieldTables.Where(y => y.TableName == pYieldTableName && y.BhAge == pAge).ToList();
            if (tbls.Count == 0)
            {
                List<YieldTable> aTbls = projectContext.YieldTables.Where(y => y.TableName == pYieldTableName).ToList();
                int xLow = 0;
                int xHigh = 0;
                bool haveLow = false;
                foreach (YieldTable item in aTbls)
                {
                    if (item.SiteIndex < pSiteIndex)
                        yldLow = item;
                    if (item.SiteIndex > pSiteIndex)
                    {
                        if (!haveLow)
                        {
                            yldLow = aTbls[xHigh - 1];
                            yldHigh = item;
                            haveLow = true;
                        }
                        //break;
                    }
                    if (haveLow)
                    {
                        if (yldHigh.SiteIndex != item.SiteIndex)
                        {
                            yldHigh = aTbls[xHigh - 1];
                            break;
                        }
                    }
                    xHigh++;
                }
            }
            else
            {
                foreach (YieldTable item in tbls)
                {
                    if (item.SiteIndex < pSiteIndex)
                        yldLow = item;
                    if (item.SiteIndex > pSiteIndex)
                    {
                        yldHigh = item;
                        break;
                    }
                }
            }

            if (yldLow.BasalArea > 0 && yldHigh.BasalArea > 0)
                yldRec.BasalArea = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.BasalArea, (float)yldHigh.BasalArea);
            else
                yldRec.BasalArea = 0;
            yldRec.BhAge = (double)pAge;
            if (yldLow.CubicGross > 0 && yldHigh.CubicGross > 0)
                yldRec.CubicGross = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.CubicGross, (float)yldHigh.CubicGross);
            else
                yldRec.CubicGross = 0;
            if (yldLow.CubicNet > 0 && yldHigh.CubicNet > 0)
                yldRec.CubicNet = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.CubicNet, (float)yldHigh.CubicNet);
            else
                yldRec.CubicNet = 0;
            if (yldLow.Dbh > 0 && yldHigh.Dbh > 0)
                yldRec.Dbh = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.Dbh, (float)yldHigh.Dbh);
            else
                yldRec.Dbh = 0;
            if (yldLow.Height > 0 && yldHigh.Height > 0)
                yldRec.Height = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.Height, (float)yldHigh.Height);
            else
                yldRec.Height = 0;
            if (yldLow.ScribnerGross > 0 && yldHigh.ScribnerGross > 0)
                yldRec.ScribnerGross = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.ScribnerGross, (float)yldHigh.ScribnerGross);
            else
                yldRec.ScribnerGross = 0;
            if (yldLow.ScribnerNet > 0 && yldHigh.ScribnerNet > 0)
                yldRec.ScribnerNet = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.ScribnerNet, (float)yldHigh.ScribnerNet);
            else
                yldRec.ScribnerNet = 0;
            yldRec.SiteIndex = pSiteIndex;
            yldRec.Species = pYieldTableName.Trim().Substring(0, 2);
            yldRec.TableName = pYieldTableName;
            if (yldLow.TotalAge > 0 && yldHigh.TotalAge > 0)
                yldRec.TotalAge = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.TotalAge, (float)yldHigh.TotalAge);
            else
                yldRec.TotalAge = 0;
            if (yldLow.TreesPerAcre > 0 && yldHigh.TreesPerAcre > 0)
                yldRec.TreesPerAcre = interpolation(pSiteIndex, (float)yldLow.SiteIndex, (float)yldHigh.SiteIndex, (float)yldLow.TreesPerAcre, (float)yldHigh.TreesPerAcre);
            else
                yldRec.TreesPerAcre = 0;

            return yldRec;
        }

        public static void DeleteTmpStandInput(ref ProjectDbContext projectContext)
        {
            List<Stand> stands = GetAllStands(ref projectContext);
            foreach (Stand item in stands)
            {
                projectContext.TmpStandInputExceptions.RemoveRange(projectContext.TmpStandInputExceptions.Where(s => s.StandsId == item.StandsId));
            }
            projectContext.SaveChanges();
        }

        public static void DeleteTmpStandInput(ref ProjectDbContext projectContext, Stand pStand)
        {
            projectContext.TmpStandInputExceptions.RemoveRange(projectContext.TmpStandInputExceptions.Where(s => s.StandsId == pStand.StandsId));

            projectContext.SaveChanges();
        }

        public static void DeleteTmpStandInput(ref ProjectDbContext projectContext, long pId)
        {
            projectContext.TmpStandInputExceptions.RemoveRange(projectContext.TmpStandInputExceptions.Where(s => s.StandsId == pId));

            projectContext.SaveChanges();
        }

        public static void DeleteStandInput(ref ProjectDbContext projectContext)
        {
            List<Stand> stands = GetAllStands(ref projectContext);
            foreach (Stand item in stands)
            {
                projectContext.StandInputs.RemoveRange(projectContext.StandInputs.Where(s => s.StandsId == item.StandsId));
            }
            projectContext.SaveChanges();
        }

        public static void DeleteStandInput(ref ProjectDbContext projectContext, Stand pStand)
        {
            projectContext.StandInputs.RemoveRange(projectContext.StandInputs.Where(s => s.StandsId == pStand.StandsId));

            projectContext.SaveChanges();
        }

        public static void ProcessCruiseToForestInventory(ref ProjectDbContext projectContext, Project currentProject)
        {
            List<Stand> stands = GetAllStands(ref projectContext);
            foreach (Stand item in stands)
                ProcessCruiseToForestInventory(ref projectContext, currentProject, item, false);
            projectContext.SaveChanges();
        }

        public static string GetLbsPerCcf(ref ProjectDbContext projectContext, Project pProject, string tSpecies, string v)
        {
            Species spc = ProjectBLL.GetSpecieByAbbrev(ref projectContext, pProject.SpeciesTableName, tSpecies);

            return (spc.Lbs.ToString());
        }

        public static int? GetPriceForTimeberEval(ref ProjectDbContext projectContext, string priceTableName, string tSpecies, string tSort, double? logAveLen, double? logAvgDia)
        {
            int? price = 0;

            switch (tSort)
            {
                case "P":
                    price = 1000; //GetPolePrice(string tSpecies, string tSort, double? pLen, double? pDia);
                    break;
                case "G":
                    price = 1000; //GetPilingPrice(string tSpecies, string tSort, double? pLen, double? pDia);
                    break;
                default:
                    price = ProjectBLL.GetPriceByCatagory(ref projectContext, priceTableName, tSpecies, tSort, logAveLen, logAvgDia);
                    break;
            }
            return price;
        }

        public static int GetMeasuredTreesForSpecies(ref ProjectDbContext projectContext, string saveSpecies)
        {
            int result = 0;
            
            List<StandSpecy> rColl = projectContext.StandSpecies.Where(t => t.SpeciesDescription == saveSpecies).ToList();
            foreach (var item in rColl)
                result += (int)item.MeasuredTrees;
            return result;
        }

        public static int GetCountTreesForSpecies(ref ProjectDbContext projectContext, string saveSpecies)
        {
            int result = 0;
            List<StandSpecy> rColl = projectContext.StandSpecies.Where(t => t.SpeciesDescription == saveSpecies).ToList();
            foreach (var item in rColl)
                result += (int)item.CountTrees;
            return result;
        }
    }
}
