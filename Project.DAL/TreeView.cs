﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.DAL
{
    public class TreeView
    {
        public int? StandsId { get; set; } // StandsID
        public int TreesId { get; set; } // TreesID (Primary key)
        public string UserPlotNumber { get; set; } // PlotNumber (length: 8)
        public int? TreeNumber { get; set; } // TreeNumber
        public string PlotFactorInput { get; set; } // PlotFactorInput (length: 2)
        public short? AgeCode { get; set; } // AgeCode
        public string SpeciesAbbreviation { get; set; } // SpeciesAbbreviation (length: 3)
        public string TreeStatusDisplayCode { get; set; } // TreeStatusDisplayCode (length: 2)
        public short? TreeCount { get; set; } // TreeCount
        public double? Dbh { get; set; } // Dbh
        public short? FormPoint { get; set; } // FormPoint
        public short? ReportedFormFactor { get; set; } // ReportedFormFactor
        public string TopDiameterFractionCode { get; set; } // TopDiameterFractionCode (length: 2)
        public short? BoleHeight { get; set; } // BoleHeight
        public short? TotalHeight { get; set; } // TotalHeight
        public string CrownPositionDisplayCode { get; set; } // CrownPositionDisplayCode (length: 2)
        public string CrownRatioDisplayCode { get; set; } // CrownRatioDisplayCode (length: 2)
        public string VigorDisplayCode { get; set; } // VigorDisplayCode (length: 2)
        public string DamageDisplayCode { get; set; } // DamageDisplayCode (length: 2)
        public string UserDefinedDisplayCode { get; set; } // UserDefinedDisplayCode (length: 2)
        public string TreeType { get; set; } // TreeType (length: 1)
        public string Sort1 { get; set; } // Sort1 (length: 2)
        public string Grade1 { get; set; } // Grade1 (length: 2)
        public string Length1 { get; set; } // Length1 (length: 2)
        public short? BdFtLd1 { get; set; } // BdFtLD1
        public short? BdFtDd1 { get; set; } // BdFtDD1
        public short? CuFtLd1 { get; set; } // CuFtLD1
        public short? CuFtDd1 { get; set; } // CuFtDD1
        public string BdFtPd1 { get; set; } // BdFtPD1 (length: 1)
        public string Sort2 { get; set; } // Sort2 (length: 2)
        public string Grade2 { get; set; } // Grade2 (length: 2)
        public string Length2 { get; set; } // Length2 (length: 2)
        public short? BdFtLd2 { get; set; } // BdFtLD2
        public short? BdFtDd2 { get; set; } // BdFtDD2
        public short? CuFtLd2 { get; set; } // CuFtLD2
        public short? CuFtDd2 { get; set; } // CuFtDD2
        public string BdFtPd2 { get; set; } // BdFtPD1 (length: 1)
        public string Sort3 { get; set; } // Sort3 (length: 2)
        public string Grade3 { get; set; } // Grade3 (length: 2)
        public string Length3 { get; set; } // Length3 (length: 2)
        public short? BdFtLd3 { get; set; } // BdFtLD3
        public short? BdFtDd3 { get; set; } // BdFtDD3
        public short? CuFtLd3 { get; set; } // CuFtLD3
        public short? CuFtDd3 { get; set; } // CuFtDD3
        public string BdFtPd3 { get; set; } // BdFtPD1 (length: 1)
        public string Sort4 { get; set; } // Sort4 (length: 2)
        public string Grade4 { get; set; } // Grade4 (length: 2)
        public string Length4 { get; set; } // Length4 (length: 2)
        public short? BdFtLd4 { get; set; } // BdFtLD4
        public short? BdFtDd4 { get; set; } // BdFtDD4
        public short? CuFtLd4 { get; set; } // CuFtLD4
        public short? CuFtDd4 { get; set; } // CuFtDD4
        public string BdFtPd4 { get; set; } // BdFtPD1 (length: 1)
        public string Sort5 { get; set; } // Sort5 (length: 2)
        public string Grade5 { get; set; } // Grade5 (length: 2)
        public string Length5 { get; set; } // Length5 (length: 2)
        public short? BdFtLd5 { get; set; } // BdFtLD5
        public short? BdFtDd5 { get; set; } // BdFtDD5
        public short? CuFtLd5 { get; set; } // CuFtLD5
        public short? CuFtDd5 { get; set; } // CuFtDD5
        public string BdFtPd5 { get; set; } // BdFtPD1 (length: 1)
        public string Sort6 { get; set; } // Sort6 (length: 2)
        public string Grade6 { get; set; } // Grade6 (length: 2)
        public string Length6 { get; set; } // Length6 (length: 2)
        public short? BdFtLd6 { get; set; } // BdFtLD6
        public short? BdFtDd6 { get; set; } // BdFtDD6
        public short? CuFtLd6 { get; set; } // CuFtLD6
        public short? CuFtDd6 { get; set; } // CuFtDD6
        public string BdFtPd6 { get; set; } // BdFtPD1 (length: 1)
        public string Sort7 { get; set; } // Sort7 (length: 2)
        public string Grade7 { get; set; } // Grade7 (length: 2)
        public string Length7 { get; set; } // Length7 (length: 2)
        public short? BdFtLd7 { get; set; } // BdFtLD7
        public short? BdFtDd7 { get; set; } // BdFtDD7
        public short? CuFtLd7 { get; set; } // CuFtLD7
        public short? CuFtDd7 { get; set; } // CuFtDD7
        public string BdFtPd7 { get; set; } // BdFtPD1 (length: 1)
        public string Sort8 { get; set; } // Sort8 (length: 2)
        public string Grade8 { get; set; } // Grade8 (length: 2)
        public string Length8 { get; set; } // Length8 (length: 2)
        public short? BdFtLd8 { get; set; } // BdFtLD8
        public short? BdFtDd8 { get; set; } // BdFtDD8
        public short? CuFtLd8 { get; set; } // CuFtLD8
        public short? CuFtDd8 { get; set; } // CuFtDD8
        public string BdFtPd8 { get; set; } // BdFtPD1 (length: 1)
        public string Sort9 { get; set; } // Sort9 (length: 2)
        public string Grade9 { get; set; } // Grade9 (length: 2)
        public string Length9 { get; set; } // Length9 (length: 2)
        public short? BdFtLd9 { get; set; } // BdFtLD9
        public short? BdFtDd9 { get; set; } // BdFtDD9
        public short? CuFtLd9 { get; set; } // CuFtLD9
        public short? CuFtDd9 { get; set; } // CuFtDD9
        public string BdFtPd9 { get; set; } // BdFtPD1 (length: 1)
        public string Sort10 { get; set; } // Sort10 (length: 2)
        public string Grade10 { get; set; } // Grade10 (length: 2)
        public string Length10 { get; set; } // Length10 (length: 2)
        public short? BdFtLd10 { get; set; } // BdFtLD10
        public short? BdFtDd10 { get; set; } // BdFtDD10
        public short? CuFtLd10 { get; set; } // CuFtLD10
        public short? CuFtDd10 { get; set; } // CuFtDD10
        public string BdFtPd10 { get; set; } // BdFtPD1 (length: 1)
        public string Sort11 { get; set; } // Sort11 (length: 2)
        public string Grade11 { get; set; } // Grade11 (length: 2)
        public string Length11 { get; set; } // Length11 (length: 2)
        public short? BdFtLd11 { get; set; } // BdFtLD11
        public short? BdFtDd11 { get; set; } // BdFtDD11
        public short? CuFtLd11 { get; set; } // CuFtLD11
        public short? CuFtDd11 { get; set; } // CuFtDD11
        public string BdFtPd11 { get; set; } // BdFtPD1 (length: 1)
        public string Sort12 { get; set; } // Sort12 (length: 2)
        public string Grade12 { get; set; } // Grade12 (length: 2)
        public string Length12 { get; set; } // Length12 (length: 2)
        public short? BdFtLd12 { get; set; } // BdFtLD12
        public short? BdFtDd12 { get; set; } // BdFtDD12
        public short? CuFtLd12 { get; set; } // CuFtLD12
        public short? CuFtDd12 { get; set; } // CuFtDD12
        public string BdFtPd12 { get; set; } // BdFtPD1 (length: 1)
    }
}
