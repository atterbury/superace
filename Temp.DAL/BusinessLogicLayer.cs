﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlServerCe;
using System;

namespace Temp.DAL
{
    public class TempBLL
    {
        #region Gets
        public static string GetConnectionString(string pDataSource)
        {
            //string CONNECTIONSTRING = string.Format(@"data source=(LocalDB)\v11.0;attachdbfilename={0};integrated security=True;connect timeout=30;connect timeout=30;MultipleActiveResultSets=True;App=EntityFramework", pDataSource);
            //return CONNECTIONSTRING; // ecb.ToString();
            var connectionString = string.Format("Data Source={0}", pDataSource);
            System.Data.SqlClient.SqlConnectionStringBuilder scsb = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);

            var ecb = new EntityConnectionStringBuilder();
            ecb.Metadata = "res://*/TempModel.csdl|res://*/TempModel.ssdl|res://*/TempModel.msl";
            ecb.Provider = "System.Data.SqlServerCe.4.0";
            ecb.ProviderConnectionString = connectionString;

            return ecb.ToString();
        }

        public static StatsSampleData GetStatsSampleData(long v)
        {
            StatsSampleData result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsSampleDatas.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }

        public static StatsPerTree GetStatsPerTree(long v)
        {
            StatsPerTree result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsPerTrees.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }

        public static StatsStandSummary GetStatsStandSummary(long v)
        {
            StatsStandSummary result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsStandSummaries.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }

        public static StatsPerPlot GetStatsPerPlot(long v)
        {
            StatsPerPlot result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsPerPlots.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }

        public static StatsPerAcre GetStatsPerAcre(long v)
        {
            StatsPerAcre result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsPerAcres.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }

        public static StatsConfStd GetStatsConfStd(long v)
        {
            StatsConfStd result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsConfStds.FirstOrDefault(s => s.StandsSelectedId == v);
            }
            return result;
        }
        #endregion

        #region Save

        #endregion

        #region Delete
        public static void DeleteTempCountTable()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.TempCountTables.RemoveRange(ctx.TempCountTables);
                ctx.SaveChanges();
            }
        }

        public static void DeleteWrkTrees()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.WrkTrees.RemoveRange(ctx.WrkTrees);
                ctx.SaveChanges();
            }
        }

        public static void DeleteSepSelectedStand()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.SepStandsSelecteds.RemoveRange(ctx.SepStandsSelecteds);
                ctx.SaveChanges();
            }
        }

        public static void DeleteComSelectedStand()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.ComStandsSelecteds.RemoveRange(ctx.ComStandsSelecteds);
                ctx.SaveChanges();
            }
        }

        public static void DeleteWrkTreeSegments()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.WrkTreeSegments.RemoveRange(ctx.WrkTreeSegments);
                ctx.SaveChanges();
            }
        }

        public static void DeleteWrkPolePiling()
        {
            try
            {
                using (var ctx = new TempDbContext())
                {
                    ctx.Database.ExecuteSqlCommand("DELETE FROM WrkPolePiling");
                    //ctx.WrkPolePilings.RemoveRange(ctx.WrkPolePilings);
                    //ctx.SaveChanges();
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public static void DeleteRptSpeciesSortTimberEvaluation()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptSpeciesSortTimberEvaluations.RemoveRange(ctx.RptSpeciesSortTimberEvaluations);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptPole()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptPoles.RemoveRange(ctx.RptPoles);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptPiling()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptPilings.RemoveRange(ctx.RptPilings);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptCatalogSpeciesVolume()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptCatalogSpeciesVolumes.RemoveRange(ctx.RptCatalogSpeciesVolumes);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptSpeciesSummaryLogsVolumes()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptSpeciesSummaryLogsVolumes.RemoveRange(ctx.RptSpeciesSummaryLogsVolumes);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptAgeClassDistributionBySpecies()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptAgeClassDistributionBySpecies.RemoveRange(ctx.RptAgeClassDistributionBySpecies);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptPlotTreeList()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.PlotTreeLists.RemoveRange(ctx.PlotTreeLists);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptPlotTreeListVolume()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptPlotTreeListVolumes.RemoveRange(ctx.RptPlotTreeListVolumes);
                ctx.SaveChanges();
            }
        }

        //public static void DeleteAdjCruisePlots()
        //{
        //    using (var ctx = new TempDbContext())
        //    {
        //        ctx.AdjCruisePlots.RemoveRange(ctx.AdjCruisePlots);
        //        ctx.SaveChanges();
        //    }
        //}

        public static void DeleteStatsPerAcre()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsPerAcres.RemoveRange(ctx.StatsPerAcres);
                ctx.SaveChanges();
            }
        }

        public static void DeleteStatsPerPlot()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsPerPlots.RemoveRange(ctx.StatsPerPlots);
                ctx.SaveChanges();
            }
        }

        public static void DeleteStatsPerTree()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsPerTrees.RemoveRange(ctx.StatsPerTrees);
                ctx.SaveChanges();
            }
        }

        public static void DeleteStatsSampleData()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsSampleDatas.RemoveRange(ctx.StatsSampleDatas);
                ctx.SaveChanges();
            }
        }

        public static void DeleteStatsStandSummary()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsStandSummaries.RemoveRange(ctx.StatsStandSummaries);
                ctx.SaveChanges();
            }
        }

        public static void DeleteStatsConfStd()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.StatsConfStds.RemoveRange(ctx.StatsConfStds);
                ctx.SaveChanges();
            }
        }
        #endregion

        public static void DeleteWrkStands()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.WrkStands.RemoveRange(ctx.WrkStands);
                ctx.SaveChanges();
            }
        }

        public static object GetRptPlotTreeListVolume(long pId)
        {
            List<RptPlotTreeListVolume> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptPlotTreeListVolumes.Where(s => s.StandsSelectedId == pId).OrderBy(s => s.PlotNumber).ThenBy(s => s.TreeNumber).ToList();
            }
            return result;
        }

        public static List<SepStandsSelected> GetSepStandsSelected()
        {
            List<SepStandsSelected> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.SepStandsSelecteds.OrderBy(r => r.TractName).ThenBy(r => r.StandName).ToList();
            }
            return result;
        }

        public static SepStandsSelected GetSepStandsSelected(long pId)
        {
            SepStandsSelected result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.SepStandsSelecteds.FirstOrDefault(r => r.StandsId == pId);
            }
            return result;
        }

        public static List<ComStandsSelected> GetComStandsSelected()
        {
            List<ComStandsSelected> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.ComStandsSelecteds.OrderBy(r => r.TractName).ThenBy(r => r.StandName).ToList();
            }
            return result;
        }

        public static RptSpeciesSummaryLogsVolume GetRptSpeciesSummaryLogVolumeRecord(long id, string pSpecies, string pStatus)
        {
            RptSpeciesSummaryLogsVolume result;
            using (var ctx = new TempDbContext())
            {
                if (id != -1)
                    result = ctx.RptSpeciesSummaryLogsVolumes.FirstOrDefault(r => r.StandsId == id && r.Species == pSpecies && r.Status == pStatus.Trim());
                else
                    result = ctx.RptSpeciesSummaryLogsVolumes.FirstOrDefault(r => r.Species == pSpecies && r.Status == pStatus.Trim());
            }
            return result;
        }

        public static TempStandData GetTempStandDataRecord(string pTractName, string pStandName, string pSpecies, string pStatus)
        {
            TempStandData result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.TempStandDatas.FirstOrDefault(r => r.TractName == pTractName && r.StandName == pStandName && r.Species == pSpecies && r.Status == pStatus.Trim());
            }
            return result;
        }

        public static TempPerValue GetTempPerValuesRecord(string pTractName, string pStandName, string pDesc)
        {
            TempPerValue result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.TempPerValues.FirstOrDefault(r => r.TractName == pTractName && r.StandName == pStandName && r.Desc == pDesc);
            }
            return result;
        }

        public static void DeleteRptCostByLineItem()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptCostByLineItems.RemoveRange(ctx.RptCostByLineItems);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptTransportationReport()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptTransportations.RemoveRange(ctx.RptTransportations);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptSpeciesSortGradeByBdFtVolume()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptSpeciesSortGradeByBdFtVolumes.RemoveRange(ctx.RptSpeciesSortGradeByBdFtVolumes);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptLogStockSortGradeMbf()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptLogStockSortGradeMbfs.RemoveRange(ctx.RptLogStockSortGradeMbfs);
                ctx.SaveChanges();
            }
        }

        //public static void DeleteRptLogStockSortGradePercentMbf()
        //{
        //    using (var ctx = new TempDbContext())
        //    {
        //        ctx.RptLogStockPercentMbf.RemoveRange(ctx.RptLogStockPercentMbf);
        //        ctx.SaveChanges();
        //    }
        //}

        public static void DeleteRptStatistics()
        {
            DeleteStatsPerAcre();
            DeleteStatsPerPlot();
            DeleteStatsPerTree();
            DeleteStatsSampleData();
            DeleteStatsStandSummary();
            DeleteStatsConfStd();
        }

        public static void DeleteRptStandTable()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptStandTables.RemoveRange(ctx.RptStandTables);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies1()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies1.RemoveRange(ctx.RptRelationshipsBySpecies1);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies2()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies2.RemoveRange(ctx.RptRelationshipsBySpecies2);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies3()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies3.RemoveRange(ctx.RptRelationshipsBySpecies3);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies4()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies4.RemoveRange(ctx.RptRelationshipsBySpecies4);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies5()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies5.RemoveRange(ctx.RptRelationshipsBySpecies5);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies6()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies6.RemoveRange(ctx.RptRelationshipsBySpecies6);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies7()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies7.RemoveRange(ctx.RptRelationshipsBySpecies7);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies8()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies8.RemoveRange(ctx.RptRelationshipsBySpecies8);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies9()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies9.RemoveRange(ctx.RptRelationshipsBySpecies9);
                ctx.SaveChanges();
            }
        }

        public static void DeleteRptRelationshipsBySpecies10()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptRelationshipsBySpecies10.RemoveRange(ctx.RptRelationshipsBySpecies10);
                ctx.SaveChanges();
            }
        }

        public static StatsPerAcre GetPerAcreInfo()
        {
            StatsPerAcre result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.StatsPerAcres.FirstOrDefault(r => r.Seq == 1);
            }
            return result;
        }

        public static object GetRptSpeciesSummaryLogVolume(long pId)
        {
            List<RptSpeciesSummaryLogsVolume> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptSpeciesSummaryLogsVolumes.Where(s => s.StandsSelectedId == pId).ToList();
            }
            return result;
        }

        public static object GetRptSpeciesSummaryLogVolume()
        {
            List<RptSpeciesSummaryLogsVolume> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptSpeciesSummaryLogsVolumes.OrderBy(s => s.Species).ThenBy(s => s.Status).ToList();
            }
            return result;
        }

        public static RptSpeciesSortTimberEvalTotal GetRevenueBySpeciesTotals()
        {
            RptSpeciesSortTimberEvalTotal result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptSpeciesSortTimberEvalTotals.FirstOrDefault();
            }
            return result;
        }

        public static RptSpeciesSortTimberEvalTotal GetRevenueBySpeciesTotals(long id)
        {
            RptSpeciesSortTimberEvalTotal result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptSpeciesSortTimberEvalTotals.FirstOrDefault(t => t.StandsId == id);
            }
            return result;
        }

        public static void WriteRevenueBySpeciesTotals(long v, float[] speciesTotals)
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptSpeciesSortTimberEvalTotals.RemoveRange(ctx.RptSpeciesSortTimberEvalTotals);
                ctx.SaveChanges();

                RptSpeciesSortTimberEvalTotal rec = new RptSpeciesSortTimberEvalTotal();
                rec.StandsId = v;
                rec.LogAvgDia = speciesTotals[0];
                rec.LogAveLen = speciesTotals[1];
                rec.TotalLogs = speciesTotals[2];
                rec.TotalTons = speciesTotals[3];
                rec.TotalCunits = speciesTotals[4];
                rec.TotalMbf = speciesTotals[5];
                rec.DollarsPerLog = speciesTotals[6];
                rec.DollarsPerAcre = speciesTotals[7];
                rec.DollarsPerTon = speciesTotals[8];
                rec.DollarsPerCcf = speciesTotals[9];
                rec.DollarsPerMbf = speciesTotals[10];
                rec.TotalDollars = speciesTotals[11];
                ctx.SaveChanges();
            }
        }

        public static object GetRptPlotTreeList(long pId)
        {
            List<PlotTreeList> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.PlotTreeLists.Where(s => s.StandsSelectedId == pId).OrderBy(s => s.PlotNumber).ThenBy(s => s.TreeNumber).ToList();
            }
            return result;
        }

        public static List<RptStandTable> GetRptStandTable(long pId)
        {
            List<RptStandTable> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptStandTables.Where(s => s.StandsSelectedId == pId).OrderBy(s => s.Species).ThenBy(s => s.DbhClass).ToList();
            }
            return result;
        }

        public static List<RptStandTable> GetRptStandTable()
        {
            List<RptStandTable> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptStandTables.OrderBy(s => s.Species).ThenBy(s => s.DbhClass).ToList();
            }
            return result;
        }

        public static List<RptCatalogSpeciesVolume> GetRptCatalogSpeciesVolume(long pId)
        {
            List<RptCatalogSpeciesVolume> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptCatalogSpeciesVolumes.Where(s => s.StandsSelectedId == pId).OrderBy(s => s.Species).ThenBy(s => s.Age).ToList();
            }
            return result;
        }

        public static object GetRptPolePiling(long p)
        {
            List<RptPole> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptPoles.Where(s => s.StandsSelectedId == p).OrderBy(s => s.StandsSelectedId).ThenBy(s => s.Species).ThenBy(s => s.SortGroup).ThenBy(s => s.LenFeet).ToList();
            }
            return result;
        }

        public static object GetRptSpeciesSortTimberEvaluation(long p)
        {
            List<RptSpeciesSortTimberEvaluation> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptSpeciesSortTimberEvaluations.Where(s => s.StandsSelectedId == p).OrderBy(s => s.StandsSelectedId).ThenBy(s => s.Species).ThenBy(s => s.Seq).ToList();
            }
            return result;
        }

        public static float GetStandAcresForReport(long p)
        {
            SepStandsSelected result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.SepStandsSelecteds.FirstOrDefault(s => s.StandsSelectedId == p);
            }
            if (result != null)
                return (float)result.NetGeographicAcres;
            else
                return 0;
        }

        public static object GetRptCostByLineItem(long p)
        {
            List<RptCostByLineItem> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptCostByLineItems.Where(s => s.StandsSelectedId == p).OrderBy(s => s.StandsSelectedId).ThenBy(s => s.CostName).ToList();
            }
            return result;
        }

        public static List<RptCostByLineItem> GetRptCostByLineItem()
        {
            List<RptCostByLineItem> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptCostByLineItems.OrderBy(s => s.PrintOrder).ToList();
            }
            return result;
        }

        public static List<RptLogStockSortGradeMbf> GetRptLogStockSortGradeMbf(long p)
        {
            List<RptLogStockSortGradeMbf> result;
            using (var ctx = new TempDbContext())
            {
                result = ctx.RptLogStockSortGradeMbfs.Where(s => s.StandsSelectedId == p).OrderBy(s => s.StandsSelectedId).ThenBy(s => s.Sort).ThenBy(s => s.Grade).ThenBy(s => s.LogLengthClass).ToList();
            }
            return result;
        }

        public static List<RptTransportation> GetRptTransportation(long p)
        {
            List<RptTransportation> result;
            using (var ctx = new TempDbContext())
            {
                //result = ctx.RptTransportation.Where(s => s.StandsSelectedId == p).OrderBy(s => s.StandsSelectedId).ThenBy(s => s.Destination).ThenBy(s => s.Product).ThenBy(s => s.Species).ToList();
                result = ctx.RptTransportations.OrderBy(s => s.Destination).ThenBy(s => s.Product).ThenBy(s => s.Species).ToList();
            }
            return result;
        }

        public static void WriteTempCopyPlot(ref TempDbContext ctx, ref TempCopyPlot plotRec)
        {
            TempCopyPlot rec = new TempCopyPlot();
            rec.Plot1 = plotRec.Plot1;
            rec.Plot2 = plotRec.Plot2;
            rec.Plot3 = plotRec.Plot3;
            rec.Plot4 = plotRec.Plot4;
            rec.Plot5 = plotRec.Plot5;
            rec.Plot6 = plotRec.Plot6;
            rec.Plot7 = plotRec.Plot7;
            rec.Plot8 = plotRec.Plot8;
            rec.Plot9 = plotRec.Plot9;
            rec.Plot10 = plotRec.Plot10;

            rec.OrigPlot1 = plotRec.Plot1;
            rec.OrigPlot2 = plotRec.Plot2;
            rec.OrigPlot3 = plotRec.Plot3;
            rec.OrigPlot4 = plotRec.Plot4;
            rec.OrigPlot5 = plotRec.Plot5;
            rec.OrigPlot6 = plotRec.Plot6;
            rec.OrigPlot7 = plotRec.Plot7;
            rec.OrigPlot8 = plotRec.Plot8;
            rec.OrigPlot9 = plotRec.Plot9;
            rec.OrigPlot10 = plotRec.Plot10;

            rec.Plot1Exist = false;
            rec.Plot2Exist = false;
            rec.Plot3Exist = false;
            rec.Plot4Exist = false;
            rec.Plot5Exist = false;
            rec.Plot6Exist = false;
            rec.Plot7Exist = false;
            rec.Plot8Exist = false;
            rec.Plot9Exist = false;
            rec.Plot10Exist = false;

            ctx.TempCopyPlots.Add(rec);
        }
            
        public static void InitTempCopyPlot(ref TempDbContext ctx, ref TempCopyPlot plotRec)
        {
            plotRec.Plot1 = string.Empty;
            plotRec.Plot2 = string.Empty;
            plotRec.Plot3 = string.Empty;
            plotRec.Plot4 = string.Empty;
            plotRec.Plot5 = string.Empty;
            plotRec.Plot6 = string.Empty;
            plotRec.Plot7 = string.Empty;
            plotRec.Plot8 = string.Empty;
            plotRec.Plot9 = string.Empty;
            plotRec.Plot10 = string.Empty;

            plotRec.OrigPlot1 = string.Empty;
            plotRec.OrigPlot2 = string.Empty;
            plotRec.OrigPlot3 = string.Empty;
            plotRec.OrigPlot4 = string.Empty;
            plotRec.OrigPlot5 = string.Empty;
            plotRec.OrigPlot6 = string.Empty;
            plotRec.OrigPlot7 = string.Empty;
            plotRec.OrigPlot8 = string.Empty;
            plotRec.OrigPlot9 = string.Empty;
            plotRec.OrigPlot10 = string.Empty;

            plotRec.Plot1Exist = false;
            plotRec.Plot2Exist = false;
            plotRec.Plot3Exist = false;
            plotRec.Plot4Exist = false;
            plotRec.Plot5Exist = false;
            plotRec.Plot6Exist = false;
            plotRec.Plot7Exist = false;
            plotRec.Plot8Exist = false;
            plotRec.Plot9Exist = false;
            plotRec.Plot10Exist = false;
        }

        public static void DeleteRptTreeSegmentVolume()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.RptTreeSegmentVolumes.RemoveRange(ctx.RptTreeSegmentVolumes);
                ctx.SaveChanges();
            }
        }

        public static void DeleteTempPlotSpecies()
        {
            using (var ctx = new TempDbContext())
            {
                ctx.TempPlotSpecies.RemoveRange(ctx.TempPlotSpecies);
                ctx.SaveChanges();
            }
        }
    }
}
