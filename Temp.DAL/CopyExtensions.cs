﻿using Project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Temp.DAL;

namespace Temp.DAL
{

    public partial class TempDbContext : System.Data.Entity.DbContext, ITempDbContext
    {
        private static void TempDbContextStaticPartial()
        {
            // dont need this; but rev-poco complains if there isn't one
            // and it overwrites the temp.cs file, so don't put this in there
            // implement work here if needed
        }
    }


    /*
     * Copy extensions do blind copies; no if conditions!
     * Do all any conditional assignments, in other area.
     */

    public partial class RptCatalogSpeciesVolume
    {
        public void copyFrom(ReportRecord r)
        {
            this.Acres = r.Acres;
            this.Age = (byte)r.Age;
            this.AvgLogBdFt = r.BdftPerLog;
            this.AvgLogCuFt = r.CuftPerLog;
            this.BasalAreaPerAcre = r.BasalAreaPerAcre;
            this.County = r.County;
            this.Date = r.Date;
            this.Dbh = r.Dbh;
            this.Ff = r.Ff;
            this.LogsPerAcre = r.LogsPerAcre;
            this.MeasuredTreesPerAcre = r.MeasuredTreesPerAcre;
            this.NetBdFtPerAcre = r.NetBdftPerAcre;
            this.NetCuFtPerAcre = r.NetCuftPerAcre;
            this.Plots = (short)r.Plots;
            this.Range = r.Range;
            this.Section = r.Section;
            this.SiteIndex = r.SiteIndex;
            this.Source = r.Source;
            this.Species = r.Species;
            this.Stand = r.Stand;
            this.StandsId = r.StandsId;
            this.StandsSelectedId = r.StandsSelectedId;
            this.State = r.State;
            this.Status = r.Status;
            this.Stocking = r.Stocking;
            this.TotalCcf = r.TotalCcf;
            this.TotalHeight = r.TotalHeight;
            this.TotalMbf = r.TotalMbf;
            this.Township = r.Township;
            this.TractName = r.TractName;
            this.TreesPerAcre = r.TreesPerAcre;
        }
    }

    public partial class RptTreeSegmentVolume
    {
        public void copyFrom(Treesegment segment)
        {
            this.TreesId = segment.TreesId;
            this.StandsId = segment.StandsId;
            this.RealStandsId = segment.StandsId;
            this.TreeSegmentsId = segment.TreeSegmentsId;
            this.TreeType = segment.TreeType;
            this.TreeNumber = (short)segment.TreeNumber;
            this.SegmentNumber = segment.SegmentNumber;
            this.PlotNumber = segment.PlotNumber;
            this.Species = segment.Species;
            this.TreeCount = (byte)segment.TreeCount;
            this.AgeCode = (byte)segment.Age;
            this.BoleHeight = segment.BoleHeight;
            this.CalcTotalHtUsedYn = segment.CalcTotalHtUsedYn;
            this.CrownPosition = segment.CrownPosition;
            this.CrownRatio = segment.CrownRatio;
            this.Damage = segment.Damage;
            this.Dbh = segment.Dbh;
            this.FormFactor = (byte)segment.FormFactor;
            this.FormPoint = (byte)segment.FormPoint;
            this.PlotFactor = segment.PlotFactorInput;
            this.Tdf = segment.Tdf;
            this.TreeStatus = segment.TreeStatusDisplayCode;
            this.UserDefined = segment.UserDefined;
            this.Vigor = segment.Vigor;
            this.CalcTopDia = segment.CalcTopDia;
            this.CalcLen = segment.CalcLen;
            this.CubicNetVolume = segment.CubicNetVolume;
            this.ScribnerNetVolume = segment.ScribnerNetVolume;
            this.CalcButtDia = segment.CalcButtDia;
            this.CalcAccumulatedLength = segment.CalcAccumulatedLength;
            this.BdFtLd = (byte)segment.BdFtLd;
            this.BdFtDd = (byte)segment.BdFtDd;
            this.CuFtLd = (byte)segment.CuFtLd;
            this.CuFtDd = (byte)segment.CuFtDd;
            this.BdFtPd = segment.BdFtPd;
            this.GradeCode = segment.GradeCode;
            this.Length = segment.CalcLen.ToString();
            this.SortCode = segment.SortCode;
        }
    }

    public partial class RptPlotTreeListVolume
    {
        public void copyFrom(SepStandsSelected item)
        {
            this.StandsSelectedId = item.StandsSelectedId;
            this.TractName = item.TractName;
            this.StandName = item.StandName;
        }

        public void copyFrom(Tree tree)
        {
            this.TreeType = tree.TreeType;
            this.AgeCode = (byte)tree.Age;
            this.PlotFactorCode = tree.PlotFactorInput;
            this.PlotId = (int)tree.PlotId;
            this.PlotNumber = tree.PlotNumber;
            this.Species = tree.SpeciesAbbreviation;
            this.TreeStatus = tree.TreeStatusDisplayCode;
            this.StandsId = (int)tree.StandsId;
            this.Dbh = tree.Dbh;
            this.FormFactor = (byte)tree.ReportedFormFactor;
            this.TotalHeight = tree.TotalHeight;
            this.CalcTotalHtUsedYn = tree.CalcTotalHtUsedYn;
            this.TreeNumber = (int)tree.TreeNumber;
            this.TreeStatus = tree.TreeStatusDisplayCode;
            this.BasalArea = tree.BasalArea;
            this.TreesPerAcre = tree.TreesPerAcre;
            this.NetCcfPerAcre = tree.TotalNetCubicPerAcre ?? 0;
            this.NetMbfPerAcre = tree.TotalNetScribnerPerAcre ?? 0;
        }
    }
}
