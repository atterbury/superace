﻿using Project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Temp.DAL
{
    public class ReportRecord
    {

        // random properties
        public long StandsSelectedId { get; set; }
        public byte SiteIndex { get; set; }
        public byte Stocking { get; set; }

        // got from stand
        public int Plots { get; set; } = 0;
        public long StandsId {get; set;} = 0;
        public float Acres {get; set;} = 0;
        public string TractName {get; set;} = null;
        public string Stand {get; set;} = null;
        public string Township {get; set;} = null;
        public string Range {get; set;} = null;
        public string Section {get; set;} = null;
        public string State {get; set;} = null;
        public string County {get; set;} = null;
        public string Source {get; set;} = null;
        public string Date;

        // got from tree
        public int Age {get; set;} = 0;
        public string Species {get; set;} = null;
        public string Status {get; set;} = null;

        // running totals
        double _dbh = 0;
        float _tpa = 0;
        float _ba = 0;
        float _cuft = 0;
        float _bdft = 0;

        // conditional running totals (on actually measured trees)
        float _measTPA = 0;
        float _measBdft = 0;
        float _measCuft = 0;
        float _measFF = 0;
        float _measLpa = 0;
        float _measHt = 0;

        // finalized values
        public byte Ff {get; set;} = 0;
        public short TotalHeight {get; set;} = 0;
        public double TreesPerAcre {get; set;} = 0;
        public double MeasuredTreesPerAcre { get; set; } = 0;
        public double BasalAreaPerAcre {get; set;} = 0;
        public float NetBdftPerAcre { get; set; } = 0;
        public float NetCuftPerAcre { get; set; } = 0;
        public double Dbh { get; set; } = 0;
        public float TotalCcf { get; set; } = 0;
        public float TotalMbf { get; set; } = 0;
        public float LogsPerAcre { get; set; } = 0;
        public float BdftPerLog { get; set; } = 0;
        public float CuftPerLog {get; set;} = 0;
        public float LogsPerAcreXtrap { get; set; } = 0;
        public float BdftPerLogXtrap { get; set; } = 0;
        public float CuftPerLogXtrap { get; set; } = 0;

        public void PullFrom(SepStandsSelected stand)
        {
            Plots = Convert.ToInt32(stand.Plots);
            StandsId = (long)stand.StandsId;
            TractName = stand.TractName;
            Stand = stand.StandName;
            Township = stand.Township;
            Range = stand.Range;
            Section = stand.Section;
            State = stand.State;
            County = stand.County;
            Acres = Convert.ToSingle(stand.NetGeographicAcres);
            Source = stand.Source;
            if (stand.DateOfStandData != null)
            {
                DateTime d = (DateTime)stand.DateOfStandData;
                Date = string.Format("{0}/{1}", d.Month, d.Year);
            }
        }

        public void PullFrom(Stand stand)
        {
            Plots = Convert.ToInt32(stand.Plots);
            StandsId = stand.StandsId;
            TractName = stand.TractName;
            Stand = stand.StandName;
            Township = stand.Township;
            Range = stand.Range;
            Section = stand.Section;
            State = stand.State;
            County = stand.County;
            Acres = Convert.ToSingle(stand.NetGeographicAcres);
            Source = stand.Source;
            if (stand.DateOfStandData != null)
            {
                DateTime d = (DateTime)stand.DateOfStandData;
                Date = string.Format("{0}/{1}", d.Month, d.Year);
            }
        }

        public void PullFrom(Tree tree)
        {
            Age = Convert.ToByte(tree.Age);
            Species = tree.SpeciesAbbreviation;
            Status = tree.TreeStatusDisplayCode;
        }

        public byte GetSiteIndex(Stand stand, string species)
        {
            if (stand.Species11.Trim() == species || stand.Species12.Trim() == species || stand.Species13.Trim() == species)
                return (byte)stand.SiteIndex1;
            if (stand.Species21.Trim() == species || stand.Species22.Trim() == species || stand.Species23.Trim() == species)
                return (byte)stand.SiteIndex2;
            if (stand.Species31.Trim() == species || stand.Species32.Trim() == species || stand.Species33.Trim() == species)
                return (byte)stand.SiteIndex3;
            if (stand.Species41.Trim() == species || stand.Species42.Trim() == species || stand.Species43.Trim() == species)
                return (byte)stand.SiteIndex4;
            return 0;
        }

        public void AddRunning(Tree tree)
        {
            // frequently used items
            float tpa = Convert.ToSingle(tree.TreesPerAcre);
            float bdft = Convert.ToSingle(tree.TotalBdFtNetVolume) * tpa;
            float cuft = Convert.ToSingle(tree.TotalCuFtNetVolume) * tpa;
            // update running totals
            _tpa += tpa;
            _bdft += bdft;
            _cuft += cuft;
            _dbh += Convert.ToDouble(tree.Dbh);
            _ba += Convert.ToSingle(tree.BasalArea);
            // some values only counted when tree are actually measured
            if (tree.TreeType == "D")
            {
                _measTPA += tpa;
                _measBdft += bdft;
                _measCuft += cuft;
                _measLpa += Convert.ToSingle(tree.LogsPerAcre);
                // height and ff are to be averaged per tree
                _measHt += ((tree.TotalHeight == 0) ? Convert.ToSingle(tree.CalcTotalHeight) : Convert.ToSingle(tree.TotalHeight)) * tpa;
                _measFF += Convert.ToSingle(tree.ReportedFormFactor) * tpa;
            }

        }

        public void Summarize()
        {
            // instead of doing math each run, optimize by dividing once at the end
            float fPlots = (float)Plots;
            // per acre averages (true up; div by number of plots at summariziation)
            TreesPerAcre = (double)(_tpa / fPlots);
            MeasuredTreesPerAcre = (double)(_measTPA / fPlots);
            BasalAreaPerAcre = (double)(_ba / fPlots);
            LogsPerAcre = _measLpa / fPlots;
            NetBdftPerAcre = _bdft / fPlots;
            NetCuftPerAcre = _cuft / fPlots;
            // compute based on other values
            TotalCcf = NetCuftPerAcre * Acres / 100;
            TotalMbf = NetBdftPerAcre * Acres / 1000;
            // compute Dbh, if have other values
            if ((BasalAreaPerAcre > 0) && (TreesPerAcre > 0))
                Dbh = Math.Sqrt(BasalAreaPerAcre / (TreesPerAcre * 0.005454154));
            // per tree, per log averages
            if (_measTPA > 0)
            {
                Ff = (byte)(_measFF / _measTPA);
                TotalHeight = (short)(_measHt / _measTPA);
                LogsPerAcreXtrap = (float)((_measLpa / _measTPA) * TreesPerAcre);
                BdftPerLogXtrap = _bdft / LogsPerAcreXtrap;
                CuftPerLogXtrap = _cuft / LogsPerAcreXtrap;
            }
            if (_measLpa > 0)
            {
                BdftPerLog = _measBdft / _measLpa;
                CuftPerLog = _measCuft / _measLpa;
            }

        }


    }
}