﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using OfficeOpenXml;

namespace SuperACE.Excel
{
    public partial class ShowExcelForm : DevExpress.XtraEditors.XtraForm
    {
        public ShowExcelForm()
        {
            InitializeComponent();
        }

        private DataTable result = null;
        private string fileName = string.Empty;

        private void ShowExcelForm_Load(object sender, EventArgs e)
        {
            GetTheExcelFile();
        }

        private void GetTheExcelFile()
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Excel Workbook|*.xlsx", ValidateNames = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName = ofd.FileName;
                }
            }

            if (result != null)
                result.Clear();
            result = GetDataTableFromExcel(fileName, false);
            gridControl1.DataSource = result;
        }

        private DataTable GetDataTableFromExcel(string path, bool hasHeader)
        {
            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetTheExcelFile();
        }
    }
}